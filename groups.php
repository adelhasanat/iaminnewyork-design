<?php include("header.php"); ?>
<div class="floating-icon">
   <div class="scrollup-btnbox anim-side btnbox scrollup-float">
      <div class="scrollup-button float-icon"><span class="icon-holder ispan"><i class="mdi mdi-arrow-up-bold-circle"></i>
</span></div>
   </div>
</div>  
<div class="clear"></div>
<div class="container page_container group_container">
   <?php include("common/leftmenu.php"); ?>
   <div class="fixed-layout ipad-mfix">
      <div class="main-content with-lmenu groups-page main-page grid-view general-page">
         <div class="combined-column combined_md_column">
            <div class="content-box nbg">
               <div class="cbox-desc md_card_tab">
                  <div class="fake-title-area divided-nav mobile-header">
                     <ul class="tabs">
                        <li class="tab"><a class="active" href="#groups-suggested" onclick="gridBoxImgUINew('.gridBox127');">Suggested</a></li>
                        <li class="tab"><a href="#groups-member" onclick="gridBoxImgUINew('.gridBox127');">Member</a></li>
                        <li class="tab"><a href="#groups-yours" onclick="gridBoxImgUINew('.gridBox127');">Yours</a></li>
                     </ul>
                  </div>
                  <div class="tab-content view-holder grid-view">
                     <div class="tab-pane fade main-pane active in" id="groups-suggested">
                        <div class="groups-list generalbox-list">
                           <div class="row">
                              <div class="col s6 m4 l3 gridBox127">
                                 <div class="card hoverable groupCard animated fadeInUp">
                                    <a href="groups-detail.php" class="groups-box general-box">
                                       <div class="photo-holder waves-effect waves-block waves-light">
                                          <img src="images/additem-groups.png">
                                       </div>
                                       <div class="content-holder">
                                          <h4>Airlines (low-cost, budget, cheap flights)</h4>
                                          <div class="cityinfo">
                                             Maxico city, Maxico
                                          </div>
                                          <div class="username">
                                             <span class="inline-list">
                                             <i class=”mdi mdi-account-group”></i></i>
                                             <span>34,546 Members</span>
                                             </span>
                                             <span class="inline-list">
                                             <i class="mdi mdi-pencil-box-outline"></i>
                                             <span>1000 Posts</span>
                                             </span>
                                          </div>
                                          <div class="icon-line subtext">														
                                             Last activity 45 minutes ago
                                          </div>
                                          <div class="members">
                                             34,546 Members
                                          </div>
                                          <div class="action-btns">
                                             <span class="followers">Last activity 45 minutes ago</span>
                                             <span class="noClick approved" onclick="groupsJoin(event,'commevent1',this)">Member</span>
                                          </div>
                                       </div>
                                    </a>
                                 </div>
                              </div>
                              <div class="col s6 m4 l3 gridBox127">
                                 <div class="card hoverable groupCard animated fadeInUp">
                                    <a href="javascript:void(0);" class="groups-box general-box">
                                       <div class="photo-holder waves-effect waves-block waves-light">
                                          <img src="images/additem-groups.png">
                                       </div>
                                       <div class="content-holder">
                                          <h4>Europe</h4>
                                          <div class="cityinfo">
                                             Maxico city, Maxico
                                          </div>
                                          <div class="username">
                                             <span class="inline-list">
                                             <i class=”mdi mdi-account-group”></i></i>
                                             <span>34,546 Members</span>
                                             </span>
                                             <span class="inline-list">
                                             <i class="mdi mdi-pencil-box-outline"></i>
                                             <span>1000 Posts</span>
                                             </span>
                                          </div>
                                          <div class="username">
                                             <i class="mdi mdi-earth"></i>
                                             <span>Worldwide</span>
                                          </div>
                                          <div class="icon-line subtext">														
                                             Last activity 45 minutes ago
                                          </div>
                                          <div class="members">
                                             34,546 Members
                                          </div>
                                          <div class="action-btns">
                                             <span class="followers">Last activity 45 minutes ago</span>
                                             <span class="noClick" onclick="groupsJoin(event,'commevent1',this)">Join</span>
                                          </div>
                                       </div>
                                    </a>
                                 </div>
                              </div>
                              <div class="col s6 m4 l3 gridBox127">
                                 <div class="card hoverable groupCard animated fadeInUp">
                                    <a href="javascript:void(0);" class="groups-box general-box">
                                       <div class="photo-holder waves-effect waves-block waves-light">
                                          <img src="images/additem-groups.png">
                                       </div>
                                       <div class="content-holder">
                                          <h4>Hitchhikers</h4>
                                          <div class="cityinfo">
                                             Maxico city, Maxico
                                          </div>
                                          <div class="username">
                                             <span class="inline-list">
                                             <i class=”mdi mdi-account-group”></i></i>
                                             <span>34,546 Members</span>
                                             </span>
                                             <span class="inline-list">
                                             <i class="mdi mdi-pencil-box-outline"></i>
                                             <span>1000 Posts</span>
                                             </span>
                                          </div>
                                          <div class="icon-line subtext">														
                                             Last activity 45 minutes ago
                                          </div>
                                          <div class="members">
                                             34,546 Members
                                          </div>
                                          <div class="action-btns">
                                             <span class="followers">Last activity 45 minutes ago</span>
                                             <span class="noClick disabled" onclick="groupsJoin(event,'commevent1',this)">Request Sent</span>
                                          </div>
                                       </div>
                                    </a>
                                 </div>
                              </div>
                              <div class="col s6 m4 l3 gridBox127">
                                 <div class="card hoverable groupCard animated fadeInUp">
                                    <a href="javascript:void(0);" class="groups-box general-box">
                                       <div class="photo-holder waves-effect waves-block waves-light">
                                          <img src="images/additem-groups.png">
                                       </div>
                                       <div class="content-holder">
                                          <h4>Worldwide Voluteering</h4>
                                          <div class="cityinfo">
                                             Maxico city, Maxico
                                          </div>
                                          <div class="username">
                                             <span class="inline-list">
                                             <i class=”mdi mdi-account-group”></i></i>
                                             <span>34,546 Members</span>
                                             </span>
                                             <span class="inline-list">
                                             <i class="mdi mdi-pencil-box-outline"></i>
                                             <span>1000 Posts</span>
                                             </span>
                                          </div>
                                          <div class="username">
                                             <i class="mdi mdi-earth"></i>
                                             <span>Worldwide</span>
                                          </div>
                                          <div class="icon-line subtext">														
                                             Last activity 45 minutes ago
                                          </div>
                                          <div class="members">
                                             34,546 Members
                                          </div>
                                          <div class="action-btns">
                                             <span class="followers">Last activity 45 minutes ago</span>
                                             <span class="noClick disabled" onclick="groupsJoin(event,'commevent1',this)">Request Sent</span>
                                          </div>
                                       </div>
                                    </a>
                                 </div>
                              </div>
                              <div class="col s6 m4 l3 gridBox127">
                                 <div class="card hoverable groupCard animated fadeInUp">
                                    <a href="javascript:void(0);" class="groups-box general-box">
                                       <div class="photo-holder waves-effect waves-block waves-light">
                                          <img src="images/additem-groups.png">
                                       </div>
                                       <div class="content-holder">
                                          <h4>Airlines (low-cost, budget, cheap flights)</h4>
                                          <div class="cityinfo">
                                             Maxico city, Maxico
                                          </div>
                                          <div class="username">
                                             <span class="inline-list">
                                             <i class=”mdi mdi-account-group”></i></i>
                                             <span>34,546 Members</span>
                                             </span>
                                             <span class="inline-list">
                                             <i class="mdi mdi-pencil-box-outline"></i>
                                             <span>1000 Posts</span>
                                             </span>
                                          </div>
                                          <div class="icon-line subtext">														
                                             Last activity 45 minutes ago
                                          </div>
                                          <div class="members">
                                             34,546 Members
                                          </div>
                                          <div class="action-btns">
                                             <span class="followers">Last activity 45 minutes ago</span>
                                             <span class="noClick approved" onclick="groupsJoin(event,'commevent1',this)">Member</span>
                                          </div>
                                       </div>
                                    </a>
                                 </div>
                              </div>
                              <div class="col s6 m4 l3 gridBox127">
                                 <div class="card hoverable groupCard animated fadeInUp">
                                    <a href="javascript:void(0);" class="groups-box general-box">
                                       <div class="photo-holder waves-effect waves-block waves-light">
                                          <img src="images/additem-groups.png">
                                       </div>
                                       <div class="content-holder">
                                          <h4>Europe</h4>
                                          <div class="cityinfo">
                                             Maxico city, Maxico
                                          </div>
                                          <div class="username">
                                             <span class="inline-list">
                                             <i class=”mdi mdi-account-group”></i></i>
                                             <span>34,546 Members</span>
                                             </span>
                                             <span class="inline-list">
                                             <i class="mdi mdi-pencil-box-outline"></i>
                                             <span>1000 Posts</span>
                                             </span>
                                          </div>
                                          <div class="username">
                                             <i class="mdi mdi-earth"></i>
                                             <span>Worldwide</span>
                                          </div>
                                          <div class="icon-line subtext">														
                                             Last activity 45 minutes ago
                                          </div>
                                          <div class="members">
                                             34,546 Members
                                          </div>
                                          <div class="action-btns">
                                             <span class="followers">Last activity 45 minutes ago</span>
                                             <span class="noClick" onclick="groupsJoin(event,'commevent1',this)">Join</span>
                                          </div>
                                       </div>
                                    </a>
                                 </div>
                              </div>
                              <div class="col s6 m4 l3 gridBox127">
                                 <div class="card hoverable groupCard animated fadeInUp">
                                    <a href="javascript:void(0);" class="groups-box general-box">
                                       <div class="photo-holder waves-effect waves-block waves-light">
                                          <img src="images/additem-groups.png">
                                       </div>
                                       <div class="content-holder">
                                          <h4>Hitchhikers</h4>
                                          <div class="cityinfo">
                                             Maxico city, Maxico
                                          </div>
                                          <div class="username">
                                             <span class="inline-list">
                                             <i class=”mdi mdi-account-group”></i></i>
                                             <span>34,546 Members</span>
                                             </span>
                                             <span class="inline-list">
                                             <i class="mdi mdi-pencil-box-outline"></i>
                                             <span>1000 Posts</span>
                                             </span>
                                          </div>
                                          <div class="icon-line subtext">														
                                             Last activity 45 minutes ago
                                          </div>
                                          <div class="members">
                                             34,546 Members
                                          </div>
                                          <div class="action-btns">
                                             <span class="followers">Last activity 45 minutes ago</span>
                                             <span class="noClick disabled" onclick="groupsJoin(event,'commevent1',this)">Request Sent</span>
                                          </div>
                                       </div>
                                    </a>
                                 </div>
                              </div>
                              <div class="col s6 m4 l3 gridBox127">
                                 <div class="card hoverable groupCard animated fadeInUp">
                                    <a href="javascript:void(0);" class="groups-box general-box">
                                       <div class="photo-holder waves-effect waves-block waves-light">
                                          <img src="images/additem-groups.png">
                                       </div>
                                       <div class="content-holder">
                                          <h4>Worldwide Voluteering</h4>
                                          <div class="cityinfo">
                                             Maxico city, Maxico
                                          </div>
                                          <div class="username">
                                             <span class="inline-list">
                                             <i class=”mdi mdi-account-group”></i></i>
                                             <span>34,546 Members</span>
                                             </span>
                                             <span class="inline-list">
                                             <i class="mdi mdi-pencil-box-outline"></i>
                                             <span>1000 Posts</span>
                                             </span>
                                          </div>
                                          <div class="username">
                                             <i class="mdi mdi-earth"></i>
                                             <span>Worldwide</span>
                                          </div>
                                          <div class="icon-line subtext">														
                                             Last activity 45 minutes ago
                                          </div>
                                          <div class="members">
                                             34,546 Members
                                          </div>
                                          <div class="action-btns">
                                             <span class="followers">Last activity 45 minutes ago</span>
                                             <span class="noClick disabled" onclick="groupsJoin(event,'commevent1',this)">Request Sent</span>
                                          </div>
                                       </div>
                                    </a>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="tab-pane fade main-pane dis-none" id="groups-member">								
                     </div>
                     <div class="tab-pane fade main-pane groups-yours dis-none" id="groups-yours">
                        <div class="groups-list generalbox-list">
                           <div class="row">
                              <div class="col s6 m4 l3 gridBox127 add-cbox">
                                 <div class="card hoverable groupCard animated fadeInUp">
                                    <div class="groups-box general-box">
                                       <a href="javascript:void(0)" class="add-groups add-general" onclick="openAddItemModal()">
                                       <span class="icont">+</span>
                                       Add new group
                                       </a>
                                    </div>
                                 </div>
                              </div>
                              <div class="col s6 m4 l3 gridBox127">
                                 <div class="card hoverable groupCard animated fadeInUp">
                                    <a href="javascript:void(0);" class="groups-box general-box">
                                       <div class="photo-holder waves-effect waves-block waves-light">
                                          <img src="images/additem-groups.png">
                                       </div>
                                       <div class="content-holder">
                                          <h4>Airlines (low-cost, budget, cheap flights)</h4>
                                          <div class="cityinfo">
                                             Maxico city, Maxico
                                          </div>
                                          <div class="username">
                                             <span class="inline-list">
                                             <i class=”mdi mdi-account-group”></i></i>
                                             <span>34,546 Members</span>
                                             </span>
                                             <span class="inline-list">
                                             <i class="mdi mdi-pencil-box-outline"></i>
                                             <span>1000 Posts</span>
                                             </span>
                                          </div>
                                          <div class="icon-line subtext">														
                                             Last activity 45 minutes ago
                                          </div>
                                          <div class="members">
                                             34,546 Members
                                          </div>
                                          <div class="action-btns">
                                             <span class="followers">Last activity 45 minutes ago</span>
                                             <span class="noClick approved" onclick="groupsJoin(event,'groups1',this)">Member</span>
                                          </div>
                                       </div>
                                    </a>
                                 </div>
                              </div>
                              <div class="col s6 m4 l3 gridBox127">
                                 <div class="card hoverable groupCard animated fadeInUp">
                                    <a href="javascript:void(0);" class="groups-box general-box">
                                       <div class="photo-holder waves-effect waves-block waves-light">
                                          <img src="images/additem-groups.png">
                                       </div>
                                       <div class="content-holder">
                                          <h4>Europe</h4>
                                          <div class="cityinfo">
                                             Maxico city, Maxico
                                          </div>
                                          <div class="username">
                                             <span class="inline-list">
                                             <i class=”mdi mdi-account-group”></i></i>
                                             <span>34,546 Members</span>
                                             </span>
                                             <span class="inline-list">
                                             <i class="mdi mdi-pencil-box-outline"></i>
                                             <span>1000 Posts</span>
                                             </span>
                                          </div>
                                          <div class="username">
                                             <i class="mdi mdi-earth"></i>
                                             <span>Worldwide</span>
                                          </div>
                                          <div class="icon-line subtext">														
                                             Last activity 45 minutes ago
                                          </div>
                                          <div class="members">
                                             34,546 Members
                                          </div>
                                          <div class="action-btns">
                                             <span class="followers">Last activity 45 minutes ago</span>
                                             <span class="noClick" onclick="groupsJoin(event,'groups1',this)">Join</span>
                                          </div>
                                       </div>
                                    </a>
                                 </div>
                              </div>
                              <div class="col s6 m4 l3 gridBox127">
                                 <div class="card hoverable groupCard animated fadeInUp">
                                    <a href="javascript:void(0);" class="groups-box general-box">
                                       <div class="photo-holder waves-effect waves-block waves-light">
                                          <img src="images/additem-groups.png">
                                       </div>
                                       <div class="content-holder">
                                          <h4>Hitchhikers</h4>
                                          <div class="cityinfo">
                                             Maxico city, Maxico
                                          </div>
                                          <div class="username">
                                             <span class="inline-list">
                                             <i class=”mdi mdi-account-group”></i></i>
                                             <span>34,546 Members</span>
                                             </span>
                                             <span class="inline-list">
                                             <i class="mdi mdi-pencil-box-outline"></i>
                                             <span>1000 Posts</span>
                                             </span>
                                          </div>
                                          <div class="icon-line subtext">														
                                             Last activity 45 minutes ago
                                          </div>
                                          <div class="members">
                                             34,546 Members
                                          </div>
                                          <div class="action-btns">
                                             <span class="followers">Last activity 45 minutes ago</span>
                                             <span class="noClick disabled" onclick="groupsJoin(event,'groups1',this)">Request Sent</span>
                                          </div>
                                       </div>
                                    </a>
                                 </div>
                              </div>
                              <div class="col s6 m4 l3 gridBox127">
                                 <div class="card hoverable groupCard animated fadeInUp">
                                    <a href="javascript:void(0);" class="groups-box general-box">
                                       <div class="photo-holder waves-effect waves-block waves-light">
                                          <img src="images/additem-groups.png">
                                       </div>
                                       <div class="content-holder">
                                          <h4>Worldwide Voluteering</h4>
                                          <div class="cityinfo">
                                             Maxico city, Maxico
                                          </div>
                                          <div class="username">
                                             <span class="inline-list">
                                             <i class=”mdi mdi-account-group”></i></i>
                                             <span>34,546 Members</span>
                                             </span>
                                             <span class="inline-list">
                                             <i class="mdi mdi-pencil-box-outline"></i>
                                             <span>1000 Posts</span>
                                             </span>
                                          </div>
                                          <div class="username">
                                             <i class="mdi mdi-earth"></i>
                                             <span>Worldwide</span>
                                          </div>
                                          <div class="icon-line subtext">														
                                             Last activity 45 minutes ago
                                          </div>
                                          <div class="members">
                                             34,546 Members
                                          </div>
                                          <div class="action-btns">
                                             <span class="followers">Last activity 45 minutes ago</span>
                                             <span class="noClick disabled" onclick="groupsJoin(event,'groups1',this)">Request Sent</span>
                                          </div>
                                       </div>
                                    </a>
                                 </div>
                              </div>
                              <div class="col s6 m4 l3 gridBox127">
                                 <div class="card hoverable groupCard animated fadeInUp">
                                    <a href="javascript:void(0);" class="groups-box general-box">
                                       <div class="photo-holder waves-effect waves-block waves-light">
                                          <img src="images/additem-groups.png">
                                       </div>
                                       <div class="content-holder">
                                          <h4>Airlines (low-cost, budget, cheap flights)</h4>
                                          <div class="cityinfo">
                                             Maxico city, Maxico
                                          </div>
                                          <div class="username">
                                             <span class="inline-list">
                                             <i class=”mdi mdi-account-group”></i></i>
                                             <span>34,546 Members</span>
                                             </span>
                                             <span class="inline-list">
                                             <i class="mdi mdi-pencil-box-outline"></i>
                                             <span>1000 Posts</span>
                                             </span>
                                          </div>
                                          <div class="icon-line subtext">														
                                             Last activity 45 minutes ago
                                          </div>
                                          <div class="members">
                                             34,546 Members
                                          </div>
                                          <div class="action-btns">
                                             <span class="followers">Last activity 45 minutes ago</span>
                                             <span class="noClick approved" onclick="groupsJoin(event,'groups1',this)">Member</span>
                                          </div>
                                       </div>
                                    </a>
                                 </div>
                              </div>
                              <div class="col s6 m4 l3 gridBox127">
                                 <div class="card hoverable groupCard animated fadeInUp">
                                    <a href="javascript:void(0);" class="groups-box general-box">
                                       <div class="photo-holder waves-effect waves-block waves-light">
                                          <img src="images/additem-groups.png">
                                       </div>
                                       <div class="content-holder">
                                          <h4>Europe</h4>
                                          <div class="cityinfo">
                                             Maxico city, Maxico
                                          </div>
                                          <div class="username">
                                             <span class="inline-list">
                                             <i class=”mdi mdi-account-group”></i></i>
                                             <span>34,546 Members</span>
                                             </span>
                                             <span class="inline-list">
                                             <i class="mdi mdi-pencil-box-outline"></i>
                                             <span>1000 Posts</span>
                                             </span>
                                          </div>
                                          <div class="username">
                                             <i class="mdi mdi-earth"></i>
                                             <span>Worldwide</span>
                                          </div>
                                          <div class="icon-line subtext">														
                                             Last activity 45 minutes ago
                                          </div>
                                          <div class="members">
                                             34,546 Members
                                          </div>
                                          <div class="action-btns">
                                             <span class="followers">Last activity 45 minutes ago</span>
                                             <span class="noClick" onclick="groupsJoin(event,'groups1',this)">Join</span>
                                          </div>
                                       </div>
                                    </a>
                                 </div>
                              </div>
                              <div class="col s6 m4 l3 gridBox127">
                                 <div class="card hoverable groupCard animated fadeInUp">
                                    <a href="javascript:void(0);" class="groups-box general-box">
                                       <div class="photo-holder waves-effect waves-block waves-light">
                                          <img src="images/additem-groups.png">
                                       </div>
                                       <div class="content-holder">
                                          <h4>Hitchhikers</h4>
                                          <div class="cityinfo">
                                             Maxico city, Maxico
                                          </div>
                                          <div class="username">
                                             <span class="inline-list">
                                             <i class=”mdi mdi-account-group”></i></i>
                                             <span>34,546 Members</span>
                                             </span>
                                             <span class="inline-list">
                                             <i class="mdi mdi-pencil-box-outline"></i>
                                             <span>1000 Posts</span>
                                             </span>
                                          </div>
                                          <div class="icon-line subtext">														
                                             Last activity 45 minutes ago
                                          </div>
                                          <div class="members">
                                             34,546 Members
                                          </div>
                                          <div class="action-btns">
                                             <span class="followers">Last activity 45 minutes ago</span>
                                             <span class="noClick disabled" onclick="groupsJoin(event,'groups1',this)">Request Sent</span>
                                          </div>
                                       </div>
                                    </a>
                                 </div>
                              </div>
                              <div class="col s6 m4 l3 gridBox127">
                                 <div class="card hoverable groupCard animated fadeInUp">
                                    <a href="javascript:void(0);" class="groups-box general-box">
                                       <div class="photo-holder waves-effect waves-block waves-light">
                                          <img src="images/additem-groups.png">
                                       </div>
                                       <div class="content-holder">
                                          <h4>Worldwide Voluteering</h4>
                                          <div class="cityinfo">
                                             Maxico city, Maxico
                                          </div>
                                          <div class="username">
                                             <span class="inline-list">
                                             <i class=”mdi mdi-account-group”></i></i>
                                             <span>34,546 Members</span>
                                             </span>
                                             <span class="inline-list">
                                             <i class="mdi mdi-pencil-box-outline"></i>
                                             <span>1000 Posts</span>
                                             </span>
                                          </div>
                                          <div class="username">
                                             <i class="mdi mdi-earth"></i>
                                             <span>Worldwide</span>
                                          </div>
                                          <div class="icon-line subtext">														
                                             Last activity 45 minutes ago
                                          </div>
                                          <div class="members">
                                             34,546 Members
                                          </div>
                                          <div class="action-btns">
                                             <span class="followers">Last activity 45 minutes ago</span>
                                             <span class="noClick disabled" onclick="groupsJoin(event,'groups1',this)">Request Sent</span>
                                          </div>
                                       </div>
                                    </a>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <?php include('common/chat.php'); ?>
      </div>
   </div>
   <?php include("common/footer.php"); ?>
</div>
<!--add group modal-->
<div id="add_group_modal" class="modal add-item-popup custom_md_modal add-groups-popup">
   <div class="modal_content_container">
      <div class="modal_content_child modal-content">
         <div class="popup-title"> 
            <button class="hidden_close_span close_span waves-effect">
            <i class="mdi mdi-close mdi-20px compose_discard_popup"></i>
            </button>	
            <h3>Create Group</h3>
            <a type="button" class="item_done crop_done hidden_close_span close_modal waves-effect" href="javascript:void(0)" >Done</a>
         </div>
         <div class="main-pcontent">
            <form class="add-item-form">
               <div class="frow frowfull">
                  <div class="crop-holder" id="image-cropper">
                     <div class="cropit-preview"></div>
                     <div class="main-img">
                        <img src="images/additem-commevents.png">
                     </div>
                     <div class="main-img1">
                        <img id="imageid" draggable="false"/>
                     </div>
                     <div class="btnupload custom_up_load" id="upload_img_action">
                        <div class="fileUpload">
                           <i class="zmdi zmdi-camera zmdi-hc-lg"></i>
                           <input type="file" name="filupload" id="crop-file" class="upload cropit-image-input" />
                        </div>
                     </div>
                     <a  href="javascript:void(0)" class="btn btn-save image_save_btn image_save dis-none">
                     <span class="zmdi zmdi-check"></span>
                     </a>
                     <a id="removeimg" href="javascript:void(0)" class="collection_image_trash image_trash">
                     <i class="mdi mdi-close mdi-20px"></i>
                     </a>
                  </div>
               </div>
               <div class="sidepad">
                  <div class="frow">
                     <input id="group_title" type="text" class="validate item_title" placeholder="Group title" />
                  </div>
                  <div class="frow">
                     <textarea id="group_tagline" class="materialize-textarea mb0 md_textarea item_tagline" placeholder="Group tagline"></textarea>
                     <span class="char-limit">0/80</span>								
                  </div>
                  <div class="frow">
                     <textarea type="text" placeholder="Descriptions of your group" class="materialize-textarea md_textarea item_about"></textarea>
                  </div>
                  <div class="frow">
                     <input type="text" placeholder="Location" class="item_address" data-query="all" onfocus="filderMapLocationModal(this)" autocomplete="off"/>
                  </div>
                  <div class="frow">
                     <div class="row">
                        <div class="col s9">
                           <p>Review posts before feed</p>
                        </div>
                        <div class="col s3">
                           <div class="switch">
                              <label>
                              <input type="checkbox" checked />
                              <span class="lever"></span>
                              </label>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="frow security-area">
                     <label>Privacy</label>
                     <div class="right">
                        <a class="dropdown_text dropdown-button" href="javascript:void(0)" data-activates="collection_privacy">
                        <span>Public</span>
                        <i class="zmdi zmdi-caret-down"></i>
                        </a>
                        <ul id="collection_privacy" class="dropdown-content  new_drop_colle">
                           <li> <a href="javascript:void(0)">Private</a> </li>
                           <li> <a href="javascript:void(0)">Connections</a> </li>
                           <li class="customli_modal"> <a href="javascript:void(0)">Custom</a> </li>
                           <li> <a href="javascript:void(0)">Public</a> </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </form>
         </div>
      </div>
   </div>
   <div class="valign-wrapper additem_modal_footer modal-footer">
      <a href="javascript:void(0)" class="btngen-center-align close_modal open_discard_modal waves-effect">Cancel</a>
      <a href="javascript:void(0)" class="btngen-center-align waves-effect" data-class="addbtn">Create</a>
   </div>
</div>
<?php include('common/custom_modal.php'); ?>
<?php include('common/discard_popup.php'); ?>
<?php include("script.php"); ?>
</body>
</html>