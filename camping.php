
<?php include("header.php"); ?>
<?php include("common/menu.php"); ?>

<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

<div class="floating-icon">
   <div class="scrollup-btnbox anim-side btnbox scrollup-float">
      <div class="scrollup-button float-icon">
         <span class="icon-holder ispan"><i class="mdi mdi-arrow-up-bold-circle"></i></span>
      </div>
   </div>
</div>
<div class="clear"></div>  
<div class="container page_container pages_container">
   <?php include("common/leftmenu.php"); ?>
   <div class="fixed-layout ipad-mfix">
      <div class="content-box nbg">
         <div class="hcontent-holder home-section gray-section tours-page tours dine-local dine-inner-pages">
            <div class="container mt-10">
               <div class="tours-section">
                  <div class="row mx-0 valign-wrapper label-head">
                     <div class="py-20 left">
                        <h3 class="heading-inner">CAMPING</h3>
                        <p class="para-inner">Enjoy a camping experience with locals</p>
                     </div>
                     <div class="ml-auto">
                        <a href="javascript:void(0)" class="campCreateAction">
                           <span class="hidden-sm">BECOME</span> HOST
                        </a>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col l3 m6 s12 wow slideInLeft">
                        <div class="tour-box">
                           <span class="imgholder">
                              <a href="camping-detail.php">
                                 <img src="images/home-tour1.jpg">
                                 <i class="mdi mdi-delete"></i>
                              </a>
                              <div class="price-tag"><span> €70 </span></div>
                           </span>
                           <span class="descholder pb-0 pt-10">
                              <a class="dine-eventtitle" dir="auto" href="">Blungarvik Camp Site</a>
                              <p>The camp site is located next to the diver Holsar where the swimming pool</p>
                           </span>
                           <div class="camping-footer">
                              <a href=""><i class="mdi mdi-map-marker"></i> View Map</a>
                              <a href="camping-detail.php" class="right">Read More</a>
                           </div>
                        </div>
                     </div> 
                     <div class="col l3 m6 s12 wow slideInLeft">
                        <div class="tour-box">
                           <span class="imgholder">
                              <a href="camping-detail.php">
                                 <img src="images/home-tour1.jpg">
                                 <i class="mdi mdi-delete"></i>
                              </a>
                              <div class="price-tag"><span> €70 </span></div>
                           </span>
                           <span class="descholder pb-0 pt-10">
                              <a class="dine-eventtitle" dir="auto" href="">Blungarvik Camp Site</a>
                              <p>The camp site is located next to the diver Holsar where the swimming pool</p>
                           </span>
                           <div class="camping-footer">
                              <a href=""><i class="mdi mdi-map-marker"></i> View Map</a>
                              <a href="camping-detail.php" class="right">Read More</a>
                           </div>
                        </div>
                     </div>
                     <div class="col l3 m6 s12 wow slideInLeft">
                        <div class="tour-box">
                           <span class="imgholder">
                              <a href="camping-detail.php">
                                 <img src="images/home-tour1.jpg">
                                 <i class="mdi mdi-delete"></i>
                              </a>
                              <div class="price-tag"><span> €70 </span></div>
                           </span>
                           <span class="descholder pb-0 pt-10">
                              <a class="dine-eventtitle" dir="auto" href="">Blungarvik Camp Site</a>
                              <p>The camp site is located next to the diver Holsar where the swimming pool</p>
                           </span>
                           <div class="camping-footer">
                              <a href=""><i class="mdi mdi-map-marker"></i> View Map</a>
                              <a href="camping-detail.php" class="right">Read More</a>
                           </div>
                        </div>
                     </div>
                     <div class="col l3 m6 s12 wow slideInLeft">
                        <div class="tour-box">
                           <span class="imgholder">
                              <a href="camping-detail.php">
                                 <img src="images/home-tour1.jpg">
                                 <i class="mdi mdi-delete"></i>
                              </a>
                              <div class="price-tag"><span> €70 </span></div>
                           </span>
                           <span class="descholder pb-0 pt-10">
                              <a class="dine-eventtitle" dir="auto" href="">Blungarvik Camp Site</a>
                              <p>The camp site is located next to the diver Holsar where the swimming pool</p>
                           </span>
                           <div class="camping-footer">
                              <a href=""><i class="mdi mdi-map-marker"></i> View Map</a>
                              <a href="camping-detail.php" class="right">Read More</a>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="new-post-mobile clear campCreateAction">
                     <a href="javascript:void(0)" class="popup-window" ><i class="mdi mdi-account"></i></a>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<?php include("common/footer.php"); ?>
</div>
<!--add page modal-->
<div id="add-item-popup" class="modal add-item-popup custom_md_modal collec_main_cus add_collection_new dropdownheight145">
   <div class="modal_content_container">
      <div class="modal_content_child modal-content">
         <div class="popup-title ">
            <button class="hidden_close_span close_span waves-effect">
            <i class="mdi mdi-close mdi-20px compose_discard_popup"></i>
            </button>			 
            <h3>Create a page</h3>
            <a type="button" class="item_done crop_done hidden_close_span close_modal waves-effect" href="javascript:void(0)" >Done</a>
         </div>
         <div class="main-pcontent">
            <form class="add-item-form">
               <div class="frow frowfull">
                  <div class="crop-holder" id="image-cropper">
                     <div class="cropit-preview"></div>
                     <div class="main-img">
                        <img src="images/additem-collections.png" class="ui-corner-all"/>
                     </div>
                     <div class="main-img1">
                        <img id="imageid" draggable="false"/>
                     </div>
                     <div class="btnupload custom_up_load" id="upload_img_action">
                        <div class="fileUpload">
                           <i class="zmdi zmdi-camera"></i>
                           <input type="file" name="filupload" id="crop-file" class="upload cropit-image-input" />
                        </div>
                     </div>
                     <a  href="javascript:void(0)" class="btn btn-save image_save_btn image_save dis-none">
                     <span class="zmdi zmdi-check"></span>
                     </a>
                     <a id="removeimg" href="javascript:void(0)" class="collection_image_trash image_trash">
                     <i class="mdi mdi-close" ></i>
                     </a>
                  </div>
               </div>
               <div class="sidepad">
                  <div class="frow">
                     <input id="page_title" type="text" class="validate item_title" placeholder="Page title" />					
                  </div>
                  <div class="frow dropdown782">
                     <select id="pageCatDrop1" class="pageservices" data-fill="n" data-action="pageservices" data-selectore="pageservices">
                     <?php
                        $page = array("Bags/Luggage" => "Bags/Luggage", "Camera/Photo" => "Camera/Photo", "Cars" => "Cars", "Clothing" => "Clothing", "Entertainment" => "Entertainment", "Professional Services" => "Professional Services", "Sporting Goods" => "Sporting Goods", "Kitchen/Cooking" => "Kitchen/Cooking", "Concert Tour" => "Concert Tour", "Concert Venue" => "Concert Venue", "Food/Beverages" => "Food/Beverages", "Outdoor Gear" => "Outdoor Gear", "Tour Operator" => "Tour Operator", "Travel Agency" => "Travel Agency", "Travel Services" => "Travel Services", "Attractions/Things to Do" => "Attractions/Things to Do", "Event Planning/Event Services" => "Event Planning/Event Services", "Hotel" => "Hotel", "Landmark" => "Landmark", "Movie Theater" => "Movie Theater", "Museum/Art gallery" => "Museum/Art gallery", "Outdoor Gear/Sporting Goods" => "Outdoor Gear/Sporting Goods", "Public Places" => "Public Places", "Travel Site" => "Travel Site", "Travel Destination" => "Travel Destination", "Organization" => "Organization", "Website" => "Website");
                        foreach ($page as $s9032n) {
                          echo "<option value=".$s9032n.">$s9032n</option>";
                        }
                     ?>
                     </select>
                  </div>
                  <div class="frow">
                     <textarea id="Collection_tagline" class="materialize-textarea mb0 md_textarea item_tagline" placeholder="Short description of your page"></textarea>
                     <span class="char-limit">0/80</span>								
                  </div>
                  <div class="frow">
                     <textarea type="text" placeholder="Tell people more about the page" class="materialize-textarea md_textarea item_about"></textarea>
                  </div>
                  <div class="frow">
                     <input type="text" id="compose_mapmodalAction" class="validate item_title compose_mapmodalAction" placeholder="Bussines address 'City/Country'" data-query="all" onfocus="filderMapLocationModal(this)" autocomplete="off">
                  </div>
                  <div class="frow">
                     <input id="extweb" type="text" class="validate item_title" placeholder="List your external website, if you have one" />
                  </div>
                  <div class="frow">
                     <span class="icon-span"><input type="radio" id="agreeemailpage" name="verify-radio"></span>
                     <p>Veryfiy ownership by sending  a text message to following email</p>
                  </div>
                  <div class="frow">
                     <input type="text" placeholder="Your company email address">
                  </div>
                  <div class="frow">
                     <input type="checkbox" id="create_page" />
                     <label for="create_page">I verify that I am the official representative of this entity and have the right to act on behalf of my entity in the creation of this page.</label>						
                  </div>
               </div>
            </form>
         </div>
      </div>
   </div>
   <div class="valign-wrapper additem_modal_footer modal-footer">
      <a href="javascript:void(0)" class="btngen-center-align close_modal open_discard_modal waves-effect">Cancel</a>
      <a href="javascript:void(0)" class="btngen-center-align waves-effect">Create</a>
   </div>
</div>

<!-- camp create modal -->
<div id="campCreateModal" class="modal tbpost_modal custom_modal split-page main_modal cust-pop dicrease-popup-compose event-detail-modal">
   <div class="modal_content_container">
      <div class="modal_content_child modal-content">
         <div class="popup-title ">
            <button class="hidden_close_span close_span waves-effect">
            <i class="mdi mdi-close mdi-20px compose_discard_popup"></i>
            </button>         
            <h3>Camp with locals detail</h3>
            <a type="button" class="item_done crop_done hidden_close_span custom_close waves-effect" href="javascript:void(0)">Done</a>
         </div>
         <div class="custom_modal_content modal_content" id="createpopup">
            <div class="ablum-yours profile-tab">
               <div class="ablum-box detail-box">
                  <div class="content-holder main-holder">
                     <div class="summery">
                        <div class="dsection bborder expandable-holder expanded">
                           <div class="form-area expandable-area">
                              <form class="ablum-form">
                                 <div class="form-box">
                                    <div class="fulldiv">
                                       <div class="frow">
                                          <div class="caption-holder">
                                             <label>Title</label>
                                          </div>
                                          <div class="detail-holder">
                                             <div class="input-field">
                                                <input type="text" placeholder="Experience title: camping under the moon" class="fullwidth locinput "/>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="fulldiv">
                                       <div class="row">
                                          <div class="col s6">
                                             <div class="frow pr-5 dropdown782">
                                                <div class="caption-holder">
                                                   <label>Min guests</label>
                                                </div>
                                                <a class="dropdown_text dropdown-button-left" href="javascript:void(0)" data-activates="campMinGuest">
                                                   <span>1</span>
                                                   <i class="zmdi zmdi-caret-down"></i>
                                                </a>
                                                <ul id="campMinGuest" class="dropdown-privacy dropdown-content custom_dropdown select-dropdown guest-ddl">
                                                   <li>
                                                      <a href="javascript:void(0)">1</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">2</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">3</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">4</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">5</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">6</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">7</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">8</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">9</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">10</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">11</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">12</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">13</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">14</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">15</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">16</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">17</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">18</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">19</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">20</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">21</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">22</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">23</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">24</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">25</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">26</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">27</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">28</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">29</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">30</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">31</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">32</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">33</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">34</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">35</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">36</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">37</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">38</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">39</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">40</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">41</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">42</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">43</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">44</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">45</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">46</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">47</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">48</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">49</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">50</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">51</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">52</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">53</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">54</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">55</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">56</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">57</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">58</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">59</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">60</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">61</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">62</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">63</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">64</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">65</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">66</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">67</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">68</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">69</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">70</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">71</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">72</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">73</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">74</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">75</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">76</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">77</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">78</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">79</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">80</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">81</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">82</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">83</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">84</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">85</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">86</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">87</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">88</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">89</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">90</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">91</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">92</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">93</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">94</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">95</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">96</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">97</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">98</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">99</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">100</option></a>
                                                   </li>
                                                </ul>
                                             </div>
                                          </div>
                                          <div class="col s6">
                                             <div class="frow pl-5 dropdown782">
                                                <div class="caption-holder">
                                                   <label>Max guests</label>
                                                </div>
                                                <a class="dropdown_text dropdown-button-left" href="javascript:void(0)" data-activates="campMaxGuest">
                                                   <span>4</span>
                                                   <i class="zmdi zmdi-caret-down"></i>
                                                </a>
                                                <ul id="campMaxGuest" class="dropdown-privacy dropdown-content custom_dropdown select-dropdown guest-ddl">
                                                   <li>
                                                      <a href="javascript:void(0)">1</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">2</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">3</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">4</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">5</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">6</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">7</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">8</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">9</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">10</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">11</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">12</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">13</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">14</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">15</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">16</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">17</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">18</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">19</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">20</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">21</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">22</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">23</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">24</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">25</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">26</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">27</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">28</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">29</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">30</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">31</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">32</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">33</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">34</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">35</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">36</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">37</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">38</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">39</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">40</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">41</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">42</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">43</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">44</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">45</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">46</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">47</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">48</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">49</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">50</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">51</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">52</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">53</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">54</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">55</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">56</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">57</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">58</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">59</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">60</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">61</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">62</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">63</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">64</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">65</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">66</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">67</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">68</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">69</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">70</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">71</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">72</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">73</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">74</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">75</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">76</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">77</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">78</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">79</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">80</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">81</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">82</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">83</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">84</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">85</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">86</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">87</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">88</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">89</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">90</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">91</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">92</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">93</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">94</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">95</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">96</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">97</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">98</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">99</option></a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">100</option></a>
                                                   </li>
                                                </ul>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="fulldiv">
                                       <div class="frow">
                                          <div class="caption-holder">
                                             <div class="row">
                                                <div class="col s4">
                                                   <label>Rate per adult guest</label>
                                                </div>
                                                <div class="col s6">
                                                   <div class="detail-holder">
                                                      <div class="input-field">
                                                         <input type="text" placeholder="20" class="fullwidth input-rate" id="createlocation"/>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="col s2">
                                                   <a class="dropdown_text dropdown-button currency_drp" href="javascript:void(0)" data-activates="currency_handler">
                                                      <span class="currency_label">USD</span>
                                                      <i class="zmdi zmdi-caret-down"></i>
                                                   </a>
                                                   <ul id="currency_handler" class="dropdown-privacy dropdown-content custom_dropdown">
                                                      <?php
                                                      $fee = array("USD", "EUR", "YEN", "CAD", "AUE");
                                                      foreach ($fee as $s8032n) {
                                                         ?>
                                                         <li> <a href="javascript:void(0)"><?=$s8032n?></a> </li>
                                                         <?php
                                                      }
                                                      ?>
                                                   </ul>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="fulldiv">
                                       <div class="frow">
                                          <div class="caption-holder">
                                             <label>Describe your camp</label>
                                          </div>
                                          <div class="detail-holder">
                                             <div class="input-field">
                                                <input type="text" placeholder="Tell people about your camp" class="fullwidth locinput "/>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="row">
                                       <div class="col s6">
                                          <div class="fulldiv mobile275 pr-5">
                                             <div class="frow">
                                                <div class="caption-holder">
                                                   <label>Camp Location</label>
                                                </div>
                                                <div class="detail-holder">
                                                   <div class="input-field">
                                                      <input type="text" placeholder="Enter city name" class="fullwidth locinput" data-query="all" onfocus="filderMapLocationModal(this)" id="createlocation"/>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="col s6">
                                          <div class="fulldiv mobile275 pl-5">
                                             <div class="frow">
                                                <div class="caption-holder">
                                                   <label>Telephone</label>
                                                </div>
                                                <div class="detail-holder">
                                                   <div class="input-field">
                                                      <input type="text" placeholder="Enter telephone number" class="fullwidth locinput "/>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="row">
                                       <div class="col s6">
                                          <div class="fulldiv mobile275 pr-5">
                                             <div class="frow">
                                                <div class="caption-holder">
                                                   <label>E-mail</label>
                                                </div>
                                                <div class="detail-holder">
                                                   <div class="input-field">
                                                      <input type="text" placeholder="Enter email address" class="fullwidth locinput "/>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="col s6">
                                          <div class="fulldiv mobile275 pl-5">
                                             <div class="frow">
                                                <div class="caption-holder">
                                                   <label>Website</label>
                                                </div>
                                                <div class="detail-holder">
                                                   <div class="input-field">
                                                      <input type="text" placeholder="Enter website url" class="fullwidth locinput "/>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="fulldiv">
                                       <div class="frow">
                                          <div class="caption-holder">
                                             <label>Opening period</label>
                                          </div>
                                          <div class="detail-holder">
                                             <div class="input-field">
                                                <input type="text" placeholder="28/5 - 15/9" class="fullwidth locinput" name="period"/>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="fulldiv">
                                       <div class="frow">
                                          <div class="caption-holder mb-10">
                                             <label>Services</label>
                                          </div>
                                          <div class="detail-holder">
                                             <a href="javascript:void(0)" class="check-image">
                                                <div class="image-select"></div>
                                                <img alt="Waste tank discharge" title="Waste tank discharge" src="images/services-icon/1.png">
                                             </a>
                                             <a href="javascript:void(0)" class="check-image">
                                                <div class="image-select"></div>
                                                <img alt="Public lavatory" title="Public lavatory" src="images/services-icon/2.png">
                                             </a>
                                             <a href="javascript:void(0)" class="check-image">
                                                <div class="image-select"></div>
                                                <img alt="Walking path" title="Walking path" src="images/services-icon/3.png">
                                             </a>
                                             <a href="javascript:void(0)" class="check-image">
                                                <div class="image-select"></div>
                                                <img alt="Swimming pool" title="Swimming pool" src="images/services-icon/4.png">
                                             </a>
                                             <a href="javascript:void(0)" class="check-image">
                                                <div class="image-select"></div>
                                                <img alt="Fishing permits" title="Fishing permits" src="images/services-icon/5.png">
                                             </a>
                                             <a href="javascript:void(0)" class="check-image">
                                                <div class="image-select"></div>
                                                <img alt="Cooking facilities" title="Cooking facilities" src="images/services-icon/6.png">
                                             </a>
                                             <a href="javascript:void(0)" class="check-image">
                                                <div class="image-select"></div>
                                                <img alt="Sports hall" title="Sports hall" src="images/services-icon/7.png">
                                             </a>
                                             <a href="javascript:void(0)" class="check-image">
                                                <div class="image-select"></div>
                                                <img alt="Washing machine" title="Washing machine" src="images/services-icon/8.png">
                                             </a>
                                             <a href="javascript:void(0)" class="check-image">
                                                <div class="image-select"></div>
                                                <img alt="Hot pot" title="Hot pot" src="images/services-icon/9.png">
                                             </a>
                                             <a href="javascript:void(0)" class="check-image">
                                                <div class="image-select"></div>
                                                <img alt="Sports field" title="Sports field" src="images/services-icon/10.png">
                                             </a>
                                             <a href="javascript:void(0)" class="check-image">
                                                <div class="image-select"></div>
                                                <img alt="Shower" title="Shower" src="images/services-icon/11.png">
                                             </a>
                                             <a href="javascript:void(0)" class="check-image">
                                                <div class="image-select"></div>
                                                <img alt="Golf course" title="Golf course" src="images/services-icon/12.png">
                                             </a>
                                             <a href="javascript:void(0)" class="check-image">
                                                <div class="image-select"></div>
                                                <img alt="Sauna" title="Sauna" src="images/services-icon/13.png">
                                             </a>
                                             <a href="javascript:void(0)" class="check-image">
                                                <div class="image-select"></div>
                                                <img alt="Play ground" title="Play ground" src="images/services-icon/14.png">
                                             </a>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="frow nomargin new-post">
                                       <div class="caption-holder">
                                          <label>Awesome photos help guests want to join up</label>
                                       </div>
                                       <div class="detail-holder">
                                          <div class="input-field ">
                                             <div class="post-photos new_pic_add">
                                                <div class="img-row">
                                                   <div class="img-box">
                                                      <div class="custom-file addimg-box add-photo ablum-add">
                                                         <span class="icont">+</span><br><span class="">Upload photo</span>
                                                         <div class="addimg-icon">
                                                         </div>
                                                         <input class="upload custom-upload remove-custom-upload" title="Choose a file to upload" required="" data-class=".post-photos .img-row" multiple="true" type="file">
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <p class="photolabelinfo">Please add three cover photos for your camp profile</p>
                                    </div>
                                 </div>
                              </form>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="valign-wrapper additem_modal_footer modal-footer">
      <a href="javascript:void(0)" class="btngen-center-align close_modal open_discard_modal waves-effect">Cancel</a>
      <a href="javascript:void(0)" class="btngen-center-align waves-effect">Publish</a>
   </div>
</div>

<?php include('common/discard_popup.php'); ?>
<div id="compose_mapmodal" class="modal map_modal compose_inner_modal modalxii_level1">
   <?php include('common/map_modal.php'); ?>
</div>
<?php include("script.php"); ?>			

</body>
</html>