<?php include("header.php"); ?>
<?php include("common/menu.php"); ?>
<div class="floating-icon">
   <div class="scrollup-btnbox anim-side btnbox scrollup-float">
      <div class="scrollup-button float-icon">
         <span class="icon-holder ispan">
            <i class="mdi mdi-arrow-up-bold-circle"></i>
         </span>
      </div> 
   </div> 
</div>
<div class="clear"></div>
<?php include("common/leftmenu.php"); ?>
<div class="fixed-layout">
   <div class="main-content with-lmenu hotels-page  main-page transheader-page">
      <div class="combined-column hotels-wrapper wide-open">
         <div class="content-box">
            <div class="banner-section">
               <div class="search-whole hotels-search container">
                  <div class="frow">
                     <div class="row row-option">
                        <div class="col m3 s12 map-desktop">
                           <div class="map-holder">
                              <div id="hotelmaplink">
                               <a href="hotelsmap.php"><i class="mdi mdi-map-marker"></i> View map</a>
                             </div>
                              <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3110.3465133386144!2d-9.167423685010494!3d38.77868997958898!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xd193295d5b45545%3A0x3f9e7b6a5f00e12c!2sPerta!5e0!3m2!1sen!2sin!4v1481089901870" width="600" height="450" frameborder="0" allowfullscreen></iframe>
                           </div>
                        </div>
                        <div class="col m9 s12 filter-inputs">
                           <h3 class="hotels-hdng"><?=$st_nm_S?> Hotels</h3>
                           <p class="filters-price">Lowest prices for</p>  
                           <div class="col l4 m4 s12">
                              <div class="sliding-middle-out input-field anim-area dateinput fullwidth">
                                 <input type="text" placeholder="— / — / —" class="form-control datepickerinput check-in" data-query="M" data-toggle="datepicker" readonly>
                                 <label for="first_name" class="active">Check In</label>
                              </div>
                           </div>
                           <div class="col l4 m4 s12">
                              <div class="sliding-middle-out input-field anim-area dateinput fullwidth">
                                 <input type="text" placeholder="— / — / —" class="form-control datepickerinput check-out" data-query="M" data-toggle="datepicker" readonly>
                                 <label for="first_name" class="active">Check Out</label>
                              </div>
                           </div>
                           <div class="col l4 m4 s12">
                              <div class="custom-drop">
                                 <div class="dropdown input-field dropdown-custom dropdown-xsmall">
                                    <a href="javascript:void(0)" class="dropdown-toggle dropdown-button" data-activates="dropdown-rooms">
                                    <i class="mdi mdi-account"></i><span class="sword">Single Room</span> <span class="mdi mdi-menu-down right caret"></span>
                                    </a>
                                    <label for="first_name" class="active">Guests</label>
                                    <ul class="dropdown-content" id="dropdown-rooms">
                                       <li><a href="javascript:void(0)"><span class="sword">Single Room</span></a></li>
                                       <li><a href="javascript:void(0)"><i class=”mdi mdi-account-group”></i></i><span class="sword">Double Room</span></a></li>
                                    </ul>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="row filter-map"> 
                        <!-- <a href="" class="col s6"><i class="mdi mdi-tune"></i>Filter</a> -->
                        <a href="javascript:void(0)" class="col s12 toggle-map" onclick="toggleDiv('div1', 'hotelId')"><i class="mdi mdi-map-marker"></i>Map</a>
                     </div>
                     <div class="row m-0 map-mobile toggle-maphotels" id="div1">
                        <div class="map-holder">
                           <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3110.3465133386144!2d-9.167423685010494!3d38.77868997958898!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xd193295d5b45545%3A0x3f9e7b6a5f00e12c!2sPerta!5e0!3m2!1sen!2sin!4v1481089901870" width="600" height="450" frameborder="0" allowfullscreen></iframe>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="container">
               <div class="cbox-desc">
                  <div class="outer-holder">
                     <div class="row">
                        <div class="search-area side-area">
                           <a href="javascript:void(0)" class="expand-link" onclick="mng_drop_searcharea(this)">
                           <span class="desc-text"><i class="mdi mdi-menu-right"></i>Advanced Search</span>
                           <span class="mbl-text"><i class="mdi mdi-tune mdi-20px grey-text"></i></span>
                           </a>
                           <div class="expandable-area">
                              <div class="content-box bshadow">
                                 <a href="javascript:void(0)" class="closearea" onclick="mng_drop_searcharea(this)">
                                 <i class="mdi mdi-close"></i>
                                 </a>
                                 <div class="cbox-desc">
                                    <div class="srow mb0">
                                       <h6 class="mb-20">Your Budget</h6>
                                       <div class="range-slider price-slider">
                                          <div id="price-slider"></div>
                                          <!-- Values -->                                   
                                          <div class="row mb-0 mt-10">
                                             <div class="range-value col s6">
                                                <span id="value-min">$0</span>
                                             </div>
                                             <div class="range-value col s6 right-align">
                                                <span id="value-max">$5000</span>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="srow">
                                       <h6>Rate Options</h6>
                                       <ul>
                                          <li>
                                             <div class="entertosend leftbox">
                                                <p>
                                                   <input type="checkbox" id="freeCan" />
                                                   <label for="freeCan">
                                                      <span class="stars-holder">Free Cancellation</span>
                                                   </label>
                                                </p>
                                             </div>
                                          </li>
                                          <li>
                                             <div class="entertosend leftbox">
                                                <p>
                                                   <input type="checkbox" id="reserve">
                                                   <label for="reserve">
                                                      <span class="stars-holder">Reserve now, pay at stay</span>
                                                   </label>
                                                </p>
                                             </div>
                                          </li>
                                       </ul>
                                    </div>
                                    <div class="srow traveler-rating-sec">
                                       <h6>Guest rating</h6>
                                       <ul>
                                          <li>
                                             <div class="entertosend leftbox">
                                                <p>
                                                   <input type="radio" name="traveler-rating" id="test6">
                                                   <label for="test6">
                                                      <span class="checks-holder">
                                                         <i class="mdi mdi-radiobox-marked active"></i>
                                                         <i class="mdi mdi-radiobox-marked active"></i>
                                                         <i class="mdi mdi-radiobox-marked active"></i>
                                                         <i class="mdi mdi-radiobox-marked active"></i>
                                                         <i class="mdi mdi-radiobox-marked active"></i>
                                                      </span>
                                                      <span class="filter-count">126</span>
                                                   </label>
                                                </p>
                                             </div>
                                          </li>
                                          <li>
                                             <div class="entertosend leftbox">
                                                <p>
                                                   <input type="radio" name="traveler-rating" id="test7">
                                                   <label for="test7">
                                                      <span class="checks-holder">
                                                         <i class="mdi mdi-radiobox-marked active"></i>
                                                         <i class="mdi mdi-radiobox-marked active"></i>
                                                         <i class="mdi mdi-radiobox-marked active"></i>
                                                         <i class="mdi mdi-radiobox-marked active"></i>
                                                         <i class="mdi mdi-radiobox-marked"></i>
                                                         <span>& up</span>
                                                      </span>
                                                      <span class="filter-count">345</span>
                                                   </label>
                                                </p>
                                             </div>
                                          </li>
                                          <li>
                                             <div class="entertosend leftbox">
                                                <p>
                                                   <input type="radio" name="traveler-rating" id="test8">
                                                   <label for="test8">
                                                      <span class="checks-holder">
                                                         <i class="mdi mdi-radiobox-marked active"></i>
                                                         <i class="mdi mdi-radiobox-marked active"></i>
                                                         <i class="mdi mdi-radiobox-marked active"></i>
                                                         <i class="mdi mdi-radiobox-marked"></i>
                                                         <i class="mdi mdi-radiobox-marked"></i>
                                                         <span>& up</span>
                                                      </span>
                                                      <span class="filter-count">343</span>
                                                   </label>
                                                </p>
                                             </div>
                                          </li>
                                          <li>
                                             <div class="entertosend leftbox">
                                                <p>
                                                   <input type="radio" name="traveler-rating" id="test9">
                                                   <label for="test9">
                                                      <span class="checks-holder">
                                                         <i class="mdi mdi-radiobox-marked active"></i>
                                                         <i class="mdi mdi-radiobox-marked active"></i>
                                                         <i class="mdi mdi-radiobox-marked"></i>
                                                         <i class="mdi mdi-radiobox-marked"></i>
                                                         <i class="mdi mdi-radiobox-marked"></i>
                                                         <span>& up</span>
                                                      </span>
                                                      <span class="filter-count">653</span>
                                                   </label>
                                                </p>
                                             </div>
                                          </li>
                                          <li>
                                             <div class="entertosend leftbox">
                                                <p>
                                                   <input type="radio" name="traveler-rating" id="test10">
                                                   <label for="test10">
                                                      <span class="checks-holder">
                                                         <i class="mdi mdi-radiobox-marked active"></i>
                                                         <i class="mdi mdi-radiobox-marked"></i>
                                                         <i class="mdi mdi-radiobox-marked"></i>
                                                         <i class="mdi mdi-radiobox-marked"></i>
                                                         <i class="mdi mdi-radiobox-marked"></i>
                                                         <span>& up</span>
                                                      </span>
                                                      <span class="filter-count">234</span>
                                                   </label>
                                                </p>
                                             </div>
                                          </li>
                                       </ul>
                                    </div>
                                    <div class="srow">
                                       <h6>Hotel Class</h6>
                                       <ul>
                                          <li>
                                             <div class="entertosend leftbox">
                                                <p>
                                                   <input type="checkbox" id="five_star" />
                                                   <label for="five_star">
                                                      <span class="stars-holder">5 stars</span>
                                                      <span class="filter-count">26</span>
                                                   </label>
                                                </p>
                                             </div>
                                          </li>
                                          <li>
                                             <div class="entertosend leftbox">
                                                <p>
                                                   <input type="checkbox" id="four_star">
                                                   <label for="four_star">
                                                      <span class="stars-holder">4 stars</span>
                                                      <span class="filter-count">23</span>
                                                   </label>
                                                </p>
                                             </div>
                                          </li>
                                          <li>
                                             <div class="entertosend leftbox">
                                                <p>
                                                   <input type="checkbox" id="three_star">
                                                   <label for="three_star">
                                                      <span class="stars-holder">3 stars</span>
                                                      <span class="filter-count">76</span>
                                                   </label>
                                                </p>
                                             </div>
                                          </li>
                                          <li>
                                             <div class="entertosend leftbox">
                                                <p>
                                                   <input type="checkbox" id="two_star">
                                                   <label for="two_star">
                                                      <span class="stars-holder">2 stars</span>
                                                      <span class="filter-count">89</span>
                                                   </label>
                                                </p>
                                             </div>
                                          </li>
                                          <li>
                                             <div class="entertosend leftbox">
                                                <p>
                                                   <input type="checkbox" id="one_star">
                                                   <label for="one_star">
                                                      <span class="stars-holder">1 stars</span>
                                                      <span class="filter-count">12</span>
                                                   </label>
                                                </p>
                                             </div>
                                          </li>
                                       </ul>
                                    </div>
                                    <div class="srow">
                                       <h6>Amenities</h6>
                                       <ul class="ul-amenities">
                                          <li>
                                             <a href="javascript:void(0)"><img src="images/amenity-spa.png" /><span>Spa</span></a>
                                          </li>
                                          <li>
                                             <a href="javascript:void(0)"><img src="images/amenity-beach.png" /><span>Beach</span></a>
                                          </li>
                                          <li>
                                             <a href="javascript:void(0)"><img src="images/amenity-wifi.png" /><span>Wifi</span></a>
                                          </li>
                                          <li>
                                             <a href="javascript:void(0)"><img src="images/amenity-breakfast.png" /><span>Breakfast</span></a>
                                          </li>
                                          <li>
                                             <a href="javascript:void(0)"><img src="images/amenity-pool.png" /><span>Pool</span></a>
                                          </li>
                                          <li>
                                             <a href="javascript:void(0)"><img src="images/amenity-spa.png" /><span>Spa</span></a>
                                          </li>
                                          <li>
                                             <a href="javascript:void(0)"><img src="images/amenity-beach.png" /><span>Beach</span></a>
                                          </li>
                                          <li>
                                             <a href="javascript:void(0)"><img src="images/amenity-breakfast.png" /><span>Breakfast</span></a>
                                          </li>
                                       </ul>
                                    </div>
                                    <div class="btn-holder">
                                       <a href="javascript:void(0)" class="btn-custom">Reset Filters</a>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="tcontent-holder">
                           <div class="top-stuff">
                              <div class="more-actions">
                                 <div class="sorting left">
                                    <label>Sort by</label>
                                    <div class="select-holder">
                                       <select class="select2" tabindex="-1" >
                                          <option>Pricing</option>
                                          <option>Ratings</option>
                                       </select>
                                    </div>
                                 </div>
                              </div>
                              <h6>1300 hotels found in <span><?=$st_nm_S?></span></h6>
                           </div>
                           <div class="hotel-list">
                              <div class="moreinfo-outer">
                                 <div class="places-content-holder">
                                    <div class="list-holder">
                                       <div class="hotel-list">
                                          <ul>
                                             <li>
                                                <div class="hotel-li expandable-holder dealli mobilelist">
                                                   <div class="summery-info">
                                                      <div class="imgholder">
                                                         <div class="owl-carousel owl-theme owl-hotels">
                                                            <div class="item"><img src="images/hotel-demo.png" alt="The Last of us"></div>
                                                            <div class="item"><img src="images/hotel-demo.png" alt="GTA V"></div>
                                                            <div class="item"><img src="images/hotel-demo.png" alt="Mirror Edge"></div>
                                                         </div>
                                                      </div>
                                                      <div class="descholder">
                                                         <a href="javascript:void(0)" class="expand-link" onclick="mng_expandable(this,'hasClose'),setHideHeader(this,'hotels','hide')">
                                                            <h4>Hyatt Regency Dubai Creek
                                                            </h4>
                                                            <p class="hotel-location"><?=$st_nm_S?></p>
                                                            <div class="clear"></div>
                                                            <div class="reviews-link">
                                                               <span class="review-count">54 reviews</span>
                                                               <span class="checks-holder">
                                                               <i class="mdi mdi-radiobox-marked active"></i>
                                                               <i class="mdi mdi-radiobox-marked active"></i>
                                                               <i class="mdi mdi-radiobox-marked active"></i>
                                                               <i class="mdi mdi-radiobox-marked active"></i>
                                                               <i class="mdi mdi-radiobox-marked active"></i>
                                                               <label>Excellent - 88/100</label>
                                                               </span>
                                                            </div>
                                                            <div class="hotel-amenities">
                                                               <ul class="ul-amenities mb0">
                                                                  <li>
                                                                     <a href="javascript:void(0)"><img src="images/amenity-spa.png" /><span>Spa</span></a>
                                                                  </li>
                                                                  <li>
                                                                     <a href="javascript:void(0)"><img src="images/amenity-beach.png" /><span>Beach</span></a>
                                                                  </li>
                                                                  <li>
                                                                     <a href="javascript:void(0)"><img src="images/amenity-wifi.png" /><span>Wifi</span></a>
                                                                  </li>
                                                                  <li>
                                                                     <a href="javascript:void(0)" data-activates="moreAmenities1" class="dropdown-button more-amenities"><i class="zmdi zmdi-more"></i></a>
                                                                     <ul id="moreAmenities1" class="dropdown-content custom_dropdown">
                                                                        <div class="amenities-list">
                                                                           <ul class="mb0">
                                                                              <li><i class="mdi mdi-wifi"></i>Free Internet Access</li>
                                                                              <li><i class="mdi mdi-wifi"></i>Free Internet Access</li>
                                                                              <li><i class="mdi mdi-wifi"></i>Free Internet Access</li>
                                                                              <li><i class="mdi mdi-wifi"></i>Free Internet Access</li>
                                                                              <li><i class="mdi mdi-wifi"></i>Free Internet Access</li>
                                                                              <li><i class="mdi mdi-wifi"></i>Free Internet Access</li>
                                                                           </ul>
                                                                        </div>
                                                                     </ul>
                                                                  </li>
                                                               </ul>
                                                            </div>
                                                            <div class="hotel-rates">
                                                               <a href="">
                                                                  <span class="h-name">Hotels.com</span>
                                                                  <span class="h-price">PKR 29,510</span>
                                                               </a>
                                                               <a href="">
                                                                  <span class="h-name">Booking.com</span>
                                                                  <span class="h-price">PKR 29,510</span>
                                                               </a>
                                                               <a href="javascript:void(0)" data-activates="moreHotelRates1" class="dropdown-button more-h-rates"><i class="mdi mdi-chevron-down"></i></a>
                                                               <ul id="moreHotelRates1" class="dropdown-content custom_dropdown">
                                                                  <li>
                                                                     <a href="">
                                                                        <span class="price">USD 257 </span>
                                                                        <span class="provider-name">Hotels.com</span>
                                                                     </a>
                                                                  </li>
                                                                  <li>
                                                                     <a href="">
                                                                        <span class="price">USD 245 </span>
                                                                        <span class="provider-name">Preffered Hotel</span>
                                                                     </a>
                                                                  </li>
                                                               </ul>
                                                            </div>
                                                            <!-- <span class="address">Dubai, Dubai(Emirates), United Arab Emirates</span>
                                                            <span class="distance-info">2.2 miles to City center</span> -->
                                                            <!-- <span class="moredeals-link">More Info</span> -->
                                                         </a>
                                                         <div class="info-action">
                                                            <span class="stars-holder">
                                                            <i class="mdi mdi-star active"></i>
                                                            <i class="mdi mdi-star active"></i>
                                                            <i class="mdi mdi-star active"></i>
                                                            <i class="mdi mdi-star active"></i>
                                                            <i class="mdi mdi-star active"></i>
                                                            </span>
                                                            <div class="clear"></div>
                                                            <span class="sitename">booking.com</span>
                                                            <div class="clear"></div>
                                                            <span class="price">JOD 184*</span>
                                                            <div class="clear"></div>
                                                            <a href="javascript:void(0)" class="deal-btn waves-effect">Book Now <i class="mdi mdi-chevron-right"></i></a>
                                                            <div class="hotels-merchandise">
                                                               <span><i class="mdi mdi-check"></i>Free cancellation</span>
                                                               <span><i class="mdi mdi-check"></i>Reserve now, pay at stay</span>
                                                            </div>
                                                            <div class="hotel-website">
                                                               <a href="javascript:void(0)">
                                                                  <i class="zmdi zmdi-globe-alt"></i>
                                                                  <span>Visit hotel website
                                                                  <i class="zmdi zmdi-arrow-right-top"></i></span>
                                                               </a>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div class="expandable-area">
                                                      <a href="javascript:void(0)" class="shrink-link" onclick="mng_expandable(this,'closeIt'),setHideHeader(this,'hotels','show')"><i class="mdi mdi-close"></i> Close</a>
                                                      <div class="clear"></div>
                                                      <div class="explandable-tabs">
                                                         <ul class="tabs subtab-menu">
                                                            <li class="tab"><a class="active" href="#subtab-details">Details</a></li>
                                                            <li class="tab"><a href="#subtab-reviews">Reviews</a></li>
                                                            <li class="tab"><a data-which="photo" href="#subtab-photos" data-tab="subtab-photos">Photos</a></li>
                                                            <li class="tab lst-li"><a href="#subtab-amenities">Amenities</a></li>
                                                         </ul>
                                                         <div class="tab-content">
                                                            <div id="subtab-details" class=" animated fadeInUp">
                                                               <div class="subdetail-box">
                                                                  <div class="infoholder">
                                                                     <div class="descholder">
                                                                        <div class="more-holder">
                                                                           <ul class="infoul">
                                                                              <li>
                                                                                 <i class="zmdi zmdi-pin"></i>
                                                                                 132 Brick Lane | E1 6RU, <?=$st_nm_S?> E1 6RU, <?=$st_nm_S?>
                                                                              </li>
                                                                              <li>
                                                                                 <i class="mdi mdi-phone"></i>
                                                                                 +44 20 7247 8210
                                                                              </li>
                                                                              <li>
                                                                                 <i class="mdi mdi-earth"></i>
                                                                                 http://www.yourwebsite.com
                                                                              </li>
                                                                              <li>
                                                                                 <i class="mdi mdi-clock-outline"></i>
                                                                                 Mon-Fri : 12:00 PM - 10:00 AM
                                                                              </li>
                                                                              <li>
                                                                                 <i class="mdi mdi-certificate "></i>
                                                                                 Ranked #1 in <?=$st_nm_S?> Hotels
                                                                              </li>
                                                                           </ul>
                                                                           <div class="tagging" onclick="explandTags(this)">
                                                                              Popular with:
                                                                              <span>point of interest</span>
                                                                              <span>establishment</span>
                                                                           </div>
                                                                        </div>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                            <div id="subtab-reviews" class=" animated fadeInUp">
                                                               <div class="reviews-summery">
                                                                  <div class="reviews-people">
                                                                     <ul>
                                                                        <li>
                                                                           <div class="reviewpeople-box">
                                                                              <div class="imgholder"><img src="images/people-3.png" /></div>
                                                                              <div class="descholder">
                                                                                 <h6>Kelly Mark <span>about 2 weeks ago</span></h6>
                                                                                 <div class="stars-holder">
                                                                                    <img src="images/filled-star.png" />
                                                                                    <img src="images/filled-star.png" />
                                                                                    <img src="images/filled-star.png" />
                                                                                    <img src="images/blank-star.png" />
                                                                                    <img src="images/blank-star.png" />
                                                                                 </div>
                                                                                 <div class="clear"></div>
                                                                                 <p>We enjoyed the lounge and bar at the Ritz where you are offered many choices for drinks and some pretty elaborate looking dishes of food as well.</p>
                                                                              </div>
                                                                           </div>
                                                                        </li>
                                                                        <li>
                                                                           <div class="reviewpeople-box">
                                                                              <div class="imgholder"><img src="images/people-2.png" /></div>
                                                                              <div class="descholder">
                                                                                 <h6>John Davior <span>about 8 months ago</span></h6>
                                                                                 <div class="stars-holder">
                                                                                    <img src="images/filled-star.png" />
                                                                                    <img src="images/filled-star.png" />
                                                                                    <img src="images/filled-star.png" />
                                                                                    <img src="images/filled-star.png" />
                                                                                    <img src="images/blank-star.png" />
                                                                                 </div>
                                                                                 <div class="clear"></div>
                                                                                 <p>If you want a fancy London experience than The Ritz is where you need to go! At least budget for High Tea!</p>
                                                                              </div>
                                                                           </div>
                                                                        </li>
                                                                        <li>
                                                                           <div class="reviewpeople-box">
                                                                              <div class="imgholder"><img src="images/people-1.png" /></div>
                                                                              <div class="descholder">
                                                                                 <h6>Joe Doe <span>about 11 months ago</span></h6>
                                                                                 <div class="stars-holder">
                                                                                    <img src="images/filled-star.png" />
                                                                                    <img src="images/filled-star.png" />
                                                                                    <img src="images/filled-star.png" />
                                                                                    <img src="images/blank-star.png" />
                                                                                    <img src="images/blank-star.png" />
                                                                                 </div>
                                                                                 <div class="clear"></div>
                                                                                 <p>I am not at all sure this is the best hotel in London, but it does deserve the reputation as one of the most glamourous.</p>
                                                                              </div>
                                                                           </div>
                                                                        </li>
                                                                     </ul>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                            <div id="subtab-photos" class="subtab-photos animated fadeInUp">
                                                               <div class="photo-gallery">
                                                                  <div class="img-preview">
                                                                     <img src="images/post-img1.jpg" />
                                                                  </div>
                                                                  <div class="thumbs-img">
                                                                     <ul>
                                                                        <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img class="himg" src="images/post-img1.jpg" /></a></li>
                                                                        <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img src="images/post-img2.jpg" class="himg" /></a></li>
                                                                        <li><a href="javascript:void(0)" onclick="previewImage(this)" class="vimg-box"><img src="images/post-img3.jpg" class="vimg" /></a></li>
                                                                        <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img src="images/post-img4.jpg" class="himg" /></a></li>
                                                                        <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img src="images/post-img5.jpg" class="himg" /></a></li>
                                                                        <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img class="himg" src="images/post-img1.jpg" /></a></li>
                                                                        <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img src="images/post-img2.jpg" class="himg" /></a></li>
                                                                        <li><a href="javascript:void(0)" onclick="previewImage(this)" class="vimg-box"><img src="images/post-img3.jpg" class="vimg" /></a></li>
                                                                        <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img src="images/post-img4.jpg" class="himg" /></a></li>
                                                                        <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img src="images/post-img5.jpg" class="himg" /></a></li>
                                                                     </ul>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                            <div id="subtab-amenities" class=" animated fadeInUp">
                                                               <ul class="ul-amenities tab-amenities">
                                                                  <li><a href="javascript:void(0)"><img src="images/amenity-spa.png"><span>Spa</span></a></li>
                                                                  <li><a href="javascript:void(0)"><img src="images/amenity-beach.png"><span>Beach</span></a></li>
                                                                  <li><a href="javascript:void(0)"><img src="images/amenity-wifi.png"><span>Wifi</span></a></li>
                                                                  <li><a href="javascript:void(0)"><img src="images/amenity-breakfast.png"><span>Breakfast</span></a></li>
                                                                  <li><a href="javascript:void(0)"><img src="images/amenity-pool.png"><span>Pool</span></a></li>
                                                                  <li><a href="javascript:void(0)"><img src="images/amenity-spa.png"><span>Spa</span></a></li>
                                                                  <li><a href="javascript:void(0)"><img src="images/amenity-beach.png"><span>Beach</span></a></li>
                                                                  <li><a href="javascript:void(0)"><img src="images/amenity-breakfast.png"><span>Breakfast</span></a></li>
                                                               </ul>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </li>
                                             <li>
                                                <div class="hotel-li expandable-holder dealli mobilelist">
                                                   <div class="summery-info">
                                                      <div class="imgholder">
                                                         <div class="owl-carousel owl-theme owl-hotels">
                                                            <div class="item"><img src="images/hotel-demo.png" alt="The Last of us"></div>
                                                            <div class="item"><img src="images/hotel-demo.png" alt="GTA V"></div>
                                                            <div class="item"><img src="images/hotel-demo.png" alt="Mirror Edge"></div>
                                                         </div>
                                                      </div>
                                                      <div class="descholder">
                                                         <a href="javascript:void(0)" class="expand-link" onclick="mng_expandable(this,'hasClose'),setHideHeader(this,'hotels','hide')">
                                                            <h4>Hyatt Regency Dubai Creek
                                                            </h4>
                                                            <p class="hotel-location"><?=$st_nm_S?></p>
                                                            <div class="clear"></div>
                                                            <div class="reviews-link">
                                                               <span class="review-count">54 reviews</span>
                                                               <span class="checks-holder">
                                                               <i class="mdi mdi-radiobox-marked active"></i>
                                                               <i class="mdi mdi-radiobox-marked active"></i>
                                                               <i class="mdi mdi-radiobox-marked active"></i>
                                                               <i class="mdi mdi-radiobox-marked active"></i>
                                                               <i class="mdi mdi-radiobox-marked active"></i>
                                                               <label>Excellent - 88/100</label>
                                                               </span>
                                                            </div>
                                                            <div class="hotel-amenities">
                                                               <ul class="ul-amenities mb0">
                                                                  <li>
                                                                     <a href="javascript:void(0)"><img src="images/amenity-spa.png" /><span>Spa</span></a>
                                                                  </li>
                                                                  <li>
                                                                     <a href="javascript:void(0)"><img src="images/amenity-beach.png" /><span>Beach</span></a>
                                                                  </li>
                                                                  <li>
                                                                     <a href="javascript:void(0)"><img src="images/amenity-wifi.png" /><span>Wifi</span></a>
                                                                  </li>
                                                                  <li>
                                                                     <a href="javascript:void(0)" data-activates="moreAmenities2" class="dropdown-button more-amenities"><i class="zmdi zmdi-more"></i></a>
                                                                     <ul id="moreAmenities2" class="dropdown-content custom_dropdown">
                                                                        <div class="amenities-list">
                                                                           <ul class="mb0">
                                                                              <li><i class="mdi mdi-wifi"></i>Free Internet Access</li>
                                                                              <li><i class="mdi mdi-wifi"></i>Free Internet Access</li>
                                                                              <li><i class="mdi mdi-wifi"></i>Free Internet Access</li>
                                                                              <li><i class="mdi mdi-wifi"></i>Free Internet Access</li>
                                                                              <li><i class="mdi mdi-wifi"></i>Free Internet Access</li>
                                                                              <li><i class="mdi mdi-wifi"></i>Free Internet Access</li>
                                                                           </ul>
                                                                        </div>
                                                                    </ul>
                                                                  </li>
                                                               </ul>
                                                            </div>
                                                            <div class="hotel-rates">
                                                               <a href="">
                                                                  <span class="h-name">Hotels.com</span>
                                                                  <span class="h-price">PKR 29,510</span>
                                                               </a>
                                                               <a href="">
                                                                  <span class="h-name">Booking.com</span>
                                                                  <span class="h-price">PKR 29,510</span>
                                                               </a>
                                                               <a href="javascript:void(0)" data-activates="moreHotelRates2" class="dropdown-button more-h-rates"><i class="mdi mdi-chevron-down"></i></a>
                                                               <ul id="moreHotelRates2" class="dropdown-content custom_dropdown">
                                                                  <li>
                                                                     <a href="">
                                                                        <span class="price">USD 257 </span>
                                                                        <span class="provider-name">Hotels.com</span>
                                                                     </a>
                                                                  </li>
                                                                  <li>
                                                                     <a href="">
                                                                        <span class="price">USD 245 </span>
                                                                        <span class="provider-name">Preffered Hotel</span>
                                                                     </a>
                                                                  </li>
                                                               </ul>
                                                            </div>
                                                            <!-- <span class="address">Dubai, Dubai(Emirates), United Arab Emirates</span>
                                                            <span class="distance-info">2.2 miles to City center</span> -->
                                                            <!-- <span class="moredeals-link">More Info</span> -->
                                                         </a>
                                                         <div class="info-action">
                                                            <span class="stars-holder">
                                                            <i class="mdi mdi-star active"></i>
                                                            <i class="mdi mdi-star active"></i>
                                                            <i class="mdi mdi-star active"></i>
                                                            <i class="mdi mdi-star active"></i>
                                                            <i class="mdi mdi-star active"></i>
                                                            </span>
                                                            <div class="clear"></div>
                                                            <span class="sitename">booking.com</span>
                                                            <div class="clear"></div>
                                                            <span class="price">JOD 184*</span>
                                                            <div class="clear"></div>
                                                            <a href="javascript:void(0)" class="deal-btn waves-effect">Book Now <i class="mdi mdi-chevron-right"></i></a>
                                                            <div class="hotels-merchandise">
                                                               <span><i class="mdi mdi-check"></i>Free cancellation</span>
                                                               <span><i class="mdi mdi-check"></i>Reserve now, pay at stay</span>
                                                            </div>
                                                            <div class="hotel-website">
                                                               <a href="javascript:void(0)">
                                                                  <i class="zmdi zmdi-globe-alt"></i>
                                                                  <span>Visit hotel website
                                                                  <i class="zmdi zmdi-arrow-right-top"></i></span>
                                                               </a>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div class="expandable-area">
                                                      <a href="javascript:void(0)" class="shrink-link" onclick="mng_expandable(this,'closeIt'),setHideHeader(this,'hotels','show')"><i class="mdi mdi-close"></i> Close</a>
                                                      <div class="clear"></div>
                                                      <div class="explandable-tabs">
                                                         <ul class="tabs subtab-menu">
                                                            <li class="tab"><a class="active" href="#subtab-details-first">Details</a></li>
                                                            <li class="tab"><a href="#subtab-reviews-first">Reviews</a></li>
                                                            <li class="tab"><a data-which="photo" href="#subtab-photos-first" data-tab="subtab-photos">Photos</a></li>
                                                            <li class="tab lst-li"><a href="#subtab-amenities-first">Amenities</a></li>
                                                         </ul>
                                                         <div class="tab-content">
                                                            <div id="subtab-details-first" class="tab-pane fade active in animated fadeInUp">
                                                               <div class="subdetail-box">
                                                                  <div class="infoholder">
                                                                     <div class="descholder">
                                                                        <div class="more-holder">
                                                                           <ul class="infoul">
                                                                              <li>
                                                                                 <i class="zmdi zmdi-pin"></i>
                                                                                 132 Brick Lane | E1 6RU, <?=$st_nm_S?> E1 6RU, <?=$st_nm_S?>
                                                                              </li>
                                                                              <li>
                                                                                 <i class="mdi mdi-phone"></i>
                                                                                 +44 20 7247 8210
                                                                              </li>
                                                                              <li>
                                                                                 <i class="mdi mdi-earth"></i>
                                                                                 http://www.yourwebsite.com
                                                                              </li>
                                                                              <li>
                                                                                 <i class="mdi mdi-clock-outline"></i>
                                                                                 Mon-Fri : 12:00 PM - 10:00 AM
                                                                              </li>
                                                                              <li>
                                                                                 <i class="mdi mdi-certificate "></i>
                                                                                 Ranked #1 in <?=$st_nm_S?> Hotels
                                                                              </li>
                                                                           </ul>
                                                                           <div class="tagging" onclick="explandTags(this)">
                                                                              Popular with:
                                                                              <span>point of interest</span>
                                                                              <span>establishment</span>
                                                                           </div>
                                                                        </div>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                            <div id="subtab-reviews-first" class="tab-pane fade animated fadeInUp">
                                                               <div class="reviews-summery">
                                                                  <div class="reviews-people">
                                                                     <ul>
                                                                        <li>
                                                                           <div class="reviewpeople-box">
                                                                              <div class="imgholder"><img src="images/people-3.png" /></div>
                                                                              <div class="descholder">
                                                                                 <h6>Kelly Mark <span>about 2 weeks ago</span></h6>
                                                                                 <div class="stars-holder">
                                                                                    <img src="images/filled-star.png" />
                                                                                    <img src="images/filled-star.png" />
                                                                                    <img src="images/filled-star.png" />
                                                                                    <img src="images/blank-star.png" />
                                                                                    <img src="images/blank-star.png" />
                                                                                 </div>
                                                                                 <div class="clear"></div>
                                                                                 <p>We enjoyed the lounge and bar at the Ritz where you are offered many choices for drinks and some pretty elaborate looking dishes of food as well.</p>
                                                                              </div>
                                                                           </div>
                                                                        </li>
                                                                        <li>
                                                                           <div class="reviewpeople-box">
                                                                              <div class="imgholder"><img src="images/people-2.png" /></div>
                                                                              <div class="descholder">
                                                                                 <h6>John Davior <span>about 8 months ago</span></h6>
                                                                                 <div class="stars-holder">
                                                                                    <img src="images/filled-star.png" />
                                                                                    <img src="images/filled-star.png" />
                                                                                    <img src="images/filled-star.png" />
                                                                                    <img src="images/filled-star.png" />
                                                                                    <img src="images/blank-star.png" />
                                                                                 </div>
                                                                                 <div class="clear"></div>
                                                                                 <p>If you want a fancy London experience than The Ritz is where you need to go! At least budget for High Tea!</p>
                                                                              </div>
                                                                           </div>
                                                                        </li>
                                                                        <li>
                                                                           <div class="reviewpeople-box">
                                                                              <div class="imgholder"><img src="images/people-1.png" /></div>
                                                                              <div class="descholder">
                                                                                 <h6>Joe Doe <span>about 11 months ago</span></h6>
                                                                                 <div class="stars-holder">
                                                                                    <img src="images/filled-star.png" />
                                                                                    <img src="images/filled-star.png" />
                                                                                    <img src="images/filled-star.png" />
                                                                                    <img src="images/blank-star.png" />
                                                                                    <img src="images/blank-star.png" />
                                                                                 </div>
                                                                                 <div class="clear"></div>
                                                                                 <p>I am not at all sure this is the best hotel in London, but it does deserve the reputation as one of the most glamourous.</p>
                                                                              </div>
                                                                           </div>
                                                                        </li>
                                                                     </ul>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                            <div id="subtab-photos-first" class="tab-pane fade subtab-photos animated fadeInUp">
                                                               <div class="photo-gallery">
                                                                  <div class="img-preview">
                                                                     <img src="images/post-img1.jpg" />
                                                                  </div>
                                                                  <div class="thumbs-img">
                                                                     <ul>
                                                                        <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img class="himg" src="images/post-img1.jpg" /></a></li>
                                                                        <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img src="images/post-img2.jpg" class="himg" /></a></li>
                                                                        <li><a href="javascript:void(0)" onclick="previewImage(this)" class="vimg-box"><img src="images/post-img3.jpg" class="vimg" /></a></li>
                                                                        <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img src="images/post-img4.jpg" class="himg" /></a></li>
                                                                        <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img src="images/post-img5.jpg" class="himg" /></a></li>
                                                                        <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img class="himg" src="images/post-img1.jpg" /></a></li>
                                                                        <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img src="images/post-img2.jpg" class="himg" /></a></li>
                                                                        <li><a href="javascript:void(0)" onclick="previewImage(this)" class="vimg-box"><img src="images/post-img3.jpg" class="vimg" /></a></li>
                                                                        <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img src="images/post-img4.jpg" class="himg" /></a></li>
                                                                        <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img src="images/post-img5.jpg" class="himg" /></a></li>
                                                                     </ul>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                            <div id="subtab-amenities-first" class="tab-pane fade animated fadeInUp">
                                                               <ul class="ul-amenities tab-amenities">
                                                                  <li><a href="javascript:void(0)"><img src="images/amenity-spa.png"><span>Spa</span></a></li>
                                                                  <li><a href="javascript:void(0)"><img src="images/amenity-beach.png"><span>Beach</span></a></li>
                                                                  <li><a href="javascript:void(0)"><img src="images/amenity-wifi.png"><span>Wifi</span></a></li>
                                                                  <li><a href="javascript:void(0)"><img src="images/amenity-breakfast.png"><span>Breakfast</span></a></li>
                                                                  <li><a href="javascript:void(0)"><img src="images/amenity-pool.png"><span>Pool</span></a></li>
                                                                  <li><a href="javascript:void(0)"><img src="images/amenity-spa.png"><span>Spa</span></a></li>
                                                                  <li><a href="javascript:void(0)"><img src="images/amenity-beach.png"><span>Beach</span></a></li>
                                                                  <li><a href="javascript:void(0)"><img src="images/amenity-breakfast.png"><span>Breakfast</span></a></li>
                                                               </ul>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </li>
                                             <li>
                                                <div class="hotel-li expandable-holder dealli mobilelist">
                                                   <div class="summery-info">
                                                      <div class="imgholder">
                                                         <div class="owl-carousel owl-theme owl-hotels">
                                                            <div class="item"><img src="images/hotel-demo.png" alt="The Last of us"></div>
                                                            <div class="item"><img src="images/hotel-demo.png" alt="GTA V"></div>
                                                            <div class="item"><img src="images/hotel-demo.png" alt="Mirror Edge"></div>
                                                         </div>
                                                      </div>
                                                      <div class="descholder">
                                                         <a href="javascript:void(0)" class="expand-link" onclick="mng_expandable(this,'hasClose'),setHideHeader(this,'hotels','hide')">
                                                            <h4>Hyatt Regency Dubai Creek</h4>
                                                            <p class="hotel-location"><?=$st_nm_S?></p>
                                                            <div class="clear"></div>
                                                            <div class="reviews-link">
                                                               <span class="review-count">54 reviews</span>
                                                               <span class="checks-holder">
                                                               <i class="mdi mdi-radiobox-marked active"></i>
                                                               <i class="mdi mdi-radiobox-marked active"></i>
                                                               <i class="mdi mdi-radiobox-marked active"></i>
                                                               <i class="mdi mdi-radiobox-marked active"></i>
                                                               <i class="mdi mdi-radiobox-marked active"></i>
                                                               <label>Excellent - 88/100</label>
                                                               </span>
                                                            </div>
                                                            <div class="hotel-amenities">
                                                               <ul class="ul-amenities mb0">
                                                                  <li>
                                                                     <a href="javascript:void(0)"><img src="images/amenity-spa.png" /><span>Spa</span></a>
                                                                  </li>
                                                                  <li>
                                                                     <a href="javascript:void(0)"><img src="images/amenity-beach.png" /><span>Beach</span></a>
                                                                  </li>
                                                                  <li>
                                                                     <a href="javascript:void(0)"><img src="images/amenity-wifi.png" /><span>Wifi</span></a>
                                                                  </li>
                                                                  <li>
                                                                     <a href="javascript:void(0)" data-activates="moreAmenities3" class="dropdown-button more-amenities"><i class="zmdi zmdi-more"></i></a>
                                                                     <ul id="moreAmenities3" class="dropdown-content custom_dropdown">
                                                                        <div class="amenities-list">
                                                                           <ul class="mb0">
                                                                              <li><i class="mdi mdi-wifi"></i>Free Internet Access</li>
                                                                              <li><i class="mdi mdi-wifi"></i>Free Internet Access</li>
                                                                              <li><i class="mdi mdi-wifi"></i>Free Internet Access</li>
                                                                              <li><i class="mdi mdi-wifi"></i>Free Internet Access</li>
                                                                              <li><i class="mdi mdi-wifi"></i>Free Internet Access</li>
                                                                              <li><i class="mdi mdi-wifi"></i>Free Internet Access</li>
                                                                           </ul>
                                                                        </div>
                                                                    </ul>
                                                                  </li>
                                                               </ul>
                                                            </div>
                                                            <div class="hotel-rates">
                                                               <a href="">
                                                                  <span class="h-name">Hotels.com</span>
                                                                  <span class="h-price">PKR 29,510</span>
                                                               </a>
                                                               <a href="">
                                                                  <span class="h-name">Booking.com</span>
                                                                  <span class="h-price">PKR 29,510</span>
                                                               </a>
                                                               <a href="javascript:void(0)" data-activates="moreHotelRates3" class="dropdown-button more-h-rates"><i class="mdi mdi-chevron-down"></i></a>
                                                               <ul id="moreHotelRates3" class="dropdown-content custom_dropdown">
                                                                  <li>
                                                                     <a href="">
                                                                        <span class="price">USD 257 </span>
                                                                        <span class="provider-name">Hotels.com</span>
                                                                     </a>
                                                                  </li>
                                                                  <li>
                                                                     <a href="">
                                                                        <span class="price">USD 245 </span>
                                                                        <span class="provider-name">Preffered Hotel</span>
                                                                     </a>
                                                                  </li>
                                                               </ul>
                                                            </div>
                                                            <!-- <span class="address">Dubai, Dubai(Emirates), United Arab Emirates</span>
                                                            <span class="distance-info">2.2 miles to City center</span> -->
                                                            <!-- <span class="moredeals-link">More Info</span> -->
                                                         </a>
                                                         <div class="info-action">
                                                            <span class="stars-holder">
                                                            <i class="mdi mdi-star active"></i>
                                                            <i class="mdi mdi-star active"></i>
                                                            <i class="mdi mdi-star active"></i>
                                                            <i class="mdi mdi-star active"></i>
                                                            <i class="mdi mdi-star active"></i>
                                                            </span>
                                                            <div class="clear"></div>
                                                            <span class="sitename">booking.com</span>
                                                            <div class="clear"></div>
                                                            <span class="price">JOD 184*</span>
                                                            <div class="clear"></div>
                                                            <a href="javascript:void(0)" class="deal-btn waves-effect">Book Now <i class="mdi mdi-chevron-right"></i></a>
                                                            <div class="hotels-merchandise">
                                                               <span><i class="mdi mdi-check"></i>Free cancellation</span>
                                                               <span><i class="mdi mdi-check"></i>Reserve now, pay at stay</span>
                                                            </div>
                                                            <div class="hotel-website">
                                                               <a href="javascript:void(0)">
                                                                  <i class="zmdi zmdi-globe-alt"></i>
                                                                  <span>Visit hotel website
                                                                  <i class="zmdi zmdi-arrow-right-top"></i></span>
                                                               </a>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div class="expandable-area">
                                                      <a href="javascript:void(0)" class="shrink-link" onclick="mng_expandable(this,'closeIt'),setHideHeader(this,'hotels','show')"><i class="mdi mdi-close"></i> Close</a>
                                                      <div class="clear"></div>
                                                      <div class="explandable-tabs">
                                                         <ul class="tabs subtab-menu">
                                                            <li class="tab"><a class="active" href="#subtab-details-second">Details</a></li>
                                                            <li class="tab"><a href="#subtab-reviews-second">Reviews</a></li>
                                                            <li class="tab"><a data-which="photo" href="#subtab-photos-second" data-tab="subtab-photos">Photos</a></li>
                                                            <li class="tab lst-li"><a href="#subtab-amenities-second">Amenities</a></li>
                                                         </ul>
                                                         <div class="tab-content">
                                                            <div id="subtab-details-second" class="tab-pane fade active in animated fadeInUp">
                                                               <div class="subdetail-box">
                                                                  <div class="infoholder">
                                                                     <div class="descholder">
                                                                        <div class="more-holder">
                                                                           <ul class="infoul">
                                                                              <li>
                                                                                 <i class="zmdi zmdi-pin"></i>
                                                                                 132 Brick Lane | E1 6RU, <?=$st_nm_S?> E1 6RU, <?=$st_nm_S?>
                                                                              </li>
                                                                              <li>
                                                                                 <i class="mdi mdi-phone"></i>
                                                                                 +44 20 7247 8210
                                                                              </li>
                                                                              <li>
                                                                                 <i class="mdi mdi-earth"></i>
                                                                                 http://www.yourwebsite.com
                                                                              </li>
                                                                              <li>
                                                                                 <i class="mdi mdi-clock-outline"></i>
                                                                                 Mon-Fri : 12:00 PM - 10:00 AM
                                                                              </li>
                                                                              <li>
                                                                                 <i class="mdi mdi-certificate "></i>
                                                                                 Ranked #1 in <?=$st_nm_S?> Hotels
                                                                              </li>
                                                                           </ul>
                                                                           <div class="tagging" onclick="explandTags(this)">
                                                                              Popular with:
                                                                              <span>point of interest</span>
                                                                              <span>establishment</span>
                                                                           </div>
                                                                        </div>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                            <div id="subtab-reviews-second" class="tab-pane fade animated fadeInUp">
                                                               <div class="reviews-summery">
                                                                  <div class="reviews-people">
                                                                     <ul>
                                                                        <li>
                                                                           <div class="reviewpeople-box">
                                                                              <div class="imgholder"><img src="images/people-3.png" /></div>
                                                                              <div class="descholder">
                                                                                 <h6>Kelly Mark <span>about 2 weeks ago</span></h6>
                                                                                 <div class="stars-holder">
                                                                                    <img src="images/filled-star.png" />
                                                                                    <img src="images/filled-star.png" />
                                                                                    <img src="images/filled-star.png" />
                                                                                    <img src="images/blank-star.png" />
                                                                                    <img src="images/blank-star.png" />
                                                                                 </div>
                                                                                 <div class="clear"></div>
                                                                                 <p>We enjoyed the lounge and bar at the Ritz where you are offered many choices for drinks and some pretty elaborate looking dishes of food as well.</p>
                                                                              </div>
                                                                           </div>
                                                                        </li>
                                                                        <li>
                                                                           <div class="reviewpeople-box">
                                                                              <div class="imgholder"><img src="images/people-2.png" /></div>
                                                                              <div class="descholder">
                                                                                 <h6>John Davior <span>about 8 months ago</span></h6>
                                                                                 <div class="stars-holder">
                                                                                    <img src="images/filled-star.png" />
                                                                                    <img src="images/filled-star.png" />
                                                                                    <img src="images/filled-star.png" />
                                                                                    <img src="images/filled-star.png" />
                                                                                    <img src="images/blank-star.png" />
                                                                                 </div>
                                                                                 <div class="clear"></div>
                                                                                 <p>If you want a fancy London experience than The Ritz is where you need to go! At least budget for High Tea!</p>
                                                                              </div>
                                                                           </div>
                                                                        </li>
                                                                        <li>
                                                                           <div class="reviewpeople-box">
                                                                              <div class="imgholder"><img src="images/people-1.png" /></div>
                                                                              <div class="descholder">
                                                                                 <h6>Joe Doe <span>about 11 months ago</span></h6>
                                                                                 <div class="stars-holder">
                                                                                    <img src="images/filled-star.png" />
                                                                                    <img src="images/filled-star.png" />
                                                                                    <img src="images/filled-star.png" />
                                                                                    <img src="images/blank-star.png" />
                                                                                    <img src="images/blank-star.png" />
                                                                                 </div>
                                                                                 <div class="clear"></div>
                                                                                 <p>I am not at all sure this is the best hotel in London, but it does deserve the reputation as one of the most glamourous.</p>
                                                                              </div>
                                                                           </div>
                                                                        </li>
                                                                     </ul>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                            <div id="subtab-photos-second" class="tab-pane fade subtab-photos animated fadeInUp">
                                                               <div class="photo-gallery">
                                                                  <div class="img-preview">
                                                                     <img src="images/post-img1.jpg" />
                                                                  </div>
                                                                  <div class="thumbs-img">
                                                                     <ul>
                                                                        <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img class="himg" src="images/post-img1.jpg" /></a></li>
                                                                        <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img src="images/post-img2.jpg" class="himg" /></a></li>
                                                                        <li><a href="javascript:void(0)" onclick="previewImage(this)" class="vimg-box"><img src="images/post-img3.jpg" class="vimg" /></a></li>
                                                                        <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img src="images/post-img4.jpg" class="himg" /></a></li>
                                                                        <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img src="images/post-img5.jpg" class="himg" /></a></li>
                                                                        <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img class="himg" src="images/post-img1.jpg" /></a></li>
                                                                        <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img src="images/post-img2.jpg" class="himg" /></a></li>
                                                                        <li><a href="javascript:void(0)" onclick="previewImage(this)" class="vimg-box"><img src="images/post-img3.jpg" class="vimg" /></a></li>
                                                                        <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img src="images/post-img4.jpg" class="himg" /></a></li>
                                                                        <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img src="images/post-img5.jpg" class="himg" /></a></li>
                                                                     </ul>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                            <div id="subtab-amenities-second" class="tab-pane fade animated fadeInUp">
                                                               <ul class="ul-amenities tab-amenities">
                                                                  <li><a href="javascript:void(0)"><img src="images/amenity-spa.png"><span>Spa</span></a></li>
                                                                  <li><a href="javascript:void(0)"><img src="images/amenity-beach.png"><span>Beach</span></a></li>
                                                                  <li><a href="javascript:void(0)"><img src="images/amenity-wifi.png"><span>Wifi</span></a></li>
                                                                  <li><a href="javascript:void(0)"><img src="images/amenity-breakfast.png"><span>Breakfast</span></a></li>
                                                                  <li><a href="javascript:void(0)"><img src="images/amenity-pool.png"><span>Pool</span></a></li>
                                                                  <li><a href="javascript:void(0)"><img src="images/amenity-spa.png"><span>Spa</span></a></li>
                                                                  <li><a href="javascript:void(0)"><img src="images/amenity-beach.png"><span>Beach</span></a></li>
                                                                  <li><a href="javascript:void(0)"><img src="images/amenity-breakfast.png"><span>Breakfast</span></a></li>
                                                               </ul>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </li>
                                          </ul>
                                          <div class="pagination">
                                             <div class="link-holder">
                                                <a href="javascript:void(0)"><i class="mdi mdi-arrow-left-bold-circle"></i> Prev</a>
                                             </div>
                                             <div class="link-holder">
                                                <a href="javascript:void(0)">Next <i class="mdi mdi-arrow-right-bold-circle"></i></a>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="moreinfo-box">
                                    <div class="fake-header">
                                       <!--<div class="page-name">Back to list</div>-->
                                       <div class="page-name"><a href="javascript:void(0)" onclick="closePlacesMoreInfo(this),setHideHeader(this,'hotels','show')"><i class="mdi mdi-arrow-left"></i>Back to list</a></div>
                                    </div>
                                    <div class="infoholder nice-scroll">
                                       <div class="imgholder"><img src="images/hotel1.png" /></div>
                                       <div class="descholder">
                                          <h4>The Guest House</h4>
                                          <div class="clear"></div>
                                          <div class="reviews-link">
                                             <span class="checks-holder">
                                             <i class="mdi mdi-star active"></i>
                                             <i class="mdi mdi-star active"></i>
                                             <i class="mdi mdi-star active"></i>
                                             <i class="mdi mdi-star active"></i>
                                             <i class="mdi mdi-star"></i>
                                             <label>34 Reviews</label>
                                             </span>
                                          </div>
                                          <span class="distance-info">Middle Eastem &amp; African, Mediterranean</span>
                                          <div class="clear"></div>
                                          <div class="more-holder">
                                             <ul class="infoul">
                                                <li>
                                                   <i class="zmdi zmdi-pin"></i>
                                                   132 Brick Lane | E1 6RU, <?=$st_nm_S?> E1 6RU, <?=$st_nm_S?>
                                                </li>
                                                <li>
                                                   <i class="mdi mdi-phone"></i>
                                                   +44 20 7247 8210
                                                </li>
                                                <li>
                                                   <i class="mdi mdi-earth"></i>
                                                   http://www.yourwebsite.com
                                                </li>
                                                <li>
                                                   <i class="mdi mdi-clock-outline"></i>
                                                   Today, 12:00 PM - 12:00 AM
                                                </li>
                                                <li>
                                                   <i class="mdi mdi-certificate "></i>
                                                   Ranked #1 in <?=$st_nm_S?> Hotels
                                                </li>
                                             </ul>
                                             <div class="tagging" onclick="explandTags(this)">
                                                Popular with:
                                                <span>Budget</span>
                                                <span>Foodies</span>
                                                <span>Family</span>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<?php include("common/footer.php"); ?>
</div>

<?php include('common/datepicker.php'); ?>

<?php include("script.php"); ?>
<script type="text/javascript">
   $(".owl-hotels").owlCarousel({
 
      navigation : true, // Show next and prev buttons
      slideSpeed : 300,
      paginationSpeed : 400,
      singleItem:true,
      loop: true,
      dots: false,
      nav: true,
      navText:["<i class='mdi mdi-chevron-left'></i>","<i class='mdi mdi-chevron-right'></i>"],
      //"singleItem:true" is a shortcut for:
      items : 1, 
      itemsDesktop : false,
      itemsDesktopSmall : false,
      itemsTablet: false,
      itemsMobile : false
 
   });

</script>

</body>
</html>
