<?php include("header.php"); ?>
<?php include("common/menu.php"); ?>
<div class="floating-icon">
   <div class="scrollup-btnbox anim-side btnbox scrollup-float">
      <div class="scrollup-button float-icon">
         <span class="icon-holder ispan">
            <i class="mdi mdi-arrow-up-bold-circle"></i>
         </span>
      </div>
   </div>
</div>
<div class="clear"></div>
<div class="">
   <?php include("common/leftmenu.php"); ?>
   <div class="fixed-layout unsetmargin">
      <div class="collection-page event-detail-page pb-0 m-t-50">
         <div class="combined-column wide-open main-page full-page">
            <div class="width-100 m-top">
                  <div class="collection-gallery-wrapper">
                     <div class="collection-container">
                        <div class="row mx-0">
                           <div class="collection-gallery">
                              <div class="collection-card">
                                 <div class="collection-card-body">  
                                    <a href="">
                                       <div class="collection-card-inner">
                                          <div class="collection-card-left">
                                             <img role="presentation" class="" src="images/real-estate.jpg" alt="">
                                          </div> 
                                          <div class="collection-card-middle">
                                             <img role="presentation" class="" src="images/real-estate1.jpg" alt="">
                                          </div>
                                          <div class="collection-card-right">
                                             <div class="img-right-top">
                                                <img role="presentation" class="" src="images/real-estate2.jpg" alt="">
                                             </div>
                                          </div>  
                                       </div>
                                    </a>
                                 </div>
                              </div> 
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="event-info-wrapper">
                     <div class="container">
                        <div class="row mx-0">
                           <div class="col m8 s12">
                              <div class="event-title-container">
                                 <div class="row mx-0 valign-wrapper homestay-title">
                                    <h1 class="event-title">
                                       One Room for Homestay
                                       <span class="private-room"> 
                                          <i class="mdi mdi-home mdi-17px"></i> 
                                          Private room in house
                                       </span>
                                    </h1>
                                    <div class="homestay-edit right">
                                       <div class="right ml-auto">
                                          <a href="javascript:void(0)" class="homestayEditAction waves-effect waves-theme"><i class="mdi mdi-pencil mdi-20px"></i></a>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="detail-title-container">
                                 <h3 class="event-detail-title">Homestay Detail</h3>
                                 <div class="people-box">
                                    <div class="img-holder">
                                       <img src="images/people-2.png">
                                    </div>
                                    <div class="desc-holder">
                                       <a href="javascript:void(0)" class="userlink">Adel Ahasanat</a>
                                    </div>
                                 </div>
                              </div>
                              <div class="info-container">
                                 <p>I invite you to a unique meal with a view on the beautiful canals of Amsterdam. Wine climate cabinet, sous vide teppanyaki and grill cooking options. The menu is 100% plant-based, kosher, lactose-free and can also be made gluten-free upon request! All drinks are included.</p>
                              </div>
                              <div class="event-title-container row mx-0 border-none">
                                 <div class="event-icons">
                                    <h5 class="valign-wrapper">Welcomes</h5>
                                    <span class="valign-wrapper"><i class="mdi mdi-check mdi-20px"></i> Males</span>
                                    <span class="valign-wrapper"><i class="mdi mdi-check mdi-20px"></i> Females</span>
                                    <span class="valign-wrapper"><i class="mdi mdi-check mdi-20px"></i> Couples</span>
                                    <span class="valign-wrapper"><i class="mdi mdi-check mdi-20px"></i> Families</span>
                                    <span class="valign-wrapper"><i class="mdi mdi-check mdi-20px"></i> Students</span>
                                 </div>
                              </div>
                              <div class="info-list full-width-list">
                                 <ul>
                                    <li>
                                       <h5>House Rules</h5>
                                       <p>
                                          Self Check-in <br>
                                          Check-in: 2pm onwards, Check-out: 2pm <br>
                                          Shoe-free house (I provide comfy slippers during your stay)
                                       </p>
                                    </li>
                                    <li>
                                       <h5>House Facilities</h5>
                                       <div class="row facilities-row mx-0">
                                          <div class="col s3">
                                             <span class="valign-wrapper"><i class="mdi mdi-check"></i>Garden</span>
                                          </div>
                                          <div class="col s3">
                                             <span class="valign-wrapper"><i class="mdi mdi-check"></i>Laudry</span>
                                          </div>
                                          <div class="col s3">
                                             <span class="valign-wrapper"><i class="mdi mdi-check"></i>Parking</span>
                                          </div>
                                          <div class="col s3">
                                             <span class="valign-wrapper"><i class="mdi mdi-check"></i>TV</span>
                                          </div>
                                          <div class="col s3">
                                             <span class="valign-wrapper"><i class="mdi mdi-check"></i>WiFi</span>
                                          </div>
                                       </div>
                                    </li>
                                    <li class="services">
                                       <h5 class="mb-10">services</h5>
                                       <a href="javascript:void(0)"><img alt="spa" title="spa" src="images/amenity-spa.png"><span>Spa</span></a>
                                       <a href="javascript:void(0)"><img alt="beach" title="beach" src="images/amenity-beach.png"><span>Beach</span></a>
                                       <a href="javascript:void(0)"><img alt="wifi" title="wifi" src="images/amenity-wifi.png"><span>Wifi</span></a>
                                       <a href="javascript:void(0)"><img alt="breakfast" title="breakfast" src="images/amenity-breakfast.png"><span>Breakfast</span></a>
                                       <a href="javascript:void(0)"><img alt="pool" title="pool" src="images/amenity-pool.png"><span>Pool</span></a>
                                    </li>
                                    <li>
                                       <h5>Meals</h5>
                                       <p>Host can offer a complmentary light breakfast at their dicretion. All other meals will incure in additional cost. Meals and additional payment should be arrange directly with your host.</p>
                                    </li>
                                 </ul>
                              </div>
                              <div class="photo-section mt-20">
                                 <div class="row mx-0 valign-wrapper">
                                    <div class="left">
                                       <h5>PHOTOS</h5>
                                    </div>
                                    <div class="right ml-auto">
                                       <a href="">+ Upload</a>
                                    </div>
                                 </div>
                                 <div class="row mt-10">
                                    <div class="col s3"><img role="presentation" class="" src="images/wgallery3.jpg" alt=""></div>
                                    <div class="col s3"><img role="presentation" class="" src="images/wgallery3.jpg" alt=""></div>
                                    <div class="col s3"><img role="presentation" class="" src="images/wgallery3.jpg" alt=""></div>
                                    <div class="col s3"><img role="presentation" class="" src="images/wgallery3.jpg" alt=""></div>
                                 </div>
                              </div>
                           </div>
                           <div class="col m4 s12">
                              <div class="event-right-wrapper">
                                 <div class="booking-form-section">
                                    <div class="price-container">
                                       <span><span class="price"> $23 </span> per night</span>
                                    </div>
                                    <div class="ddl-select">
                                       <label>Date</label>
                                       <select class="select2" tabindex="-1" >
                                          <option>Saturday 06/01/2019</option>
                                          <option>Saturday 06/01/2019</option>
                                       </select>
                                    </div>
                                    <div class="ddl-select">
                                       <label>Number of guests</label>
                                       <select class="select2" tabindex="-1" >
                                          <option>1 guest</option>
                                          <option>2 guests</option>
                                       </select>
                                    </div>
                                    <div class="personal-message ddl-select">
                                       <label>Personal Message</label>
                                       <textarea class="materialize-textarea md_textarea item_tagline" placeholder="The space and facilities look wonderful! I will be in town for a few days and i m wondering if you could host me Thank you!"></textarea>
                                    </div>
                                    <div class="btn-sec">
                                       <a class="waves-effect waves-light btn" href="javascript:void(0)">Message to Host</a>
                                    </div>
                                 </div>
                                 <div class="contact-host valign-wrapper">
                                    <span><i class="mdi mdi-comment-outline"></i> Questions? </span>
                                    <a href="">Contact the host</a>
                                    <span class="right ml-auto"><i class="mdi mdi-chevron-right mdi-17px"></i></span>
                                 </div>
                                 <div class="save-wishlist">
                                    <p class="text-center m-0">
                                       <span class="icon-heart"><i class="mdi mdi-heart mdi-20px"></i></span>
                                       <a href="">Save to your wishlist</a>
                                    </p>
                                 </div>
                                 <div class="request-work">
                                    <h6>How requesting works...</h6>
                                    <p>
                                       <i class="mdi mdi-calendar"></i>
                                       <span>Suggest a date for your stay to the host. Select how many guests you would like to bring.</span>
                                    </p>
                                    <p>
                                       <i class="mdi mdi-account-multiple-outline mdi-17px"></i></i>
                                       <span>After clicking "Message to host", the host will then message you about availabilty. You will not be charged to send a request. </span>
                                    </p>
                                 </div>  
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="row reviews-row mx-0">
                        <div class="container">
                           <div class="col m8 s12">
                              <div class="reviews-section mt-20">
                                 <div class="row mx-0 valign-wrapper">
                                    <div class="left">
                                       <h5>REVIEWS</h5>
                                    </div>
                                    <div class="right ml-auto">
                                       <a href="">+ Review</a>
                                    </div>
                                 </div>
                                 <ul class="collection">
                                    <li class="collection-item avatar">
                                       <img src="images/demo-profile.jpg" alt="" class="circle">
                                       <span class="title">Adel Hasanat</span>
                                       <span class="ratings">
                                          <i class="mdi mdi-star"></i>
                                          <i class="mdi mdi-star"></i>
                                          <i class="mdi mdi-star"></i>
                                          <i class="mdi mdi-star"></i>
                                          <i class="mdi mdi-star"></i>
                                       </span>
                                       <p class="date">May 03, 2019</p>
                                       <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                    </li>
                                    <li class="collection-item avatar">
                                       <img src="images/demo-profile.jpg" alt="" class="circle">
                                       <span class="title">Adel Hasanat</span>
                                       <span class="ratings">
                                          <i class="mdi mdi-star"></i>
                                          <i class="mdi mdi-star"></i>
                                          <i class="mdi mdi-star"></i>
                                          <i class="mdi mdi-star"></i>
                                          <i class="mdi mdi-star"></i>
                                       </span>
                                       <p class="date">May 03, 2019</p>
                                       <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                    </li>
                                    <li class="collection-item avatar">
                                       <img src="images/demo-profile.jpg" alt="" class="circle">
                                       <span class="title">Adel Hasanat</span>
                                       <span class="ratings">
                                          <i class="mdi mdi-star"></i>
                                          <i class="mdi mdi-star"></i>
                                          <i class="mdi mdi-star"></i>
                                          <i class="mdi mdi-star"></i>
                                          <i class="mdi mdi-star"></i>
                                       </span>
                                       <p class="date">May 03, 2019</p>
                                       <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                    </li>
                                 </ul>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
            </div>
         </div>
      </div>
   </div>
</div>
   

<!-- edit data modal -->
<div id="homestayEditModal" class="modal tbpost_modal custom_modal split-page main_modal cust-pop dicrease-popup-compose event-detail-modal">
   <div class="modal_content_container">
      <div class="modal_content_child modal-content">
         <div class="popup-title ">
            <button class="hidden_close_span close_span waves-effect">
            <i class="mdi mdi-close mdi-20px compose_discard_popup"></i>
            </button>         
            <h3>Edit Homestay detail</h3>
            <a type="button" class="item_done crop_done hidden_close_span custom_close waves-effect" href="javascript:void(0)">Done</a>
         </div>
         <div class="custom_modal_content modal_content" id="createpopup">
            <div class="ablum-yours profile-tab">
               <div class="ablum-box detail-box">
                  <div class="content-holder main-holder">
                     <div class="summery">
                        <div class="dsection bborder expandable-holder expanded">
                           <div class="form-area expandable-area">
                              <form class="ablum-form">
                                 <div class="form-box">
                                    <div class="fulldiv">
                                       <div class="frow">
                                          <div class="caption-holder">
                                             <label>Title</label>
                                          </div>
                                          <div class="detail-holder">
                                             <div class="input-field">
                                                <input type="text" placeholder="Homestay title: i.e One room for homestay" class="fullwidth locinput "/>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="fulldiv">
                                       <div class="row">
                                          <div class="col s6">
                                             <div class="frow pr-5">
                                                <div class="caption-holder">
                                                   <label>Property type</label>
                                                </div>
                                                <a class="dropdown_text dropdown-button-left" href="javascript:void(0)" data-activates="homestayProp">
                                                   <span>House</span>
                                                   <i class="zmdi zmdi-caret-down"></i>
                                                </a>
                                                <ul id="homestayProp" class="dropdown-privacy dropdown-content custom_dropdown">
                                                   <li>
                                                      <a href="javascript:void(0)">House</a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">Apartment</a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">Condominium</a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">Farmstay</a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">Houseboat</a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">Bed and breakfast</a>
                                                   </li>
                                                </ul>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="fulldiv">
                                       <div class="frow">
                                          <div class="caption-holder mb-5">
                                             <label>What will guests have?</label>
                                          </div>
                                          <div class="detail-holder">
                                             <div class="detail-holder inline-radio">
                                                <input name="licensedguy" checked="" type="radio" id="enPl" value="enPl">
                                                <label for="enPl">Entire place</label>
                                                <input name="licensedguy" type="radio" id="prRm" value="prRm">
                                                <label for="prRm">Private room</label>
                                                <input name="licensedguy" type="radio" id="shRm" value="shRm">
                                                <label for="shRm">Shared room</label>     
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="fulldiv">
                                       <div class="frow">
                                          <div class="caption-holder mb-5">
                                             <label>Bath</label>
                                          </div>
                                          <div class="detail-holder">
                                             <div class="detail-holder inline-radio">
                                                <input name="bath" checked="" type="radio" id="prBt" value="prBt">
                                                <label for="prBt">private</label>
                                                <input name="bath" type="radio" id="shBt" value="shBt">
                                                <label for="shBt">Shared</label>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="fulldiv">
                                       <div class="frow">
                                          <div class="caption-holder">
                                             <label>Welcomed guest</label>
                                          </div>
                                          <div class="detail-holder">
                                             <div class="fitem">
                                                <div class="h-checkbox entertosend leftbox">
                                                   <input id="males" type="checkbox">
                                                   <label for="males">Males</label>
                                                </div>
                                             </div>
                                             <div class="fitem">
                                                <div class="h-checkbox entertosend leftbox">
                                                   <input id="females" type="checkbox">
                                                   <label for="females">Females</label>
                                                </div>
                                             </div>
                                             <div class="fitem">
                                                <div class="h-checkbox entertosend leftbox">
                                                   <input id="couples" type="checkbox">
                                                   <label for="couples">Couples</label>
                                                </div>
                                             </div>
                                             <div class="fitem">
                                                <div class="h-checkbox entertosend leftbox">
                                                   <input id="families" type="checkbox">
                                                   <label for="families">Families</label>
                                                </div>
                                             </div>
                                             <div class="fitem">
                                                <div class="h-checkbox entertosend leftbox">
                                                   <input id="students" type="checkbox">
                                                   <label for="students">Students</label>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="fulldiv">
                                       <div class="frow">
                                          <div class="caption-holder">
                                             <label>Homestay location</label>
                                          </div>
                                          <div class="detail-holder">
                                             <div class="input-field">
                                                <input type="text" placeholder="Enter city name" class="fullwidth locinput" data-query="all" onfocus="filderMapLocationModal(this)"id="createlocation"/>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="fulldiv">
                                       <div class="frow">
                                          <div class="caption-holder">
                                             <div class="row">
                                                <div class="col s4">
                                                   <label>Rate per adult guest</label>
                                                </div>
                                                <div class="col s6">
                                                   <div class="detail-holder">
                                                      <div class="input-field">
                                                         <input type="text" placeholder="20" class="fullwidth input-rate" id="createlocation"/>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="col s2">
                                                   <a class="dropdown_text dropdown-button currency_drp" href="javascript:void(0)" data-activates="currency_handler">
                                                      <span class="currency_label">USD</span>
                                                      <i class="zmdi zmdi-caret-down"></i>
                                                   </a>
                                                   <ul id="currency_handler" class="dropdown-privacy dropdown-content custom_dropdown">
                                                      <?php
                                                      $fee = array("USD", "EUR", "YEN", "CAD", "AUE");
                                                      foreach ($fee as $s8032n) {
                                                         ?>
                                                         <li> <a href="javascript:void(0)"><?=$s8032n?></a> </li>
                                                         <?php
                                                      }
                                                      ?>
                                                   </ul>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="fulldiv">
                                       <div class="frow">
                                          <div class="caption-holder">
                                             <label>Descibe your homestay</label>
                                          </div>
                                          <div class="detail-holder">
                                             <div class="input-field">
                                                <input type="text" placeholder="Tell people about your Homestay"/>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="fulldiv">
                                       <div class="frow">
                                          <div class="caption-holder">
                                             <label>Homestay rules</label>
                                          </div>
                                          <div class="detail-holder">
                                             <div class="input-field">
                                                <input type="text" placeholder="Tell your guest about your rules for homestay" class="fullwidth locinput "/>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="fulldiv">
                                       <div class="frow">
                                          <div class="caption-holder mb-10">
                                             <label>Services</label>
                                          </div>
                                          <div class="detail-holder">
                                             <a href="javascript:void(0)" class="check-image">
                                                <div class="image-select"></div>
                                                <img alt="spa" title="spa" src="images/amenity-spa.png">
                                             </a>
                                             <a href="javascript:void(0)" class="check-image">
                                                <div class="image-select"></div>
                                                <img alt="beach" title="beach" src="images/amenity-beach.png">
                                             </a>
                                             <a href="javascript:void(0)" class="check-image">
                                                <div class="image-select"></div>
                                                <img alt="wifi" title="wifi" src="images/amenity-wifi.png">
                                             </a>
                                             <a href="javascript:void(0)" class="check-image">
                                                <div class="image-select"></div>
                                                <img alt="breakfast" title="breakfast" src="images/amenity-breakfast.png">
                                             </a>
                                             <a href="javascript:void(0)" class="check-image">
                                                <div class="image-select"></div>
                                                <img alt="pool" title="pool" src="images/amenity-pool.png">
                                             </a>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="frow nomargin new-post">
                                       <div class="caption-holder">
                                          <label>Awesome photos help guests want to join upload</label>
                                       </div>
                                       <div class="detail-holder">
                                          <div class="input-field ">
                                             <div class="post-photos new_pic_add">
                                                <div class="img-row">
                                                   <div class="img-box">
                                                      <div class="custom-file addimg-box add-photo ablum-add">
                                                         <span class="icont">+</span><br><span class="">Upload photo</span>
                                                         <div class="addimg-icon">
                                                         </div>
                                                         <input class="upload custom-upload remove-custom-upload" title="Choose a file to upload" required="" data-class=".post-photos .img-row" multiple="true" type="file">
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <p class="photolabelinfo">Please add three cover photos for your camp profile</p>
                                    </div>
                                 </div>
                              </form>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="valign-wrapper additem_modal_footer modal-footer">
      <a href="javascript:void(0)" class="btngen-center-align close_modal open_discard_modal waves-effect">Cancel</a>
      <a href="javascript:void(0)" class="btngen-center-align waves-effect">Save</a>
   </div>
</div>

<?php include('common/discard_popup.php'); ?>

<!--map modal-->
<div id="compose_mapmodal" class="modal map_modal compose_inner_modal modalxii_level1">
   <?php include('common/map_modal.php'); ?>
</div>

<link rel="stylesheet" type="text/css" href="css/owl.carousel.css">
<script type="text/javascript" src="js/owl.carousel.js"></script>
<script type="text/javascript" src="js/wall.js"></script>

<?php include("script.php"); ?>