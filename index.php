<?php include("header.php"); ?> 
<div class="index-page"><?php include("common/menu.php"); ?></div>
<div class="menu-wrap">
   <div class="menu-sec h-search">
      <img src="images/homepage-bg.jpg" alt="header img">
      <div class="search-box">
           <div class="box-content mt-0">
               <div class="bc-row home-search">
                  <div class="row mx-0">
                     <div class="was-in-country dropdown782">
                        <a href="http://www.visit<?=$st_nm_L?>.jo" target="_blank"><span>Visit <?=$st_nm_S?></span></a>
                        <a href="javascript:void(0)" class="compose_visitcountryAction"><i class="zmdi zmdi-chevron-down"></i></a>
                     </div>
                  </div>
               </div>
           </div>
        </div> 
   </div> 
</div>
<div class="nav-sec menu-box">
   <div class="row mx-0">
      <div class="menu-links">
         <ul class="ul-main">
            <li>
               <a href="index.php">
                  <img src="images/home-icon.png" alt="menu icon">
                  <?=$st_nm_S?>
               </a>
            </li>
            <li>
               <a href="vtour.php">
                  <img src="images/hotels-icon.png" alt="menu icon">
                  <span class="virtualword">Virtual </span>Tours
               </a>
            </li>
            <li>
               <a href="todo.php">
                  <img src="images/to-do-icon.png" alt="menu icon">
                  To Do
               </a>
            </li>
            <li>
               <a href="watch.php">
                  <img src="images/live_shows.png" alt="menu icon">
                  Watch
               </a>
            </li>
            <li>
               <a href="discussion.php">
                  <img src="images/discussion-icon.png" alt="menu icon">
                  Discussion
               </a>
            </li>
            <li>
               <a href="photostream.php">
                  <img src="images/photostream-icon.png" alt="menu icon">
                  Photos
               </a>
            </li>
            <li>
               <a href="reviews.php">
                  <img src="images/tips-icon.png" alt="menu icon">
                  Reviews
               </a>
            </li>
            <li>
               <a href="blog.php">
                  <img src="images/blog-icon.png" alt="menu icon">
                  Blog
               </a>
            </li>
            <li>
               <a class="dropdown-button" href="javascript:void(0)" data-activates="moreLinks">
                  <img src="images/more-icon.png" alt="menu icon">
                  More 
               </a>
               <ul id="moreLinks" class="dropdown-content custom_dropdown">
                  <li><a href="questions.php">Questions</a></li>
                  <li><a href="tripstory.php">Trip Story</a></li>
                  <li><a href="tips.php">Tips</a></li>
                  <li><a href="collections.php">Photo Collections</a></li>
                  <li><a href="locals.php"><?=$st_nm_S?> Locals</a></li>
                  <li><a href="travellers.php">People travelling to <?=$st_nm_S?></a></li>
                  <li><a href="local-guide.php">Local Guide</a></li>
                  <li><a href="local-driver.php">Local Driver</a></li>
                  <li><a href="local-guide.php">City Guide</a></li>
                  <li><a href="business-pages.php">Business pages</a></li>
               </ul>
            </li>
         </ul>
      </div>
   </div>
</div>
<div class="floating-icon">
   <div class="scrollup-btnbox anim-side btnbox scrollup-float">
      <div class="scrollup-button float-icon">
         <span class="icon-holder ispan">
            <i class="mdi mdi-arrow-up-bold-circle"></i>
         </span>
      </div>
   </div>
</div>
<div class="clear"></div>
<div class="container page_container">
   <?php include("common/leftmenu.php"); ?>
   <div class="fixed-layout">
      <div class="main-content main-page places-page iamin-page pb-0 mt-0">
         <div class="combined-column wide-open main-page full-page">
         <div class="tablist sub-tabs">
            <ul class="tabs tabs-fixed-width text-menu left tabsnew">
               <li class="tab"><a tabname="Wall" href="#places-all"></a></li>
            </ul>
         </div>
         <div class="places-content places-all">
            <div class="container">
               <div class="places-column m-top">
                  <div class="">
                     <div class="placesall-content bottom_tabs">
                        <div class="content-box">
                           <!-- <div class="sec-heading">
                              <h2>
                                 <span class="heading-wrap">
                                    <span>Stay & Eat</span>
                                 </span>
                              </h2>
                           </div> -->
                           <div class="placesection yellowsection">
                              <div class="tcontent-holder">
                                 <div class="cbox-title m-t nborder" id="hotel-title">
                                    <div class="valign-wrapper left">
                                       <img src="images/lodgeicon-sm.png" />
                                       <span><span>Popular</span> Hotel in <?=$st_nm_S?></span>
                                    </div>
                                    <div class="top-stuff right">
                                       <div class="more-actions">
                                          <a href="hotels.php" class="viewall-link clicable">View All</a>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="cbox-desc">
                                    <div class="places-content-holder">
                                       <div class="list-holder">
                                          <div class="row">
                                             <div class="col m4 s12 pb-holder">
                                                <div class="placebox">
                                                   <a href="javascript:void(0)">
                                                      <div class="imgholder himg-box">
                                                         <img src="images/lodge-img-1.jpg" class="himg imgfix" />
                                                         <div class="overlay"></div>
                                                      </div>
                                                      <div class="descholder">
                                                         <h5>Moeavenpick Resort <?=$st_nm_S?></h5>
                                                         <span class="ratings">
                                                         <i class="mdi mdi-star active"></i>
                                                         <i class="mdi mdi-star active"></i>
                                                         <i class="mdi mdi-star active"></i>
                                                         <i class="mdi mdi-star"></i>
                                                         <i class="mdi mdi-star"></i>
                                                         <label>45 Reviews</label>
                                                         </span>
                                                         <div class="tags">
                                                            <span>Luxury</span>
                                                            <span>Families</span>
                                                         </div>
                                                      </div>
                                                   </a>
                                                </div>
                                             </div>
                                             <div class="col m4 s12 pb-holder">
                                                <div class="placebox">
                                                   <a href="javascript:void(0)">
                                                      <div class="imgholder himg-box">
                                                         <img src="images/lodge-img-2.jpg" class="himg" />
                                                         <div class="overlay"></div>
                                                      </div>
                                                      <div class="descholder">
                                                         <h5><?=$st_nm_S?> Moon Hotel</h5>
                                                         <span class="ratings">
                                                         <i class="mdi mdi-star active"></i>
                                                         <i class="mdi mdi-star active"></i>
                                                         <i class="mdi mdi-star active"></i>
                                                         <i class="mdi mdi-star active"></i>
                                                         <i class="mdi mdi-star"></i>
                                                         <label>20 Reviews</label>
                                                         </span>
                                                         <div class="tags">
                                                            <span>Budget</span>
                                                         </div>
                                                      </div>
                                                   </a>
                                                </div>
                                             </div>
                                             <div class="col m4 s12 pb-holder third-col hide-on-small-only">
                                                <div class="placebox">
                                                   <a href="javascript:void(0)">
                                                      <div class="imgholder himg-box">
                                                         <img src="images/lodge-img-3.jpg" class="himg" />
                                                         <div class="overlay"></div>
                                                      </div>
                                                      <div class="descholder">
                                                         <h5>Marriott <?=$st_nm_S?> Hotel</h5>
                                                         <span class="ratings">
                                                         <i class="mdi mdi-star active"></i>
                                                         <i class="mdi mdi-star active"></i>
                                                         <i class="mdi mdi-star active"></i>
                                                         <i class="mdi mdi-star active"></i>
                                                         <i class="mdi mdi-star active"></i>
                                                         <label>65 Reviews</label>
                                                         </span>
                                                         <div class="tags">
                                                            <span>Luxury</span>
                                                            <span>Business</span>
                                                         </div>
                                                      </div>
                                                   </a>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="placesection redsection">
                              <div class="tcontent-holder">
                                 <div class="cbox-title nborder" id="restaurant-title">
                                    <div class="valign-wrapper left">
                                       <img src="images/dineicon-sm.png" />
                                       <span><span>Popular</span> Restaurants in <?=$st_nm_S?></span>
                                    </div>
                                    <div class="top-stuff right">
                                       <a href="restaurants.php" class="viewall-link clicable">View All</a>
                                    </div>
                                 </div>
                                 <div class="cbox-desc">
                                    <div class="places-content-holder">
                                       <div class="list-holder">
                                          <div class="row">
                                             <div class="col m4 s12 pb-holder">
                                                <div class="placebox">
                                                   <a href="javascript:void(0)">
                                                      <div class="imgholder himg-box">
                                                         <img src="images/dine-img-1.jpg" class="himg" />
                                                         <div class="overlay"></div>
                                                      </div>
                                                      <div class="descholder">
                                                         <h5>Alqantarah restaurant</h5>
                                                         <span class="ratings">
                                                         <i class="mdi mdi-star active"></i>
                                                         <i class="mdi mdi-star active"></i>
                                                         <i class="mdi mdi-star active"></i>
                                                         <i class="mdi mdi-star"></i>
                                                         <i class="mdi mdi-star"></i>
                                                         <label>45 Reviews</label>
                                                         </span>
                                                      </div>
                                                   </a>
                                                </div>
                                             </div>
                                             <div class="col m4 s12 pb-holder">
                                                <div class="placebox">
                                                   <a href="javascript:void(0)">
                                                      <div class="imgholder himg-box">
                                                         <img src="images/dine-img-2.jpg" class="himg" />
                                                         <div class="overlay"></div>
                                                      </div>
                                                      <div class="descholder">
                                                         <h5>The Basin Restaurant</h5>
                                                         <span class="ratings">
                                                         <i class="mdi mdi-star active"></i>
                                                         <i class="mdi mdi-star active"></i>
                                                         <i class="mdi mdi-star active"></i>
                                                         <i class="mdi mdi-star active"></i>
                                                         <i class="mdi mdi-star"></i>
                                                         <label>20 Reviews</label>
                                                         </span>
                                                         <div class="tags">
                                                            <span>Foodies</span>
                                                            <span>Adventure</span>
                                                         </div>
                                                      </div>
                                                   </a>
                                                </div>
                                             </div>
                                             <div class="col m4 s12 pb-holder third-col hide-on-small-only">
                                                <div class="placebox">
                                                   <a href="javascript:void(0)">
                                                      <div class="imgholder himg-box">
                                                         <img src="images/dine-img-3.jpg" class="himg" />
                                                         <div class="overlay"></div>
                                                      </div>
                                                      <div class="descholder">
                                                         <h5>The <?=$st_nm_S?> Kitchen</h5>
                                                         <span class="ratings">
                                                         <i class="mdi mdi-star active"></i>
                                                         <i class="mdi mdi-star active"></i>
                                                         <i class="mdi mdi-star active"></i>
                                                         <i class="mdi mdi-star active"></i>
                                                         <i class="mdi mdi-star active"></i>
                                                         <label>65 Reviews</label>
                                                         </span>
                                                      </div>
                                                   </a>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="sec-heading">
                              <h2 class="mb-15">
                                 <span class="heading-wrap">
                                    <span>Plan your Next Trip</span>
                                 </span>
                              </h2>
                           </div>
                           <div class="placesection bluesection">
                              <div class="tcontent-holder">
                                 <div class="cbox-title nborder" id="todo-title">
                                    <!-- <div class="valign-wrapper left">
                                       <img src="images/todoicon-sm.png" />
                                       <span><span>Popular</span> Attractions in <?=$st_nm_S?></span>
                                    </div> -->
                                    <div class="top-stuff right">
                                       <a href="attractions.php" class="viewall-link">View All</a>
                                       <div class="more-actions">
                                          <ul class="tabs nav-tabs nav-custom-tabs tabsnew valign-wrapper">
                                          </ul>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="cbox-desc">
                                    <div class="places-content-holder">
                                       <div class="list-holder">
                                          <div class="row">
                                             <div class="col m4 s12 pb-holder">
                                                <div class="placebox">
                                                   <a href="javascript:void(0)">
                                                      <div class="imgholder himg-box">
                                                         <img src="images/todo-img-1.jpg" class="himg" />
                                                         <div class="overlay"></div>
                                                      </div>
                                                      <div class="descholder">
                                                         <h5><?=$st_nm_S?> <?=$st_nm_S?></h5>
                                                         <span class="ratings">
                                                         <i class="mdi mdi-star active"></i>
                                                         <i class="mdi mdi-star active"></i>
                                                         <i class="mdi mdi-star active"></i>
                                                         <i class="mdi mdi-star"></i>
                                                         <i class="mdi mdi-star"></i>
                                                         <label>45 Reviews</label>
                                                         </span>
                                                         <div class="tags">
                                                            <span>History</span>
                                                            <span>Adventure</span>
                                                         </div>
                                                      </div>
                                                   </a>
                                                </div>
                                             </div>
                                             <div class="col m4 s12 pb-holder">
                                                <div class="placebox">
                                                   <a href="javascript:void(0)">
                                                      <div class="imgholder himg-box">
                                                         <img src="images/todo-img-2.jpg" class="himg" />
                                                         <div class="overlay"></div>
                                                      </div>
                                                      <div class="descholder">
                                                         <h5>The Treasury - AI Khazneh</h5>
                                                         <span class="ratings">
                                                         <i class="mdi mdi-star active"></i>
                                                         <i class="mdi mdi-star active"></i>
                                                         <i class="mdi mdi-star active"></i>
                                                         <i class="mdi mdi-star active"></i>
                                                         <i class="mdi mdi-star"></i>
                                                         <label>20 Reviews</label>
                                                         </span>
                                                         <div class="tags">
                                                            <span>History</span>
                                                         </div>
                                                      </div>
                                                   </a>
                                                </div>
                                             </div>
                                             <div class="col m4 s12 pb-holder third-col hide-on-small-only">
                                                <div class="placebox">
                                                   <a href="javascript:void(0)">
                                                      <div class="imgholder himg-box">
                                                         <img src="images/todo-img-3.jpg" class="himg" />
                                                         <div class="overlay"></div>
                                                      </div>
                                                      <div class="descholder">
                                                         <h5>The Siq</h5>
                                                         <span class="ratings">
                                                         <i class="mdi mdi-star active"></i>
                                                         <i class="mdi mdi-star active"></i>
                                                         <i class="mdi mdi-star active"></i>
                                                         <i class="mdi mdi-star active"></i>
                                                         <i class="mdi mdi-star active"></i>
                                                         <label>65 Reviews</label>
                                                         </span>
                                                         <div class="tags">
                                                            <span>History</span>
                                                            <span>Adventure</span>
                                                         </div>
                                                      </div>
                                                   </a>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="placesection local-guide-sec">
                              <div class="tcontent-holder mb-20">
                                 <div class="cbox-title nborder title-1">
                                    <div class="valign-wrapper left mb-10">
                                       <a href="javascript:void(0)">
                                          <span class="heading-icon"><i class="mdi mdi-account"></i></span>
                                       </a>
                                       <span>Local guide in <?=$st_nm_S?></span> 
                                    </div>
                                    <div class="top-stuff right">
                                       <a href="javascript:void(0)" class="viewall-link" onclick="openDirectTab('places-guides')">View All</a>
                                    </div>
                                    <p>Hire a local guide and enjoy tour experience</p>
                                 </div>
                                 <div class="cbox-desc tour-guides">
                                    <div class="places-content-holder">
                                       <div class="list-holder guidebox-list">
                                          <div class="row">
                                             <div class="col l3 m3 s6">
                                                <div class="guide-box">
                                                   <div class="imgholder">
                                                      <img class="circle" src="images/demo-profile.jpg" />
                                                      <div class="overlay">
                                                         <span class="licensed-span">Licensed</span>
                                                      </div>
                                                   </div>
                                                   <div class="descholder">
                                                      <h3>Nimish Parekh</h3>
                                                      <p>Guide for <?=$st_nm_S?></p>
                                                      <span class="stars-holder yellow-stars">
                                                      <i class="mdi mdi-star active"></i>
                                                      <i class="mdi mdi-star active"></i>
                                                      <i class="mdi mdi-star active"></i>
                                                      <i class="mdi mdi-star active"></i>
                                                      <i class="mdi mdi-star"></i>
                                                      <label>23 Refers</label>
                                                      </span>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="col l3 m3 s6">
                                                <div class="guide-box">
                                                   <div class="imgholder">
                                                      <img src="images/guide-1.png" />
                                                      <div class="overlay">
                                                         <span class="licensed-span">Licensed</span>
                                                      </div>
                                                   </div>
                                                   <div class="descholder">
                                                      <h3>Nimish Parekh</h3>
                                                      <p>Guide for <?=$st_nm_S?></p>
                                                      <span class="stars-holder yellow-stars">
                                                      <i class="mdi mdi-star active"></i>
                                                      <i class="mdi mdi-star active"></i>
                                                      <i class="mdi mdi-star active"></i>
                                                      <i class="mdi mdi-star active"></i>
                                                      <i class="mdi mdi-star"></i>
                                                      <label>23 Refers</label>
                                                      </span>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="col l3 m3 s6 hide-on-small-only">
                                                <div class="guide-box">
                                                   <div class="imgholder">
                                                      <img src="images/guide-2.png" />
                                                      <div class="overlay">
                                                         <span class="licensed-span">Licensed</span>
                                                      </div>
                                                   </div>
                                                   <div class="descholder">
                                                      <h3>Nimish Parekh</h3>
                                                      <p>Guide for <?=$st_nm_S?></p>
                                                      <span class="stars-holder yellow-stars">
                                                      <i class="mdi mdi-star active"></i>
                                                      <i class="mdi mdi-star active"></i>
                                                      <i class="mdi mdi-star active"></i>
                                                      <i class="mdi mdi-star active"></i>
                                                      <i class="mdi mdi-star"></i>
                                                      <label>23 Refers</label>
                                                      </span>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="col l3 m3 s6 last_guide hide-on-small-only">
                                                <div class="guide-box">
                                                   <div class="imgholder">
                                                      <img src="images/guide-3.png" />
                                                      <div class="overlay">
                                                         <span class="licensed-span">Licensed</span>
                                                      </div>
                                                   </div>
                                                   <div class="descholder">
                                                      <h3>Nimish Parekh</h3>
                                                      <p>Guide for <?=$st_nm_S?></p>
                                                      <span class="stars-holder yellow-stars">
                                                      <i class="mdi mdi-star active"></i>
                                                      <i class="mdi mdi-star active"></i>
                                                      <i class="mdi mdi-star active"></i>
                                                      <i class="mdi mdi-star active"></i>
                                                      <i class="mdi mdi-star"></i>
                                                      <label>23 Refers</label>
                                                      </span>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="placesection general-page events-sec moreheight">
                              <div class="tcontent-holder grid-view">
                                 <div class="cbox-title nborder title-1">
                                    <div class="valign-wrapper left mb-10">
                                       <a href="javascript:void(0)">
                                          <span class="heading-icon"><i class="mdi mdi-calendar"></i></span>
                                       </a>
                                       <span>Events this week in <?=$st_nm_S?></span> 
                                    </div>
                                    <div class="top-stuff right">
                                       <a href="javascript:void(0)" class="viewall-link" onclick="openDirectTab('places-events')">View All</a>
                                    </div>
                                    <p>Discover the best events happening in <?=$st_nm_S?> every week hand picked by Trip com&apos;s City editors.</p>
                                 </div>
                                 <div class="cbox-desc">
                                    <div class="places-content-holder">
                                       <div class="commevents-list generalbox-list">
                                          <div class="row">
                                             <div class="col l3 m4 s12">
                                                <a href="javascript:void(0);" onclick="commeventsGoing(event,'commevent1',this)" class="commevents-box general-box">
                                                   <div class="photo-holder">
                                                      <img src="images/additem-commevents.png">
                                                   </div>
                                                   <div class="content-holder">
                                                      <h4>CS London weekly meet #234 @Pendrel's Oak, Holborn</h4>
                                                      <div class="userinfo">
                                                         <span class="month">Nov</span>
                                                         <span class="date">17</span>
                                                      </div>
                                                      <div class="username">
                                                         <span>Greg Batmarx</span>
                                                      </div>
                                                      <div class="action-btns">
                                                         <span class="noClick" onclick="commeventsGoing(event,'commevent1',this)">Attend</span>
                                                      </div>
                                                   </div>
                                                </a>
                                             </div>
                                             <div class="col l3 m4 s12">
                                                <a href="javascript:void(0)" class="commevents-box general-box">
                                                   <div class="photo-holder">
                                                      <img src="images/collection-img-2.png">
                                                   </div>
                                                   <div class="content-holder">
                                                      <h4>Renewable energy</h4>
                                                      <div class="icon-line">
                                                         <span>Tagline goes here</span>
                                                      </div>
                                                      <div class="userinfo">
                                                         <span class="month">Nov</span>
                                                         <span class="date">17</span>
                                                      </div>
                                                      <div class="username">
                                                         <span>Greg Batmarx</span>
                                                      </div>
                                                      <div class="icon-line">
                                                         <i class="zmdi zmdi-check"></i>
                                                         Chrarles Thomas, Ankit Shah and Pratik Patel followed this
                                                      </div>
                                                      <div class="action-btns">
                                                         <span class="followers">120 Followers</span>
                                                         <span class="noClick" onclick="commeventsGoing(event,'commevent2',this)">Attend</span>
                                                      </div>
                                                   </div>
                                                </a>
                                             </div>
                                             <div class="col l3 m4 s12 hide-on-small-only">
                                                <a href="javascript:void(0)" class="commevents-box general-box">
                                                   <div class="photo-holder">
                                                      <img src="images/collection-img-3.png">
                                                   </div>
                                                   <div class="content-holder">
                                                      <h4>Black-white illustrations to H. L.</h4>
                                                      <div class="icon-line">
                                                         <span>Tagline goes here</span>
                                                      </div>
                                                      <div class="userinfo">
                                                         <span class="month">Nov</span>
                                                         <span class="date">17</span>
                                                      </div>
                                                      <div class="username">
                                                         <span>Henry Lion Oldie</span>
                                                      </div>
                                                      <div class="icon-line">
                                                         <i class="zmdi zmdi-check"></i>
                                                         Chrarles Thomas, Ankit Shah and Pratik Patel followed this
                                                      </div>
                                                      <div class="action-btns">
                                                         <span class="followers">120 Followers</span>
                                                         <span class="noClick" onclick="commeventsGoing(event,'commevent3',this)">Attend</span>
                                                      </div>
                                                   </div>
                                                </a>
                                             </div>
                                             <div class="col l3 m4 s12 hide-on-small-only">
                                                <a href="javascript:void(0)" class="commevents-box general-box">
                                                   <div class="photo-holder">
                                                      <img src="images/collection-img-4.png">
                                                   </div>
                                                   <div class="content-holder">
                                                      <h4>B&W Photography</h4>
                                                      <div class="icon-line">
                                                         <span>Tagline goes here</span>
                                                      </div>
                                                      <div class="userinfo">
                                                         <span class="month">Nov</span>
                                                         <span class="date">17</span>
                                                      </div>
                                                      <div class="username">
                                                         <span>Rui Luis</span>
                                                      </div>
                                                      <div class="icon-line">
                                                         <i class="zmdi zmdi-check"></i>
                                                         Chrarles Thomas, Ankit Shah and Pratik Patel followed this
                                                      </div>
                                                      <div class="action-btns">
                                                         <span class="followers">120 Followers</span>
                                                         <span class="noClick" onclick="commeventsGoing(event,'commevent4',this)">Attend</span>
                                                      </div>
                                                   </div>
                                                </a>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="placesection general-page groups-sec moreheight">
                              <div class="tcontent-holder grid-view">
                                 <div class="cbox-title nborder title-1">
                                    <div class="valign-wrapper left mb-10">
                                       <a href="javascript:void(0)">
                                          <span class="heading-icon"><i class="mdi mdi-account-multiple"></i></span>
                                       </a>
                                       <span>Groups in <?=$st_nm_S?></span> 
                                    </div>
                                    <div class="top-stuff right">
                                       <a href="javascript:void(0)" class="viewall-link" onclick="openDirectTab('places-groups')">View All</a>
                                    </div>
                                    <p>Discover the best events happening in <?=$st_nm_S?> every week hand picked by Trip com's City editors.</p>
                                 </div>
                                 <div class="cbox-desc">
                                    <div class="places-content-holder">
                                       <div class="groups-list generalbox-list">
                                          <div class="row">
                                             <div class="col l3 m4 s12">
                                                <a href="javascript:void(0);" class="groups-box general-box">
                                                   <div class="photo-holder">
                                                      <img src="images/additem-groups.png">
                                                   </div>
                                                   <div class="content-holder">
                                                      <h4>Airlines (low-cost, budget, cheap flights)</h4>
                                                      <div class="cityinfo">
                                                         Maxico city, Maxico
                                                      </div>
                                                      <div class="username">
                                                         <span class="inline-list">
                                                         <i class=”mdi mdi-account-group”></i></i>
                                                         <span>34,546 Members</span>
                                                         </span>
                                                         <span class="inline-list">
                                                         <i class="mdi mdi-pencil-box-outline"></i>
                                                         <span>1000 Posts</span>
                                                         </span>
                                                      </div>
                                                      <div class="icon-line subtext">
                                                         Last activity 45 minutes ago
                                                      </div>
                                                      <div class="members">
                                                         34,546 Members
                                                      </div>
                                                      <div class="action-btns">
                                                         <span class="followers">Last activity 45 minutes ago</span>
                                                         <span class="noClick approved" onclick="groupsJoin(event,'commevent1',this)">Member</span>
                                                      </div>
                                                   </div>
                                                </a>
                                             </div>
                                             <div class="col l3 m4 s12">
                                                <a href="javascript:void(0);" class="groups-box general-box">
                                                   <div class="photo-holder">
                                                      <img src="images/additem-groups.png">
                                                   </div>
                                                   <div class="content-holder">
                                                      <h4>Europe</h4>
                                                      <div class="cityinfo">
                                                         Maxico city, Maxico
                                                      </div>
                                                      <div class="username">
                                                         <span class="inline-list">
                                                         <i class=”mdi mdi-account-group”></i></i>
                                                         <span>34,546 Members</span>
                                                         </span>
                                                         <span class="inline-list">
                                                         <i class="mdi mdi-pencil-box-outline"></i>
                                                         <span>1000 Posts</span>
                                                         </span>
                                                      </div>
                                                      <div class="username">
                                                         <i class="mdi mdi-earth"></i>
                                                         <span>Worldwide</span>
                                                      </div>
                                                      <div class="icon-line subtext">
                                                         Last activity 45 minutes ago
                                                      </div>
                                                      <div class="members">
                                                         34,546 Members
                                                      </div>
                                                      <div class="action-btns">
                                                         <span class="followers">Last activity 45 minutes ago</span>
                                                         <span class="noClick" onclick="groupsJoin(event,'commevent1',this)">Join</span>
                                                      </div>
                                                   </div>
                                                </a>
                                             </div>
                                             <div class="col l3 m4 s12 hide-on-small-only">
                                                <a href="javascript:void(0);" class="groups-box general-box">
                                                   <div class="photo-holder">
                                                      <img src="images/additem-groups.png">
                                                   </div>
                                                   <div class="content-holder">
                                                      <h4>Hitchhikers</h4>
                                                      <div class="cityinfo">
                                                         Maxico city, Maxico
                                                      </div>
                                                      <div class="username">
                                                         <span class="inline-list">
                                                         <i class=”mdi mdi-account-group”></i></i>
                                                         <span>34,546 Members</span>
                                                         </span>
                                                         <span class="inline-list">
                                                         <i class="mdi mdi-pencil-box-outline"></i>
                                                         <span>1000 Posts</span>
                                                         </span>
                                                      </div>
                                                      <div class="icon-line subtext">
                                                         Last activity 45 minutes ago
                                                      </div>
                                                      <div class="members">
                                                         34,546 Members
                                                      </div>
                                                      <div class="action-btns">
                                                         <span class="followers">Last activity 45 minutes ago</span>
                                                         <span class="noClick disabled" onclick="groupsJoin(event,'commevent1',this)">Request Sent</span>
                                                      </div>
                                                   </div>
                                                </a>
                                             </div>
                                             <div class="col l3 m4 s12 hide-on-small-only">
                                                <a href="javascript:void(0);" class="groups-box general-box">
                                                   <div class="photo-holder">
                                                      <img src="images/additem-groups.png">
                                                   </div>
                                                   <div class="content-holder">
                                                      <h4>Worldwide Voluteering</h4>
                                                      <div class="cityinfo">
                                                         Maxico city, Maxico
                                                      </div>
                                                      <div class="username">
                                                         <span class="inline-list">
                                                         <i class=”mdi mdi-account-group”></i></i>
                                                         <span>34,546 Members</span>
                                                         </span>
                                                         <span class="inline-list">
                                                         <i class="mdi mdi-pencil-box-outline"></i>
                                                         <span>1000 Posts</span>
                                                         </span>
                                                      </div>
                                                      <div class="username">
                                                         <i class="mdi mdi-earth"></i>
                                                         <span>Worldwide</span>
                                                      </div>
                                                      <div class="icon-line subtext">
                                                         Last activity 45 minutes ago
                                                      </div>
                                                      <div class="members">
                                                         34,546 Members
                                                      </div>
                                                      <div class="action-btns">
                                                         <span class="followers">Last activity 45 minutes ago</span>
                                                         <span class="noClick disabled" onclick="groupsJoin(event,'commevent1',this)">Request Sent</span>
                                                      </div>
                                                   </div>
                                                </a>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="sec-heading">
                              <h2>
                                 <span class="heading-wrap">
                                    <span>Travel Advice</span>
                                 </span>
                              </h2>
                           </div>
                           <div class="placesection local-guide-sec pb-20">
                              <div class="tcontent-holder mb-20">
                                 <!-- <div class="cbox-title nborder title-1">
                                    <div class="valign-wrapper left mb-10">
                                       <a href="javascript:void(0)">
                                          <span class="heading-icon"><i class="mdi mdi-account"></i></span>
                                       </a>
                                       <span>Travel Advice</span> 
                                    </div>
                                 </div> -->
                                 <div class="cbox-desc tour-guides mt-0">
                                    <div class="places-content-holder">
                                       <div class="list-holder guidebox-list">
                                          <div class="row">
                                             <div class="col l3 m3 s6">
                                                <div class="advice-box">
                                                   <a href="javascript:void(0)" class="valign-wrapper left w-100">
                                                      <img class="circle" src="images/fact_sheet_best_time_to_visit.svg" alt="" />
                                                      <span>Best time to visit</span>
                                                   </a>
                                                </div>
                                             </div>
                                             <div class="col l3 m3 s6">
                                                <div class="advice-box">
                                                   <a href="javascript:void(0)" class="valign-wrapper left w-100">
                                                      <img class="circle" src="images/fact_sheet_getting_around.svg" alt="" />
                                                      <span>Getting around</span>
                                                   </a>
                                                </div>
                                             </div>
                                             <div class="col l3 m3 s6">
                                                <div class="advice-box">
                                                   <a href="javascript:void(0)" class="valign-wrapper left w-100">
                                                      <img class="circle" src="images/fact_sheet_local_customs.svg" alt="" />
                                                      <span>Local customs</span>
                                                   </a>
                                                </div>
                                             </div>
                                             <div class="col l3 m3 s6">
                                                <div class="advice-box">
                                                   <a href="javascript:void(0)" class="valign-wrapper left w-100">
                                                      <img class="circle" src="images/fact_sheet_tips.svg" alt="" />
                                                      <span>Tips from the pros</span>
                                                   </a>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>

                           <div class="placesection destinations-sec">
                              <div class="tcontent-holder topattractions-section">
                                 <div class="cbox-title nborder title-1">
                                    <div class="valign-wrapper left">
                                       <a href="javascript:void(0)">
                                          <span class="heading-icon"><i class="mdi mdi-map-marker"></i></span>
                                       </a>
                                       <span>Similar Destinations</span> 
                                    </div>
                                 </div>
                                 <div class="cbox-desc">
                                    <div class="places-content-holder">
                                       <div class="list-holder">
                                          <div class="row">
                                             <div class="col m4 s6 fpb-holder">
                                                <div class="f-placebox destibox">
                                                   <a href="javascript:void(0)">
                                                      <div class="imgholder himg-box">
                                                         <img src="images/placesdest-img-1.jpg" class="himg" />
                                                         <div class="overlay"></div>
                                                      </div>
                                                      <div class="descholder">
                                                         <h5>Rome, Italy</h5>
                                                         <ul>
                                                            <li><i class="mdi mdi-menu-right"></i>10 Hotels</li>
                                                            <li><i class="mdi mdi-menu-right"></i>2 Restaurant</li>
                                                            <li><i class="mdi mdi-menu-right"></i>40 Travellers</li>
                                                         </ul>
                                                         <span class="btn btn-primary btn-sm right waves-effect waves-light">Explore</span>
                                                      </div>
                                                   </a>
                                                </div>
                                             </div>
                                             <div class="col m4 s6 fpb-holder">
                                                <div class="f-placebox destibox">
                                                   <a href="javascript:void(0)">
                                                      <div class="imgholder himg-box">
                                                         <img src="images/placesdest-img-2.jpg" class="himg" />
                                                         <div class="overlay"></div>
                                                      </div>
                                                      <div class="descholder">
                                                         <h5>Florence, Italy</h5>
                                                         <ul>
                                                            <li><i class="mdi mdi-menu-right"></i>10 Hotels</li>
                                                            <li><i class="mdi mdi-menu-right"></i>2 Restaurant</li>
                                                            <li><i class="mdi mdi-menu-right"></i>40 Travellers</li>
                                                         </ul>
                                                         <span class="btn btn-primary btn-sm right waves-effect waves-light">Explore</span>
                                                      </div>
                                                   </a>
                                                </div>
                                             </div>
                                             <div class="col m4 s6 fpb-holder third-col hide-on-small-only">
                                                <div class="f-placebox destibox">
                                                   <a href="javascript:void(0)">
                                                      <div class="imgholder himg-box">
                                                         <img src="images/placesdest-img-3.jpg" class="himg" />
                                                         <div class="overlay"></div>
                                                      </div>
                                                      <div class="descholder">
                                                         <h5>Versailles, France</h5>
                                                         <ul>
                                                            <li><i class="mdi mdi-menu-right"></i>10 Hotels</li>
                                                            <li><i class="mdi mdi-menu-right"></i>2 Restaurant</li>
                                                            <li><i class="mdi mdi-menu-right"></i>40 Travellers</li>
                                                         </ul>
                                                         <span class="btn btn-primary btn-sm right waves-effect waves-light">Explore</span>
                                                      </div>
                                                   </a>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="placesection pop-photos-sec">
                              <div class="tcontent-holder topattractions-section">
                                 <div class="cbox-title nborder title-1">
                                    <div class="valign-wrapper left">
                                       <a href="javascript:void(0)">
                                          <span class="heading-icon"><i class="mdi mdi-file-image"></i></span>
                                       </a>
                                       <span>Popular Photos</span> 
                                    </div>
                                    <div class="top-stuff right">
                                       <div class="more-actions">
                                          <a href="photostream.php" class="viewall-link clicable">View All</a>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="cbox-desc">
                                    <div class="places-content-holder">
                                       <div class="list-holder topplaces-list">
                                          <!-- New Slider-->
                                          <div class="row">
                                             <div class="gallery-content">
                                                <div id="placebox" class="lgt-gallery-photo lgt-gallery-justified home-justified-gallery dis-none">
                                                   <div data-src="images/wgallery1.jpg" class="allow-gallery">
                                                        <img class="himg" src="images/wgallery1.jpg"/>   
                                                        <a href="javascript:void(0)" class="removeicon"><i class="mdi mdi-delete"></i></a>
                                                      <div class="caption">
                                                         <div class="left">
                                                            <span class="title">Big Bird</span> <br>
                                                            <span class="attribution">By Adel Hasanat</span>
                                                         </div>
                                                         <div class="right icons">
                                                            <a href="javascript: void(0)" class="prevent-gallery like"><i class="mdi mdi-thumb-up-outline mdi-15px" onclick="toggleIcons(this)"></i>
                                                            </a><span class="lcount">7</span>
                                                            <a href="javascript: void(0)" class="prevent-gallery"><i class="mdi mdi-comment-outline mdi-15px cmnt"></i></a><span class="lcount">7</span>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div data-src="images/wgallery2.jpg" class="allow-gallery">
                                                        <img class="himg" src="images/wgallery2.jpg"/>   
                                                        <a href="javascript:void(0)" class="removeicon"><i class="mdi mdi-delete"></i></a>
                                                      <div class="caption">
                                                         <div class="left">
                                                            <span class="title">Big Bird</span> <br>
                                                            <span class="attribution">By Adel Hasanat</span>
                                                         </div>
                                                         <div class="right icons">
                                                            <a href="javascript: void(0)" class="prevent-gallery like"><i class="mdi mdi-thumb-up-outline mdi-15px" onclick="toggleIcons(this)"></i>
                                                            </a><span class="lcount">7</span>
                                                            <a href="javascript: void(0)" class="prevent-gallery"><i class="mdi mdi-comment-outline mdi-15px cmnt"></i></a><span class="lcount">7</span>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div data-src="images/wgallery3.jpg" class="allow-gallery">
                                                        <img class="himg" src="images/wgallery3.jpg"/>   
                                                        <a href="javascript:void(0)" class="removeicon"><i class="mdi mdi-delete"></i></a>
                                                      <div class="caption">
                                                         <div class="left">
                                                            <span class="title">Big Bird</span> <br>
                                                            <span class="attribution">By Adel Hasanat</span>
                                                         </div>
                                                         <div class="right icons">
                                                            <a href="javascript: void(0)" class="prevent-gallery like"><i class="mdi mdi-thumb-up-outline mdi-15px" onclick="toggleIcons(this)"></i>
                                                            </a><span class="lcount">7</span>
                                                            <a href="javascript: void(0)" class="prevent-gallery"><i class="mdi mdi-comment-outline mdi-15px cmnt"></i></a><span class="lcount">7</span>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div data-src="images/wgallery3.jpg" class="allow-gallery">
                                                        <img class="himg" src="images/wgallery3.jpg"/>   
                                                        <a href="javascript:void(0)" class="removeicon"><i class="mdi mdi-delete"></i></a>
                                                      <div class="caption">
                                                         <div class="left">
                                                            <span class="title">Big Bird</span> <br>
                                                            <span class="attribution">By Adel Hasanat</span>
                                                         </div>
                                                         <div class="right icons">
                                                            <a href="javascript: void(0)" class="prevent-gallery like"><i class="mdi mdi-thumb-up-outline mdi-15px" onclick="toggleIcons(this)"></i>
                                                            </a><span class="lcount">7</span>
                                                            <a href="javascript: void(0)" class="prevent-gallery"><i class="mdi mdi-comment-outline mdi-15px cmnt"></i></a><span class="lcount">7</span>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div data-src="images/wgallery3.jpg" class="allow-gallery">
                                                        <img class="himg" src="images/wgallery3.jpg"/>   
                                                        <a href="javascript:void(0)" class="removeicon"><i class="mdi mdi-delete"></i></a>
                                                      <div class="caption">
                                                         <div class="left">
                                                            <span class="title">Big Bird</span> <br>
                                                            <span class="attribution">By Adel Hasanat</span>
                                                         </div>
                                                         <div class="right icons">
                                                            <a href="javascript: void(0)" class="prevent-gallery like"><i class="mdi mdi-thumb-up-outline mdi-15px" onclick="toggleIcons(this)"></i>
                                                            </a><span class="lcount">7</span>
                                                            <a href="javascript: void(0)" class="prevent-gallery"><i class="mdi mdi-comment-outline mdi-15px cmnt"></i></a><span class="lcount">7</span>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div data-src="images/wgallery3.jpg" class="allow-gallery">
                                                        <img class="himg" src="images/wgallery3.jpg"/>   
                                                        <a href="javascript:void(0)" class="removeicon"><i class="mdi mdi-delete"></i></a>
                                                      <div class="caption">
                                                         <div class="left">
                                                            <span class="title">Big Bird</span> <br>
                                                            <span class="attribution">By Adel Hasanat</span>
                                                         </div>
                                                         <div class="right icons">
                                                            <a href="javascript: void(0)" class="prevent-gallery like"><i class="mdi mdi-thumb-up-outline mdi-15px" onclick="toggleIcons(this)"></i>
                                                            </a><span class="lcount">7</span>
                                                            <a href="javascript: void(0)" class="prevent-gallery"><i class="mdi mdi-comment-outline mdi-15px cmnt"></i></a><span class="lcount">7</span>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div data-src="images/wgallery3.jpg" class="allow-gallery">
                                                        <img class="himg" src="images/wgallery3.jpg"/>   
                                                        <a href="javascript:void(0)" class="removeicon"><i class="mdi mdi-delete"></i></a>
                                                      <div class="caption">
                                                         <div class="left">
                                                            <span class="title">Big Bird</span> <br>
                                                            <span class="attribution">By Adel Hasanat</span>
                                                         </div>
                                                         <div class="right icons">
                                                            <a href="javascript: void(0)" class="prevent-gallery like"><i class="mdi mdi-thumb-up-outline mdi-15px" onclick="toggleIcons(this)"></i>
                                                            </a><span class="lcount">7</span>
                                                            <a href="javascript: void(0)" class="prevent-gallery"><i class="mdi mdi-comment-outline mdi-15px cmnt"></i></a><span class="lcount">7</span>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div data-src="images/wgallery3.jpg" class="allow-gallery">
                                                        <img class="himg" src="images/wgallery3.jpg"/>   
                                                        <a href="javascript:void(0)" class="removeicon"><i class="mdi mdi-delete"></i></a>
                                                      <div class="caption">
                                                         <div class="left">
                                                            <span class="title">Big Bird</span> <br>
                                                            <span class="attribution">By Adel Hasanat</span>
                                                         </div>
                                                         <div class="right icons">
                                                            <a href="javascript: void(0)" class="prevent-gallery like"><i class="mdi mdi-thumb-up-outline mdi-15px" onclick="toggleIcons(this)"></i>
                                                            </a><span class="lcount">7</span>
                                                            <a href="javascript: void(0)" class="prevent-gallery"><i class="mdi mdi-comment-outline mdi-15px cmnt"></i></a><span class="lcount">7</span>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div data-src="images/wgallery3.jpg" class="allow-gallery">
                                                        <img class="himg" src="images/wgallery3.jpg"/>   
                                                        <a href="javascript:void(0)" class="removeicon"><i class="mdi mdi-delete"></i></a>
                                                      <div class="caption">
                                                         <div class="left">
                                                            <span class="title">Big Bird</span> <br>
                                                            <span class="attribution">By Adel Hasanat</span>
                                                         </div>
                                                         <div class="right icons">
                                                            <a href="javascript: void(0)" class="prevent-gallery like"><i class="mdi mdi-thumb-up-outline mdi-15px" onclick="toggleIcons(this)"></i>
                                                            </a><span class="lcount">7</span>
                                                            <a href="javascript: void(0)" class="prevent-gallery"><i class="mdi mdi-comment-outline mdi-15px cmnt"></i></a><span class="lcount">7</span>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div data-src="images/wgallery3.jpg" class="allow-gallery">
                                                        <img class="himg" src="images/wgallery3.jpg"/>   
                                                        <a href="javascript:void(0)" class="removeicon"><i class="mdi mdi-delete"></i></a>
                                                      <div class="caption">
                                                         <div class="left">
                                                            <span class="title">Big Bird</span> <br>
                                                            <span class="attribution">By Adel Hasanat</span>
                                                         </div>
                                                         <div class="right icons">
                                                            <a href="javascript: void(0)" class="prevent-gallery like"><i class="mdi mdi-thumb-up-outline mdi-15px" onclick="toggleIcons(this)"></i>
                                                            </a><span class="lcount">7</span>
                                                            <a href="javascript: void(0)" class="prevent-gallery"><i class="mdi mdi-comment-outline mdi-15px cmnt"></i></a><span class="lcount">7</span>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <!--New Slider-->
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="sec-heading">
                              <h2>
                                 <span class="heading-wrap">
                                    <span>Explore Locals</span>
                                 </span>
                              </h2>
                           </div>
                           <div class="placesection loc-dine-sec">
                              <div class="tcontent-holder topattractions-section">
                                 <div class="cbox-title nborder title-1">
                                    <div class="valign-wrapper left">
                                       <a href="javascript:void(0)">
                                          <span class="heading-icon"><i class="mdi mdi-basecamp"></i></span>
                                       </a>
                                       <span>Locals Dine</span> 
                                    </div>
                                 </div>
                                 <div class="cbox-desc pt-0">
                                    <div class="places-content-holder">
                                       <div class="list-holder topplaces-list">
                                          <!-- New Slider-->
                                          <div class=" mx-0">
                                             <div class="hcontent-holder home-section gray-section tours-page tours dine-local st_iamin_nm pt-0">
                                                <div class="">
                                                   <div class="tours-section pt-0">
                                                      <div class="row">
                                                         <div class="col l4 m6 s12 wow slideInLeft">
                                                            <div class="tour-box">
                                                               <span class="imgholder">
                                                                  <img src="images/home-tour1.jpg">
                                                                  <div class="price-tag"><span> €70 </span></div>
                                                               </span>
                                                               <span class="descholder">
                                                                  <a href="">
                                                                     <img src="https://screen-api.eatwith.com/files/478854/-/scale_crop/68x68/center/-/progressive/yes/" alt="">
                                                                  </a>
                                                                  <small class="dine-hosttext">Hosted by <a dir="auto" href="">Tino</a> in Amsterdam</small>
                                                                  <div class="dine-eventtags">
                                                                     <div class="tag-inner">Dinner</div>
                                                                  </div>
                                                                  <a class="dine-eventtitle" dir="auto" href="">A Seafood Feast with a Venetian Sailor</a>
                                                                  <div class="dine-rating pt-20 center">
                                                                     <i class="mdi mdi-star"></i>
                                                                     <i class="mdi mdi-star"></i>
                                                                     <i class="mdi mdi-star"></i>
                                                                     <i class="mdi mdi-star"></i>
                                                                     <i class="mdi mdi-star"></i>
                                                                  </div>
                                                               </span>
                                                            </div>
                                                         </div>
                                                         <div class="col l4 m6 s12 wow slideInLeft">
                                                            <div class="tour-box">
                                                               <span class="imgholder">
                                                                  <img src="images/home-tour1.jpg">
                                                                  <div class="price-tag"><span> €70 </span></div>
                                                               </span>
                                                               <span class="descholder">
                                                                  <a href="">
                                                                     <img src="https://screen-api.eatwith.com/files/478854/-/scale_crop/68x68/center/-/progressive/yes/" alt="">
                                                                  </a>
                                                                  <small class="dine-hosttext">Hosted by <a dir="auto" href="">Tino</a> in Amsterdam</small>
                                                                  <div class="dine-eventtags">
                                                                     <div class="tag-inner">Dinner</div>
                                                                  </div>
                                                                  <a class="dine-eventtitle" dir="auto" href="">A Seafood Feast with a Venetian Sailor</a>
                                                                  <div class="dine-rating pt-20 center">
                                                                     <i class="mdi mdi-star"></i>
                                                                     <i class="mdi mdi-star"></i>
                                                                     <i class="mdi mdi-star"></i>
                                                                     <i class="mdi mdi-star"></i>
                                                                     <i class="mdi mdi-star"></i>
                                                                  </div>
                                                               </span>
                                                            </div>
                                                         </div>
                                                         <div class="col l4 m6 s12 wow slideInLeft hide-on-small-only">
                                                            <div class="tour-box">
                                                               <span class="imgholder">
                                                                  <img src="images/home-tour1.jpg">
                                                                  <div class="price-tag"><span> €70 </span></div>
                                                               </span>
                                                               <span class="descholder">
                                                                  <a href="">
                                                                     <img src="https://screen-api.eatwith.com/files/478854/-/scale_crop/68x68/center/-/progressive/yes/" alt="">
                                                                  </a>
                                                                  <small class="dine-hosttext">Hosted by <a dir="auto" href="">Tino</a> in Amsterdam</small>
                                                                  <div class="dine-eventtags">
                                                                     <div class="tag-inner">Dinner</div>
                                                                  </div>
                                                                  <a class="dine-eventtitle" dir="auto" href="">A Seafood Feast with a Venetian Sailor</a>
                                                                  <div class="dine-rating pt-20 center">
                                                                     <i class="mdi mdi-star"></i>
                                                                     <i class="mdi mdi-star"></i>
                                                                     <i class="mdi mdi-star"></i>
                                                                     <i class="mdi mdi-star"></i>
                                                                     <i class="mdi mdi-star"></i>
                                                                  </div>
                                                               </span>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <!--New Slider-->
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="placesection homestay-sec">
                              <div class="tcontent-holder topattractions-section">
                                 <div class="cbox-title nborder title-1">
                                    <div class="valign-wrapper left">
                                       <a href="javascript:void(0)">
                                          <span class="heading-icon"><i class="mdi mdi-hotel"></i></span>
                                       </a>
                                       <span>Homestay</span> 
                                    </div>
                                 </div>
                                 <div class="cbox-desc pt-0">
                                    <div class="places-content-holder">
                                       <div class="list-holder topplaces-list">
                                          <!-- New Slider-->
                                          <div class=" mx-0">
                                             <div class="hcontent-holder home-section gray-section tours-page tours dine-local st_iamin_nm pt-0">
                                                <div class="">
                                                   <div class="tours-section pt-0">
                                                      <div class="row">
                                                         <div class="col l4 m6 s12 wow slideInLeft">
                                                            <div class="tour-box">
                                                               <span class="imgholder">
                                                                  <img src="images/home-tour1.jpg">
                                                                  <div class="price-tag"><span> €70 </span></div>
                                                               </span>
                                                               <span class="descholder">
                                                                  <a href="">
                                                                     <img src="https://screen-api.eatwith.com/files/478854/-/scale_crop/68x68/center/-/progressive/yes/" alt="">
                                                                  </a>
                                                                  <small class="dine-hosttext">Hosted by <a dir="auto" href="">Tino</a> in Amsterdam</small>
                                                                  <div class="dine-eventtags">
                                                                     <div class="dine-roomtext">one room shared bath</div>
                                                                  </div>
                                                                  <a class="dine-eventtitle" dir="auto" href="">Title of the Homestay</a>
                                                                  <div class="dine-rating center">
                                                                     <i class="mdi mdi-star"></i>
                                                                     <i class="mdi mdi-star"></i>
                                                                     <i class="mdi mdi-star"></i>
                                                                     <i class="mdi mdi-star"></i>
                                                                     <i class="mdi mdi-star"></i>
                                                                  </div>
                                                               </span>
                                                            </div>
                                                         </div>
                                                         <div class="col l4 m6 s12 wow slideInLeft">
                                                            <div class="tour-box">
                                                               <span class="imgholder">
                                                                  <img src="images/home-tour1.jpg">
                                                                  <div class="price-tag"><span> €70 </span></div>
                                                               </span>
                                                               <span class="descholder">
                                                                  <a href="">
                                                                     <img src="https://screen-api.eatwith.com/files/478854/-/scale_crop/68x68/center/-/progressive/yes/" alt="">
                                                                  </a>
                                                                  <small class="dine-hosttext">Hosted by <a dir="auto" href="">Tino</a> in Amsterdam</small>
                                                                  <div class="dine-eventtags">
                                                                     <div class="dine-roomtext">one room shared bath</div>
                                                                  </div>
                                                                  <a class="dine-eventtitle" dir="auto" href="">Title of the Homestay</a>
                                                                  <div class="dine-rating center">
                                                                     <i class="mdi mdi-star"></i>
                                                                     <i class="mdi mdi-star"></i>
                                                                     <i class="mdi mdi-star"></i>
                                                                     <i class="mdi mdi-star"></i>
                                                                     <i class="mdi mdi-star"></i>
                                                                  </div>
                                                               </span>
                                                            </div>
                                                         </div>
                                                         <div class="col l4 m6 s12 wow slideInLeft hide-on-small-only">
                                                            <div class="tour-box">
                                                               <span class="imgholder">
                                                                  <img src="images/home-tour1.jpg">
                                                                  <div class="price-tag"><span> €70 </span></div>
                                                               </span>
                                                               <span class="descholder">
                                                                  <a href="">
                                                                     <img src="https://screen-api.eatwith.com/files/478854/-/scale_crop/68x68/center/-/progressive/yes/" alt="">
                                                                  </a>
                                                                  <small class="dine-hosttext">Hosted by <a dir="auto" href="">Tino</a> in Amsterdam</small>
                                                                  <div class="dine-eventtags">
                                                                     <div class="dine-roomtext">one room shared bath</div>
                                                                  </div>
                                                                  <a class="dine-eventtitle" dir="auto" href="">Title of the Homestay</a>
                                                                  <div class="dine-rating center">
                                                                     <i class="mdi mdi-star"></i>
                                                                     <i class="mdi mdi-star"></i>
                                                                     <i class="mdi mdi-star"></i>
                                                                     <i class="mdi mdi-star"></i>
                                                                     <i class="mdi mdi-star"></i>
                                                                  </div>
                                                               </span>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <!--New Slider-->
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="placesection cities-sec">
                              <div class="tcontent-holder">
                                 <div class="cbox-title nborder title-1">
                                    <div class="valign-wrapper left">
                                       <a href="javascript:void(0)">
                                          <span class="heading-icon"><i class="mdi mdi-map-marker-radius"></i></span>
                                       </a>
                                       <span><span>Popular</span> nearby cities</span> 
                                    </div>
                                 </div>
                                 <div class="cbox-desc p-t-0">
                                    <div class="places-content-holder">
                                       <div class="list-holder">
                                          <div class="row">
                                             <div class="col m4 s6 fpb-holder">
                                                <div class="f-placebox destibox">
                                                   <a href="javascript:void(0)">
                                                      <div class="imgholder himg-box">
                                                         <img src="images/topplaces-1.jpg" class="himg" />
                                                         <div class="overlay"></div>
                                                      </div>
                                                      <div class="descholder">
                                                         <h5><?=$st_nm_S?>, <?=$st_nm_S?></h5>
                                                         <ul>
                                                            <li><i class="mdi mdi-menu-right"></i>10 Hotels</li>
                                                            <li><i class="mdi mdi-menu-right"></i>2 Restaurant</li>
                                                            <li><i class="mdi mdi-menu-right"></i>40 Travellers</li>
                                                         </ul>
                                                      </div>
                                                   </a>
                                                </div>
                                             </div>
                                             <div class="col m4 s6 fpb-holder">
                                                <div class="f-placebox destibox">
                                                   <a href="javascript:void(0)">
                                                      <div class="imgholder himg-box">
                                                         <img src="images/topplaces-2.jpg" class="himg" />
                                                         <div class="overlay"></div>
                                                      </div>
                                                      <div class="descholder">
                                                         <h5>Az Zarqa, <?=$st_nm_S?></h5>
                                                         <ul>
                                                            <li><i class="mdi mdi-menu-right"></i>10 Hotels</li>
                                                            <li><i class="mdi mdi-menu-right"></i>2 Restaurant</li>
                                                            <li><i class="mdi mdi-menu-right"></i>40 Travellers</li>
                                                         </ul>
                                                      </div>
                                                   </a>
                                                </div>
                                             </div>
                                             <div class="col m4 s12 fpb-holder third-col hide-on-small-only">
                                                <div class="f-placebox destibox">
                                                   <a href="javascript:void(0)">
                                                      <div class="imgholder himg-box">
                                                         <img src="images/topplaces-3.jpg" class="himg" />
                                                         <div class="overlay"></div>
                                                      </div>
                                                      <div class="descholder">
                                                         <h5>Madaba, <?=$st_nm_S?></h5>
                                                         <ul>
                                                            <li><i class="mdi mdi-menu-right"></i>10 Hotels</li>
                                                            <li><i class="mdi mdi-menu-right"></i>2 Restaurant</li>
                                                            <li><i class="mdi mdi-menu-right"></i>40 Travellers</li>
                                                         </ul>
                                                      </div>
                                                   </a>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="map-holder visible">
                                          <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3110.3465133386144!2d-9.167423685010494!3d38.77868997958898!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xd193295d5b45545%3A0x3f9e7b6a5f00e12c!2sPerta!5e0!3m2!1sen!2sin!4v1481089901870" width="600" height="450" frameborder="0" allowfullscreen></iframe>
                                          <a href="javascript:void(0)" class="overlay" onclick="expandMap(this,'#todo-title')"></a>
                                          <a href="javascript:void(0)" class="closelink" onclick="shrinkMap(this)"><i class="mdi mdi-close"></i> Close</a>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <?php include("common/footer.php"); ?>
                     </div>

                     <div id="places-dine" class="placesdine-content subtab places-discussion-main top_tabs dis-none">
                        <div class="content-box">
                           <div class="mbl-tabnav">
                              <a href="javascript:void(0)" onclick="openDirectTab('places-all')"><i class="mdi mdi-arrow-left"></i></a> 
                              <h6>Restaurants</h6>
                           </div>
                           <div class="placesection redsection">
                              <div class="cbox-desc hotels-page np">
                                 <div class="tcontent-holder moreinfo-outer">
                                    <div class="top-stuff top-graybg" id="all-restaurant">
                                       <div class="more-actions">
                                          <ul class="tabs tabsnew text-right">
                                             <li class="tab"><a href="javascript:void(0)" class="manageMap" onclick="openMapSection(this)"><i class="zmdi zmdi-pin"></i>Map</a></li>
                                             <li class="tab"><a href="javascript:void(0)" onclick="openListSection(this)"><i class="zmdi zmdi-view-list-alt zmdi-hc-lg"></i>List</a></li>
                                          </ul>
                                       </div>
                                       <h6>345 restaurants found in <span><?=$st_nm_S?></span></h6>
                                    </div>
                                    <div class="places-content-holder">
                                       <div class="map-holder">
                                          <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3110.3465133386144!2d-9.167423685010494!3d38.77868997958898!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xd193295d5b45545%3A0x3f9e7b6a5f00e12c!2sPerta!5e0!3m2!1sen!2sin!4v1481089901870" width="600" height="450" frameborder="0" allowfullscreen></iframe>
                                          <a href="javascript:void(0)" class="overlay" onclick="expandMap(this,'#all-restaurant')"></a>
                                          <a href="javascript:void(0)" class="closelink" onclick="shrinkMap(this)"><i class="mdi mdi-close"></i> Close</a>
                                       </div>
                                       <div class="list-holder">
                                          <div class="hotel-list fullw-list">
                                             <ul>
                                                <li>
                                                   <div class="hotel-li expandable-holder dealli mobilelist">
                                                      <div class="summery-info">
                                                         <div class="imgholder himg-box"><img src="images/restaurant1.png" class="himg" /></div>
                                                         <div class="descholder">
                                                            <a href="javascript:void(0)" class="expand-link" onclick="mng_expandable(this,'hasClose')">
                                                               <h4>Hashim Restaurant</h4>
                                                               <div class="clear"></div>
                                                               <div class="reviews-link">
                                                                  <span class="review-count">30 Reviews</span>
                                                                  <span class="checks-holder">
                                                                  <i class="mdi mdi-star active"></i>
                                                                  <i class="mdi mdi-star active"></i>
                                                                  <i class="mdi mdi-star active"></i>
                                                                  <i class="mdi mdi-star active"></i>
                                                                  <i class="mdi mdi-star"></i>
                                                                  <label>Very Good - 70/100</label>
                                                                  </span>
                                                               </div>
                                                               <span class="address">Middle Eastem & African, Mediterranean</span>
                                                               <span class="distance-info"><i class="mdi mdi-phone"></i> 999-999-9999</span>
                                                               <div class="more-holder">
                                                                  <div class="tagging" onclick="explandTags(this)">
                                                                     Popular with:
                                                                     <span>Budget</span>
                                                                  </div>
                                                               </div>
                                                            </a>
                                                         </div>
                                                      </div>
                                                      <div class="expandable-area">
                                                         <a href="javascript:void(0)" class="shrink-link" onclick="mng_expandable(this,'closeIt')"><i class="mdi mdi-close"></i> Close</a>
                                                         <div class="clear"></div>
                                                         <div class="explandable-tabs">
                                                            <ul class="tabs tabsnew subtab-menu">
                                                               <li class="active tab"><a  href="#subtab-reviews2">Reviews</a></li>
                                                               <li class="tab"><a data-which="photo" href="#subtab-photos2" data-tab="subtab-photos">Photos</a></li>
                                                               <li><a aria-expanded="false" data-toggle="tab" href="#subtab-details2">Details</a></li>
                                                            </ul>
                                                            <div class="tab-content">
                                                               <div id="subtab-reviews2" class="tab-pane fade active in">
                                                                  <div class="reviews-summery">
                                                                     <div class="reviews-people">
                                                                        <ul>
                                                                           <li>
                                                                              <div class="reviewpeople-box">
                                                                                 <div class="imgholder"><img src="images/people-3.png" /></div>
                                                                                 <div class="descholder">
                                                                                    <h6>Kelly Mark <span>about 2 weeks ago</span></h6>
                                                                                    <div class="stars-holder">
                                                                                       <img src="images/filled-star.png" />
                                                                                       <img src="images/filled-star.png" />
                                                                                       <img src="images/filled-star.png" />
                                                                                       <img src="images/blank-star.png" />
                                                                                       <img src="images/blank-star.png" />
                                                                                    </div>
                                                                                    <div class="clear"></div>
                                                                                    <p>We enjoyed the lounge and bar at the Ritz where you are offered many choices for drinks and some pretty elaborate looking dishes of food as well.</p>
                                                                                 </div>
                                                                              </div>
                                                                           </li>
                                                                           <li>
                                                                              <div class="reviewpeople-box">
                                                                                 <div class="imgholder"><img src="images/people-2.png" /></div>
                                                                                 <div class="descholder">
                                                                                    <h6>John Davior <span>about 8 months ago</span></h6>
                                                                                    <div class="stars-holder">
                                                                                       <img src="images/filled-star.png" />
                                                                                       <img src="images/filled-star.png" />
                                                                                       <img src="images/filled-star.png" />
                                                                                       <img src="images/filled-star.png" />
                                                                                       <img src="images/blank-star.png" />
                                                                                    </div>
                                                                                    <div class="clear"></div>
                                                                                    <p>If you want a fancy London experience than The Ritz is where you need to go! At least budget for High Tea!</p>
                                                                                 </div>
                                                                              </div>
                                                                           </li>
                                                                           <li>
                                                                              <div class="reviewpeople-box">
                                                                                 <div class="imgholder"><img src="images/people-1.png" /></div>
                                                                                 <div class="descholder">
                                                                                    <h6>Joe Doe <span>about 11 months ago</span></h6>
                                                                                    <div class="stars-holder">
                                                                                       <img src="images/filled-star.png" />
                                                                                       <img src="images/filled-star.png" />
                                                                                       <img src="images/filled-star.png" />
                                                                                       <img src="images/blank-star.png" />
                                                                                       <img src="images/blank-star.png" />
                                                                                    </div>
                                                                                    <div class="clear"></div>
                                                                                    <p>I am not at all sure this is the best hotel in London, but it does deserve the reputation as one of the most glamourous.</p>
                                                                                 </div>
                                                                              </div>
                                                                           </li>
                                                                        </ul>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                               <div id="subtab-photos2" class="tab-pane fade subtab-photos">
                                                                  <div class="photo-gallery">
                                                                     <div class="img-preview">
                                                                        <img src="images/post-img1.jpg" />
                                                                     </div>
                                                                     <div class="thumbs-img">
                                                                        <ul>
                                                                           <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img class="himg" src="images/post-img1.jpg" /></a></li>
                                                                           <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img src="images/post-img2.jpg" class="himg" /></a></li>
                                                                           <li><a href="javascript:void(0)" onclick="previewImage(this)" class="vimg-box"><img src="images/post-img3.jpg" class="vimg" /></a></li>
                                                                           <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img src="images/post-img4.jpg" class="himg" /></a></li>
                                                                           <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img src="images/post-img5.jpg" class="himg" /></a></li>
                                                                           <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img class="himg" src="images/post-img1.jpg" /></a></li>
                                                                           <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img src="images/post-img2.jpg" class="himg" /></a></li>
                                                                           <li><a href="javascript:void(0)" onclick="previewImage(this)" class="vimg-box"><img src="images/post-img3.jpg" class="vimg" /></a></li>
                                                                           <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img src="images/post-img4.jpg" class="himg" /></a></li>
                                                                           <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img src="images/post-img5.jpg" class="himg" /></a></li>
                                                                        </ul>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                               <div id="subtab-details2" class="tab-pane fade">
                                                                  <div class="subdetail-box">
                                                                     <div class="infoholder">
                                                                        <div class="descholder">
                                                                           <div class="more-holder">
                                                                              <ul class="infoul">
                                                                                 <li>
                                                                                    <i class="zmdi zmdi-pin"></i>
                                                                                    132 Brick Lane | E1 6RU, <?=$st_nm_S?> E1 6RU, <?=$st_nm_S?>
                                                                                 </li>
                                                                                 <li>
                                                                                    <i class="mdi mdi-phone"></i>
                                                                                    +44 20 7247 8210
                                                                                 </li>
                                                                                 <li>
                                                                                    <i class="mdi mdi-earth"></i>
                                                                                    http://www.yourwebsite.com
                                                                                 </li>
                                                                                 <li>
                                                                                    <i class="mdi mdi-clock-outline"></i>
                                                                                    Mon-Fri : 12:00 PM - 10:00 AM
                                                                                 </li>
                                                                                 <li>
                                                                                    <i class="mdi mdi-certificate "></i>
                                                                                    Ranked #1 in <?=$st_nm_S?> Hotels
                                                                                 </li>
                                                                              </ul>
                                                                              <div class="tagging" onclick="explandTags(this)">
                                                                                 Popular with:
                                                                                 <span>point of interest</span>
                                                                                 <span>establishment</span>
                                                                              </div>
                                                                           </div>
                                                                        </div>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </li>
                                                <li>
                                                   <div class="hotel-li expandable-holder">
                                                      <div class="summery-info">
                                                         <div class="imgholder himg-box"><img src="images/restaurant2.png" class="himg" /></div>
                                                         <div class="descholder">
                                                            <h4>Emirates Grand</h4>
                                                            <div class="clear"></div>
                                                            <div class="reviews-link">
                                                               <span class="checks-holder">
                                                               <i class="mdi mdi-star active"></i>
                                                               <i class="mdi mdi-star active"></i>
                                                               <i class="mdi mdi-star active"></i>
                                                               <i class="mdi mdi-star active"></i>
                                                               <i class="mdi mdi-star"></i>
                                                               <label>34 Reviews</label>
                                                               </span>
                                                            </div>
                                                            <span class="distance-info">Middle Eastem & African, Mediterranean</span>
                                                            <div class="more-holder">
                                                               <div class="tagging" onclick="explandTags(this)">
                                                                  Popular with:
                                                                  <span>Family</span>
                                                                  <span>Luxury</span>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div class="expandable-area drawer-holder">
                                                         <div class="expanded-details">
                                                            Some details goes here...Some details goes here...Some details goes here...Some details goes here...Some details goes here...Some details goes here...Some details goes here...
                                                         </div>
                                                      </div>
                                                   </div>
                                                </li>
                                                <li>
                                                   <div class="hotel-li expandable-holder">
                                                      <div class="summery-info">
                                                         <div class="imgholder himg-box"><img src="images/restaurant3.png" class="himg" /></div>
                                                         <div class="descholder">
                                                            <h4>Al-Quds</h4>
                                                            <div class="clear"></div>
                                                            <div class="reviews-link">
                                                               <span class="checks-holder">
                                                               <i class="mdi mdi-star active"></i>
                                                               <i class="mdi mdi-star active"></i>
                                                               <i class="mdi mdi-star active"></i>
                                                               <i class="mdi mdi-star active"></i>
                                                               <i class="mdi mdi-star"></i>
                                                               <label>34 Reviews</label>
                                                               </span>
                                                            </div>
                                                            <span class="distance-info">Middle Eastem & African, Mediterranean</span>
                                                            <div class="more-holder">
                                                               <div class="tagging" onclick="explandTags(this)">
                                                                  Popular with:
                                                                  <span>Luxury</span>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div class="expandable-area drawer-holder">
                                                         <div class="expanded-details">
                                                            Some details goes here...Some details goes here...Some details goes here...Some details goes here...Some details goes here...Some details goes here...Some details goes here...
                                                         </div>
                                                      </div>
                                                   </div>
                                                </li>
                                             </ul>
                                             <div class="pagination">
                                                <div class="link-holder">
                                                   <a href="javascript:void(0)"><i class="mdi mdi-arrow-left-bold-circle"></i> Prev</a>
                                                </div>
                                                <div class="link-holder">
                                                   <a href="javascript:void(0)">Next <i class="mdi mdi-arrow-right-bold-circle"></i></a>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="moreinfo-box">
                                       <a href="javascript:void(0)" onclick="closePlacesMoreInfo(this)" class="backarrow"><i class="mdi mdi-arrow-left-bold-circle"></i></a>
                                       <div class="infoholder nice-scroll">
                                          <div class="imgholder"><img src="images/hotel1.png" /></div>
                                          <div class="descholder wid_0">
                                             <h4>The Guest House</h4>
                                             <div class="clear"></div>
                                             <div class="reviews-link">
                                                <span class="checks-holder">
                                                <i class="mdi mdi-star active"></i>
                                                <i class="mdi mdi-star active"></i>
                                                <i class="mdi mdi-star active"></i>
                                                <i class="mdi mdi-star active"></i>
                                                <i class="mdi mdi-star"></i>
                                                <label>34 Reviews</label>
                                                </span>
                                             </div>
                                             <span class="distance-info">Middle Eastem &amp; African, Mediterranean</span>
                                             <div class="clear"></div>
                                             <div class="more-holder">
                                                <ul class="infoul">
                                                   <li>
                                                      <i class="zmdi zmdi-pin"></i>
                                                      132 Brick Lane | E1 6RU, <?=$st_nm_S?> E1 6RU, <?=$st_nm_S?>
                                                   </li>
                                                   <li>
                                                      <i class="mdi mdi-phone"></i>
                                                      +44 20 7247 8210
                                                   </li>
                                                   <li>
                                                      <i class="mdi mdi-earth"></i>
                                                      http://www.yourwebsite.com
                                                   </li>
                                                   <li>
                                                      <i class="mdi mdi-clock-outline"></i>
                                                      Today, 12:00 PM - 12:00 AM
                                                   </li>
                                                   <li>
                                                      <i class="mdi mdi-certificate "></i>
                                                      Ranked #1 in <?=$st_nm_S?> Hotels
                                                   </li>
                                                </ul>
                                                <div class="tagging" onclick="explandTags(this)">
                                                   Popular with:
                                                   <span>Budget</span>
                                                   <span>Foodies</span>
                                                   <span>Family</span>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>

                     <div id="places-lodge" class="placeslodge-content subtab places-discussion-main top_tabs dis-none">
                        <div class="content-box hotels-page">
                           <div class="search-area side-area">
                              <a href="javascript:void(0)" class="expand-link" onclick="mng_drop_searcharea(this)"><i class="mdi mdi-tune mdi-20px grey-text"></i></a>
                              <div class="expandable-area">
                                 <div class="content-box bshadow">
                                    <a href="javascript:void(0)" class="closearea" onclick="mng_drop_searcharea(this)">
                                    <img src="images/cross-icon.png" />
                                    </a>
                                    <div class="cbox-title nborder">
                                       Narrow your search results
                                    </div>
                                    <div class="cbox-desc">
                                       <div class="srow">
                                          <h6>Hotel Class</h6>
                                          <ul>
                                             <li>
                                                <div class="h-checkbox entertosend leftbox">
                                                   <input id="filter1" type="checkbox">
                                                   <label for="filter1">
                                                   <span class="stars-holder">
                                                   <i class="mdi mdi-star"></i>
                                                   <i class="mdi mdi-star"></i>
                                                   <i class="mdi mdi-star"></i>
                                                   <i class="mdi mdi-star"></i>
                                                   <i class="mdi mdi-star"></i>
                                                   </span></label>
                                                </div>
                                             </li>
                                             <li>
                                                <div class="h-checkbox entertosend leftbox">
                                                   <input id="filter2" type="checkbox">
                                                   <label for="filter2">
                                                   <span class="stars-holder">
                                                   <i class="mdi mdi-star"></i>
                                                   <i class="mdi mdi-star"></i>
                                                   <i class="mdi mdi-star"></i>
                                                   <i class="mdi mdi-star"></i>
                                                   </span></label>
                                                </div>
                                             </li>
                                             <li>
                                                <div class="h-checkbox entertosend leftbox">
                                                   <input id="filter3" type="checkbox">
                                                   <label for="filter3">
                                                   <span class="stars-holder">
                                                   <i class="mdi mdi-star"></i>
                                                   <i class="mdi mdi-star"></i>
                                                   <i class="mdi mdi-star"></i>
                                                   </span>
                                                   </label>
                                                </div>
                                             </li>
                                             <li>
                                                <div class="h-checkbox entertosend leftbox">
                                                   <input id="filter4" type="checkbox">
                                                   <label for="filter4">
                                                   <span class="stars-holder">
                                                   <i class="mdi mdi-star"></i>
                                                   <i class="mdi mdi-star"></i>                                                                   </span>
                                                   </label>
                                                </div>
                                             </li>
                                             <li>
                                                <div class="h-checkbox entertosend leftbox">
                                                   <input id="filter5" type="checkbox">
                                                   <label for="filter5">
                                                   <span class="stars-holder">
                                                   <i class="mdi mdi-star"></i>                                                                                                                                                                                                           </span>
                                                   </label>
                                                </div>
                                             </li>
                                          </ul>
                                       </div>
                                       <div class="srow">
                                          <h6>Guest Ratings</h6>
                                          <ul>
                                             <li>
                                                <div class="h-checkbox entertosend leftbox">
                                                   <input id="filterstar1" type="checkbox">
                                                   <label for="filterstar1">
                                                   <span class="checks-holder">
                                                   <i class="zmdi zmdi-check-circle active"></i>
                                                   <i class="zmdi zmdi-check-circle active"></i>
                                                   <i class="zmdi zmdi-check-circle active"></i>
                                                   <i class="zmdi zmdi-check-circle active"></i>
                                                   <i class="zmdi zmdi-check-circle active"></i>
                                                   </span>
                                                   </label>
                                                </div>
                                             </li>
                                             <li>
                                                <div class="h-checkbox entertosend leftbox">
                                                   <input id="filterstar2" type="checkbox">
                                                   <label for="filterstar2">
                                                   <span class="checks-holder">
                                                   <i class="zmdi zmdi-check-circle active"></i>
                                                   <i class="zmdi zmdi-check-circle active"></i>
                                                   <i class="zmdi zmdi-check-circle active"></i>
                                                   <i class="zmdi zmdi-check-circle active"></i>
                                                   <i class="zmdi zmdi-check-circle"></i>
                                                   </span>
                                                   </label>
                                                </div>
                                             </li>
                                             <li>
                                                <div class="h-checkbox entertosend leftbox">
                                                   <input id="filterstar3" type="checkbox">
                                                   <label for="filterstar3">
                                                   <span class="checks-holder">
                                                   <i class="zmdi zmdi-check-circle active"></i>
                                                   <i class="zmdi zmdi-check-circle active"></i>
                                                   <i class="zmdi zmdi-check-circle active"></i>
                                                   <i class="zmdi zmdi-check-circle"></i>
                                                   <i class="zmdi zmdi-check-circle"></i>
                                                   </span>
                                                   </label>
                                                </div>
                                             </li>
                                             <li>
                                                <div class="h-checkbox entertosend leftbox">
                                                   <input id="filterstar4" type="checkbox">
                                                   <label for="filterstar4">
                                                   <span class="checks-holder">
                                                   <i class="zmdi zmdi-check-circle active"></i>
                                                   <i class="zmdi zmdi-check-circle active"></i>
                                                   <i class="zmdi zmdi-check-circle"></i>
                                                   <i class="zmdi zmdi-check-circle"></i>
                                                   <i class="zmdi zmdi-check-circle"></i>
                                                   </span>
                                                   </label>
                                                </div>
                                             </li>
                                             <li>
                                                <div class="h-checkbox entertosend leftbox">
                                                   <input id="filterstar5" type="checkbox">
                                                   <label for="filterstar5">
                                                   <span class="checks-holder">
                                                   <i class="zmdi zmdi-check-circle active"></i>
                                                   <i class="zmdi zmdi-check-circle"></i>
                                                   <i class="zmdi zmdi-check-circle"></i>
                                                   <i class="zmdi zmdi-check-circle"></i>
                                                   <i class="zmdi zmdi-check-circle"></i>
                                                   </span>
                                                   </label>
                                                </div>
                                             </li>
                                          </ul>
                                       </div>
                                       <div class="srow">
                                          <h6>Nightly Price</h6>
                                          <!--<div class="range-slider price-slider">
                                             <input type="text" class="amount" readonly
                                             <div class="slider-range"></div>
                                             <div class="min-value">$500</div>
                                             <div class="max-value">$10,000</div>
                                             </div>-->
                                       </div>
                                       <div class="srow">
                                          <h6>Distance from</h6>
                                          <div class="sliding-middle-out anim-area underlined fullwidth">
                                             <select class="select2" tabindex="-1" >
                                                <option>City Center</option>
                                                <option>Palace</option>
                                                <option>Bus Station</option>
                                                <option>Railway Station</option>
                                             </select>
                                          </div>
                                          <!--<div class="range-slider distance-slider">
                                             <input type="text" class="amount" readonly 
                                             <div class="slider-range"></div>
                                             <div class="min-value">$500</div>
                                             <div class="max-value">$10,000</div>
                                             </div>-->
                                       </div>
                                       <div class="srow">
                                          <h6>Amenities</h6>
                                          <ul class="ul-amenities">
                                             <li>
                                                <a href="javascript:void(0)"><img src="images/amenity-spa.png" /><span>Spa</span></a>
                                             </li>
                                             <li>
                                                <a href="javascript:void(0)"><img src="images/amenity-beach.png" /><span>Beach</span></a>
                                             </li>
                                             <li>
                                                <a href="javascript:void(0)"><img src="images/amenity-wifi.png" /><span>Wifi</span></a>
                                             </li>
                                             <li>
                                                <a href="javascript:void(0)"><img src="images/amenity-breakfast.png" /><span>Breakfast</span></a>
                                             </li>
                                             <li>
                                                <a href="javascript:void(0)"><img src="images/amenity-pool.png" /><span>Pool</span></a>
                                             </li>
                                             <li>
                                                <a href="javascript:void(0)"><img src="images/amenity-spa.png" /><span>Spa</span></a>
                                             </li>
                                             <li>
                                                <a href="javascript:void(0)"><img src="images/amenity-beach.png" /><span>Beach</span></a>
                                             </li>
                                             <li>
                                                <a href="javascript:void(0)"><img src="images/amenity-breakfast.png" /><span>Breakfast</span></a>
                                             </li>
                                          </ul>
                                       </div>
                                       <div class="btn-holder">
                                          <a href="javascript:void(0)" class="btn btn-primary btn-md waves-effect waves-light">Reset Filters</a>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="content-box bshadow">
                                    <div class="cbox-title nborder">
                                       Search Hotels
                                    </div>
                                    <div class="cbox-desc">
                                       <div class="srow">
                                          <h6>Search hotel by name</h6>
                                          <div class="sliding-middle-out anim-area underlined fullwidth">
                                             <input type="text" placeholder="Enter hotel name" class="fullwidth">
                                          </div>
                                       </div>
                                       <div class="srow">
                                          <h6>Search hotel by address</h6>
                                          <div class="sliding-middle-out anim-area underlined fullwidth">
                                             <input type="text" placeholder="Enter hotel address" class="fullwidth">
                                          </div>
                                       </div>
                                       <div class="btn-holder">
                                          <a href="javascript:void(0)" class="btn btn-primary btn-md waves-effect waves-light">Search</a>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="content-box">
                           <div class="mbl-tabnav">
                              <a href="javascript:void(0)" onclick="openDirectTab('places-all')"><i class="mdi mdi-arrow-left"></i></a> 
                              <h6>Hotels</h6>
                           </div>
                           <div class="placesection redsection">
                              <div class="cbox-desc hotels-page np">
                                 <div class="tcontent-holder moreinfo-outer">
                                    <div class="top-stuff top-graybg" id="all-hotels">
                                       <div class="more-actions">
                                          <div class="sorting left">
                                             <label>Sort by</label>
                                             <div class="select-holder">
                                                <select class="select2" tabindex="-1" >
                                                   <option>Pricing</option>
                                                   <option>Ratings</option>
                                                </select>
                                             </div>
                                          </div>
                                          <ul class="tabs tabsnew text-right">
                                             <li class="tab"><a href="javascript:void(0)" class="manageMap" onclick="openMapSection(this)"><i class="zmdi zmdi-pin"></i>Map</a></li>
                                             <li class="tab"><a href="javascript:void(0)" onclick="openListSection(this)"><i class="zmdi zmdi-view-list-alt zmdi-hc-lg"></i>List</a></li>
                                          </ul>
                                       </div>
                                       <h6>23 hotels found in <span><?=$st_nm_S?></span></h6>
                                    </div>
                                    <div class="places-content-holder">
                                       <div class="map-holder">
                                          <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3110.3465133386144!2d-9.167423685010494!3d38.77868997958898!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xd193295d5b45545%3A0x3f9e7b6a5f00e12c!2sPerta!5e0!3m2!1sen!2sin!4v1481089901870" width="600" height="450" frameborder="0" allowfullscreen></iframe>
                                          <a href="javascript:void(0)" class="overlay" onclick="expandMap(this,'#all-hotels')"></a>
                                          <a href="javascript:void(0)" class="closelink" onclick="shrinkMap(this)"><i class="mdi mdi-close"></i> Close</a>
                                       </div>
                                       <div class="list-holder">
                                          <div class="hotel-list">
                                             <ul>
                                                <li>
                                                   <div class="hotel-li expandable-holder dealli mobilelist">
                                                      <div class="summery-info">
                                                         <div class="imgholder"><img src="images/hotel-demo.png" /></div>
                                                         <div class="descholder">
                                                            <a href="javascript:void(0)" class="expand-link" onclick="mng_expandable(this,'hasClose')">
                                                               <h4>Hyatt Regency Dubai Creek
                                                               </h4>
                                                               <div class="clear"></div>
                                                               <div class="reviews-link">
                                                                  <span class="review-count">54 reviews</span>
                                                                  <span class="checks-holder">
                                                                  <i class="zmdi zmdi-check-circle active"></i>
                                                                  <i class="zmdi zmdi-check-circle active"></i>
                                                                  <i class="zmdi zmdi-check-circle active"></i>
                                                                  <i class="zmdi zmdi-check-circle active"></i>
                                                                  <i class="zmdi zmdi-check-circle active"></i>
                                                                  <label>Excellent - 88/100</label>
                                                                  </span>
                                                               </div>
                                                               <span class="address">Dubai, Dubai(Emirates), United Arab Emirates</span>
                                                               <span class="distance-info">2.2 miles to City center</span>
                                                               <div class="more-holder">
                                                                  <div class="tagging" onclick="explandTags(this)">
                                                                     Popular with:
                                                                     <span>point of interest</span>
                                                                     <span>establishment</span>
                                                                  </div>
                                                               </div>
                                                            </a>
                                                            <div class="info-action">
                                                               <span class="stars-holder">
                                                               <i class="mdi mdi-star active"></i>
                                                               <i class="mdi mdi-star active"></i>
                                                               <i class="mdi mdi-star active"></i>
                                                               <i class="mdi mdi-star active"></i>
                                                               <i class="mdi mdi-star active"></i>
                                                               </span>
                                                               <div class="clear"></div>
                                                               <span class="sitename">booking.com</span>
                                                               <div class="clear"></div>
                                                               <span class="price">JOD 184*</span>
                                                               <div class="clear"></div>
                                                               <a href="javascript:void(0)" class="deal-btn waves-effect waves-light">Book Now <i class="mdi mdi-chevron-right"></i></a>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div class="expandable-area">
                                                         <a href="javascript:void(0)" class="shrink-link" onclick="mng_expandable(this,'closeIt')"><i class="mdi mdi-close"></i> Close</a>
                                                         <div class="clear"></div>
                                                         <div class="explandable-tabs">
                                                            <ul class="tabs tabsnew subtab-menu">
                                                               <li class="tab"><a class="" href="#subtab-details">Details</a></li>
                                                               <li class="tab"><a href="#subtab-review">Reviews</a></li>
                                                               <li class="tab"><a data-which="photo" href="#subtab-photos" data-tab="subtab-photos">Photos</a></li>
                                                               <!--<li class="tab"><a data-toggle="tab" href="#subtab-amenities">Amenities</a></li>-->
                                                            </ul>
                                                            <div class="tab-content">
                                                               <div id="subtab-details" class="">
                                                                  <div class="subdetail-box">
                                                                     <div class="infoholder">
                                                                        <div class="descholder">
                                                                           <div class="more-holder">
                                                                              <ul class="infoul">
                                                                                 <li>
                                                                                    <i class="zmdi zmdi-pin"></i>
                                                                                    132 Brick Lane | E1 6RU, <?=$st_nm_S?> E1 6RU, <?=$st_nm_S?>
                                                                                 </li>
                                                                                 <li>
                                                                                    <i class="mdi mdi-phone"></i>
                                                                                    +44 20 7247 8210
                                                                                 </li>
                                                                                 <li>
                                                                                    <i class="mdi mdi-earth"></i>
                                                                                    http://www.yourwebsite.com
                                                                                 </li>
                                                                                 <li>
                                                                                    <i class="mdi mdi-clock-outline"></i>
                                                                                    Mon-Fri : 12:00 PM - 10:00 AM
                                                                                 </li>
                                                                                 <li>
                                                                                    <i class="mdi mdi-certificate "></i>
                                                                                    Ranked #1 in <?=$st_nm_S?> Hotels
                                                                                 </li>
                                                                              </ul>
                                                                              <div class="tagging" onclick="explandTags(this)">
                                                                                 Popular with:
                                                                                 <span>point of interest</span>
                                                                                 <span>establishment</span>
                                                                              </div>
                                                                           </div>
                                                                        </div>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                               <div id="subtab-review" class="">
                                                                  <div class="reviews-summery">
                                                                     <div class="reviews-people">
                                                                        <ul>
                                                                           <li>
                                                                              <div class="reviewpeople-box">
                                                                                 <div class="imgholder"><img src="images/people-3.png" /></div>
                                                                                 <div class="descholder">
                                                                                    <h6>Kelly Mark <span>about 2 weeks ago</span></h6>
                                                                                    <div class="stars-holder">
                                                                                       <img src="images/filled-star.png" />
                                                                                       <img src="images/filled-star.png" />
                                                                                       <img src="images/filled-star.png" />
                                                                                       <img src="images/blank-star.png" />
                                                                                       <img src="images/blank-star.png" />
                                                                                    </div>
                                                                                    <div class="clear"></div>
                                                                                    <p>We enjoyed the lounge and bar at the Ritz where you are offered many choices for drinks and some pretty elaborate looking dishes of food as well.</p>
                                                                                 </div>
                                                                              </div>
                                                                           </li>
                                                                           <li>
                                                                              <div class="reviewpeople-box">
                                                                                 <div class="imgholder"><img src="images/people-2.png" /></div>
                                                                                 <div class="descholder">
                                                                                    <h6>John Davior <span>about 8 months ago</span></h6>
                                                                                    <div class="stars-holder">
                                                                                       <img src="images/filled-star.png" />
                                                                                       <img src="images/filled-star.png" />
                                                                                       <img src="images/filled-star.png" />
                                                                                       <img src="images/filled-star.png" />
                                                                                       <img src="images/blank-star.png" />
                                                                                    </div>
                                                                                    <div class="clear"></div>
                                                                                    <p>If you want a fancy London experience than The Ritz is where you need to go! At least budget for High Tea!</p>
                                                                                 </div>
                                                                              </div>
                                                                           </li>
                                                                           <li>
                                                                              <div class="reviewpeople-box">
                                                                                 <div class="imgholder"><img src="images/people-1.png" /></div>
                                                                                 <div class="descholder">
                                                                                    <h6>Joe Doe <span>about 11 months ago</span></h6>
                                                                                    <div class="stars-holder">
                                                                                       <img src="images/filled-star.png" />
                                                                                       <img src="images/filled-star.png" />
                                                                                       <img src="images/filled-star.png" />
                                                                                       <img src="images/blank-star.png" />
                                                                                       <img src="images/blank-star.png" />
                                                                                    </div>
                                                                                    <div class="clear"></div>
                                                                                    <p>I am not at all sure this is the best hotel in London, but it does deserve the reputation as one of the most glamourous.</p>
                                                                                 </div>
                                                                              </div>
                                                                           </li>
                                                                        </ul>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                               <div id="subtab-photos" class=" subtab-photos">
                                                                  <div class="photo-gallery">
                                                                     <div class="img-preview">
                                                                        <img src="images/post-img1.jpg" />
                                                                     </div>
                                                                     <div class="thumbs-img">
                                                                        <ul>
                                                                           <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img class="himg" src="images/post-img1.jpg" /></a></li>
                                                                           <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img src="images/post-img2.jpg" class="himg" /></a></li>
                                                                           <li><a href="javascript:void(0)" onclick="previewImage(this)" class="vimg-box"><img src="images/post-img3.jpg" class="vimg" /></a></li>
                                                                           <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img src="images/post-img4.jpg" class="himg" /></a></li>
                                                                           <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img src="images/post-img5.jpg" class="himg" /></a></li>
                                                                           <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img class="himg" src="images/post-img1.jpg" /></a></li>
                                                                           <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img src="images/post-img2.jpg" class="himg" /></a></li>
                                                                           <li><a href="javascript:void(0)" onclick="previewImage(this)" class="vimg-box"><img src="images/post-img3.jpg" class="vimg" /></a></li>
                                                                           <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img src="images/post-img4.jpg" class="himg" /></a></li>
                                                                           <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img src="images/post-img5.jpg" class="himg" /></a></li>
                                                                        </ul>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </li>
                                                <li>
                                                   <div class="hotel-li expandable-holder dealli">
                                                      <div class="summery-info">
                                                         <div class="imgholder"><img src="images/hotel-demo.png" /></div>
                                                         <div class="descholder">
                                                            <a href="javascript:void(0)" class="expand-link" onclick="mng_expandable(this,'hasClose')">
                                                               <h4>Hyatt Regency Dubai Creek
                                                               </h4>
                                                               <div class="clear"></div>
                                                               <div class="reviews-link">
                                                                  <span class="review-count">54 reviews</span>
                                                                  <span class="checks-holder">
                                                                  <i class="zmdi zmdi-check-circle active"></i>
                                                                  <i class="zmdi zmdi-check-circle active"></i>
                                                                  <i class="zmdi zmdi-check-circle active"></i>
                                                                  <i class="zmdi zmdi-check-circle active"></i>
                                                                  <i class="zmdi zmdi-check-circle active"></i>
                                                                  <label>Excellent - 88/100</label>
                                                                  </span>
                                                               </div>
                                                               <span class="address">Dubai, Dubai(Emirates), United Arab Emirates</span>
                                                               <span class="distance-info">2.2 miles to City center</span>
                                                               <div class="more-holder">
                                                                  <div class="tagging" onclick="explandTags(this)">
                                                                     Popular with:
                                                                     <span>point of interest</span>
                                                                     <span>establishment</span>
                                                                  </div>
                                                               </div>
                                                            </a>
                                                            <div class="info-action">
                                                               <span class="stars-holder">
                                                               <i class="mdi mdi-star active"></i>
                                                               <i class="mdi mdi-star active"></i>
                                                               <i class="mdi mdi-star active"></i>
                                                               <i class="mdi mdi-star active"></i>
                                                               <i class="mdi mdi-star active"></i>
                                                               </span>
                                                               <div class="clear"></div>
                                                               <span class="sitename">booking.com</span>
                                                               <div class="clear"></div>
                                                               <span class="price">JOD 184*</span>
                                                               <div class="clear"></div>
                                                               <a href="javascript:void(0)" class="deal-btn">Book Now <i class="mdi mdi-chevron-right"></i></a>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div class="expandable-area">
                                                         <a href="javascript:void(0)" class="shrink-link" onclick="mng_expandable(this,'closeIt')"><i class="mdi mdi-close"></i> Close</a>
                                                         <div class="clear"></div>
                                                         <div class="explandable-tabs">
                                                            <ul class="tabs tabsnew subtab-menu">
                                                               <li class="active tab"><a href="#subtab-details">Details</a></li>
                                                               <li class="tab"><a href="#subtab-reviews">Reviews</a></li>
                                                               <li><a aria-expanded="false" data-toggle="tab" data-which="photo" href="#subtab-photos" data-tab="subtab-photos">Photos</a></li>
                                                               <li><a aria-expanded="false" data-toggle="tab" href="#subtab-amenities">Amenities</a></li>
                                                            </ul>
                                                            <div class="tab-content">
                                                               <div id="subtab-details" class="tab-pane fade active in">
                                                                  <div class="subdetail-box">
                                                                     <div class="infoholder">
                                                                        <div class="descholder">
                                                                           <div class="more-holder">
                                                                              <ul class="infoul">
                                                                                 <li>
                                                                                    <i class="zmdi zmdi-pin"></i>
                                                                                    132 Brick Lane | E1 6RU, <?=$st_nm_S?> E1 6RU, <?=$st_nm_S?>
                                                                                 </li>
                                                                                 <li>
                                                                                    <i class="mdi mdi-phone"></i>
                                                                                    +44 20 7247 8210
                                                                                 </li>
                                                                                 <li>
                                                                                    <i class="mdi mdi-earth"></i>
                                                                                    http://www.yourwebsite.com
                                                                                 </li>
                                                                                 <li>
                                                                                    <i class="mdi mdi-clock-outline"></i>
                                                                                    Mon-Fri : 12:00 PM - 10:00 AM
                                                                                 </li>
                                                                                 <li>
                                                                                    <i class="mdi mdi-certificate "></i>
                                                                                    Ranked #1 in <?=$st_nm_S?> Hotels
                                                                                 </li>
                                                                              </ul>
                                                                              <div class="tagging" onclick="explandTags(this)">
                                                                                 Popular with:
                                                                                 <span>point of interest</span>
                                                                                 <span>establishment</span>
                                                                              </div>
                                                                           </div>
                                                                        </div>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                               <div id="subtab-reviews" class="tab-pane fade">
                                                                  <div class="reviews-summery">
                                                                     <div class="reviews-people">
                                                                        <ul>
                                                                           <li>
                                                                              <div class="reviewpeople-box">
                                                                                 <div class="imgholder"><img src="images/people-3.png" /></div>
                                                                                 <div class="descholder">
                                                                                    <h6>Kelly Mark <span>about 2 weeks ago</span></h6>
                                                                                    <div class="stars-holder">
                                                                                       <img src="images/filled-star.png" />
                                                                                       <img src="images/filled-star.png" />
                                                                                       <img src="images/filled-star.png" />
                                                                                       <img src="images/blank-star.png" />
                                                                                       <img src="images/blank-star.png" />
                                                                                    </div>
                                                                                    <div class="clear"></div>
                                                                                    <p>We enjoyed the lounge and bar at the Ritz where you are offered many choices for drinks and some pretty elaborate looking dishes of food as well.</p>
                                                                                 </div>
                                                                              </div>
                                                                           </li>
                                                                           <li>
                                                                              <div class="reviewpeople-box">
                                                                                 <div class="imgholder"><img src="images/people-2.png" /></div>
                                                                                 <div class="descholder">
                                                                                    <h6>John Davior <span>about 8 months ago</span></h6>
                                                                                    <div class="stars-holder">
                                                                                       <img src="images/filled-star.png" />
                                                                                       <img src="images/filled-star.png" />
                                                                                       <img src="images/filled-star.png" />
                                                                                       <img src="images/filled-star.png" />
                                                                                       <img src="images/blank-star.png" />
                                                                                    </div>
                                                                                    <div class="clear"></div>
                                                                                    <p>If you want a fancy London experience than The Ritz is where you need to go! At least budget for High Tea!</p>
                                                                                 </div>
                                                                              </div>
                                                                           </li>
                                                                           <li>
                                                                              <div class="reviewpeople-box">
                                                                                 <div class="imgholder"><img src="images/people-1.png" /></div>
                                                                                 <div class="descholder">
                                                                                    <h6>Joe Doe <span>about 11 months ago</span></h6>
                                                                                    <div class="stars-holder">
                                                                                       <img src="images/filled-star.png" />
                                                                                       <img src="images/filled-star.png" />
                                                                                       <img src="images/filled-star.png" />
                                                                                       <img src="images/blank-star.png" />
                                                                                       <img src="images/blank-star.png" />
                                                                                    </div>
                                                                                    <div class="clear"></div>
                                                                                    <p>I am not at all sure this is the best hotel in London, but it does deserve the reputation as one of the most glamourous.</p>
                                                                                 </div>
                                                                              </div>
                                                                           </li>
                                                                        </ul>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                               <div id="subtab-photos" class="tab-pane fade subtab-photos">
                                                                  <div class="photo-gallery">
                                                                     <div class="img-preview">
                                                                        <img src="images/post-img1.jpg" />
                                                                     </div>
                                                                     <div class="thumbs-img">
                                                                        <ul>
                                                                           <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img class="himg" src="images/post-img1.jpg" /></a></li>
                                                                           <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img src="images/post-img2.jpg" class="himg" /></a></li>
                                                                           <li><a href="javascript:void(0)" onclick="previewImage(this)" class="vimg-box"><img src="images/post-img3.jpg" class="vimg" /></a></li>
                                                                           <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img src="images/post-img4.jpg" class="himg" /></a></li>
                                                                           <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img src="images/post-img5.jpg" class="himg" /></a></li>
                                                                           <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img class="himg" src="images/post-img1.jpg" /></a></li>
                                                                           <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img src="images/post-img2.jpg" class="himg" /></a></li>
                                                                           <li><a href="javascript:void(0)" onclick="previewImage(this)" class="vimg-box"><img src="images/post-img3.jpg" class="vimg" /></a></li>
                                                                           <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img src="images/post-img4.jpg" class="himg" /></a></li>
                                                                           <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img src="images/post-img5.jpg" class="himg" /></a></li>
                                                                        </ul>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                               <div id="subtab-amenities" class="tab-pane fade">
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </li>
                                                <li>
                                                   <div class="hotel-li expandable-holder dealli">
                                                      <div class="summery-info">
                                                         <div class="imgholder"><img src="images/hotel-demo.png" /></div>
                                                         <div class="descholder">
                                                            <a href="javascript:void(0)" class="expand-link" onclick="mng_expandable(this,'hasClose')">
                                                               <h4>Hyatt Regency Dubai Creek
                                                               </h4>
                                                               <div class="clear"></div>
                                                               <div class="reviews-link">
                                                                  <span class="review-count">54 reviews</span>
                                                                  <span class="checks-holder">
                                                                  <i class="zmdi zmdi-check-circle active"></i>
                                                                  <i class="zmdi zmdi-check-circle active"></i>
                                                                  <i class="zmdi zmdi-check-circle active"></i>
                                                                  <i class="zmdi zmdi-check-circle active"></i>
                                                                  <i class="zmdi zmdi-check-circle active"></i>
                                                                  <label>Excellent - 88/100</label>
                                                                  </span>
                                                               </div>
                                                               <span class="address">Dubai, Dubai(Emirates), United Arab Emirates</span>
                                                               <span class="distance-info">2.2 miles to City center</span>
                                                               <div class="more-holder">
                                                                  <div class="tagging" onclick="explandTags(this)">
                                                                     Popular with:
                                                                     <span>point of interest</span>
                                                                     <span>establishment</span>
                                                                  </div>
                                                               </div>
                                                            </a>
                                                            <div class="info-action">
                                                               <span class="stars-holder">
                                                               <i class="mdi mdi-star active"></i>
                                                               <i class="mdi mdi-star active"></i>
                                                               <i class="mdi mdi-star active"></i>
                                                               <i class="mdi mdi-star active"></i>
                                                               <i class="mdi mdi-star active"></i>
                                                               </span>
                                                               <div class="clear"></div>
                                                               <span class="sitename">booking.com</span>
                                                               <div class="clear"></div>
                                                               <span class="price">JOD 184*</span>
                                                               <div class="clear"></div>
                                                               <a href="javascript:void(0)" class="deal-btn">Book Now <i class="mdi mdi-chevron-right"></i></a>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div class="expandable-area">
                                                         <a href="javascript:void(0)" class="shrink-link" onclick="mng_expandable(this,'closeIt')"><i class="mdi mdi-close"></i> Close</a>
                                                         <div class="clear"></div>
                                                         <div class="explandable-tabs">
                                                            <ul class="tabs tabsnew subtab-menu">
                                                               <li class="active tab"><a href="#subtab-details">Details</a></li>
                                                               <li class="tab"><a href="#subtab-reviews">Reviews</a></li>
                                                               <li><a aria-expanded="false" data-toggle="tab" data-which="photo" href="#subtab-photos" data-tab="subtab-photos">Photos</a></li>
                                                               <li><a aria-expanded="false" data-toggle="tab" href="#subtab-amenities">Amenities</a></li>
                                                            </ul>
                                                            <div class="tab-content">
                                                               <div id="subtab-details" class="tab-pane fade active in">
                                                                  <div class="subdetail-box">
                                                                     <div class="infoholder">
                                                                        <div class="descholder">
                                                                           <div class="more-holder">
                                                                              <ul class="infoul">
                                                                                 <li>
                                                                                    <i class="zmdi zmdi-pin"></i>
                                                                                    132 Brick Lane | E1 6RU, <?=$st_nm_S?> E1 6RU, <?=$st_nm_S?>
                                                                                 </li>
                                                                                 <li>
                                                                                    <i class="mdi mdi-phone"></i>
                                                                                    +44 20 7247 8210
                                                                                 </li>
                                                                                 <li>
                                                                                    <i class="mdi mdi-earth"></i>
                                                                                    http://www.yourwebsite.com
                                                                                 </li>
                                                                                 <li>
                                                                                    <i class="mdi mdi-clock-outline"></i>
                                                                                    Mon-Fri : 12:00 PM - 10:00 AM
                                                                                 </li>
                                                                                 <li>
                                                                                    <i class="mdi mdi-certificate "></i>
                                                                                    Ranked #1 in <?=$st_nm_S?> Hotels
                                                                                 </li>
                                                                              </ul>
                                                                              <div class="tagging" onclick="explandTags(this)">
                                                                                 Popular with:
                                                                                 <span>point of interest</span>
                                                                                 <span>establishment</span>
                                                                              </div>
                                                                           </div>
                                                                        </div>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                               <div id="subtab-reviews" class="tab-pane fade">
                                                                  <div class="reviews-summery">
                                                                     <div class="reviews-people">
                                                                        <ul>
                                                                           <li>
                                                                              <div class="reviewpeople-box">
                                                                                 <div class="imgholder"><img src="images/people-3.png" /></div>
                                                                                 <div class="descholder">
                                                                                    <h6>Kelly Mark <span>about 2 weeks ago</span></h6>
                                                                                    <div class="stars-holder">
                                                                                       <img src="images/filled-star.png" />
                                                                                       <img src="images/filled-star.png" />
                                                                                       <img src="images/filled-star.png" />
                                                                                       <img src="images/blank-star.png" />
                                                                                       <img src="images/blank-star.png" />
                                                                                    </div>
                                                                                    <div class="clear"></div>
                                                                                    <p>We enjoyed the lounge and bar at the Ritz where you are offered many choices for drinks and some pretty elaborate looking dishes of food as well.</p>
                                                                                 </div>
                                                                              </div>
                                                                           </li>
                                                                           <li>
                                                                              <div class="reviewpeople-box">
                                                                                 <div class="imgholder"><img src="images/people-2.png" /></div>
                                                                                 <div class="descholder">
                                                                                    <h6>John Davior <span>about 8 months ago</span></h6>
                                                                                    <div class="stars-holder">
                                                                                       <img src="images/filled-star.png" />
                                                                                       <img src="images/filled-star.png" />
                                                                                       <img src="images/filled-star.png" />
                                                                                       <img src="images/filled-star.png" />
                                                                                       <img src="images/blank-star.png" />
                                                                                    </div>
                                                                                    <div class="clear"></div>
                                                                                    <p>If you want a fancy London experience than The Ritz is where you need to go! At least budget for High Tea!</p>
                                                                                 </div>
                                                                              </div>
                                                                           </li>
                                                                           <li>
                                                                              <div class="reviewpeople-box">
                                                                                 <div class="imgholder"><img src="images/people-1.png" /></div>
                                                                                 <div class="descholder">
                                                                                    <h6>Joe Doe <span>about 11 months ago</span></h6>
                                                                                    <div class="stars-holder">
                                                                                       <img src="images/filled-star.png" />
                                                                                       <img src="images/filled-star.png" />
                                                                                       <img src="images/filled-star.png" />
                                                                                       <img src="images/blank-star.png" />
                                                                                       <img src="images/blank-star.png" />
                                                                                    </div>
                                                                                    <div class="clear"></div>
                                                                                    <p>I am not at all sure this is the best hotel in London, but it does deserve the reputation as one of the most glamourous.</p>
                                                                                 </div>
                                                                              </div>
                                                                           </li>
                                                                        </ul>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                               <div id="subtab-photos" class="tab-pane fade subtab-photos">
                                                                  <div class="photo-gallery">
                                                                     <div class="img-preview">
                                                                        <img src="images/post-img1.jpg" />
                                                                     </div>
                                                                     <div class="thumbs-img">
                                                                        <ul>
                                                                           <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img class="himg" src="images/post-img1.jpg" /></a></li>
                                                                           <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img src="images/post-img2.jpg" class="himg" /></a></li>
                                                                           <li><a href="javascript:void(0)" onclick="previewImage(this)" class="vimg-box"><img src="images/post-img3.jpg" class="vimg" /></a></li>
                                                                           <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img src="images/post-img4.jpg" class="himg" /></a></li>
                                                                           <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img src="images/post-img5.jpg" class="himg" /></a></li>
                                                                           <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img class="himg" src="images/post-img1.jpg" /></a></li>
                                                                           <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img src="images/post-img2.jpg" class="himg" /></a></li>
                                                                           <li><a href="javascript:void(0)" onclick="previewImage(this)" class="vimg-box"><img src="images/post-img3.jpg" class="vimg" /></a></li>
                                                                           <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img src="images/post-img4.jpg" class="himg" /></a></li>
                                                                           <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img src="images/post-img5.jpg" class="himg" /></a></li>
                                                                        </ul>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                               <div id="subtab-amenities" class="tab-pane fade">
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </li>
                                             </ul>
                                             <div class="pagination">
                                                <div class="link-holder">
                                                   <a href="javascript:void(0)"><i class="mdi mdi-arrow-left-bold-circle"></i> Prev</a>
                                                </div>
                                                <div class="link-holder">
                                                   <a href="javascript:void(0)">Next <i class="mdi mdi-arrow-right-bold-circle"></i></a>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="moreinfo-box">
                                       <a href="javascript:void(0)" onclick="closePlacesMoreInfo(this)" class="backarrow"><i class="mdi mdi-arrow-left-bold-circle"></i></a>
                                       <div class="infoholder nice-scroll">
                                          <div class="imgholder"><img src="images/hotel1.png" /></div>
                                          <div class="descholder">
                                             <h4>The Guest House</h4>
                                             <div class="clear"></div>
                                             <div class="reviews-link">
                                                <span class="checks-holder">
                                                <i class="mdi mdi-star active"></i>
                                                <i class="mdi mdi-star active"></i>
                                                <i class="mdi mdi-star active"></i>
                                                <i class="mdi mdi-star active"></i>
                                                <i class="mdi mdi-star"></i>
                                                <label>34 Reviews</label>
                                                </span>
                                             </div>
                                             <span class="distance-info">Middle Eastem &amp; African, Mediterranean</span>
                                             <div class="clear"></div>
                                             <div class="more-holder">
                                                <ul class="infoul">
                                                   <li>
                                                      <i class="zmdi zmdi-pin"></i>
                                                      132 Brick Lane | E1 6RU, <?=$st_nm_S?> E1 6RU, <?=$st_nm_S?>
                                                   </li>
                                                   <li>
                                                      <i class="mdi mdi-phone"></i>
                                                      +44 20 7247 8210
                                                   </li>
                                                   <li>
                                                      <i class="mdi mdi-earth"></i>
                                                      http://www.yourwebsite.com
                                                   </li>
                                                   <li>
                                                      <i class="mdi mdi-clock-outline"></i>
                                                      Today, 12:00 PM - 12:00 AM
                                                   </li>
                                                   <li>
                                                      <i class="mdi mdi-certificate "></i>
                                                      Ranked #1 in <?=$st_nm_S?> Hotels
                                                   </li>
                                                </ul>
                                                <div class="tagging" onclick="explandTags(this)">
                                                   Popular with:
                                                   <span>Budget</span>
                                                   <span>Foodies</span>
                                                   <span>Family</span>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>

                  </div>
               </div>
               <?php include('common/gen_wall_col.php'); ?>
            </div>
         </div>
         </div>
         </div>
         </div>

         </div>

         <!-- compose tool box modal -->
         <div id="compose_discus" class="modal compose_tool_box post-popup custom_modal main_modal new-wall-post set_re_height compose_discus_popup">
          <div class="hidden_header">
              <div class="content_header">
                  <button class="close_span cancel_poup waves-effect">
                  <i class="mdi mdi-close mdi-20px compose_discard_popup"></i>
                  </button>
                  <p class="modal_header_xs">Write Post</p>
                  <a type="button" class="post_btn action_btn post_btn_xs close_modal waves-effect"  onclick="verify()">Post</a>
              </div>
          </div>
          <div class="modal-content">
              <div class="new-post active">
                  <div class="top-stuff">
                      <!--<div class="side-user">-->
                      <div class="postuser-info">
                          <span class="img-holder"><img class="circle" src="images/demo-profile.jpg" /></span>
                          <div class="desc-holder">
                              <span class="profile_name">Nimish Parekh</span>
                              <label id="tag_person" class="tag_person_new"></label>
                              <div class="public_dropdown_container">
                                  <a class="dropdown_text dropdown-button-left normalpostcreateprivacylabel" href="javascript:void(0)" onclick="privacymodal(this)" data-modeltag="normalpostcreateprivacylabel" data-fetch="no" data-label="normalpost">
                                  <span id="post_privacy2" class="post_privacy_label">Public</span>
                                  <i class="zmdi zmdi-caret-down"></i>
                                  </a>
                              </div>
                          </div>
                      </div>
                      <div class="settings-icon">
                          <a class="dropdown-button "  href="javascript:void(0)" data-activates="newpost_settings">
                          <i class="zmdi zmdi-more"></i>
                          </a>
                          <ul id="newpost_settings" class="dropdown-content custom_dropdown">
                              <li>
                                  <a href="javascript:void(0)">
                                  <input type="checkbox" id="toolbox_disable_sharing" />
                                  <label for="toolbox_disable_sharing">Disable Sharing</label>
                                  </a>
                              </li>
                              <li>
                                  <a href="javascript:void(0)">
                                  <input type="checkbox" id="toolbox_disable_comments" />
                                  <label for="toolbox_disable_comments">Disable Comments</label>
                                  </a>
                              </li>
                              <li>
                                  <a  onclick="clearPost()">Clear Post</a>
                              </li>
                          </ul>
                      </div>
                  </div>
                  <div class="clear"></div>
                  <div class="scroll_div">
                      <div class="npost-content">
                          <div class="post-mcontent">
                              <div class="npost-title title_post_container">                                    
                                  <input type="text" class="title" placeholder="Your tip title">                                 
                              </div>
                              <div class="clear"></div>
                              <div class="desc">
                                  <textarea id="new_post_comment" placeholder="What's new?" class="materialize-textarea comment_textarea new_post_comment"></textarea>
                              </div>
                              <div class="post-photos">
                                  <div class="img-row">
                                  </div>
                              </div>
                              <div class="post-tag">
                                  <div class="areatitle">With</div>
                                  <div class="areadesc">
                                      <input type="text" class="ptag" placeholder="Who are you with?"/>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
          <div class="modal-footer">
              <div class="new-post active">
                  <div class="post-bcontent">
                      <div class="footer_icon_container">
                          <button class="comment_footer_icon waves-effect" id="compose_uploadphotomodalAction">
                          <i class="zmdi zmdi-camera"></i>
                          </button>
                          <button class="comment_footer_icon waves-effect" id="compose_addpersonAction">
                          <i class="zmdi zmdi-account"></i>
                          </button>
                          <button class="comment_footer_icon compose_titleAction waves-effect" id="compose_titleAction">
                          <img src="images/addtitleBl.png">
                          </button>
                      </div>
                      <div class="public_dropdown_container_xs">
                          <a class="dropdown_text dropdown-button-left normalpostcreateprivacylabel" href="javascript:void(0)" onclick="privacymodal(this)" data-modeltag="normalpostcreateprivacylabel" data-fetch="no" data-label="normalpost">
                          <span id="post_privacy2" class="post_privacy_label">Public</span>
                          <i class="zmdi zmdi-caret-down"></i>
                          </a>
                      </div>
                      <div class="post-bholder">
                          <div class="post-loader"><img src="images/home-loader.gif"/></div>
                          <div class="hidden_xs">
                              <a href="javascript:void(0)" class="btngen-center-align close_modal open_discard_modal waves-effect">cancel</a>
                              <a href="javascript:void(0)" class="btngen-center-align waves-effect btn-flat disabled submit">Post</a>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
         </div>
         <!-- Discuss-->
  
         <div id="compose_mapmodal" class="modal map_modal compose_inner_modal modalxii_level1 map_modalUniq">
         <?php include('common/map_modal.php'); ?>
         </div>
         <?php include('common/addperson_popup.php'); ?>
         <?php include('common/add_photo_popup.php'); ?>
         <!-- Add Photo-->
         <!-- compose Comment modal -->
         <div id="comment_modal_xs" class="modal tbpost_modal custom_modal split-page main_modal cust-pop dicrease-popup-compose compose_Comment_Action">
         <div class="modal_content_container">
         <div class="modal_content_child modal-content">
         <div class="popup-title ">
            <button class="hidden_close_span close_span waves-effect">
            <i class="mdi mdi-close mdi-20px compose_discard_popup"></i>
            </button>           
            <h3>All Comments</h3>
            <a type="button" class="item_done crop_done hidden_close_span waves-effect custom_close" href="javascript:void(0)">Done</a>
            <a type="button" class="item_done crop_done comment-close custom_close waves-effect" href="javascript:void(0)"><i class="mdi mdi-close"></i></a>
         </div>
         <div class="custom_modal_content modal_content" id="createpopup">
            <div class="comment-box-tab profile-tab">
               <div class="comment-poup-box detail-box">
                  <div class="content-holder main-holder">
                     <div class="summery">
                        <div class="dsection bborder expandable-holder expanded">
                           <div class="form-area expandable-area post-holder">
                              <div class="post-more">
                                 <a href="javascript:void(0)" class="view-morec">View more comments</a>
                                 <span class="total-comments">3 of 7</span>
                              </div>
                              <div class="post-comments">
                                 <div class="pcomments">
                                    <div class="pcomment-earlier">
                                       <div class="pcomment-holder">
                                          <div class="pcomment main-comment">
                                             <div class="img-holder">
                                                <div class="profiletipholder">
                                                   <span class="profile-tooltip">
                                                   <img class="circle" src="images/demo-profile.jpg"/>
                                                   </span>
                                                   <span class="profiletooltip_content slidingpan-holder">
                                                      <div class="profile-tip dis-none">
                                                         <div class="profile-tip-avatar">
                                                            <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                                 
                                                            <div class="sliding-pan location-span">
                                                               <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                            </div>
                                                         </div>
                                                         <div class="profile-tip-name">
                                                            <a href="javascript:void(0)">Adel Hasanat</a>
                                                         </div>
                                                         <div class="profile-tip-info">
                                                            <div class="profiletip-icon">
                                                               <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                            </div>
                                                            <div class="profiletip-icon">
                                                               <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                            </div>
                                                            <div class="profiletip-icon">
                                                               <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class=”mdi mdi-eye”></i></span></a>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </span>
                                                </div>
                                             </div>
                                             <div class="desc-holder">
                                                <div class="normal-mode">
                                                   <div class="desc">
                                                      <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                                      <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                                   </div>
                                                   <div class="comment-stuff">
                                                      <div class="more-opt">
                                                         <span class="likeholder">
                                                         <span class="like-tooltip">
                                                         <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>">
                                                         <i class="zmdi zmdi-thumb-up"></i>
                                                         </a>
                                                         </span>
                                                         </span>
                                                         <a href="javascript:void(0)" class="pa-reply reply-comment"><span>icon</span></a>
                                                         <a class="dropdown-button more_btn" href="javascript:void(0);" data-activates="comment_ecit32">                               <i class="zmdi zmdi-hc-2x zmdi-more-vert"></i>
                                                         </a>
                                                         <ul id="comment_ecit32" class="dropdown-content custom_dropdown">
                                                            <li><a class="edit-comment" href="javascript:void(0)">Edit</a></li>
                                                            <li><a class="delete-comment" href="javascript:void(0)">Delete</a></li>
                                                         </ul>
                                                      </div>
                                                      <div class="less-opt">
                                                         <div class="timestamp">8h</div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="edit-mode">
                                                   <div class="desc">
                                                      <textarea class="editcomment-tt materialize-textarea mb0 md_textarea descinput" id="ec-1">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                                      <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close"></i></a>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="clear"></div>
                                          <div class="comment-reply-holder comment-addreply">
                                             <div class="addnew-comment valign-wrapper comment-reply">
                                                <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                                                <div class="desc-holder">                                   
                                                   <textarea class="materialize-textarea mb0 md_textarea descinput">Write a reply...</textarea>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="pcomment-holder">
                                          <div class="pcomment main-comment">
                                             <div class="img-holder">
                                                <div class="profiletipholder">
                                                   <span class="profile-tooltip">
                                                   <img class="circle" src="images/demo-profile.jpg"/>
                                                   </span>
                                                   <span class="profiletooltip_content slidingpan-holder">
                                                      <div class="profile-tip dis-none">
                                                         <div class="profile-tip-avatar">
                                                            <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                                 
                                                            <div class="sliding-pan location-span">
                                                               <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                            </div>
                                                         </div>
                                                         <div class="profile-tip-name">
                                                            <a href="javascript:void(0)">Adel Hasanat</a>
                                                         </div>
                                                         <div class="profile-tip-info">
                                                            <div class="profiletip-icon">
                                                               <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                            </div>
                                                            <div class="profiletip-icon">
                                                               <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                            </div>
                                                            <div class="profiletip-icon">
                                                               <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class=”mdi mdi-eye”></i></span></a>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </span>
                                                </div>
                                             </div>
                                             <div class="desc-holder">
                                                <div class="normal-mode">
                                                   <div class="desc">
                                                      <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                                      <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                                   </div>
                                                   <div class="comment-stuff">
                                                      <div class="more-opt">
                                                         <span class="likeholder">
                                                         <span class="like-tooltip">
                                                         <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>">
                                                         <i class="zmdi zmdi-thumb-up"></i>
                                                         </a>
                                                         </span>
                                                         </span>    
                                                         <a href="javascript:void(0)" class="pa-reply reply-comment"><span>icon</span></a>
                                                         <a class="dropdown-button more_btn" href="javascript:void(0);" data-activates="comment_ecit33">                               <i class="zmdi zmdi-hc-2x zmdi-more-vert"></i>
                                                         </a>
                                                         <ul id="comment_ecit33" class="dropdown-content custom_dropdown">
                                                            <li><a class="edit-comment" href="javascript:void(0)">Edit</a></li>
                                                            <li><a class="delete-comment" href="javascript:void(0)">Delete</a></li>
                                                         </ul>
                                                      </div>
                                                      <div class="less-opt">
                                                         <div class="timestamp">8h</div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="edit-mode">
                                                   <div class="desc">
                                                      <textarea class="editcomment-tt materialize-textarea mb0 md_textarea descinput" id="ec-2">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                                      <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close"></i></a>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="clear"></div>
                                          <div class="comment-reply-holder comment-addreply">
                                             <div class="addnew-comment valign-wrapper comment-reply">
                                                <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                                                <div class="desc-holder">                                   
                                                   <textarea class="materialize-textarea mb0 md_textarea descinput">Write a reply...</textarea>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="pcomment-holder has-comments">
                                       <div class="pcomment main-comment">
                                          <div class="img-holder">
                                             <div class="profiletipholder">
                                                <span class="profile-tooltip">
                                                <img class="circle" src="images/demo-profile.jpg"/>
                                                </span>
                                                <span class="profiletooltip_content slidingpan-holder">
                                                   <div class="profile-tip dis-none">
                                                      <div class="profile-tip-avatar">
                                                         <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                                    
                                                         <div class="sliding-pan location-span">
                                                            <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                         </div>
                                                      </div>
                                                      <div class="profile-tip-name">
                                                         <a href="javascript:void(0)">Adel Hasanat</a>
                                                      </div>
                                                      <div class="profile-tip-info">
                                                         <div class="profiletip-icon">
                                                            <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                         </div>
                                                         <div class="profiletip-icon">
                                                            <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                         </div>
                                                         <div class="profiletip-icon">
                                                            <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class=”mdi mdi-eye”></i></span></a>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </span>
                                             </div>
                                          </div>
                                          <div class="desc-holder">
                                             <div class="normal-mode">
                                                <div class="desc">
                                                   <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                                   <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                                </div>
                                                <div class="comment-stuff">
                                                   <div class="more-opt">
                                                      <span class="likeholder">
                                                      <span class="like-tooltip">
                                                      <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>">
                                                      <i class="zmdi zmdi-thumb-up"></i>
                                                      </a>
                                                      </span>
                                                      </span>
                                                      <a href="javascript:void(0)" class="pa-reply reply-comment"><span>icon</span></a>
                                                      <a class="dropdown-button more_btn" href="javascript:void(0);" data-activates="comment_ecit34" >                              <i class="zmdi zmdi-hc-2x zmdi-more-vert"></i>
                                                      </a>
                                                      <ul id="comment_ecit34" class="dropdown-content custom_dropdown">
                                                         <li><a class="edit-comment" href="javascript:void(0)">Edit</a></li>
                                                         <li><a class="delete-comment" href="javascript:void(0)">Delete</a></li>
                                                      </ul>
                                                   </div>
                                                   <div class="less-opt">
                                                      <div class="timestamp">8h</div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="edit-mode">
                                                <div class="desc">
                                                   <textarea class="editcomment-tt materialize-textarea mb0 md_textarea descinput" id="ec-3">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                                   <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close"></i></a>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="clear"></div>
                                       <div class="comment-reply-holder">
                                          <div class="comments-reply-summery">
                                             <a href="javascript:void(0)" onclick="openReplies(this)">
                                             <i class="mdi mdi-share"></i>
                                             2 Replies                                                  
                                             </a>
                                             <i class="mdi mdi-bullseye dot-i"></i>
                                             Just Now
                                          </div>
                                          <div class="comments-reply-details">
                                             <div class="pcomment comment-reply">
                                                <div class="img-holder">
                                                   <div class="profiletipholder">
                                                      <span class="profile-tooltip">
                                                      <img class="circle" src="images/demo-profile.jpg"/>
                                                      </span>
                                                      <span class="profiletooltip_content slidingpan-holder">
                                                         <div class="profile-tip dis-none">
                                                            <div class="profile-tip-avatar">
                                                               <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                                  
                                                               <div class="sliding-pan location-span">
                                                                  <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                               </div>
                                                            </div>
                                                            <div class="profile-tip-name">
                                                               <a href="javascript:void(0)">Adel Hasanat</a>
                                                            </div>
                                                            <div class="profile-tip-info">
                                                               <div class="profiletip-icon">
                                                                  <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                               </div>
                                                               <div class="profiletip-icon">
                                                                  <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                               </div>
                                                               <div class="profiletip-icon">
                                                                  <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class=”mdi mdi-eye”></i></span></a>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </span>
                                                   </div>
                                                </div>
                                                <div class="desc-holder">
                                                   <div class="normal-mode">
                                                      <div class="desc">
                                                         <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                                         <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                                      </div>
                                                      <div class="comment-stuff">
                                                         <div class="more-opt">
                                                            <span class="likeholder">
                                                            <span class="like-tooltip">
                                                            <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>">
                                                            <i class="zmdi zmdi-thumb-up"></i>
                                                            </a>
                                                            </span>
                                                            </span>
                                                            <a href="javascript:void(0)" class="pa-reply reply-comment"><span>icon</span></a>
                                                            <a class="dropdown-button more_btn" href="javascript:void(0);" data-activates="comment_ecit35">                                <i class="zmdi zmdi-hc-2x zmdi-more-vert"></i>
                                                            </a>
                                                            <ul id="comment_ecit35" class="dropdown-content custom_dropdown">
                                                               <li><a class="edit-comment" href="javascript:void(0)">Edit</a></li>
                                                               <li><a class="delete-comment" href="javascript:void(0)">Delete</a></li>
                                                            </ul>
                                                         </div>
                                                         <div class="less-opt">
                                                            <div class="timestamp">8h</div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div class="edit-mode">
                                                      <div class="desc">
                                                         <textarea class="editcomment-tt materialize-textarea mb0 md_textarea descinput" id="ec-4">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                                         <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close"></i></a>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="pcomment comment-reply">
                                                <div class="img-holder">
                                                   <div class="profiletipholder">
                                                      <span class="profile-tooltip">
                                                      <img class="circle" src="images/demo-profile.jpg"/>
                                                      </span>
                                                      <span class="profiletooltip_content slidingpan-holder">
                                                         <div class="profile-tip dis-none">
                                                            <div class="profile-tip-avatar">
                                                               <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                                  
                                                               <div class="sliding-pan location-span">
                                                                  <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                               </div>
                                                            </div>
                                                            <div class="profile-tip-name">
                                                               <a href="javascript:void(0)">Adel Hasanat</a>
                                                            </div>
                                                            <div class="profile-tip-info">
                                                               <div class="profiletip-icon">
                                                                  <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                               </div>
                                                               <div class="profiletip-icon">
                                                                  <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                               </div>
                                                               <div class="profiletip-icon">
                                                                  <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class=”mdi mdi-eye”></i></span></a>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </span>
                                                   </div>
                                                </div>
                                                <div class="desc-holder">
                                                   <div class="normal-mode">
                                                      <div class="desc">
                                                         <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                                         <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit...</p>
                                                      </div>
                                                      <div class="comment-stuff">
                                                         <div class="more-opt">
                                                            <span class="likeholder">
                                                            <span class="like-tooltip">
                                                            <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>">
                                                            <i class="zmdi zmdi-thumb-up"></i>
                                                            </a>
                                                            </span>
                                                            </span> 
                                                            <a href="javascript:void(0)" class="pa-reply reply-comment"><span>icon</span></a>
                                                            <a class="dropdown-button more_btn" href="javascript:void(0);" data-activates="comment_ecit37">                                <i class="zmdi zmdi-hc-2x zmdi-more-vert"></i>
                                                            </a>
                                                            <ul id="comment_ecit37" class="dropdown-content custom_dropdown">
                                                               <li><a class="edit-comment" href="javascript:void(0)">Edit</a></li>
                                                               <li><a class="delete-comment" href="javascript:void(0)">Delete</a></li>
                                                            </ul>
                                                         </div>
                                                         <div class="less-opt">
                                                            <div class="timestamp">8h</div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div class="edit-mode">
                                                      <div class="desc">
                                                         <textarea class="editcomment-tt materialize-textarea mb0 md_textarea descinput" id="ec-5">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                                         <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close"></i></a>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="comment-reply-holder comment-addreply">
                                          <div class="addnew-comment valign-wrapper comment-reply">
                                             <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                                             <div class="desc-holder">                                  
                                                <textarea class="materialize-textarea mb0 md_textarea descinput">Write a reply...</textarea>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="pcomment-holder">
                                       <div class="pcomment main-comment">
                                          <div class="img-holder">
                                             <div class="profiletipholder">
                                                <span class="profile-tooltip">
                                                <img class="circle" src="images/demo-profile.jpg"/>
                                                </span>
                                                <span class="profiletooltip_content slidingpan-holder">
                                                   <div class="profile-tip dis-none">
                                                      <div class="profile-tip-avatar">
                                                         <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                                    
                                                         <div class="sliding-pan location-span">
                                                            <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                         </div>
                                                      </div>
                                                      <div class="profile-tip-name">
                                                         <a href="javascript:void(0)">Adel Hasanat</a>
                                                      </div>
                                                      <div class="profile-tip-info">
                                                         <div class="profiletip-icon">
                                                            <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                         </div>
                                                         <div class="profiletip-icon">
                                                            <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                         </div>
                                                         <div class="profiletip-icon">
                                                            <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class=”mdi mdi-eye”></i></span></a>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </span>
                                             </div>
                                          </div>
                                          <div class="desc-holder">
                                             <div class="normal-mode">
                                                <div class="desc">
                                                   <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                                   <p class="shorten">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.<a a="" href="javascript:void(0)" class="overlay" onclick="explandReadMore(this)"><span class="readlink">Read More</span></a></p>
                                                </div>
                                                <div class="comment-stuff">
                                                   <div class="more-opt">
                                                      <span class="likeholder">
                                                      <span class="like-tooltip">
                                                      <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>">
                                                      <i class="zmdi zmdi-thumb-up"></i>
                                                      </a>
                                                      </span>
                                                      </span>   
                                                      <a href="javascript:void(0)" class="pa-reply reply-comment"><span>icon</span></a>
                                                      <a class="dropdown-button more_btn" href="javascript:void(0);" data-activates="comment_ecit36" >                              <i class="zmdi zmdi-hc-2x zmdi-more-vert"></i>
                                                      </a>
                                                      <ul id="comment_ecit36" class="dropdown-content custom_dropdown">
                                                         <li><a class="edit-comment" href="javascript:void(0)">Edit</a></li>
                                                         <li><a class="delete-comment" href="javascript:void(0)">Delete</a></li>
                                                      </ul>
                                                   </div>
                                                   <div class="less-opt">
                                                      <div class="timestamp">8h</div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="edit-mode">
                                                <div class="desc">
                                                   <textarea class="editcomment-tt materialize-textarea mb0 md_textarea descinput" id="ec-6">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                                   <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close"></i></a>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="clear"></div>
                                       <div class="comment-reply-holder comment-addreply">
                                          <div class="addnew-comment valign-wrapper comment-reply">
                                             <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                                             <div class="desc-holder">                                  
                                                <textarea class="materialize-textarea mb0 md_textarea descinput">Write a reply...</textarea>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         </div>
</div>
<div class="modal-footer">
<div class="new-post active">
<div class="post-bcontent">
   <div class="addnew-comment valign-wrapper">
      <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
      <div class="desc-holder">                                    
         <textarea class="materialize-textarea mb0 md_textarea descinput" id="comment_txt_1">Write a reply...</textarea>
      </div>
   </div>
</div>
</div>
</div>
<div class="additem_modal_footer modal-footer">
<div class="">
<div class="btn-holder">
   <div class="addnew-comment valign-wrapper">
      <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
      <div class="desc-holder">                                    
         <textarea class="materialize-textarea mb0 md_textarea descinput" id="comment_txt_1">Write a reply...</textarea>
      </div>
   </div>
</div>
</div>
</div>
      <div class="new-post-mobile clear">
         <a class="popup-window composetoolboxAction" href="javascript:void(0)"><i class="mdi mdi-pencil"></i></a>
      </div>
   </div>
</div>
	
</div>	
<!--map modal-->
<div id="compose_mapmodal" class="modal map_modal compose_inner_modal modalxii_level1">
   <?php include('common/map_modal.php'); ?>
</div>

<link rel="stylesheet" type="text/css" href="css/owl.carousel.css">
<script type="text/javascript" src="js/owl.carousel.js"></script>
<script type="text/javascript" src="js/wall.js"></script>
<script type="text/javascript">
$(document).ready(function(){ 
   $w = $(window).width();
   if ( $w > 739) {      
      $(".places-tabs .sub-tabs li a").click(function(){
         $("body").removeClass("remove_scroller");
      }); 
      $(".tabs.icon-menu.tabsnew li a").click(function(){
         $("body").removeClass("remove_scroller");
      }); 
      $(".mbl-tabnav").click(function(){
         $("body").removeClass("remove_scroller");
      }); 
      $(".clicable.viewall-link").click(function(){
         $("body").removeClass("remove_scroller");
      }); 
   } else {
      $(".places-tabs .sub-tabs li a").click(function(){
         $("body").addClass("remove_scroller");
      }); 
      $(".clicable.viewall-link").click(function(){
         $("body").addClass("remove_scroller");
      });         
      $(".tabs.icon-menu.tabsnew li a").click(function(){
         $("body").addClass("remove_scroller");
      }); 
      $(".mbl-tabnav").click(function(){
         $("body").removeClass("remove_scroller");
      });
   }

   $(".header-icon-tabs .tabsnew .tab a").click(function(){
      $(".bottom_tabs").hide();
   });

   $(".places-tabs .tab a").click(function(){
      $(".top_tabs").hide();
   });

   // footer work for places home page only
   $('.footer-section').css('left', '0');
   $w = $(window).width();
   if($w <= 768) {
      $('.main-footer').css({
         'width': '100%',
         'left': '0'
      });
   } else {
      var $_I = $('.places-content.places-all').width();
      var $__I = $('.places-content.places-all').find('.container').width();

      var $half = parseInt($_I) - parseInt($__I);
      $half = parseInt($half) / 2;

      // $('.main-footer').css({
      //    'width': $_I+'px',
      //    'left': '-'+$half+'px'
      // });
   }

   settlePopularPhotos();
});

$(window).resize(function() {
   // footer work for places home page only
   if($('#places-all').hasClass('active')) {
      $('.footer-section').css('left', '0');
      $w = $(window).width();
      if($w <= 768) {
         $('.main-footer').css({
            'width': '100%',
            'left': '0'
         });
      } else {
         var $_I = $('.places-content.places-all').width();
         var $__I = $('.places-content.places-all').find('.container').width();

         var $half = parseInt($_I) - parseInt($__I);
         $half = parseInt($half) / 2;

         $('.main-footer').css({
            'width': $_I+'px',
            'left': '-'+$half+'px'
         });
      }
   }
});

$(window).resize(function() {
    settlePopularPhotos();
});

$(document).on('click', '.tablist .tab a', function(e) {
$href = $(this).attr('href');
$href = $href.replace('#', '');

$('.places-content').removeClass().addClass('places-content '+$href);
   $this = $(this);
});

   // js to initialize justified gallery
   setTimeout(
     function() 
     {
         $('.home-justified-gallery').justifiedGallery({
            lastRow: 'nojustify',
            rowHeight: 220,
            maxRowHeight: 220,
            margins: 10,
            sizeRangeSuffixes: {
                 lt100: '_t',
                 lt240: '_m',
                 lt320: '_n',
                 lt500: '',
                 lt640: '_z',
                 lt1024: '_b'
            }
         });
         $('.home-justified-gallery').css('opacity', '1');
     }, 8000);

function settlePopularPhotos() {
   $width = $(window).width();
   $totalImage = $('.pop-photos-sec').find('.allow-gallery').length;
   if($width <=600) {
   if($totalImage >=3) {
      $('.pop-photos-sec').find('#placebox').html('<div data-src="images/wgallery1.jpg" class="allow-gallery"> <img class="himg" src="images/wgallery1.jpg"/> <a href="javascript:void(0)" class="removeicon"><i class="mdi mdi-delete"></i></a> <div class="caption"> <div class="left"> <span class="title">Big Bird</span> <br> <span class="attribution">By Adel Hasanat</span> </div> <div class="right icons"> <a href="javascript: void(0)" class="prevent-gallery like"><i class="mdi mdi-thumb-up-outline mdi-15px" onclick="toggleIcons(this)"></i> </a><span class="lcount">7</span> <a href="javascript: void(0)" class="prevent-gallery"><i class="mdi mdi-comment-outline mdi-15px cmnt"></i></a><span class="lcount">7</span> </div> </div> </div> <div data-src="images/wgallery2.jpg" class="allow-gallery"> <img class="himg" src="images/wgallery2.jpg"/> <a href="javascript:void(0)" class="removeicon"><i class="mdi mdi-delete"></i></a> <div class="caption"> <div class="left"> <span class="title">Big Bird</span> <br> <span class="attribution">By Adel Hasanat</span> </div> <div class="right icons"> <a href="javascript: void(0)" class="prevent-gallery like"><i class="mdi mdi-thumb-up-outline mdi-15px" onclick="toggleIcons(this)"></i> </a><span class="lcount">7</span> <a href="javascript: void(0)" class="prevent-gallery"><i class="mdi mdi-comment-outline mdi-15px cmnt"></i></a><span class="lcount">7</span> </div> </div> </div>');
      justifiedGalleryinitialize();
      lightGalleryinitialize();
   }
   } else {
   if($totalImage <=2) {
      $('.pop-photos-sec').find('#placebox').html('<div data-src="images/wgallery1.jpg" class="allow-gallery"> <img class="himg" src="images/wgallery1.jpg"/> <a href="javascript:void(0)" class="removeicon"><i class="mdi mdi-delete"></i></a> <div class="caption"> <div class="left"> <span class="title">Big Bird</span> <br> <span class="attribution">By Adel Hasanat</span> </div> <div class="right icons"> <a href="javascript: void(0)" class="prevent-gallery like"><i class="mdi mdi-thumb-up-outline mdi-15px" onclick="toggleIcons(this)"></i> </a><span class="lcount">7</span> <a href="javascript: void(0)" class="prevent-gallery"><i class="mdi mdi-comment-outline mdi-15px cmnt"></i></a><span class="lcount">7</span> </div> </div> </div> <div data-src="images/wgallery2.jpg" class="allow-gallery"> <img class="himg" src="images/wgallery2.jpg"/> <a href="javascript:void(0)" class="removeicon"><i class="mdi mdi-delete"></i></a> <div class="caption"> <div class="left"> <span class="title">Big Bird</span> <br> <span class="attribution">By Adel Hasanat</span> </div> <div class="right icons"> <a href="javascript: void(0)" class="prevent-gallery like"><i class="mdi mdi-thumb-up-outline mdi-15px" onclick="toggleIcons(this)"></i> </a><span class="lcount">7</span> <a href="javascript: void(0)" class="prevent-gallery"><i class="mdi mdi-comment-outline mdi-15px cmnt"></i></a><span class="lcount">7</span> </div> </div> </div> <div data-src="images/wgallery3.jpg" class="allow-gallery hide-on-small-only"> <img class="himg" src="images/wgallery3.jpg"/> <a href="javascript:void(0)" class="removeicon"><i class="mdi mdi-delete"></i></a> <div class="caption"> <div class="left"> <span class="title">Big Bird</span> <br> <span class="attribution">By Adel Hasanat</span> </div> <div class="right icons"> <a href="javascript: void(0)" class="prevent-gallery like"><i class="mdi mdi-thumb-up-outline mdi-15px" onclick="toggleIcons(this)"></i> </a><span class="lcount">7</span> <a href="javascript: void(0)" class="prevent-gallery"><i class="mdi mdi-comment-outline mdi-15px cmnt"></i></a><span class="lcount">7</span> </div> </div> </div>');
      justifiedGalleryinitialize();
      lightGalleryinitialize();

   }
   }
}
</script>

<?php include('common/discard_popup.php'); ?>
<?php include('common/upload_gallery_popup.php'); ?>
<?php include('common/privacymodal.php'); ?>
<?php include('common/custom_modal.php'); ?>
<?php include('common/addcategories_popup.php'); ?> 
<?php include('common/visit_country_popup.php'); ?>

<?php include("script.php"); ?>