<?php include("header.php"); ?>
<div class="floating-icon">
   <div class="scrollup-btnbox anim-side btnbox scrollup-float">
      <div class="scrollup-button float-icon"><span class="icon-holder ispan"><i class="mdi mdi-arrow-up-bold-circle"></i>
</span></div>
   </div>
</div>
<div class="clear"></div>
<?php include("common/leftmenu.php"); ?>

<div class="fixed-layout">
      <div class="main-content with-lmenu sub-page peopleknow-page">
         <div class="combined-column">
            <span class="mob-title">Suggested connections</span>
            <div class="content-box bshadow">
               <div class="cbox-title">                  
                  Suggested connections
               </div>
               <div class="cbox-desc">
                  <div class="connections-grid suggetionbox">
                     <div class="row">
                        <div class="grid-box">
                           <div class="connect-box">
                              <div class="imgholder online-img">
                                 <img src="images/connections-1.png"/>
                                 <span class="online-mark"><i class="zmdi zmdi-check"></i></span>
                              </div>
                              <div class="descholder">
                                 <a href="javascript:void(0)" class="userlink">
                                    <span>Ali musa</span>
                                 </a>
                                 <p class="designation">Suggested by Ali musa</p>
                                 <p class="locationarea">Lives in Ahmedabad,  India</p>
                                 <p class="info">101 connections</p>
                                 <div class="btn-area">
                                    <a href="javascript:void(0)" class="">
                                       <i class="mdi mdi-account-plus"></i>
                                       <i class="dis-none mdi mdi-account-minus"></i>
                                    </a>
                                    <a href="javascript:void(0)" class="tb-pyk-remove"></a>
                                 </div>
                              </div>                                                               
                           </div>
                        </div>
                        <div class="grid-box">
                           <div class="connect-box">
                              <div class="imgholder online-img">
                                 <img src="images/connections-1.png"/>
                                 <span class="online-mark"><i class="zmdi zmdi-check"></i></span>
                              </div>
                              <div class="descholder">
                                 <a href="javascript:void(0)" class="userlink">
                                    <span>Ali musa</span>
                                 </a>
                                 <p class="designation">Suggested by Ali musa</p>
                                 <p class="locationarea">Lives in Ahmedabad,  India</p>
                                 <p class="info">101 connections</p>
                                 <div class="btn-area">
                                    <a href="javascript:void(0)" class="">
                                       <i class="dis-none mdi mdi-account-plus"></i>	
                                       <i class="mdi mdi-account-minus"></i>
                                    </a>
                                    <a href="javascript:void(0)" class="tb-pyk-remove"></a>
                                 </div>
                              </div>                                                               
                           </div>
                        </div>
                        <div class="grid-box">
                           <div class="connect-box">
                              <div class="imgholder">
                                 <img src="images/connections-1.png"/>
                                 <span class="online-mark"><i class="zmdi zmdi-check"></i></span>
                              </div>
                              <div class="descholder">
                                 <a href="javascript:void(0)" class="userlink">
                                    <span>Ali musa</span>
                                 </a>
                                 <p class="designation">Suggested by Ali musa</p>
                                 <p class="locationarea">Lives in Ahmedabad,  India</p>
                                 <p class="info">101 connections</p>
                                 <div class="btn-area">
                                    <div class="showlabel">
                                       <div class="dropdown dropdown-custom req-btn">
                                          <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                             <i class="mdi mdi-chevron-down"></i>
                                          </a>
                                          <ul class="dropdown-menu fetchconnectmenu">
                                             <li><a href="javascript:void(0)">Option 1</a></li>
                                          </ul>
                                       </div>
                                    </div>
                                 </div>
                              </div>                                                               
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <span class="mob-title">Connect requests</span>
            <div class="content-box bshadow">
            <div class="cbox-title">
               Respond to your connect request
            </div>
            <div class="cbox-desc">
               <div class="connections-grid freq">
                  <div class="row">
                     <div class="grid-box">
                        <div class="connect-box">
                           <div class="imgholder">
                              <img src="images/connections-1.png"/>
                              <span class="online-mark"><i class="zmdi zmdi-check"></i></span>
                           </div>
                           <div class="descholder">
                              <a href="javascript:void(0)" class="userlink"><span>Ger Brian</span></a>
                              <span class="designation">Senior Recruiter</span>
                              <span class="info"><?=$st_nm_S?>, Illinos</span>
                              <span class="info">899 Connections</span>
                              <div class="dropdown dropdown-custom mbl-menu">
                                 <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                 <i class="zmdi zmdi-chevron-down"></i>
                                 </a>
                                 <ul class="dropdown-menu">
                                    <li><a class="btn btn-primary btn-sm accept-connect" href="javascript:void(0)">Accept</a></li>
                                    <li><a class="btn btn-primary btn-sm delete-connect btn-gray" href="javascript:void(0)">Delete</a></li>
                                   </ul>
                                 </div>
                              </div>
                              <div class="btn-area btns-holder desc-menu">
                                 <div class="req-btn">
                                    <button class="btn btn-primary btn-sm delete-connect btn-gray">Delete</button>
                                    <button class="btn btn-primary btn-sm accept-connect">Confirm</button>
                                 </div>
                                 <div class="showlabel">
                                    <div class="dropdown dropdown-custom req-btn">
                                     <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                    <i class="mdi mdi-chevron-down"></i>
                                     </a>
                                     <ul class="dropdown-menu fetchconnectmenu">
                                    </ul>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="grid-box">
                        <div class="connect-box">
                           <div class="imgholder">
                              <img src="images/connections-1.png"/>
                              <span class="online-mark"><i class="zmdi zmdi-check"></i></span>
                           </div>
                           <div class="descholder">
                              <a href="javascript:void(0)" class="userlink"><span>Ger Brian</span></a>
                              <span class="designation">Senior Recruiter</span>
                              <span class="info"><?=$st_nm_S?>, Illinos</span>
                              <span class="info">899 Connections</span>
                              <div class="dropdown dropdown-custom mbl-menu">
                                 <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                 <i class="zmdi zmdi-chevron-down"></i>
                                 </a>
                                 <ul class="dropdown-menu">
                                    <li><a class="btn btn-primary btn-sm accept-connect" href="javascript:void(0)">Accept</a></li>
                                    <li><a class="btn btn-primary btn-sm delete-connect btn-gray" href="javascript:void(0)">Delete</a></li>
                                   </ul>
                                 </div>
                              </div>
                              <div class="btn-area btns-holder desc-menu">
                                 <div class="req-btn">
                                    <button class="btn btn-primary btn-sm delete-connect btn-gray">Delete</button>
                                    <button class="btn btn-primary btn-sm accept-connect">Confirm</button>
                                 </div>
                                 <div class="showlabel">
                                    <div class="dropdown dropdown-custom req-btn">
                                     <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                    <i class="mdi mdi-chevron-down"></i>
                                     </a>
                                     <ul class="dropdown-menu fetchconnectmenu">
                                    </ul>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="grid-box">
                        <div class="connect-box">
                           <div class="imgholder">
                              <img src="images/connections-1.png"/>
                              <span class="online-mark"><i class="zmdi zmdi-check"></i></span>
                           </div>
                           <div class="descholder">
                              <a href="javascript:void(0)" class="userlink"><span>Ger Brian</span></a>
                              <span class="designation">Senior Recruiter</span>
                              <span class="info"><?=$st_nm_S?>, Illinos</span>
                              <span class="info">899 Connections</span>
                              <div class="dropdown dropdown-custom mbl-menu">
                                 <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                 <i class="zmdi zmdi-chevron-down"></i>
                                 </a>
                                 <ul class="dropdown-menu">
                                    <li><a class="btn btn-primary btn-sm accept-connect" href="javascript:void(0)">Accept</a></li>
                                    <li><a class="btn btn-primary btn-sm delete-connect btn-gray" href="javascript:void(0)">Delete</a></li>
                                   </ul>
                                 </div>
                              </div>
                              <div class="btn-area btns-holder desc-menu">
                                 <div class="req-btn">
                                    <button class="btn btn-primary btn-sm delete-connect btn-gray">Delete</button>
                                    <button class="btn btn-primary btn-sm accept-connect">Confirm</button>
                                 </div>
                                 <div class="showlabel">
                                    <div class="dropdown dropdown-custom req-btn">
                                     <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                    <i class="mdi mdi-chevron-down"></i>
                                     </a>
                                     <ul class="dropdown-menu fetchconnectmenu">
                                    </ul>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="grid-box">
                        <div class="connect-box">
                           <div class="imgholder">
                              <img src="images/connections-1.png"/>
                              <span class="online-mark"><i class="zmdi zmdi-check"></i></span>
                           </div>
                           <div class="descholder">
                              <a href="javascript:void(0)" class="userlink"><span>Ger Brian</span></a>
                              <span class="designation">Senior Recruiter</span>
                              <span class="info"><?=$st_nm_S?>, Illinos</span>
                              <span class="info">899 Connections</span>
                              <div class="dropdown dropdown-custom mbl-menu">
                                 <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                 <i class="zmdi zmdi-chevron-down"></i>
                                 </a>
                                 <ul class="dropdown-menu">
                                    <li><a class="btn btn-primary btn-sm accept-connect" href="javascript:void(0)">Accept</a></li>
                                    <li><a class="btn btn-primary btn-sm delete-connect btn-gray" href="javascript:void(0)">Delete</a></li>
                                   </ul>
                                 </div>
                              </div>
                              <div class="btn-area btns-holder desc-menu">
                                 <div class="req-btn">
                                    <button class="btn btn-primary btn-sm delete-connect btn-gray">Delete</button>
                                    <button class="btn btn-primary btn-sm accept-connect">Confirm</button>
                                 </div>
                                 <div class="showlabel">
                                    <div class="dropdown dropdown-custom req-btn">
                                     <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                    <i class="mdi mdi-chevron-down"></i>
                                     </a>
                                     <ul class="dropdown-menu fetchconnectmenu">
                                    </ul>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                  </div>
               </div>
            </div>
         </div>
         <span class="mob-title">People you may know</span>
         <div class="content-box bshadow">
            <div class="cbox-title">                  
               People you may know
            </div> 
            <div class="cbox-desc">
               <div class="connections-grid">
                  <div class="row">
                     <div class="grid-box overlay">
                        <div class="connect-box">
                          <div class="imgholder">
                              <img src="images/connections-1.png"/>
                              <span class="online-mark"><i class="zmdi zmdi-check"></i></span>
                          </div>
                          <div class="descholder more-span">
                              <a href="javascript:void(0)" class="userlink">
                                  <span>Ali Musa</span>
                              </a>
                              <span class="info">Lives in Railway Station Road,  Pakistan</span>
                              <span class="info"></span>
                              <span class="requestsent" class="request-sent">Connect request sent</span>       
                              <span class="requestsent" class="request-sent dis-none"></span>
                              <div class="btn-area">
                                  <a href="javascript:void(0)" class="">
                                      <i class="mdi mdi-account-plus"></i>
                                  </a>
                                  <a href="javascript:void(0)" class="tb-pyk-remove"></a>
                              </div>
                           </div>
                        </div>
                     </div>

                     <div class="grid-box overlay">
                        <div class="connect-box">
                          <div class="imgholder">
                              <img src="images/connections-1.png"/>
                              <span class="online-mark"><i class="zmdi zmdi-check"></i></span>
                          </div>
                          <div class="descholder more-span">
                              <a href="javascript:void(0)" class="userlink">
                                  <span>Ali Musa</span>
                              </a>
                              <span class="info">Lives in Railway Station Road,  Pakistan</span>
                              <span class="info"></span>
                              <span class="requestsent" class="request-sent">Connect request sent</span>       
                              <span class="requestsent" class="request-sent dis-none"></span>
                              <div class="btn-area">
                                  <a href="javascript:void(0)" class="">
                                    <i class="mdi mdi-account-plus"></i>
                                  </a>
                                  <a href="javascript:void(0)" class="tb-pyk-remove"></a>
                              </div>
                           </div>
                        </div>
                     </div>

                     <div class="grid-box overlay">
                        <div class="connect-box">
                          <div class="imgholder">
                              <img src="images/connections-1.png"/>
                              <span class="online-mark"><i class="zmdi zmdi-check"></i></span>
                          </div>
                          <div class="descholder more-span">
                              <a href="javascript:void(0)" class="userlink">
                                  <span>Ali Musa</span>
                              </a>
                              <span class="info">Lives in Railway Station Road,  Pakistan</span>
                              <span class="info"></span>
                              <span class="requestsent" class="request-sent">Connect request sent</span>       
                              <span class="requestsent" class="request-sent dis-none"></span>
                              <div class="btn-area">
                                 <div class="showlabel">
                                    <div class="dropdown dropdown-custom req-btn">
                                     <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                    <i class="mdi mdi-chevron-down"></i>
                                     </a>
                                     <ul class="dropdown-menu fetchconnectmenu">
                                    </ul>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <?php include('common/chat.php'); ?>
   </div>
</div>
<?php include("common/footer.php"); ?>
</div>	
<?php include("script.php"); ?>
</body>
</html>