<?php include("header.php"); ?>
<div class="notice-holder fixed">
   <div class="settings-notice">
      <span class="success-note"></span>
      <span class="error-note"></span>
      <span class="info-note"></span>
   </div>
</div>
<div class="floating-icon">
   <div class="scrollup-btnbox anim-side btnbox scrollup-float">
      <div class="scrollup-button float-icon"><span class="icon-holder ispan"><i class="mdi mdi-arrow-up-bold-circle"></i>
</span></div>
   </div>
</div>
<div class="clear"></div>
<div class="container page_container">
   <?php include("common/leftmenu.php"); ?>
   <span class="search-xs-btn" onclick="personSearchSlide()">
   <i class="zmdi zmdi-search"></i>
   </span>  
   <span class="messagesearch-xs-btn" onclick="messageSearchSlide()">
   <i class="zmdi zmdi-search mdi-17px"></i>
   </span>
   <div class="settings-icon globel_setting">
      <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='globel_setting_xs'>
      <i class="zmdi zmdi-more-vert"></i>
      </a>
      <ul id='globel_setting_xs' class='dropdown-content custom_dropdown' onclick=''>
         <li>
            <a href="javascript:void(0)" id="" onclick="new_group_action()" class="i_mute">New group</a>
         </li>
         <li>
            <a href="javascript:void(0)" onclick="MessageRequest()" class="i_mute">New requests</a>
         </li>
         <li>
            <a href="javascript:void(0)" id="" onclick="archieved_action()">Archived</a>
         </li>
         <li>
            <a href="javascript:void(0)" id="" onclick="message_save_action()">Saved messages</a>
         </li>
         <li>
            <a href="javascript:void(0)" id="" onclick="message_setting_action()">Settings</a>
         </li>
         <li>
            <a href="javascript:void(0)" id="" onclick="blocked_contact_action()">Block</a>
         </li>
      </ul>
   </div>
   <div class="settings-icon person_dropdown xs_dropdown">
      <a class="dropdown-button more_btn" href="javascript:void(0)" data-activates="setting_messages_xs">
      <i class="zmdi zmdi-more-vert"></i>
      </a>
      <ul id="setting_messages_xs" class="dropdown-content custom_dropdown">
         <li>
            <a class="contact_info_action">Contact info</a>
         </li>
         <li>
            <a class="group_info_action">Group info</a>
         </li>
         <li>
            <a href="javascript:void(0)" class="mute-setting" onclick="manageMuteConverasion('mute')">Mute Coversation</a>
         </li>
         <li>
            <a>Clear message</a>
         </li>
         <li>
            <a onclick="archiveChat()">Archive chat</a>
         </li>
         <li>
            <a onclick="UnreadMessages()">Mark unread</a>
         </li>
         <li>
            <a onclick="DeleteChat()">Delete Chat</a>
         </li>
         <li>
            <a href="javascript:void(0)" class="block-setting" onclick="manageBlockConverasion('block')">Unblock Messages</a>
         </li>
         <li>
            <a>Exit Group</a>
         </li>
      </ul>
   </div>
   <div class="fixed-layout messages-page-fixed mt-0">
      <div class="main-content with-lmenu messages-page main-page">
         <div class="combined-column">
            <div class="content-box messages-list">
               <div class="cbox-desc">
                  <div class="left-section">
                     <div class="side-user">
                        <div class="side_user_container">
                           <span class="img-holder"><a href="wall.html"><img class="circle" src="images/demo-profile.jpg" /></a></span>
                           <a href="wall.html"><span class="desc-holder">Nimish Parekh</span></a>										
                        </div>
                        <div class="settings-icon globel_setting">
                           <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='globel_setting'>
                           <i class="zmdi zmdi-more-vert"></i>
                           </a>
                           <ul id='globel_setting' class='dropdown-content custom_dropdown'>
                              <li>
                                 <a href="javascript:void(0)" id="" onclick="new_group_action()" class="i_mute">New group</a>
                              </li>
                              <li>
                                 <a href="javascript:void(0)" onclick="MessageRequest()" class="i_mute">New requests</a>
                              </li>
                              <li>
                                 <a href="javascript:void(0)" id="" onclick="archieved_action()">Archived</a>
                              </li>
                              <li>
                                 <a href="javascript:void(0)" id="" onclick="message_save_action()">Saved messages</a>
                              </li>
                              <li>
                                 <a href="javascript:void(0)" id="" onclick="message_setting_action()">Settings</a>
                              </li>
                              <li>
                                 <a href="javascript:void(0)" id="" onclick="blocked_contact_action()">Block</a>
                              </li>
                           </ul>
                        </div>
                     </div>
                     <div class="connections-search search_lg">
                        <div class="fsearch-form">
                           <nav class="message_search">
                              <div class="nav-wrapper">
                                 <form>
                                    <div class="input-field">
                                       <input id="msg_search" type="search" placeholder="Search" required="" class="custom_msg_search"/>
                                       <a href="javascript:void(0)" class="label-icon" for="search">
                                       <i class="zmdi zmdi-search"></i>
                                       </a>
                                    </div>
                                 </form>
                              </div>
                           </nav>
                        </div>
                     </div>
                     <div class="clear"></div>
                     <div class="tab-content">
                        <div class="tab-pane fade main-pane active in" id="messages-inbox">
                           <div class="gloader-holder doneloading">
                              <div class="gloader-content">
                                 <div class="message-userlist nice-scroll">
                                    <ul>
                                       <li>
                                          <a href="javascript:void(0)" class="active" onclick="openMessage(this)" id="user1">
                                             <span class="muser-holder">
                                                <span class="imgholder">
                                                   <img src="images/logo-symbol.png"/>
                                                </span>
                                                <span class="online-dot"></span>
                                                <span class="descholder">
                                                   <h5>Support Team</h5>
                                                   <span class="timestamp">4:45 pm</span>
                                                   <span class="newmsg-count">2</span>
                                                </span>
                                             </span>
                                          </a>
                                       </li>

                                       <li class="unreadmsgli">
                                          <a href="javascript:void(0)" class="active" onclick="openMessage(this)" id="user1">
                                             <span class="muser-holder">
                                                <span class="imgholder dot_block">
                                                   <img src="images/whoisaround-img.png" class="blockprofile_img" />
                                                   <i class="mdi mdi-cancel blockicon_img"></i>
                                                </span>
                                                <span class="online-dot"></span>
                                                <span class="descholder">
                                                   <h5>Ramani Bhadresh</h5>
                                                   <p>Hi</p>
                                                   <span class="timestamp">4:45 pm</span>
                                                   <span class="newmsg-count">2</span>
                                                </span>
                                             </span>
                                          </a>
                                       </li>
                              
                                       <li>
                                          <a href="javascript:void(0)" onclick="openMessage(this)" id="user1">
                                             <span class="muser-holder">
                                                <span class="imgholder dot_away"><img src="images/whoisaround-img.png"/></span>
                                                <span class="online-dot"></span>
                                                <span class="descholder">
                                                   <h5>Adel Google</h5>
                                                   <p>Hi</p>
                                                   <span class="timestamp">4:45 pm</span>
                                                   
                                                </span>
                                             </span>
                                          </a>
                                       </li>
                                       <li>
                                          <a href="javascript:void(0)" onclick="openMessage(this)" id="user1">
                                             <span class="muser-holder">
                                                <span class="imgholder dot_mute"><img src="images/whoisaround-img.png"/></span>
                                                <span class="online-dot"></span>
                                                <span class="descholder">
                                                   <h5>Vipul Patel</h5>
                                                   <p>Hi</p>
                                                   <span class="timestamp">4:45 pm</span>
                                                   
                                                </span>
                                             </span>
                                          </a>
                                       </li>
                                       <li>
                                          <a href="javascript:void(0)" onclick="openMessage(this)" id="user1">
                                             <span class="muser-holder">
                                                <span class="imgholder dot_online">
                                                   <img src="images/whoisaround-img.png"/>
                                                </span>
                                                <span class="online-dot"></span>
                                                <span class="descholder">
                                                   <h5>Vipul Patel</h5>
                                                   <p>Hi</p>
                                                   <span class="timestamp">4:45 pm</span>
                                                   
                                                </span>
                                             </span>
                                          </a>
                                       </li>
                                       <li>
                                          <a href="javascript:void(0)" onclick="openMessage(this)" id="user1">
                                             <span class="muser-holder">
                                                <span class="imgholder dot_online"><img src="images/whoisaround-img.png"/></span>
                                                <span class="online-dot"></span>
                                                <span class="descholder">
                                                   <h5>Vipul Patel</h5>
                                                   <p>Hi</p>
                                                   <span class="timestamp">4:45 pm</span>
                                                   
                                                </span>
                                             </span>
                                          </a>
                                       </li>
                                       <li>
                                          <a href="javascript:void(0)" onclick="openMessage(this)" id="user1">
                                             <span class="muser-holder">
                                                <span class="imgholder dot_online"><img src="images/whoisaround-img.png"/></span>
                                                <span class="online-dot"></span>
                                                <span class="descholder">
                                                   <h5>Vipul Patel</h5>
                                                   <p>Hi</p>
                                                   <span class="timestamp">4:45 pm</span>
                                                   
                                                </span>
                                             </span>
                                          </a>
                                       </li>
                                       <li>
                                          <a href="javascript:void(0)" onclick="openMessage(this)" id="user1">
                                             <span class="muser-holder">
                                                <span class="imgholder dot_online"><img src="images/whoisaround-img.png"/></span>
                                                <span class="online-dot"></span>
                                                <span class="descholder">
                                                   <h5>Vipul Patel</h5>
                                                   <p>Hi</p>
                                                   <span class="timestamp">4:45 pm</span>
                                                   
                                                </span>
                                             </span>
                                          </a>
                                       </li>
                                       <li>
                                          <a href="javascript:void(0)" onclick="openMessage(this)" id="user1">
                                             <span class="muser-holder">
                                                <span class="imgholder dot_online"><img src="images/whoisaround-img.png"/></span>
                                                <span class="online-dot"></span>
                                                <span class="descholder">
                                                   <h5>Vipul Patel</h5>
                                                   <p>Hi</p>
                                                   <span class="timestamp">4:45 pm</span>
                                                   
                                                </span>
                                             </span>
                                          </a>
                                       </li>
                                       <li>
                                          <a href="javascript:void(0)" onclick="openMessage(this)" id="user1">
                                             <span class="muser-holder">
                                                <span class="imgholder dot_online"><img src="images/whoisaround-img.png"/></span>
                                                <span class="online-dot"></span>
                                                <span class="descholder">
                                                   <h5>Vipul Patel</h5>
                                                   <p>Hi</p>
                                                   <span class="timestamp">4:45 pm</span>
                                                   
                                                </span>
                                             </span>
                                          </a>
                                       </li>
                                       <li>
                                          <a href="javascript:void(0)" onclick="openMessage(this)" id="user1">
                                             <span class="muser-holder">
                                                <span class="imgholder dot_online"><img src="images/whoisaround-img.png"/></span>
                                                <span class="online-dot"></span>
                                                <span class="descholder">
                                                   <h5>Vipul Patel</h5>
                                                   <p>Hi</p>
                                                   <span class="timestamp">4:45 pm</span>
                                                   
                                                </span>
                                             </span>
                                          </a>
                                       </li>
                                       <li>
                                          <a href="javascript:void(0)" onclick="openMessage(this)" id="user1">
                                             <span class="muser-holder">
                                                <span class="imgholder dot_online"><img src="images/whoisaround-img.png"/></span>
                                                <span class="online-dot"></span>
                                                <span class="descholder">
                                                   <h5>Vipul Patel</h5>
                                                   <p>Hi</p>
                                                   <span class="timestamp">4:45 pm</span>
                                                   
                                                </span>
                                             </span>
                                          </a>
                                       </li>
                                       <li>
                                          <a href="javascript:void(0)" onclick="openMessage(this)" id="user1">
                                             <span class="muser-holder">
                                                <span class="imgholder dot_online"><img src="images/whoisaround-img.png"/></span>
                                                <span class="online-dot"></span>
                                                <span class="descholder">
                                                   <h5>Vipul Patel</h5>
                                                   <p>Hi</p>
                                                   <span class="timestamp">4:45 pm</span>
                                                   
                                                </span>
                                             </span>
                                          </a>
                                       </li>
                                       <li>
                                          <a href="javascript:void(0)" onclick="openMessage(this)" id="user1">
                                             <span class="muser-holder">
                                                <span class="imgholder dot_online"><img src="images/whoisaround-img.png"/></span>
                                                <span class="online-dot"></span>
                                                <span class="descholder">
                                                   <h5>Vipul Patel</h5>
                                                   <p>Hi</p>
                                                   <span class="timestamp">4:45 pm</span>
                                                   
                                                </span>
                                             </span>
                                          </a>
                                       </li>
                                       <li>
                                          <a href="javascript:void(0)" onclick="openMessage(this)" id="user1">
                                             <span class="muser-holder">
                                                <span class="imgholder dot_online"><img src="images/whoisaround-img.png"/></span>
                                                <span class="online-dot"></span>
                                                <span class="descholder">
                                                   <h5>Vipul Patel</h5>
                                                   <p>Hi</p>
                                                   <span class="timestamp">4:45 pm</span>
                                                   
                                                </span>
                                             </span>
                                          </a>
                                       </li>
                                       <li>
                                          <a href="javascript:void(0)" onclick="openMessage(this)" id="user1">
                                             <span class="muser-holder">
                                                <span class="imgholder dot_online"><img src="images/whoisaround-img.png"/></span>
                                                <span class="online-dot"></span>
                                                <span class="descholder">
                                                   <h5>Vipul Patel</h5>
                                                   <p>Hi</p>
                                                   <span class="timestamp">4:45 pm</span>
                                                   
                                                </span>
                                             </span>
                                          </a>
                                       </li>
                                       <li>
                                          <a href="javascript:void(0)" onclick="openMessage(this)" id="user1">
                                             <span class="muser-holder">
                                                <span class="imgholder dot_online"><img src="images/whoisaround-img.png"/></span>
                                                <span class="online-dot"></span>
                                                <span class="descholder">
                                                   <h5>Vipul Patel</h5>
                                                   <p>Hi</p>
                                                   <span class="timestamp">4:45 pm</span>
                                                   
                                                </span>
                                             </span>
                                          </a>
                                       </li>
                                       <li>
                                          <a href="javascript:void(0)" onclick="openMessage(this)" id="user1">
                                             <span class="muser-holder">
                                                <span class="imgholder dot_online"><img src="images/whoisaround-img.png"/></span>
                                                <span class="online-dot"></span>
                                                <span class="descholder">
                                                   <h5>Vipul Patel</h5>
                                                   <p>Hi</p>
                                                   <span class="timestamp">4:45 pm</span>
                                                   
                                                </span>
                                             </span>
                                          </a>
                                       </li>
                                       <li>
                                          <a href="javascript:void(0)" onclick="openMessage(this)" id="user1">
                                             <span class="muser-holder">
                                                <span class="imgholder dot_online"><img src="images/whoisaround-img.png"/></span>
                                                <span class="online-dot"></span>
                                                <span class="descholder">
                                                   <h5>Vipul Patel</h5>
                                                   <p>Hi</p>
                                                   <span class="timestamp">4:45 pm</span>
                                                   
                                                </span>
                                             </span>
                                          </a>
                                       </li>
                                       <li>
                                          <a href="javascript:void(0)" onclick="openMessage(this)" id="user1">
                                             <span class="muser-holder">
                                                <span class="imgholder dot_online"><img src="images/whoisaround-img.png"/></span>
                                                <span class="online-dot"></span>
                                                <span class="descholder">
                                                   <h5>Vipul Patel</h5>
                                                   <p>Hi</p>
                                                   <span class="timestamp">4:45 pm</span>
                                                   
                                                </span>
                                             </span>
                                          </a>
                                       </li>
                                       <li>
                                          <a href="javascript:void(0)" onclick="openMessage(this)" id="user1">
                                             <span class="muser-holder">
                                                <span class="imgholder dot_online"><img src="images/whoisaround-img.png"/></span>
                                                <span class="online-dot"></span>
                                                <span class="descholder">
                                                   <h5>Vipul Patel</h5>
                                                   <p>Hi</p>
                                                   <span class="timestamp">4:45 pm</span>
                                                   
                                                </span>
                                             </span>
                                          </a>
                                       </li>
                                       <li>
                                          <a href="javascript:void(0)" onclick="openMessage(this)" id="user1">
                                             <span class="muser-holder">
                                                <span class="imgholder dot_online"><img src="images/whoisaround-img.png"/></span>
                                                <span class="online-dot"></span>
                                                <span class="descholder">
                                                   <h5>Vipul Patel</h5>
                                                   <p>Hi</p>
                                                   <span class="timestamp">4:45 pm</span>
                                                   
                                                </span>
                                             </span>
                                          </a>
                                       </li>
                                       
                                       <li>
                                          <a href="javascript:void(0);">show earlier message</a>
                                       </li>
                                    </ul>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class='clear'></div>
                     </div>
                     <a href="javascript:void(0)" onclick="personSearchSlide()" id="addnewchat" class="addnewchat mobile-btn">
                        <i class="zmdi zmdi-edit"></i>
                     </a>
                  </div>
                  <div class="right-section">
                     <div class="topstuff">
                        <div class="msgwindow-name">
                           <div class="imgholder dot_online"><img src="images/whoisaround-img.png"/></div>
                           <span class="desc-holder">Vipul Patel</span>
                           <span class="online-dot"></span>  
                           <span class="person_status">last seen 1hr &nbsp; | </span>
                           <!--<span class="userstatus">&nbsp; i like nonsense it wakes up the brain cells</span>-->
                           <span class="usertime"> 12:57 PM </span>
                           <span class="usercountry">| INDIA </span>											
                        </div>
                        <div class="msgwindow-name msgwindow-group">
                           <div class="imgholder fourpersongroup">
                              <div class="group-person">
                                 <img src="images/whoisaround-img.png"/>
                              </div>
                              <div class="group-person">
                                 <img class="circle" src="images/demo-profile.jpg" />
                              </div>
                              <div class="group-person">
                                 <img class="circle" src="images/demo-profile.jpg" />
                              </div>
                              <div class="group-person">
                                 <img src="images/whoisaround-img.png"/>
                              </div>
                           </div>
                           <span class="desc-holder">
                           <span class="group-name-default">
                           <span class="group-name"> Vipul Patel </span>
                           <a href="javascript:void(0)" id="edit-group-name" class="edit-group-name"><i class="mdi mdi-pencil" ></i></a> 
                           </span>
                           <span class="group-name-editor">
                           <input type="text" class="group-name-input" />
                           <a href="javascript:void(0)" id="edit-group-done" class="edit-group-done"><i class="zmdi zmdi-check" ></i></a>
                           <a href="javascript:void(0)" id="edit-group-cancle" class="edit-group-cancle"><i class="mdi mdi-close" ></i></a>
                           </span>
                           </span> 
                           <span class="group-status"><i class="mdi mdi-chevron-right" ></i>&nbsp; 2 participants</span>
                        </div>
                        <div class="connections-search">
                           <div class="header_right_icon" id="videocamcall">
                              <i class="zmdi zmdi-hc-2x zmdi-videocam i_mute"></i>
                           </div>
                           <div class="header_right_icon">
                              <i class="zmdi zmdi-phone i_mute"></i>
                           </div>
                           <div class="header_right_icon"  onclick="add_person_group()">
                              <i class="zmdi zmdi-account-add i_mute"></i>
                           </div>
                           <div class="header_right_icon add_group_icon"  onclick="create_group()">
                              <i class="zmdi zmdi-accounts-add"></i>
                           </div>
                           <div class="header_right_icon msg_search_icon" id="search_messages_Action">
                              <i class="zmdi zmdi-search"></i>
                           </div>
                           <div class="header_right_icon settings-icon person_dropdown ">
                              <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='setting_messages'>
                              <i class="zmdi zmdi-more-vert"></i>
                              </a>
                              <ul id='setting_messages' class='dropdown-content custom_dropdown'>
                                 <li><a href="javascript:void(0)" class="contact_info_action">Contact info</a></li>
                                 <li><a href="javascript:void(0)" class="group_info_action">Group info</a></li>
                                 <li><a href="javascript:void(0)" onclick="selectMessageBox()">Select messages</a></li>
                                 <li><a href="javascript:void(0)" class="mute-setting" onclick="manageMuteConverasion('mute')">Mute Coversation</a></li>
                                 <li><a href="javascript:void(0)">Clear message</a></li>
                                 <li><a href="javascript:void(0)" onclick="archiveChat()">Archive chat</a></li>
                                 <li><a href="javascript:void(0)" onclick="UnreadMessages()">Mark unread</a></li>
                                 <li><a href="javascript:void(0)" onclick="DeleteChat()">Delete Chat</a></li>
                                 <li><a href="javascript:void(0)" class="block-setting" onclick="manageBlockConverasion('block')">Unblock Messages</a></li>
                                 <li><a href="javascript:void(0)">Exit Group</a></li>
                              </ul>
                           </div>
                        </div>
                        <div class="add-chat-search">
                           <label>To: </label>
                           <select multiple>
                              <option value="AL">Bhadresh</option>
                              <option value="WY">Alap</option>
                              <option value="WY">Markand</option>
                              <option value="WY">Hiral</option>
                           </select>
                           <a href="javascript:void(0)" id="canceladdchat" class="canceladdchat"><i class="mdi mdi-close"></i></a>
                           <a href="javascript:void(0)" id="doneaddchat" class="btn btn-sm btn-primary doneaddchat right">Done</a>
                        </div>
                     </div>
                     <div class="main-msgwindow">
                           <input class="customsearchmain input" name="customsearchmain" type="text" placeholder="Search message text" autocomplete="off">
                           <a href="javascript:void(0)" class="customsearchmain searchicon label-icon" for="search">
                              <i class="zmdi zmdi-search"></i>
                           </a>
                           <div>
                           <h4 class="dateshower"></h4>
                           <div class="photos-thread">
                              <a href="javascript:void(0)" onclick="hideMsgPhotos()" class="backlink"><i class="mdi mdi-menu-left"></i> Back to conversation</a>
                              <div class="photos-area nice-scroll">
                                 <div class="albums-grid images-container">
                                    <div class="row">
                                       <div class="grid-box">
                                          <div class="photo-box">
                                             <div class="imgholder">
                                                <figure>
                                                   <a href="images/post-img1.jpg" data-size="1600x1600" data-med="images/post-img1.jpg" data-med-size="1024x1024" data-author="Folkert Gorter" class="himg-box">
                                                   <img class="himg" src="images/post-img1.jpg">
                                                   </a>																		
                                                </figure>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="grid-box">
                                          <div class="photo-box">
                                             <div class="imgholder">
                                                <figure>
                                                   <a href="images/album2.png" data-size="1600x1600" data-med="images/album2.png" data-med-size="1024x1024" data-author="Folkert Gorter" class="himg-box">
                                                   <img class="himg" src="images/album2.png">
                                                   </a>																			
                                                </figure>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="grid-box">
                                          <div class="photo-box">
                                             <div class="imgholder">
                                                <figure>
                                                   <a href="images/post-img5.jpg" data-size="1600x1600" data-med="images/post-img5.jpg" data-med-size="1024x1024" data-author="Folkert Gorter" class="vimg-box">
                                                   <img class="vimg" src="images/post-img5.jpg">
                                                   </a>
                                                </figure>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="grid-box">
                                          <div class="photo-box">
                                             <div class="imgholder">
                                                <figure>
                                                   <a href="images/post-img4.jpg" data-size="1600x1600" data-med="images/post-img4.jpg" data-med-size="1024x1024" data-author="Folkert Gorter" class="himg-box">
                                                   <img class="himg" src="images/post-img4.jpg">
                                                   </a>
                                                </figure>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="grid-box">
                                          <div class="photo-box">
                                             <div class="imgholder">
                                                <figure>
                                                   <a href="images/album5.png" data-size="1600x1600" data-med="images/album5.png" data-med-size="1024x1024" data-author="Folkert Gorter" class="himg-box">
                                                   <img class="himg" src="images/album5.png">
                                                   </a>																			
                                                </figure>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="grid-box">
                                          <div class="photo-box">
                                             <div class="imgholder">
                                                <figure>
                                                   <a href="images/post-img3.jpg" data-size="1600x1600" data-med="images/post-img3.jpg" data-med-size="1024x1024" data-author="Folkert Gorter" class="vimg-box">
                                                   <img class="vimg" src="images/post-img3.jpg">
                                                   </a>
                                                </figure>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="grid-box">
                                          <div class="photo-box">
                                             <div class="imgholder">
                                                <figure>
                                                   <a href="images/album7.png" data-size="1600x1600" data-med="images/album7.png" data-med-size="1024x1024" data-author="Folkert Gorter" class="Himg-box">
                                                   <img class="Himg" src="images/album7.png">
                                                   </a>
                                                </figure>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="allmsgs-holder">
                              <div class="msg-notice">
                                 <div class="mute-notice">
                                    This conversation has been muted. All the push notifications will be turned off. <a href="javascript:void(0)" onclick="manageMuteConverasion('unmute')">Unmute</a>
                                 </div>
                                 <div class="block-notice">
                                    This conversation is blocked. <a href="javascript:void(0)" onclick="manageBlockConverasion('unblock')">Unblock</a>
                                 </div>
                              </div>
                              <ul class="current-messages">
                                 <li class="mainli active context" id="li-user1">
                                    <div class="msgdetail-list nice-scroll">
                                       <div class="msglist-holder images-container">
                                          <ul class="outer">
                                             <li class="msgli received msg-income">
                                                <div class="checkbox-holder">
                                                   <div class="h-checkbox entertosend msg-checkbox">
                                                      <input type="checkbox" id="test1">
                                                      <label>&nbsp;</label>
                                                   </div>
                                                </div>
                                                <div class="msgdetail-box">
                                                   <div class="imgholder"><img src="images/whoisaround-img.png"/></div>
                                                   <div class="descholder">
                                                      <div class="msg-handle">
                                                         <p>Hi, Adel How are you? <span class="timestamp">1:30 PM</span></p>
                                                         <span class="select_msg_checkbox">
                                                         <input type="checkbox" class="" id="select_msg1" />
                                                         <label for="select_msg1"></label>
                                                         </span>
                                                         <span class="settings-icon">
                                                            <a class="dropdown-button more_btn" href="javascript:void(0)" data-activates="chatmsg1_setting">
                                                            <i class="zmdi zmdi-more"></i>
                                                            </a>
                                                            <ul id="chatmsg1_setting" class="dropdown-content custom_dropdown persondropdown  individiual_chat_setting">
                                                               
                                                               <div class="lds-css ng-scope">
                                                                  <center>
                                                                    <div style="width:100%;height:100%" class="lds-rolling-crntmsgpsndrp">
                                                                      <div class="lststg"></div>
                                                                    </div>
                                                                  </center>
                                                               </div>
         
                                                               <!-- 
                                                               <li>
                                                                  <a>Reply</a>
                                                               </li>
                                                               <li>
                                                                  <a>Forward message</a>
                                                               </li>
                                                               <li>
                                                                  <a>Save message</a>
                                                               </li>
                                                               <li>
                                                                  <a>Delete message</a>
                                                               </li> -->
                                                            </ul>
                                                         </span>
                                                      </div>
                                                      <div class="msg-handle">
                                                         <p>11 I hope you’d have gone through my previous message. We’d like to discuss this project in details so that we can nullify your concerns. <span class="timestamp">1:30 PM</span></p>
                                                         <span class="select_msg_checkbox">
                                                         <input type="checkbox" class="" id="select_msg2" />
                                                         <label for="select_msg2"></label>
                                                         </span>
                                                         <span class="settings-icon">
                                                            <a class="dropdown-button more_btn" href="javascript:void(0)" data-activates="chatmsg2_setting">
                                                            <i class="zmdi zmdi-more"></i>
                                                            </a>
                                                            <ul id="chatmsg2_setting" class="dropdown-content custom_dropdown persondropdown  individiual_chat_setting">
                                                               <li>
                                                                  <a>Reply</a>
                                                               </li>
                                                               <li>
                                                                  <a>Forward message</a>
                                                               </li>
                                                               <li>
                                                                  <a>Save message</a>
                                                               </li>
                                                               <li>
                                                                  <a>Delete message</a>
                                                               </li>
                                                            </ul>
                                                         </span>
                                                      </div>
                                                   </div>
                                                </div>
                                             </li>
                                             <li class="date-divider"><span>31 Jan 2016</span></li>
                                             <li class="msgli received msg-income">
                                                <div class="checkbox-holder">
                                                   <div class="h-checkbox entertosend msg-checkbox">
                                                      <input type="checkbox" id="test1">
                                                      <label>&nbsp;</label>
                                                   </div>
                                                </div>
                                                <div class="msgdetail-box">
                                                   <div class="imgholder"><img src="images/whoisaround-img.png"/></div>
                                                   <div class="descholder">
                                                      <div class="msg-handle">
                                                         <p>
                                                            Hi, Adel How are you?                                                
                                                            <br />
                                                            I hope you’d have gone through my previous message. We’d like to discuss this project in details so that we can nullify your concerns.
                                                            <span class="timestamp">1:30 PM</span>
                                                         </p>
                                                         <span class="select_msg_checkbox">
                                                         <input type="checkbox" class="" id="select_msg3" />
                                                         <label for="select_msg3"></label>
                                                         </span>
                                                         <span class="settings-icon">
                                                            <a class="dropdown-button more_btn" href="javascript:void(0)" data-activates="chatmsg3_setting">
                                                            <i class="zmdi zmdi-more"></i>
                                                            </a>
                                                            <ul id="chatmsg3_setting" class="dropdown-content custom_dropdown persondropdown  individiual_chat_setting">
                                                               <li>
                                                                  <a>Reply</a>
                                                               </li>
                                                               <li>
                                                                  <a>Forward message</a>
                                                               </li>
                                                               <li>
                                                                  <a>Save message</a>
                                                               </li>
                                                               <li>
                                                                  <a>Delete message</a>
                                                               </li>
                                                            </ul>
                                                         </span>
                                                      </div>
                                                   </div>
                                                </div>
                                             </li>
                                             <li class="msgli msg-outgoing">
                                                <div class="checkbox-holder">
                                                   <div class="h-checkbox entertosend msg-checkbox">
                                                      <input type="checkbox" id="test1">
                                                      <label>&nbsp;</label>
                                                   </div>
                                                </div>
                                                <div class="msgdetail-box">
                                                   <div class="descholder">
                                                      <div class="msg-handle">
                                                         <img class="msg-status" src="images/msg-read.png">
                                                         <span class="msg-img">
                                                            <figure>
                                                               <a href="images/msg2.jpg" data-size="1600x1600" data-med="images/msg2.jpg" data-med-size="1024x1024" data-author="Folkert Gorter"><img src="images/msg2.jpg"/></a>
                                                               <a href="javascript:void(0)" class="download"><i class="mdi mdi-download"></i></a>
                                                               <span class="timestamp">1:30 PM</span>
                                                            </figure>
                                                         </span>
                                                         <span class="select_msg_checkbox">
                                                         <input type="checkbox" class="" id="select_msg4" />
                                                         <label for="select_msg4"></label>
                                                         </span>
                                                         <span class="settings-icon">
                                                            <a class="dropdown-button more_btn" href="javascript:void(0)" data-activates="chatmsg4_setting">
                                                            <i class="zmdi zmdi-more"></i>
                                                            </a>
                                                            <ul id="chatmsg4_setting" class="dropdown-content custom_dropdown persondropdown  individiual_chat_setting">
                                                               <li>
                                                                  <a>Reply</a>
                                                               </li>
                                                               <li>
                                                                  <a>Forward message</a>
                                                               </li>
                                                               <li>
                                                                  <a>Save message</a>
                                                               </li>
                                                               <li>
                                                                  <a>Delete message</a>
                                                               </li>
                                                            </ul>
                                                         </span>
                                                      </div>
                                                   </div>
                                                </div>
                                             </li>
                                             <li class="msgli received msg-income">
                                                <div class="checkbox-holder">
                                                   <div class="h-checkbox entertosend msg-checkbox">
                                                      <input type="checkbox" id="test1">
                                                      <label>&nbsp;</label>
                                                   </div>
                                                </div>
                                                <div class="msgdetail-box">
                                                   <div class="imgholder"><img src="images/whoisaround-img.png"/></div>
                                                   <div class="descholder">
                                                      <div class="msg-handle">
                                                         <p>
                                                            Hi, Adel How are you?                                                
                                                            <br />
                                                            I hope you’d have gone through my previous message. We’d like to discuss this project in details so that we can nullify your concerns.
                                                            <span class="timestamp">1:30 PM</span>
                                                         </p>
                                                         <span class="select_msg_checkbox">
                                                         <input type="checkbox" class="" id="select_msg5" />
                                                         <label for="select_msg5"></label>
                                                         </span>
                                                         <span class="settings-icon">
                                                            <a class="dropdown-button more_btn" href="javascript:void(0)" data-activates="chatmsg5_setting">
                                                            <i class="zmdi zmdi-more"></i>
                                                            </a>
                                                            <ul id="chatmsg5_setting" class="dropdown-content custom_dropdown persondropdown  individiual_chat_setting">
                                                               <li>
                                                                  <a>Reply</a>
                                                               </li>
                                                               <li>
                                                                  <a>Forward message</a>
                                                               </li>
                                                               <li>
                                                                  <a>Save message</a>
                                                               </li>
                                                               <li>
                                                                  <a>Delete message</a>
                                                               </li>
                                                            </ul>
                                                         </span>
                                                      </div>
                                                   </div>
                                                </div>
                                             </li>
                                             <li class="date-divider"><span>4 Mar 2016</span></li>
                                             <li class="msgli msg-outgoing">
                                                <div class="checkbox-holder">
                                                   <div class="h-checkbox entertosend msg-checkbox">
                                                      <input type="checkbox" id="test1">
                                                      <label>&nbsp;</label>
                                                   </div>
                                                </div>
                                                <div class="msgdetail-box">
                                                   <div class="descholder">
                                                      <div class="msg-handle">
                                                         <img class="msg-status" src="images/msg-read.png">
                                                         <span class="msg-img">
                                                            <figure><a href="images/msg1.jpg" data-size="1600x1600" data-med="images/msg1.jpg" data-med-size="1024x1024" data-author="Folkert Gorter"><img src="images/msg1.jpg"/></a>
                                                               <a href="javascript:void(0)" class="download"><i class="mdi mdi-download"></i></a>
                                                               <span class="timestamp">11:30 PM</span>
                                                            </figure>
                                                         </span>
                                                         <span class="select_msg_checkbox">
                                                         <input type="checkbox" class="" id="select_msg6" />
                                                         <label for="select_msg6"></label>
                                                         </span>
                                                         <span class="settings-icon">
                                                            <a class="dropdown-button more_btn" href="javascript:void(0)" data-activates="chatmsg6_setting">
                                                            <i class="zmdi zmdi-more"></i>
                                                            </a>
                                                            <ul id="chatmsg6_setting" class="dropdown-content custom_dropdown persondropdown  individiual_chat_setting">
                                                               <li>
                                                                  <a>Reply</a>
                                                               </li>
                                                               <li>
                                                                  <a>Forward message</a>
                                                               </li>
                                                               <li>
                                                                  <a>Save message</a>
                                                               </li>
                                                               <li>
                                                                  <a>Delete message</a>
                                                               </li>
                                                            </ul>
                                                         </span>
                                                      </div>
                                                   </div>
                                                </div>
                                             </li>
                                             <li class="msgli msg-outgoing">
                                                <div class="checkbox-holder">
                                                   <div class="h-checkbox entertosend msg-checkbox">
                                                      <input type="checkbox" id="test1">
                                                      <label>&nbsp;</label>
                                                   </div>
                                                </div>
                                                <div class="msgdetail-box">
                                                   <div class="descholder">
                                                      <div class="msg-handle">
                                                         <p>Wishing you a very happy birthday! <span class="timestamp">1:30 PM</span></p>
                                                         <span class="select_msg_checkbox">
                                                         <input type="checkbox" class="" id="select_msg7" />
                                                         <label for="select_msg7"></label>
                                                         </span>
                                                         <span class="settings-icon">
                                                            <a class="dropdown-button more_btn" href="javascript:void(0)" data-activates="chatmsg7_setting">
                                                            <i class="zmdi zmdi-more"></i>
                                                            </a>
                                                            <ul id="chatmsg7_setting" class="dropdown-content custom_dropdown persondropdown  individiual_chat_setting">
                                                               <li>
                                                                  <a>Reply</a>
                                                               </li>
                                                               <li>
                                                                  <a>Forward message</a>
                                                               </li>
                                                               <li>
                                                                  <a>Save message</a>
                                                               </li>
                                                               <li>
                                                                  <a>Delete message</a>
                                                               </li>
                                                            </ul>
                                                         </span>
                                                      </div>
                                                      <div class="msg-handle">
                                                         <img class="msg-status" src="images/msg-read.png">
                                                         <span class="gift-img">
                                                         <a href="javascript:void(0)" onclick="useGift('(cake)','popular','preview')">
                                                         <i class="mdi mdi-gift"></i>
                                                         </a>
                                                         </span>
                                                         <span class="select_msg_checkbox">
                                                         <input type="checkbox" class="" id="select_msg8" />
                                                         <label for="select_msg8"></label>
                                                         </span>
                                                         <span class="settings-icon">
                                                            <a class="dropdown-button more_btn" href="javascript:void(0)" data-activates="chatmsg8_setting">
                                                            <i class="zmdi zmdi-more"></i>
                                                            </a>
                                                            <ul id="chatmsg8_setting" class="dropdown-content custom_dropdown persondropdown  individiual_chat_setting">
                                                               <li>
                                                                  <a>Reply</a>
                                                               </li>
                                                               <li>
                                                                  <a>Forward message</a>
                                                               </li>
                                                               <li>
                                                                  <a>Save message</a>
                                                               </li>
                                                               <li>
                                                                  <a>Delete message</a>
                                                               </li>
                                                            </ul>
                                                         </span>
                                                      </div>
                                                   </div>
                                                </div>
                                             </li>
                                             <li class="msgli msg-outgoing">
                                                <div class="checkbox-holder">
                                                   <div class="h-checkbox entertosend msg-checkbox">
                                                      <input type="checkbox" id="test1">
                                                      <label>&nbsp;</label>
                                                   </div>
                                                </div>
                                                <div class="msgdetail-box">
                                                   <div class="descholder">
                                                      <div class="msg-handle">
                                                         <p>Wishing you a very happy birthday! <span class="timestamp">1:30 PM</span></p>
                                                         <span class="select_msg_checkbox">
                                                         <input type="checkbox" class="" id="select_msg9" />
                                                         <label for="select_msg9"></label>
                                                         </span>
                                                         <span class="settings-icon">
                                                            <a class="dropdown-button more_btn" href="javascript:void(0)" data-activates="chatmsg9_setting">
                                                            <i class="zmdi zmdi-more"></i>
                                                            </a>
                                                            <ul id="chatmsg9_setting" class="dropdown-content custom_dropdown persondropdown  individiual_chat_setting">
                                                               <li>
                                                                  <a>Reply</a>
                                                               </li>
                                                               <li>
                                                                  <a>Forward message</a>
                                                               </li>
                                                               <li>
                                                                  <a>Save message</a>
                                                               </li>
                                                               <li>
                                                                  <a>Delete message</a>
                                                               </li>
                                                            </ul>
                                                         </span>
                                                      </div>
                                                      <div class="msg-handle">
                                                         <img class="msg-status" src="images/msg-read.png">
                                                         <span class="gift-img">
                                                         <a href="javascript:void(0)" onclick="useGift('(cake)','popular','preview')">
                                                         <i class="mdi mdi-gift"></i>
                                                         </a>
                                                         </span>
                                                         <span class="select_msg_checkbox">
                                                         <input type="checkbox" class="" id="select_msg10" />
                                                         <label for="select_msg10"></label>
                                                         </span>
                                                         <span class="settings-icon">
                                                            <a class="dropdown-button more_btn" href="javascript:void(0)" data-activates="chatmsg10_setting">
                                                            <i class="zmdi zmdi-more"></i>
                                                            </a>
                                                            <ul id="chatmsg10_setting" class="dropdown-content custom_dropdown persondropdown  individiual_chat_setting">
                                                               <li>
                                                                  <a>Reply</a>
                                                               </li>
                                                               <li>
                                                                  <a>Forward message</a>
                                                               </li>
                                                               <li>
                                                                  <a>Save message</a>
                                                               </li>
                                                               <li>
                                                                  <a>Delete message</a>
                                                               </li>
                                                            </ul>
                                                         </span>
                                                      </div>
                                                   </div>
                                                </div>
                                             </li>
                                             <li class="msgli msg-outgoing">
                                                <div class="checkbox-holder">
                                                   <div class="h-checkbox entertosend msg-checkbox">
                                                      <input type="checkbox" id="test1">
                                                      <label>&nbsp;</label>
                                                   </div>
                                                </div>
                                                <div class="msgdetail-box">
                                                   <div class="descholder">
                                                      <div class="msg-handle">
                                                         <p>Wishing you a very happy birthday! <span class="timestamp">1:30 PM</span></p>
                                                         <span class="select_msg_checkbox">
                                                         <input type="checkbox" class="" id="select_msg11" />
                                                         <label for="select_msg11"></label>
                                                         </span>
                                                         <span class="settings-icon">
                                                            <a class="dropdown-button more_btn" href="javascript:void(0)" data-activates="chatmsg11_setting">
                                                            <i class="zmdi zmdi-more"></i>
                                                            </a>
                                                            <ul id="chatmsg11_setting" class="dropdown-content custom_dropdown persondropdown  individiual_chat_setting">
                                                               <li>
                                                                  <a>Reply</a>
                                                               </li>
                                                               <li>
                                                                  <a>Forward message</a>
                                                               </li>
                                                               <li>
                                                                  <a>Save message</a>
                                                               </li>
                                                               <li>
                                                                  <a>Delete message</a>
                                                               </li>
                                                            </ul>
                                                         </span>
                                                      </div>
                                                      <div class="msg-handle">
                                                         <img class="msg-status" src="images/msg-read.png">
                                                         <span class="gift-img">
                                                         <a href="javascript:void(0)" onclick="useGift('(cake)','popular','preview')">
                                                         <i class="mdi mdi-gift"></i>
                                                         </a>
                                                         </span>
                                                         <span class="select_msg_checkbox">
                                                         <input type="checkbox" class="" id="select_msg12" />
                                                         <label for="select_msg12"></label>
                                                         </span>
                                                         <span class="settings-icon">
                                                            <a class="dropdown-button more_btn" href="javascript:void(0)" data-activates="chatmsg12_setting">
                                                            <i class="zmdi zmdi-more"></i>
                                                            </a>
                                                            <ul id="chatmsg12_setting" class="dropdown-content custom_dropdown persondropdown  individiual_chat_setting">
                                                               <li>
                                                                  <a>Reply</a>
                                                               </li>
                                                               <li>
                                                                  <a>Forward message</a>
                                                               </li>
                                                               <li>
                                                                  <a>Save message</a>
                                                               </li>
                                                               <li>
                                                                  <a>Delete message</a>
                                                               </li>
                                                            </ul>
                                                         </span>
                                                      </div>
                                                   </div>
                                                </div>
                                             </li>
                                             <li class="msgli msg-outgoing">
                                                <div class="checkbox-holder">
                                                   <div class="h-checkbox entertosend msg-checkbox">
                                                      <input type="checkbox" id="test1">
                                                      <label>&nbsp;</label>
                                                   </div>
                                                </div>
                                                <div class="msgdetail-box">
                                                   <div class="descholder">
                                                      <div class="msg-handle">
                                                         <p>Wishing you a very happy birthday! <span class="timestamp">1:30 PM</span></p>
                                                         <span class="select_msg_checkbox">
                                                         <input type="checkbox" class="" id="select_msg13" />
                                                         <label for="select_msg13"></label>
                                                         </span>
                                                         <span class="settings-icon">
                                                            <a class="dropdown-button more_btn" href="javascript:void(0)" data-activates="chatmsg13_setting">
                                                            <i class="zmdi zmdi-more"></i>
                                                            </a>
                                                            <ul id="chatmsg13_setting" class="dropdown-content custom_dropdown persondropdown  individiual_chat_setting">
                                                               <li>
                                                                  <a>Reply</a>
                                                               </li>
                                                               <li>
                                                                  <a>Forward message</a>
                                                               </li>
                                                               <li>
                                                                  <a>Save message</a>
                                                               </li>
                                                               <li>
                                                                  <a>Delete message</a>
                                                               </li>
                                                            </ul>
                                                         </span>
                                                      </div>
                                                      <div class="msg-handle">
                                                         <img class="msg-status" src="images/msg-read.png">
                                                         <span class="gift-img">
                                                         <a href="javascript:void(0)" onclick="useGift('(cake)','popular','preview')">
                                                         <i class="mdi mdi-gift"></i>
                                                         </a>
                                                         </span>
                                                         <span class="select_msg_checkbox">
                                                         <input type="checkbox" class="" id="select_msg14" />
                                                         <label for="select_msg14"></label>
                                                         </span>
                                                         <span class="settings-icon">
                                                            <a class="dropdown-button more_btn" href="javascript:void(0)" data-activates="chatmsg14_setting">
                                                            <i class="zmdi zmdi-more"></i>
                                                            </a>
                                                            <ul id="chatmsg14_setting" class="dropdown-content custom_dropdown persondropdown  individiual_chat_setting">
                                                               <li>
                                                                  <a>Reply</a>
                                                               </li>
                                                               <li>
                                                                  <a>Forward message</a>
                                                               </li>
                                                               <li>
                                                                  <a>Save message</a>
                                                               </li>
                                                               <li>
                                                                  <a>Delete message</a>
                                                               </li>
                                                            </ul>
                                                         </span>
                                                      </div>
                                                   </div>
                                                </div>
                                             </li>
                                             <li class="msgli msg-outgoing">
                                                <div class="checkbox-holder">
                                                   <div class="h-checkbox entertosend msg-checkbox">
                                                      <input type="checkbox" id="test1">
                                                      <label>&nbsp;</label>
                                                   </div>
                                                </div>
                                                <div class="msgdetail-box">
                                                   <div class="descholder">
                                                      <div class="msg-handle">
                                                         <p>Wishing you a very happy birthday! <span class="timestamp">1:30 PM</span></p>
                                                         <span class="select_msg_checkbox">
                                                         <input type="checkbox" class="" id="select_msg15" />
                                                         <label for="select_msg15"></label>
                                                         </span>
                                                         <span class="settings-icon">
                                                            <a class="dropdown-button more_btn" href="javascript:void(0)" data-activates="chatmsg15_setting">
                                                            <i class="zmdi zmdi-more"></i>
                                                            </a>
                                                            <ul id="chatmsg15_setting" class="dropdown-content custom_dropdown persondropdown  individiual_chat_setting">
                                                               <li>
                                                                  <a>Reply</a>
                                                               </li>
                                                               <li>
                                                                  <a>Forward message</a>
                                                               </li>
                                                               <li>
                                                                  <a>Save message</a>
                                                               </li>
                                                               <li>
                                                                  <a>Delete message</a>
                                                               </li>
                                                            </ul>
                                                         </span>
                                                      </div>
                                                      <div class="msg-handle">
                                                         <img class="msg-status" src="images/msg-read.png">
                                                         <span class="gift-img">
                                                         <a href="javascript:void(0)" onclick="useGift('(cake)','popular','preview')">
                                                         <i class="mdi mdi-gift"></i>
                                                         </a>
                                                         </span>
                                                         <span class="select_msg_checkbox">
                                                         <input type="checkbox" class="" id="select_msg16" />
                                                         <label for="select_msg16"></label>
                                                         </span>
                                                         <span class="settings-icon">
                                                            <a class="dropdown-button more_btn" href="javascript:void(0)" data-activates="chatmsg16_setting">
                                                            <i class="zmdi zmdi-more"></i>
                                                            </a>
                                                            <ul id="chatmsg16_setting" class="dropdown-content custom_dropdown persondropdown  individiual_chat_setting">
                                                               <li>
                                                                  <a>Reply</a>
                                                               </li>
                                                               <li>
                                                                  <a>Forward message</a>
                                                               </li>
                                                               <li>
                                                                  <a>Save message</a>
                                                               </li>
                                                               <li>
                                                                  <a>Delete message</a>
                                                               </li>
                                                            </ul>
                                                         </span>
                                                      </div>
                                                   </div>
                                                </div>
                                             </li>
                                             <li class="msgli msg-outgoing">
                                                <div class="checkbox-holder">
                                                   <div class="h-checkbox entertosend msg-checkbox">
                                                      <input type="checkbox" id="test1">
                                                      <label>&nbsp;</label>
                                                   </div>
                                                </div>
                                                <div class="msgdetail-box">
                                                   <div class="descholder">
                                                      <div class="handle-msg">
                                                         <p>Wishing you a very happy birthday! <span class="timestamp">1:30 PM</span></p>
                                                         <span class="select_msg_checkbox">
                                                         <input type="checkbox" class="" id="select_msg17" />
                                                         <label for="select_msg17"></label>
                                                         </span>
                                                         <span class="settings-icon">
                                                            <a class="dropdown-button more_btn" href="javascript:void(0)" data-activates="chatmsg17_setting">
                                                            <i class="zmdi zmdi-more"></i>
                                                            </a>
                                                            <ul id="chatmsg17_setting" class="dropdown-content custom_dropdown persondropdown  individiual_chat_setting">
                                                               <li>
                                                                  <a>Reply</a>
                                                               </li>
                                                               <li>
                                                                  <a>Forward message</a>
                                                               </li>
                                                               <li>
                                                                  <a>Save message</a>
                                                               </li>
                                                               <li>
                                                                  <a>Delete message</a>
                                                               </li>
                                                            </ul>
                                                         </span>
                                                      </div>
                                                      <div class="handle-msg">
                                                         <img class="msg-status" src="images/msg-read.png">
                                                         <span class="gift-img">
                                                         <a href="javascript:void(0)" onclick="useGift('(cake)','popular','preview')">
                                                         <i class="mdi mdi-gift"></i>
                                                         </a>
                                                         </span>
                                                         <span class="select_msg_checkbox">
                                                         <input type="checkbox" class="" id="select_msg18" />
                                                         <label for="select_msg18"></label>
                                                         </span>
                                                         <span class="settings-icon">
                                                            <a class="dropdown-button more_btn" href="javascript:void(0)" data-activates="chatmsg18_setting">
                                                            <i class="zmdi zmdi-more"></i>
                                                            </a>
                                                            <ul id="chatmsg18_setting" class="dropdown-content custom_dropdown persondropdown  individiual_chat_setting" >
                                                               <li>
                                                                  <a>Reply</a>
                                                               </li>
                                                               <li>
                                                                  <a>Forward message</a>
                                                               </li>
                                                               <li>
                                                                  <a>Save message</a>
                                                               </li>
                                                               <li>
                                                                  <a>Delete message</a>
                                                               </li>
                                                            </ul>
                                                         </span>
                                                      </div>
                                                   </div>
                                                </div>
                                             </li>
                                             <li class="msgli msg-outgoing">
                                                <div class="checkbox-holder">
                                                   <div class="h-checkbox entertosend msg-checkbox">
                                                      <input type="checkbox" id="test1">
                                                      <label>&nbsp;</label>
                                                   </div>
                                                </div>
                                                <div class="msgdetail-box">
                                                   <div class="descholder">
                                                      <div class="msg-handle">
                                                         <p>Wishing you a very happy birthday! <span class="timestamp">1:30 PM</span></p>
                                                         <span class="select_msg_checkbox">
                                                         <input type="checkbox" class="" id="select_msg19" />
                                                         <label for="select_msg19"></label>
                                                         </span>
                                                         <span class="settings-icon">
                                                            <a class="dropdown-button more_btn" href="javascript:void(0)" data-activates="chatmsg19_setting">
                                                            <i class="zmdi zmdi-more"></i>
                                                            </a>
                                                            <ul id="chatmsg19_setting" class="dropdown-content custom_dropdown persondropdown  individiual_chat_setting">
                                                               <li>
                                                                  <a>Reply</a>
                                                               </li>
                                                               <li>
                                                                  <a>Forward message</a>
                                                               </li>
                                                               <li>
                                                                  <a>Save message</a>
                                                               </li>
                                                               <li>
                                                                  <a>Delete message</a>
                                                               </li>
                                                            </ul>
                                                         </span>
                                                      </div>
                                                      <div class="msg-handle">
                                                         <img class="msg-status" src="images/msg-read.png">
                                                         <span class="gift-img">
                                                         <a href="javascript:void(0)" onclick="useGift('(cake)','popular','preview')">
                                                         <i class="mdi mdi-gift"></i>
                                                         </a>
                                                         </span>
                                                         <span class="select_msg_checkbox">
                                                         <input type="checkbox" class="" id="select_msg20" />
                                                         <label for="select_msg20"></label>
                                                         </span>
                                                         <span class="settings-icon">
                                                            <a class="dropdown-button more_btn" href="javascript:void(0)" data-activates="chatmsg20_setting">
                                                            <i class="zmdi zmdi-more"></i>
                                                            </a>
                                                            <ul id="chatmsg20_setting" class="dropdown-content custom_dropdown persondropdown  individiual_chat_setting">
                                                               <li>
                                                                  <a>Reply</a>
                                                               </li>
                                                               <li>
                                                                  <a>Forward message</a>
                                                               </li>
                                                               <li>
                                                                  <a>Save message</a>
                                                               </li>
                                                               <li>
                                                                  <a>Delete message</a>
                                                               </li>
                                                            </ul>
                                                         </span>
                                                      </div>
                                                   </div>
                                                </div>
                                             </li>
                                             <li class="msgli msg-outgoing">
                                                <div class="checkbox-holder">
                                                   <div class="h-checkbox entertosend msg-checkbox">
                                                      <input type="checkbox" id="test1">
                                                      <label>&nbsp;</label>
                                                   </div>
                                                </div>
                                                <div class="msgdetail-box">
                                                   <div class="descholder">
                                                      <div class="msg-handle">
                                                         <p>Wishing you a very happy birthday! <span class="timestamp">1:30 PM</span></p>
                                                         <span class="select_msg_checkbox">
                                                         <input type="checkbox" class="" id="select_msg21" />
                                                         <label for="select_msg21"></label>
                                                         </span>
                                                         <span class="settings-icon">
                                                            <a class="dropdown-button more_btn" href="javascript:void(0)" data-activates="chatmsg21_setting">
                                                            <i class="zmdi zmdi-more"></i>
                                                            </a>
                                                            <ul id="chatmsg21_setting" class="dropdown-content custom_dropdown persondropdown  individiual_chat_setting">
                                                               <li>
                                                                  <a>Reply</a>
                                                               </li>
                                                               <li>
                                                                  <a>Forward message</a>
                                                               </li>
                                                               <li>
                                                                  <a>Save message</a>
                                                               </li>
                                                               <li>
                                                                  <a>Delete message</a>
                                                               </li>
                                                            </ul>
                                                         </span>
                                                      </div>
                                                      <div class="msg-handle">
                                                         <img class="msg-status" src="images/msg-read.png">
                                                         <span class="gift-img">
                                                         <a href="javascript:void(0)" onclick="useGift('(cake)','popular','preview')">
                                                         <i class="mdi mdi-gift"></i>
                                                         </a>
                                                         </span>
                                                         <span class="select_msg_checkbox">
                                                         <input type="checkbox" class="" id="select_msg22" />
                                                         <label for="select_msg22"></label>
                                                         </span>
                                                         <span class="settings-icon">
                                                            <a class="dropdown-button more_btn" href="javascript:void(0)" data-activates="chatmsg22_setting">
                                                            <i class="zmdi zmdi-more"></i>
                                                            </a>
                                                            <ul id="chatmsg22_setting" class="dropdown-content custom_dropdown persondropdown  individiual_chat_setting">
                                                               <li>
                                                                  <a>Reply</a>
                                                               </li>
                                                               <li>
                                                                  <a>Forward message</a>
                                                               </li>
                                                               <li>
                                                                  <a>Save message</a>
                                                               </li>
                                                               <li>
                                                                  <a>Delete message</a>
                                                               </li>
                                                            </ul>
                                                         </span>
                                                      </div>
                                                   </div>
                                                </div>
                                             </li>
                                             <li class="msgli msg-outgoing">
                                                <div class="checkbox-holder">
                                                   <div class="h-checkbox entertosend msg-checkbox">
                                                      <input type="checkbox" id="test1">
                                                      <label>&nbsp;</label>
                                                   </div>
                                                </div>
                                                <div class="msgdetail-box">
                                                   <div class="descholder">
                                                      <div class="msg-handle">
                                                         <p>Wishing you a very happy birthday! <span class="timestamp">1:30 PM</span></p>
                                                         <span class="select_msg_checkbox">
                                                         <input type="checkbox" class="" id="select_msg23" />
                                                         <label for="select_msg23"></label>
                                                         </span>
                                                         <span class="settings-icon">
                                                            <a class="dropdown-button more_btn" href="javascript:void(0)" data-activates="chatmsg23_setting">
                                                            <i class="zmdi zmdi-more"></i>
                                                            </a>
                                                            <ul id="chatmsg23_setting" class="dropdown-content custom_dropdown persondropdown  individiual_chat_setting">
                                                               <li>
                                                                  <a>Reply</a>
                                                               </li>
                                                               <li>
                                                                  <a>Forward message</a>
                                                               </li>
                                                               <li>
                                                                  <a>Save message</a>
                                                               </li>
                                                               <li>
                                                                  <a>Delete message</a>
                                                               </li>
                                                            </ul>
                                                         </span>
                                                      </div>
                                                      <div class="msg-handle">
                                                         <img class="msg-status" src="images/msg-unread.png">
                                                         <span class="gift-img">
                                                         <a href="javascript:void(0)" onclick="useGift('(cake)','popular','preview')">
                                                         <i class="mdi mdi-gift"></i>
                                                         </a>
                                                         </span>
                                                         <span class="select_msg_checkbox">
                                                         <input type="checkbox" class="" id="select_msg24" />
                                                         <label for="select_msg24"></label>
                                                         </span>
                                                         <span class="settings-icon">
                                                            <a class="dropdown-button more_btn" href="javascript:void(0)" data-activates="chatmsg24_setting">
                                                            <i class="zmdi zmdi-more"></i>
                                                            </a>
                                                            <ul id="chatmsg24_setting" class="dropdown-content custom_dropdown persondropdown  individiual_chat_setting">
                                                               <li>
                                                                  <a>Reply</a>
                                                               </li>
                                                               <li>
                                                                  <a>Forward message</a>
                                                               </li>
                                                               <li>
                                                                  <a>Save message</a>
                                                               </li>
                                                               <li>
                                                                  <a>Delete message</a>
                                                               </li>
                                                            </ul>
                                                         </span>
                                                      </div>
                                                   </div>
                                                </div>
                                             </li>
                                             <li class="msgli msg-outgoing">
                                                <div class="checkbox-holder">
                                                   <div class="h-checkbox entertosend msg-checkbox">
                                                      <input type="checkbox" id="test1">
                                                      <label>&nbsp;</label>
                                                   </div>
                                                </div>
                                                <div class="msgdetail-box">
                                                   <div class="descholder">
                                                      <div class="msg-handle">
                                                         <p>Wishing you a very happy birthday! <span class="timestamp">1:30 PM</span></p>
                                                         <span class="select_msg_checkbox">
                                                         <input type="checkbox" class="" id="select_msg25" />
                                                         <label for="select_msg25"></label>
                                                         </span>
                                                         <span class="settings-icon">
                                                            <a class="dropdown-button more_btn" href="javascript:void(0)" data-activates="chatmsg25_setting">
                                                            <i class="zmdi zmdi-more"></i>
                                                            </a>
                                                            <ul id="chatmsg25_setting" class="dropdown-content custom_dropdown persondropdown  individiual_chat_setting">
                                                               <li>
                                                                  <a>Reply</a>
                                                               </li>
                                                               <li>
                                                                  <a>Forward message</a>
                                                               </li>
                                                               <li>
                                                                  <a>Save message</a>
                                                               </li>
                                                               <li>
                                                                  <a>Delete message</a>
                                                               </li>
                                                            </ul>
                                                         </span>
                                                      </div>
                                                      <div class="msg-handle">
                                                         <img class="msg-status" src="images/msg-sent.png">
                                                         <span class="gift-img">
                                                         <a href="javascript:void(0)" onclick="useGift('(cake)','popular','preview')">
                                                         <i class="mdi mdi-gift"></i>
                                                         </a>
                                                         </span>
                                                         <span class="select_msg_checkbox">
                                                         <input type="checkbox" class="" id="select_msg26" />
                                                         <label for="select_msg26"></label>
                                                         </span>
                                                         <span class="settings-icon">
                                                            <a class="dropdown-button more_btn" href="javascript:void(0)" data-activates="chatmsg26_setting">
                                                            <i class="zmdi zmdi-more"></i>
                                                            </a>
                                                            <ul id="chatmsg26_setting" class="dropdown-content custom_dropdown persondropdown  individiual_chat_setting">
                                                               <li>
                                                                  <a>Reply</a>
                                                               </li>
                                                               <li>
                                                                  <a>Forward message</a>
                                                               </li>
                                                               <li>
                                                                  <a>Save message</a>
                                                               </li>
                                                               <li>
                                                                  <a>Delete message</a>
                                                               </li>
                                                            </ul>
                                                         </span>
                                                      </div>
                                                   </div>
                                                </div>
                                             <li class="msgli msg-outgoing">
                                                <div class="checkbox-holder">
                                                   <div class="h-checkbox entertosend msg-checkbox">
                                                      <input id="test1" type="checkbox">
                                                      <label>&nbsp;</label>
                                                   </div>
                                                </div>
                                                <div class="msgdetail-box">
                                                   <div class="descholder">
                                                      <div class="msg-handle">
                                                         <img class="msg-status" src="images/msg-read.png">
                                                         <span class="msg-img">
                                                            <figure><a href="images/msg1.jpg" data-size="1600x1600" data-med="images/msg1.jpg" data-med-size="1024x1024" data-author="Folkert Gorter"><img src="images/msg1.jpg"></a>
                                                               <a href="javascript:void(0)" class="download"><i class="mdi mdi-download"></i></a>
                                                               <span class="timestamp">11:30 PM</span>
                                                            </figure>
                                                            <div class="overlay">
                                                            </div>
                                                         </span>
                                                         <span class="select_msg_checkbox">
                                                         <input type="checkbox" class="" id="select_msg27" />
                                                         <label for="select_msg27"></label>
                                                         </span>
                                                         <span class="settings-icon">
                                                            <a class="dropdown-button more_btn" href="javascript:void(0)" data-activates="chatmsg27_setting">
                                                            <i class="zmdi zmdi-more"></i>
                                                            </a>
                                                            <ul id="chatmsg27_setting" class="dropdown-content custom_dropdown persondropdown  individiual_chat_setting">
                                                               <li>
                                                                  <a>Reply</a>
                                                               </li>
                                                               <li>
                                                                  <a>Forward message</a>
                                                               </li>
                                                               <li>
                                                                  <a>Save message</a>
                                                               </li>
                                                               <li>
                                                                  <a>Delete message</a>
                                                               </li>
                                                            </ul>
                                                         </span>
                                                      </div>
                                                   </div>
                                                </div>
                                             </li>
                                             </li>
                                          </ul>
                                       </div>
                                    </div>
                                 </li>
                              </ul>
                              <div class="newmessage" id="li-user-blank">
                                 <div class="msgdetail-list nice-scroll" tabindex="6"></div>
                              </div>

                              <div class="hidden-attachment-box hidden">
                                 <div class="innerhidden-attachment-box">
                                    <div class="up">
                                       <div class="wp-location attachmentdiv">
                                          <a href="javascript:void(0)">
                                             <div class="selfdiv">
                                                <center>
                                                   <img src="images/wp-simily.png">
                                                   <div>Video</div>
                                                </center>
                                             </div>
                                          </a>
                                       </div>
                                       <div class="wp-photo attachmentdiv">
                                          <a href="javascript:void(0)">
                                             <div class="selfdiv">
                                                <center>
                                                   <img src="images/wp-camera.png">
                                                   <div>Camera</div>
                                                </center>
                                             </div>
                                          </a>
                                       </div>
                                       <div class="wp-video attachmentdiv">
                                          <a href="javascript:void(0)">
                                             <div class="selfdiv">
                                                <center>
                                                   <img src="images/wp-photovideo.png">
                                                   <div>Photo</div>
                                                   <input type="file" id="attach_file_add" class="dis-none" />
                                                </center>
                                             </div>
                                          </a>
                                       </div>
                                    </div>
                                    <div class="bottom">
                                       <div class="wp-gift attachmentdiv">
                                          <a href="javascript:void(0)" onclick="giftModalAction()">
                                             <div class="selfdiv">
                                                <center>
                                                   <img src="images/wp-gift.png">
                                                   <div>Gift</div>
                                                </center>
                                             </div>
                                          </a>
                                       </div>
                                       <div class="wp-location attachmentdiv">
                                          <a href="javascript:void(0)">
                                             <div class="selfdiv">
                                                <center>
                                                   <img src="images/wp-location.png">
                                                   <div>Location</div>
                                                </center>
                                             </div>
                                          </a>
                                       </div>
                                       <div class="wp-location attachmentdiv">
                                          <a href="javascript:void(0)">
                                             <div class="selfdiv">
                                                <center>
                                                   <img src="images/wp-contact.png">
                                                   <div>Contact</div>
                                                </center>
                                             </div>
                                          </a>
                                       </div>
                                    </div>
                                 </div>
                              </div>

                              <div class="addnew-msg">
                                 <div class="write-msg input-field">
                                    <div class="fixed-action-btn horizontal click-to-toggle attachment_add_icon docustomize">
                                       <a href="javascript:void(0)"><i class="mdi mdi-attachment mdi-rotate-135 prefix"></i></a>
                                       <ul>
                                          <li>
                                             <a href="javascript:void(0)">
                                                <img src="images/wp-location.png">
                                             </a>
                                          </li>
                                          <li>
                                             <a href="javascript:void(0)">
                                                <img src="images/wp-contact.png">
                                             </a>
                                          </li>
                                          <li>
                                             <a href="javascript:void(0)">
                                                <img src="images/wp-photovideo.png">
                                                <input type="file" id="attach_file_add" class="dis-none" />
                                             </a>
                                          </li>
                                          <li>
                                             <a href="javascript:void(0)" onclick="giftModalAction()">
                                                <img src="images/wp-gift.png">
                                             </a>
                                          </li>
                                       </ul>
                                    </div>
                                    <div class="emotion-holder gifticonclick">
                                        <i class="zmdi zmdi-mood" onclick="manageEmotionBox(this,'messages')"></i>
                                        <div class="emotion-box dis-none">
                                          <div class="nice-scroll emotions">
                                             <ul class="emotion-list">
                                             </ul>
                                          </div>
                                        </div>
                                    </div>
                                    <textarea id="inputMessageWall" class="inputMessageWall materialize-textarea" placeholder="Type your message"></textarea>
                                 </div>   
                                 <div class="msg-stuff">
                                    <div class="send-msg">
                                       <button class="btn btn-primary btn-xxs btn-msg-send" onclick="messageSendFromMessage();"><i class="mdi mdi-telegram"></i></button>
                                    </div>
                                 </div>
                              </div>

                              <div class="bottom-stuff">
                                 <h6>Select messages to delete</h6>
                                 <div class="btn-holder">
                                    <a href="javascript:void(0)" class="btn btn-primary btn-sm">Delete</a>
                                    <a href="javascript:void(0)" class="btn btn-primary btn-sm" onclick="hideMsgCheckbox()">Cancel</a>
                                 </div>
                              </div>
                              <div class="selected_messages_box">
                                 <a class="close_selected_messages_box waves-effect" onclick="closeSelectedMessage()">
                                 <i class="mdi mdi-close"></i>
                                 </a>
                                 <p class="selected_msg_number">
                                    <span>0</span>  selected
                                 </p>
                                 <div class="selected_msg_functions">
                                    <a>
                                    <i class="zmdi zmdi-star"></i>
                                    </a>
                                    <a onclick="DeleteChat()">
                                    <i class="zmdi zmdi-delete"></i>
                                    </a>
                                    <a>
                                    <i class="zmdi zmdi-forward"></i>
                                    </a>
                                    <a>
                                    <i class="zmdi zmdi-upload"></i>
                                    </a>
                                 </div>
                              </div>
                           </div>
                           </div>
                        </div>
                     </div>
                  </div> 
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
</div>	

<!--side modal-->
<!--new group modal-->
<div class="new_group_modal side_modal left_side_modal">
   <div class="custom_side_header">
      <span class="slide_out_btn slide_out_right_btn close_side_slider waves-effect">
      <i class="zmdi zmdi-arrow-left"></i>
      </span>
      <h3>New Group</h3>
   </div>
   <div class="side_modal_container side_modal_content new_group_content">
      <span class="big_profile_image">
      <img src="images/whoisaround-img1.png" />
      </span>
      <div class="group_name_form">
         <input placeholder="Group Name" id="group_name" type="text"  />
      </div>
      <span class="btn-floating btn-large bottom_newgroup_btn" onclick="addPerson()">
      <i class="zmdi zmdi-arrow-right"></i>
      </span>
   </div>
</div>
<!--Message request modal-->
<div class="message_request_modal side_modal left_side_modal">
   <div class="custom_side_header">
      <span class="slide_out_btn slide_out_right_btn close_side_slider waves-effect">
      <i class="zmdi zmdi-arrow-left"></i>
      </span>
      <h3>Message Requests</h3>
   </div>
   <div class="side_modal_container side_modal_content">
      <div class="message_request_msg">
         <p>Open a request to get more info about who's messaging you.They won't know you've seen it until you accept.</p>
      </div>
      <div class="message_requst_info">
         <span class="participants_profile">
         <img src="images/whoisaround-img.png" />
         </span>
         <span class="participants_name">
         <span class="day_time">
         Tuesday
         </span>
         <span class="msg_name">Vipul Patel</span>
         <span class="requsted_msg">Hi</span>
         <span class="request_option">
         <a class="request_approve" href="javascript:void(0)">Accept</a>
         <a class="request_reject" href="javascript:void(0)">Decline</a>
         </span>
         </span>
      </div>
   </div>
</div>
<!--create group modal-->
<div class="create_group_modal side_modal right_side_modal">
   <div class="custom_side_header">
      <span class="slide_out_btn slide_out_right_btn close_side_slider waves-effect" onclick="add_to_Creategroup_close()">
      <i class="mdi mdi-close"></i>
      </span>
      <h3>Create Group</h3>
      <a class="waves-effect waves-light btn add_to_group_done" onclick="add_to_Creategroup_close()">Done</a>
   </div>
   <div class="side_modal_container side_modal_content create_group_content three_person">
      <div class="group_info_profile">
         <span class="big_profile_image">
            <div>
               <img src="images/whoisaround-img.png" />
               <!-- <img src="images/whoisaround-img1.png" />
               <img class="circle" src="images/demo-profile.jpg" /> -->
            </div>
            <input type="file" id="upload_creategroup_img" class="dis-none" />
            <label for="upload_creategroup_img" class="edit_profile_pic">
            <i class="zmdi zmdi-edit"></i>
            </label>
         </span>
      </div>
      <div class="creategroup_name_form">
         <div class="contact_user_container">
            <p class="contact_user_name">Vipul Patel, Bhadresh Ramani</p>
            <div class="group_name_text dis-none">
               <input placeholder="" id="create_group_name_text" type="text" class="validate" />
            </div>
            <p class="contact_user_lastseen">Created by 12345 67890, 06/nov/2017</p>
            <span class="group_name_edit" onclick="group_name_edit()" >
            <i class="zmdi zmdi-edit"></i>
            </span>
            <span class="group_name_remove" onclick="group_name_remove()" >
            <i class="mdi mdi-close"></i>
            </span>
         </div>
      </div>
      <div class="participants_container">
         <p class="participants_number">2 participants</p>
         <a class="participants_info" onclick="CreateGroupaddPerson()">
         <span class="participants_profile">
         <span class="add_participents">
         <i class="zmdi zmdi-account-add"></i>
         </span>
         </span>
         <span class="participants_name add_participants_name">Add Participants</span>
         </a>
         <div class="participants_info">
            <span class="participants_profile">
            <img src="images/whoisaround-img.png" />
            </span>
            <span class="participants_name">Vipul Patel</span>
            <span class="group_admin">
            Group Admin
            </span>
         </div>
         <div class="participants_info">
            <span class="participants_profile">
            <img src="images/whoisaround-img1.png" />
            </span>
            <span class="participants_name">Bhadresh Ramani</span>
            <span class="settings-icon group_participants_info">
               <a class="dropdown-button more_btn" href="javascript:void(0)" data-activates="group_person_info">
               <i class="zmdi zmdi-more"></i>
               </a>
               <ul id="group_person_info" class="dropdown-content custom_dropdown">
                  <li>
                     <a>Make group admin</a>
                  </li>
                  <li>
                     <a>Remove</a>
                  </li>
               </ul>
            </span>
         </div>
      </div>
      <!--
         <span class="btn-floating btn-large bottom_creategroup_btn" onclick="CreateGroupaddPerson()">
         	<i class="zmdi zmdi-arrow-left"></i>
         </span>
         -->
   </div>
</div>
<!--add person to new group modal-->
<div class="add_person_newgroup side_modal left_side_modal">
   <div class="custom_side_header">
      <span class="close_side_slider waves-effect" onclick="closeInnerSlide()">
      <i class="zmdi zmdi-arrow-left"></i>
      </span>
      <h3>Add to Group</h3>
      <a class="waves-effect waves-light btn add_to_group_done" onclick="closeInnerSlide()">Done</a>
   </div>
   <div class="side_modal_container side_modal_content">
      <div  class="custom_search group_modal_search">
         <button class="close_message_search arrow_back_icon" >
         <i class="zmdi zmdi-arrow-left"></i>
         </button>
         <button class="close_message_search search_messages_icon" >
         <i class="zmdi zmdi-search"></i>
         </button>
         <input id="add_to_group_search"  class="custom_search_input" type="text" placeholder="Search People" autocomplete="off" />
         <span class="remove_focus_text_icon">
         <i class="mdi mdi-close"></i>
         </span>
      </div>
      <div class="added_to_group_person">
         <div class="added_person">
            <img src="images/whoisaround-img1.png" />
            <span class="remove_added_person">
            <i class="mdi mdi-close"></i>
            </span>
         </div>
         <div class="added_person">
            <img src="images/whoisaround-img.png" />
            <span class="remove_added_person">
            <i class="mdi mdi-close"></i>
            </span>
         </div>
         <div class="added_person">
            <img class="circle" src="images/demo-profile.jpg" />
            <span class="remove_added_person">
            <i class="mdi mdi-close"></i>
            </span>
         </div>
      </div>
      <p class="suggested_label">Suggested</p>
      <div class="suggested_person_addto_group">
         <label class="add_to_group_container" for="filled_for_person_00">
            <span class="add_to_group_personprofile">
            <img class="circle" src="images/demo-profile.jpg"/>
            </span>
            <div class="add_to_group__personlabel">
               <p class="group_person_name" id="checkPerson0">Adel Google1</p>
            </div>
            <p class="addgroup_user_checkbox">
               <input type="checkbox" id="filled_for_person_00"  class="chk_person" />
         <label for="filled_for_person_00"></label>
         </p>
         </label>
         <label class="add_to_group_container" for="filled_for_person_11">
            <span class="add_to_group_personprofile">
            <img class="circle" src="images/demo-profile.jpg"/>
            </span>
            <div class="add_to_group__personlabel">
               <p class="group_person_name" id="checkPerson0">Adel Google1</p>
            </div>
            <p class="addgroup_user_checkbox">
               <input type="checkbox" id="filled_for_person_11"  class="chk_person" />
         <label for="filled_for_person_11"></label>
         </p>
         </label>
         <label class="add_to_group_container" for="filled_for_person_22">
            <span class="add_to_group_personprofile">
            <img class="circle" src="images/demo-profile.jpg"/>
            </span>
            <div class="add_to_group__personlabel">
               <p class="group_person_name" id="checkPerson0">Adel Google1</p>
            </div>
            <p class="addgroup_user_checkbox">
               <input type="checkbox" id="filled_for_person_22"  class="chk_person" />
         <label for="filled_for_person_22"></label>
         </p>
         </label>
      </div>
   </div>
</div>
<!--add person to create group modal-->
<div class="add_person_creategroup side_modal right_side_modal">
   <div class="custom_side_header">
      <span class="close_side_slider waves-effect" onclick="add_to_createdgroup_done()">
      <i class="zmdi zmdi-arrow-right"></i>
      </span>
      <h3>Add to Group</h3>
      <a class="waves-effect waves-light btn add_to_group_done" onclick="add_to_createdgroup_done()">Done</a>
   </div>
   <div class="side_modal_container side_modal_content">
      <div  class="custom_search group_modal_search">
         <div>
            <button class="close_message_search arrow_back_icon" >
            <i class="zmdi zmdi-arrow-left"></i>
            </button>
            <button class="close_message_search search_messages_icon" >
            <i class="zmdi zmdi-search"></i>
            </button>
            <input id="add_to_group_search"  class="custom_search_input" type="text" placeholder="Search People" autocomplete="off" />
            <span class="remove_focus_text_icon">
            <i class="mdi mdi-close"></i>
            </span>
         </div>
      </div>
   </div>
   <div class="added_to_group_person">
      <div class="added_person">
         <img src="images/whoisaround-img1.png" />
         <span class="remove_added_person">
         <i class="mdi mdi-close"></i>
         </span>
      </div>
      <div class="added_person">
         <img src="images/whoisaround-img.png" />
         <span class="remove_added_person">
         <i class="mdi mdi-close"></i>
         </span>
      </div>
   </div>
   <p class="suggested_label">Suggested</p>
   <div class="suggested_person_addto_group">
      <label class="add_to_group_container" for="filled_for_person_0" >
         <span class="add_to_group_personprofile">
         <img class="circle" src="images/demo-profile.jpg"/>
         </span>
         <div class="add_to_group__personlabel">
            <p class="group_person_name" id="checkPerson0">Bhadresh Ramani</p>
         </div>
         <p class="addgroup_user_checkbox">
            <input type="checkbox" id="filled_for_person_0"  class="chk_person" checked="checked"/>
      <label for="filled_for_person_0"></label>
      </p>
      </label>
      <label class="add_to_group_container" for="filled_for_person_1">
         <span class="add_to_group_personprofile">
         <img class="circle" src="images/demo-profile.jpg"/>
         </span>
         <div class="add_to_group__personlabel">
            <p class="group_person_name" id="checkPerson0">Vipul Patel</p>
         </div>
         <p class="addgroup_user_checkbox">
            <input type="checkbox" id="filled_for_person_1"  class="chk_person" checked="checked"/>
      <label for="filled_for_person_1"></label>
      </p>
      </label>
      <label class="add_to_group_container" for="filled_for_person_4">
         <span class="add_to_group_personprofile">
         <img class="circle" src="images/demo-profile.jpg"/>
         </span>
         <div class="add_to_group__personlabel">
            <p class="group_person_name" id="checkPerson0">User Name</p>
         </div>
         <p class="addgroup_user_checkbox">
            <input type="checkbox" id="filled_for_person_4"  class="chk_person"/>
      <label for="filled_for_person_4"></label>
      </p>
      </label>
      <label class="add_to_group_container" for="filled_for_person_5">
         <span class="add_to_group_personprofile">
         <img class="circle" src="images/demo-profile.jpg"/>
         </span>
         <div class="add_to_group__personlabel">
            <p class="group_person_name" id="checkPerson0">User Name</p>
         </div>
         <p class="addgroup_user_checkbox">
            <input type="checkbox" id="filled_for_person_5"  class="chk_person"/>
      <label for="filled_for_person_5"></label>
      </p>
      </label>
      <label class="add_to_group_container" for="filled_for_person_6">
         <span class="add_to_group_personprofile">
         <img class="circle" src="images/demo-profile.jpg"/>
         </span>
         <div class="add_to_group__personlabel">
            <p class="group_person_name" id="checkPerson0">User Name</p>
         </div>
         <p class="addgroup_user_checkbox">
            <input type="checkbox" id="filled_for_person_6"  class="chk_person"/>
      <label for="filled_for_person_6"></label>
      </p>
      </label>
   </div>
</div>
<!--add to group modal-->
<div class="add_to_group_modal side_modal right_side_modal inner_slide_modal">
   <div class="custom_side_header">
      <span class="add_to_group_close close_side_slider waves-effect" onclick="add_to_group_close()">
      <i class="mdi mdi-close"></i>
      </span>
      <h3>Add to Group</h3>
      <a class="waves-effect waves-light btn add_to_group_done" onclick="add_to_group_done()">Done</a>
   </div>
   <div class="side_modal_container side_modal_content">
      <div  class="custom_search group_modal_search">
         <div>
            <button class="close_message_search arrow_back_icon" >
            <i class="zmdi zmdi-arrow-left"></i>
            </button>
            <button class="close_message_search search_messages_icon" >
            <i class="zmdi zmdi-search"></i>
            </button>
            <input id="add_to_group_search"  class="custom_search_input" type="text" placeholder="Search People" autocomplete="off" />
            <span class="remove_focus_text_icon">
            <i class="mdi mdi-close"></i>
            </span>
         </div>
      </div>
      <div class="added_to_group_person">
         <div class="added_person">
            <img src="images/whoisaround-img1.png" />
            <span class="remove_added_person">
            <i class="mdi mdi-close"></i>
            </span>
         </div>
         <div class="added_person">
            <img src="images/whoisaround-img.png" />
            <span class="remove_added_person">
            <i class="mdi mdi-close"></i>
            </span>
         </div>
         <div class="added_person">
            <img class="circle" src="images/demo-profile.jpg" />
            <span class="remove_added_person">
            <i class="mdi mdi-close"></i>
            </span>
         </div>
      </div>
      <p class="suggested_label">Suggested</p>
      <div class="suggested_person_addto_group">
         <label class="add_to_group_container" for="filled_for_person_2">
            <span class="add_to_group_personprofile">
            <img class="circle" src="images/demo-profile.jpg"/>
            </span>
            <div class="add_to_group__personlabel">
               <p class="group_person_name" id="checkPerson0">Adel Google1</p>
            </div>
            <p class="addgroup_user_checkbox">
               <input type="checkbox" id="filled_for_person_2"  class="chk_person" />
         <label for="filled_for_person_2"></label>
         </p>
         </label>
         <label class="add_to_group_container" for="filled_for_person_3">
            <span class="add_to_group_personprofile">
            <img class="circle" src="images/demo-profile.jpg"/>
            </span>
            <div class="add_to_group__personlabel">
               <p class="group_person_name" id="checkPerson0">Adel Google1</p>
            </div>
            <p class="addgroup_user_checkbox">
               <input type="checkbox" id="filled_for_person_3"  class="chk_person" />
         <label for="filled_for_person_3"></label>
         </p>
         </label>
         <label class="add_to_group_container" for="filled_for_person_4">
            <span class="add_to_group_personprofile">
            <img class="circle" src="images/demo-profile.jpg"/>
            </span>
            <div class="add_to_group__personlabel">
               <p class="group_person_name" id="checkPerson0">Adel Google1</p>
            </div>
            <p class="addgroup_user_checkbox">
               <input type="checkbox" id="filled_for_person_4"  class="chk_person" />
         <label for="filled_for_person_4"></label>
         </p>
         </label>
      </div>
   </div>
</div>
<!--add to block modal-->
<div class="add_to_block_modal side_modal left_side_modal inner_slide_modal">
   <div class="custom_side_header">
      <span class="add_to_group_close close_side_slider waves-effect" onclick="BlockCancel()">
      <i class="zmdi zmdi-arrow-left"></i>
      </span>
      <h3>Add to Block</h3>
      <a class="waves-effect waves-light btn add_to_group_done" onclick="BlockCancel()">Done</a>
   </div>
   <div class="side_modal_container side_modal_content">
      <div  class="custom_search group_modal_search">
         <div>
            <button class="close_message_search arrow_back_icon" >
            <i class="zmdi zmdi-arrow-left"></i>
            </button>
            <button class="close_message_search search_messages_icon" >
            <i class="zmdi zmdi-search"></i>
            </button>
            <input id="add_to_group_search"  class="custom_search_input" type="text" placeholder="Search People" autocomplete="off" />
            <span class="remove_focus_text_icon">
            <i class="mdi mdi-close"></i>
            </span>
         </div>
      </div>
      <div class="added_to_group_person">
         <div class="added_person">
            <img src="images/whoisaround-img1.png" />
            <span class="remove_added_person">
            <i class="mdi mdi-close"></i>
            </span>
         </div>
         <div class="added_person">
            <img src="images/whoisaround-img.png" />
            <span class="remove_added_person">
            <i class="mdi mdi-close"></i>
            </span>
         </div>
         <div class="added_person">
            <img class="circle" src="images/demo-profile.jpg" />
            <span class="remove_added_person">
            <i class="mdi mdi-close"></i>
            </span>
         </div>
      </div>
      <p class="suggested_label">Suggested</p>
      <div class="suggested_person_addto_group">
         <label class="add_to_group_container" for="block_person_1">
            <span class="add_to_group_personprofile">
            <img class="circle" src="images/demo-profile.jpg"/>
            </span>
            <div class="add_to_group__personlabel">
               <p class="group_person_name" id="checkPerson0">Adel Google1</p>
            </div>
            <p class="addgroup_user_checkbox">
               <input type="checkbox" id="block_person_1"  class="chk_person" />
         <label for="block_person_1"></label>
         </p>
         </label>
         <label class="add_to_group_container" for="block_person_2">
            <span class="add_to_group_personprofile">
            <img class="circle" src="images/demo-profile.jpg"/>
            </span>
            <div class="add_to_group__personlabel">
               <p class="group_person_name" id="checkPerson0">Adel Google1</p>
            </div>
            <p class="addgroup_user_checkbox">
               <input type="checkbox" id="block_person_2"  class="chk_person" />
         <label for="block_person_2"></label>
         </p>
         </label>
         <label class="add_to_group_container" for="block_person_3">
            <span class="add_to_group_personprofile">
            <img class="circle" src="images/demo-profile.jpg"/>
            </span>
            <div class="add_to_group__personlabel">
               <p class="group_person_name" id="checkPerson0">Adel Google1</p>
            </div>
            <p class="addgroup_user_checkbox">
               <input type="checkbox" id="block_person_3"  class="chk_person" />
         <label for="block_person_3"></label>
         </p>
         </label>
      </div>
   </div>
</div>
<!--messages saved modal-->
<div class="message_save_modal side_modal left_side_modal">
   <div class="custom_side_header">
      <span class="slide_out_btn slide_out_right_btn close_side_slider waves-effect">
      <i class="zmdi zmdi-arrow-left"></i>
      </span>
      <h3>Saved messages</h3>
   </div>
   <div class="side_modal_container saved_message_container">
      <div class="msg_division">
         <span class="msg_profile">
         <img src="images/whoisaround-img.png" />
         </span>
         <div class="msg_info">
            <span class="person_send">
            Shaab
            </span>
            <span class="link_dot"></span>
            <span class="person_receive">
            You
            </span>
            <div class="settings-icon saved_msg_setting_icon">
               <a class="dropdown-button" href="javascript:void(0)" data-activates="saved_msg_setting">
               <i class="zmdi zmdi-more"></i>
               </a>
               <ul id="saved_msg_setting" class="dropdown-content custom_dropdown">
                  <li>
                     <a>Download</a>
                  </li>
                  <li>
                     <a>Forward message</a>
                  </li>
                  <li>
                     <a>Unsave message</a>
                  </li>
                  <li>
                     <a>Delete message</a>
                  </li>
               </ul>
            </div>
            <span class="day_time">
            Tuesday
            </span>
            <div class="msg_div_border">
               <div class="msg_text msg_content">
                  Lorem Ipsum is simply dummy text of the printing and typesetting industry.
               </div>
            </div>
         </div>
      </div>
      <div class="msg_division">
         <span class="msg_profile">
         <img src="images/whoisaround-img1.png" />
         </span>
         <div class="msg_info">
            <span class="person_send">
            Hasan Hallatat
            </span>
            <span class="link_dot"></span>
            <span class="person_receive">
            You
            </span>
            <div class="settings-icon saved_msg_setting_icon">
               <a class="dropdown-button" href="javascript:void(0)" data-activates="saved_msg_setting2">
               <i class="zmdi zmdi-more"></i>
               </a>
               <ul id="saved_msg_setting2" class="dropdown-content custom_dropdown">
                  <li>
                     <a>Download</a>
                  </li>
                  <li>
                     <a>Forward message</a>
                  </li>
                  <li>
                     <a>Unsave message</a>
                  </li>
                  <li>
                     <a>Delete message</a>
                  </li>
               </ul>
            </div>
            <!--<span class="right_msg_dropdown">
               <i class="zmdi zmdi-chevron-right"></i>
               </span>-->
            <span class="day_time">
            7/11/2017
            </span>
            <div class="msg_div_border">
               <div class="msg_video msg_content">
                  <video controls="">
                     <source src="https://www.w3schools.com/html/mov_bbb.mp4" type="video/mp4"/>
                  </video>
               </div>
            </div>
         </div>
      </div>
      <div class="msg_division">
         <span class="msg_profile">
         <img class="circle" src="images/demo-profile.jpg" />
         </span>
         <div class="msg_info">
            <span class="person_send">
            aAmneh
            </span>
            <span class="link_dot"></span>
            <span class="person_receive">
            You
            </span>
            <div class="settings-icon saved_msg_setting_icon">
               <a class="dropdown-button" href="javascript:void(0)" data-activates="saved_msg_setting3">
               <i class="zmdi zmdi-more"></i>
               </a>
               <ul id="saved_msg_setting3" class="dropdown-content custom_dropdown">
                  <li>
                     <a>Download</a>
                  </li>
                  <li>
                     <a>Forward message</a>
                  </li>
                  <li>
                     <a>Unsave message</a>
                  </li>
                  <li>
                     <a>Delete message</a>
                  </li>
               </ul>
            </div>
            <span class="day_time">
            7/11/2017
            </span>
            <div class="msg_div_border">
               <div class="msg_text msg_content">
                  dummy text of the printing and typesetting industry.
               </div>
               <div class="msg_text msg_content">
                  Ipsum is simply dummy text of the printing and typesetting industry.
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="side_modal_content">
   </div>
</div>
<!--person messages saved modal-->
<div class="person_message_save_modal side_modal right_side_modal right_innermodal">
   <div class="custom_side_header">
      <a class="close_side_slider waves-effect" onclick="closeInnerRightSlide()">
      <i class="zmdi zmdi-arrow-right"></i>
      </a>
      <h3>Saved messages</h3>
   </div>
   <div class="side_modal_container saved_message_container">
      <div class="msg_division">
         <span class="msg_profile">
         <img src="images/whoisaround-img.png" />
         </span>
         <div class="msg_info">
            <span class="person_send">
            Shaab
            </span>
            <span class="link_dot"></span>
            <span class="person_receive">
            You
            </span>
            <div class="settings-icon saved_msg_setting_icon">
               <a class="dropdown-button" href="javascript:void(0)" data-activates="saved_msg_setting4">
               <i class="zmdi zmdi-more"></i>
               </a>
               <ul id="saved_msg_setting4" class="dropdown-content custom_dropdown">
                  <li>
                     <a>Download</a>
                  </li>
                  <li>
                     <a>Forward message</a>
                  </li>
                  <li>
                     <a>Unsave message</a>
                  </li>
                  <li>
                     <a>Delete message</a>
                  </li>
               </ul>
            </div>
            <span class="day_time">
            Tuesday
            </span>
            <div >
               <div class="msg_text msg_content">
                  Lorem Ipsum is simply dummy text of the printing and typesetting industry.
               </div>
               <div class="msg_video msg_content">
                  <video controls="">
                     <source src="https://www.w3schools.com/html/mov_bbb.mp4" type="video/mp4"/>
                  </video>
               </div>
               <div class="msg_text msg_content">
                  dummy text of the printing and typesetting industry.
               </div>
               <div class="msg_text msg_content">
                  Ipsum is simply dummy text of the printing and typesetting industry.
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="side_modal_content">
   </div>
</div>
<!--Archieve modal-->
<div class="archieved_chat_modal side_modal left_side_modal">
   <div class="custom_side_header">
      <span class="slide_out_btn slide_out_right_btn close_side_slider waves-effect">
      <i class="zmdi zmdi-arrow-left"></i>
      </span>
      <h3>Archieved chats</h3>
   </div>
   <div class="side_modal_container archeive_container">
      <div class="archeive_info">
         <span class="participants_profile">
         <img src="images/whoisaround-img.png" />
         </span>
         <span class="participants_name">Vipul Patel</span>
         <span class="day_time">
         Tuesday
         </span>
         <span class="settings-icon archeive_chat_dropdown">
            <a class="dropdown-button more_btn" href="javascript:void(0)" data-activates="archeive_chat_options">
            <i class="zmdi zmdi-more"></i>
            </a>
            <ul id="archeive_chat_options" class="dropdown-content custom_dropdown">
               <li>
                  <a>Delete chat</a>
               </li>
               <li>
                  <a>Unarchive chat</a>
               </li>
            </ul>
         </span>
      </div>
      <span class="no_archeive_chat_icon">
      <i class="zmdi zmdi-archive"></i>
      </span>
      <p class="archeived_msg">No archeived chat</p>
   </div>
</div>
<!--Block contact modal-->
<div class="blocked_contact_modal side_modal left_side_modal">
   <div class="custom_side_header">
      <span class="slide_out_btn slide_out_right_btn close_side_slider waves-effect">
      <i class="zmdi zmdi-arrow-left"></i>
      </span>
      <h3>Blocked Contacts</h3>
   </div>
   <div class="side_modal_container">
      <a class="blocked_addperson_box" onclick="AddBlockModal()">
         <span class="add_to_block_icon">
         <i class="zmdi zmdi-account-add"></i>
         </span>
         <p class="add_to_block_name">Add blocked contact</p>
      </a>
      <div class="block_contact_container">
         <div class="blocked_person_box">
            <span class="blocked_person_img">
            <img class="circle" src="images/demo-profile.jpg" />
            </span>
            <div class="number_blocked">
               <p class="blocked_number">+967736870060</p>
               <p class="blocked_name">Hey there! i am using Iamin<?=$st_nm_L?></p>
            </div>
            <span class="blocked_remove waves-effect">
            <i class="mdi mdi-close"></i>
            </span>
         </div>
         <div class="blocked_person_box">
            <span class="blocked_person_img">
            <img class="circle" src="images/demo-profile.jpg" />
            </span>
            <div class="number_blocked">
               <p class="blocked_number">Adel Ahasanat</p>
               <p class="blocked_name">Hey there! i am using Iamin<?=$st_nm_L?></p>
            </div>
            <span class="blocked_remove waves-effect">
            <i class="mdi mdi-close"></i>
            </span>
         </div>
         <p class="blocked_msg">
            Blocked Contacts will no longer be able to call you or send you messages
         </p>
      </div>
   </div>
</div>
<!--setting modal--> 
<div class="msg_setting_modal side_modal left_side_modal">
   <div class="custom_side_header">
      <span class="slide_out_btn slide_out_right_btn close_side_slider waves-effect">
      <i class="zmdi zmdi-arrow-left"></i>
      </span>
      <h3>Settings</h3>
   </div>
   <div class="side_modal_container">
      <div class="profile_setting_box">
         <span class="user_profile_setting">
         <img class="circle" src="images/demo-profile.jpg" />
         </span>
         <div class="user_profile_container">
            <span class="user_name_setting">
            Nimish
            </span>
            <span class="user_status_setting">
            Hey there ! i am using iamin<?=$st_nm_L?>
            </span>
            <span class="profile_name_edit" onclick="ProfileNameChange()">
            <i class="zmdi zmdi-edit"></i>
            </span>
            <div class="setting_profile_name">
               <input placeholder="" id="profile_name_add" type="text" class="validate" />
            </div>
         </div>
      </div>
      <div class="msg_setting_content">
         <div class="frow">
            <a class="setting_switch">
               <span class="switch_header">Sound</span>
               <span class="contact_list_options">Play sound when new message is received</span>
               <span class="contact_list_icon">
                  <div class="switch contact_info_switch">
                     <label>
                     <input type="checkbox" />
                     <span class="lever"></span>
                     </label>
                  </div>
               </span>
            </a>
            <a class="setting_switch">
               <span class="switch_header">Display preview</span>
               <span class="contact_list_options">Show new message preview</span>
               <span class="contact_list_icon">
                  <div class="switch contact_info_switch">
                     <label>
                     <input type="checkbox" />
                     <span class="lever"></span>
                     </label>
                  </div>
               </span>
            </a>
         </div>
         <div class="frow i_mute">
            <select>
               <option value="" disabled="" selected>Turn off alerts and sound for...</option>
               <option value="1">Option 1</option>
               <option value="2">Option 2</option>
               <option value="3">Option 3</option>
            </select>
         </div>
         <div class="msg_setting_label_container">
            <p class="msg_setting_label">
               <input type="checkbox" id="filled_in_box3" checked="checked" />
               <label for="filled_in_box3" class="setting_options">Show me a way when inactive for 10 minutes</label>
            </p>
            <p class="msg_setting_label">
               <input type="checkbox" id="filled_in_box2" checked="checked" />
               <label for="filled_in_box2" class="setting_options">Enter to sens message</label>
            </p>
         </div>
      </div>
   </div>
</div>
<!--Search Messages modal-->
<div class="search_messages_modal side_modal right_side_modal">
   <div class="custom_side_header">
      <span class="slide_out_btn slide_out_right_btn close_side_slider waves-effect" onclick="closeMessageSearchSlide()">
      <i class="mdi mdi-close"></i>
      </span>
      <h3>Search Messages</h3>
   </div>
   <div class="side_modal_container">
      <div class="custom_search msg_search_box">
         <div>
            <button class="close_message_search arrow_back_icon">
            <i class="zmdi zmdi-arrow-left"></i>
            </button>
            <button class="close_message_search search_messages_icon">
            <i class="zmdi zmdi-search"></i>
            </button>
            <input id="messages_search" class="custom_search_input" type="text" placeholder="Search message text" autocomplete="off" />
            <span class="remove_focus_text_icon dis-none">
            <i class="mdi mdi-close"></i>
            </span>
         </div>
      </div>
   </div>
   <div class="side_modal_content">
   </div>
</div>
<!--contact info modal-->
<div class="contact_info_modal side_modal right_side_modal">
   <div class="custom_side_header">
      <span class="slide_out_right_btn close_side_slider waves-effect" onclick="contactInfo()">
      <i class="mdi mdi-close"></i>
      </span>
      <h3>Contact info</h3>
   </div>
   <div class="side_modal_container contact_info_container">
      <div class="contact_info_profile">
         <span class="big_profile_image">
         <img src="images/whoisaround-img.png" />
         </span>
      </div>
      <div class="contact_user_container">
         <p class="contact_user_name">User Name</p>
         <p class="contact_user_lastseen">last seen today at 12:45PM</p>
      </div>
      <ul class="contact_info_ul">
         <li>
            <a>
            <span class="contact_list_options">View Wall</span>
            <span class="contact_list_icon">
            <i class="zmdi zmdi-chevron-right"></i>
            </span>
            </a>
         </li>
         <li>
            <a>
            <span class="contact_list_options">Send Gift</span>
            <span class="contact_list_icon">
            <i class="zmdi zmdi-chevron-right"></i>
            </span>
            </a>
         </li>
         <li>
            <a onclick="savedPersonMessages()">
            <span class="contact_list_options">Saved messages</span>
            <span class="contact_list_icon">
            <i class="zmdi zmdi-chevron-right"></i>
            </span>
            </a>
         </li>
      </ul>
      <ul class="contact_info_ul contact_info_mute">
         <li>
            <a>
               <span class="contact_list_options">Mute</span>
               <span class="contact_list_icon">
                  <div class="switch contact_info_switch">
                     <label>
                     <input type="checkbox" />
                     <span class="lever"></span>
                     </label>
                  </div>
               </span>
            </a>
         </li>
         <li>
            <a>
               <span class="contact_list_options">Block</span>
               <span class="contact_list_icon">
                  <div class="switch contact_info_switch">
                     <label>
                     <input type="checkbox" />
                     <span class="lever"></span>
                     </label>
                  </div>
               </span>
            </a>
         </li>
      </ul>
   </div>
</div>
<!--group info modal-->
<div class="group_info_modal side_modal right_side_modal">
   <div class="custom_side_header">
      <span class="close_side_slider waves-effect" onclick="CloseGroupInfo()">
      <i class="mdi mdi-close"></i>
      </span>
      <h3>Group info</h3>
      <a type="button" class="waves-effect waves-light btn add_to_group_done" onclick="CloseGroupInfo()">Done</a>
   </div>
   <div class="side_modal_container group_info_container">
      <div class="group_info_profile three_person">
         <span class="big_profile_image">
            <div>
               <img src="images/whoisaround-img1.png" />
               <!-- <img src="images/whoisaround-img1.png" />
               <img src="images/whoisaround-img1.png" /> -->
            </div>
            <input type="file" id="upload_group_img" class="dis-none" />
            <label for="upload_group_img" class="edit_profile_pic">
            <i class="mdi mdi-camera" ></i>
            </label>
         </span>
      </div>
      <div class="contact_user_container">
         <p class="contact_user_name">Group Name</p>
         <div class="group_name_text">
            <input placeholder="" id="group_name_text" type="text" class="validate" />
         </div>
         <p class="contact_user_lastseen">Created by 12345 67890, 06/nov/2017</p>
         <span class="group_name_edit" onclick="group_name_edit()" >
         <i class="zmdi zmdi-edit"></i>
         </span>
         <span class="group_name_remove" onclick="group_name_remove()" >
         <i class="mdi mdi-close"></i>
         </span>
      </div>
      <ul class="contact_info_ul">
         <li>
            <a>
               <span class="contact_list_options">Mute</span>
               <span class="contact_list_icon">
                  <div class="switch contact_info_switch">
                     <label>
                     <input type="checkbox"/>
                     <span class="lever"></span>
                     </label>
                  </div>
               </span>
            </a>
         </li>
      </ul>
      <div class="participants_container">
         <p class="participants_number">3 participants</p>
         <a class="participants_info" onclick="add_person_group()">
         <span class="participants_profile">
         <span class="add_participents">
         <i class="zmdi zmdi-account-add"></i>
         </span>
         </span>
         <span class="participants_name add_participants_name">Add Participants</span>
         </a>
         <div class="participants_info">
            <span class="participants_profile">
            <img src="images/whoisaround-img.png" />
            </span>
            <span class="participants_name">Vipul Patel</span>
            <span class="settings-icon group_participants_info">
               <a class="dropdown-button more_btn" href="javascript:void(0)" data-activates="group_person_info1">
               <i class="zmdi zmdi-more"></i>
               </a>
               <ul id="group_person_info1" class="dropdown-content custom_dropdown">
                  <li>
                     <a>Make group admin</a>
                  </li>
                  <li>
                     <a>Remove</a>
                  </li>
               </ul>
            </span>
         </div>
         <div class="participants_info">
            <span class="participants_profile">
            <img src="images/whoisaround-img1.png" />
            </span>
            <span class="participants_name">Bhadresh Ramani</span>
            <span class="settings-icon group_participants_info">
               <a class="dropdown-button more_btn" href="javascript:void(0)" data-activates="group_person_info2">
               <i class="zmdi zmdi-more"></i>
               </a>
               <ul id="group_person_info2" class="dropdown-content custom_dropdown">
                  <li>
                     <a>Make group admin</a>
                  </li>
                  <li>
                     <a>Remove</a>
                  </li>
               </ul>
            </span>
         </div>
         <div class="participants_info">
            <span class="participants_profile">
            <img class="circle" src="images/demo-profile.jpg" />
            </span>
            <span class="participants_name">Nimish Parekh</span>
            <span class="settings-icon group_participants_info">
               <a class="dropdown-button more_btn" href="javascript:void(0)" data-activates="group_person_info3">
               <i class="zmdi zmdi-more"></i>
               </a>
               <ul id="group_person_info3" class="dropdown-content custom_dropdown">
                  <li>
                     <a>Make group admin</a>
                  </li>
                  <li>
                     <a>Remove</a>
                  </li>
               </ul>
            </span>
         </div>
      </div>
      <div class="exit_group">
         <ul>
            <li>
               <a>
               <span class="exit_group_icon">
               <i class="mdi mdi-logout" ></i>
               </span>
               <span class="exit_group_label">
               Exit group
               </span>
               </a>
            </li>
         </ul>
      </div>
   </div>
</div>
<!--group info modal-->
<div class="person_search_slide_xs side_modal left_side_modal">
   <div class="custom_side_header">
      <span class="slide_out_btn slide_out_right_btn close_side_slider waves-effect">
      <i class="zmdi zmdi-arrow-left"></i>
      </span>
      <h3>New Chat</h3>
   </div>
   <div class="side_modal_container side_modal_content">
      <div  class="custom_search group_modal_search">
         <div>
            <button class="close_message_search arrow_back_icon" >
            <i class="zmdi zmdi-arrow-left"></i>
            </button>
            <button class="close_message_search search_messages_icon" >
            <i class="zmdi zmdi-search"></i>
            </button>
            <input id="add_to_group_search"  class="custom_search_input" type="text" placeholder="Type name..." autocomplete="off" />
            <span class="remove_focus_text_icon">
            <i class="mdi mdi-close"></i>
            </span>
         </div>
      </div>
      <div class="suggested_person_addto_group">
         <a href="javascript:void(0)" data-id="user1" onclick="openThread(this)" class="add_to_group_container">
            <span class="add_to_group_personprofile">
            <img src="images/whoisaround-img.png"/>
            </span>
            <div class="add_to_group__personlabel">
               <p class="group_person_name" id="checkPerson0">Vipul Patel</p>
            </div>
         </a>
         <a href="javascript:void(0)" data-id="user2" onclick="openThread(this)" class="add_to_group_container">
            <span class="add_to_group_personprofile">
            <img src="images/whoisaround-img1.png"/>
            </span>
            <div class="add_to_group__personlabel">
               <p class="group_person_name" id="checkPerson0">Bhadresh Ramani</p>
            </div>
         </a>
         <a href="javascript:void(0)" data-id="user3" onclick="openThread(this)" class="add_to_group_container">
            <span class="add_to_group_personprofile">
            <img class="circle" src="images/demo-profile.jpg"/>
            </span>
            <div class="add_to_group__personlabel">
               <p class="group_person_name" id="checkPerson0">Vipul Patel, Nimish Parekh</p>
            </div>
         </a>
      </div>
   </div>
</div>
<!--xs Search Messages modal -->
<div class="msgsearch_messages_modal side_modal left_side_modal">
   <div class="custom_side_header">
      <span class="slide_out_btn slide_out_right_btn close_side_slider waves-effect" onclick="closeMessageSearchSlide()">
      <i class="mdi mdi-close"></i>
      </span>
      <h3>Search Messages</h3>
   </div>
   <div>
      <div class="custom_search msg_search_box">
         <div>
            <button class="close_message_search arrow_back_icon">
            <i class="zmdi zmdi-arrow-left"></i>
            </button>
            <button class="close_message_search search_messages_icon">
            <i class="zmdi zmdi-search"></i>
            </button>
            <input id="messages_search" class="custom_search_input" type="text" placeholder="Search message text" autocomplete="off" />
            <span class="remove_focus_text_icon dis-none">
            <i class="mdi mdi-close"></i>
            </span>
         </div>
      </div>
   </div>
   <div class="side_modal_content">
   </div>
</div>
<!--gift modal-->
<div id="giftlist_popup" class="modal  gift_modal  giftlist-popup">
   <div class="custom_message_modal_header">
      <p>Send gift</p>
      <button class="close_modal_icon waves-effect" onclick="gift_modal()">
      <i class="mdi mdi-close mdi-20px"></i>
      </button>
   </div>
   <div class="gift_content">
      <div class="popup-content">
         <div class="emostickers-holder">
            <div class="emostickers-box">
               <div class="emostickers" tabindex="8">
                  <ul class="emostickers-list">
                     <li>
                        <a href="javascript:void(0)" data-class="(wine)" onclick="useGift('(wine)','popular','editmode', 'message');">
                        <span class="emosticker emosticker-wine" title="wine">(wine)</span>
                        </a>
                     </li>
                     <li>
                        <a href="javascript:void(0)" data-class="(icecream)" onclick="useGift('(icecream)','popular','editmode', 'message');">
                        <span class="emosticker emosticker-icecream" title="icecream">(icecream)</span>
                        </a>
                     </li>
                     <li>
                        <a href="javascript:void(0)" data-class="(coffee)" onclick="useGift('(coffee)','popular','editmode', 'message');">
                        <span class="emosticker emosticker-coffee" title="coffee">(coffee)</span>
                        </a>
                     </li>
                     <li>
                        <a href="javascript:void(0)" data-class="(heart)" onclick="useGift('(heart)','popular','editmode', 'message');">
                        <span class="emosticker emosticker-heart" title="heart">(heart)</span>
                        </a>
                     </li>
                     <li>
                        <a href="javascript:void(0)" data-class="(flower)" onclick="useGift('(flower)','popular','editmode', 'message');">
                        <span class="emosticker emosticker-flower" title="flower">(flower)</span>
                        </a>
                     </li>
                     <li>
                        <a href="javascript:void(0)" data-class="(cake)" onclick="useGift('(cake)','popular','editmode', 'message');">
                        <span class="emosticker emosticker-cake" title="cake">(cake)</span>
                        </a>
                     </li>
                     <li>
                        <a href="javascript:void(0)" data-class="(handshake)" onclick="useGift('(handshake)','popular','editmode', 'message');">
                        <span class="emosticker emosticker-handshake" title="handshake">(handshake)</span>
                        </a>
                     </li>
                     <li>
                        <a href="javascript:void(0)" data-class="(gift)" onclick="useGift('(gift)','popular','editmode', 'message');">
                        <span class="emosticker emosticker-gift" title="gift">(gift)</span>
                        </a>
                     </li>
                     <li>
                        <a href="javascript:void(0)" data-class="(goodmorning)" onclick="useGift('(goodmorning)','popular','editmode', 'message');">
                        <span class="emosticker emosticker-goodmorning" title="goodmorning">(goodmorning)</span>
                        </a>
                     </li>
                     <li>
                        <a href="javascript:void(0)" data-class="(goodnight)" onclick="useGift('(goodnight)','popular','editmode', 'message');">
                        <span class="emosticker emosticker-goodnight" title="goodnight">(goodnight)</span>
                        </a>
                     </li>
                     <li>
                        <a href="javascript:void(0)" data-class="(backpack)" onclick="useGift('(backpack)','popular','editmode', 'message');">
                        <span class="emosticker emosticker-backpack" title="backpack">(backpack)</span>
                        </a>
                     </li>
                     <li>
                        <a href="javascript:void(0)" data-class="(parasailing)" onclick="useGift('(parasailing)','popular','editmode', 'message');">
                        <span class="emosticker emosticker-parasailing" title="parasailing">(parasailing)</span>
                        </a>
                     </li>
                     <li>
                        <a href="javascript:void(0)" data-class="(train)" onclick="useGift('(train)','popular','editmode', 'message');">
                        <span class="emosticker emosticker-train" title="train">(train)</span>
                        </a>
                     </li>
                     <li>
                        <a href="javascript:void(0)" data-class="(flipflop)" onclick="useGift('(flipflop)','popular','editmode', 'message');">
                        <span class="emosticker emosticker-flipflop" title="flipflop">(flipflop)</span>
                        </a>
                     </li>
                     <li>
                        <a href="javascript:void(0)" data-class="(airplane)" onclick="useGift('(airplane)','popular','editmode', 'message');">
                        <span class="emosticker emosticker-airplane" title="airplane">(airplane)</span>
                        </a>
                     </li>
                     <li>
                        <a href="javascript:void(0)" data-class="(sunbed)" onclick="useGift('(sunbed)','popular','editmode', 'message');">
                        <span class="emosticker emosticker-sunbed" title="sunbed">(sunbed)</span>
                        </a>
                     </li>
                     <li>
                        <a href="javascript:void(0)" data-class="(happyhalloween)" onclick="useGift('(happyhalloween)','popular','editmode', 'message');">
                        <span class="emosticker emosticker-happyhalloween" title="happyhalloween">(happyhalloween)</span>
                        </a>
                     </li>
                     <li>
                        <a href="javascript:void(0)" data-class="(merrychristmas)" onclick="useGift('(merrychristmas)','popular','editmode', 'message');">
                        <span class="emosticker emosticker-merrychristmas" title="merrychristmas">(merrychristmas)</span>
                        </a>
                     </li>
                     <li>
                        <a href="javascript:void(0)" data-class="(eidmubarak)" onclick="useGift('(eidmubarak)','popular','editmode', 'message');">
                        <span class="emosticker emosticker-eidmubarak" title="eidmubarak">(eidmubarak)</span>
                        </a>
                     </li>
                     <li>
                        <a href="javascript:void(0)" data-class="(happyyear)" onclick="useGift('(happyyear)','popular','editmode', 'message');">
                        <span class="emosticker emosticker-happyyear" title="happyyear">(happyyear)</span>
                        </a>
                     </li>
                     <li>
                        <a href="javascript:void(0)" data-class="(happymothers)" onclick="useGift('(happymothers)','popular','editmode', 'message');">
                        <span class="emosticker emosticker-happymothers" title="happymothers">(happymothers)</span>
                        </a>
                     </li>
                     <li>
                        <a href="javascript:void(0)" data-class="(happyfathers)" onclick="useGift('(happyfathers)','popular','editmode', 'message');">
                        <span class="emosticker emosticker-happyfathers" title="happyfathers">(happyfathers)</span>
                        </a>
                     </li>
                     <li>
                        <a href="javascript:void(0)" data-class="(happyaniversary)" onclick="useGift('(happyaniversary)','popular','editmode', 'message');">
                        <span class="emosticker emosticker-happyaniversary" title="happyaniversary">(happyaniversary)</span>
                        </a>
                     </li>
                     <li>
                        <a href="javascript:void(0)" data-class="(happybirthday)" onclick="useGift('(happybirthday)','popular','editmode', 'message');">
                        <span class="emosticker emosticker-happybirthday" title="happybirthday">(happybirthday)</span>
                        </a>
                     </li>
                  </ul>
               </div>
            </div>
            <span class="credits">100 Credits</span>
         </div>
      </div>
   </div>
</div> 

<?php include('common/usegift_popup.php'); ?>

<?php include('common/addperson_popup.php'); ?>

<!--mute discard-->
<div id="mute_modal" class="modal compose_discard_modal custom_modal">
   <div class="modal-content">
      <p class="discard_modal_msg">Mute Chat ?</p>
   </div>
   <div class="modal-footer">
      <a class="modal_keep btngen-center-align " >Done</a>
      <a type="button" class="modal_discard btngen-center-align " onclick="clearPost()">Cancel</a>
   </div>
</div>
<!--Archieve discard-->
<div id="Archieve_modal" class="modal compose_discard_modal custom_modal">
   <div class="modal-content">
      <p class="discard_modal_msg">Archieve Chat ?</p>
   </div>
   <div class="modal-footer">
      <a class="modal_keep btngen-center-align " >Done</a>
      <a type="button" class="modal_discard btngen-center-align " onclick="clearPost()">Cancel</a>
   </div>
</div>
<!--Unread message discard-->
<div id="UnreadMessages_modal" class="modal compose_discard_modal custom_modal">
   <div class="modal-content">
      <p class="discard_modal_msg">Archieve Chat ?</p>
   </div>
   <div class="modal-footer">
      <a class="modal_keep btngen-center-align " >Done</a>
      <a type="button" class="modal_discard btngen-center-align " onclick="clearPost()">Cancel</a>
   </div>
</div>
<!--Delete chat discard-->
<div id="DeteletChat_modal" class="modal compose_discard_modal custom_modal">
   <div class="modal-content">
      <p class="discard_modal_msg">Delete Chat ?</p>
   </div>
   <div class="modal-footer">
      <a class="modal_keep btngen-center-align " >Done</a>
      <a type="button" class="modal_discard btngen-center-align " onclick="clearPost()">Cancel</a>
   </div>
</div>
<script type="text/javascript">
   document.body.style.overflowY = "hidden";
</script>
<?php include("script.php"); ?>
<script type="text/javascript" src="js/message.js"></script>
<script type="text/javascript" src="js/markjs/jquery.mark.js"></script>
<script type="text/javascript" src="js/markjs/jquery.mark.es6.js"></script>
<script type="text/javascript" src="js/markjs/custom_mark.js"></script>
</body>
</html>