<?php include("header.php"); ?>
<?php include("common/menu.php"); ?>
<div class="floating-icon">
   <div class="scrollup-btnbox anim-side btnbox scrollup-float">
      <div class="scrollup-button float-icon">
         <span class="icon-holder ispan">
            <i class="mdi mdi-arrow-up-bold-circle"></i>
         </span>
      </div>
   </div>
</div>
<div class="clear"></div>
<div class="">
   <?php include("common/leftmenu.php"); ?>
   <div class="fixed-layout">
      <div class="collection-page event-detail-page pb-0 m-t-50">
         <div class="combined-column wide-open main-page full-page">
            <div class="width-100 m-top">
               <div class="header-content">
                  <h1><span class="text-primary ff-bold">Visit</span> from Home</h1>
                  <p>Stay inspired and experience <br> the very best of <?=$st_nm_S?>, from home.</p>
               </div>
               <div class="page-content">
                  <div class="row mx-0">
                    <div class="col s12 m6">
                        <div class="card">
                          <div class="card-image">
                            <img src="images/<?=$st_nm_L?>-manument.jpg">
                          </div>
                          <div class="card-content">
                              <h3><?=$st_nm_S?></h3>
                              <p>I am a very simple card. I am good at containing small bits of information. I am convenient because I require little markup to use effectively.</p>
                          </div>
                          <div class="card-action">
                              <a href="vtour-detail.php" class="btn ff-medium bg-primary">Learn More</a>
                          </div>
                        </div>
                     </div>
                     <div class="col s12 m6">
                        <div class="card">
                          <div class="card-image">
                            <img src="images/<?=$st_nm_L?>-deadsea.jpg">
                          </div>
                          <div class="card-content">
                              <h3><?=$st_nm_L?>, Dead Sea</h3>
                              <p>I am a very simple card. I am good at containing small bits of information. I am convenient because I require little markup to use effectively.</p>
                          </div>
                          <div class="card-action">
                              <a href="vtour-detail.php" class="btn ff-medium bg-primary">Learn More</a>
                          </div>
                        </div>
                     </div>
                     <div class="col s12 m12 py-70">
                        <div class="card horizontal">
                           <div class="card-image">
                             <img src="images/<?=$st_nm_L?>-wadirum.jpg">
                           </div>
                           <div class="card-stacked">
                              <div class="card-content">
                                 <h3><?=$st_nm_S?>, Wadi Rum</h3>
                                 <p>I am a very simple card. I am good at containing small bits of information. I am convenient because I require little markup to use effectively.</p>
                              </div>
                              <div class="card-action">
                                 <a href="vtour-detail.php" class="btn ff-medium bg-primary">Learn More</a>
                             </div>
                           </div>
                        </div>
                     </div>
                     <div class="col s12 m6">
                        <div class="card">
                          <div class="card-image">
                            <img src="images/wadimusa.jpg">
                          </div>
                          <div class="card-content">
                              <h3><?=$st_nm_S?>, Wadi Musa</h3>
                              <p>I am a very simple card. I am good at containing small bits of information. I am convenient because I require little markup to use effectively.</p>
                          </div>
                          <div class="card-action">
                              <a href="vtour-detail.php" class="btn ff-medium bg-primary">Learn More</a>
                          </div>
                        </div>
                     </div>
                     <div class="col s12 m6">
                        <div class="card">
                          <div class="card-image">
                            <img src="images/<?=$st_nm_L?>-tour1.jpg">
                          </div>
                          <div class="card-content">
                              <h3><?=$st_nm_S?>, Amman </h3>
                              <p>I am a very simple card. I am good at containing small bits of information. I am convenient because I require little markup to use effectively.</p>
                          </div>
                          <div class="card-action">
                              <a href="vtour-detail.php" class="btn ff-medium bg-primary">Learn More</a>
                          </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>


<?php include("script.php"); ?>