
<?php
   $file = basename($_SERVER['REQUEST_URI'], '?' . $_SERVER['QUERY_STRING']);
   $file = str_replace('.php','',$file);
   $search_holder="search-holder main-sholder ";
?>
   <div class="header-themebar">
      <div class="container">
         <div class="header-nav">
            <div class="mobile-menu topicon">
               <a href="javascript:void(0)" class="mbl-menuicon1 waves-effect"><i class="mdi mdi-menu"></i></a>
               <?php if($file == "settings"){ ?>
               <div class="gotohome">
                  <a href="javascript:void(0)" onclick="resetInnerPage('settings','show')"><i class="mdi mdi-arrow-left"></i></a>
               </div>
               <?php } ?>
               <?php if($file == "wall" || $file == "wall-new" || $file == "business-page" || $file == "business-page-new"){ ?>
               <div class="gotohome">
                  <a href="javascript:void(0)" onclick="resetInnerPage('wall','show')"><i class="mdi mdi-arrow-left"></i></a>
               </div>
               <?php } ?>
               <?php if($file == "places"){ ?>
               <div class="gotohome">
                  <a href="javascript:void(0)" onclick="resetPlacesTab()"><i class="mdi mdi-arrow-left"></i></a>
               </div>
               <?php } ?>
            </div>
            <div class="logo-holder">
               <?php if($file == "places" || $file == "trip" || $file == "tours" || $file == "flight" || $file == "hotels" || $file == "hotels-new" || $file == "credit" || $file == "credit-transfer" || $file == "credit-update" || $file == "vip-member" || $file == "vip-package" || $file == "verify" || $file == "billing-info" || $file == "settings" || $file == "advertisement" || $file == "manage-ad" || $file == "ad-manager" || $file == "wall" || $file == "wall-new" || $file == "business-page" || $file == "business-page-new" || $file == "messages" || $file == "messages-new"){ ?>
               <div class="mobile-menu">
                  <a href="javascript:void(0)" class="waves-effect"><i class="mdi mdi-menu"></i></a>												
               </div>
               <?php } ?>
               <a href="index.php" class="desk-logo"><img src="images/white-logo.png"/></a>
            </div>
            <div class="page-name mainpage-name"><?php echo $file; ?></div>
            <div class="page-name innerpage-name" >
               <?php if($file == "settings"){ ?>Basic Information<?php } ?>
               <?php if($file == "wall" || $file == "wall-new" || $file == "business-page" || $file == "business-page-new"){ ?>Wall
               <?php } ?>							
            </div>
            <?php if($file == "messages" || $file == "messages-new"){ ?>
            <div class="mbl-innerhead">
               <div class="gotohome">
                  <a href="javascript:void(0)" onclick="closeAddNewMsg()"><i class="mdi mdi-arrow-left"></i></a>
               </div>
               <div class="logo-holder">
                  <span class="top_img">
                  <img src="images/whoisaround-img.png"/>
                  </span>
                  <a href="javascript:void(0)" class="mbl-logo page-name" onclick="contactInfo()">Vipul Patel</a>
                  <div class="top_message_status">
                     <span class="">last seen 1hr &nbsp; | </span>
                     <!--<span class="userstatus">&nbsp; i like nonsense it wakes up the brain cells</span>-->
                     <span class=""> 12:57 PM </span>
                     <span class="">| INDIA </span>
                  </div>
               </div>
            </div>
            <?php } ?>
            <?php if($file == "business-page" || $file == "business-page-new"){ ?>
            <div class="mbl-innerhead">
               <div class="gotohome">
                  <a href="javascript:void(0)" onclick="backToMain('businesspage')"><i class="mdi mdi-arrow-left"></i></a>
               </div>
               <div class="logo-holder">					
                  <a href="javascript:void(0)" class="mbl-logo page-name">Vipul Patel</a>
               </div>
            </div>
            <?php } ?>				
            <div class="profile-top">
               <a href="wall.php" class="profile-info">
               <img  class="circle" src="images/demo-profile.jpg"/>
               <span class="user-name">Nimish</span>
               </a>
               <div class="fixed-action-btn horizontal header_add_btn">
                  <a class='dropdown-button add_btn' href='javascript:void(0)' data-activates='add_btn'><i class="mdi mdi-plus"></i></a>
                  <ul>
                     <li>
                        <a class="btn-floating yellow darken-2" href="hotels.php" title="Hotels"><i class="mdi mdi-office-building"></i></a>
                     </li>
                     <li>
                        <a class="btn-floating deep-purple" href="flight.php" title="Flight"><i class="mdi mdi-airplane"></i></a>
                     </li>
                     <li>
                        <a class="btn-floating red darken-3" href="tours.php" title="Tours"><i class="mdi mdi-bus" ></i></a>
                     </li>
                     <li>
                        <a class="btn-floating blue"  href="trip.php" title="Trip"><i class="mdi mdi-plus"></i></a>
                     </li>
                     <li>
                        <a class="btn-floating pink" href="javascript:void(0)" onclick="generateDiscard('dis_logout')" title="Logout"><i class="mdi mdi-logout"></i></a>
                     </li>
                  </ul>
               </div>
               <!--<a class="account_btn login_account" href="javascript:void(0)"><i class="mdi mdi-lock"></i></a>-->
               <a class='dropdown-button account_btn waves-effect' href='javascript:void(0)' data-activates='account_setting'><i class="zmdi zmdi-more-vert"></i></a>
               <!-- Dropdown Structure -->
               <ul id='account_setting' class='dropdown-content custom_dropdown account_custom_app'>
                  <li><a href="settings.php">Account Settings</a></li>
                  <li><a href="vip-member.php">VIP Member</a></li>
                  <li><a href="credit.php">Credits</a></li>
                  <li><a href="verify.php">Verification</a></li>
                  <li><a href="advertisement.php">Advertising Manager</a></li>
                  <li><a href="billing-info.php">Billing Information</a></li>
               </ul>
            </div>
            <div class="not-icons desktop">
               <div class="not-connections noticon">
                  <div class="dropdown dropdown-custom ">
                     <!-- Dropdown Trigger -->
                     <a class='dropdown-button more_btn connectcountinner' href='javascript:void(0)' data-activates='not_frndreq'>
                     <i class="mdi mdi-account-outline"></i>
                     <span class="new-notification">3</span>
                     </a>
                     <!-- Dropdown Structure -->
                     <ul id='not_frndreq' class='dropdown-content request_dropdown dropdown-menu'>
                        <li id="not_frndreq_prts_li">
                           <div class="fr-list not-area">
                              <span class="not-title">Connect Requests</span>
                              <div class="not-resultlist nice-scroll">
                                 <ul class="fr-listing">
                                    <li>
                                       <form>
                                          <div class="fr-holder">
                                             <div class="img-holder">
                                                <a href="javascript:void(0)"><img class="img-responsive" src="images/demo-profile.jpg"></a>
                                             </div>
                                             <div class="desc-holder">
                                                <div class="desc">
                                                   <a href="javascript:void(0)">Abc Def</a>
                                                   <span class="mf-info"></span>
                                                </div>
                                                <div class="fr-btn-holder">
                                                   <button class="btn btn-primary btn-sm">Confirm</button>
                                                   <button class="btn btn-primary btn-sm btn-gray">Delete</button>
                                                </div>
                                             </div>
                                          </div>
                                       </form>
                                    </li>
                                    <li>
                                       <form>
                                          <div class="fr-holder">
                                             <div class="img-holder">
                                                <a href="javascript:void(0)"><img class="img-responsive" src="images/male.png"></a>
                                             </div>
                                             <div class="desc-holder">
                                                <div class="desc">
                                                   <a href="javascript:void(0)">Abc Def</a>
                                                   <span class="mf-info"></span>
                                                </div>
                                                <div class="fr-btn-holder">
                                                   <button class="btn btn-primary btn-sm">Confirm</button>
                                                   <button class="btn btn-primary btn-sm btn-gray">Delete</button>
                                                </div>
                                             </div>
                                          </div>
                                       </form>
                                    </li>
                                    <li>
                                       <form>
                                          <div class="fr-holder">
                                             <div class="img-holder">
                                                <a href="javascript:void(0)"><img class="img-responsive" src="images/demo-profile.jpg"></a>
                                             </div>
                                             <div class="desc-holder">
                                                <div class="desc">
                                                   <a href="javascript:void(0)">Abc Def</a>
                                                   <span class="mf-info"></span>
                                                </div>
                                                <div class="fr-btn-holder">
                                                   <button class="btn btn-primary btn-sm">Confirm</button>
                                                   <button class="btn btn-primary btn-sm btn-gray">Delete</button>
                                                </div>
                                             </div>
                                          </div>
                                       </form>
                                    </li>
                                    <li>
                                       <form>
                                          <div class="fr-holder">
                                             <div class="img-holder">
                                                <a href="javascript:void(0)"><img class="img-responsive" src="images/male.png"></a>
                                             </div>
                                             <div class="desc-holder">
                                                <div class="desc">
                                                   <a href="javascript:void(0)">Abc Def</a>
                                                   <span class="mf-info"></span>
                                                </div>
                                                <div class="fr-btn-holder">
                                                   <button class="btn btn-primary btn-sm">Confirm</button>
                                                   <button class="btn btn-primary btn-sm btn-gray">Delete</button>
                                                </div>
                                             </div>
                                          </div>
                                       </form>
                                    </li>
                                    <li>
                                       <form>
                                          <div class="fr-holder">
                                             <div class="img-holder">
                                                <a href="javascript:void(0)"><img class="img-responsive" src="images/demo-profile.jpg"></a>
                                             </div>
                                             <div class="desc-holder">
                                                <div class="desc">
                                                   <a href="javascript:void(0)">Abc Def</a>
                                                   <span class="mf-info"></span>
                                                </div>
                                                <div class="fr-btn-holder">
                                                   <button class="btn btn-primary btn-sm">Confirm</button>
                                                   <button class="btn btn-primary btn-sm btn-gray">Delete</button>
                                                </div>
                                             </div>
                                          </div>
                                       </form>
                                    </li>
                                    <li>
                                       <form>
                                          <div class="fr-holder">
                                             <div class="img-holder">
                                                <a href="javascript:void(0)"><img class="img-responsive" src="images/male.png"></a>
                                             </div>
                                             <div class="desc-holder">
                                                <div class="desc">
                                                   <a href="javascript:void(0)">Abc Def</a>
                                                   <span class="mf-info"></span>
                                                </div>
                                                <div class="fr-btn-holder">
                                                   <button class="btn btn-primary btn-sm">Confirm</button>
                                                   <button class="btn btn-primary btn-sm btn-gray">Delete</button>
                                                </div>
                                             </div>
                                          </div>
                                       </form>
                                    </li>
                                    <li>
                                       <form>
                                          <div class="fr-holder">
                                             <div class="img-holder">
                                                <a href="javascript:void(0)"><img class="img-responsive" src="images/demo-profile.jpg"></a>
                                             </div>
                                             <div class="desc-holder">
                                                <div class="desc">
                                                   <a href="javascript:void(0)">Abc Def</a>
                                                   <span class="mf-info"></span>
                                                </div>
                                                <div class="fr-btn-holder">
                                                   <button class="btn btn-primary btn-sm">Confirm</button>
                                                   <button class="btn btn-primary btn-sm btn-gray">Delete</button>
                                                </div>
                                             </div>
                                          </div>
                                       </form>
                                    </li>
                                    <li>
                                       <form>
                                          <div class="fr-holder">
                                             <div class="img-holder">
                                                <a href="javascript:void(0)"><img class="img-responsive" src="images/male.png"></a>
                                             </div>
                                             <div class="desc-holder">
                                                <div class="desc">
                                                   <a href="javascript:void(0)">Abc Def</a>
                                                   <span class="mf-info"></span>
                                                </div>
                                                <div class="fr-btn-holder">
                                                   <button class="btn btn-primary btn-sm">Confirm</button>
                                                   <button class="btn btn-primary btn-sm btn-gray">Delete</button>
                                                </div>
                                             </div>
                                          </div>
                                       </form>
                                    </li>
                                    <li>
                                       <form>
                                          <div class="fr-holder">
                                             <div class="img-holder">
                                                <a href="javascript:void(0)"><img class="img-responsive" src="images/demo-profile.jpg"></a>
                                             </div>
                                             <div class="desc-holder">
                                                <div class="desc">
                                                   <a href="javascript:void(0)">Abc Def</a>
                                                   <span class="mf-info"></span>
                                                </div>
                                                <div class="fr-btn-holder">
                                                   <button class="btn btn-primary btn-sm">Confirm</button>
                                                   <button class="btn btn-primary btn-sm btn-gray">Delete</button>
                                                </div>
                                             </div>
                                          </div>
                                       </form>
                                    </li>
                                    <li>
                                       <form>
                                          <div class="fr-holder">
                                             <div class="img-holder">
                                                <a href="javascript:void(0)"><img class="img-responsive" src="images/male.png"></a>
                                             </div>
                                             <div class="desc-holder">
                                                <div class="desc">
                                                   <a href="javascript:void(0)">Abc Def</a>
                                                   <span class="mf-info"></span>
                                                </div>
                                                <div class="fr-btn-holder">
                                                   <button class="btn btn-primary btn-sm">Confirm</button>
                                                   <button class="btn btn-primary btn-sm btn-gray">Delete</button>
                                                </div>
                                             </div>
                                          </div>
                                       </form>
                                    </li>
                                    <li>
                                       <form>
                                          <div class="fr-holder">
                                             <div class="img-holder">
                                                <a href="javascript:void(0)"><img class="img-responsive" src="images/demo-profile.jpg"></a>
                                             </div>
                                             <div class="desc-holder">
                                                <div class="desc">
                                                   <a href="javascript:void(0)">Abc Def</a>
                                                   <span class="mf-info"></span>
                                                </div>
                                                <div class="fr-btn-holder">
                                                   <button class="btn btn-primary btn-sm">Confirm</button>
                                                   <button class="btn btn-primary btn-sm btn-gray">Delete</button>
                                                </div>
                                             </div>
                                          </div>
                                       </form>
                                    </li>
                                    <li>
                                       <form>
                                          <div class="fr-holder">
                                             <div class="img-holder">
                                                <a href="javascript:void(0)"><img class="img-responsive" src="images/male.png"></a>
                                             </div>
                                             <div class="desc-holder">
                                                <div class="desc">
                                                   <a href="javascript:void(0)">Abc Def</a>
                                                   <span class="mf-info"></span>
                                                </div>
                                                <div class="fr-btn-holder">
                                                   <button class="btn btn-primary btn-sm">Confirm</button>
                                                   <button class="btn btn-primary btn-sm btn-gray">Delete</button>
                                                </div>
                                             </div>
                                          </div>
                                       </form>
                                    </li>
                                    <li>
                                       <form>
                                          <div class="fr-holder">
                                             <div class="img-holder">
                                                <a href="javascript:void(0)"><img class="img-responsive" src="images/demo-profile.jpg"></a>
                                             </div>
                                             <div class="desc-holder">
                                                <div class="desc">
                                                   <a href="javascript:void(0)">Abc Def</a>
                                                   <span class="mf-info"></span>
                                                </div>
                                                <div class="fr-btn-holder">
                                                   <button class="btn btn-primary btn-sm">Confirm</button>
                                                   <button class="btn btn-primary btn-sm btn-gray">Delete</button>
                                                </div>
                                             </div>
                                          </div>
                                       </form>
                                    </li>
                                    <li>
                                       <form>
                                          <div class="fr-holder">
                                             <div class="img-holder">
                                                <a href="javascript:void(0)"><img class="img-responsive" src="images/male.png"></a>
                                             </div>
                                             <div class="desc-holder">
                                                <div class="desc">
                                                   <a href="javascript:void(0)">Abc Def</a>
                                                   <span class="mf-info"></span>
                                                </div>
                                                <div class="fr-btn-holder">
                                                   <button class="btn btn-primary btn-sm">Confirm</button>
                                                   <button class="btn btn-primary btn-sm btn-gray">Delete</button>
                                                </div>
                                             </div>
                                          </div>
                                       </form>
                                    </li>
                                 </ul>
                              </div>
                              <span class="not-result bshadow"><a href="javascript:void(0)">View All</a></span>
                           </div>
                        </li>
                     </ul>
                  </div>
               </div>
               <div class="not-messages noticon">
               <div class="not-notification noticon">
                  <div class="dropdown dropdown-custom ">
                     <!-- Dropdown Trigger -->
                     <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='not_notify'>
                     <i class="mdi mdi-bell-outline"></i>
                     <span class="new-notification">10</span>
                     </a>
                     <!-- Dropdown Structure -->
                     <ul id='not_notify' class='dropdown-content request_dropdown'>
                        <li>
                           <div class="noti-list not-area">
                              <span class="not-title">Notifications</span>
                              <div class="not-resultlist nice-scroll">
                                 <ul class="noti-listing">
                                    <li>
                                       <div class="noti-holder">
                                          <a href="javascript:void(0)">
                                             <span class="img-holder">
                                             <img class="img-responsive" src="images/demo-profile.jpg">
                                             </span>
                                             <span class="desc-holder">
                                                <span class="desc">
                                                <span class="btext">Adel Hasanat</span> wants you to be <span class="btext">Admin</span> for the group <span class="btext">PHP Developers</span>
                                                </span>
                                                <div class="btn-holder">
                                                   <button class="btn btn-primary btn-sm">Confirm</button>
                                                   <button class="btn btn-primary btn-sm btn-gray">Delete</button>
                                                </div>
                                                <span class="time-stamp">
                                                <i class="mdi mdi-earth"></i> Just Now
                                                </span>
                                             </span>
                                          </a>
                                       </div>
                                    </li>
                                    <li>
                                       <div class="noti-holder">
                                          <a href="javascript:void(0)">
                                          <span class="img-holder">
                                          <img class="img-responsive" src="images/demo-profile.jpg">
                                          </span>
                                          <span class="desc-holder">
                                          <span class="desc">
                                          <span class="btext">Abc Def</span> replied on your comment:
                                          </span>
                                          <span class="time-stamp">
                                          <i class="mdi mdi-earth"></i> Just Now
                                          </span>
                                          </span>
                                          </a>
                                       </div>
                                    </li>
                                    <li>
                                       <div class="noti-holder">
                                          <a href="javascript:void(0)">
                                          <span class="img-holder">
                                          <img class="img-responsive" src="images/demo-profile.jpg">
                                          </span>
                                          <span class="desc-holder">
                                          <span class="desc">
                                          <span class="btext">Abc Def</span> added a photo
                                          </span>
                                          <span class="time-stamp">
                                          <i class="mdi mdi-account"></i> 20 mins ago
                                          </span>
                                          </span>
                                          </a>
                                       </div>
                                    </li>
                                    <li>
                                       <div class="noti-holder">
                                          <a href="javascript:void(0)">
                                          <span class="img-holder">
                                          <img class="img-responsive" src="images/demo-profile.jpg">
                                          </span>
                                          <span class="desc-holder">
                                          <span class="desc">
                                          <span class="btext">Abc Def</span> replied on your comment:
                                          </span>
                                          <span class="time-stamp">
                                          <i class="mdi mdi-earth"></i> Just Now
                                          </span>
                                          </span>
                                          </a>
                                       </div>
                                    </li>
                                    <li>
                                       <div class="noti-holder">
                                          <a href="javascript:void(0)">
                                          <span class="img-holder">
                                          <img class="img-responsive" src="images/demo-profile.jpg">
                                          </span>
                                          <span class="desc-holder">
                                          <span class="desc">
                                          <span class="btext">Abc Def</span> added a photo
                                          </span>
                                          <span class="time-stamp">
                                          <i class="mdi mdi-account"></i> 20 mins ago
                                          </span>
                                          </span>
                                          </a>
                                       </div>
                                    </li>
                                    <li>
                                       <div class="noti-holder">
                                          <a href="javascript:void(0)">
                                          <span class="img-holder">
                                          <img class="img-responsive" src="images/demo-profile.jpg">
                                          </span>
                                          <span class="desc-holder">
                                          <span class="desc">
                                          <span class="btext">Abc Def</span> replied on your comment:
                                          </span>
                                          <span class="time-stamp">
                                          <i class="mdi mdi-earth"></i> Just Now
                                          </span>
                                          </span>
                                          </a>
                                       </div>
                                    </li>
                                    <li>
                                       <div class="noti-holder">
                                          <a href="javascript:void(0)">
                                          <span class="img-holder">
                                          <img class="img-responsive" src="images/demo-profile.jpg">
                                          </span>
                                          <span class="desc-holder">
                                          <span class="desc">
                                          <span class="btext">Abc Def</span> added a photo
                                          </span>
                                          <span class="time-stamp">
                                          <i class="mdi mdi-account"></i> 20 mins ago
                                          </span>
                                          </span>
                                          </a>
                                       </div>
                                    </li>
                                    <li>
                                       <div class="noti-holder">
                                          <a href="javascript:void(0)">
                                          <span class="img-holder">
                                          <img class="img-responsive" src="images/demo-profile.jpg">
                                          </span>
                                          <span class="desc-holder">
                                          <span class="desc">
                                          <span class="btext">Abc Def</span> replied on your comment:
                                          </span>
                                          <span class="time-stamp">
                                          <i class="mdi mdi-earth"></i> Just Now
                                          </span>
                                          </span>
                                          </a>
                                       </div>
                                    </li>
                                    <li>
                                       <div class="noti-holder">
                                          <a href="javascript:void(0)">
                                          <span class="img-holder">
                                          <img class="img-responsive" src="images/demo-profile.jpg">
                                          </span>
                                          <span class="desc-holder">
                                          <span class="desc">
                                          <span class="btext">Abc Def</span> added a photo
                                          </span>
                                          <span class="time-stamp">
                                          <i class="mdi mdi-account"></i> 20 mins ago
                                          </span>
                                          </span>
                                          </a>
                                       </div>
                                    </li>
                                    <li>
                                       <div class="noti-holder">
                                          <a href="javascript:void(0)">
                                          <span class="img-holder">
                                          <img class="img-responsive" src="images/demo-profile.jpg">
                                          </span>
                                          <span class="desc-holder">
                                          <span class="desc">
                                          <span class="btext">Abc Def</span> replied on your comment:
                                          </span>
                                          <span class="time-stamp">
                                          <i class="mdi mdi-earth"></i> Just Now
                                          </span>
                                          </span>
                                          </a>
                                       </div>
                                    </li>
                                    <li>
                                       <div class="noti-holder">
                                          <a href="javascript:void(0)">
                                          <span class="img-holder">
                                          <img class="img-responsive" src="images/demo-profile.jpg">
                                          </span>
                                          <span class="desc-holder">
                                          <span class="desc">
                                          <span class="btext">Abc Def</span> added a photo
                                          </span>
                                          <span class="time-stamp">
                                          <i class="mdi mdi-account"></i> 20 mins ago
                                          </span>
                                          </span>
                                          </a>
                                       </div>
                                    </li>
                                    <li>
                                       <div class="noti-holder">
                                          <a href="javascript:void(0)">
                                          <span class="img-holder">
                                          <img class="img-responsive" src="images/demo-profile.jpg">
                                          </span>
                                          <span class="desc-holder">
                                          <span class="desc">
                                          <span class="btext">Abc Def</span> replied on your comment:
                                          </span>
                                          <span class="time-stamp">
                                          <i class="mdi mdi-earth"></i> Just Now
                                          </span>
                                          </span>
                                          </a>
                                       </div>
                                    </li>
                                    <li>
                                       <div class="noti-holder">
                                          <a href="javascript:void(0)">
                                          <span class="img-holder">
                                          <img class="img-responsive" src="images/demo-profile.jpg">
                                          </span>
                                          <span class="desc-holder">
                                          <span class="desc">
                                          <span class="btext">Abc Def</span> added a photo
                                          </span>
                                          <span class="time-stamp">
                                          <i class="mdi mdi-account"></i> 20 mins ago
                                          </span>
                                          </span>
                                          </a>
                                       </div>
                                    </li>
                                    <li>
                                       <div class="noti-holder">
                                          <a href="javascript:void(0)">
                                          <span class="img-holder">
                                          <img class="img-responsive" src="images/demo-profile.jpg">
                                          </span>
                                          <span class="desc-holder">
                                          <span class="desc">
                                          <span class="btext">Abc Def</span> replied on your comment:
                                          </span>
                                          <span class="time-stamp">
                                          <i class="mdi mdi-earth"></i> Just Now
                                          </span>
                                          </span>
                                          </a>
                                       </div>
                                    </li>
                                    <li>
                                       <div class="noti-holder">
                                          <a href="javascript:void(0)">
                                          <span class="img-holder">
                                          <img class="img-responsive" src="images/demo-profile.jpg">
                                          </span>
                                          <span class="desc-holder">
                                          <span class="desc">
                                          <span class="btext">Abc Def</span> added a photo
                                          </span>
                                          <span class="time-stamp">
                                          <i class="mdi mdi-account"></i> 20 mins ago
                                          </span>
                                          </span>
                                          </a>
                                       </div>
                                    </li>
                                    <li>
                                       <div class="noti-holder">
                                          <a href="javascript:void(0)">
                                          <span class="img-holder">
                                          <img class="img-responsive" src="images/demo-profile.jpg">
                                          </span>
                                          <span class="desc-holder">
                                          <span class="desc">
                                          <span class="btext">Abc Def</span> replied on your comment:
                                          </span>
                                          <span class="time-stamp">
                                          <i class="mdi mdi-earth"></i> Just Now
                                          </span>
                                          </span>
                                          </a>
                                       </div>
                                    </li>
                                    <li>
                                       <div class="noti-holder">
                                          <a href="javascript:void(0)">
                                          <span class="img-holder">
                                          <img class="img-responsive" src="images/demo-profile.jpg">
                                          </span>
                                          <span class="desc-holder">
                                          <span class="desc">
                                          <span class="btext">Abc Def</span> added a photo
                                          </span>
                                          <span class="time-stamp">
                                          <i class="mdi mdi-account"></i> 20 mins ago
                                          </span>
                                          </span>
                                          </a>
                                       </div>
                                    </li>
                                 </ul>
                                 <span class="not-result bshadow"><a href="javascript:void(0)">View All</a></span>
                              </div>
                           </div>
                        </li>
                     </ul>
                  </div>
               </div>
            </div>
            <div class="<?php echo $search_holder; ?>">
               <div class="search-section">
                  <form>

               <?php if($file == "places"){ ?>
               <input  data-query="M" id="icon_telephone"  onfocus="filderMapLocationModal(this)" autocomplete="off"  type="text" placeholder="Enter your search term..." class="search-input" style="width:0;">
               <?php } ?>						
			 
               <?php if($file !== "places"){ ?>
                     <input autocomplete="off"  type="text" placeholder="Enter your search term..." class="search-input" style="width:0;">
                     <?php } ?><span class="search-btn">   
                     <input type="text" value="" class="search-submit">	
                     <i class="mdi mdi-magnify"></i>
                     </span>
					<?php if($file !== "places"){ ?>
				   <?php include('common/search-data.php'); ?>  <?php } ?>	
                  </form>
               </div>
            </div>
         </div>
      </div>
   </div>