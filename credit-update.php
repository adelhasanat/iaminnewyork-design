<?php include("header.php"); ?>
<div class="floating-icon">
   <div class="scrollup-btnbox anim-side btnbox scrollup-float">
      <div class="scrollup-button float-icon"><span class="icon-holder ispan"><i class="mdi mdi-arrow-up-bold-circle"></i>
</span></div>
   </div>
</div>
<div class="clear"></div>
<?php include("common/leftmenu.php"); ?>
<div class="fixed-layout ipad-mfix">
   <div class="main-content sub-page credit-page main-page p-0">
      <div class="combined-column wide-open">
         <div class="content-box m-0">
            <div class="cbox-desc nobg">
               <div class="ribbon-section">
                  <h3 class="ribbon-img">i am in <?=$st_nm_L?> credits</h3>
                  <p>I am in <?=$st_nm_L?> credits are virtual currency, <br />which can be used to buy extended services on I am in <?=$st_nm_L?></p>
               </div>
               <div class="container">
                  <div class="credit-summery">
                     <div class="credit-balance left">
                        <h4>Your Iamin<?=$st_nm_L?> Credit Balance</h4>
                        <span class="badge">
                        <span>3</span>
                        <span>5</span>
                        <span>7</span>
                        </span>
                     </div>
                     <div class="add-credit">
                        <!-- <a href="#payment-popup" class="popup-modal btn green-styled-btn">Add Credit</a> -->
                        <a class="waves-effect waves-light btn modal-trigger" href="#payment-popup">Add Credit</a>
                        <div class="clear"></div>
                        <div class="dropdown dropdown-custom lmenu">
                           <a href="javascript:void(0)" class="dropdown-toggle dropdown-button" data-activates='dropdown1'>
                           See History <i class="mdi mdi-chevron-down"></i>
                           </a>
                           <ul id='dropdown1' class='dropdown-content'>
                              <li><a href="credit.php">Credit Benifits</a></li>
                              <li><a href="credit-update.php">See History</a></li>
                              <li><a href="credit-transfer.php">Tranfer Credits</a></li>
                           </ul>
                        </div>
                     </div>
                  </div>
                  <div class="credit-updates">
                     <div class="section-title">
                        <h5>You have <span>8</span> credits updates</h5>
                     </div>
                     <ul class="cupdates-ul">
                        <li>
                           <div class="cupdates-box">
                              <div class="imgholder"><img src="images/credit-img.png"/></div>
                              <div class="descholder">
                                 <div class="desc">
                                    <h5 class="credit-compli">Used</h5>
                                    <p>You used 20 credits by transferring credits to Jack</p>
                                 </div>
                                 <div class="timestamp">
                                    15 Jul 2016                                     
                                 </div>
                              </div>
                           </div>
                        </li>
                        <li>
                           <div class="cupdates-box">
                              <div class="imgholder"><img src="images/credit-img.png"/></div>
                              <div class="descholder">
                                 <div class="desc">
                                    <h5 class="credit-compli">Earned</h5>
                                    <p>You earned 250 credits by becoming a new member in travelbuddy</p>
                                 </div>
                                 <div class="timestamp">
                                    15 Jul 2016                                     
                                 </div>
                              </div>
                           </div>
                        </li>
                        <li>
                           <div class="cupdates-box">
                              <div class="imgholder"><img src="images/credit-img.png"/></div>
                              <div class="descholder">
                                 <div class="desc">
                                    <h5 class="credit-compli">Earned</h5>
                                    <p>You earned 4 credits for adding 4 photos</p>
                                 </div>
                                 <div class="timestamp">
                                    15 Jul 2016													
                                 </div>
                              </div>
                           </div>
                        </li>
                        <li>
                           <div class="cupdates-box">
                              <div class="imgholder"><img src="images/credit-img.png"/></div>
                              <div class="descholder">
                                 <div class="desc">
                                    <h5 class="credit-compli">Used</h5>
                                    <p>You used 4 credits for apply new theme</p>
                                 </div>
                                 <div class="timestamp">
                                    15 Jul 2016													
                                 </div>
                              </div>
                           </div>
                        </li>
                        <li>
                           <div class="cupdates-box">
                              <div class="imgholder"><img src="images/credit-img.png"/></div>
                              <div class="descholder">
                                 <div class="desc">
                                    <h5 class="credit-compli">Purchased</h5>
                                    <p>You bought 500 credits</p>
                                 </div>
                                 <div class="timestamp">
                                    15 Jul 2016													
                                 </div>
                              </div>
                           </div>
                        </li>
                        <li>
                           <div class="cupdates-box">
                              <div class="imgholder"><img src="images/credit-img.png"/></div>
                              <div class="descholder">
                                 <div class="desc">
                                    <h5 class="credit-compli">Earned</h5>
                                    <p>You earned 4 credits for adding 4 photos</p>
                                 </div>
                                 <div class="timestamp">
                                    15 Jul 2016													
                                 </div>
                              </div>
                           </div>
                        </li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
         <div class="credit-exchanged">
            Total credit exchanged <span>230 credits</span>
         </div>
      </div>
      <?php include('common/chat.php'); ?>
   </div>
</div>
<?php include("common/footer.php"); ?>
</div>	
<div id="payment-popup" class="modal payment-popup credit-payment-modal fullpopup">
<?php include('common/credit_payment_popup.php'); ?>
</div>
<?php include("script.php"); ?>
</body>
</html>