
<?php include("header.php"); ?>
<div class="floating-icon">
   <div class="scrollup-btnbox anim-side btnbox scrollup-float">
      <div class="scrollup-button float-icon"><span class="icon-holder ispan"><i class="mdi mdi-arrow-up-bold-circle"></i>
</span></div>
   </div> 
</div>
<div class="clear"></div>
<div class="container page_container">
   <?php include("common/leftmenu.php"); ?>
   <div class="fixed-layout">
      <div id="notification-layout" class="main-content with-lmenu sub-page notification">
         <div class="combined-column">
            <div class="content-box bshadow">
               <div class="cbox-title">						
                  Notifications
               </div>
               <div class="fullside-list">
                  <ul class="noti-listing">
                     <li class="mainli">
                        <div class="noti-holder">
                           <a href="javascript:void(0)">
                           <span class="img-holder">
                           <img src="images/demo-profile.jpg" class="img-responsive">
                           </span>
                           <span class="desc-holder">
                           <span class="desc">
                           <span class="btext">Abc Def</span> replied on your comment:
                           </span>
                           <span class="time-stamp">
                           <i class="zmdi zmdi-globe"></i> Just Now
                           </span>											
                           </span>
                           </a>
                           <div class="dropdown dropdown-custom">
                              <a class="dropdown-button nothemecolor" href="javascript:void(0)" data-activates="notification">
                              <i class="zmdi zmdi-more-vert mdi-18px"></i>
                              </a>
                              <ul id="notification" class="dropdown-content custom_dropdown">
                                 <li><a href="javascript:void(0)">Hide this notification</a></li>
                                 <li><a href="javascript:void(0)">Turn off notifications</a></li>
                              </ul>
                           </div>
                           <a href="javascript:void(0)" onclick="markNotRead(this)" class="readicon nothemecolor"><i class="mdi mdi-bullseye"></i></a>
                        </div>
                     </li>
                     <li class="mainli">
                        <div class="noti-holder">
                           <a href="javascript:void(0)">
                           <span class="img-holder">
                           <img src="images/demo-profile.jpg" class="img-responsive">
                           </span>
                           <span class="desc-holder">
                           <span class="desc">
                           <span class="btext">Abc Def</span> added a photo
                           </span>
                           <span class="time-stamp">
                           <i class="zmdi zmdi-account"></i> 20 mins ago
                           </span>
                           </span>
                           </a>
                           <div class="dropdown dropdown-custom">
                              <a class="dropdown-button nothemecolor" href="javascript:void(0)" data-activates="notification2">
                              <i class="zmdi zmdi-more-vert mdi-18px"></i>
                              </a>
                              <ul id="notification2" class="dropdown-content custom_dropdown">
                                 <li><a href="javascript:void(0)">Hide this notification</a></li>
                                 <li><a href="javascript:void(0)">Turn off notifications</a></li>
                              </ul>
                           </div>
                           <a href="javascript:void(0)" onclick="markNotRead(this)" class="readicon nothemecolor"><i class="mdi mdi-bullseye"></i></a>
                        </div>
                     </li>
                     <li class="mainli">
                        <div class="noti-holder">
                           <a href="javascript:void(0)">
                           <span class="img-holder">
                           <img src="images/demo-profile.jpg" class="img-responsive">
                           </span>
                           <span class="desc-holder">
                           <span class="desc">
                           <span class="btext">Abc Def</span> replied on your comment:
                           </span>
                           <span class="time-stamp">
                           <i class="zmdi zmdi-globe"></i> Just Now
                           </span>
                           </span>
                           </a>
                           <div class="dropdown dropdown-custom">
                              <a class="dropdown-button nothemecolor" href="javascript:void(0)" data-activates="notification3">
                              <i class="zmdi zmdi-more-vert mdi-18px"></i>
                              </a>
                              <ul id="notification3" class="dropdown-content custom_dropdown">
                                 <li><a href="javascript:void(0)">Hide this notification</a></li>
                                 <li><a href="javascript:void(0)">Turn off notifications</a></li>
                              </ul>
                           </div>
                           <a href="javascript:void(0)" onclick="markNotRead(this)" class="readicon nothemecolor"><i class="mdi mdi-bullseye"></i></a>
                        </div>
                     </li>
                     <li class="mainli">
                        <div class="noti-holder">
                           <a href="javascript:void(0)">
                           <span class="img-holder">
                           <img src="images/demo-profile.jpg" class="img-responsive">
                           </span>
                           <span class="desc-holder">
                           <span class="desc">
                           <span class="btext">Abc Def</span> added a photo
                           </span>
                           <span class="time-stamp">
                           <i class="zmdi zmdi-account"></i> 20 mins ago
                           </span>
                           </span>
                           </a>
                           <div class="dropdown dropdown-custom">
                              <a class="dropdown-button nothemecolor" href="javascript:void(0)" data-activates="notification4">
                              <i class="zmdi zmdi-more-vert mdi-18px"></i>
                              </a>
                              <ul id="notification4" class="dropdown-content custom_dropdown">
                                 <li><a href="javascript:void(0)">Hide this notification</a></li>
                                 <li><a href="javascript:void(0)">Turn off notifications</a></li>
                              </ul>
                           </div>
                           <a href="javascript:void(0)" onclick="markNotRead(this)" class="readicon nothemecolor"><i class="mdi mdi-bullseye"></i></a>
                        </div>
                     </li>
                     <li class="mainli">
                        <div class="noti-holder">
                           <a href="javascript:void(0)">
                           <span class="img-holder">
                           <img src="images/demo-profile.jpg" class="img-responsive">
                           </span>
                           <span class="desc-holder">
                           <span class="desc">
                           <span class="btext">Abc Def</span> replied on your comment:
                           </span>
                           <span class="time-stamp">
                           <i class="zmdi zmdi-globe"></i> Just Now
                           </span>
                           </span>
                           </a>
                           <div class="dropdown dropdown-custom">
                              <a class="dropdown-button nothemecolor" href="javascript:void(0)" data-activates="notification5">
                              <i class="zmdi zmdi-more-vert mdi-18px"></i>
                              </a>
                              <ul id="notification5" class="dropdown-content custom_dropdown">
                                 <li><a href="javascript:void(0)">Hide this notification</a></li>
                                 <li><a href="javascript:void(0)">Turn off notifications</a></li>
                              </ul>
                           </div>
                           <a href="javascript:void(0)" onclick="markNotRead(this)" class="readicon nothemecolor"><i class="mdi mdi-bullseye"></i></a>
                        </div>
                     </li>
                     <li class="mainli">
                        <div class="noti-holder">
                           <a href="javascript:void(0)">
                           <span class="img-holder">
                           <img src="images/demo-profile.jpg" class="img-responsive">
                           </span>
                           <span class="desc-holder">
                           <span class="desc">
                           <span class="btext">Abc Def</span> added a photo
                           </span>
                           <span class="time-stamp">
                           <i class="zmdi zmdi-account"></i> 20 mins ago
                           </span>
                           </span>
                           </a>
                           <div class="dropdown dropdown-custom">
                              <a class="dropdown-button nothemecolor" href="javascript:void(0)" data-activates="notification6">
                              <i class="zmdi zmdi-more-vert mdi-18px"></i>
                              </a>
                              <ul id="notification6" class="dropdown-content custom_dropdown">
                                 <li><a href="javascript:void(0)">Hide this notification</a></li>
                                 <li><a href="javascript:void(0)">Turn off notifications</a></li>
                              </ul>
                           </div>
                           <a href="javascript:void(0)" onclick="markNotRead(this)" class="readicon nothemecolor"><i class="mdi mdi-bullseye"></i></a>
                        </div>
                     </li>
                  </ul>
               </div>
            </div>
         </div>
         <?php include('common/chat.php'); ?>
      </div>
   </div>
</div>
<?php include("common/footer.php"); ?>
</div>	
<?php include("script.php"); ?>
</body>
</html>