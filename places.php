<?php include("header.php"); ?>

<div class="floating-icon">
<div class="scrollup-btnbox anim-side btnbox scrollup-float">
<div class="scrollup-button float-icon"><span class="icon-holder ispan"><i class="mdi mdi-arrow-up-bold-circle"></i>
</span></div>
</div>
</div>
<?php include("common/leftmenu.php"); ?>
<div class="clear"></div>
<div class="fixed-layout ipad-mfix hide-addflow">
<div class="main-content main-page places-page pb-0">
<div class="places-mapview hasfitler">
   <div class="map-filter">
      <div class="fitem">
         <label>Sort By</label>
         <div class="compo">
            <select class="select2" tabindex="-1" >
               <option>Pricing</option>
               <option>Ratings</option>
            </select>
         </div>
      </div>
      <div class="fitem">
         <label>Hotel Class</label>
         <div class="compo">
            <select class="select2" tabindex="-1" >
               <option>5 Stars</option>
               <option>4 Stars</option>
               <option>3 Stars</option>
               <option>2 Stars</option>
               <option>1 Star</option>
            </select>
         </div>
      </div>
      <div class="fitem">
         <label>Guest Ratings</label>
         <div class="compo">
            <select class="select2" tabindex="-1" >
               <option>5 Checks</option>
               <option>4 Checks</option>
               <option>3 Checks</option>
               <option>2 Checks</option>
               <option>1 Check</option>
            </select>
         </div>
      </div>
      <div class="fitem">
         <label>Nightly Price</label>
         <div class="compo">
            <div class="">
               <input type="text" class="amount" readonly>
               <div class="slider-range"></div>
               <div class="min-value">$500</div>
               <div class="max-value">$10,000</div>
            </div>
         </div>
      </div>
      <div class="fitem">
         <label>Amenities</label>
         <div class="compo amenities">
            <select class="select2" tabindex="-1"  multiple>
               <option>Spa</option>
               <option>Beach</option>
               <option>Wifi</option>
               <option>Breakfast</option>
               <option>Pool</option>
               <option>Transport</option>
            </select>
         </div>
      </div>
   </div>
   <div class="list-box moreinfo-outer">
      <div class="sidetitle">
         <h4>Hotels in <?=$st_nm_S?></h4>
      </div>
      <div class="sidelist nice-scroll">
         <div class="hotels-page">
            <div class="hotel-list">
               <ul>
                  <li>
                     <div class="hotel-li">
                        <a href="javascript:void(0)" class="summery-info" onclick="openPlacesMoreInfo(this)">
                           <div class="imgholder himg-box"><img src="images/hotel1.png" class="himg" /></div>
                           <div class="descholder">
                              <h4>The Guest House</h4>
                              <div class="clear"></div>
                              <div class="reviews-link">
                                 <span class="checks-holder">
                                 <i class="mdi mdi-star active"></i>
                                 <i class="mdi mdi-star active"></i>
                                 <i class="mdi mdi-star active"></i>
                                 <i class="mdi mdi-star active"></i>
                                 <i class="mdi mdi-star"></i>
                                 <label>34 Reviews</label>
                                 </span>
                              </div>
                           </div>
                        </a>
                     </div>
                  </li>
                  <li>
                     <div class="hotel-li">
                        <a href="javascript:void(0)" class="summery-info" onclick="openPlacesMoreInfo(this)">
                           <div class="imgholder himg-box"><img src="images/hotel2.png" class="himg" /></div>
                           <div class="descholder">
                              <h4>Movenpick Resort</h4>
                              <div class="clear"></div>
                              <div class="reviews-link">
                                 <span class="checks-holder">
                                 <i class="mdi mdi-star active"></i>
                                 <i class="mdi mdi-star active"></i>
                                 <i class="mdi mdi-star active"></i>
                                 <i class="mdi mdi-star active"></i>
                                 <i class="mdi mdi-star"></i>
                                 <label>34 Reviews</label>
                                 </span>
                              </div>
                           </div>
                        </a>
                     </div>
                  </li>
                  <li>
                     <div class="hotel-li">
                        <a href="javascript:void(0)" class="summery-info" onclick="openPlacesMoreInfo(this)">
                           <div class="imgholder himg-box"><img src="images/hotel3.png" class="himg" /></div>
                           <div class="descholder">
                              <h4><?=$st_nm_S?> Hotel</h4>
                              <div class="clear"></div>
                              <div class="reviews-link">
                                 <span class="checks-holder">
                                 <i class="mdi mdi-star active"></i>
                                 <i class="mdi mdi-star active"></i>
                                 <i class="mdi mdi-star active"></i>
                                 <i class="mdi mdi-star active"></i>
                                 <i class="mdi mdi-star"></i>
                                 <label>34 Reviews</label>
                                 </span>
                              </div>
                           </div>
                        </a>
                     </div>
                  </li>
               </ul>
            </div>
         </div>
      </div>
      <div class="moreinfo-box">
         <a href="javascript:void(0)" onclick="closePlacesMoreInfo(this)" class="backarrow"><i class="mdi mdi-arrow-left-bold-circle"></i></a>
         <div class="infoholder nice-scroll">
            <div class="imgholder"><img src="images/hotel1.png" /></div>
            <div class="descholder">
               <h4>The Guest House</h4>
               <div class="clear"></div>
               <div class="reviews-link">
                  <span class="checks-holder">
                  <i class="mdi mdi-star active"></i>
                  <i class="mdi mdi-star active"></i>
                  <i class="mdi mdi-star active"></i>
                  <i class="mdi mdi-star active"></i>
                  <i class="mdi mdi-star"></i>
                  <label>34 Reviews</label>
                  </span>
               </div>
               <span class="distance-info">Middle Eastem &amp; African, Mediterranean</span>
               <div class="clear"></div>
               <div class="more-holder">
                  <ul class="infoul">
                     <li>
                        <i class="zmdi zmdi-pin"></i>
                        132 Brick Lane | E1 6RU, <?=$st_nm_S?> E1 6RU
                     </li>
                     <li>
                        <i class="mdi mdi-phone"></i>
                        +44 20 7247 8210
                     </li>
                     <li>
                        <i class="mdi mdi-earth"></i>
                        http://www.yourwebsite.com
                     </li>
                     <li>
                        <i class="mdi mdi-clock-outline"></i>
                        Today, 12:00 PM - 12:00 AM
                     </li>
                     <li>
                        <i class="mdi mdi-certificate "></i>
                        Ranked #1 in <?=$st_nm_S?> Hotels
                     </li>
                  </ul>
                  <div class="tagging" onclick="explandTags(this)">
                     Popular with:
                     <span>Budget</span>
                     <span>Foodies</span>
                     <span>Family</span>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="map-box">
      <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3110.3465133386144!2d-9.167423685010494!3d38.77868997958898!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xd193295d5b45545%3A0x3f9e7b6a5f00e12c!2sPerta!5e0!3m2!1sen!2sin!4v1481089901870" width="600" height="450" frameborder="0" allowfullscreen></iframe>
      <div class="overlay">
         <a href="javascript:void(0)" onclick="closeMapView()">Back to list view</a>
      </div>
   </div>
</div>
<div class="combined-column wide-open main-page full-page">
<div class="places-banner" style="background:url(images/placebanner.jpg) center center no-repeat;background-attachment:cover;">
   <div class="overlay"></div>
   <div class="banner-section">
      <div class="container">
         <div class="tabicon header-icon-tabs">
            <ul class="tabs icon-menu tabsnew nav-tabs">
               <li class="tab"><a href="#places-lodge"><span class="icon lodge-icon"><img src="images/lodge-icon.png" /></span><span class="caption">Hotels</span></a></li>
               <li class="tab"><a href="#places-dine"><span class="icon dine-icon"><img src="images/dine-icon.png" /></span><span class="caption">Restaurants</span></a></li>
               <li class="tab"><a href="#places-todo"><span class="icon todo-icon"><img src="images/todo-icon.png" /></span><span class="caption">To Do</span></a></li>
            </ul>
         </div>
         <h4>Restaurent in <?=$st_nm_S?></h4>
         <div class="places-tabs">
            <div class="tablist sub-tabs">
               <ul class="tabs tabs-fixed-width text-menu left tabsnew">
                  <li class="tab"><a tabname="Wall" href="#places-all"><?=$st_nm_S?></a></li>
                  <li class="tab"><a tabname="Discussion" href="#places-discussion">Discussion</a></li>
                  <li class="tab"><a tabname="Reviews" href="#places-reviews">Reviews</a></li>
                  <li class="tab"><a tabname="Travellers" href="#places-travellers" onclick="fixPlacesImageUI('.placestravellers-content')">Travellers</a></li>
                  <li class="tab"><a tabname="Locals" href="#places-locals" onclick="fixPlacesImageUI('.placeslocals-content')">Locals</a></li>
                  <li class="tab places-photos"><a tabname="Photos" href="#places-photos">Photos</a></li>
                  <li class="tab"><a tabname="Tips" href="#places-tip">Tips</a></li>
                  <li class="tab"><a tabname="Ask" href="#places-ask">Ask</a></li>
               </ul>
            </div>
         </div>
         <div class="banner-buttons">
            <a href="javascript:void(0)" class="btn btn-border waves-effect">I&apos;ve been there</a>
            <a href="javascript:void(0)" class="btn btn-border waves-effect">I want to visit</a>
         </div>
      </div>
      <div class="mbl-banner-buttons">
         <a href="javascript:void(0)" class="arrowbtn" onclick="manageBannerButtons(this)"><i class="mdi mdi-chevron-down"></i></a>
         <div class="btn-holder">
            <a href="javascript:void(0)" class="btn btn-border active">I&apos;ve been there</a>
            <a href="javascript:void(0)" class="btn btn-border waves-effect">I want to visit</a>
         </div>
      </div>
   </div>
</div>
<div class="places-content places-all">
   <div class="container cshfsiput">
      <div class="places-column cshfsiput m-top">
         <div class="tab-content">
            <div id="places-all" class="placesall-content bottom_tabs">
               <div class="content-box">
                  <div class="placesection yellowsection">
                     <div class="tcontent-holder">
                        <div class="cbox-title m-t nborder" id="hotel-title">
                           <div class="valign-wrapper left">
                              <a href="javascript:void(0)">
                                 <img src="images/lodgeicon-sm.png" />
                              </a>
                              <span>Popular Hotel in <?=$st_nm_S?></span>
                           </div>
                           <div class="top-stuff right">
                              <div class="more-actions">
                                 <a href="javascript:void(0)" class="viewall-link clicable" onclick="openDirectTab('places-lodge')">View All</a>
                                 <ul class="tabs nav-tabs nav-custom-tabs tabsnew valign-wrapper">
                                 </ul>
                              </div>
                           </div>
                        </div>
                        <div class="cbox-desc">
                           <div class="places-content-holder">
                              <div class="list-holder">
                                 <div class="row">
                                    <div class="col m4 s12 pb-holder">
                                       <div class="placebox">
                                          <a href="javascript:void(0)">
                                             <div class="imgholder himg-box">
                                                <img src="images/lodge-img-1.jpg" class="himg imgfix" />
                                                <div class="overlay"></div>
                                             </div>
                                             <div class="descholder">
                                                <h5>Moeavenpick Resort <?=$st_nm_S?></h5>
                                                <span class="ratings">
                                                <i class="mdi mdi-star active"></i>
                                                <i class="mdi mdi-star active"></i>
                                                <i class="mdi mdi-star active"></i>
                                                <i class="mdi mdi-star"></i>
                                                <i class="mdi mdi-star"></i>
                                                <label>45 Reviews</label>
                                                </span>
                                                <div class="tags">
                                                   <span>Luxury</span>
                                                   <span>Families</span>
                                                </div>
                                             </div>
                                          </a>
                                       </div>
                                    </div>
                                    <div class="col m4 s12 pb-holder">
                                       <div class="placebox">
                                          <a href="javascript:void(0)">
                                             <div class="imgholder himg-box">
                                                <img src="images/lodge-img-2.jpg" class="himg" />
                                                <div class="overlay"></div>
                                             </div>
                                             <div class="descholder">
                                                <h5><?=$st_nm_S?> Moon Hotel</h5>
                                                <span class="ratings">
                                                <i class="mdi mdi-star active"></i>
                                                <i class="mdi mdi-star active"></i>
                                                <i class="mdi mdi-star active"></i>
                                                <i class="mdi mdi-star active"></i>
                                                <i class="mdi mdi-star"></i>
                                                <label>20 Reviews</label>
                                                </span>
                                                <div class="tags">
                                                   <span>Budget</span>
                                                </div>
                                             </div>
                                          </a>
                                       </div>
                                    </div>
                                    <div class="col m4 s12 pb-holder third-col">
                                       <div class="placebox">
                                          <a href="javascript:void(0)">
                                             <div class="imgholder himg-box">
                                                <img src="images/lodge-img-3.jpg" class="himg" />
                                                <div class="overlay"></div>
                                             </div>
                                             <div class="descholder">
                                                <h5>Marriott <?=$st_nm_S?> Hotel</h5>
                                                <span class="ratings">
                                                <i class="mdi mdi-star active"></i>
                                                <i class="mdi mdi-star active"></i>
                                                <i class="mdi mdi-star active"></i>
                                                <i class="mdi mdi-star active"></i>
                                                <i class="mdi mdi-star active"></i>
                                                <label>65 Reviews</label>
                                                </span>
                                                <div class="tags">
                                                   <span>Luxury</span>
                                                   <span>Business</span>
                                                </div>
                                             </div>
                                          </a>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="placesection redsection">
                     <div class="tcontent-holder">
                        <div class="cbox-title nborder" id="restaurant-title">
                           <div class="valign-wrapper left">
                              <img src="images/dineicon-sm.png" />
                              <span>Popular Restaurants in <?=$st_nm_S?></span>
                           </div>
                           <div class="top-stuff right">
                              <a href="javascript:void(0)" class="viewall-link clicable" onclick="openDirectTab('places-dine')">View All</a>
                              <div class="more-actions">
                                 <ul class="tabs nav-tabs nav-custom-tabs tabsnew valign-wrapper">
                                 </ul>
                              </div>
                           </div>
                        </div>
                        <div class="cbox-desc">
                           <div class="places-content-holder">
                              <div class="list-holder">
                                 <div class="row">
                                    <div class="col m4 s12 pb-holder">
                                       <div class="placebox">
                                          <a href="javascript:void(0)">
                                             <div class="imgholder himg-box">
                                                <img src="images/dine-img-1.jpg" class="himg" />
                                                <div class="overlay"></div>
                                             </div>
                                             <div class="descholder">
                                                <h5>Alqantarah restaurant</h5>
                                                <span class="ratings">
                                                <i class="mdi mdi-star active"></i>
                                                <i class="mdi mdi-star active"></i>
                                                <i class="mdi mdi-star active"></i>
                                                <i class="mdi mdi-star"></i>
                                                <i class="mdi mdi-star"></i>
                                                <label>45 Reviews</label>
                                                </span>
                                             </div>
                                          </a>
                                       </div>
                                    </div>
                                    <div class="col m4 s12 pb-holder">
                                       <div class="placebox">
                                          <a href="javascript:void(0)">
                                             <div class="imgholder himg-box">
                                                <img src="images/dine-img-2.jpg" class="himg" />
                                                <div class="overlay"></div>
                                             </div>
                                             <div class="descholder">
                                                <h5>The Basin Restaurant</h5>
                                                <span class="ratings">
                                                <i class="mdi mdi-star active"></i>
                                                <i class="mdi mdi-star active"></i>
                                                <i class="mdi mdi-star active"></i>
                                                <i class="mdi mdi-star active"></i>
                                                <i class="mdi mdi-star"></i>
                                                <label>20 Reviews</label>
                                                </span>
                                                <div class="tags">
                                                   <span>Foodies</span>
                                                   <span>Adventure</span>
                                                </div>
                                             </div>
                                          </a>
                                       </div>
                                    </div>
                                    <div class="col m4 s12 pb-holder third-col">
                                       <div class="placebox">
                                          <a href="javascript:void(0)">
                                             <div class="imgholder himg-box">
                                                <img src="images/dine-img-3.jpg" class="himg" />
                                                <div class="overlay"></div>
                                             </div>
                                             <div class="descholder">
                                                <h5>The <?=$st_nm_S?> Kitchen</h5>
                                                <span class="ratings">
                                                <i class="mdi mdi-star active"></i>
                                                <i class="mdi mdi-star active"></i>
                                                <i class="mdi mdi-star active"></i>
                                                <i class="mdi mdi-star active"></i>
                                                <i class="mdi mdi-star active"></i>
                                                <label>65 Reviews</label>
                                                </span>
                                             </div>
                                          </a>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="placesection bluesection">
                     <div class="tcontent-holder">
                        <div class="cbox-title nborder" id="todo-title">
                           <div class="valign-wrapper left">
                              <a href="javascript:void(0)">
                              <img src="images/todoicon-sm.png" /></a>
                              <span>Popular Attractions in <?=$st_nm_S?></span>
                           </div>
                           <div class="top-stuff right">
                              <a href="javascript:void(0)" class="viewall-link" onclick="openDirectTab('places-todo')">View All</a>
                              <div class="more-actions">
                                 <ul class="tabs nav-tabs nav-custom-tabs tabsnew valign-wrapper">
                                 </ul>
                              </div>
                           </div>
                        </div>
                        <div class="cbox-desc">
                           <div class="places-content-holder">
                              <div class="list-holder">
                                 <div class="row">
                                    <div class="col m4 s12 pb-holder">
                                       <div class="placebox">
                                          <a href="javascript:void(0)">
                                             <div class="imgholder himg-box">
                                                <img src="images/todo-img-1.jpg" class="himg" />
                                                <div class="overlay"></div>
                                             </div>
                                             <div class="descholder">
                                                <h5><?=$st_nm_S?></h5>
                                                <span class="ratings">
                                                <i class="mdi mdi-star active"></i>
                                                <i class="mdi mdi-star active"></i>
                                                <i class="mdi mdi-star active"></i>
                                                <i class="mdi mdi-star"></i>
                                                <i class="mdi mdi-star"></i>
                                                <label>45 Reviews</label>
                                                </span>
                                                <div class="tags">
                                                   <span>History</span>
                                                   <span>Adventure</span>
                                                </div>
                                             </div>
                                          </a>
                                       </div>
                                    </div>
                                    <div class="col m4 s12 pb-holder">
                                       <div class="placebox">
                                          <a href="javascript:void(0)">
                                             <div class="imgholder himg-box">
                                                <img src="images/todo-img-2.jpg" class="himg" />
                                                <div class="overlay"></div>
                                             </div>
                                             <div class="descholder">
                                                <h5>The Treasury - AI Khazneh</h5>
                                                <span class="ratings">
                                                <i class="mdi mdi-star active"></i>
                                                <i class="mdi mdi-star active"></i>
                                                <i class="mdi mdi-star active"></i>
                                                <i class="mdi mdi-star active"></i>
                                                <i class="mdi mdi-star"></i>
                                                <label>20 Reviews</label>
                                                </span>
                                                <div class="tags">
                                                   <span>History</span>
                                                </div>
                                             </div>
                                          </a>
                                       </div>
                                    </div>
                                    <div class="col m4 s12 pb-holder third-col">
                                       <div class="placebox">
                                          <a href="javascript:void(0)">
                                             <div class="imgholder himg-box">
                                                <img src="images/todo-img-3.jpg" class="himg" />
                                                <div class="overlay"></div>
                                             </div>
                                             <div class="descholder">
                                                <h5>The Siq</h5>
                                                <span class="ratings">
                                                <i class="mdi mdi-star active"></i>
                                                <i class="mdi mdi-star active"></i>
                                                <i class="mdi mdi-star active"></i>
                                                <i class="mdi mdi-star active"></i>
                                                <i class="mdi mdi-star active"></i>
                                                <label>65 Reviews</label>
                                                </span>
                                                <div class="tags">
                                                   <span>History</span>
                                                   <span>Adventure</span>
                                                </div>
                                             </div>
                                          </a>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="placesection">
                     <div class="tcontent-holder">
                        <div class="cbox-title nborder title-1">
                           <span>Tour guides in</span> <?=$st_nm_S?>
                           <div class="top-stuff right">
                              <a href="javascript:void(0)" class="viewall-link" onclick="openDirectTab('places-guides')">View All Guides</a>
                           </div>
                           <p>Discover the best events happening in London every week hand picked by Trip com's City editors.</p>
                        </div>
                        <div class="cbox-desc tour-guides">
                           <div class="places-content-holder">
                              <div class="list-holder guidebox-list">
                                 <div class="row">
                                    <div class="col l3 m3 s6">
                                       <div class="guide-box">
                                          <div class="imgholder">
                                             <img class="circle" src="images/demo-profile.jpg" />
                                             <div class="overlay">
                                                <span class="licensed-span">Licensed</span>
                                             </div>
                                          </div>
                                          <div class="descholder">
                                             <h3>Nimish Parekh</h3>
                                             <p>Guide for <?=$st_nm_S?></p>
                                             <span class="stars-holder yellow-stars">
                                             <i class="mdi mdi-star active"></i>
                                             <i class="mdi mdi-star active"></i>
                                             <i class="mdi mdi-star active"></i>
                                             <i class="mdi mdi-star active"></i>
                                             <i class="mdi mdi-star"></i>
                                             <label>23 Refers</label>
                                             </span>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col l3 m3 s6">
                                       <div class="guide-box">
                                          <div class="imgholder">
                                             <img src="images/guide-1.png" />
                                             <div class="overlay">
                                                <span class="licensed-span">Licensed</span>
                                             </div>
                                          </div>
                                          <div class="descholder">
                                             <h3>Nimish Parekh</h3>
                                             <p>Guide for <?=$st_nm_S?></p>
                                             <span class="stars-holder yellow-stars">
                                             <i class="mdi mdi-star active"></i>
                                             <i class="mdi mdi-star active"></i>
                                             <i class="mdi mdi-star active"></i>
                                             <i class="mdi mdi-star active"></i>
                                             <i class="mdi mdi-star"></i>
                                             <label>23 Refers</label>
                                             </span>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col l3 m3 s6">
                                       <div class="guide-box">
                                          <div class="imgholder">
                                             <img src="images/guide-2.png" />
                                             <div class="overlay">
                                                <span class="licensed-span">Licensed</span>
                                             </div>
                                          </div>
                                          <div class="descholder">
                                             <h3>Nimish Parekh</h3>
                                             <p>Guide for <?=$st_nm_S?></p>
                                             <span class="stars-holder yellow-stars">
                                             <i class="mdi mdi-star active"></i>
                                             <i class="mdi mdi-star active"></i>
                                             <i class="mdi mdi-star active"></i>
                                             <i class="mdi mdi-star active"></i>
                                             <i class="mdi mdi-star"></i>
                                             <label>23 Refers</label>
                                             </span>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col l3 m3 s6 last_guide">
                                       <div class="guide-box">
                                          <div class="imgholder">
                                             <img src="images/guide-3.png" />
                                             <div class="overlay">
                                                <span class="licensed-span">Licensed</span>
                                             </div>
                                          </div>
                                          <div class="descholder">
                                             <h3>Nimish Parekh</h3>
                                             <p>Guide for <?=$st_nm_S?></p>
                                             <span class="stars-holder yellow-stars">
                                             <i class="mdi mdi-star active"></i>
                                             <i class="mdi mdi-star active"></i>
                                             <i class="mdi mdi-star active"></i>
                                             <i class="mdi mdi-star active"></i>
                                             <i class="mdi mdi-star"></i>
                                             <label>23 Refers</label>
                                             </span>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="placesection">
                     <div class="tcontent-holder">
                        <div class="cbox-title nborder title-1">
                           <span>Hangouts in</span> <?=$st_nm_S?>
                           <div class="top-stuff right">
                              <a href="javascript:void(0)" class="viewall-link" onclick="openDirectTab('places-hangout')">View All</a>
                           </div>
                           <p>Discover the best events happening in London every week hand picked by Trip com's City editors.</p>
                        </div>
                        <div class="cbox-desc">
                           <div class="places-content-holder">
                              <div class="list-holder hangoutbox-list">
                                 <div class="row">
                                    <div class="col l3 m3 s12">
                                       <div class="hangout-box">
                                          <div class="imgholder">
                                             <img class="circle" src="images/demo-profile.jpg" />
                                             <div class="overlay">
                                                <div class="event-span">Bowling</div>
                                                <div class="title-span">
                                                   Nimish Parekh
                                                </div>
                                             </div>
                                          </div>
                                          <div class="descholder">
                                             <h6>Nimish Parekh</h6>
                                             <div class="event-span">Bowling</div>
                                             <p>November 20,2016 at 5:00pm</p>
                                             <p>Royal Ground, London</p>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col l3 m3 s12">
                                       <div class="hangout-box">
                                          <div class="imgholder">
                                             <img class="circle" src="images/demo-profile.jpg" />
                                             <div class="overlay">
                                                <div class="event-span">Dinner</div>
                                                <div class="title-span">
                                                   Nimish Parekh
                                                </div>
                                             </div>
                                          </div>
                                          <div class="descholder">
                                             <h6>Nimish Parekh</h6>
                                             <div class="event-span">Dinner</div>
                                             <p>November 20, 2016 at 5:00pm</p>
                                             <p>Hyatt Hotel, London</p>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col l3 m3 s12">
                                       <div class="hangout-box">
                                          <div class="imgholder">
                                             <img class="circle" src="images/demo-profile.jpg" />
                                             <div class="overlay">
                                                <div class="event-span">Movie</div>
                                                <div class="title-span">
                                                   Nimish Parekh
                                                </div>
                                             </div>
                                          </div>
                                          <div class="descholder">
                                             <h6>Nimish Parekh</h6>
                                             <div class="event-span">Movie</div>
                                             <p>November 20, 2016 at 5:00pm</p>
                                             <p>Cinemax Theatre, Amman</p>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col l3 m3 s12 hide-on-small-only">
                                       <div class="hangout-box">
                                          <div class="imgholder">
                                             <img class="circle" src="images/demo-profile.jpg" />
                                             <div class="overlay">
                                                <div class="event-span">Cricket</div>
                                                <div class="title-span">
                                                   Nimish Parekh
                                                </div>
                                             </div>
                                          </div>
                                          <div class="descholder">
                                             <h6>Nimish Parekh</h6>
                                             <div class="event-span">Cricket</div>
                                             <p>November 20, 2016 at 5:00pm</p>
                                             <p>Royal Ground, London</p>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="placesection general-page">
                     <div class="tcontent-holder grid-view">
                        <div class="cbox-title nborder title-1">
                           <span>Events this week:</span> <?=$st_nm_S?>
                           <div class="top-stuff right">
                              <a href="javascript:void(0)" class="viewall-link" onclick="openDirectTab('places-events')">View All Events</a>
                           </div>
                           <p>Discover the best events happening in London every week hand picked by Trip com&apos;s City editors.</p>
                        </div>
                        <div class="cbox-desc">
                           <div class="places-content-holder">
                              <div class="commevents-list generalbox-list">
                                 <div class="row">
                                    <div class="col l3 m4 s12">
                                       <a href="javascript:void(0);" onclick="commeventsGoing(event,'commevent1',this)" class="commevents-box general-box">
                                          <div class="photo-holder">
                                             <img src="images/additem-commevents.png">
                                          </div>
                                          <div class="content-holder">
                                             <h4>CS London weekly meet #234 @Pendrel's Oak, Holborn</h4>
                                             <div class="userinfo">
                                                <span class="month">Nov</span>
                                                <span class="date">17</span>
                                             </div>
                                             <div class="username">
                                                <span>Greg Batmarx</span>
                                             </div>
                                             <div class="action-btns">
                                                <span class="noClick" onclick="commeventsGoing(event,'commevent1',this)">Attend</span>
                                             </div>
                                          </div>
                                       </a>
                                    </div>
                                    <div class="col l3 m4 s12">
                                       <a href="javascript:void(0)" class="commevents-box general-box">
                                          <div class="photo-holder">
                                             <img src="images/collection-img-2.png">
                                          </div>
                                          <div class="content-holder">
                                             <h4>Renewable energy</h4>
                                             <div class="icon-line">
                                                <span>Tagline goes here</span>
                                             </div>
                                             <div class="userinfo">
                                                <span class="month">Nov</span>
                                                <span class="date">17</span>
                                             </div>
                                             <div class="username">
                                                <span>Greg Batmarx</span>
                                             </div>
                                             <div class="icon-line">
                                                <i class="zmdi zmdi-check"></i>
                                                Chrarles Thomas, Ankit Shah and Pratik Patel followed this
                                             </div>
                                             <div class="action-btns">
                                                <span class="followers">120 Followers</span>
                                                <span class="noClick" onclick="commeventsGoing(event,'commevent2',this)">Attend</span>
                                             </div>
                                          </div>
                                       </a>
                                    </div>
                                    <div class="col l3 m4 s12">
                                       <a href="javascript:void(0)" class="commevents-box general-box">
                                          <div class="photo-holder">
                                             <img src="images/collection-img-3.png">
                                          </div>
                                          <div class="content-holder">
                                             <h4>Black-white illustrations to H. L.</h4>
                                             <div class="icon-line">
                                                <span>Tagline goes here</span>
                                             </div>
                                             <div class="userinfo">
                                                <span class="month">Nov</span>
                                                <span class="date">17</span>
                                             </div>
                                             <div class="username">
                                                <span>Henry Lion Oldie</span>
                                             </div>
                                             <div class="icon-line">
                                                <i class="zmdi zmdi-check"></i>
                                                Chrarles Thomas, Ankit Shah and Pratik Patel followed this
                                             </div>
                                             <div class="action-btns">
                                                <span class="followers">120 Followers</span>
                                                <span class="noClick" onclick="commeventsGoing(event,'commevent3',this)">Attend</span>
                                             </div>
                                          </div>
                                       </a>
                                    </div>
                                    <div class="col l3 m4 s12">
                                       <a href="javascript:void(0)" class="commevents-box general-box">
                                          <div class="photo-holder">
                                             <img src="images/collection-img-4.png">
                                          </div>
                                          <div class="content-holder">
                                             <h4>B&W Photography</h4>
                                             <div class="icon-line">
                                                <span>Tagline goes here</span>
                                             </div>
                                             <div class="userinfo">
                                                <span class="month">Nov</span>
                                                <span class="date">17</span>
                                             </div>
                                             <div class="username">
                                                <span>Rui Luis</span>
                                             </div>
                                             <div class="icon-line">
                                                <i class="zmdi zmdi-check"></i>
                                                Chrarles Thomas, Ankit Shah and Pratik Patel followed this
                                             </div>
                                             <div class="action-btns">
                                                <span class="followers">120 Followers</span>
                                                <span class="noClick" onclick="commeventsGoing(event,'commevent4',this)">Attend</span>
                                             </div>
                                          </div>
                                       </a>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="placesection general-page">
                     <div class="tcontent-holder grid-view">
                        <div class="cbox-title nborder title-1">
                           <span>Groups in</span> <?=$st_nm_S?>
                           <div class="top-stuff right">
                              <a href="javascript:void(0)" class="viewall-link" onclick="openDirectTab('places-groups')">View All Groups</a>
                           </div>
                           <p>Discover the best events happening in London every week hand picked by Trip com's City editors.</p>
                        </div>
                        <div class="cbox-desc">
                           <div class="places-content-holder">
                              <div class="groups-list generalbox-list">
                                 <div class="row">
                                    <div class="col l3 m4 s12">
                                       <a href="javascript:void(0);" class="groups-box general-box">
                                          <div class="photo-holder">
                                             <img src="images/additem-groups.png">
                                          </div>
                                          <div class="content-holder">
                                             <h4>Airlines (low-cost, budget, cheap flights)</h4>
                                             <div class="cityinfo">
                                                Maxico city, Maxico
                                             </div>
                                             <div class="username">
                                                <span class="inline-list">
                                                <i class=”mdi mdi-account-group”></i></i>
                                                <span>34,546 Members</span>
                                                </span>
                                                <span class="inline-list">
                                                <i class="mdi mdi-pencil-box-outline"></i>
                                                <span>1000 Posts</span>
                                                </span>
                                             </div>
                                             <div class="icon-line subtext">
                                                Last activity 45 minutes ago
                                             </div>
                                             <div class="members">
                                                34,546 Members
                                             </div>
                                             <div class="action-btns">
                                                <span class="followers">Last activity 45 minutes ago</span>
                                                <span class="noClick approved" onclick="groupsJoin(event,'commevent1',this)">Member</span>
                                             </div>
                                          </div>
                                       </a>
                                    </div>
                                    <div class="col l3 m4 s12">
                                       <a href="javascript:void(0);" class="groups-box general-box">
                                          <div class="photo-holder">
                                             <img src="images/additem-groups.png">
                                          </div>
                                          <div class="content-holder">
                                             <h4>Europe</h4>
                                             <div class="cityinfo">
                                                Maxico city, Maxico
                                             </div>
                                             <div class="username">
                                                <span class="inline-list">
                                                <i class=”mdi mdi-account-group”></i></i>
                                                <span>34,546 Members</span>
                                                </span>
                                                <span class="inline-list">
                                                <i class="mdi mdi-pencil-box-outline"></i>
                                                <span>1000 Posts</span>
                                                </span>
                                             </div>
                                             <div class="username">
                                                <i class="mdi mdi-earth"></i>
                                                <span>Worldwide</span>
                                             </div>
                                             <div class="icon-line subtext">
                                                Last activity 45 minutes ago
                                             </div>
                                             <div class="members">
                                                34,546 Members
                                             </div>
                                             <div class="action-btns">
                                                <span class="followers">Last activity 45 minutes ago</span>
                                                <span class="noClick" onclick="groupsJoin(event,'commevent1',this)">Join</span>
                                             </div>
                                          </div>
                                       </a>
                                    </div>
                                    <div class="col l3 m4 s12">
                                       <a href="javascript:void(0);" class="groups-box general-box">
                                          <div class="photo-holder">
                                             <img src="images/additem-groups.png">
                                          </div>
                                          <div class="content-holder">
                                             <h4>Hitchhikers</h4>
                                             <div class="cityinfo">
                                                Maxico city, Maxico
                                             </div>
                                             <div class="username">
                                                <span class="inline-list">
                                                <i class=”mdi mdi-account-group”></i></i>
                                                <span>34,546 Members</span>
                                                </span>
                                                <span class="inline-list">
                                                <i class="mdi mdi-pencil-box-outline"></i>
                                                <span>1000 Posts</span>
                                                </span>
                                             </div>
                                             <div class="icon-line subtext">
                                                Last activity 45 minutes ago
                                             </div>
                                             <div class="members">
                                                34,546 Members
                                             </div>
                                             <div class="action-btns">
                                                <span class="followers">Last activity 45 minutes ago</span>
                                                <span class="noClick disabled" onclick="groupsJoin(event,'commevent1',this)">Request Sent</span>
                                             </div>
                                          </div>
                                       </a>
                                    </div>
                                    <div class="col l3 m4 s12">
                                       <a href="javascript:void(0);" class="groups-box general-box">
                                          <div class="photo-holder">
                                             <img src="images/additem-groups.png">
                                          </div>
                                          <div class="content-holder">
                                             <h4>Worldwide Voluteering</h4>
                                             <div class="cityinfo">
                                                Maxico city, Maxico
                                             </div>
                                             <div class="username">
                                                <span class="inline-list">
                                                <i class=”mdi mdi-account-group”></i></i>
                                                <span>34,546 Members</span>
                                                </span>
                                                <span class="inline-list">
                                                <i class="mdi mdi-pencil-box-outline"></i>
                                                <span>1000 Posts</span>
                                                </span>
                                             </div>
                                             <div class="username">
                                                <i class="mdi mdi-earth"></i>
                                                <span>Worldwide</span>
                                             </div>
                                             <div class="icon-line subtext">
                                                Last activity 45 minutes ago
                                             </div>
                                             <div class="members">
                                                34,546 Members
                                             </div>
                                             <div class="action-btns">
                                                <span class="followers">Last activity 45 minutes ago</span>
                                                <span class="noClick disabled" onclick="groupsJoin(event,'commevent1',this)">Request Sent</span>
                                             </div>
                                          </div>
                                       </a>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="placesection">
                     <div class="tcontent-holder">
                        <div class="cbox-title nborder title-1">
                           Similar Destinations
                        </div>
                        <div class="cbox-desc p-t-0">
                           <div class="places-content-holder">
                              <div class="list-holder">
                                 <div class="row">
                                    <div class="col m4 s6 fpb-holder">
                                       <div class="f-placebox destibox">
                                          <a href="javascript:void(0)">
                                             <div class="imgholder himg-box">
                                                <img src="images/placesdest-img-1.jpg" class="himg" />
                                                <div class="overlay"></div>
                                             </div>
                                             <div class="descholder">
                                                <h5>Rome, Italy</h5>
                                                <ul>
                                                   <li><i class="mdi mdi-menu-right"></i>10 Hotels</li>
                                                   <li><i class="mdi mdi-menu-right"></i>2 Restaurant</li>
                                                   <li><i class="mdi mdi-menu-right"></i>40 Travellers</li>
                                                </ul>
                                                <span class="btn btn-primary btn-sm right waves-effect waves-light">Explore</span>
                                             </div>
                                          </a>
                                       </div>
                                    </div>
                                    <div class="col m4 s6 fpb-holder">
                                       <div class="f-placebox destibox">
                                          <a href="javascript:void(0)">
                                             <div class="imgholder himg-box">
                                                <img src="images/placesdest-img-2.jpg" class="himg" />
                                                <div class="overlay"></div>
                                             </div>
                                             <div class="descholder">
                                                <h5>Florence, Italy</h5>
                                                <ul>
                                                   <li><i class="mdi mdi-menu-right"></i>10 Hotels</li>
                                                   <li><i class="mdi mdi-menu-right"></i>2 Restaurant</li>
                                                   <li><i class="mdi mdi-menu-right"></i>40 Travellers</li>
                                                </ul>
                                                <span class="btn btn-primary btn-sm right waves-effect waves-light">Explore</span>
                                             </div>
                                          </a>
                                       </div>
                                    </div>
                                    <div class="col m4 s6 fpb-holder third-col hide-on-small-only">
                                       <div class="f-placebox destibox">
                                          <a href="javascript:void(0)">
                                             <div class="imgholder himg-box">
                                                <img src="images/placesdest-img-3.jpg" class="himg" />
                                                <div class="overlay"></div>
                                             </div>
                                             <div class="descholder">
                                                <h5>Versailles, France</h5>
                                                <ul>
                                                   <li><i class="mdi mdi-menu-right"></i>10 Hotels</li>
                                                   <li><i class="mdi mdi-menu-right"></i>2 Restaurant</li>
                                                   <li><i class="mdi mdi-menu-right"></i>40 Travellers</li>
                                                </ul>
                                                <span class="btn btn-primary btn-sm right waves-effect waves-light">Explore</span>
                                             </div>
                                          </a>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="placesection">
                     <div class="tcontent-holder">
                        <div class="cbox-title nborder title-1">
                           Popular nearby cities
                        </div>
                        <div class="cbox-desc p-t-0">
                           <div class="places-content-holder">
                              <div class="list-holder">
                                 <div class="row">
                                    <div class="col m4 s6 fpb-holder">
                                       <div class="f-placebox destibox">
                                          <a href="javascript:void(0)">
                                             <div class="imgholder himg-box">
                                                <img src="images/topplaces-1.jpg" class="himg" />
                                                <div class="overlay"></div>
                                             </div>
                                             <div class="descholder">
                                                <h5><?=$st_nm_S?></h5>
                                                <ul>
                                                   <li><i class="mdi mdi-menu-right"></i>10 Hotels</li>
                                                   <li><i class="mdi mdi-menu-right"></i>2 Restaurant</li>
                                                   <li><i class="mdi mdi-menu-right"></i>40 Travellers</li>
                                                </ul>
                                             </div>
                                          </a>
                                       </div>
                                    </div>
                                    <div class="col m4 s6 fpb-holder">
                                       <div class="f-placebox destibox">
                                          <a href="javascript:void(0)">
                                             <div class="imgholder himg-box">
                                                <img src="images/topplaces-2.jpg" class="himg" />
                                                <div class="overlay"></div>
                                             </div>
                                             <div class="descholder">
                                                <h5>Az Zarqa, <?=$st_nm_S?></h5>
                                                <ul>
                                                   <li><i class="mdi mdi-menu-right"></i>10 Hotels</li>
                                                   <li><i class="mdi mdi-menu-right"></i>2 Restaurant</li>
                                                   <li><i class="mdi mdi-menu-right"></i>40 Travellers</li>
                                                </ul>
                                             </div>
                                          </a>
                                       </div>
                                    </div>
                                    <div class="col m4 s12 fpb-holder third-col hide-on-small-only">
                                       <div class="f-placebox destibox">
                                          <a href="javascript:void(0)">
                                             <div class="imgholder himg-box">
                                                <img src="images/topplaces-3.jpg" class="himg" />
                                                <div class="overlay"></div>
                                             </div>
                                             <div class="descholder">
                                                <h5>Madaba, <?=$st_nm_S?></h5>
                                                <ul>
                                                   <li><i class="mdi mdi-menu-right"></i>10 Hotels</li>
                                                   <li><i class="mdi mdi-menu-right"></i>2 Restaurant</li>
                                                   <li><i class="mdi mdi-menu-right"></i>40 Travellers</li>
                                                </ul>
                                             </div>
                                          </a>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="map-holder visible">
                                 <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3110.3465133386144!2d-9.167423685010494!3d38.77868997958898!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xd193295d5b45545%3A0x3f9e7b6a5f00e12c!2sPerta!5e0!3m2!1sen!2sin!4v1481089901870" width="600" height="450" frameborder="0" allowfullscreen></iframe>
                                 <a href="javascript:void(0)" class="overlay" onclick="expandMap(this,'#todo-title')"></a>
                                 <a href="javascript:void(0)" class="closelink" onclick="shrinkMap(this)"><i class="mdi mdi-close"></i> Close</a>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="placesection">
                     <div class="tcontent-holder topplaces-section">
                        <div class="cbox-desc">
                           <div class="cbox-title nborder title-1">
                              Discover Top places
                           </div>
                           <div class="places-content-holder">
                              <div class="list-holder topplaces-list">
                                 <!-- New Slider-->
                                 <div class=" mx-0">
                                    <div class="owl-carousel cus-gallery">
                                       <!-- Item-->
                                       <div class="item">
                                          <div class="">
                                             <div class="f-placebox">
                                                <div class="imgholder himg-box"><img src="images/topplaces-1.jpg" class="himg" /></div>
                                                <div class="descholder">
                                                   <h5>Brussels<span>Belgium</span></h5>
                                                   <div class="tags">
                                                      <span>Museum</span>
                                                      <span>Local Beer</span>
                                                      <span>Architechture</span>
                                                   </div>
                                                   <button class="btn btn-primary btn-sm right white-text waves-effect waves-light">Discover</button>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <!-- Item-->
                                       <div class="item">
                                          <div class="">
                                             <div class="f-placebox">
                                                <div class="imgholder himg-box"><img src="images/topplaces-2.jpg" class="himg" /></div>
                                                <div class="descholder">
                                                   <h5>Brussels<span>Belgium</span></h5>
                                                   <div class="tags">
                                                      <span>Architechture</span>
                                                      <span>Museum</span>
                                                      <span>Local Beer</span>
                                                   </div>
                                                   <button class="btn btn-primary btn-sm right white-text waves-effect waves-light">Discover</button>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <!-- Item-->
                                       <div class="item">
                                          <div class="">
                                             <div class="f-placebox">
                                                <div class="imgholder himg-box"><img src="images/topplaces-3.jpg" class="himg" /></div>
                                                <div class="descholder">
                                                   <h5>Brussels<span>Belgium</span></h5>
                                                   <div class="tags">
                                                      <span>Architechture</span>
                                                      <span>Museum</span>
                                                      <span>Local Beer</span>
                                                   </div>
                                                   <button class="btn btn-primary btn-sm right white-text waves-effect waves-light">Discover</button>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <!-- Item-->
                                       <div class="item">
                                          <div class="">
                                             <div class="f-placebox">
                                                <div class="imgholder himg-box"><img src="images/topplaces-4.jpg" class="himg" /></div>
                                                <div class="descholder">
                                                   <h5>Brussels<span>Belgium</span></h5>
                                                   <div class="tags">
                                                      <span>Architechture</span>
                                                      <span>Museum</span>
                                                      <span>Local Beer</span>
                                                   </div>
                                                   <button class="btn btn-primary btn-sm right white-text waves-effect waves-light">Discover</button>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <!-- Item-->
                                       <!-- Item-->
                                       <div class="item">
                                          <div class="">
                                             <div class="f-placebox">
                                                <div class="imgholder himg-box"><img src="images/topplaces-1.jpg" class="himg" /></div>
                                                <div class="descholder">
                                                   <h5>Brussels<span>Belgium</span></h5>
                                                   <div class="tags">
                                                      <span>Architechture</span>
                                                      <span>Museum</span>
                                                      <span>Local Beer</span>
                                                   </div>
                                                   <button class="btn btn-primary btn-sm right white-text waves-effect waves-light">Discover</button>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <!-- Item-->
                                    </div>
                                 </div>
                                 <!--New Slider-->
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="placesection">
                     <div class="tcontent-holder topattractions-section">
                        <div class="cbox-title nborder title-1">
                           Top Attractions
                        </div>
                        <div class="cbox-desc">
                           <div class="places-content-holder">
                              <div class="list-holder topplaces-list">
                                 <!-- New Slider-->
                                 <div class=" mx-0">
                                    <div class="owl-carousel cus-gallery">
                                       <!-- Item-->
                                       <div class="item">
                                          <div class="">
                                             <div class="f-placebox">
                                                <div class="imgholder himg-box"><img src="images/topplaces-1.jpg" class="himg"/></div>
                                                <div class="descholder">
                                                   <h5>Attraction Name<span>Belgium</span></h5>
                                                   <div class="tags">
                                                      <p><strong>Peak Time :</strong> Feb - Apr</p>
                                                      <p><strong>Visitors :</strong> 12000 / year</p>
                                                   </div>
                                                   <button class="btn btn-primary btn-sm right white-text waves-effect waves-light">Visit</button>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <!-- Item-->
                                       <div class="item">
                                          <div class="">
                                             <div class="f-placebox">
                                                <div class="imgholder himg-box"><img src="images/topplaces-2.jpg" class="himg"/></div>
                                                <div class="descholder">
                                                   <h5>Attraction Name<span>Belgium</span></h5>
                                                   <div class="tags">
                                                      <p><strong>Peak Time :</strong> Feb - Apr</p>
                                                      <p><strong>Visitors :</strong> 12000 / year</p>
                                                   </div>
                                                   <button class="btn btn-primary btn-sm right white-text waves-effect waves-light">Visit</button>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <!-- Item-->
                                       <div class="item">
                                          <div class="">
                                             <div class="f-placebox">
                                                <div class="imgholder himg-box"><img src="images/topplaces-3.jpg" class="himg"/></div>
                                                <div class="descholder">
                                                   <h5>Attraction Name<span>Belgium</span></h5>
                                                   <div class="tags">
                                                      <p><strong>Peak Time :</strong> Feb - Apr</p>
                                                      <p><strong>Visitors :</strong> 12000 / year</p>
                                                   </div>
                                                   <button class="btn btn-primary btn-sm right white-text waves-effect waves-light">Visit</button>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <!-- Item-->
                                       <div class="item">
                                          <div class="">
                                             <div class="f-placebox">
                                                <div class="imgholder himg-box"><img src="images/topplaces-4.jpg" class="himg"/></div>
                                                <div class="descholder">
                                                   <h5>Attraction Name<span>Belgium</span></h5>
                                                   <div class="tags">
                                                      <p><strong>Peak Time :</strong> Feb - Apr</p>
                                                      <p><strong>Visitors :</strong> 12000 / year</p>
                                                   </div>
                                                   <button class="btn btn-primary btn-sm right white-text waves-effect waves-light">Visit</button>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <!-- Item-->
                                       <div class="item">
                                          <div class="">
                                             <div class="f-placebox">
                                                <div class="imgholder himg-box"><img src="images/topplaces-1.jpg" class="himg"/></div>
                                                <div class="descholder">
                                                   <h5>Attraction Name<span>Belgium</span></h5>
                                                   <div class="tags">
                                                      <p><strong>Peak Time :</strong> Feb - Apr</p>
                                                      <p><strong>Visitors :</strong> 12000 / year</p>
                                                   </div>
                                                   <button class="btn btn-primary btn-sm right white-text waves-effect waves-light">Visit</button>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <!-- Item-->
                                    </div>
                                 </div>
                                 <!--New Slider-->
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <?php 
               include("common/footer.php"); 
               ?>
            </div>
            <div id="places-discussion" class="placesdiscussion-content subtab bottom_tabs">
               <div class="row cshfsiput cshfsi">
                  <div class="social-section profile-box detailBox">
                    <div class="item titlelabel user-info">
                      <p class="center-align u-name mb0">Discussion for Amman (<span class="count">10</span>)</p>
                      <p class="user-desg center-align"></p>
                    </div>
                    <div class="item profile-info row mx-0 width-100">
                      <div class="sub-item">
                        <h5 class="">Photo</h5>
                        <p class="">10</p>
                      </div>
                      <div class="sub-item">
                        <h5 class="">Post</h5>
                        <p class="">15</p>
                      </div>
                      <div class="sub-item">
                        <h5 class="">Connections</h5>
                        <p class="">20</p>
                      </div>
                      <div class="sub-item">
                        <h5 class="">Profile views</h5>
                        <p class="">05</p>
                      </div>
                    </div>
                  </div>
                  <div class="postBox">
                     <div class="content-box ">
                        <div class="new-post base-newpost cshfsiput cshfsi compose_discus">
                           <div class="npost-content">
                              <div class="post-mcontent">
                                 <i class="mdi mdi-pencil-box-outline main-icon"></i>
                                 <div class="desc">
                                    <div class="input-field comments_box">
                                       <p>Discussion for Paris</p>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="cbox-desc nm-postlist post-list cshfsiput cshfsi">
                           <div class="post-holder bborder tippost-holder">
                              <div class="post-topbar">
                                 <div class="post-userinfo">
                                    <div class="img-holder">
                                       <div id="profiletip-1" class="profiletipholder">
                                          <span class="profile-tooltip">
                                          <img class="circle" src="images/demo-profile.jpg" />
                                          </span>
                                          <span class="profiletooltip_content">
                                             <div class="profile-tip">
                                                <div class="profile-tip-cover"><img src="images/cover.jpg"></div>
                                                <div class="profile-tip-avatar">
                                                   <a href="javascript:void(0)">
                                                   <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">
                                                   </a>
                                                </div>
                                                <div class="profile-tip-info">
                                                   <div class="cover-username"><a href="javascript:void(0)">Adel Hasanat</a></div>
                                                   <div class="cover-headline">
                                                      <span class="ptip-icon"><i class="fa  fa-suitcase"></i></span>
                                                      Web Designer, Cricketer
                                                   </div>
                                                   <div class="profiletip-bio">
                                                      <span class="ptip-icon"><i class="mdi mdi-home"></i></span>
                                                      Lives in : <span>Gariyadhar</span>
                                                   </div>
                                                   <div class="profiletip-bio">
                                                      <span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span>
                                                      Currently in : <span>Gariyadhar, Gujarat, India</span>
                                                   </div>
                                                </div>
                                                <div class="profile-tip-divider"></div>
                                                <div class="profile-tip-btn">
                                                   <a href="javascript:void(0)" class="btn btn-primary btn-sm"><i class=”mdi mdi-eye”></i>View Profile</a>
                                                </div>
                                             </div>
                                          </span>
                                       </div>
                                    </div>
                                    <div class="desc-holder">
                                       <a href="javascript:void(0)">Adel Hasanat</a> tip for <a class="sub-link" href="javascript:void(0)"><?=$st_nm_S?></a>
                                       <span class="timestamp">August 31 at 08:45 pm<span class="glyphicon glyphicon-globe"></span></span>
                                    </div>
                                 </div>
                                 <div class="settings-icon">
                                    <div class="dropdown">
                                       <a class="dropdown-button" href="javascript:void(0)" data-activates="dropdown-editdisc">
                                       <i class="zmdi zmdi-more zmdi-hc-2x"></i>
                                       </a>
                                       <ul class="dropdown-content" id="dropdown-editdisc">
                                          <li>
                                             <a href="javascript:void(0)" class="edit_discus">Edit Discussion</a>
                                          </li>
                                       </ul>
                                    </div>
                                 </div>
                              </div>
                              <div class="post-content">
                                 <div class="post-details">
                                    <div class="post-title">Random Title</div>
                                    <div class="post-desc">
                                       <div class="para-section">
                                          <div class="para">
                                             <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sed varius risus. Duis rhoncus eros et pellentesque imperdiet.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sed varius risus. Duis rhoncus eros et pellentesque imperdiet.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sed varius risus. Duis rhoncus eros et pellentesque imperdiet.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sed varius risus. Duis rhoncus eros et pellentesque imperdiet.</p>
                                          </div>
                                          <a href="javascript:void(0)" onclick="showAllContent(this)">Read More</a>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="post-img-holder">
                                    <div class="lgt-gallery post-img two-img lgt-gallery-photo dis-none">
                                       <a href="images/post-img2.jpg" data-size="1600x1600" data-med="images/post-img2.jpg" data-med-size="1024x1024" data-author="Folkert Gorter" class="pimg-holder himg-box">
                                       <img class="himg" src="images/post-img2.jpg" alt="" />
                                       </a>
                                       <a href="images/post-img3.jpg" data-size="1600x1068" data-med="images/post-img3.jpg" data-med-size="1024x683" data-author="Samuel Rohl" class="pimg-holder vimg-box">
                                       <img class="himg" src="images/post-img3.jpg" alt="" />
                                       </a>
                                    </div>
                                 </div>
                              </div>
                              <div class="clear"></div>
                              <div class="post-data">
                                 <div class="post-actions">
                                    <div class="right like-tooltip">
                                       <a href="javascript:void(0)" class="pa-like" data-title="User Name"><i class="zmdi zmdi-thumb-up"></i></a>
                                       <span class="lcount">4</span>
                                       <a href="javascript:void(0)" class="pa-comment"><i class="zmdi zmdi-comment"></i></a>
                                       <span class="comment-lcount">4</span>
                                    </div>
                                 </div>
                                 <div class="comments-section panel">
                                    <div class="comments-area">
                                       <div class="post-more">
                                          <a href="javascript:void(0)" class="view-morec">View more comments</a>
                                          <span class="total-comments">3 of 7</span>
                                       </div>
                                       <div class="post-comments">
                                          <div class="pcomments">
                                             <div class="pcomment-earlier">
                                                <div class="pcomment-holder">
                                                   <div class="pcomment main-comment">
                                                      <div class="img-holder">
                                                         <div id="commentptip-1" class="profiletipholder">
                                                            <span class="profile-tooltip">
                                                            <img class="circle" src="images/demo-profile.jpg" />
                                                            </span>
                                                            <span class="profiletooltip_content">
                                                               <div class="profile-tip">
                                                                  <div class="profile-tip-cover"><img src="images/cover.jpg"></div>
                                                                  <div class="profile-tip-avatar">
                                                                     <a href="javascript:void(0)">
                                                                     <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">
                                                                     </a>
                                                                  </div>
                                                                  <div class="profile-tip-info">
                                                                     <div class="cover-username"><a href="javascript:void(0)">Adel Hasanat</a></div>
                                                                     <div class="cover-headline">
                                                                        <span class="ptip-icon"><i class="fa  fa-suitcase"></i></span>
                                                                        Web Designer, Cricketer
                                                                     </div>
                                                                     <div class="profiletip-bio">
                                                                        <span class="ptip-icon"><i class="mdi mdi-home"></i></span>
                                                                        Lives in : <span>Gariyadhar</span>
                                                                     </div>
                                                                     <div class="profiletip-bio">
                                                                        <span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span>
                                                                        Currently in : <span>Gariyadhar, Gujarat, India</span>
                                                                     </div>
                                                                  </div>
                                                                  <div class="profile-tip-divider"></div>
                                                                  <div class="profile-tip-btn">
                                                                     <a href="javascript:void(0)" class="btn btn-primary btn-sm"><i class=”mdi mdi-eye”></i>View Profile</a>
                                                                  </div>
                                                               </div>
                                                            </span>
                                                         </div>
                                                      </div>
                                                      <div class="desc-holder">
                                                         <div class="normal-mode">
                                                            <div class="desc">
                                                               <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                                               <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                                            </div>
                                                            <div class="comment-stuff">
                                                               <div class="more-opt">
                                                                  <a href="javascript:void(0)" class="pa-like"><span>icon</span></a>
                                                                  <a href="javascript:void(0)" class="pa-reply reply-comment"><span>icon</span></a>
                                                                  <div class="dropdown dropdown-custom dropdown-xxsmall">
                                                                     <a href="javascript:void(0)" class="dropdown-toggle dropdown-button" data-activates="dropdown-editdeleteDisc1">
                                                                     <i class="zmdi zmdi-hc-2x zmdi-more-vert"></i>
                                                                     </a>
                                                                     <ul class="dropdown-content" id="dropdown-editdeleteDisc1">
                                                                        <li><a href="javascript:void(0)" class="edit-comment">Edit</a></li>
                                                                        <li><a href="javascript:void(0)" class="delete-comment">Delete</a></li>
                                                                     </ul>
                                                                  </div>
                                                               </div>
                                                               <div class="less-opt">
                                                                  <div class="timestamp">8h</div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                         <div class="edit-mode">
                                                            <div class="desc">
                                                               <textarea class="materialize-textarea mb0 md_textarea item_tagline">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                                               <a href="javascript:void(0)" class="btn btn-primary btn-sm editcomment-cancel waves-effect waves-light">Cancel</a>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div class="clear"></div>
                                                   <div class="comment-reply-holder comment-addreply">
                                                      <div class="addnew-comment valign-wrapper comment-reply">
                                                         <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg" /></a></div>
                                                         <div class="desc-holder">
                                                            <div class="sliding-middle-custom anim-area">
                                                               <textarea class="materialize-textarea mb0 md_textarea item_tagline">Write a reply...</textarea>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="pcomment-holder">
                                                   <div class="pcomment main-comment">
                                                      <div class="img-holder">
                                                         <div id="commentptip-2" class="profiletipholder">
                                                            <span class="profile-tooltip">
                                                            <img class="circle" src="images/demo-profile.jpg" />
                                                            </span>
                                                            <span class="profiletooltip_content">
                                                               <div class="profile-tip">
                                                                  <div class="profile-tip-cover"><img src="images/cover.jpg"></div>
                                                                  <div class="profile-tip-avatar">
                                                                     <a href="javascript:void(0)">
                                                                     <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">
                                                                     </a>
                                                                  </div>
                                                                  <div class="profile-tip-info">
                                                                     <div class="cover-username"><a href="javascript:void(0)">Adel Hasanat</a></div>
                                                                     <div class="cover-headline">
                                                                        <span class="ptip-icon"><i class="fa  fa-suitcase"></i></span>
                                                                        Web Designer, Cricketer
                                                                     </div>
                                                                     <div class="profiletip-bio">
                                                                        <span class="ptip-icon"><i class="mdi mdi-home"></i></span>
                                                                        Lives in : <span>Gariyadhar</span>
                                                                     </div>
                                                                     <div class="profiletip-bio">
                                                                        <span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span>
                                                                        Currently in : <span>Gariyadhar, Gujarat, India</span>
                                                                     </div>
                                                                  </div>
                                                                  <div class="profile-tip-divider"></div>
                                                                  <div class="profile-tip-btn">
                                                                     <a href="javascript:void(0)" class="btn btn-primary btn-sm"><i class=”mdi mdi-eye”></i>View Profile</a>
                                                                  </div>
                                                               </div>
                                                            </span>
                                                         </div>
                                                      </div>
                                                      <div class="desc-holder">
                                                         <div class="normal-mode">
                                                            <div class="desc">
                                                               <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                                               <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                                            </div>
                                                            <div class="comment-stuff">
                                                               <div class="more-opt">
                                                                  <a href="javascript:void(0)" class="pa-like"><span>icon</span></a>
                                                                  <a href="javascript:void(0)" class="pa-reply reply-comment"><span>icon</span></a>
                                                                  <div class="dropdown dropdown-custom dropdown-xxsmall">
                                                                     <a href="javascript:void(0)" class="dropdown-toggle dropdown-button" data-activates="dropdown-editdeleteDisc2">
                                                                     <i class="zmdi zmdi-hc-2x zmdi-more-vert"></i>
                                                                     </a>
                                                                     <ul class="dropdown-content" id="dropdown-editdeleteDisc2"">
                                                                        <li><a href="javascript:void(0)" class="edit-comment">Edit</a></li>
                                                                        <li><a href="javascript:void(0)" class="delete-comment">Delete</a></li>
                                                                     </ul>
                                                                  </div>
                                                               </div>
                                                               <div class="less-opt">
                                                                  <div class="timestamp">8h</div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                         <div class="edit-mode">
                                                            <div class="desc">
                                                               <textarea class="editcomment-tt materialize-textarea mb0 md_textarea item_tagline">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                                               <a href="javascript:void(0)" class="btn btn-primary btn-sm editcomment-cancel waves-effect waves-light">Cancel</a>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div class="clear"></div>
                                                   <div class="comment-reply-holder comment-addreply">
                                                      <div class="addnew-comment valign-wrapper comment-reply">
                                                         <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg" /></a></div>
                                                         <div class="desc-holder">
                                                            <div class="sliding-middle-custom anim-area">
                                                               <textarea class="materialize-textarea mb0 md_textarea item_tagline">Write a reply...</textarea>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="pcomment-holder has-comments">
                                                <div class="pcomment main-comment">
                                                   <div class="img-holder">
                                                      <div id="commentptip-3" class="profiletipholder">
                                                         <span class="profile-tooltip">
                                                         <img class="circle" src="images/demo-profile.jpg" />
                                                         </span>
                                                         <span class="profiletooltip_content">
                                                            <div class="profile-tip">
                                                               <div class="profile-tip-cover"><img src="images/cover.jpg"></div>
                                                               <div class="profile-tip-avatar">
                                                                  <a href="javascript:void(0)">
                                                                  <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">
                                                                  </a>
                                                               </div>
                                                               <div class="profile-tip-info">
                                                                  <div class="cover-username"><a href="javascript:void(0)">Adel Hasanat</a></div>
                                                                  <div class="cover-headline">
                                                                     <span class="ptip-icon"><i class="fa  fa-suitcase"></i></span>
                                                                     Web Designer, Cricketer
                                                                  </div>
                                                                  <div class="profiletip-bio">
                                                                     <span class="ptip-icon"><i class="mdi mdi-home"></i></span>
                                                                     Lives in : <span>Gariyadhar</span>
                                                                  </div>
                                                                  <div class="profiletip-bio">
                                                                     <span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span>
                                                                     Currently in : <span>Gariyadhar, Gujarat, India</span>
                                                                  </div>
                                                               </div>
                                                               <div class="profile-tip-divider"></div>
                                                               <div class="profile-tip-btn">
                                                                  <a href="javascript:void(0)" class="btn btn-primary btn-sm"><i class=”mdi mdi-eye”></i>View Profile</a>
                                                               </div>
                                                            </div>
                                                         </span>
                                                      </div>
                                                   </div>
                                                   <div class="desc-holder">
                                                      <div class="normal-mode">
                                                         <div class="desc">
                                                            <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                                         </div>
                                                         <div class="comment-stuff">
                                                            <div class="more-opt">
                                                               <a href="javascript:void(0)" class="pa-like"><span>icon</span></a>
                                                               <a href="javascript:void(0)" class="pa-reply reply-comment"><span>icon</span></a>
                                                               <div class="dropdown dropdown-custom dropdown-xxsmall">
                                                                  <a href="javascript:void(0)" class="dropdown-toggle dropdown-button" data-activates="dropdown-editdeleteDisc3">
                                                                  <i class="zmdi zmdi-hc-2x zmdi-more-vert"></i>
                                                                  </a>
                                                                  <ul class="dropdown-content" id="dropdown-editdeleteDisc3">
                                                                     <li><a href="javascript:void(0)" class="edit-comment">Edit</a></li>
                                                                     <li><a href="javascript:void(0)" class="delete-comment">Delete</a></li>
                                                                  </ul>
                                                               </div>
                                                            </div>
                                                            <div class="less-opt">
                                                               <div class="timestamp">8h</div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div class="edit-mode">
                                                         <div class="desc">
                                                            <textarea class="editcomment-tt materialize-textarea mb0 md_textarea item_tagline">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                                            <a href="javascript:void(0)" class="btn btn-primary btn-sm editcomment-cancel waves-effect waves-light">Cancel</a>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="clear"></div>
                                                <div class="comment-reply-holder">
                                                   <div class="pcomment comment-reply">
                                                      <div class="img-holder">
                                                         <div id="commentptip-5" class="profiletipholder">
                                                            <span class="profile-tooltip">
                                                            <img class="circle" src="images/demo-profile.jpg" />
                                                            </span>
                                                            <span class="profiletooltip_content">
                                                               <div class="profile-tip">
                                                                  <div class="profile-tip-cover"><img src="images/cover.jpg"></div>
                                                                  <div class="profile-tip-avatar">
                                                                     <a href="javascript:void(0)">
                                                                     <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">
                                                                     </a>
                                                                  </div>
                                                                  <div class="profile-tip-info">
                                                                     <div class="cover-username"><a href="javascript:void(0)">Adel Hasanat</a></div>
                                                                     <div class="cover-headline">
                                                                        <span class="ptip-icon"><i class="fa  fa-suitcase"></i></span>
                                                                        Web Designer, Cricketer
                                                                     </div>
                                                                     <div class="profiletip-bio">
                                                                        <span class="ptip-icon"><i class="mdi mdi-home"></i></span>
                                                                        Lives in : <span>Gariyadhar</span>
                                                                     </div>
                                                                     <div class="profiletip-bio">
                                                                        <span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span>
                                                                        Currently in : <span>Gariyadhar, Gujarat, India</span>
                                                                     </div>
                                                                  </div>
                                                                  <div class="profile-tip-divider"></div>
                                                                  <div class="profile-tip-btn">
                                                                     <a href="javascript:void(0)" class="btn btn-primary btn-sm"><i class=”mdi mdi-eye”></i>View Profile</a>
                                                                  </div>
                                                               </div>
                                                            </span>
                                                         </div>
                                                      </div>
                                                      <div class="desc-holder">
                                                         <div class="normal-mode">
                                                            <div class="desc">
                                                               <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                                               <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                                            </div>
                                                            <div class="comment-stuff">
                                                               <div class="more-opt">
                                                                  <a href="javascript:void(0)" class="pa-like"><span>icon</span></a>
                                                                  <a href="javascript:void(0)" class="pa-reply reply-comment"><span>icon</span></a>
                                                                  <div class="dropdown dropdown-custom dropdown-xxsmall">
                                                                     <a href="javascript:void(0)" class="dropdown-toggle dropdown-button" data-activates="dropdown-editdeleteDisc4">
                                                                     <i class="zmdi zmdi-hc-2x zmdi-more-vert"></i>
                                                                     </a>
                                                                     <ul class="dropdown-content" id="dropdown-editdeleteDisc4">
                                                                        <li><a href="javascript:void(0)" class="edit-comment">Edit</a></li>
                                                                        <li><a href="javascript:void(0)" class="delete-comment">Delete</a></li>
                                                                     </ul>
                                                                  </div>
                                                               </div>
                                                               <div class="less-opt">
                                                                  <div class="timestamp">8h</div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                         <div class="edit-mode">
                                                            <div class="desc">
                                                               <textarea class="editcomment-tt materialize-textarea mb0 md_textarea item_tagline">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                                               <a href="javascript:void(0)" class="btn btn-primary btn-sm editcomment-cancel waves-effect waves-light">Cancel</a>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div class="pcomment comment-reply">
                                                      <div class="img-holder">
                                                         <div id="commentptip-6" class="profiletipholder">
                                                            <span class="profile-tooltip">
                                                            <img class="circle" src="images/demo-profile.jpg" />
                                                            </span>
                                                            <span class="profiletooltip_content">
                                                               <div class="profile-tip">
                                                                  <div class="profile-tip-cover"><img src="images/cover.jpg"></div>
                                                                  <div class="profile-tip-avatar">
                                                                     <a href="javascript:void(0)">
                                                                     <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">
                                                                     </a>
                                                                  </div>
                                                                  <div class="profile-tip-info">
                                                                     <div class="cover-username"><a href="javascript:void(0)">Adel Hasanat</a></div>
                                                                     <div class="cover-headline">
                                                                        <span class="ptip-icon"><i class="fa  fa-suitcase"></i></span>
                                                                        Web Designer, Cricketer
                                                                     </div>
                                                                     <div class="profiletip-bio">
                                                                        <span class="ptip-icon"><i class="mdi mdi-home"></i></span>
                                                                        Lives in : <span>Gariyadhar</span>
                                                                     </div>
                                                                     <div class="profiletip-bio">
                                                                        <span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span>
                                                                        Currently in : <span>Gariyadhar, Gujarat, India</span>
                                                                     </div>
                                                                  </div>
                                                                  <div class="profile-tip-divider"></div>
                                                                  <div class="profile-tip-btn">
                                                                     <a href="javascript:void(0)" class="btn btn-primary btn-sm"><i class=”mdi mdi-eye”></i>View Profile</a>
                                                                  </div>
                                                               </div>
                                                            </span>
                                                         </div>
                                                      </div>
                                                      <div class="desc-holder">
                                                         <div class="normal-mode">
                                                            <div class="desc">
                                                               <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                                               <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit...</p>
                                                            </div>
                                                            <div class="comment-stuff">
                                                               <div class="more-opt">
                                                                  <a href="javascript:void(0)" class="pa-like"><span>icon</span></a>
                                                                  <a href="javascript:void(0)" class="pa-reply reply-comment"><span>icon</span></a>
                                                                  <div class="dropdown dropdown-custom dropdown-xxsmall">
                                                                     <a href="javascript:void(0)" class="dropdown-toggle dropdown-button" data-activates="dropdown-editdeleteDisc5">
                                                                     <i class="zmdi zmdi-hc-2x zmdi-more-vert"></i>
                                                                     </a>
                                                                     <ul class="dropdown-content" id="dropdown-editdeleteDisc5">
                                                                        <li><a href="javascript:void(0)" class="edit-comment">Edit</a></li>
                                                                        <li><a href="javascript:void(0)" class="delete-comment">Delete</a></li>
                                                                     </ul>
                                                                  </div>
                                                               </div>
                                                               <div class="less-opt">
                                                                  <div class="timestamp">8h</div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                         <div class="edit-mode">
                                                            <div class="desc">
                                                               <textarea class="editcomment-tt materialize-textarea mb0 md_textarea item_tagline">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                                               <a href="javascript:void(0)" class="btn btn-primary btn-sm editcomment-cancel waves-effect waves-light">Cancel</a>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="comment-reply-holder comment-addreply">
                                                   <div class="addnew-comment valign-wrapper comment-reply">
                                                      <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg" /></a></div>
                                                      <div class="desc-holder">
                                                         <div class="sliding-middle-custom anim-area">
                                                            <textarea class="materialize-textarea mb0 md_textarea item_tagline">Write a reply...</textarea>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="pcomment-holder">
                                                <div class="pcomment main-comment">
                                                   <div class="img-holder">
                                                      <div id="commentptip-4" class="profiletipholder">
                                                         <span class="profile-tooltip">
                                                         <img class="circle" src="images/demo-profile.jpg" />
                                                         </span>
                                                         <span class="profiletooltip_content">
                                                            <div class="profile-tip">
                                                               <div class="profile-tip-cover"><img src="images/cover.jpg"></div>
                                                               <div class="profile-tip-avatar">
                                                                  <a href="javascript:void(0)">
                                                                  <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">
                                                                  </a>
                                                               </div>
                                                               <div class="profile-tip-info">
                                                                  <div class="cover-username"><a href="javascript:void(0)">Adel Hasanat</a></div>
                                                                  <div class="cover-headline">
                                                                     <span class="ptip-icon"><i class="fa  fa-suitcase"></i></span>
                                                                     Web Designer, Cricketer
                                                                  </div>
                                                                  <div class="profiletip-bio">
                                                                     <span class="ptip-icon"><i class="mdi mdi-home"></i></span>
                                                                     Lives in : <span>Gariyadhar</span>
                                                                  </div>
                                                                  <div class="profiletip-bio">
                                                                     <span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span>
                                                                     Currently in : <span>Gariyadhar, Gujarat, India</span>
                                                                  </div>
                                                               </div>
                                                               <div class="profile-tip-divider"></div>
                                                               <div class="profile-tip-btn">
                                                                  <a href="javascript:void(0)" class="btn btn-primary btn-sm"><i class=”mdi mdi-eye”></i>View Profile</a>
                                                               </div>
                                                            </div>
                                                         </span>
                                                      </div>
                                                   </div>
                                                   <div class="desc-holder">
                                                      <div class="normal-mode">
                                                         <div class="desc">
                                                            <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                                         </div>
                                                         <div class="comment-stuff">
                                                            <div class="more-opt">
                                                               <a href="javascript:void(0)" class="pa-like"><span>icon</span></a>
                                                               <a href="javascript:void(0)" class="pa-reply reply-comment"><span>icon</span></a>
                                                               <div class="dropdown dropdown-custom dropdown-xxsmall">
                                                                  <a href="javascript:void(0)" class="dropdown-toggle dropdown-button" data-activates="dropdown-editdeleteDisc6">
                                                                  <i class="zmdi zmdi-hc-2x zmdi-more-vert"></i>
                                                                  </a>
                                                                  <ul class="dropdown-content" id="dropdown-editdeleteDisc6">
                                                                     <li><a href="javascript:void(0)" class="edit-comment">Edit</a></li>
                                                                     <li><a href="javascript:void(0)" class="delete-comment">Delete</a></li>
                                                                  </ul>
                                                               </div>
                                                            </div>
                                                            <div class="less-opt">
                                                               <div class="timestamp">8h</div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div class="edit-mode">
                                                         <div class="desc">
                                                            <textarea class="editcomment-tt materialize-textarea mb0 md_textarea item_tagline">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                                            <a href="javascript:void(0)" class="btn btn-primary btn-sm editcomment-cancel waves-effect waves-light">Cancel</a>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="clear"></div>
                                                <div class="comment-reply-holder comment-addreply">
                                                   <div class="addnew-comment valign-wrapper comment-reply">
                                                      <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg" /></a></div>
                                                      <div class="desc-holder">
                                                         <div class="sliding-middle-custom anim-area">
                                                            <textarea class="materialize-textarea mb0 md_textarea item_tagline">Write a reply...</textarea>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="addnew-comment valign-wrapper">
                                             <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg" /></a></div>
                                             <div class="desc-holder">
                                                <div class="sliding-middle-custom anim-area">
                                                   <textarea class="materialize-textarea mb0 md_textarea item_tagline">Write a comment</textarea>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="post-holder bborder tippost-holder">
                              <div class="post-topbar">
                                 <div class="post-userinfo">
                                    <div class="img-holder">
                                       <div id="profiletip-1" class="profiletipholder">
                                          <span class="profile-tooltip">
                                          <img class="circle" src="images/demo-profile.jpg" />
                                          </span>
                                          <span class="profiletooltip_content">
                                             <div class="profile-tip">
                                                <div class="profile-tip-cover"><img src="images/cover.jpg"></div>
                                                <div class="profile-tip-avatar">
                                                   <a href="javascript:void(0)">
                                                   <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">
                                                   </a>
                                                </div>
                                                <div class="profile-tip-info">
                                                   <div class="cover-username"><a href="javascript:void(0)">Adel Hasanat</a></div>
                                                   <div class="cover-headline">
                                                      <span class="ptip-icon"><i class="fa  fa-suitcase"></i></span>
                                                      Web Designer, Cricketer
                                                   </div>
                                                   <div class="profiletip-bio">
                                                      <span class="ptip-icon"><i class="mdi mdi-home"></i></span>
                                                      Lives in : <span>Gariyadhar</span>
                                                   </div>
                                                   <div class="profiletip-bio">
                                                      <span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span>
                                                      Currently in : <span>Gariyadhar, Gujarat, India</span>
                                                   </div>
                                                </div>
                                                <div class="profile-tip-divider"></div>
                                                <div class="profile-tip-btn">
                                                   <a href="javascript:void(0)" class="btn btn-primary btn-sm"><i class=”mdi mdi-eye”></i>View Profile</a>
                                                </div>
                                             </div>
                                          </span>
                                       </div>
                                    </div>
                                    <div class="desc-holder">
                                       <a href="javascript:void(0)">Nimish Parekh</a> tip for <a class="sub-link" href="javascript:void(0)"><?=$st_nm_S?></a>
                                       <span class="timestamp">August 31 at 08:45 pm<span class="glyphicon glyphicon-globe"></span></span>
                                    </div>
                                 </div>
                                 <div class="settings-icon">
                                    <div class="dropdown">
                                       <a class="dropdown-button" href="javascript:void(0)" data-activates="dropdown-editdisc2">
                                       <i class="zmdi zmdi-more zmdi-hc-2x"></i>
                                       </a>
                                       <ul class="dropdown-content" id="dropdown-editdisc2">
                                          <li>
                                             <a href="javascript:void(0)" class="edit_discus">Edit Discussion</a>
                                          </li>
                                       </ul>
                                    </div>
                                 </div>
                              </div>
                              <div class="post-content">
                                 <div class="post-details">
                                    <div class="post-desc">
                                       <div class="para-section">
                                          <div class="para">
                                             <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sed varius risus. Duis rhoncus eros et pellentesque imperdiet.<br />Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sed varius risus. Duis rhoncus eros et pellentesque imperdiet.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sed varius risus. Duis rhoncus eros et pellentesque imperdiet.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sed varius risus. Duis rhoncus eros et pellentesque imperdiet.</p>
                                          </div>
                                          <a href="javascript:void(0)" onclick="showAllContent(this)">Read More</a>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="clear"></div>
                              <div class="post-data">
                                 <div class="post-actions">
                                    <div class="right like-tooltip">
                                       <a href="javascript:void(0)" class="pa-like" data-title="User Name"><i class="zmdi zmdi-thumb-up"></i></a>
                                       <span class="lcount">4</span>
                                       <a href="javascript:void(0)" class="pa-comment"><i class="zmdi zmdi-comment"></i></a>
                                       <span class="comment-lcount">4</span>
                                    </div>
                                 </div>
                                 <div class="comments-section panel">
                                    <div class="comments-area">
                                       <div class="post-more">
                                          <a href="javascript:void(0)" class="view-morec">View more comments</a>
                                          <span class="total-comments">3 of 7</span>
                                       </div>
                                       <div class="post-comments">
                                          <div class="pcomments">
                                             <div class="pcomment-earlier">
                                                <div class="pcomment-holder">
                                                   <div class="pcomment main-comment">
                                                      <div class="img-holder">
                                                         <div id="commentptip-1" class="profiletipholder">
                                                            <span class="profile-tooltip">
                                                            <img class="circle" src="images/demo-profile.jpg" />
                                                            </span>
                                                            <span class="profiletooltip_content">
                                                               <div class="profile-tip">
                                                                  <div class="profile-tip-cover"><img src="images/cover.jpg"></div>
                                                                  <div class="profile-tip-avatar">
                                                                     <a href="javascript:void(0)">
                                                                     <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">
                                                                     </a>
                                                                  </div>
                                                                  <div class="profile-tip-info">
                                                                     <div class="cover-username"><a href="javascript:void(0)">Adel Hasanat</a></div>
                                                                     <div class="cover-headline">
                                                                        <span class="ptip-icon"><i class="fa  fa-suitcase"></i></span>
                                                                        Web Designer, Cricketer
                                                                     </div>
                                                                     <div class="profiletip-bio">
                                                                        <span class="ptip-icon"><i class="mdi mdi-home"></i></span>
                                                                        Lives in : <span>Gariyadhar</span>
                                                                     </div>
                                                                     <div class="profiletip-bio">
                                                                        <span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span>
                                                                        Currently in : <span>Gariyadhar, Gujarat, India</span>
                                                                     </div>
                                                                  </div>
                                                                  <div class="profile-tip-divider"></div>
                                                                  <div class="profile-tip-btn">
                                                                     <a href="javascript:void(0)" class="btn btn-primary btn-sm"><i class=”mdi mdi-eye”></i>View Profile</a>
                                                                  </div>
                                                               </div>
                                                            </span>
                                                         </div>
                                                      </div>
                                                      <div class="desc-holder">
                                                         <div class="normal-mode">
                                                            <div class="desc">
                                                               <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                                               <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                                            </div>
                                                            <div class="comment-stuff">
                                                               <div class="more-opt">
                                                                  <a href="javascript:void(0)" class="pa-like"><span>icon</span></a>
                                                                  <a href="javascript:void(0)" class="pa-reply reply-comment"><span>icon</span></a>
                                                                  <div class="dropdown dropdown-custom dropdown-xxsmall">
                                                                     <a href="javascript:void(0)" class="dropdown-toggle dropdown-button" data-activates="dropdown-editdelete13">
                                                                     <i class="zmdi zmdi-hc-2x zmdi-more-vert"></i>
                                                                     </a>
                                                                     <ul class="dropdown-content" id="dropdown-editdelete13">
                                                                        <li><a href="javascript:void(0)" class="edit-comment">Edit</a></li>
                                                                        <li><a href="javascript:void(0)" class="delete-comment">Delete</a></li>
                                                                     </ul>
                                                                  </div>
                                                               </div>
                                                               <div class="less-opt">
                                                                  <div class="timestamp">8h</div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                         <div class="edit-mode">
                                                            <div class="desc">
                                                               <textarea class="editcomment-tt materialize-textarea mb0 md_textarea item_tagline">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                                               <a href="javascript:void(0)" class="btn btn-primary btn-sm editcomment-cancel waves-effect waves-light">Cancel</a>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div class="clear"></div>
                                                   <div class="comment-reply-holder comment-addreply">
                                                      <div class="addnew-comment valign-wrapper comment-reply">
                                                         <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg" /></a></div>
                                                         <div class="desc-holder">
                                                            <div class="sliding-middle-custom anim-area">
                                                               <textarea class="materialize-textarea mb0 md_textarea item_tagline">Write a reply...</textarea>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="pcomment-holder">
                                                   <div class="pcomment main-comment">
                                                      <div class="img-holder">
                                                         <div id="commentptip-2" class="profiletipholder">
                                                            <span class="profile-tooltip">
                                                            <img class="circle" src="images/demo-profile.jpg" />
                                                            </span>
                                                            <span class="profiletooltip_content">
                                                               <div class="profile-tip">
                                                                  <div class="profile-tip-cover"><img src="images/cover.jpg"></div>
                                                                  <div class="profile-tip-avatar">
                                                                     <a href="javascript:void(0)">
                                                                     <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">
                                                                     </a>
                                                                  </div>
                                                                  <div class="profile-tip-info">
                                                                     <div class="cover-username"><a href="javascript:void(0)">Adel Hasanat</a></div>
                                                                     <div class="cover-headline">
                                                                        <span class="ptip-icon"><i class="fa  fa-suitcase"></i></span>
                                                                        Web Designer, Cricketer
                                                                     </div>
                                                                     <div class="profiletip-bio">
                                                                        <span class="ptip-icon"><i class="mdi mdi-home"></i></span>
                                                                        Lives in : <span>Gariyadhar</span>
                                                                     </div>
                                                                     <div class="profiletip-bio">
                                                                        <span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span>
                                                                        Currently in : <span>Gariyadhar, Gujarat, India</span>
                                                                     </div>
                                                                  </div>
                                                                  <div class="profile-tip-divider"></div>
                                                                  <div class="profile-tip-btn">
                                                                     <a href="javascript:void(0)" class="btn btn-primary btn-sm"><i class=”mdi mdi-eye”></i>View Profile</a>
                                                                  </div>
                                                               </div>
                                                            </span>
                                                         </div>
                                                      </div>
                                                      <div class="desc-holder">
                                                         <div class="normal-mode">
                                                            <div class="desc">
                                                               <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                                               <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                                            </div>
                                                            <div class="comment-stuff">
                                                               <div class="more-opt">
                                                                  <a href="javascript:void(0)" class="pa-like"><span>icon</span></a>
                                                                  <a href="javascript:void(0)" class="pa-reply reply-comment"><span>icon</span></a>
                                                                  <div class="dropdown dropdown-custom dropdown-xxsmall">
                                                                     <a href="javascript:void(0)" class="dropdown-toggle dropdown-button" data-activates="dropdown-editdelete12">
                                                                     <i class="zmdi zmdi-hc-2x zmdi-more-vert"></i>
                                                                     </a>
                                                                     <ul class="dropdown-content" id="dropdown-editdelete12">
                                                                        <li><a href="javascript:void(0)" class="edit-comment">Edit</a></li>
                                                                        <li><a href="javascript:void(0)" class="delete-comment">Delete</a></li>
                                                                     </ul>
                                                                  </div>
                                                               </div>
                                                               <div class="less-opt">
                                                                  <div class="timestamp">8h</div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                         <div class="edit-mode">
                                                            <div class="desc">
                                                               <textarea class="editcomment-tt materialize-textarea mb0 md_textarea item_tagline">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                                               <a href="javascript:void(0)" class="btn btn-primary btn-sm editcomment-cancel waves-effect waves-light">Cancel</a>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div class="clear"></div>
                                                   <div class="comment-reply-holder comment-addreply">
                                                      <div class="addnew-comment valign-wrapper comment-reply">
                                                         <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg" /></a></div>
                                                         <div class="desc-holder">
                                                            <div class="sliding-middle-custom anim-area">
                                                               <textarea class="materialize-textarea mb0 md_textarea item_tagline">Write a reply...</textarea>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="pcomment-holder has-comments">
                                                <div class="pcomment main-comment">
                                                   <div class="img-holder">
                                                      <div id="commentptip-3" class="profiletipholder">
                                                         <span class="profile-tooltip">
                                                         <img class="circle" src="images/demo-profile.jpg" />
                                                         </span>
                                                         <span class="profiletooltip_content">
                                                            <div class="profile-tip">
                                                               <div class="profile-tip-cover"><img src="images/cover.jpg"></div>
                                                               <div class="profile-tip-avatar">
                                                                  <a href="javascript:void(0)">
                                                                  <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">
                                                                  </a>
                                                               </div>
                                                               <div class="profile-tip-info">
                                                                  <div class="cover-username"><a href="javascript:void(0)">Adel Hasanat</a></div>
                                                                  <div class="cover-headline">
                                                                     <span class="ptip-icon"><i class="fa  fa-suitcase"></i></span>
                                                                     Web Designer, Cricketer
                                                                  </div>
                                                                  <div class="profiletip-bio">
                                                                     <span class="ptip-icon"><i class="mdi mdi-home"></i></span>
                                                                     Lives in : <span>Gariyadhar</span>
                                                                  </div>
                                                                  <div class="profiletip-bio">
                                                                     <span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span>
                                                                     Currently in : <span>Gariyadhar, Gujarat, India</span>
                                                                  </div>
                                                               </div>
                                                               <div class="profile-tip-divider"></div>
                                                               <div class="profile-tip-btn">
                                                                  <a href="javascript:void(0)" class="btn btn-primary btn-sm"><i class=”mdi mdi-eye”></i>View Profile</a>
                                                               </div>
                                                            </div>
                                                         </span>
                                                      </div>
                                                   </div>
                                                   <div class="desc-holder">
                                                      <div class="normal-mode">
                                                         <div class="desc">
                                                            <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                                         </div>
                                                         <div class="comment-stuff">
                                                            <div class="more-opt">
                                                               <a href="javascript:void(0)" class="pa-like"><span>icon</span></a>
                                                               <a href="javascript:void(0)" class="pa-reply reply-comment"><span>icon</span></a>
                                                               <div class="dropdown dropdown-custom dropdown-xxsmall">
                                                                  <a href="javascript:void(0)" class="dropdown-toggle dropdown-button" data-activates="dropdown-editdelete11">
                                                                  <i class="zmdi zmdi-hc-2x zmdi-more-vert"></i>
                                                                  </a>
                                                                  <ul class="dropdown-content" id="dropdown-editdelete11">
                                                                     <li><a href="javascript:void(0)" class="edit-comment">Edit</a></li>
                                                                     <li><a href="javascript:void(0)" class="delete-comment">Delete</a></li>
                                                                  </ul>
                                                               </div>
                                                            </div>
                                                            <div class="less-opt">
                                                               <div class="timestamp">8h</div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div class="edit-mode">
                                                         <div class="desc">
                                                            <textarea class="editcomment-tt materialize-textarea mb0 md_textarea item_tagline">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                                            <a href="javascript:void(0)" class="btn btn-primary btn-sm editcomment-cancel waves-effect waves-light">Cancel</a>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="clear"></div>
                                                <div class="comment-reply-holder">
                                                   <div class="pcomment comment-reply">
                                                      <div class="img-holder">
                                                         <div id="commentptip-5" class="profiletipholder">
                                                            <span class="profile-tooltip">
                                                            <img class="circle" src="images/demo-profile.jpg" />
                                                            </span>
                                                            <span class="profiletooltip_content">
                                                               <div class="profile-tip">
                                                                  <div class="profile-tip-cover"><img src="images/cover.jpg"></div>
                                                                  <div class="profile-tip-avatar">
                                                                     <a href="javascript:void(0)">
                                                                     <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">
                                                                     </a>
                                                                  </div>
                                                                  <div class="profile-tip-info">
                                                                     <div class="cover-username"><a href="javascript:void(0)">Adel Hasanat</a></div>
                                                                     <div class="cover-headline">
                                                                        <span class="ptip-icon"><i class="fa  fa-suitcase"></i></span>
                                                                        Web Designer, Cricketer
                                                                     </div>
                                                                     <div class="profiletip-bio">
                                                                        <span class="ptip-icon"><i class="mdi mdi-home"></i></span>
                                                                        Lives in : <span>Gariyadhar</span>
                                                                     </div>
                                                                     <div class="profiletip-bio">
                                                                        <span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span>
                                                                        Currently in : <span>Gariyadhar, Gujarat, India</span>
                                                                     </div>
                                                                  </div>
                                                                  <div class="profile-tip-divider"></div>
                                                                  <div class="profile-tip-btn">
                                                                     <a href="javascript:void(0)" class="btn btn-primary btn-sm"><i class=”mdi mdi-eye”></i>View Profile</a>
                                                                  </div>
                                                               </div>
                                                            </span>
                                                         </div>
                                                      </div>
                                                      <div class="desc-holder">
                                                         <div class="normal-mode">
                                                            <div class="desc">
                                                               <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                                               <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                                            </div>
                                                            <div class="comment-stuff">
                                                               <div class="more-opt">
                                                                  <a href="javascript:void(0)" class="pa-like"><span>icon</span></a>
                                                                  <a href="javascript:void(0)" class="pa-reply reply-comment"><span>icon</span></a>
                                                                  <div class="dropdown dropdown-custom dropdown-xxsmall">
                                                                     <a href="javascript:void(0)" class="dropdown-toggle dropdown-button" data-activates="dropdown-editdelete10">
                                                                     <i class="zmdi zmdi-hc-2x zmdi-more-vert"></i>
                                                                     </a>
                                                                     <ul class="dropdown-content" id="dropdown-editdelete10">
                                                                        <li><a href="javascript:void(0)" class="edit-comment">Edit</a></li>
                                                                        <li><a href="javascript:void(0)" class="delete-comment">Delete</a></li>
                                                                     </ul>
                                                                  </div>
                                                               </div>
                                                               <div class="less-opt">
                                                                  <div class="timestamp">8h</div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                         <div class="edit-mode">
                                                            <div class="desc">
                                                               <textarea class="editcomment-tt materialize-textarea mb0 md_textarea item_tagline">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                                               <a href="javascript:void(0)" class="btn btn-primary btn-sm editcomment-cancel waves-effect waves-light">Cancel</a>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div class="pcomment comment-reply">
                                                      <div class="img-holder">
                                                         <div id="commentptip-6" class="profiletipholder">
                                                            <span class="profile-tooltip">
                                                            <img class="circle" src="images/demo-profile.jpg" />
                                                            </span>
                                                            <span class="profiletooltip_content">
                                                               <div class="profile-tip">
                                                                  <div class="profile-tip-cover"><img src="images/cover.jpg"></div>
                                                                  <div class="profile-tip-avatar">
                                                                     <a href="javascript:void(0)">
                                                                     <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">
                                                                     </a>
                                                                  </div>
                                                                  <div class="profile-tip-info">
                                                                     <div class="cover-username"><a href="javascript:void(0)">Adel Hasanat</a></div>
                                                                     <div class="cover-headline">
                                                                        <span class="ptip-icon"><i class="fa  fa-suitcase"></i></span>
                                                                        Web Designer, Cricketer
                                                                     </div>
                                                                     <div class="profiletip-bio">
                                                                        <span class="ptip-icon"><i class="mdi mdi-home"></i></span>
                                                                        Lives in : <span>Gariyadhar</span>
                                                                     </div>
                                                                     <div class="profiletip-bio">
                                                                        <span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span>
                                                                        Currently in : <span>Gariyadhar, Gujarat, India</span>
                                                                     </div>
                                                                  </div>
                                                                  <div class="profile-tip-divider"></div>
                                                                  <div class="profile-tip-btn">
                                                                     <a href="javascript:void(0)" class="btn btn-primary btn-sm"><i class=”mdi mdi-eye”></i>View Profile</a>
                                                                  </div>
                                                               </div>
                                                            </span>
                                                         </div>
                                                      </div>
                                                      <div class="desc-holder">
                                                         <div class="normal-mode">
                                                            <div class="desc">
                                                               <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                                               <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit...</p>
                                                            </div>
                                                            <div class="comment-stuff">
                                                               <div class="more-opt">
                                                                  <a href="javascript:void(0)" class="pa-like"><span>icon</span></a>
                                                                  <a href="javascript:void(0)" class="pa-reply reply-comment"><span>icon</span></a>
                                                                  <div class="dropdown dropdown-custom dropdown-xxsmall">
                                                                     <a href="javascript:void(0)" class="dropdown-toggle dropdown-button" data-activates="dropdown-editdelete9">
                                                                     <i class="zmdi zmdi-hc-2x zmdi-more-vert"></i>
                                                                     </a>
                                                                     <ul class="dropdown-content" id="dropdown-editdelete9">
                                                                        <li><a href="javascript:void(0)" class="edit-comment">Edit</a></li>
                                                                        <li><a href="javascript:void(0)" class="delete-comment">Delete</a></li>
                                                                     </ul>
                                                                  </div>
                                                               </div>
                                                               <div class="less-opt">
                                                                  <div class="timestamp">8h</div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                         <div class="edit-mode">
                                                            <div class="desc">
                                                               <textarea class="editcomment-tt materialize-textarea mb0 md_textarea item_tagline">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                                               <a href="javascript:void(0)" class="btn btn-primary btn-sm editcomment-cancel waves-effect waves-light">Cancel</a>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="comment-reply-holder comment-addreply">
                                                   <div class="addnew-comment valign-wrapper comment-reply">
                                                      <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg" /></a></div>
                                                      <div class="desc-holder">
                                                         <div class="sliding-middle-custom anim-area">
                                                            <textarea class="materialize-textarea mb0 md_textarea item_tagline">Write a reply...</textarea>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="pcomment-holder">
                                                <div class="pcomment main-comment">
                                                   <div class="img-holder">
                                                      <div id="commentptip-4" class="profiletipholder">
                                                         <span class="profile-tooltip">
                                                         <img class="circle" src="images/demo-profile.jpg" />
                                                         </span>
                                                         <span class="profiletooltip_content">
                                                            <div class="profile-tip">
                                                               <div class="profile-tip-cover"><img src="images/cover.jpg"></div>
                                                               <div class="profile-tip-avatar">
                                                                  <a href="javascript:void(0)">
                                                                  <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">
                                                                  </a>
                                                               </div>
                                                               <div class="profile-tip-info">
                                                                  <div class="cover-username"><a href="javascript:void(0)">Adel Hasanat</a></div>
                                                                  <div class="cover-headline">
                                                                     <span class="ptip-icon"><i class="fa  fa-suitcase"></i></span>
                                                                     Web Designer, Cricketer
                                                                  </div>
                                                                  <div class="profiletip-bio">
                                                                     <span class="ptip-icon"><i class="mdi mdi-home"></i></span>
                                                                     Lives in : <span>Gariyadhar</span>
                                                                  </div>
                                                                  <div class="profiletip-bio">
                                                                     <span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span>
                                                                     Currently in : <span>Gariyadhar, Gujarat, India</span>
                                                                  </div>
                                                               </div>
                                                               <div class="profile-tip-divider"></div>
                                                               <div class="profile-tip-btn">
                                                                  <a href="javascript:void(0)" class="btn btn-primary btn-sm"><i class=”mdi mdi-eye”></i>View Profile</a>
                                                               </div>
                                                            </div>
                                                         </span>
                                                      </div>
                                                   </div>
                                                   <div class="desc-holder">
                                                      <div class="normal-mode">
                                                         <div class="desc">
                                                            <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                                         </div>
                                                         <div class="comment-stuff">
                                                            <div class="more-opt">
                                                               <a href="javascript:void(0)" class="pa-like"><span>icon</span></a>
                                                               <a href="javascript:void(0)" class="pa-reply reply-comment"><span>icon</span></a>
                                                               <div class="dropdown dropdown-custom dropdown-xxsmall">
                                                                  <a href="javascript:void(0)" class="dropdown-toggle dropdown-button" data-activates="dropdown-editdelete8">
                                                                  <i class="zmdi zmdi-hc-2x zmdi-more-vert"></i>
                                                                  </a>
                                                                  <ul class="dropdown-content" id="dropdown-editdelete8">
                                                                     <li><a href="javascript:void(0)" class="edit-comment">Edit</a></li>
                                                                     <li><a href="javascript:void(0)" class="delete-comment">Delete</a></li>
                                                                  </ul>
                                                               </div>
                                                            </div>
                                                            <div class="less-opt">
                                                               <div class="timestamp">8h</div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div class="edit-mode">
                                                         <div class="desc">
                                                            <textarea class="editcomment-tt materialize-textarea mb0 md_textarea item_tagline">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                                            <a href="javascript:void(0)" class="btn btn-primary btn-sm editcomment-cancel waves-effect waves-light">Cancel</a>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="clear"></div>
                                                <div class="comment-reply-holder comment-addreply">
                                                   <div class="addnew-comment valign-wrapper comment-reply">
                                                      <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg" /></a></div>
                                                      <div class="desc-holder">
                                                         <div class="sliding-middle-custom anim-area">
                                                            <textarea class="materialize-textarea mb0 md_textarea item_tagline">Write a reply...</textarea>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="addnew-comment valign-wrapper">
                                             <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg" /></a></div>
                                             <div class="desc-holder">
                                                <div class="sliding-middle-custom anim-area">
                                                   <textarea class="materialize-textarea mb0 md_textarea item_tagline">Write a comment</textarea>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="new-post-mobile clear disscu_show">
                        <a href="javascript:void(0)" class="popup-window compose_discus" ><i class="mdi mdi-pencil"></i></a>
                     </div>
                  </div> 
               </div>

            </div>
            <div id="places-reviews" class="placesreviews-content subtab bottom_tabs">
               <div class="row cshfsiput cshfsi">
                  <div class="social-section profile-box detailBox">
                    <div class="item titlelabel user-info">
                      <p class="center-align u-name mb0">Discussion for Amman (<span class="count">10</span>)</p>
                      <p class="user-desg center-align"></p>
                    </div>
                    <div class="item profile-info row mx-0 width-100">
                      <div class="sub-item">
                        <h5 class="">Photo</h5>
                        <p class="">10</p>
                      </div>
                      <div class="sub-item">
                        <h5 class="">Post</h5>
                        <p class="">15</p>
                      </div>
                      <div class="sub-item">
                        <h5 class="">Connections</h5>
                        <p class="">20</p>
                      </div>
                      <div class="sub-item">
                        <h5 class="">Profile views</h5>
                        <p class="">05</p>
                      </div>
                    </div>
                  </div>
                  <div class="postBox">
                     <div class="content-box">
                        <div class="new-post cshfsiput cshfsi base-newpost compose_newreview">
                           <form action=""> 
                              <div class="rating-stars setRating" onmouseout="ratingJustOut(this)">
                                 <span>Let's start your rating</span>
                                 <i class="mdi mdi-star ratecls1 ratecls2 ratecls3 ratecls4 ratecls5" data-value="1" onmouseover="ratingJustOver(this)"></i> 
                                 <i class="mdi mdi-star ratecls2 ratecls3 ratecls4 ratecls5" data-value="2" onmouseover="ratingJustOver(this)"></i>
                                 <i class="mdi mdi-star ratecls3 ratecls4 ratecls5" data-value="3" onmouseover="ratingJustOver(this)"></i>
                                 <i class="mdi mdi-star ratecls4 ratecls5" data-value="4" onmouseover="ratingJustOver(this)"></i>
                                 <i class="mdi mdi-star ratecls5" data-value="5" onmouseover="ratingJustOver(this)"></i>&nbsp;&nbsp;
                                 <span class="star-text"></span>
                              </div>
                           </form>
                        </div>

                        <div class="cbox-desc nm-postlist post-list cshfsiput cshfsi reviews-column">
                           <div class="post-holder bborder reviewpost-holder">
                              <div class="post-topbar">
                                 <div class="post-userinfo">
                                    <div class="img-holder">
                                       <div id="profiletip-1" class="profiletipholder">
                                          <span class="profile-tooltip">
                                          <img class="circle" src="images/demo-profile.jpg" />
                                          </span>
                                          <span class="profiletooltip_content">
                                             <div class="profile-tip">
                                                <div class="profile-tip-cover"><img src="images/cover.jpg"></div>
                                                <div class="profile-tip-avatar">
                                                   <a href="javascript:void(0)">
                                                   <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">
                                                   </a>
                                                </div>
                                                <div class="profile-tip-info">
                                                   <div class="cover-username"><a href="javascript:void(0)">Adel Hasanat</a></div>
                                                   <div class="cover-headline">
                                                      <span class="ptip-icon"><i class="fa  fa-suitcase"></i></span>
                                                      Web Designer, Cricketer
                                                   </div>
                                                   <div class="profiletip-bio">
                                                      <span class="ptip-icon"><i class="mdi mdi-home"></i></span>
                                                      Lives in : <span>Gariyadhar</span>
                                                   </div>
                                                   <div class="profiletip-bio">
                                                      <span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span>
                                                      Currently in : <span>Gariyadhar, Gujarat, India</span>
                                                   </div>
                                                </div>
                                                <div class="profile-tip-divider"></div>
                                                <div class="profile-tip-btn">
                                                   <a href="javascript:void(0)" class="btn btn-primary btn-sm"><i class=”mdi mdi-eye”></i>View Profile</a>
                                                </div>
                                             </div>
                                          </span>
                                       </div>
                                    </div>
                                    <div class="desc-holder">
                                       <a href="javascript:void(0)">Nimish Parekh</a> reviewed <a class="sub-link" href="javascript:void(0)"><?=$st_nm_S?></a>
                                       <span class="timestamp">August 31 at 08:45 pm<span class="glyphicon glyphicon-globe"></span></span>
                                    </div>
                                 </div>
                                 <div class="settings-icon">
                                    <div class="dropdown">
                                       <a class="dropdown-button" href="javascript:void(0)" data-activates='dropdown-editreview'>
                                       <i class="zmdi zmdi-more zmdi-hc-2x"></i>
                                       </a>
                                       <ul class="dropdown-content" id="dropdown-editreview">
                                          <li>
                                             <a href="javascript:void(0)" class="edit_newreview">Edit review</a>
                                          </li>
                                       </ul>
                                    </div>
                                 </div>
                              </div>
                              <div class="post-content hasimg">
                                 <div class="post-details">
                                    <div class="rating-stars">
                                       <i class="mdi mdi-star active"></i>
                                       <i class="mdi mdi-star active"></i>
                                       <i class="mdi mdi-star active"></i>
                                       <i class="mdi mdi-star"></i>
                                       <i class="mdi mdi-star"></i>
                                    </div>
                                    <div class="post-desc">
                                       <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sed varius risus. Duis rhoncus eros et pellentesque imperdiet.</p>
                                    </div>
                                 </div>
                                 <div class="post-img-holder">
                                    <div class="lgt-gallery post-img one-img">
                                       <a href="images/post-img.jpg" data-size="1600x1600" data-med="images/post-img.jpg" data-med-size="1024x1024" data-author="Folkert Gorter" class="pimg-holder himg-box">
                                       <img class="himg" src="images/post-img.jpg" alt="" />
                                       </a>
                                    </div>
                                    <a href="javascript:void(0)" class="pinlink"><i class="mdi mdi-nature"></i></a>
                                 </div>
                              </div>
                              <div class="clear"></div>
                              <div class="post-data">
                                 <div class="post-actions">
                                    <div class="right like-tooltip">
                                       <a href="javascript:void(0)" class="pa-like" data-title="User Name"><i class="zmdi zmdi-thumb-up"></i></a>
                                       <span class="lcount">4</span>
                                       <a href="javascript:void(0)" class="pa-comment"><i class="zmdi zmdi-comment"></i></a>
                                       <span class="comment-lcount">4</span>
                                    </div>
                                 </div>
                                 <div class="comments-section panel">
                                    <div class="comments-area">
                                       <div class="post-more">
                                          <a href="javascript:void(0)" class="view-morec">View more comments</a>
                                          <span class="total-comments">3 of 7</span>
                                       </div>
                                       <div class="post-comments">
                                          <div class="pcomments">
                                             <div class="pcomment-earlier">
                                                <div class="pcomment-holder">
                                                   <div class="pcomment main-comment">
                                                      <div class="img-holder">
                                                         <div id="commentptip-1" class="profiletipholder">
                                                            <span class="profile-tooltip">
                                                            <img class="circle" src="images/demo-profile.jpg" />
                                                            </span>
                                                            <span class="profiletooltip_content">
                                                               <div class="profile-tip">
                                                                  <div class="profile-tip-cover"><img src="images/cover.jpg"></div>
                                                                  <div class="profile-tip-avatar">
                                                                     <a href="javascript:void(0)">
                                                                     <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">
                                                                     </a>
                                                                  </div>
                                                                  <div class="profile-tip-info">
                                                                     <div class="cover-username"><a href="javascript:void(0)">Adel Hasanat</a></div>
                                                                     <div class="cover-headline">
                                                                        <span class="ptip-icon"><i class="fa  fa-suitcase"></i></span>
                                                                        Web Designer, Cricketer
                                                                     </div>
                                                                     <div class="profiletip-bio">
                                                                        <span class="ptip-icon"><i class="mdi mdi-home"></i></span>
                                                                        Lives in : <span>Gariyadhar</span>
                                                                     </div>
                                                                     <div class="profiletip-bio">
                                                                        <span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span>
                                                                        Currently in : <span>Gariyadhar, Gujarat, India</span>
                                                                     </div>
                                                                  </div>
                                                                  <div class="profile-tip-divider"></div>
                                                                  <div class="profile-tip-btn">
                                                                     <a href="javascript:void(0)" class="btn btn-primary btn-sm"><i class=”mdi mdi-eye”></i>View Profile</a>
                                                                  </div>
                                                               </div>
                                                            </span>
                                                         </div>
                                                      </div>
                                                      <div class="desc-holder">
                                                         <div class="normal-mode">
                                                            <div class="desc">
                                                               <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                                               <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                                            </div>
                                                            <div class="comment-stuff">
                                                               <div class="more-opt">
                                                                  <a href="javascript:void(0)" class="pa-like"><span>icon</span></a>
                                                                  <a href="javascript:void(0)" class="pa-reply reply-comment"><span>icon</span></a>
                                                                  <div class="dropdown dropdown-custom dropdown-xxsmall">
                                                                     <a href="javascript:void(0)" class="dropdown-toggle dropdown-button" data-activates="dropdown-editdeleterev1">
                                                                     <i class="zmdi zmdi-hc-2x zmdi-more-vert"></i>
                                                                     </a>
                                                                     <ul class="dropdown-content" id="dropdown-editdeleterev1">
                                                                        <li><a href="javascript:void(0)" class="edit-comment">Edit</a></li>
                                                                        <li><a href="javascript:void(0)" class="delete-comment">Delete</a></li>
                                                                     </ul>
                                                                  </div>
                                                               </div>
                                                               <div class="less-opt">
                                                                  <div class="timestamp">8h</div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                         <div class="edit-mode">
                                                            <div class="desc">
                                                               <textarea class="editcomment-tt materialize-textarea mb0 md_textarea item_tagline">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                                               <a href="javascript:void(0)" class="btn btn-primary btn-sm editcomment-cancel waves-effect waves-light">Cancel</a>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div class="clear"></div>
                                                   <div class="comment-reply-holder comment-addreply">
                                                      <div class="addnew-comment valign-wrapper comment-reply">
                                                         <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg" /></a></div>
                                                         <div class="desc-holder">
                                                            <div class="sliding-middle-custom anim-area">
                                                               <textarea class="materialize-textarea mb0 md_textarea item_tagline">Write a reply...</textarea>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="pcomment-holder">
                                                   <div class="pcomment main-comment">
                                                      <div class="img-holder">
                                                         <div id="commentptip-2" class="profiletipholder">
                                                            <span class="profile-tooltip">
                                                            <img class="circle" src="images/demo-profile.jpg" />
                                                            </span>
                                                            <span class="profiletooltip_content">
                                                               <div class="profile-tip">
                                                                  <div class="profile-tip-cover"><img src="images/cover.jpg"></div>
                                                                  <div class="profile-tip-avatar">
                                                                     <a href="javascript:void(0)">
                                                                     <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">
                                                                     </a>
                                                                  </div>
                                                                  <div class="profile-tip-info">
                                                                     <div class="cover-username"><a href="javascript:void(0)">Adel Hasanat</a></div>
                                                                     <div class="cover-headline">
                                                                        <span class="ptip-icon"><i class="fa  fa-suitcase"></i></span>
                                                                        Web Designer, Cricketer
                                                                     </div>
                                                                     <div class="profiletip-bio">
                                                                        <span class="ptip-icon"><i class="mdi mdi-home"></i></span>
                                                                        Lives in : <span>Gariyadhar</span>
                                                                     </div>
                                                                     <div class="profiletip-bio">
                                                                        <span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span>
                                                                        Currently in : <span>Gariyadhar, Gujarat, India</span>
                                                                     </div>
                                                                  </div>
                                                                  <div class="profile-tip-divider"></div>
                                                                  <div class="profile-tip-btn">
                                                                     <a href="javascript:void(0)" class="btn btn-primary btn-sm"><i class=”mdi mdi-eye”></i>View Profile</a>
                                                                  </div>
                                                               </div>
                                                            </span>
                                                         </div>
                                                      </div>
                                                      <div class="desc-holder">
                                                         <div class="normal-mode">
                                                            <div class="desc">
                                                               <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                                               <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                                            </div>
                                                            <div class="comment-stuff">
                                                               <div class="more-opt">
                                                                  <a href="javascript:void(0)" class="pa-like"><span>icon</span></a>
                                                                  <a href="javascript:void(0)" class="pa-reply reply-comment"><span>icon</span></a>
                                                                  <div class="dropdown dropdown-custom dropdown-xxsmall">
                                                                     <a href="javascript:void(0)" class="dropdown-toggle dropdown-button" data-activates="dropdown-editdeleterev2">
                                                                     <i class="zmdi zmdi-hc-2x zmdi-more-vert"></i>
                                                                     </a>
                                                                     <ul class="dropdown-content" id="dropdown-editdeleterev2">
                                                                        <li><a href="javascript:void(0)" class="edit-comment">Edit</a></li>
                                                                        <li><a href="javascript:void(0)" class="delete-comment">Delete</a></li>
                                                                     </ul>
                                                                  </div>
                                                               </div>
                                                               <div class="less-opt">
                                                                  <div class="timestamp">8h</div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                         <div class="edit-mode">
                                                            <div class="desc">
                                                               <textarea class="editcomment-tt materialize-textarea mb0 md_textarea item_tagline">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                                               <a href="javascript:void(0)" class="btn btn-primary btn-sm editcomment-cancel waves-effect waves-light">Cancel</a>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div class="clear"></div>
                                                   <div class="comment-reply-holder comment-addreply">
                                                      <div class="addnew-comment valign-wrapper comment-reply">
                                                         <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg" /></a></div>
                                                         <div class="desc-holder">
                                                            <div class="sliding-middle-custom anim-area">
                                                               <textarea class="materialize-textarea mb0 md_textarea item_tagline">Write a reply...</textarea>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="pcomment-holder has-comments">
                                                <div class="pcomment main-comment">
                                                   <div class="img-holder">
                                                      <div id="commentptip-3" class="profiletipholder">
                                                         <span class="profile-tooltip">
                                                         <img class="circle" src="images/demo-profile.jpg" />
                                                         </span>
                                                         <span class="profiletooltip_content">
                                                            <div class="profile-tip">
                                                               <div class="profile-tip-cover"><img src="images/cover.jpg"></div>
                                                               <div class="profile-tip-avatar">
                                                                  <a href="javascript:void(0)">
                                                                  <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">
                                                                  </a>
                                                               </div>
                                                               <div class="profile-tip-info">
                                                                  <div class="cover-username"><a href="javascript:void(0)">Adel Hasanat</a></div>
                                                                  <div class="cover-headline">
                                                                     <span class="ptip-icon"><i class="fa  fa-suitcase"></i></span>
                                                                     Web Designer, Cricketer
                                                                  </div>
                                                                  <div class="profiletip-bio">
                                                                     <span class="ptip-icon"><i class="mdi mdi-home"></i></span>
                                                                     Lives in : <span>Gariyadhar</span>
                                                                  </div>
                                                                  <div class="profiletip-bio">
                                                                     <span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span>
                                                                     Currently in : <span>Gariyadhar, Gujarat, India</span>
                                                                  </div>
                                                               </div>
                                                               <div class="profile-tip-divider"></div>
                                                               <div class="profile-tip-btn">
                                                                  <a href="javascript:void(0)" class="btn btn-primary btn-sm"><i class=”mdi mdi-eye”></i>View Profile</a>
                                                               </div>
                                                            </div>
                                                         </span>
                                                      </div>
                                                   </div>
                                                   <div class="desc-holder">
                                                      <div class="normal-mode">
                                                         <div class="desc">
                                                            <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                                         </div>
                                                         <div class="comment-stuff">
                                                            <div class="more-opt">
                                                               <a href="javascript:void(0)" class="pa-like"><span>icon</span></a>
                                                               <a href="javascript:void(0)" class="pa-reply reply-comment"><span>icon</span></a>
                                                               <div class="dropdown dropdown-custom dropdown-xxsmall">
                                                                  <a href="javascript:void(0)" class="dropdown-toggle dropdown-button" data-activates="dropdown-editdeleterev3">
                                                                  <i class="zmdi zmdi-hc-2x zmdi-more-vert"></i>
                                                                  </a>
                                                                  <ul class="dropdown-content" id="dropdown-editdeleterev3">
                                                                     <li><a href="javascript:void(0)" class="edit-comment">Edit</a></li>
                                                                     <li><a href="javascript:void(0)" class="delete-comment">Delete</a></li>
                                                                  </ul>
                                                               </div>
                                                            </div>
                                                            <div class="less-opt">
                                                               <div class="timestamp">8h</div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div class="edit-mode">
                                                         <div class="desc">
                                                            <textarea class="editcomment-tt materialize-textarea mb0 md_textarea item_tagline">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                                            <a href="javascript:void(0)" class="btn btn-primary btn-sm editcomment-cancel waves-effect waves-light">Cancel</a>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="clear"></div>
                                                <div class="comment-reply-holder">
                                                   <div class="pcomment comment-reply">
                                                      <div class="img-holder">
                                                         <div id="commentptip-5" class="profiletipholder">
                                                            <span class="profile-tooltip">
                                                            <img class="circle" src="images/demo-profile.jpg" />
                                                            </span>
                                                            <span class="profiletooltip_content">
                                                               <div class="profile-tip">
                                                                  <div class="profile-tip-cover"><img src="images/cover.jpg"></div>
                                                                  <div class="profile-tip-avatar">
                                                                     <a href="javascript:void(0)">
                                                                     <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">
                                                                     </a>
                                                                  </div>
                                                                  <div class="profile-tip-info">
                                                                     <div class="cover-username"><a href="javascript:void(0)">Adel Hasanat</a></div>
                                                                     <div class="cover-headline">
                                                                        <span class="ptip-icon"><i class="fa  fa-suitcase"></i></span>
                                                                        Web Designer, Cricketer
                                                                     </div>
                                                                     <div class="profiletip-bio">
                                                                        <span class="ptip-icon"><i class="mdi mdi-home"></i></span>
                                                                        Lives in : <span>Gariyadhar</span>
                                                                     </div>
                                                                     <div class="profiletip-bio">
                                                                        <span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span>
                                                                        Currently in : <span>Gariyadhar, Gujarat, India</span>
                                                                     </div>
                                                                  </div>
                                                                  <div class="profile-tip-divider"></div>
                                                                  <div class="profile-tip-btn">
                                                                     <a href="javascript:void(0)" class="btn btn-primary btn-sm"><i class=”mdi mdi-eye”></i>View Profile</a>
                                                                  </div>
                                                               </div>
                                                            </span>
                                                         </div>
                                                      </div>
                                                      <div class="desc-holder">
                                                         <div class="normal-mode">
                                                            <div class="desc">
                                                               <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                                               <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                                            </div>
                                                            <div class="comment-stuff">
                                                               <div class="more-opt">
                                                                  <a href="javascript:void(0)" class="pa-like"><span>icon</span></a>
                                                                  <a href="javascript:void(0)" class="pa-reply reply-comment"><span>icon</span></a>
                                                                  <div class="dropdown dropdown-custom dropdown-xxsmall">
                                                                     <a href="javascript:void(0)" class="dropdown-toggle dropdown-button" data-activates="dropdown-editdeleterev4">
                                                                     <i class="zmdi zmdi-hc-2x zmdi-more-vert"></i>
                                                                     </a>
                                                                     <ul class="dropdown-content" id="dropdown-editdeleterev4">
                                                                        <li><a href="javascript:void(0)" class="edit-comment">Edit</a></li>
                                                                        <li><a href="javascript:void(0)" class="delete-comment">Delete</a></li>
                                                                     </ul>
                                                                  </div>
                                                               </div>
                                                               <div class="less-opt">
                                                                  <div class="timestamp">8h</div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                         <div class="edit-mode">
                                                            <div class="desc">
                                                               <textarea class="editcomment-tt materialize-textarea mb0 md_textarea item_tagline">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                                               <a href="javascript:void(0)" class="btn btn-primary btn-sm editcomment-cancel waves-effect waves-light">Cancel</a>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div class="pcomment comment-reply">
                                                      <div class="img-holder">
                                                         <div id="commentptip-6" class="profiletipholder">
                                                            <span class="profile-tooltip">
                                                            <img class="circle" src="images/demo-profile.jpg" />
                                                            </span>
                                                            <span class="profiletooltip_content">
                                                               <div class="profile-tip">
                                                                  <div class="profile-tip-cover"><img src="images/cover.jpg"></div>
                                                                  <div class="profile-tip-avatar">
                                                                     <a href="javascript:void(0)">
                                                                     <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">
                                                                     </a>
                                                                  </div>
                                                                  <div class="profile-tip-info">
                                                                     <div class="cover-username"><a href="javascript:void(0)">Adel Hasanat</a></div>
                                                                     <div class="cover-headline">
                                                                        <span class="ptip-icon"><i class="fa  fa-suitcase"></i></span>
                                                                        Web Designer, Cricketer
                                                                     </div>
                                                                     <div class="profiletip-bio">
                                                                        <span class="ptip-icon"><i class="mdi mdi-home"></i></span>
                                                                        Lives in : <span>Gariyadhar</span>
                                                                     </div>
                                                                     <div class="profiletip-bio">
                                                                        <span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span>
                                                                        Currently in : <span>Gariyadhar, Gujarat, India</span>
                                                                     </div>
                                                                  </div>
                                                                  <div class="profile-tip-divider"></div>
                                                                  <div class="profile-tip-btn">
                                                                     <a href="javascript:void(0)" class="btn btn-primary btn-sm"><i class=”mdi mdi-eye”></i>View Profile</a>
                                                                  </div>
                                                               </div>
                                                            </span>
                                                         </div>
                                                      </div>
                                                      <div class="desc-holder">
                                                         <div class="normal-mode">
                                                            <div class="desc">
                                                               <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                                               <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit...</p>
                                                            </div>
                                                            <div class="comment-stuff">
                                                               <div class="more-opt">
                                                                  <a href="javascript:void(0)" class="pa-like"><span>icon</span></a>
                                                                  <a href="javascript:void(0)" class="pa-reply reply-comment"><span>icon</span></a>
                                                                  <div class="dropdown dropdown-custom dropdown-xxsmall">
                                                                     <a href="javascript:void(0)" class="dropdown-toggle dropdown-button" data-activates="dropdown-editdeleterev5">
                                                                     <i class="zmdi zmdi-hc-2x zmdi-more-vert"></i>
                                                                     </a>
                                                                     <ul class="dropdown-content" id="dropdown-editdeleterev5">
                                                                        <li><a href="javascript:void(0)" class="edit-comment">Edit</a></li>
                                                                        <li><a href="javascript:void(0)" class="delete-comment">Delete</a></li>
                                                                     </ul>
                                                                  </div>
                                                               </div>
                                                               <div class="less-opt">
                                                                  <div class="timestamp">8h</div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                         <div class="edit-mode">
                                                            <div class="desc">
                                                               <textarea class="editcomment-tt materialize-textarea mb0 md_textarea item_tagline">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                                               <a href="javascript:void(0)" class="btn btn-primary btn-sm editcomment-cancel waves-effect waves-light">Cancel</a>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="comment-reply-holder comment-addreply">
                                                   <div class="addnew-comment valign-wrapper comment-reply">
                                                      <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg" /></a></div>
                                                      <div class="desc-holder">
                                                         <div class="sliding-middle-custom anim-area">
                                                            <textarea class="materialize-textarea mb0 md_textarea item_tagline">Write a reply...</textarea>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="pcomment-holder">
                                                <div class="pcomment main-comment">
                                                   <div class="img-holder">
                                                      <div id="commentptip-4" class="profiletipholder">
                                                         <span class="profile-tooltip">
                                                         <img class="circle" src="images/demo-profile.jpg" />
                                                         </span>
                                                         <span class="profiletooltip_content">
                                                            <div class="profile-tip">
                                                               <div class="profile-tip-cover"><img src="images/cover.jpg"></div>
                                                               <div class="profile-tip-avatar">
                                                                  <a href="javascript:void(0)">
                                                                  <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">
                                                                  </a>
                                                               </div>
                                                               <div class="profile-tip-info">
                                                                  <div class="cover-username"><a href="javascript:void(0)">Adel Hasanat</a></div>
                                                                  <div class="cover-headline">
                                                                     <span class="ptip-icon"><i class="fa  fa-suitcase"></i></span>
                                                                     Web Designer, Cricketer
                                                                  </div>
                                                                  <div class="profiletip-bio">
                                                                     <span class="ptip-icon"><i class="mdi mdi-home"></i></span>
                                                                     Lives in : <span>Gariyadhar</span>
                                                                  </div>
                                                                  <div class="profiletip-bio">
                                                                     <span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span>
                                                                     Currently in : <span>Gariyadhar, Gujarat, India</span>
                                                                  </div>
                                                               </div>
                                                               <div class="profile-tip-divider"></div>
                                                               <div class="profile-tip-btn">
                                                                  <a href="javascript:void(0)" class="btn btn-primary btn-sm"><i class=”mdi mdi-eye”></i>View Profile</a>
                                                               </div>
                                                            </div>
                                                         </span>
                                                      </div>
                                                   </div>
                                                   <div class="desc-holder">
                                                      <div class="normal-mode">
                                                         <div class="desc">
                                                            <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                                         </div>
                                                         <div class="comment-stuff">
                                                            <div class="more-opt">
                                                               <a href="javascript:void(0)" class="pa-like"><span>icon</span></a>
                                                               <a href="javascript:void(0)" class="pa-reply reply-comment"><span>icon</span></a>
                                                               <div class="dropdown dropdown-custom dropdown-xxsmall">
                                                                  <a href="javascript:void(0)" class="dropdown-toggle dropdown-button" data-activates="dropdown-editdeleterev6">
                                                                  <i class="zmdi zmdi-hc-2x zmdi-more-vert"></i>
                                                                  </a>
                                                                  <ul class="dropdown-content" id="dropdown-editdeleterev6">
                                                                     <li><a href="javascript:void(0)" class="edit-comment">Edit</a></li>
                                                                     <li><a href="javascript:void(0)" class="delete-comment">Delete</a></li>
                                                                  </ul>
                                                               </div>
                                                            </div>
                                                            <div class="less-opt">
                                                               <div class="timestamp">8h</div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div class="edit-mode">
                                                         <div class="desc">
                                                            <textarea class="editcomment-tt materialize-textarea mb0 md_textarea item_tagline">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                                            <a href="javascript:void(0)" class="btn btn-primary btn-sm editcomment-cancel waves-effect waves-light">Cancel</a>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="clear"></div>
                                                <div class="comment-reply-holder comment-addreply">
                                                   <div class="addnew-comment valign-wrapper comment-reply">
                                                      <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg" /></a></div>
                                                      <div class="desc-holder">
                                                         <div class="sliding-middle-custom anim-area">
                                                            <textarea class="materialize-textarea mb0 md_textarea item_tagline">Write a reply...</textarea>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="addnew-comment valign-wrapper">
                                             <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg" /></a></div>
                                             <div class="desc-holder">
                                                <div class="sliding-middle-custom anim-area">
                                                   <textarea class="materialize-textarea mb0 md_textarea item_tagline">Write a comment</textarea>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="post-holder bborder reviewpost-holder">
                              <div class="post-topbar">
                                 <div class="post-userinfo">
                                    <div class="img-holder">
                                       <div id="profiletip-1" class="profiletipholder">
                                          <span class="profile-tooltip">
                                          <img class="circle" src="images/demo-profile.jpg" />
                                          </span>
                                          <span class="profiletooltip_content">
                                             <div class="profile-tip">
                                                <div class="profile-tip-cover"><img src="images/cover.jpg"></div>
                                                <div class="profile-tip-avatar">
                                                   <a href="javascript:void(0)">
                                                   <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">
                                                   </a>
                                                </div>
                                                <div class="profile-tip-info">
                                                   <div class="cover-username"><a href="javascript:void(0)">Adel Hasanat</a></div>
                                                   <div class="cover-headline">
                                                      <span class="ptip-icon"><i class="fa  fa-suitcase"></i></span>
                                                      Web Designer, Cricketer
                                                   </div>
                                                   <div class="profiletip-bio">
                                                      <span class="ptip-icon"><i class="mdi mdi-home"></i></span>
                                                      Lives in : <span>Gariyadhar</span>
                                                   </div>
                                                   <div class="profiletip-bio">
                                                      <span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span>
                                                      Currently in : <span>Gariyadhar, Gujarat, India</span>
                                                   </div>
                                                </div>
                                                <div class="profile-tip-divider"></div>
                                                <div class="profile-tip-btn">
                                                   <a href="javascript:void(0)" class="btn btn-primary btn-sm"><i class=”mdi mdi-eye”></i>View Profile</a>
                                                </div>
                                             </div>
                                          </span>
                                       </div>
                                    </div>
                                    <div class="desc-holder">
                                       <a href="javascript:void(0)">Nimish Parekh</a> reviewed <a class="sub-link" href="javascript:void(0)"><?=$st_nm_S?></a>
                                       <span class="timestamp">August 31 at 08:45 pm<span class="glyphicon glyphicon-globe"></span></span>
                                    </div>
                                 </div>
                                 <div class="settings-icon">
                                    <div class="dropdown">
                                       <a class="dropdown-button" href="javascript:void(0)" data-activates='dropdown-editreview2'>
                                       <i class="zmdi zmdi-more zmdi-hc-2x"></i>
                                       </a>
                                       <ul class="dropdown-content" id="dropdown-editreview2">
                                          <li>
                                             <a href="javascript:void(0)" class="edit_newreview">Edit review</a>
                                          </li>
                                       </ul>
                                    </div>
                                 </div>
                              </div>
                              <div class="post-content">
                                 <div class="post-details">
                                    <div class="rating-stars">
                                       <i class="mdi mdi-star active"></i>
                                       <i class="mdi mdi-star active"></i>
                                       <i class="mdi mdi-star active"></i>
                                       <i class="mdi mdi-star active"></i>
                                       <i class="mdi mdi-star"></i>
                                    </div>
                                    <div class="post-desc">
                                       <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sed varius risus. Duis rhoncus eros et pellentesque imperdiet.</p>
                                    </div>
                                 </div>
                              </div>
                              <div class="clear"></div>
                              <div class="post-data">
                                 <div class="post-actions">
                                    <div class="right like-tooltip">
                                       <a href="javascript:void(0)" class="pa-like" data-title="User Name"><i class="zmdi zmdi-thumb-up"></i></a>
                                       <span class="lcount">4</span>
                                       <a href="javascript:void(0)" class="pa-comment"><i class="zmdi zmdi-comment"></i></a>
                                       <span class="comment-lcount">4</span>
                                    </div>
                                 </div>
                                 <div class="comments-section panel">
                                    <div class="comments-area">
                                       <div class="post-more">
                                          <a href="javascript:void(0)" class="view-morec">View more comments</a>
                                          <span class="total-comments">3 of 7</span>
                                       </div>
                                       <div class="post-comments">
                                          <div class="pcomments">
                                             <div class="pcomment-earlier">
                                                <div class="pcomment-holder">
                                                   <div class="pcomment main-comment">
                                                      <div class="img-holder">
                                                         <div id="commentptip-1" class="profiletipholder">
                                                            <span class="profile-tooltip">
                                                            <img class="circle" src="images/demo-profile.jpg" />
                                                            </span>
                                                            <span class="profiletooltip_content">
                                                               <div class="profile-tip">
                                                                  <div class="profile-tip-cover"><img src="images/cover.jpg"></div>
                                                                  <div class="profile-tip-avatar">
                                                                     <a href="javascript:void(0)">
                                                                     <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">
                                                                     </a>
                                                                  </div>
                                                                  <div class="profile-tip-info">
                                                                     <div class="cover-username"><a href="javascript:void(0)">Adel Hasanat</a></div>
                                                                     <div class="cover-headline">
                                                                        <span class="ptip-icon"><i class="fa  fa-suitcase"></i></span>
                                                                        Web Designer, Cricketer
                                                                     </div>
                                                                     <div class="profiletip-bio">
                                                                        <span class="ptip-icon"><i class="mdi mdi-home"></i></span>
                                                                        Lives in : <span>Gariyadhar</span>
                                                                     </div>
                                                                     <div class="profiletip-bio">
                                                                        <span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span>
                                                                        Currently in : <span>Gariyadhar, Gujarat, India</span>
                                                                     </div>
                                                                  </div>
                                                                  <div class="profile-tip-divider"></div>
                                                                  <div class="profile-tip-btn">
                                                                     <a href="javascript:void(0)" class="btn btn-primary btn-sm"><i class=”mdi mdi-eye”></i>View Profile</a>
                                                                  </div>
                                                               </div>
                                                            </span>
                                                         </div>
                                                      </div>
                                                      <div class="desc-holder">
                                                         <div class="normal-mode">
                                                            <div class="desc">
                                                               <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                                               <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                                            </div>
                                                            <div class="comment-stuff">
                                                               <div class="more-opt">
                                                                  <a href="javascript:void(0)" class="pa-like"><span>icon</span></a>
                                                                  <a href="javascript:void(0)" class="pa-reply reply-comment"><span>icon</span></a>
                                                                  <div class="dropdown dropdown-custom dropdown-xxsmall">
                                                                     <a href="javascript:void(0)" class="dropdown-toggle dropdown-button" data-activates="dropdown-editdeleterev7">
                                                                     <i class="zmdi zmdi-hc-2x zmdi-more-vert"></i>
                                                                     </a>
                                                                     <ul class="dropdown-content" id="dropdown-editdeleterev7">
                                                                        <li><a href="javascript:void(0)" class="edit-comment">Edit</a></li>
                                                                        <li><a href="javascript:void(0)" class="delete-comment">Delete</a></li>
                                                                     </ul>
                                                                  </div>
                                                               </div>
                                                               <div class="less-opt">
                                                                  <div class="timestamp">8h</div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                         <div class="edit-mode">
                                                            <div class="desc">
                                                               <textarea class="editcomment-tt materialize-textarea mb0 md_textarea item_tagline">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                                               <a href="javascript:void(0)" class="btn btn-primary btn-sm editcomment-cancel waves-effect waves-light">Cancel</a>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div class="clear"></div>
                                                   <div class="comment-reply-holder comment-addreply">
                                                      <div class="addnew-comment valign-wrapper comment-reply">
                                                         <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg" /></a></div>
                                                         <div class="desc-holder">
                                                            <div class="sliding-middle-custom anim-area">
                                                               <textarea class="materialize-textarea mb0 md_textarea item_tagline">Write a reply...</textarea>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="pcomment-holder">
                                                   <div class="pcomment main-comment">
                                                      <div class="img-holder">
                                                         <div id="commentptip-2" class="profiletipholder">
                                                            <span class="profile-tooltip">
                                                            <img class="circle" src="images/demo-profile.jpg" />
                                                            </span>
                                                            <span class="profiletooltip_content">
                                                               <div class="profile-tip">
                                                                  <div class="profile-tip-cover"><img src="images/cover.jpg"></div>
                                                                  <div class="profile-tip-avatar">
                                                                     <a href="javascript:void(0)">
                                                                     <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">
                                                                     </a>
                                                                  </div>
                                                                  <div class="profile-tip-info">
                                                                     <div class="cover-username"><a href="javascript:void(0)">Adel Hasanat</a></div>
                                                                     <div class="cover-headline">
                                                                        <span class="ptip-icon"><i class="fa  fa-suitcase"></i></span>
                                                                        Web Designer, Cricketer
                                                                     </div>
                                                                     <div class="profiletip-bio">
                                                                        <span class="ptip-icon"><i class="mdi mdi-home"></i></span>
                                                                        Lives in : <span>Gariyadhar</span>
                                                                     </div>
                                                                     <div class="profiletip-bio">
                                                                        <span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span>
                                                                        Currently in : <span>Gariyadhar, Gujarat, India</span>
                                                                     </div>
                                                                  </div>
                                                                  <div class="profile-tip-divider"></div>
                                                                  <div class="profile-tip-btn">
                                                                     <a href="javascript:void(0)" class="btn btn-primary btn-sm"><i class=”mdi mdi-eye”></i>View Profile</a>
                                                                  </div>
                                                               </div>
                                                            </span>
                                                         </div>
                                                      </div>
                                                      <div class="desc-holder">
                                                         <div class="normal-mode">
                                                            <div class="desc">
                                                               <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                                               <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                                            </div>
                                                            <div class="comment-stuff">
                                                               <div class="more-opt">
                                                                  <a href="javascript:void(0)" class="pa-like"><span>icon</span></a>
                                                                  <a href="javascript:void(0)" class="pa-reply reply-comment"><span>icon</span></a>
                                                                  <div class="dropdown dropdown-custom dropdown-xxsmall">
                                                                     <a href="javascript:void(0)" class="dropdown-toggle dropdown-button" data-activates="dropdown-editdeleterev8">
                                                                     <i class="zmdi zmdi-hc-2x zmdi-more-vert"></i>
                                                                     </a>
                                                                     <ul class="dropdown-content" id="dropdown-editdeleterev8">
                                                                        <li><a href="javascript:void(0)" class="edit-comment">Edit</a></li>
                                                                        <li><a href="javascript:void(0)" class="delete-comment">Delete</a></li>
                                                                     </ul>
                                                                  </div>
                                                               </div>
                                                               <div class="less-opt">
                                                                  <div class="timestamp">8h</div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                         <div class="edit-mode">
                                                            <div class="desc">
                                                               <textarea class="editcomment-tt materialize-textarea mb0 md_textarea item_tagline">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                                               <a href="javascript:void(0)" class="btn btn-primary btn-sm editcomment-cancel waves-effect waves-light">Cancel</a>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div class="clear"></div>
                                                   <div class="comment-reply-holder comment-addreply">
                                                      <div class="addnew-comment valign-wrapper comment-reply">
                                                         <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg" /></a></div>
                                                         <div class="desc-holder">
                                                            <div class="sliding-middle-custom anim-area">
                                                               <textarea class="materialize-textarea mb0 md_textarea item_tagline">Write a reply...</textarea>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="pcomment-holder has-comments">
                                                <div class="pcomment main-comment">
                                                   <div class="img-holder">
                                                      <div id="commentptip-3" class="profiletipholder">
                                                         <span class="profile-tooltip">
                                                         <img class="circle" src="images/demo-profile.jpg" />
                                                         </span>
                                                         <span class="profiletooltip_content">
                                                            <div class="profile-tip">
                                                               <div class="profile-tip-cover"><img src="images/cover.jpg"></div>
                                                               <div class="profile-tip-avatar">
                                                                  <a href="javascript:void(0)">
                                                                  <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">
                                                                  </a>
                                                               </div>
                                                               <div class="profile-tip-info">
                                                                  <div class="cover-username"><a href="javascript:void(0)">Adel Hasanat</a></div>
                                                                  <div class="cover-headline">
                                                                     <span class="ptip-icon"><i class="fa  fa-suitcase"></i></span>
                                                                     Web Designer, Cricketer
                                                                  </div>
                                                                  <div class="profiletip-bio">
                                                                     <span class="ptip-icon"><i class="mdi mdi-home"></i></span>
                                                                     Lives in : <span>Gariyadhar</span>
                                                                  </div>
                                                                  <div class="profiletip-bio">
                                                                     <span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span>
                                                                     Currently in : <span>Gariyadhar, Gujarat, India</span>
                                                                  </div>
                                                               </div>
                                                               <div class="profile-tip-divider"></div>
                                                               <div class="profile-tip-btn">
                                                                  <a href="javascript:void(0)" class="btn btn-primary btn-sm"><i class=”mdi mdi-eye”></i>View Profile</a>
                                                               </div>
                                                            </div>
                                                         </span>
                                                      </div>
                                                   </div>
                                                   <div class="desc-holder">
                                                      <div class="normal-mode">
                                                         <div class="desc">
                                                            <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                                         </div>
                                                         <div class="comment-stuff">
                                                            <div class="more-opt">
                                                               <a href="javascript:void(0)" class="pa-like"><span>icon</span></a>
                                                               <a href="javascript:void(0)" class="pa-reply reply-comment"><span>icon</span></a>
                                                               <div class="dropdown dropdown-custom dropdown-xxsmall">
                                                                  <a href="javascript:void(0)" class="dropdown-toggle dropdown-button" data-activates="dropdown-editdeleterev9">
                                                                  <i class="zmdi zmdi-hc-2x zmdi-more-vert"></i>
                                                                  </a>
                                                                  <ul class="dropdown-content" id="dropdown-editdeleterev9">
                                                                     <li><a href="javascript:void(0)" class="edit-comment">Edit</a></li>
                                                                     <li><a href="javascript:void(0)" class="delete-comment">Delete</a></li>
                                                                  </ul>
                                                               </div>
                                                            </div>
                                                            <div class="less-opt">
                                                               <div class="timestamp">8h</div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div class="edit-mode">
                                                         <div class="desc">
                                                            <textarea class="editcomment-tt materialize-textarea mb0 md_textarea item_tagline">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                                            <a href="javascript:void(0)" class="btn btn-primary btn-sm editcomment-cancel waves-effect waves-light">Cancel</a>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="clear"></div>
                                                <div class="comment-reply-holder">
                                                   <div class="pcomment comment-reply">
                                                      <div class="img-holder">
                                                         <div id="commentptip-5" class="profiletipholder">
                                                            <span class="profile-tooltip">
                                                            <img class="circle" src="images/demo-profile.jpg" />
                                                            </span>
                                                            <span class="profiletooltip_content">
                                                               <div class="profile-tip">
                                                                  <div class="profile-tip-cover"><img src="images/cover.jpg"></div>
                                                                  <div class="profile-tip-avatar">
                                                                     <a href="javascript:void(0)">
                                                                     <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">
                                                                     </a>
                                                                  </div>
                                                                  <div class="profile-tip-info">
                                                                     <div class="cover-username"><a href="javascript:void(0)">Adel Hasanat</a></div>
                                                                     <div class="cover-headline">
                                                                        <span class="ptip-icon"><i class="fa  fa-suitcase"></i></span>
                                                                        Web Designer, Cricketer
                                                                     </div>
                                                                     <div class="profiletip-bio">
                                                                        <span class="ptip-icon"><i class="mdi mdi-home"></i></span>
                                                                        Lives in : <span>Gariyadhar</span>
                                                                     </div>
                                                                     <div class="profiletip-bio">
                                                                        <span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span>
                                                                        Currently in : <span>Gariyadhar, Gujarat, India</span>
                                                                     </div>
                                                                  </div>
                                                                  <div class="profile-tip-divider"></div>
                                                                  <div class="profile-tip-btn">
                                                                     <a href="javascript:void(0)" class="btn btn-primary btn-sm"><i class=”mdi mdi-eye”></i>View Profile</a>
                                                                  </div>
                                                               </div>
                                                            </span>
                                                         </div>
                                                      </div>
                                                      <div class="desc-holder">
                                                         <div class="normal-mode">
                                                            <div class="desc">
                                                               <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                                               <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                                            </div>
                                                            <div class="comment-stuff">
                                                               <div class="more-opt">
                                                                  <a href="javascript:void(0)" class="pa-like"><span>icon</span></a>
                                                                  <a href="javascript:void(0)" class="pa-reply reply-comment"><span>icon</span></a>
                                                                  <div class="dropdown dropdown-custom dropdown-xxsmall">
                                                                     <a href="javascript:void(0)" class="dropdown-toggle dropdown-button" data-activates="dropdown-editdeleterev10">
                                                                     <i class="zmdi zmdi-hc-2x zmdi-more-vert"></i>
                                                                     </a>
                                                                     <ul class="dropdown-content" id="dropdown-editdeleterev10">
                                                                        <li><a href="javascript:void(0)" class="edit-comment">Edit</a></li>
                                                                        <li><a href="javascript:void(0)" class="delete-comment">Delete</a></li>
                                                                     </ul>
                                                                  </div>
                                                               </div>
                                                               <div class="less-opt">
                                                                  <div class="timestamp">8h</div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                         <div class="edit-mode">
                                                            <div class="desc">
                                                               <textarea class="editcomment-tt materialize-textarea mb0 md_textarea item_tagline">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                                               <a href="javascript:void(0)" class="btn btn-primary btn-sm editcomment-cancel waves-effect waves-light">Cancel</a>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div class="pcomment comment-reply">
                                                      <div class="img-holder">
                                                         <div id="commentptip-6" class="profiletipholder">
                                                            <span class="profile-tooltip">
                                                            <img class="circle" src="images/demo-profile.jpg" />
                                                            </span>
                                                            <span class="profiletooltip_content">
                                                               <div class="profile-tip">
                                                                  <div class="profile-tip-cover"><img src="images/cover.jpg"></div>
                                                                  <div class="profile-tip-avatar">
                                                                     <a href="javascript:void(0)">
                                                                     <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">
                                                                     </a>
                                                                  </div>
                                                                  <div class="profile-tip-info">
                                                                     <div class="cover-username"><a href="javascript:void(0)">Adel Hasanat</a></div>
                                                                     <div class="cover-headline">
                                                                        <span class="ptip-icon"><i class="fa  fa-suitcase"></i></span>
                                                                        Web Designer, Cricketer
                                                                     </div>
                                                                     <div class="profiletip-bio">
                                                                        <span class="ptip-icon"><i class="mdi mdi-home"></i></span>
                                                                        Lives in : <span>Gariyadhar</span>
                                                                     </div>
                                                                     <div class="profiletip-bio">
                                                                        <span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span>
                                                                        Currently in : <span>Gariyadhar, Gujarat, India</span>
                                                                     </div>
                                                                  </div>
                                                                  <div class="profile-tip-divider"></div>
                                                                  <div class="profile-tip-btn">
                                                                     <a href="javascript:void(0)" class="btn btn-primary btn-sm"><i class=”mdi mdi-eye”></i>View Profile</a>
                                                                  </div>
                                                               </div>
                                                            </span>
                                                         </div>
                                                      </div>
                                                      <div class="desc-holder">
                                                         <div class="normal-mode">
                                                            <div class="desc">
                                                               <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                                               <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit...</p>
                                                            </div>
                                                            <div class="comment-stuff">
                                                               <div class="more-opt">
                                                                  <a href="javascript:void(0)" class="pa-like"><span>icon</span></a>
                                                                  <a href="javascript:void(0)" class="pa-reply reply-comment"><span>icon</span></a>
                                                                  <div class="dropdown dropdown-custom dropdown-xxsmall">
                                                                     <a href="javascript:void(0)" class="dropdown-toggle dropdown-button" data-activates="dropdown-editdeleterev11">
                                                                     <i class="zmdi zmdi-hc-2x zmdi-more-vert"></i>
                                                                     </a>
                                                                     <ul class="dropdown-content" id="dropdown-editdeleterev11">
                                                                        <li><a href="javascript:void(0)" class="edit-comment">Edit</a></li>
                                                                        <li><a href="javascript:void(0)" class="delete-comment">Delete</a></li>
                                                                     </ul>
                                                                  </div>
                                                               </div>
                                                               <div class="less-opt">
                                                                  <div class="timestamp">8h</div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                         <div class="edit-mode">
                                                            <div class="desc">
                                                               <textarea class="editcomment-tt materialize-textarea mb0 md_textarea item_tagline">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                                               <a href="javascript:void(0)" class="btn btn-primary btn-sm editcomment-cancel waves-effect waves-light">Cancel</a>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="comment-reply-holder comment-addreply">
                                                   <div class="addnew-comment valign-wrapper comment-reply">
                                                      <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg" /></a></div>
                                                      <div class="desc-holder">
                                                         <div class="sliding-middle-custom anim-area">
                                                            <textarea class="materialize-textarea mb0 md_textarea item_tagline">Write a reply...</textarea>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="pcomment-holder">
                                                <div class="pcomment main-comment">
                                                   <div class="img-holder">
                                                      <div id="commentptip-4" class="profiletipholder">
                                                         <span class="profile-tooltip">
                                                         <img class="circle" src="images/demo-profile.jpg" />
                                                         </span>
                                                         <span class="profiletooltip_content">
                                                            <div class="profile-tip">
                                                               <div class="profile-tip-cover"><img src="images/cover.jpg"></div>
                                                               <div class="profile-tip-avatar">
                                                                  <a href="javascript:void(0)">
                                                                  <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">
                                                                  </a>
                                                               </div>
                                                               <div class="profile-tip-info">
                                                                  <div class="cover-username"><a href="javascript:void(0)">Adel Hasanat</a></div>
                                                                  <div class="cover-headline">
                                                                     <span class="ptip-icon"><i class="fa  fa-suitcase"></i></span>
                                                                     Web Designer, Cricketer
                                                                  </div>
                                                                  <div class="profiletip-bio">
                                                                     <span class="ptip-icon"><i class="mdi mdi-home"></i></span>
                                                                     Lives in : <span>Gariyadhar</span>
                                                                  </div>
                                                                  <div class="profiletip-bio">
                                                                     <span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span>
                                                                     Currently in : <span>Gariyadhar, Gujarat, India</span>
                                                                  </div>
                                                               </div>
                                                               <div class="profile-tip-divider"></div>
                                                               <div class="profile-tip-btn">
                                                                  <a href="javascript:void(0)" class="btn btn-primary btn-sm"><i class=”mdi mdi-eye”></i>View Profile</a>
                                                               </div>
                                                            </div>
                                                         </span>
                                                      </div>
                                                   </div>
                                                   <div class="desc-holder">
                                                      <div class="normal-mode">
                                                         <div class="desc">
                                                            <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                                         </div>
                                                         <div class="comment-stuff">
                                                            <div class="more-opt">
                                                               <a href="javascript:void(0)" class="pa-like"><span>icon</span></a>
                                                               <a href="javascript:void(0)" class="pa-reply reply-comment"><span>icon</span></a>
                                                               <div class="dropdown dropdown-custom dropdown-xxsmall">
                                                                  <a href="javascript:void(0)" class="dropdown-toggle dropdown-button" data-activates="dropdown-editdeleterev12">
                                                                  <i class="zmdi zmdi-hc-2x zmdi-more-vert"></i>
                                                                  </a>
                                                                  <ul class="dropdown-content" id="dropdown-editdeleterev12">
                                                                     <li><a href="javascript:void(0)" class="edit-comment">Edit</a></li>
                                                                     <li><a href="javascript:void(0)" class="delete-comment">Delete</a></li>
                                                                  </ul>
                                                               </div>
                                                            </div>
                                                            <div class="less-opt">
                                                               <div class="timestamp">8h</div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div class="edit-mode">
                                                         <div class="desc">
                                                            <textarea class="editcomment-tt materialize-textarea mb0 md_textarea item_tagline">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                                            <a href="javascript:void(0)" class="btn btn-primary btn-sm editcomment-cancel waves-effect waves-light">Cancel</a>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="clear"></div>
                                                <div class="comment-reply-holder comment-addreply">
                                                   <div class="addnew-comment valign-wrapper comment-reply">
                                                      <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg" /></a></div>
                                                      <div class="desc-holder">
                                                         <div class="sliding-middle-custom anim-area">
                                                            <textarea class="materialize-textarea mb0 md_textarea item_tagline">Write a reply...</textarea>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="addnew-comment valign-wrapper">
                                             <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg" /></a></div>
                                             <div class="desc-holder">
                                                <div class="sliding-middle-custom anim-area">
                                                   <textarea class="materialize-textarea mb0 md_textarea item_tagline">Write a comment</textarea>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="new-post-mobile clear">
                        <a href="javascript:void(0)" class="popup-window compose_newreview" ><i class="mdi mdi-pencil"></i></a>
                     </div>
                  </div>
               </div>
            </div>
            <div id="places-travellers" class="placestravellers-content subtab bottom_tabs">
               <div class="content-box">
                  <div class="mbl-tabnav">
                     <a href="javascript:void(0)" onclick="openDirectTab('places-all')"><i class="mdi mdi-arrow-left"></i></a> 
                     <h6>Travellers</h6>
                  </div>
                  <div class="cbox-title nborder">
                     People travelling to <?=$st_nm_S?> <span class="lt">(8)</span>
                  </div>
                  <div class="cbox-desc person-list">
                     <div class="row">
                        <div class="col s12 m4 l3 xl3">
                           <div class="person-box">
                              <div class="imgholder"> 
                                 <img src="images/demo-profile.jpg" />
                                 <div class="overlay">
                                    <div class="vip-span"><img src="images/vip-tag.png"/></div>
                                    <div class="add-span">
                                       <a href="javascript:void(0)" class="add-icon gray-text-555"><i class="mdi mdi-account-plus"></i></a>
                                       <a href="javascript:void(0)" class="add-btn btn btn-primary" onclick="addConnectBtn(this)">Add Connect</a>
                                    </div>
                                 </div>
                              </div>
                              <div class="descholder">
                                 <h5><a href="javascript:void(0)">Joe Doe<span class="online-dot"><i class="zmdi zmdi-check"></i></span></a></h5>
                                 <p>Lives in London</p>
                              </div>
                           </div>
                        </div>
                        <div class="col s12 m4 l3 xl3">
                           <div class="person-box">
                              <div class="imgholder  online-img">
                                 <img src="images/demo-profile.jpg" />
                                 <div class="overlay">
                                    <div class="vip-span"><img src="images/vip-tag.png" /></div>
                                    <div class="more-span">
                                       <div class="dropdown">
                                          <a href="javascript:void(0)" class="dropdown-toggle dropdown-button gray-text-555" data-activates="dropdown-travellers2"><i class="mdi mdi-chevron-down"></i></a>
                                          <ul class="dropdown-menu dropdown-content" id="dropdown-travellers2">
                                             <li><a href="javascript:void(0)">option 1</a></li>
                                             <li><a href="javascript:void(0)">option 2</a></li>
                                             <li><a href="javascript:void(0)">option 2</a></li>
                                             <li><a href="javascript:void(0)">option 2</a></li>
                                             <li><a href="javascript:void(0)">option 2</a></li>
                                             <li><a href="javascript:void(0)">option 2</a></li>
                                          </ul>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="descholder">
                                 <h5><a href="javascript:void(0)">Joe Doe</a></h5>
                                 <p>Lives in London</p>
                              </div>
                           </div>
                        </div>
                        <div class="col s12 m4 l3 xl3">
                           <div class="person-box">
                              <div class="imgholder">
                                 <img src="images/demo-profile.jpg" />
                                 <div class="overlay">
                                    <div class="add-span">
                                       <a href="javascript:void(0)" class="add-icon gray-text-555"><i class="mdi mdi-account-plus"></i></a>
                                       <a href="javascript:void(0)" class="add-btn btn btn-primary" onclick="addConnectBtn(this)">Add Connect</a>
                                    </div>
                                 </div>
                              </div>
                              <div class="descholder">
                                 <h5><a href="javascript:void(0)">Joe Doe<span class="online-dot"><i class="zmdi zmdi-check"></i></span></a></h5>
                                 <p>Lives in London</p>
                              </div>
                           </div>
                        </div>
                        <div class="col s12 m4 l3 xl3">
                           <div class="person-box">
                              <div class="imgholder  online-img">
                                 <img src="images/demo-profile.jpg" />
                                 <div class="overlay">
                                    <div class="more-span">
                                       <div class="dropdown">
                                          <a href="javascript:void(0)" class="dropdown-toggle dropdown-button gray-text-555" data-activates="dropdown-travellers4"><i class="mdi mdi-chevron-down"></i></a>
                                          <ul class="dropdown-menu dropdown-content" id="dropdown-travellers4">
                                             <li><a href="javascript:void(0)">option 1</a></li>
                                             <li><a href="javascript:void(0)">option 2</a></li>
                                          </ul>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="descholder">
                                 <h5><a href="javascript:void(0)">Joe Doe</a></h5>
                                 <p>Lives in London</p>
                              </div>
                           </div>
                        </div>
                        <div class="col s12 m4 l3 xl3">
                           <div class="person-box">
                              <div class="imgholder">
                                 <img src="images/demo-profile.jpg" />
                                 <div class="overlay">
                                    <div class="vip-span"><img src="images/vip-tag.png" /></div>
                                    <div class="more-span">
                                       <div class="dropdown">
                                          <a href="javascript:void(0)" class="dropdown-toggle dropdown-button gray-text-555" data-activates="dropdown-travellers5"><i class="mdi mdi-chevron-down"></i></a>
                                          <ul class="dropdown-menu dropdown-content" id="dropdown-travellers5">
                                             <li><a href="javascript:void(0)">option 1</a></li>
                                             <li><a href="javascript:void(0)">option 2</a></li>
                                          </ul>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="descholder">
                                 <h5><a href="javascript:void(0)">Joe Doe<span class="online-dot"><i class="zmdi zmdi-check"></i></span></a></h5>
                                 <p>Lives in London</p>
                              </div>
                           </div>
                        </div>
                        <div class="col s12 m4 l3 xl3">
                           <div class="person-box">
                              <div class="imgholder">
                                 <img src="images/demo-profile.jpg" />
                                 <div class="overlay">
                                    <div class="vip-span"><img src="images/vip-tag.png" /></div>
                                    <div class="more-span">
                                       <div class="dropdown">
                                          <a href="javascript:void(0)" class="dropdown-toggle dropdown-button gray-text-555" data-activates="dropdown-travellers6"><i class="mdi mdi-chevron-down"></i></a>
                                          <ul class="dropdown-menu dropdown-content" id="dropdown-travellers6">
                                             <li><a href="javascript:void(0)">option 1</a></li>
                                             <li><a href="javascript:void(0)">option 2</a></li>
                                          </ul>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="descholder">
                                 <h5><a href="javascript:void(0)">Joe Doe</a></h5>
                                 <p>Lives in London</p>
                              </div>
                           </div>
                        </div>
                        <div class="col s12 m4 l3 xl3">
                           <div class="person-box">
                              <div class="imgholder">
                                 <img src="images/demo-profile.jpg" />
                                 <div class="overlay">
                                    <div class="more-span">
                                       <div class="dropdown">
                                          <a href="javascript:void(0)" class="dropdown-toggle dropdown-button gray-text-555" data-activates="dropdown-travellers7"><i class="mdi mdi-chevron-down"></i></a>
                                          <ul class="dropdown-menu dropdown-content" id="dropdown-travellers7">
                                             <li><a href="javascript:void(0)">option 1</a></li>
                                             <li><a href="javascript:void(0)">option 2</a></li>
                                          </ul>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="descholder">
                                 <h5><a href="javascript:void(0)">Joe Doe</a></h5>
                                 <p>Lives in London</p>
                              </div>
                           </div>
                        </div>
                        <div class="col s12 m4 l3 xl3">
                           <div class="person-box">
                              <div class="imgholder">
                                 <img src="images/demo-profile.jpg" />
                                 <div class="overlay">
                                    <div class="add-span">
                                       <a href="javascript:void(0)" class="add-icon gray-text-555"><i class="mdi mdi-account-plus"></i></a>
                                       <a href="javascript:void(0)" class="add-btn btn btn-primary" onclick="addConnectBtn(this)">Add Connect</a>
                                    </div>
                                 </div>
                              </div>
                              <div class="descholder">
                                 <h5><a href="javascript:void(0)">Joe Doe<span class="online-dot"><i class="zmdi zmdi-check"></i></span></a></h5>
                                 <p>Lives in London</p>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div id="places-locals" class="placeslocals-content subtab bottom_tabs">
               <div class="content-box">
                  <div class="mbl-tabnav">
                     <a href="javascript:void(0)" onclick="openDirectTab('places-all')"><i class="mdi mdi-arrow-left"></i></a> 
                     <h6>Locals</h6>
                  </div>
                  <div class="cbox-title nborder">
                     <?=$st_nm_S?> Locals <span class="lt">(8)</span>
                  </div>
                  <div class="cbox-desc person-list">
                     <div class="row">
                        <div class="col s12 m4 l3 xl3">
                           <div class="person-box">
                              <div class="imgholder">
                                 <img src="images/demo-profile.jpg" />
                                 <div class="overlay">
                                    <div class="more-span">
                                       <div class="dropdown">
                                          <a href="javascript:void(0)" class="dropdown-toggle dropdown-button gray-text-555" data-activates="dropdown-locals1 "><i class="mdi mdi-chevron-down"></i></a>
                                          <ul class="dropdown-menu dropdown-content" id="dropdown-locals1">
                                             <li><a href="javascript:void(0)">option 1</a></li>
                                             <li><a href="javascript:void(0)">option 2</a></li>
                                          </ul>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="descholder">
                                 <h5><a href="javascript:void(0)">Joe Doe<span class="online-dot"><i class="zmdi zmdi-check"></i></span></a></h5>
                                 <p>Lives in London</p>
                              </div>
                           </div>
                        </div>
                        <div class="col s12 m4 l3 xl3">
                           <div class="person-box">
                              <div class="imgholder">
                                 <img src="images/demo-profile.jpg" />
                                 <div class="overlay">
                                    <div class="vip-span"><img src="images/vip-tag.png" /></div>
                                    <div class="more-span">
                                       <div class="dropdown">
                                          <a href="javascript:void(0)" class="dropdown-toggle dropdown-button gray-text-555" data-activates="dropdown-locals2"><i class="mdi mdi-chevron-down"></i></a>
                                          <ul class="dropdown-menu dropdown-content" id="dropdown-locals2">
                                             <li><a href="javascript:void(0)">option 1</a></li>
                                             <li><a href="javascript:void(0)">option 2</a></li>
                                          </ul>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="descholder">
                                 <h5><a href="javascript:void(0)">Joe Doe</a></h5>
                                 <p>Lives in London</p>
                              </div>
                           </div>
                        </div>
                        <div class="col s12 m4 l3 xl3">
                           <div class="person-box">
                              <div class="imgholder">
                                 <img src="images/demo-profile.jpg" />
                                 <div class="overlay">
                                    <div class="add-span">
                                       <a href="javascript:void(0)" class="add-icon gray-text-555"><i class="mdi mdi-account-plus"></i></a>
                                       <a href="javascript:void(0)" class="add-btn btn btn-primary" onclick="addConnectBtn(this)">Add Connect</a>
                                    </div>
                                 </div>
                              </div>
                              <div class="descholder">
                                 <h5><a href="javascript:void(0)">Joe Doe<span class="online-dot"><i class="zmdi zmdi-check"></i></span></a></h5>
                                 <p>Lives in London</p>
                              </div>
                           </div>
                        </div>
                        <div class="col s12 m4 l3 xl3">
                           <div class="person-box">
                              <div class="imgholder">
                                 <img src="images/demo-profile.jpg" />
                                 <div class="overlay">
                                    <div class="more-span">
                                       <div class="dropdown">
                                          <a href="javascript:void(0)" class="dropdown-toggle dropdown-button gray-text-555" data-activates="dropdown-locals4"><i class="mdi mdi-chevron-down"></i></a>
                                          <ul class="dropdown-menu dropdown-content" id="dropdown-locals4">
                                             <li><a href="javascript:void(0)">option 1</a></li>
                                             <li><a href="javascript:void(0)">option 2</a></li>
                                          </ul>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="descholder">
                                 <h5><a href="javascript:void(0)">Joe Doe</a></h5>
                                 <p>Lives in London</p>
                              </div>
                           </div>
                        </div>
                        <div class="col s12 m4 l3 xl3">
                           <div class="person-box">
                              <div class="imgholder">
                                 <img src="images/demo-profile.jpg" />
                                 <div class="overlay">
                                    <div class="vip-span"><img src="images/vip-tag.png" /></div>
                                    <div class="add-span">
                                       <a href="javascript:void(0)" class="add-icon gray-text-555"><i class="mdi mdi-account-plus"></i></a>
                                       <a href="javascript:void(0)" class="add-btn btn btn-primary" onclick="addConnectBtn(this)">Add Connect</a>
                                    </div>
                                 </div>
                              </div>
                              <div class="descholder">
                                 <h5><a href="javascript:void(0)">Joe Doe<span class="online-dot"><i class="zmdi zmdi-check"></i></span></a></h5>
                                 <p>Lives in London</p>
                              </div>
                           </div>
                        </div>
                        <div class="col s12 m4 l3 xl3">
                           <div class="person-box">
                              <div class="imgholder">
                                 <img src="images/demo-profile.jpg" />
                                 <div class="overlay">
                                    <div class="vip-span"><img src="images/vip-tag.png" /></div>
                                    <div class="more-span">
                                       <div class="dropdown">
                                          <a href="javascript:void(0)" class="dropdown-toggle dropdown-button gray-text-555" data-activates="dropdown-locals6"><i class="mdi mdi-chevron-down"></i></a>
                                          <ul class="dropdown-menu dropdown-content" id="dropdown-locals6">
                                             <li><a href="javascript:void(0)">option 1</a></li>
                                             <li><a href="javascript:void(0)">option 2</a></li>
                                          </ul>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="descholder">
                                 <h5><a href="javascript:void(0)">Joe Doe</a></h5>
                                 <p>Lives in London</p>
                              </div>
                           </div>
                        </div>
                        <div class="col s12 m4 l3 xl3">
                           <div class="person-box">
                              <div class="imgholder">
                                 <img src="images/demo-profile.jpg" />
                                 <div class="overlay">
                                    <div class="more-span">
                                       <div class="dropdown">
                                          <a href="javascript:void(0)" class="dropdown-toggle dropdown-button gray-text-555" data-activates="dropdown-locals7"><i class="mdi mdi-chevron-down"></i></a>
                                          <ul class="dropdown-menu dropdown-content" id="dropdown-locals7">
                                             <li><a href="javascript:void(0)">option 1</a></li>
                                             <li><a href="javascript:void(0)">option 2</a></li>
                                          </ul>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="descholder">
                                 <h5><a href="javascript:void(0)">Joe Doe</a></h5>
                                 <p>Lives in London</p>
                              </div>
                           </div>
                        </div>
                        <div class="col s12 m4 l3 xl3">
                           <div class="person-box">
                              <div class="imgholder">
                                 <img src="images/demo-profile.jpg" />
                                 <div class="overlay">
                                    <div class="add-span">
                                       <a href="javascript:void(0)" class="add-icon gray-text-555"><i class="mdi mdi-account-plus"></i></a>
                                       <a href="javascript:void(0)" class="add-btn btn btn-primary" onclick="addConnectBtn(this)">Add Connect</a>
                                    </div>
                                    <div class="more-span">
                                       <div class="dropdown">
                                          <a href="javascript:void(0)" class="dropdown-toggle dropdown-button gray-text-555" data-activates="dropdown-locals8"><i class="mdi mdi-chevron-down"></i></a>
                                          <ul class="dropdown-menu dropdown-content" id="dropdown-locals8">
                                             <li><a href="javascript:void(0)">option 1</a></li>
                                             <li><a href="javascript:void(0)">option 2</a></li>
                                          </ul>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="descholder">
                                 <h5><a href="javascript:void(0)">Joe Doe<span class="online-dot"><i class="zmdi zmdi-check"></i></span></a></h5>
                                 <p>Lives in London</p>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div id="places-photos" class="placesphotos-content subtab bottom_tabs">
               <div class="content-box">
                  <div class="mbl-tabnav">
                     <a href="javascript:void(0)" onclick="openDirectTab('places-all')"><i class="mdi mdi-arrow-left"></i></a> 
                     <h6>Photos</h6>
                  </div>
                  <div class="cbox-title">
                     <?=$st_nm_S?> Photos <span class="lt">(20)</span>
                     <a href="javascript:void(0)" class="right-link"></a>
                     <div class="right po_asb">
                        <form>
                           <div class="custom-file">
                              <div class="title "><i class="mdi mdi-plus"></i> Upload photo</div>
                              <input name="upload" class="new_uploader remove_height upload-gallery" title="Choose a file to upload" required="" multiple="" type="file">
                           </div>
                        </form>
                     </div> 
                  </div>
                  <div class="cbox-desc gallery-content">
                     <div class="gloader"><img src="images/loading.gif" class="g-loading" /></div>
                     <div class="lgt-gallery-photo lgt-gallery-justified dis-none">
                        <div data-src="images/wgallery1.jpg" class="allow-gallery">
                             <img class="himg" src="images/wgallery1.jpg"/>  
                             <a href="javascript:void(0)" class="removeicon prevent-gallery" onclick="removepic(this)"><i class="mdi mdi-delete"></i></a>
                           <div class="caption">
                              <div class="left">
                                 <span class="title">Big Bird</span> <br>
                                 <span class="attribution">By Adel Hasanat</span>
                              </div>
                              <div class="right icons">
                                 <a href="javascript: void(0)" class="prevent-gallery like"><i class="mdi mdi-thumb-up-outline mdi-15px" onclick="toggleIcons(this)"></i><span class="lcount">7</span>
                                 </a>
                                 <a href="javascript: void(0)" class="prevent-gallery"><i class="mdi mdi-comment-outline mdi-15px cmnt"></i></a>
                              </div>
                           </div>
                        </div>
                        <div data-src="images/wgallery2.jpg" class="allow-gallery">
                             <img class="himg" src="images/wgallery2.jpg"/>  
                             <a href="javascript:void(0)" class="removeicon prevent-gallery" onclick="removepic(this)"><i class="mdi mdi-delete"></i></a>
                           <div class="caption">
                              <div class="left">
                                 <span class="title">Big Bird</span> <br>
                                 <span class="attribution">By Adel Hasanat</span>
                              </div>
                              <div class="right icons">
                                 <a href="javascript: void(0)" class="prevent-gallery like"><i class="mdi mdi-thumb-up-outline mdi-15px" onclick="toggleIcons(this)"></i><span class="lcount">7</span></a>
                                 <a href="javascript: void(0)" class="prevent-gallery"><i class="mdi mdi-comment-outline mdi-15px cmnt"></i></a>
                              </div>
                           </div>
                        </div>
                           <div data-src="images/wgallery3.jpg" class="allow-gallery">
                             <img class="himg" src="images/wgallery3.jpg"/>  
                             <a href="javascript:void(0)" class="removeicon prevent-gallery" onclick="removepic(this)"><i class="mdi mdi-delete"></i></a>
                           <div class="caption">
                              <div class="left">
                                 <span class="title">Big Bird</span> <br>
                                 <span class="attribution">By Adel Hasanat</span>
                              </div>
                              <div class="right icons">
                                 <a href="javascript: void(0)" class="prevent-gallery like"><i class="mdi mdi-thumb-up-outline mdi-15px" onclick="toggleIcons(this)"></i><span class="lcount">7</span></a>
                                 <a href="javascript: void(0)" class="prevent-gallery"><i class="mdi mdi-comment-outline mdi-15px cmnt"></i></a>
                              </div>
                           </div>
                        </div>
                       </div>
                  </div>
               </div>
            </div>
            <div id="places-tip" class="placestip-content subtab bottom_tabs">
               <div class="row cshfsiput cshfsi">
                  <div class="social-section profile-box detailBox">
                    <div class="item titlelabel user-info">
                      <p class="center-align u-name mb0">Discussion for Amman (<span class="count">10</span>)</p>
                      <p class="user-desg center-align"></p>
                    </div>
                    <div class="item profile-info row mx-0 width-100">
                      <div class="sub-item">
                        <h5 class="">Photo</h5>
                        <p class="">10</p>
                      </div>
                      <div class="sub-item">
                        <h5 class="">Post</h5>
                        <p class="">15</p>
                      </div>
                      <div class="sub-item">
                        <h5 class="">Connections</h5>
                        <p class="">20</p>
                      </div>
                      <div class="sub-item">
                        <h5 class="">Profile views</h5>
                        <p class="">05</p>
                      </div>
                    </div>
                  </div>
                  <div class="postBox">
                     <div class="content-box ">
                        <div class="new-post base-newpost cshfsiput cshfsi compose_newtrip">
                           <div class="npost-content">
                              <div class="post-mcontent">
                                 <i class="mdi mdi-pencil-box-outline main-icon"></i>
                                 <div class="desc">
                                    <div class="input-field comments_box">
                                       <p>Add a tip</p>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>

                        <div class="cbox-desc nm-postlist post-list cshfsiput cshfsi">
                           <div class="post-holder bborder tippost-holder">
                              <div class="post-topbar">
                                 <div class="post-userinfo">
                                    <div class="img-holder">
                                       <div id="profiletip-1" class="profiletipholder">
                                          <span class="profile-tooltip">
                                          <img class="circle" src="images/demo-profile.jpg" />
                                          </span>
                                          <span class="profiletooltip_content">
                                             <div class="profile-tip">
                                                <div class="profile-tip-cover"><img src="images/cover.jpg"></div>
                                                <div class="profile-tip-avatar">
                                                   <a href="javascript:void(0)">
                                                   <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">
                                                   </a>
                                                </div>
                                                <div class="profile-tip-info">
                                                   <div class="cover-username"><a href="javascript:void(0)">Adel Hasanat</a></div>
                                                   <div class="cover-headline">
                                                      <span class="ptip-icon"><i class="fa  fa-suitcase"></i></span>
                                                      Web Designer, Cricketer
                                                   </div>
                                                   <div class="profiletip-bio">
                                                      <span class="ptip-icon"><i class="mdi mdi-home"></i></span>
                                                      Lives in : <span>Gariyadhar</span>
                                                   </div>
                                                   <div class="profiletip-bio">
                                                      <span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span>
                                                      Currently in : <span>Gariyadhar, Gujarat, India</span>
                                                   </div>
                                                </div>
                                                <div class="profile-tip-divider"></div>
                                                <div class="profile-tip-btn">
                                                   <a href="javascript:void(0)" class="btn btn-primary btn-sm"><i class=”mdi mdi-eye”></i>View Profile</a>
                                                </div>
                                             </div>
                                          </span>
                                       </div>
                                    </div>
                                    <div class="desc-holder">
                                       <a href="javascript:void(0)">Adel Hasanat</a> tip for <a class="sub-link" href="javascript:void(0)"><?=$st_nm_S?></a>
                                       <span class="timestamp">August 31 at 08:45 pm<span class="glyphicon glyphicon-globe"></span></span>
                                    </div>
                                 </div>
                                 <div class="settings-icon">
                                    <div class="dropdown">
                                       <a class="dropdown-button" href="javascript:void(0)" data-activates="dropdown-edittip">
                                       <i class="zmdi zmdi-more zmdi-hc-2x"></i>
                                       </a>
                                       <ul class="dropdown-content cust_tip_drop" id="dropdown-edittip">
                                          <li>
                                             <a href="javascript:void(0)" class="edit_newtrip">Hide tip</a>
                                          </li>
                                          <li>
                                             <a href="javascript:void(0)" class="edit_newtrip">Save tip</a>
                                          </li>
                                          <li>
                                             <a href="javascript:void(0)" class="edit_newtrip">Mute notification for this post</a>
                                          </li>
                                          <li>
                                             <a href="javascript:void(0)" class="edit_newtrip">Delete tip</a>
                                          </li>
                                          <li>
                                             <a href="javascript:void(0)" class="edit_newtrip">Edit tip</a>
                                          </li>
                                       </ul>
                                    </div>
                                 </div>
                              </div>
                              <div class="post-content">
                                 <div class="post-details">
                                    <div class="post-title">Random Title</div>
                                    <div class="post-desc">
                                       <div class="para-section">
                                          <div class="para">
                                             <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sed varius risus. Duis rhoncus eros et pellentesque imperdiet.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sed varius risus. Duis rhoncus eros et pellentesque imperdiet.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sed varius risus. Duis rhoncus eros et pellentesque imperdiet.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sed varius risus. Duis rhoncus eros et pellentesque imperdiet.</p>
                                          </div>
                                          <a href="javascript:void(0)" onclick="showAllContent(this)">Read More</a>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="post-img-holder">
                                    <div class="lgt-gallery post-img two-img">
                                       <a href="images/post-img2.jpg" data-size="1600x1600" data-med="images/post-img2.jpg" data-med-size="1024x1024" data-author="Folkert Gorter" class="pimg-holder himg-box">
                                       <img class="himg" src="images/post-img2.jpg" alt="" />
                                       </a>
                                       <a href="images/post-img3.jpg" data-size="1600x1068" data-med="images/post-img3.jpg" data-med-size="1024x683" data-author="Samuel Rohl" class="pimg-holder vimg-box">
                                       <img class="himg" src="images/post-img3.jpg" alt="" />
                                       </a>
                                    </div>
                                 </div>
                              </div>
                              <div class="clear"></div>
                              <div class="post-data">
                                 <div class="post-actions">
                                    <div class="right like-tooltip">
                                       <a href="javascript:void(0)" class="pa-like" data-title="User Name"><i class="zmdi zmdi-thumb-up"></i></a>
                                       <span class="lcount">4</span>
                                       <a href="javascript:void(0)" class="pa-comment"><i class="zmdi zmdi-comment"></i></a>
                                       <span class="comment-lcount">4</span>
                                    </div>
                                 </div>
                                 <div class="comments-section panel">
                                    <div class="comments-area">
                                       <div class="post-more">
                                          <a href="javascript:void(0)" class="view-morec">View more comments</a>
                                          <span class="total-comments">3 of 7</span>
                                       </div>
                                       <div class="post-comments">
                                          <div class="pcomments">
                                             <div class="pcomment-earlier">
                                                <div class="pcomment-holder">
                                                   <div class="pcomment main-comment">
                                                      <div class="img-holder">
                                                         <div id="commentptip-1" class="profiletipholder">
                                                            <span class="profile-tooltip">
                                                            <img class="circle" src="images/demo-profile.jpg" />
                                                            </span>
                                                            <span class="profiletooltip_content">
                                                               <div class="profile-tip">
                                                                  <div class="profile-tip-cover"><img src="images/cover.jpg"></div>
                                                                  <div class="profile-tip-avatar">
                                                                     <a href="javascript:void(0)">
                                                                     <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">
                                                                     </a>
                                                                  </div>
                                                                  <div class="profile-tip-info">
                                                                     <div class="cover-username"><a href="javascript:void(0)">Adel Hasanat</a></div>
                                                                     <div class="cover-headline">
                                                                        <span class="ptip-icon"><i class="fa  fa-suitcase"></i></span>
                                                                        Web Designer, Cricketer
                                                                     </div>
                                                                     <div class="profiletip-bio">
                                                                        <span class="ptip-icon"><i class="mdi mdi-home"></i></span>
                                                                        Lives in : <span>Gariyadhar</span>
                                                                     </div>
                                                                     <div class="profiletip-bio">
                                                                        <span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span>
                                                                        Currently in : <span>Gariyadhar, Gujarat, India</span>
                                                                     </div>
                                                                  </div>
                                                                  <div class="profile-tip-divider"></div>
                                                                  <div class="profile-tip-btn">
                                                                     <a href="javascript:void(0)" class="btn btn-primary btn-sm"><i class=”mdi mdi-eye”></i>View Profile</a>
                                                                  </div>
                                                               </div>
                                                            </span>
                                                         </div>
                                                      </div>
                                                      <div class="desc-holder">
                                                         <div class="normal-mode">
                                                            <div class="desc">
                                                               <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                                               <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                                            </div>
                                                            <div class="comment-stuff">
                                                               <div class="more-opt">
                                                                  <a href="javascript:void(0)" class="pa-like"><span>icon</span></a>
                                                                  <a href="javascript:void(0)" class="pa-reply reply-comment"><span>icon</span></a>
                                                                  <div class="dropdown dropdown-custom dropdown-xxsmall">
                                                                     <a href="javascript:void(0)" class="dropdown-toggle dropdown-button" data-activates="dropdown-editdelete7">
                                                                     <i class="zmdi zmdi-hc-2x zmdi-more-vert"></i>
                                                                     </a>
                                                                     <ul class="dropdown-content" id="dropdown-editdelete7">
                                                                        <li><a href="javascript:void(0)" class="edit-comment">Edit</a></li>
                                                                        <li><a href="javascript:void(0)" class="delete-comment">Delete</a></li>
                                                                     </ul>
                                                                  </div>
                                                               </div>
                                                               <div class="less-opt">
                                                                  <div class="timestamp">8h</div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                         <div class="edit-mode">
                                                            <div class="desc">
                                                               <textarea class="editcomment-tt materialize-textarea mb0 md_textarea item_tagline">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                                               <a href="javascript:void(0)" class="btn btn-primary btn-sm editcomment-cancel waves-effect waves-light">Cancel</a>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div class="clear"></div>
                                                   <div class="comment-reply-holder comment-addreply">
                                                      <div class="addnew-comment valign-wrapper comment-reply">
                                                         <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg" /></a></div>
                                                         <div class="desc-holder">
                                                            <div class="sliding-middle-custom anim-area">
                                                               <textarea class="materialize-textarea mb0 md_textarea item_tagline">Write a reply...</textarea>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="pcomment-holder">
                                                   <div class="pcomment main-comment">
                                                      <div class="img-holder">
                                                         <div id="commentptip-2" class="profiletipholder">
                                                            <span class="profile-tooltip">
                                                            <img class="circle" src="images/demo-profile.jpg" />
                                                            </span>
                                                            <span class="profiletooltip_content">
                                                               <div class="profile-tip">
                                                                  <div class="profile-tip-cover"><img src="images/cover.jpg"></div>
                                                                  <div class="profile-tip-avatar">
                                                                     <a href="javascript:void(0)">
                                                                     <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">
                                                                     </a>
                                                                  </div>
                                                                  <div class="profile-tip-info">
                                                                     <div class="cover-username"><a href="javascript:void(0)">Adel Hasanat</a></div>
                                                                     <div class="cover-headline">
                                                                        <span class="ptip-icon"><i class="fa  fa-suitcase"></i></span>
                                                                        Web Designer, Cricketer
                                                                     </div>
                                                                     <div class="profiletip-bio">
                                                                        <span class="ptip-icon"><i class="mdi mdi-home"></i></span>
                                                                        Lives in : <span>Gariyadhar</span>
                                                                     </div>
                                                                     <div class="profiletip-bio">
                                                                        <span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span>
                                                                        Currently in : <span>Gariyadhar, Gujarat, India</span>
                                                                     </div>
                                                                  </div>
                                                                  <div class="profile-tip-divider"></div>
                                                                  <div class="profile-tip-btn">
                                                                     <a href="javascript:void(0)" class="btn btn-primary btn-sm"><i class=”mdi mdi-eye”></i>View Profile</a>
                                                                  </div>
                                                               </div>
                                                            </span>
                                                         </div>
                                                      </div>
                                                      <div class="desc-holder">
                                                         <div class="normal-mode">
                                                            <div class="desc">
                                                               <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                                               <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                                            </div>
                                                            <div class="comment-stuff">
                                                               <div class="more-opt">
                                                                  <a href="javascript:void(0)" class="pa-like"><span>icon</span></a>
                                                                  <a href="javascript:void(0)" class="pa-reply reply-comment"><span>icon</span></a>
                                                                  <div class="dropdown dropdown-custom dropdown-xxsmall">
                                                                     <a href="javascript:void(0)" class="dropdown-toggle dropdown-button" data-activates="dropdown-editdelete6">
                                                                     <i class="zmdi zmdi-hc-2x zmdi-more-vert"></i>
                                                                     </a>
                                                                     <ul class="dropdown-content" id="dropdown-editdelete6"">
                                                                        <li><a href="javascript:void(0)" class="edit-comment">Edit</a></li>
                                                                        <li><a href="javascript:void(0)" class="delete-comment">Delete</a></li>
                                                                     </ul>
                                                                  </div>
                                                               </div>
                                                               <div class="less-opt">
                                                                  <div class="timestamp">8h</div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                         <div class="edit-mode">
                                                            <div class="desc">
                                                               <textarea class="editcomment-tt materialize-textarea mb0 md_textarea item_tagline">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                                               <a href="javascript:void(0)" class="btn btn-primary btn-sm editcomment-cancel waves-effect waves-light">Cancel</a>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div class="clear"></div>
                                                   <div class="comment-reply-holder comment-addreply">
                                                      <div class="addnew-comment valign-wrapper comment-reply">
                                                         <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg" /></a></div>
                                                         <div class="desc-holder">
                                                            <div class="sliding-middle-custom anim-area">
                                                               <textarea class="materialize-textarea mb0 md_textarea item_tagline">Write a reply...</textarea>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="pcomment-holder has-comments">
                                                <div class="pcomment main-comment">
                                                   <div class="img-holder">
                                                      <div id="commentptip-3" class="profiletipholder">
                                                         <span class="profile-tooltip">
                                                         <img class="circle" src="images/demo-profile.jpg" />
                                                         </span>
                                                         <span class="profiletooltip_content">
                                                            <div class="profile-tip">
                                                               <div class="profile-tip-cover"><img src="images/cover.jpg"></div>
                                                               <div class="profile-tip-avatar">
                                                                  <a href="javascript:void(0)">
                                                                  <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">
                                                                  </a>
                                                               </div>
                                                               <div class="profile-tip-info">
                                                                  <div class="cover-username"><a href="javascript:void(0)">Adel Hasanat</a></div>
                                                                  <div class="cover-headline">
                                                                     <span class="ptip-icon"><i class="fa  fa-suitcase"></i></span>
                                                                     Web Designer, Cricketer
                                                                  </div>
                                                                  <div class="profiletip-bio">
                                                                     <span class="ptip-icon"><i class="mdi mdi-home"></i></span>
                                                                     Lives in : <span>Gariyadhar</span>
                                                                  </div>
                                                                  <div class="profiletip-bio">
                                                                     <span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span>
                                                                     Currently in : <span>Gariyadhar, Gujarat, India</span>
                                                                  </div>
                                                               </div>
                                                               <div class="profile-tip-divider"></div>
                                                               <div class="profile-tip-btn">
                                                                  <a href="javascript:void(0)" class="btn btn-primary btn-sm"><i class=”mdi mdi-eye”></i>View Profile</a>
                                                               </div>
                                                            </div>
                                                         </span>
                                                      </div>
                                                   </div>
                                                   <div class="desc-holder">
                                                      <div class="normal-mode">
                                                         <div class="desc">
                                                            <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                                         </div>
                                                         <div class="comment-stuff">
                                                            <div class="more-opt">
                                                               <a href="javascript:void(0)" class="pa-like"><span>icon</span></a>
                                                               <a href="javascript:void(0)" class="pa-reply reply-comment"><span>icon</span></a>
                                                               <div class="dropdown dropdown-custom dropdown-xxsmall">
                                                                  <a href="javascript:void(0)" class="dropdown-toggle dropdown-button" data-activates="dropdown-editdelete5">
                                                                  <i class="zmdi zmdi-hc-2x zmdi-more-vert"></i>
                                                                  </a>
                                                                  <ul class="dropdown-content" id="dropdown-editdelete5">
                                                                     <li><a href="javascript:void(0)" class="edit-comment">Edit</a></li>
                                                                     <li><a href="javascript:void(0)" class="delete-comment">Delete</a></li>
                                                                  </ul>
                                                               </div>
                                                            </div>
                                                            <div class="less-opt">
                                                               <div class="timestamp">8h</div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div class="edit-mode">
                                                         <div class="desc">
                                                            <textarea class="editcomment-tt">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                                            <a href="javascript:void(0)" class="btn btn-primary btn-sm editcomment-cancel waves-effect waves-light">Cancel</a>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="clear"></div>
                                                <div class="comment-reply-holder">
                                                   <div class="pcomment comment-reply">
                                                      <div class="img-holder">
                                                         <div id="commentptip-5" class="profiletipholder">
                                                            <span class="profile-tooltip">
                                                            <img class="circle" src="images/demo-profile.jpg" />
                                                            </span>
                                                            <span class="profiletooltip_content">
                                                               <div class="profile-tip">
                                                                  <div class="profile-tip-cover"><img src="images/cover.jpg"></div>
                                                                  <div class="profile-tip-avatar">
                                                                     <a href="javascript:void(0)">
                                                                     <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">
                                                                     </a>
                                                                  </div>
                                                                  <div class="profile-tip-info">
                                                                     <div class="cover-username"><a href="javascript:void(0)">Adel Hasanat</a></div>
                                                                     <div class="cover-headline">
                                                                        <span class="ptip-icon"><i class="fa  fa-suitcase"></i></span>
                                                                        Web Designer, Cricketer
                                                                     </div>
                                                                     <div class="profiletip-bio">
                                                                        <span class="ptip-icon"><i class="mdi mdi-home"></i></span>
                                                                        Lives in : <span>Gariyadhar</span>
                                                                     </div>
                                                                     <div class="profiletip-bio">
                                                                        <span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span>
                                                                        Currently in : <span>Gariyadhar, Gujarat, India</span>
                                                                     </div>
                                                                  </div>
                                                                  <div class="profile-tip-divider"></div>
                                                                  <div class="profile-tip-btn">
                                                                     <a href="javascript:void(0)" class="btn btn-primary btn-sm"><i class=”mdi mdi-eye”></i>View Profile</a>
                                                                  </div>
                                                               </div>
                                                            </span>
                                                         </div>
                                                      </div>
                                                      <div class="desc-holder">
                                                         <div class="normal-mode">
                                                            <div class="desc">
                                                               <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                                               <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                                            </div>
                                                            <div class="comment-stuff">
                                                               <div class="more-opt">
                                                                  <a href="javascript:void(0)" class="pa-like"><span>icon</span></a>
                                                                  <a href="javascript:void(0)" class="pa-reply reply-comment"><span>icon</span></a>
                                                                  <div class="dropdown dropdown-custom dropdown-xxsmall">
                                                                     <a href="javascript:void(0)" class="dropdown-toggle dropdown-button" data-activates="dropdown-editdelete4">
                                                                     <i class="zmdi zmdi-hc-2x zmdi-more-vert"></i>
                                                                     </a>
                                                                     <ul class="dropdown-content" id="dropdown-editdelete4">
                                                                        <li><a href="javascript:void(0)" class="edit-comment">Edit</a></li>
                                                                        <li><a href="javascript:void(0)" class="delete-comment">Delete</a></li>
                                                                     </ul>
                                                                  </div>
                                                               </div>
                                                               <div class="less-opt">
                                                                  <div class="timestamp">8h</div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                         <div class="edit-mode">
                                                            <div class="desc">
                                                               <textarea class="editcomment-tt">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                                               <a href="javascript:void(0)" class="btn btn-primary btn-sm editcomment-cancel waves-effect waves-light">Cancel</a>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div class="pcomment comment-reply">
                                                      <div class="img-holder">
                                                         <div id="commentptip-6" class="profiletipholder">
                                                            <span class="profile-tooltip">
                                                            <img class="circle" src="images/demo-profile.jpg" />
                                                            </span>
                                                            <span class="profiletooltip_content">
                                                               <div class="profile-tip">
                                                                  <div class="profile-tip-cover"><img src="images/cover.jpg"></div>
                                                                  <div class="profile-tip-avatar">
                                                                     <a href="javascript:void(0)">
                                                                     <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">
                                                                     </a>
                                                                  </div>
                                                                  <div class="profile-tip-info">
                                                                     <div class="cover-username"><a href="javascript:void(0)">Adel Hasanat</a></div>
                                                                     <div class="cover-headline">
                                                                        <span class="ptip-icon"><i class="fa  fa-suitcase"></i></span>
                                                                        Web Designer, Cricketer
                                                                     </div>
                                                                     <div class="profiletip-bio">
                                                                        <span class="ptip-icon"><i class="mdi mdi-home"></i></span>
                                                                        Lives in : <span>Gariyadhar</span>
                                                                     </div>
                                                                     <div class="profiletip-bio">
                                                                        <span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span>
                                                                        Currently in : <span>Gariyadhar, Gujarat, India</span>
                                                                     </div>
                                                                  </div>
                                                                  <div class="profile-tip-divider"></div>
                                                                  <div class="profile-tip-btn">
                                                                     <a href="javascript:void(0)" class="btn btn-primary btn-sm"><i class=”mdi mdi-eye”></i>View Profile</a>
                                                                  </div>
                                                               </div>
                                                            </span>
                                                         </div>
                                                      </div>
                                                      <div class="desc-holder">
                                                         <div class="normal-mode">
                                                            <div class="desc">
                                                               <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                                               <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit...</p>
                                                            </div>
                                                            <div class="comment-stuff">
                                                               <div class="more-opt">
                                                                  <a href="javascript:void(0)" class="pa-like"><span>icon</span></a>
                                                                  <a href="javascript:void(0)" class="pa-reply reply-comment"><span>icon</span></a>
                                                                  <div class="dropdown dropdown-custom dropdown-xxsmall">
                                                                     <a href="javascript:void(0)" class="dropdown-toggle dropdown-button" data-activates="dropdown-editdelete3">
                                                                     <i class="zmdi zmdi-hc-2x zmdi-more-vert"></i>
                                                                     </a>
                                                                     <ul class="dropdown-content" id="dropdown-editdelete3">
                                                                        <li><a href="javascript:void(0)" class="edit-comment">Edit</a></li>
                                                                        <li><a href="javascript:void(0)" class="delete-comment">Delete</a></li>
                                                                     </ul>
                                                                  </div>
                                                               </div>
                                                               <div class="less-opt">
                                                                  <div class="timestamp">8h</div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                         <div class="edit-mode">
                                                            <div class="desc">
                                                               <textarea class="editcomment-tt">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                                               <a href="javascript:void(0)" class="btn btn-primary btn-sm editcomment-cancel waves-effect waves-light">Cancel</a>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="comment-reply-holder comment-addreply">
                                                   <div class="addnew-comment valign-wrapper comment-reply">
                                                      <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg" /></a></div>
                                                      <div class="desc-holder">
                                                         <div class="sliding-middle-custom anim-area">
                                                            <textarea class="materialize-textarea mb0 md_textarea item_tagline">Write a reply...</textarea>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="pcomment-holder">
                                                <div class="pcomment main-comment">
                                                   <div class="img-holder">
                                                      <div id="commentptip-4" class="profiletipholder">
                                                         <span class="profile-tooltip">
                                                         <img class="circle" src="images/demo-profile.jpg" />
                                                         </span>
                                                         <span class="profiletooltip_content">
                                                            <div class="profile-tip">
                                                               <div class="profile-tip-cover"><img src="images/cover.jpg"></div>
                                                               <div class="profile-tip-avatar">
                                                                  <a href="javascript:void(0)">
                                                                  <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">
                                                                  </a>
                                                               </div>
                                                               <div class="profile-tip-info">
                                                                  <div class="cover-username"><a href="javascript:void(0)">Adel Hasanat</a></div>
                                                                  <div class="cover-headline">
                                                                     <span class="ptip-icon"><i class="fa  fa-suitcase"></i></span>
                                                                     Web Designer, Cricketer
                                                                  </div>
                                                                  <div class="profiletip-bio">
                                                                     <span class="ptip-icon"><i class="mdi mdi-home"></i></span>
                                                                     Lives in : <span>Gariyadhar</span>
                                                                  </div>
                                                                  <div class="profiletip-bio">
                                                                     <span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span>
                                                                     Currently in : <span>Gariyadhar, Gujarat, India</span>
                                                                  </div>
                                                               </div>
                                                               <div class="profile-tip-divider"></div>
                                                               <div class="profile-tip-btn">
                                                                  <a href="javascript:void(0)" class="btn btn-primary btn-sm"><i class=”mdi mdi-eye”></i>View Profile</a>
                                                               </div>
                                                            </div>
                                                         </span>
                                                      </div>
                                                   </div>
                                                   <div class="desc-holder">
                                                      <div class="normal-mode">
                                                         <div class="desc">
                                                            <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                                         </div>
                                                         <div class="comment-stuff">
                                                            <div class="more-opt">
                                                               <a href="javascript:void(0)" class="pa-like"><span>icon</span></a>
                                                               <a href="javascript:void(0)" class="pa-reply reply-comment"><span>icon</span></a>
                                                               <div class="dropdown dropdown-custom dropdown-xxsmall">
                                                                  <a href="javascript:void(0)" class="dropdown-toggle dropdown-button" data-activates="dropdown-editdelete2">
                                                                  <i class="zmdi zmdi-hc-2x zmdi-more-vert"></i>
                                                                  </a>
                                                                  <ul class="dropdown-content" id="dropdown-editdelete2">
                                                                     <li><a href="javascript:void(0)" class="edit-comment">Edit</a></li>
                                                                     <li><a href="javascript:void(0)" class="delete-comment">Delete</a></li>
                                                                  </ul>
                                                               </div>
                                                            </div>
                                                            <div class="less-opt">
                                                               <div class="timestamp">8h</div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div class="edit-mode">
                                                         <div class="desc">
                                                            <textarea class="editcomment-tt">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                                            <a href="javascript:void(0)" class="btn btn-primary btn-sm editcomment-cancel waves-effect waves-light">Cancel</a>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="clear"></div>
                                                <div class="comment-reply-holder comment-addreply">
                                                   <div class="addnew-comment valign-wrapper comment-reply">
                                                      <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg" /></a></div>
                                                      <div class="desc-holder">
                                                         <div class="sliding-middle-custom anim-area">
                                                            <textarea class="materialize-textarea mb0 md_textarea item_tagline">Write a reply...</textarea>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="addnew-comment valign-wrapper">
                                             <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg" /></a></div>
                                             <div class="desc-holder">
                                                <div class="sliding-middle-out anim-area">
                                                   <textarea>Write a comment</textarea>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="post-holder bborder tippost-holder">
                              <div class="post-topbar">
                                 <div class="post-userinfo">
                                    <div class="img-holder">
                                       <div id="profiletip-1" class="profiletipholder">
                                          <span class="profile-tooltip">
                                          <img class="circle" src="images/demo-profile.jpg" />
                                          </span>
                                          <span class="profiletooltip_content">
                                             <div class="profile-tip">
                                                <div class="profile-tip-cover"><img src="images/cover.jpg"></div>
                                                <div class="profile-tip-avatar">
                                                   <a href="javascript:void(0)">
                                                   <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">
                                                   </a>
                                                </div>
                                                <div class="profile-tip-info">
                                                   <div class="cover-username"><a href="javascript:void(0)">Adel Hasanat</a></div>
                                                   <div class="cover-headline">
                                                      <span class="ptip-icon"><i class="fa  fa-suitcase"></i></span>
                                                      Web Designer, Cricketer
                                                   </div>
                                                   <div class="profiletip-bio">
                                                      <span class="ptip-icon"><i class="mdi mdi-home"></i></span>
                                                      Lives in : <span>Gariyadhar</span>
                                                   </div>
                                                   <div class="profiletip-bio">
                                                      <span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span>
                                                      Currently in : <span>Gariyadhar, Gujarat, India</span>
                                                   </div>
                                                </div>
                                                <div class="profile-tip-divider"></div>
                                                <div class="profile-tip-btn">
                                                   <a href="javascript:void(0)" class="btn btn-primary btn-sm"><i class=”mdi mdi-eye”></i>View Profile</a>
                                                </div>
                                             </div>
                                          </span>
                                       </div>
                                    </div>
                                    <div class="desc-holder">
                                       <a href="javascript:void(0)">Nimish Parekh</a> tip for <a class="sub-link" href="javascript:void(0)"><?=$st_nm_S?></a>
                                       <span class="timestamp">August 31 at 08:45 pm<span class="glyphicon glyphicon-globe"></span></span>
                                    </div>
                                 </div>
                                 <div class="settings-icon">
                                    <div class="dropdown">
                                       <a class="dropdown-button" href="javascript:void(0)" data-activates="dropdown-edittip2">
                                       <i class="zmdi zmdi-more zmdi-hc-2x"></i>
                                       </a>
                                       <ul class="dropdown-content" id="dropdown-edittip2">
                                          <li>
                                             <a href="javascript:void(0)" class="edit_newtrip">Edit tip</a>
                                          </li>
                                       </ul>
                                    </div>
                                 </div>
                              </div>
                              <div class="post-content">
                                 <div class="post-details">
                                    <div class="post-desc">
                                       <div class="para-section">
                                          <div class="para">
                                             <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sed varius risus. Duis rhoncus eros et pellentesque imperdiet.<br />Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sed varius risus. Duis rhoncus eros et pellentesque imperdiet.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sed varius risus. Duis rhoncus eros et pellentesque imperdiet.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sed varius risus. Duis rhoncus eros et pellentesque imperdiet.</p>
                                          </div>
                                          <a href="javascript:void(0)" onclick="showAllContent(this)">Read More</a>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="clear"></div>
                              <div class="post-data">
                                 <div class="post-actions">
                                    <div class="right like-tooltip">
                                       <a href="javascript:void(0)" class="pa-like" data-title="User Name"><i class="zmdi zmdi-thumb-up"></i></a>
                                       <span class="lcount">4</span>
                                       <a href="javascript:void(0)" class="pa-comment"><i class="zmdi zmdi-comment"></i></a>
                                       <span class="comment-lcount">4</span>
                                    </div>
                                 </div>
                                 <div class="comments-section panel">
                                    <div class="comments-area">
                                       <div class="post-more">
                                          <a href="javascript:void(0)" class="view-morec">View more comments</a>
                                          <span class="total-comments">3 of 7</span>
                                       </div>
                                       <div class="post-comments">
                                          <div class="pcomments">
                                             <div class="pcomment-earlier">
                                                <div class="pcomment-holder">
                                                   <div class="pcomment main-comment">
                                                      <div class="img-holder">
                                                         <div id="commentptip-1" class="profiletipholder">
                                                            <span class="profile-tooltip">
                                                            <img class="circle" src="images/demo-profile.jpg" />
                                                            </span>
                                                            <span class="profiletooltip_content">
                                                               <div class="profile-tip">
                                                                  <div class="profile-tip-cover"><img src="images/cover.jpg"></div>
                                                                  <div class="profile-tip-avatar">
                                                                     <a href="javascript:void(0)">
                                                                     <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">
                                                                     </a>
                                                                  </div>
                                                                  <div class="profile-tip-info">
                                                                     <div class="cover-username"><a href="javascript:void(0)">Adel Hasanat</a></div>
                                                                     <div class="cover-headline">
                                                                        <span class="ptip-icon"><i class="fa  fa-suitcase"></i></span>
                                                                        Web Designer, Cricketer
                                                                     </div>
                                                                     <div class="profiletip-bio">
                                                                        <span class="ptip-icon"><i class="mdi mdi-home"></i></span>
                                                                        Lives in : <span>Gariyadhar</span>
                                                                     </div>
                                                                     <div class="profiletip-bio">
                                                                        <span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span>
                                                                        Currently in : <span>Gariyadhar, Gujarat, India</span>
                                                                     </div>
                                                                  </div>
                                                                  <div class="profile-tip-divider"></div>
                                                                  <div class="profile-tip-btn">
                                                                     <a href="javascript:void(0)" class="btn btn-primary btn-sm"><i class=”mdi mdi-eye”></i>View Profile</a>
                                                                  </div>
                                                               </div>
                                                            </span>
                                                         </div>
                                                      </div>
                                                      <div class="desc-holder">
                                                         <div class="normal-mode">
                                                            <div class="desc">
                                                               <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                                               <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                                            </div>
                                                            <div class="comment-stuff">
                                                               <div class="more-opt">
                                                                  <a href="javascript:void(0)" class="pa-like"><span>icon</span></a>
                                                                  <a href="javascript:void(0)" class="pa-reply reply-comment"><span>icon</span></a>
                                                                  <div class="dropdown dropdown-custom dropdown-xxsmall">
                                                                     <a href="javascript:void(0)" class="dropdown-toggle dropdown-button" data-activates="dropdown-editdelete13">
                                                                     <i class="zmdi zmdi-hc-2x zmdi-more-vert"></i>
                                                                     </a>
                                                                     <ul class="dropdown-content" id="dropdown-editdelete13">
                                                                        <li><a href="javascript:void(0)" class="edit-comment">Edit</a></li>
                                                                        <li><a href="javascript:void(0)" class="delete-comment">Delete</a></li>
                                                                     </ul>
                                                                  </div>
                                                               </div>
                                                               <div class="less-opt">
                                                                  <div class="timestamp">8h</div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                         <div class="edit-mode">
                                                            <div class="desc">
                                                               <textarea class="editcomment-tt">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                                               <a href="javascript:void(0)" class="btn btn-primary btn-sm editcomment-cancel waves-effect waves-light">Cancel</a>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div class="clear"></div>
                                                   <div class="comment-reply-holder comment-addreply">
                                                      <div class="addnew-comment valign-wrapper comment-reply">
                                                         <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg" /></a></div>
                                                         <div class="desc-holder">
                                                            <div class="sliding-middle-custom anim-area">
                                                               <textarea class="materialize-textarea mb0 md_textarea item_tagline">Write a reply...</textarea>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="pcomment-holder">
                                                   <div class="pcomment main-comment">
                                                      <div class="img-holder">
                                                         <div id="commentptip-2" class="profiletipholder">
                                                            <span class="profile-tooltip">
                                                            <img class="circle" src="images/demo-profile.jpg" />
                                                            </span>
                                                            <span class="profiletooltip_content">
                                                               <div class="profile-tip">
                                                                  <div class="profile-tip-cover"><img src="images/cover.jpg"></div>
                                                                  <div class="profile-tip-avatar">
                                                                     <a href="javascript:void(0)">
                                                                     <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">
                                                                     </a>
                                                                  </div>
                                                                  <div class="profile-tip-info">
                                                                     <div class="cover-username"><a href="javascript:void(0)">Adel Hasanat</a></div>
                                                                     <div class="cover-headline">
                                                                        <span class="ptip-icon"><i class="fa  fa-suitcase"></i></span>
                                                                        Web Designer, Cricketer
                                                                     </div>
                                                                     <div class="profiletip-bio">
                                                                        <span class="ptip-icon"><i class="mdi mdi-home"></i></span>
                                                                        Lives in : <span>Gariyadhar</span>
                                                                     </div>
                                                                     <div class="profiletip-bio">
                                                                        <span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span>
                                                                        Currently in : <span>Gariyadhar, Gujarat, India</span>
                                                                     </div>
                                                                  </div>
                                                                  <div class="profile-tip-divider"></div>
                                                                  <div class="profile-tip-btn">
                                                                     <a href="javascript:void(0)" class="btn btn-primary btn-sm"><i class=”mdi mdi-eye”></i>View Profile</a>
                                                                  </div>
                                                               </div>
                                                            </span>
                                                         </div>
                                                      </div>
                                                      <div class="desc-holder">
                                                         <div class="normal-mode">
                                                            <div class="desc">
                                                               <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                                               <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                                            </div>
                                                            <div class="comment-stuff">
                                                               <div class="more-opt">
                                                                  <a href="javascript:void(0)" class="pa-like"><span>icon</span></a>
                                                                  <a href="javascript:void(0)" class="pa-reply reply-comment"><span>icon</span></a>
                                                                  <div class="dropdown dropdown-custom dropdown-xxsmall">
                                                                     <a href="javascript:void(0)" class="dropdown-toggle dropdown-button" data-activates="dropdown-editdelete12">
                                                                     <i class="zmdi zmdi-hc-2x zmdi-more-vert"></i>
                                                                     </a>
                                                                     <ul class="dropdown-content" id="dropdown-editdelete12">
                                                                        <li><a href="javascript:void(0)" class="edit-comment">Edit</a></li>
                                                                        <li><a href="javascript:void(0)" class="delete-comment">Delete</a></li>
                                                                     </ul>
                                                                  </div>
                                                               </div>
                                                               <div class="less-opt">
                                                                  <div class="timestamp">8h</div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                         <div class="edit-mode">
                                                            <div class="desc">
                                                               <textarea class="editcomment-tt">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                                               <a href="javascript:void(0)" class="btn btn-primary btn-sm editcomment-cancel waves-effect waves-light">Cancel</a>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div class="clear"></div>
                                                   <div class="comment-reply-holder comment-addreply">
                                                      <div class="addnew-comment valign-wrapper comment-reply">
                                                         <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg" /></a></div>
                                                         <div class="desc-holder">
                                                            <div class="sliding-middle-custom anim-area">
                                                               <textarea class="materialize-textarea mb0 md_textarea item_tagline">Write a reply...</textarea>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="pcomment-holder has-comments">
                                                <div class="pcomment main-comment">
                                                   <div class="img-holder">
                                                      <div id="commentptip-3" class="profiletipholder">
                                                         <span class="profile-tooltip">
                                                         <img class="circle" src="images/demo-profile.jpg" />
                                                         </span>
                                                         <span class="profiletooltip_content">
                                                            <div class="profile-tip">
                                                               <div class="profile-tip-cover"><img src="images/cover.jpg"></div>
                                                               <div class="profile-tip-avatar">
                                                                  <a href="javascript:void(0)">
                                                                  <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">
                                                                  </a>
                                                               </div>
                                                               <div class="profile-tip-info">
                                                                  <div class="cover-username"><a href="javascript:void(0)">Adel Hasanat</a></div>
                                                                  <div class="cover-headline">
                                                                     <span class="ptip-icon"><i class="fa  fa-suitcase"></i></span>
                                                                     Web Designer, Cricketer
                                                                  </div>
                                                                  <div class="profiletip-bio">
                                                                     <span class="ptip-icon"><i class="mdi mdi-home"></i></span>
                                                                     Lives in : <span>Gariyadhar</span>
                                                                  </div>
                                                                  <div class="profiletip-bio">
                                                                     <span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span>
                                                                     Currently in : <span>Gariyadhar, Gujarat, India</span>
                                                                  </div>
                                                               </div>
                                                               <div class="profile-tip-divider"></div>
                                                               <div class="profile-tip-btn">
                                                                  <a href="javascript:void(0)" class="btn btn-primary btn-sm"><i class=”mdi mdi-eye”></i>View Profile</a>
                                                               </div>
                                                            </div>
                                                         </span>
                                                      </div>
                                                   </div>
                                                   <div class="desc-holder">
                                                      <div class="normal-mode">
                                                         <div class="desc">
                                                            <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                                         </div>
                                                         <div class="comment-stuff">
                                                            <div class="more-opt">
                                                               <a href="javascript:void(0)" class="pa-like"><span>icon</span></a>
                                                               <a href="javascript:void(0)" class="pa-reply reply-comment"><span>icon</span></a>
                                                               <div class="dropdown dropdown-custom dropdown-xxsmall">
                                                                  <a href="javascript:void(0)" class="dropdown-toggle dropdown-button" data-activates="dropdown-editdelete11">
                                                                  <i class="zmdi zmdi-hc-2x zmdi-more-vert"></i>
                                                                  </a>
                                                                  <ul class="dropdown-content" id="dropdown-editdelete11">
                                                                     <li><a href="javascript:void(0)" class="edit-comment">Edit</a></li>
                                                                     <li><a href="javascript:void(0)" class="delete-comment">Delete</a></li>
                                                                  </ul>
                                                               </div>
                                                            </div>
                                                            <div class="less-opt">
                                                               <div class="timestamp">8h</div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div class="edit-mode">
                                                         <div class="desc">
                                                            <textarea class="editcomment-tt">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                                            <a href="javascript:void(0)" class="btn btn-primary btn-sm editcomment-cancel waves-effect waves-light">Cancel</a>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="clear"></div>
                                                <div class="comment-reply-holder">
                                                   <div class="pcomment comment-reply">
                                                      <div class="img-holder">
                                                         <div id="commentptip-5" class="profiletipholder">
                                                            <span class="profile-tooltip">
                                                            <img class="circle" src="images/demo-profile.jpg" />
                                                            </span>
                                                            <span class="profiletooltip_content">
                                                               <div class="profile-tip">
                                                                  <div class="profile-tip-cover"><img src="images/cover.jpg"></div>
                                                                  <div class="profile-tip-avatar">
                                                                     <a href="javascript:void(0)">
                                                                     <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">
                                                                     </a>
                                                                  </div>
                                                                  <div class="profile-tip-info">
                                                                     <div class="cover-username"><a href="javascript:void(0)">Adel Hasanat</a></div>
                                                                     <div class="cover-headline">
                                                                        <span class="ptip-icon"><i class="fa  fa-suitcase"></i></span>
                                                                        Web Designer, Cricketer
                                                                     </div>
                                                                     <div class="profiletip-bio">
                                                                        <span class="ptip-icon"><i class="mdi mdi-home"></i></span>
                                                                        Lives in : <span>Gariyadhar</span>
                                                                     </div>
                                                                     <div class="profiletip-bio">
                                                                        <span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span>
                                                                        Currently in : <span>Gariyadhar, Gujarat, India</span>
                                                                     </div>
                                                                  </div>
                                                                  <div class="profile-tip-divider"></div>
                                                                  <div class="profile-tip-btn">
                                                                     <a href="javascript:void(0)" class="btn btn-primary btn-sm"><i class=”mdi mdi-eye”></i>View Profile</a>
                                                                  </div>
                                                               </div>
                                                            </span>
                                                         </div>
                                                      </div>
                                                      <div class="desc-holder">
                                                         <div class="normal-mode">
                                                            <div class="desc">
                                                               <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                                               <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                                            </div>
                                                            <div class="comment-stuff">
                                                               <div class="more-opt">
                                                                  <a href="javascript:void(0)" class="pa-like"><span>icon</span></a>
                                                                  <a href="javascript:void(0)" class="pa-reply reply-comment"><span>icon</span></a>
                                                                  <div class="dropdown dropdown-custom dropdown-xxsmall">
                                                                     <a href="javascript:void(0)" class="dropdown-toggle dropdown-button" data-activates="dropdown-editdelete10">
                                                                     <i class="zmdi zmdi-hc-2x zmdi-more-vert"></i>
                                                                     </a>
                                                                     <ul class="dropdown-content" id="dropdown-editdelete10">
                                                                        <li><a href="javascript:void(0)" class="edit-comment">Edit</a></li>
                                                                        <li><a href="javascript:void(0)" class="delete-comment">Delete</a></li>
                                                                     </ul>
                                                                  </div>
                                                               </div>
                                                               <div class="less-opt">
                                                                  <div class="timestamp">8h</div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                         <div class="edit-mode">
                                                            <div class="desc">
                                                               <textarea class="editcomment-tt">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                                               <a href="javascript:void(0)" class="btn btn-primary btn-sm editcomment-cancel waves-effect waves-light">Cancel</a>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div class="pcomment comment-reply">
                                                      <div class="img-holder">
                                                         <div id="commentptip-6" class="profiletipholder">
                                                            <span class="profile-tooltip">
                                                            <img class="circle" src="images/demo-profile.jpg" />
                                                            </span>
                                                            <span class="profiletooltip_content">
                                                               <div class="profile-tip">
                                                                  <div class="profile-tip-cover"><img src="images/cover.jpg"></div>
                                                                  <div class="profile-tip-avatar">
                                                                     <a href="javascript:void(0)">
                                                                     <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">
                                                                     </a>
                                                                  </div>
                                                                  <div class="profile-tip-info">
                                                                     <div class="cover-username"><a href="javascript:void(0)">Adel Hasanat</a></div>
                                                                     <div class="cover-headline">
                                                                        <span class="ptip-icon"><i class="fa  fa-suitcase"></i></span>
                                                                        Web Designer, Cricketer
                                                                     </div>
                                                                     <div class="profiletip-bio">
                                                                        <span class="ptip-icon"><i class="mdi mdi-home"></i></span>
                                                                        Lives in : <span>Gariyadhar</span>
                                                                     </div>
                                                                     <div class="profiletip-bio">
                                                                        <span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span>
                                                                        Currently in : <span>Gariyadhar, Gujarat, India</span>
                                                                     </div>
                                                                  </div>
                                                                  <div class="profile-tip-divider"></div>
                                                                  <div class="profile-tip-btn">
                                                                     <a href="javascript:void(0)" class="btn btn-primary btn-sm"><i class=”mdi mdi-eye”></i>View Profile</a>
                                                                  </div>
                                                               </div>
                                                            </span>
                                                         </div>
                                                      </div>
                                                      <div class="desc-holder">
                                                         <div class="normal-mode">
                                                            <div class="desc">
                                                               <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                                               <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit...</p>
                                                            </div>
                                                            <div class="comment-stuff">
                                                               <div class="more-opt">
                                                                  <a href="javascript:void(0)" class="pa-like"><span>icon</span></a>
                                                                  <a href="javascript:void(0)" class="pa-reply reply-comment"><span>icon</span></a>
                                                                  <div class="dropdown dropdown-custom dropdown-xxsmall">
                                                                     <a href="javascript:void(0)" class="dropdown-toggle dropdown-button" data-activates="dropdown-editdelete9">
                                                                     <i class="zmdi zmdi-hc-2x zmdi-more-vert"></i>
                                                                     </a>
                                                                     <ul class="dropdown-content" id="dropdown-editdelete9">
                                                                        <li><a href="javascript:void(0)" class="edit-comment">Edit</a></li>
                                                                        <li><a href="javascript:void(0)" class="delete-comment">Delete</a></li>
                                                                     </ul>
                                                                  </div>
                                                               </div>
                                                               <div class="less-opt">
                                                                  <div class="timestamp">8h</div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                         <div class="edit-mode">
                                                            <div class="desc">
                                                               <textarea class="editcomment-tt">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                                               <a href="javascript:void(0)" class="btn btn-primary btn-sm editcomment-cancel waves-effect waves-light">Cancel</a>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="comment-reply-holder comment-addreply">
                                                   <div class="addnew-comment valign-wrapper comment-reply">
                                                      <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg" /></a></div>
                                                      <div class="desc-holder">
                                                         <div class="sliding-middle-custom anim-area">
                                                            <textarea class="materialize-textarea mb0 md_textarea item_tagline">Write a reply...</textarea>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="pcomment-holder">
                                                <div class="pcomment main-comment">
                                                   <div class="img-holder">
                                                      <div id="commentptip-4" class="profiletipholder">
                                                         <span class="profile-tooltip">
                                                         <img class="circle" src="images/demo-profile.jpg" />
                                                         </span>
                                                         <span class="profiletooltip_content">
                                                            <div class="profile-tip">
                                                               <div class="profile-tip-cover"><img src="images/cover.jpg"></div>
                                                               <div class="profile-tip-avatar">
                                                                  <a href="javascript:void(0)">
                                                                  <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">
                                                                  </a>
                                                               </div>
                                                               <div class="profile-tip-info">
                                                                  <div class="cover-username"><a href="javascript:void(0)">Adel Hasanat</a></div>
                                                                  <div class="cover-headline">
                                                                     <span class="ptip-icon"><i class="fa  fa-suitcase"></i></span>
                                                                     Web Designer, Cricketer
                                                                  </div>
                                                                  <div class="profiletip-bio">
                                                                     <span class="ptip-icon"><i class="mdi mdi-home"></i></span>
                                                                     Lives in : <span>Gariyadhar</span>
                                                                  </div>
                                                                  <div class="profiletip-bio">
                                                                     <span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span>
                                                                     Currently in : <span>Gariyadhar, Gujarat, India</span>
                                                                  </div>
                                                               </div>
                                                               <div class="profile-tip-divider"></div>
                                                               <div class="profile-tip-btn">
                                                                  <a href="javascript:void(0)" class="btn btn-primary btn-sm"><i class=”mdi mdi-eye”></i>View Profile</a>
                                                               </div>
                                                            </div>
                                                         </span>
                                                      </div>
                                                   </div>
                                                   <div class="desc-holder">
                                                      <div class="normal-mode">
                                                         <div class="desc">
                                                            <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                                         </div>
                                                         <div class="comment-stuff">
                                                            <div class="more-opt">
                                                               <a href="javascript:void(0)" class="pa-like"><span>icon</span></a>
                                                               <a href="javascript:void(0)" class="pa-reply reply-comment"><span>icon</span></a>
                                                               <div class="dropdown dropdown-custom dropdown-xxsmall">
                                                                  <a href="javascript:void(0)" class="dropdown-toggle dropdown-button" data-activates="dropdown-editdelete8">
                                                                  <i class="zmdi zmdi-hc-2x zmdi-more-vert"></i>
                                                                  </a>
                                                                  <ul class="dropdown-content" id="dropdown-editdelete8">
                                                                     <li><a href="javascript:void(0)" class="edit-comment">Edit</a></li>
                                                                     <li><a href="javascript:void(0)" class="delete-comment">Delete</a></li>
                                                                  </ul>
                                                               </div>
                                                            </div>
                                                            <div class="less-opt">
                                                               <div class="timestamp">8h</div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div class="edit-mode">
                                                         <div class="desc">
                                                            <textarea class="editcomment-tt">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                                            <a href="javascript:void(0)" class="btn btn-primary btn-sm editcomment-cancel waves-effect waves-light">Cancel</a>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="clear"></div>
                                                <div class="comment-reply-holder comment-addreply">
                                                   <div class="addnew-comment valign-wrapper comment-reply">
                                                      <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg" /></a></div>
                                                      <div class="desc-holder">
                                                         <div class="sliding-middle-out anim-area">
                                                            <textarea class="materialize-textarea mb0 md_textarea item_tagline">Write a reply...</textarea>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="addnew-comment valign-wrapper">
                                             <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg" /></a></div>
                                             <div class="desc-holder">
                                                <div class="sliding-middle-out anim-area">
                                                   <textarea>Write a comment</textarea>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="new-post-mobile clear">
                        <a href="javascript:void(0)" class="popup-window compose_newtrip" ><i class="mdi mdi-pencil"></i></a>
                     </div>
                  </div>
               </div>
            </div>
            <div id="places-ask" class="placesask-content subtab bottom_tabs">
               <div class="row cshfsiput cshfsi">
                  <div class="social-section profile-box detailBox">
                    <div class="item titlelabel user-info">
                      <p class="center-align u-name mb0">Discussion for Amman (<span class="count">10</span>)</p>
                      <p class="user-desg center-align"></p>
                    </div>
                    <div class="item profile-info row mx-0 width-100">
                      <div class="sub-item">
                        <h5 class="">Photo</h5>
                        <p class="">10</p>
                      </div>
                      <div class="sub-item">
                        <h5 class="">Post</h5>
                        <p class="">15</p>
                      </div>
                      <div class="sub-item">
                        <h5 class="">Connections</h5>
                        <p class="">20</p>
                      </div>
                      <div class="sub-item">
                        <h5 class="">Profile views</h5>
                        <p class="">05</p>
                      </div>
                    </div>
                  </div>
                  <div class="col s12 m9 l9 xl9 postBox">
                     <div class="content-box ">
                        <div class="new-post base-newpost compose_newask cshfsiput cshfsi">
                           <div class="npost-content">
                              <div class="post-mcontent">
                                 <i class="mdi mdi-pencil-box-outline main-icon"></i>
                                 <div class="desc">
                                    <div class="input-field comments_box">
                                       <p>Ask a question</p>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>

                        <div class="cbox-desc nm-postlist post-list cshfsiput cshfsi">
                           <div class="post-holder bborder reviewpost-holder">
                              <div class="post-topbar">
                                 <div class="post-userinfo">
                                    <div class="img-holder">
                                       <div id="profiletip-1" class="profiletipholder">
                                          <span class="profile-tooltip">
                                          <img class="circle" src="images/demo-profile.jpg" />
                                          </span>
                                          <span class="profiletooltip_content">
                                             <div class="profile-tip">
                                                <div class="profile-tip-cover"><img src="images/cover.jpg"></div>
                                                <div class="profile-tip-avatar">
                                                   <a href="javascript:void(0)">
                                                   <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">
                                                   </a>
                                                </div>
                                                <div class="profile-tip-info">
                                                   <div class="cover-username"><a href="javascript:void(0)">Adel Hasanat</a></div>
                                                   <div class="cover-headline">
                                                      <span class="ptip-icon"><i class="fa  fa-suitcase"></i></span>
                                                      Web Designer, Cricketer
                                                   </div>
                                                   <div class="profiletip-bio">
                                                      <span class="ptip-icon"><i class="mdi mdi-home"></i></span>
                                                      Lives in : <span>Gariyadhar</span>
                                                   </div>
                                                   <div class="profiletip-bio">
                                                      <span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span>
                                                      Currently in : <span>Gariyadhar, Gujarat, India</span>
                                                   </div>
                                                </div>
                                                <div class="profile-tip-divider"></div>
                                                <div class="profile-tip-btn">
                                                   <a href="javascript:void(0)" class="btn btn-primary btn-sm"><i class=”mdi mdi-eye”></i>View Profile</a>
                                                </div>
                                             </div>
                                          </span>
                                       </div>
                                    </div>
                                    <div class="desc-holder">
                                       <a href="javascript:void(0)">Adel Hasanat</a> has a question about <a class="sub-link" href="javascript:void(0)"><?=$st_nm_S?></a>
                                       <span class="timestamp">August 31 at 08:45 pm<span class="glyphicon glyphicon-globe"></span></span>
                                    </div>
                                 </div>
                                 <div class="settings-icon">
                                    <div class="dropdown">
                                       <a class="dropdown-button more_btn" href="javascript:void(0)" data-activates="dropdown-editques">
                                       <i class="zmdi zmdi-more zmdi-hc-2x"></i>
                                       </a>
                                       <ul class="dropdown-content custom_dropdown" id="dropdown-editques">
                                          <li><a href="javascript:void(0)" class="edit_newask">Edit question</a></li>
                                       </ul>
                                    </div>
                                 </div>
                              </div>
                              <div class="post-content">
                                 <div class="post-details">
                                    <div class="post-title">Is veg food easily available?</div>
                                    <div class="post-desc">
                                       <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sed varius risus. Duis rhoncus eros et pellentesque imperdiet.?</p>
                                    </div>
                                 </div>
                              </div>
                              <div class="clear"></div>
                              <div class="post-data">
                                 <div class="post-actions">
                                    <div class="right like-tooltip">
                                       <a href="javascript:void(0)" class="pa-like" data-title="User Name"><i class="zmdi zmdi-thumb-up"></i></a>
                                       <span class="lcount">4</span>
                                       <a href="javascript:void(0)" class="pa-comment"><i class="zmdi zmdi-comment"></i></a>
                                       <span class="comment-lcount">4</span>
                                    </div>
                                 </div>
                                 <div class="comments-section panel">
                                    <div class="comments-area">
                                       <div class="post-more">
                                          <a href="javascript:void(0)" class="view-morec">View more comments</a>
                                          <span class="total-comments">3 of 7</span>
                                       </div>
                                       <div class="post-comments">
                                          <div class="pcomments">
                                             <div class="pcomment-earlier">
                                                <div class="pcomment-holder">
                                                   <div class="pcomment main-comment">
                                                      <div class="img-holder">
                                                         <div id="commentptip-1" class="profiletipholder">
                                                            <span class="profile-tooltip">
                                                            <img class="circle" src="images/demo-profile.jpg" />
                                                            </span>
                                                            <span class="profiletooltip_content">
                                                               <div class="profile-tip">
                                                                  <div class="profile-tip-cover"><img src="images/cover.jpg"></div>
                                                                  <div class="profile-tip-avatar">
                                                                     <a href="javascript:void(0)">
                                                                     <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">
                                                                     </a>
                                                                  </div>
                                                                  <div class="profile-tip-info">
                                                                     <div class="cover-username"><a href="javascript:void(0)">Adel Hasanat</a></div>
                                                                     <div class="cover-headline">
                                                                        <span class="ptip-icon"><i class="fa  fa-suitcase"></i></span>
                                                                        Web Designer, Cricketer
                                                                     </div>
                                                                     <div class="profiletip-bio">
                                                                        <span class="ptip-icon"><i class="mdi mdi-home"></i></span>
                                                                        Lives in : <span>Gariyadhar</span>
                                                                     </div>
                                                                     <div class="profiletip-bio">
                                                                        <span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span>
                                                                        Currently in : <span>Gariyadhar, Gujarat, India</span>
                                                                     </div>
                                                                  </div>
                                                                  <div class="profile-tip-divider"></div>
                                                                  <div class="profile-tip-btn">
                                                                     <a href="javascript:void(0)" class="btn btn-primary btn-sm"><i class=”mdi mdi-eye”></i>View Profile</a>
                                                                  </div>
                                                               </div>
                                                            </span>
                                                         </div>
                                                      </div>
                                                      <div class="desc-holder">
                                                         <div class="normal-mode">
                                                            <div class="desc">
                                                               <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                                               <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                                            </div>
                                                            <div class="comment-stuff">
                                                               <div class="more-opt">
                                                                  <a href="javascript:void(0)" class="pa-like"><span>icon</span></a>
                                                                  <a href="javascript:void(0)" class="pa-reply reply-comment"><span>icon</span></a>
                                                                  <div class="dropdown dropdown-custom dropdown-xxsmall">
                                                                     <a href="javascript:void(0)" class="dropdown-toggle dropdown-button" data-activates="dropdown-editdeleteask1">
                                                                     <i class="zmdi zmdi-hc-2x zmdi-more-vert"></i>
                                                                     </a>
                                                                     <ul class="dropdown-content" id="dropdown-editdeleteask1">
                                                                        <li><a href="javascript:void(0)" class="edit-comment">Edit</a></li>
                                                                        <li><a href="javascript:void(0)" class="delete-comment">Delete</a></li>
                                                                     </ul>
                                                                  </div>
                                                               </div>
                                                               <div class="less-opt">
                                                                  <div class="timestamp">8h</div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                         <div class="edit-mode">
                                                            <div class="desc">
                                                               <textarea class="editcomment-tt">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                                               <a href="javascript:void(0)" class="btn btn-primary btn-sm editcomment-cancel waves-effect waves-light">Cancel</a>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div class="clear"></div>
                                                   <div class="comment-reply-holder comment-addreply">
                                                      <div class="addnew-comment valign-wrapper comment-reply">
                                                         <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg" /></a></div>
                                                         <div class="desc-holder">
                                                            <div class="sliding-middle-custom anim-area">
                                                               <textarea class="materialize-textarea mb0 md_textarea item_tagline">Write a reply...</textarea>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="pcomment-holder">
                                                   <div class="pcomment main-comment">
                                                      <div class="img-holder">
                                                         <div id="commentptip-2" class="profiletipholder">
                                                            <span class="profile-tooltip">
                                                            <img class="circle" src="images/demo-profile.jpg" />
                                                            </span>
                                                            <span class="profiletooltip_content">
                                                               <div class="profile-tip">
                                                                  <div class="profile-tip-cover"><img src="images/cover.jpg"></div>
                                                                  <div class="profile-tip-avatar">
                                                                     <a href="javascript:void(0)">
                                                                     <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">
                                                                     </a>
                                                                  </div>
                                                                  <div class="profile-tip-info">
                                                                     <div class="cover-username"><a href="javascript:void(0)">Adel Hasanat</a></div>
                                                                     <div class="cover-headline">
                                                                        <span class="ptip-icon"><i class="fa  fa-suitcase"></i></span>
                                                                        Web Designer, Cricketer
                                                                     </div>
                                                                     <div class="profiletip-bio">
                                                                        <span class="ptip-icon"><i class="mdi mdi-home"></i></span>
                                                                        Lives in : <span>Gariyadhar</span>
                                                                     </div>
                                                                     <div class="profiletip-bio">
                                                                        <span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span>
                                                                        Currently in : <span>Gariyadhar, Gujarat, India</span>
                                                                     </div>
                                                                  </div>
                                                                  <div class="profile-tip-divider"></div>
                                                                  <div class="profile-tip-btn">
                                                                     <a href="javascript:void(0)" class="btn btn-primary btn-sm"><i class=”mdi mdi-eye”></i>View Profile</a>
                                                                  </div>
                                                               </div>
                                                            </span>
                                                         </div>
                                                      </div>
                                                      <div class="desc-holder">
                                                         <div class="normal-mode">
                                                            <div class="desc">
                                                               <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                                               <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                                            </div>
                                                            <div class="comment-stuff">
                                                               <div class="more-opt">
                                                                  <a href="javascript:void(0)" class="pa-like"><span>icon</span></a>
                                                                  <a href="javascript:void(0)" class="pa-reply reply-comment"><span>icon</span></a>
                                                                  <div class="dropdown dropdown-custom dropdown-xxsmall">
                                                                     <a href="javascript:void(0)" class="dropdown-toggle dropdown-button" data-activates="dropdown-editdeleteask2">
                                                                     <i class="zmdi zmdi-hc-2x zmdi-more-vert"></i>
                                                                     </a>
                                                                     <ul class="dropdown-content" id="dropdown-editdeleteask2">
                                                                        <li><a href="javascript:void(0)" class="edit-comment">Edit</a></li>
                                                                        <li><a href="javascript:void(0)" class="delete-comment">Delete</a></li>
                                                                     </ul>
                                                                  </div>
                                                               </div>
                                                               <div class="less-opt">
                                                                  <div class="timestamp">8h</div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                         <div class="edit-mode">
                                                            <div class="desc">
                                                               <textarea class="editcomment-tt">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                                               <a href="javascript:void(0)" class="btn btn-primary btn-sm editcomment-cancel waves-effect waves-light">Cancel</a>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div class="clear"></div>
                                                   <div class="comment-reply-holder comment-addreply">
                                                      <div class="addnew-comment valign-wrapper comment-reply">
                                                         <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg" /></a></div>
                                                         <div class="desc-holder">
                                                            <div class="sliding-middle-custom anim-area">
                                                               <textarea class="materialize-textarea mb0 md_textarea item_tagline">Write a reply...</textarea>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="pcomment-holder has-comments">
                                                <div class="pcomment main-comment">
                                                   <div class="img-holder">
                                                      <div id="commentptip-3" class="profiletipholder">
                                                         <span class="profile-tooltip">
                                                         <img class="circle" src="images/demo-profile.jpg" />
                                                         </span>
                                                         <span class="profiletooltip_content">
                                                            <div class="profile-tip">
                                                               <div class="profile-tip-cover"><img src="images/cover.jpg"></div>
                                                               <div class="profile-tip-avatar">
                                                                  <a href="javascript:void(0)">
                                                                  <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">
                                                                  </a>
                                                               </div>
                                                               <div class="profile-tip-info">
                                                                  <div class="cover-username"><a href="javascript:void(0)">Adel Hasanat</a></div>
                                                                  <div class="cover-headline">
                                                                     <span class="ptip-icon"><i class="fa  fa-suitcase"></i></span>
                                                                     Web Designer, Cricketer
                                                                  </div>
                                                                  <div class="profiletip-bio">
                                                                     <span class="ptip-icon"><i class="mdi mdi-home"></i></span>
                                                                     Lives in : <span>Gariyadhar</span>
                                                                  </div>
                                                                  <div class="profiletip-bio">
                                                                     <span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span>
                                                                     Currently in : <span>Gariyadhar, Gujarat, India</span>
                                                                  </div>
                                                               </div>
                                                               <div class="profile-tip-divider"></div>
                                                               <div class="profile-tip-btn">
                                                                  <a href="javascript:void(0)" class="btn btn-primary btn-sm"><i class=”mdi mdi-eye”></i>View Profile</a>
                                                               </div>
                                                            </div>
                                                         </span>
                                                      </div>
                                                   </div>
                                                   <div class="desc-holder">
                                                      <div class="normal-mode">
                                                         <div class="desc">
                                                            <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                                         </div>
                                                         <div class="comment-stuff">
                                                            <div class="more-opt">
                                                               <a href="javascript:void(0)" class="pa-like"><span>icon</span></a>
                                                               <a href="javascript:void(0)" class="pa-reply reply-comment"><span>icon</span></a>
                                                               <div class="dropdown dropdown-custom dropdown-xxsmall">
                                                                  <a href="javascript:void(0)" class="dropdown-toggle dropdown-button" data-activates="dropdown-editdeleteask3">
                                                                  <i class="zmdi zmdi-hc-2x zmdi-more-vert"></i>
                                                                  </a>
                                                                  <ul class="dropdown-content" id="dropdown-editdeleteask3">
                                                                     <li><a href="javascript:void(0)" class="edit-comment">Edit</a></li>
                                                                     <li><a href="javascript:void(0)" class="delete-comment">Delete</a></li>
                                                                  </ul>
                                                               </div>
                                                            </div>
                                                            <div class="less-opt">
                                                               <div class="timestamp">8h</div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div class="edit-mode">
                                                         <div class="desc">
                                                            <textarea class="editcomment-tt">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                                            <a href="javascript:void(0)" class="btn btn-primary btn-sm editcomment-cancel waves-effect waves-light">Cancel</a>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="clear"></div>
                                                <div class="comment-reply-holder">
                                                   <div class="pcomment comment-reply">
                                                      <div class="img-holder">
                                                         <div id="commentptip-5" class="profiletipholder">
                                                            <span class="profile-tooltip">
                                                            <img class="circle" src="images/demo-profile.jpg" />
                                                            </span>
                                                            <span class="profiletooltip_content">
                                                               <div class="profile-tip">
                                                                  <div class="profile-tip-cover"><img src="images/cover.jpg"></div>
                                                                  <div class="profile-tip-avatar">
                                                                     <a href="javascript:void(0)">
                                                                     <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">
                                                                     </a>
                                                                  </div>
                                                                  <div class="profile-tip-info">
                                                                     <div class="cover-username"><a href="javascript:void(0)">Adel Hasanat</a></div>
                                                                     <div class="cover-headline">
                                                                        <span class="ptip-icon"><i class="fa  fa-suitcase"></i></span>
                                                                        Web Designer, Cricketer
                                                                     </div>
                                                                     <div class="profiletip-bio">
                                                                        <span class="ptip-icon"><i class="mdi mdi-home"></i></span>
                                                                        Lives in : <span>Gariyadhar</span>
                                                                     </div>
                                                                     <div class="profiletip-bio">
                                                                        <span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span>
                                                                        Currently in : <span>Gariyadhar, Gujarat, India</span>
                                                                     </div>
                                                                  </div>
                                                                  <div class="profile-tip-divider"></div>
                                                                  <div class="profile-tip-btn">
                                                                     <a href="javascript:void(0)" class="btn btn-primary btn-sm"><i class=”mdi mdi-eye”></i>View Profile</a>
                                                                  </div>
                                                               </div>
                                                            </span>
                                                         </div>
                                                      </div>
                                                      <div class="desc-holder">
                                                         <div class="normal-mode">
                                                            <div class="desc">
                                                               <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                                               <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                                            </div>
                                                            <div class="comment-stuff">
                                                               <div class="more-opt">
                                                                  <a href="javascript:void(0)" class="pa-like"><span>icon</span></a>
                                                                  <a href="javascript:void(0)" class="pa-reply reply-comment"><span>icon</span></a>
                                                                  <div class="dropdown dropdown-custom dropdown-xxsmall">
                                                                     <a href="javascript:void(0)" class="dropdown-toggle dropdown-button" data-activates="dropdown-editdeleteask4">
                                                                     <i class="zmdi zmdi-hc-2x zmdi-more-vert"></i>
                                                                     </a>
                                                                     <ul class="dropdown-content" id="dropdown-editdeleteask4">
                                                                        <li><a href="javascript:void(0)" class="edit-comment">Edit</a></li>
                                                                        <li><a href="javascript:void(0)" class="delete-comment">Delete</a></li>
                                                                     </ul>
                                                                  </div>
                                                               </div>
                                                               <div class="less-opt">
                                                                  <div class="timestamp">8h</div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                         <div class="edit-mode">
                                                            <div class="desc">
                                                               <textarea class="editcomment-tt">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                                               <a href="javascript:void(0)" class="btn btn-primary btn-sm editcomment-cancel waves-effect waves-light">Cancel</a>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div class="pcomment comment-reply">
                                                      <div class="img-holder">
                                                         <div id="commentptip-6" class="profiletipholder">
                                                            <span class="profile-tooltip">
                                                            <img class="circle" src="images/demo-profile.jpg" />
                                                            </span>
                                                            <span class="profiletooltip_content">
                                                               <div class="profile-tip">
                                                                  <div class="profile-tip-cover"><img src="images/cover.jpg"></div>
                                                                  <div class="profile-tip-avatar">
                                                                     <a href="javascript:void(0)">
                                                                     <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">
                                                                     </a>
                                                                  </div>
                                                                  <div class="profile-tip-info">
                                                                     <div class="cover-username"><a href="javascript:void(0)">Adel Hasanat</a></div>
                                                                     <div class="cover-headline">
                                                                        <span class="ptip-icon"><i class="fa  fa-suitcase"></i></span>
                                                                        Web Designer, Cricketer
                                                                     </div>
                                                                     <div class="profiletip-bio">
                                                                        <span class="ptip-icon"><i class="mdi mdi-home"></i></span>
                                                                        Lives in : <span>Gariyadhar</span>
                                                                     </div>
                                                                     <div class="profiletip-bio">
                                                                        <span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span>
                                                                        Currently in : <span>Gariyadhar, Gujarat, India</span>
                                                                     </div>
                                                                  </div>
                                                                  <div class="profile-tip-divider"></div>
                                                                  <div class="profile-tip-btn">
                                                                     <a href="javascript:void(0)" class="btn btn-primary btn-sm"><i class=”mdi mdi-eye”></i>View Profile</a>
                                                                  </div>
                                                               </div>
                                                            </span>
                                                         </div>
                                                      </div>
                                                      <div class="desc-holder">
                                                         <div class="normal-mode">
                                                            <div class="desc">
                                                               <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                                               <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit...</p>
                                                            </div>
                                                            <div class="comment-stuff">
                                                               <div class="more-opt">
                                                                  <a href="javascript:void(0)" class="pa-like"><span>icon</span></a>
                                                                  <a href="javascript:void(0)" class="pa-reply reply-comment"><span>icon</span></a>
                                                                  <div class="dropdown dropdown-custom dropdown-xxsmall">
                                                                     <a href="javascript:void(0)" class="dropdown-toggle dropdown-button" data-activates="dropdown-editdeleteask5">
                                                                     <i class="zmdi zmdi-hc-2x zmdi-more-vert"></i>
                                                                     </a>
                                                                     <ul class="dropdown-content" id="dropdown-editdeleteask5">
                                                                        <li><a href="javascript:void(0)" class="edit-comment">Edit</a></li>
                                                                        <li><a href="javascript:void(0)" class="delete-comment">Delete</a></li>
                                                                     </ul>
                                                                  </div>
                                                               </div>
                                                               <div class="less-opt">
                                                                  <div class="timestamp">8h</div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                         <div class="edit-mode">
                                                            <div class="desc">
                                                               <textarea class="editcomment-tt">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                                               <a href="javascript:void(0)" class="btn btn-primary btn-sm editcomment-cancel waves-effect waves-light">Cancel</a>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="comment-reply-holder comment-addreply">
                                                   <div class="addnew-comment valign-wrapper comment-reply">
                                                      <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg" /></a></div>
                                                      <div class="desc-holder">
                                                         <div class="sliding-middle-custom anim-area">
                                                            <textarea class="materialize-textarea mb0 md_textarea item_tagline">Write a reply...</textarea>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="pcomment-holder">
                                                <div class="pcomment main-comment">
                                                   <div class="img-holder">
                                                      <div id="commentptip-4" class="profiletipholder">
                                                         <span class="profile-tooltip">
                                                         <img class="circle" src="images/demo-profile.jpg" />
                                                         </span>
                                                         <span class="profiletooltip_content">
                                                            <div class="profile-tip">
                                                               <div class="profile-tip-cover"><img src="images/cover.jpg"></div>
                                                               <div class="profile-tip-avatar">
                                                                  <a href="javascript:void(0)">
                                                                  <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">
                                                                  </a>
                                                               </div>
                                                               <div class="profile-tip-info">
                                                                  <div class="cover-username"><a href="javascript:void(0)">Adel Hasanat</a></div>
                                                                  <div class="cover-headline">
                                                                     <span class="ptip-icon"><i class="fa  fa-suitcase"></i></span>
                                                                     Web Designer, Cricketer
                                                                  </div>
                                                                  <div class="profiletip-bio">
                                                                     <span class="ptip-icon"><i class="mdi mdi-home"></i></span>
                                                                     Lives in : <span>Gariyadhar</span>
                                                                  </div>
                                                                  <div class="profiletip-bio">
                                                                     <span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span>
                                                                     Currently in : <span>Gariyadhar, Gujarat, India</span>
                                                                  </div>
                                                               </div>
                                                               <div class="profile-tip-divider"></div>
                                                               <div class="profile-tip-btn">
                                                                  <a href="javascript:void(0)" class="btn btn-primary btn-sm"><i class=”mdi mdi-eye”></i>View Profile</a>
                                                               </div>
                                                            </div>
                                                         </span>
                                                      </div>
                                                   </div>
                                                   <div class="desc-holder">
                                                      <div class="normal-mode">
                                                         <div class="desc">
                                                            <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                                         </div>
                                                         <div class="comment-stuff">
                                                            <div class="more-opt">
                                                               <a href="javascript:void(0)" class="pa-like"><span>icon</span></a>
                                                               <a href="javascript:void(0)" class="pa-reply reply-comment"><span>icon</span></a>
                                                               <div class="dropdown dropdown-custom dropdown-xxsmall">
                                                                  <a href="javascript:void(0)" class="dropdown-toggle dropdown-button" data-activates="dropdown-editdeleteask6">
                                                                  <i class="zmdi zmdi-hc-2x zmdi-more-vert"></i>
                                                                  </a>
                                                                  <ul class="dropdown-content" id="dropdown-editdeleteask6">
                                                                     <li><a href="javascript:void(0)" class="edit-comment">Edit</a></li>
                                                                     <li><a href="javascript:void(0)" class="delete-comment">Delete</a></li>
                                                                  </ul>
                                                               </div>
                                                            </div>
                                                            <div class="less-opt">
                                                               <div class="timestamp">8h</div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div class="edit-mode">
                                                         <div class="desc">
                                                            <textarea class="editcomment-tt">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                                            <a href="javascript:void(0)" class="btn btn-primary btn-sm editcomment-cancel waves-effect waves-light">Cancel</a>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="clear"></div>
                                                <div class="comment-reply-holder comment-addreply">
                                                   <div class="addnew-comment valign-wrapper comment-reply">
                                                      <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg" /></a></div>
                                                      <div class="desc-holder">
                                                         <div class="sliding-middle-custom anim-area">
                                                            <textarea class="materialize-textarea mb0 md_textarea item_tagline">Write a reply...</textarea>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="addnew-comment valign-wrapper">
                                             <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg" /></a></div>
                                             <div class="desc-holder">
                                                <div class="sliding-middle-out anim-area">
                                                   <textarea>Write a comment</textarea>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="post-holder bborder reviewpost-holder">
                              <div class="post-topbar">
                                 <div class="post-userinfo">
                                    <div class="img-holder">
                                       <div id="profiletip-1" class="profiletipholder">
                                          <span class="profile-tooltip">
                                          <img class="circle" src="images/demo-profile.jpg" />
                                          </span>
                                          <span class="profiletooltip_content">
                                             <div class="profile-tip">
                                                <div class="profile-tip-cover"><img src="images/cover.jpg"></div>
                                                <div class="profile-tip-avatar">
                                                   <a href="javascript:void(0)">
                                                   <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">
                                                   </a>
                                                </div>
                                                <div class="profile-tip-info">
                                                   <div class="cover-username"><a href="javascript:void(0)">Adel Hasanat</a></div>
                                                   <div class="cover-headline">
                                                      <span class="ptip-icon"><i class="fa  fa-suitcase"></i></span>
                                                      Web Designer, Cricketer
                                                   </div>
                                                   <div class="profiletip-bio">
                                                      <span class="ptip-icon"><i class="mdi mdi-home"></i></span>
                                                      Lives in : <span>Gariyadhar</span>
                                                   </div>
                                                   <div class="profiletip-bio">
                                                      <span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span>
                                                      Currently in : <span>Gariyadhar, Gujarat, India</span>
                                                   </div>
                                                </div>
                                                <div class="profile-tip-divider"></div>
                                                <div class="profile-tip-btn">
                                                   <a href="javascript:void(0)" class="btn btn-primary btn-sm"><i class=”mdi mdi-eye”></i>View Profile</a>
                                                </div>
                                             </div>
                                          </span>
                                       </div>
                                    </div>
                                    <div class="desc-holder">
                                       <a href="javascript:void(0)">Nimish Parekh</a> has a question about <a class="sub-link" href="javascript:void(0)"><?=$st_nm_S?></a>
                                       <span class="timestamp">August 31 at 08:45 pm<span class="glyphicon glyphicon-globe"></span></span>
                                    </div>
                                 </div>
                                 <div class="settings-icon">
                                    <div class="dropdown">
                                       <a class="dropdown-button more_btn" href="javascript:void(0)" data-activates="dropdown-editques2">
                                       <i class="zmdi zmdi-more zmdi-hc-2x"></i>
                                       </a>
                                       <ul class="dropdown-content custom_dropdown" id="dropdown-editques2">
                                          <li><a href="javascript:void(0)" class="edit_newask">Edit question</a></li>
                                       </ul>
                                    </div>
                                 </div>
                              </div>
                              <div class="post-content">
                                 <div class="post-details">
                                    <div class="post-desc">
                                       <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sed varius risus. Duis rhoncus eros et pellentesque imperdiet.<br />Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sed varius risus. Duis rhoncus eros et pellentesque imperdiet.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec?</p>
                                    </div>
                                 </div>
                              </div>
                              <div class="clear"></div>
                              <div class="post-data">
                                 <div class="post-actions">
                                    <div class="right like-tooltip">
                                       <a href="javascript:void(0)" class="pa-like" data-title="User Name"><i class="zmdi zmdi-thumb-up"></i></a>
                                       <span class="lcount">4</span>
                                       <a href="javascript:void(0)" class="pa-comment"><i class="zmdi zmdi-comment"></i></a>
                                       <span class="comment-lcount">4</span>
                                    </div>
                                 </div>
                                 <div class="comments-section panel">
                                    <div class="comments-area">
                                       <div class="post-more">
                                          <a href="javascript:void(0)" class="view-morec">View more comments</a>
                                          <span class="total-comments">3 of 7</span>
                                       </div>
                                       <div class="post-comments">
                                          <div class="pcomments">
                                             <div class="pcomment-earlier">
                                                <div class="pcomment-holder">
                                                   <div class="pcomment main-comment">
                                                      <div class="img-holder">
                                                         <div id="commentptip-1" class="profiletipholder">
                                                            <span class="profile-tooltip">
                                                            <img class="circle" src="images/demo-profile.jpg" />
                                                            </span>
                                                            <span class="profiletooltip_content">
                                                               <div class="profile-tip">
                                                                  <div class="profile-tip-cover"><img src="images/cover.jpg"></div>
                                                                  <div class="profile-tip-avatar">
                                                                     <a href="javascript:void(0)">
                                                                     <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">
                                                                     </a>
                                                                  </div>
                                                                  <div class="profile-tip-info">
                                                                     <div class="cover-username"><a href="javascript:void(0)">Adel Hasanat</a></div>
                                                                     <div class="cover-headline">
                                                                        <span class="ptip-icon"><i class="fa  fa-suitcase"></i></span>
                                                                        Web Designer, Cricketer
                                                                     </div>
                                                                     <div class="profiletip-bio">
                                                                        <span class="ptip-icon"><i class="mdi mdi-home"></i></span>
                                                                        Lives in : <span>Gariyadhar</span>
                                                                     </div>
                                                                     <div class="profiletip-bio">
                                                                        <span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span>
                                                                        Currently in : <span>Gariyadhar, Gujarat, India</span>
                                                                     </div>
                                                                  </div>
                                                                  <div class="profile-tip-divider"></div>
                                                                  <div class="profile-tip-btn">
                                                                     <a href="javascript:void(0)" class="btn btn-primary btn-sm"><i class=”mdi mdi-eye”></i>View Profile</a>
                                                                  </div>
                                                               </div>
                                                            </span>
                                                         </div>
                                                      </div>
                                                      <div class="desc-holder">
                                                         <div class="normal-mode">
                                                            <div class="desc">
                                                               <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                                               <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                                            </div>
                                                            <div class="comment-stuff">
                                                               <div class="more-opt">
                                                                  <a href="javascript:void(0)" class="pa-like"><span>icon</span></a>
                                                                  <a href="javascript:void(0)" class="pa-reply reply-comment"><span>icon</span></a>
                                                                  <div class="dropdown dropdown-custom dropdown-xxsmall">
                                                                     <a href="javascript:void(0)" class="dropdown-toggle dropdown-button" data-activates="dropdown-editdeleteask7">
                                                                     <i class="zmdi zmdi-hc-2x zmdi-more-vert"></i>
                                                                     </a>
                                                                     <ul class="dropdown-content" id="dropdown-editdeleteask7">
                                                                        <li><a href="javascript:void(0)" class="edit-comment">Edit</a></li>
                                                                        <li><a href="javascript:void(0)" class="delete-comment">Delete</a></li>
                                                                     </ul>
                                                                  </div>
                                                               </div>
                                                               <div class="less-opt">
                                                                  <div class="timestamp">8h</div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                         <div class="edit-mode">
                                                            <div class="desc">
                                                               <textarea class="editcomment-tt">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                                               <a href="javascript:void(0)" class="btn btn-primary btn-sm editcomment-cancel waves-effect waves-light">Cancel</a>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div class="clear"></div>
                                                   <div class="comment-reply-holder comment-addreply">
                                                      <div class="addnew-comment valign-wrapper comment-reply">
                                                         <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg" /></a></div>
                                                         <div class="desc-holder">
                                                            <div class="sliding-middle-custom anim-area">
                                                               <textarea class="materialize-textarea mb0 md_textarea item_tagline">Write a reply...</textarea>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="pcomment-holder">
                                                   <div class="pcomment main-comment">
                                                      <div class="img-holder">
                                                         <div id="commentptip-2" class="profiletipholder">
                                                            <span class="profile-tooltip">
                                                            <img class="circle" src="images/demo-profile.jpg" />
                                                            </span>
                                                            <span class="profiletooltip_content">
                                                               <div class="profile-tip">
                                                                  <div class="profile-tip-cover"><img src="images/cover.jpg"></div>
                                                                  <div class="profile-tip-avatar">
                                                                     <a href="javascript:void(0)">
                                                                     <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">
                                                                     </a>
                                                                  </div>
                                                                  <div class="profile-tip-info">
                                                                     <div class="cover-username"><a href="javascript:void(0)">Adel Hasanat</a></div>
                                                                     <div class="cover-headline">
                                                                        <span class="ptip-icon"><i class="fa  fa-suitcase"></i></span>
                                                                        Web Designer, Cricketer
                                                                     </div>
                                                                     <div class="profiletip-bio">
                                                                        <span class="ptip-icon"><i class="mdi mdi-home"></i></span>
                                                                        Lives in : <span>Gariyadhar</span>
                                                                     </div>
                                                                     <div class="profiletip-bio">
                                                                        <span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span>
                                                                        Currently in : <span>Gariyadhar, Gujarat, India</span>
                                                                     </div>
                                                                  </div>
                                                                  <div class="profile-tip-divider"></div>
                                                                  <div class="profile-tip-btn">
                                                                     <a href="javascript:void(0)" class="btn btn-primary btn-sm"><i class=”mdi mdi-eye”></i>View Profile</a>
                                                                  </div>
                                                               </div>
                                                            </span>
                                                         </div>
                                                      </div>
                                                      <div class="desc-holder">
                                                         <div class="normal-mode">
                                                            <div class="desc">
                                                               <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                                               <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                                            </div>
                                                            <div class="comment-stuff">
                                                               <div class="more-opt">
                                                                  <a href="javascript:void(0)" class="pa-like"><span>icon</span></a>
                                                                  <a href="javascript:void(0)" class="pa-reply reply-comment"><span>icon</span></a>
                                                                  <div class="dropdown dropdown-custom dropdown-xxsmall">
                                                                     <a href="javascript:void(0)" class="dropdown-toggle dropdown-button" data-activates="dropdown-editdeleteask8">
                                                                     <i class="zmdi zmdi-hc-2x zmdi-more-vert"></i>
                                                                     </a>
                                                                     <ul class="dropdown-content" id="dropdown-editdeleteask8">
                                                                        <li><a href="javascript:void(0)" class="edit-comment">Edit</a></li>
                                                                        <li><a href="javascript:void(0)" class="delete-comment">Delete</a></li>
                                                                     </ul>
                                                                  </div>
                                                               </div>
                                                               <div class="less-opt">
                                                                  <div class="timestamp">8h</div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                         <div class="edit-mode">
                                                            <div class="desc">
                                                               <textarea class="editcomment-tt">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                                               <a href="javascript:void(0)" class="btn btn-primary btn-sm editcomment-cancel waves-effect waves-light">Cancel</a>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div class="clear"></div>
                                                   <div class="comment-reply-holder comment-addreply">
                                                      <div class="addnew-comment valign-wrapper comment-reply">
                                                         <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg" /></a></div>
                                                         <div class="desc-holder">
                                                            <div class="sliding-middle-custom anim-area">
                                                               <textarea class="materialize-textarea mb0 md_textarea item_tagline">Write a reply...</textarea>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="pcomment-holder has-comments">
                                                <div class="pcomment main-comment">
                                                   <div class="img-holder">
                                                      <div id="commentptip-3" class="profiletipholder">
                                                         <span class="profile-tooltip">
                                                         <img class="circle" src="images/demo-profile.jpg" />
                                                         </span>
                                                         <span class="profiletooltip_content">
                                                            <div class="profile-tip">
                                                               <div class="profile-tip-cover"><img src="images/cover.jpg"></div>
                                                               <div class="profile-tip-avatar">
                                                                  <a href="javascript:void(0)">
                                                                  <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">
                                                                  </a>
                                                               </div>
                                                               <div class="profile-tip-info">
                                                                  <div class="cover-username"><a href="javascript:void(0)">Adel Hasanat</a></div>
                                                                  <div class="cover-headline">
                                                                     <span class="ptip-icon"><i class="fa  fa-suitcase"></i></span>
                                                                     Web Designer, Cricketer
                                                                  </div>
                                                                  <div class="profiletip-bio">
                                                                     <span class="ptip-icon"><i class="mdi mdi-home"></i></span>
                                                                     Lives in : <span>Gariyadhar</span>
                                                                  </div>
                                                                  <div class="profiletip-bio">
                                                                     <span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span>
                                                                     Currently in : <span>Gariyadhar, Gujarat, India</span>
                                                                  </div>
                                                               </div>
                                                               <div class="profile-tip-divider"></div>
                                                               <div class="profile-tip-btn">
                                                                  <a href="javascript:void(0)" class="btn btn-primary btn-sm"><i class=”mdi mdi-eye”></i>View Profile</a>
                                                               </div>
                                                            </div>
                                                         </span>
                                                      </div>
                                                   </div>
                                                   <div class="desc-holder">
                                                      <div class="normal-mode">
                                                         <div class="desc">
                                                            <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                                         </div>
                                                         <div class="comment-stuff">
                                                            <div class="more-opt">
                                                               <a href="javascript:void(0)" class="pa-like"><span>icon</span></a>
                                                               <a href="javascript:void(0)" class="pa-reply reply-comment"><span>icon</span></a>
                                                               <div class="dropdown dropdown-custom dropdown-xxsmall">
                                                                  <a href="javascript:void(0)" class="dropdown-toggle dropdown-button" data-activates="dropdown-editdeleteask9">
                                                                  <i class="zmdi zmdi-hc-2x zmdi-more-vert"></i>
                                                                  </a>
                                                                  <ul class="dropdown-content" id="dropdown-editdeleteask9">
                                                                     <li><a href="javascript:void(0)" class="edit-comment">Edit</a></li>
                                                                     <li><a href="javascript:void(0)" class="delete-comment">Delete</a></li>
                                                                  </ul>
                                                               </div>
                                                            </div>
                                                            <div class="less-opt">
                                                               <div class="timestamp">8h</div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div class="edit-mode">
                                                         <div class="desc">
                                                            <textarea class="editcomment-tt">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                                            <a href="javascript:void(0)" class="btn btn-primary btn-sm editcomment-cancel waves-effect waves-light">Cancel</a>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="clear"></div>
                                                <div class="comment-reply-holder">
                                                   <div class="pcomment comment-reply">
                                                      <div class="img-holder">
                                                         <div id="commentptip-5" class="profiletipholder">
                                                            <span class="profile-tooltip">
                                                            <img class="circle" src="images/demo-profile.jpg" />
                                                            </span>
                                                            <span class="profiletooltip_content">
                                                               <div class="profile-tip">
                                                                  <div class="profile-tip-cover"><img src="images/cover.jpg"></div>
                                                                  <div class="profile-tip-avatar">
                                                                     <a href="javascript:void(0)">
                                                                     <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">
                                                                     </a>
                                                                  </div>
                                                                  <div class="profile-tip-info">
                                                                     <div class="cover-username"><a href="javascript:void(0)">Adel Hasanat</a></div>
                                                                     <div class="cover-headline">
                                                                        <span class="ptip-icon"><i class="fa  fa-suitcase"></i></span>
                                                                        Web Designer, Cricketer
                                                                     </div>
                                                                     <div class="profiletip-bio">
                                                                        <span class="ptip-icon"><i class="mdi mdi-home"></i></span>
                                                                        Lives in : <span>Gariyadhar</span>
                                                                     </div>
                                                                     <div class="profiletip-bio">
                                                                        <span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span>
                                                                        Currently in : <span>Gariyadhar, Gujarat, India</span>
                                                                     </div>
                                                                  </div>
                                                                  <div class="profile-tip-divider"></div>
                                                                  <div class="profile-tip-btn">
                                                                     <a href="javascript:void(0)" class="btn btn-primary btn-sm"><i class=”mdi mdi-eye”></i>View Profile</a>
                                                                  </div>
                                                               </div>
                                                            </span>
                                                         </div>
                                                      </div>
                                                      <div class="desc-holder">
                                                         <div class="normal-mode">
                                                            <div class="desc">
                                                               <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                                               <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                                            </div>
                                                            <div class="comment-stuff">
                                                               <div class="more-opt">
                                                                  <a href="javascript:void(0)" class="pa-like"><span>icon</span></a>
                                                                  <a href="javascript:void(0)" class="pa-reply reply-comment"><span>icon</span></a>
                                                                  <div class="dropdown dropdown-custom dropdown-xxsmall">
                                                                     <a href="javascript:void(0)" class="dropdown-toggle dropdown-button" data-activates="dropdown-editdeleteask10">
                                                                     <i class="zmdi zmdi-hc-2x zmdi-more-vert"></i>
                                                                     </a>
                                                                     <ul class="dropdown-content" id="dropdown-editdeleteask10">
                                                                        <li><a href="javascript:void(0)" class="edit-comment">Edit</a></li>
                                                                        <li><a href="javascript:void(0)" class="delete-comment">Delete</a></li>
                                                                     </ul>
                                                                  </div>
                                                               </div>
                                                               <div class="less-opt">
                                                                  <div class="timestamp">8h</div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                         <div class="edit-mode">
                                                            <div class="desc">
                                                               <textarea class="editcomment-tt">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                                               <a href="javascript:void(0)" class="btn btn-primary btn-sm editcomment-cancel waves-effect waves-light">Cancel</a>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div class="pcomment comment-reply">
                                                      <div class="img-holder">
                                                         <div id="commentptip-6" class="profiletipholder">
                                                            <span class="profile-tooltip">
                                                            <img class="circle" src="images/demo-profile.jpg" />
                                                            </span>
                                                            <span class="profiletooltip_content">
                                                               <div class="profile-tip">
                                                                  <div class="profile-tip-cover"><img src="images/cover.jpg"></div>
                                                                  <div class="profile-tip-avatar">
                                                                     <a href="javascript:void(0)">
                                                                     <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">
                                                                     </a>
                                                                  </div>
                                                                  <div class="profile-tip-info">
                                                                     <div class="cover-username"><a href="javascript:void(0)">Adel Hasanat</a></div>
                                                                     <div class="cover-headline">
                                                                        <span class="ptip-icon"><i class="fa  fa-suitcase"></i></span>
                                                                        Web Designer, Cricketer
                                                                     </div>
                                                                     <div class="profiletip-bio">
                                                                        <span class="ptip-icon"><i class="mdi mdi-home"></i></span>
                                                                        Lives in : <span>Gariyadhar</span>
                                                                     </div>
                                                                     <div class="profiletip-bio">
                                                                        <span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span>
                                                                        Currently in : <span>Gariyadhar, Gujarat, India</span>
                                                                     </div>
                                                                  </div>
                                                                  <div class="profile-tip-divider"></div>
                                                                  <div class="profile-tip-btn">
                                                                     <a href="javascript:void(0)" class="btn btn-primary btn-sm"><i class=”mdi mdi-eye”></i>View Profile</a>
                                                                  </div>
                                                               </div>
                                                            </span>
                                                         </div>
                                                      </div>
                                                      <div class="desc-holder">
                                                         <div class="normal-mode">
                                                            <div class="desc">
                                                               <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                                               <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit...</p>
                                                            </div>
                                                            <div class="comment-stuff">
                                                               <div class="more-opt">
                                                                  <a href="javascript:void(0)" class="pa-like"><span>icon</span></a>
                                                                  <a href="javascript:void(0)" class="pa-reply reply-comment"><span>icon</span></a>
                                                                  <div class="dropdown dropdown-custom dropdown-xxsmall">
                                                                     <a href="javascript:void(0)" class="dropdown-toggle dropdown-button" data-activates="dropdown-editdeleteask11">
                                                                     <i class="zmdi zmdi-hc-2x zmdi-more-vert"></i>
                                                                     </a>
                                                                     <ul class="dropdown-content" id="dropdown-editdeleteask11">
                                                                        <li><a href="javascript:void(0)" class="edit-comment">Edit</a></li>
                                                                        <li><a href="javascript:void(0)" class="delete-comment">Delete</a></li>
                                                                     </ul>
                                                                  </div>
                                                               </div>
                                                               <div class="less-opt">
                                                                  <div class="timestamp">8h</div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                         <div class="edit-mode">
                                                            <div class="desc">
                                                               <textarea class="editcomment-tt">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                                               <a href="javascript:void(0)" class="btn btn-primary btn-sm editcomment-cancel waves-effect waves-light">Cancel</a>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="comment-reply-holder comment-addreply">
                                                   <div class="addnew-comment valign-wrapper comment-reply">
                                                      <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg" /></a></div>
                                                      <div class="desc-holder">
                                                         <div class="sliding-middle-custom anim-area">
                                                            <textarea class="materialize-textarea mb0 md_textarea item_tagline">Write a reply...</textarea>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="pcomment-holder">
                                                <div class="pcomment main-comment">
                                                   <div class="img-holder">
                                                      <div id="commentptip-4" class="profiletipholder">
                                                         <span class="profile-tooltip">
                                                         <img class="circle" src="images/demo-profile.jpg" />
                                                         </span>
                                                         <span class="profiletooltip_content">
                                                            <div class="profile-tip">
                                                               <div class="profile-tip-cover"><img src="images/cover.jpg"></div>
                                                               <div class="profile-tip-avatar">
                                                                  <a href="javascript:void(0)">
                                                                  <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">
                                                                  </a>
                                                               </div>
                                                               <div class="profile-tip-info">
                                                                  <div class="cover-username"><a href="javascript:void(0)">Adel Hasanat</a></div>
                                                                  <div class="cover-headline">
                                                                     <span class="ptip-icon"><i class="fa  fa-suitcase"></i></span>
                                                                     Web Designer, Cricketer
                                                                  </div>
                                                                  <div class="profiletip-bio">
                                                                     <span class="ptip-icon"><i class="mdi mdi-home"></i></span>
                                                                     Lives in : <span>Gariyadhar</span>
                                                                  </div>
                                                                  <div class="profiletip-bio">
                                                                     <span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span>
                                                                     Currently in : <span>Gariyadhar, Gujarat, India</span>
                                                                  </div>
                                                               </div>
                                                               <div class="profile-tip-divider"></div>
                                                               <div class="profile-tip-btn">
                                                                  <a href="javascript:void(0)" class="btn btn-primary btn-sm"><i class=”mdi mdi-eye”></i>View Profile</a>
                                                               </div>
                                                            </div>
                                                         </span>
                                                      </div>
                                                   </div>
                                                   <div class="desc-holder">
                                                      <div class="normal-mode">
                                                         <div class="desc">
                                                            <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                                         </div>
                                                         <div class="comment-stuff">
                                                            <div class="more-opt">
                                                               <a href="javascript:void(0)" class="pa-like"><span>icon</span></a>
                                                               <a href="javascript:void(0)" class="pa-reply reply-comment"><span>icon</span></a>
                                                               <div class="dropdown dropdown-custom dropdown-xxsmall">
                                                                  <a href="javascript:void(0)" class="dropdown-toggle dropdown-button" data-activates="dropdown-editdeleteask12">
                                                                  <i class="zmdi zmdi-hc-2x zmdi-more-vert"></i>
                                                                  </a>
                                                                  <ul class="dropdown-content" id="dropdown-editdeleteask12">
                                                                     <li><a href="javascript:void(0)" class="edit-comment">Edit</a></li>
                                                                     <li><a href="javascript:void(0)" class="delete-comment">Delete</a></li>
                                                                  </ul>
                                                               </div>
                                                            </div>
                                                            <div class="less-opt">
                                                               <div class="timestamp">8h</div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div class="edit-mode">
                                                         <div class="desc">
                                                            <textarea class="editcomment-tt">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                                            <a href="javascript:void(0)" class="btn btn-primary btn-sm editcomment-cancel waves-effect waves-light">Cancel</a>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="clear"></div>
                                                <div class="comment-reply-holder comment-addreply">
                                                   <div class="addnew-comment valign-wrapper comment-reply">
                                                      <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg" /></a></div>
                                                      <div class="desc-holder">
                                                         <div class="sliding-middle-custom anim-area">
                                                            <textarea class="materialize-textarea mb0 md_textarea item_tagline">Write a reply...</textarea>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="addnew-comment valign-wrapper">
                                             <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg" /></a></div>
                                             <div class="desc-holder">
                                                <div class="sliding-middle-out anim-area">
                                                   <textarea>Write a comment</textarea>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="new-post-mobile clear">
                        <a href="javascript:void(0)" class="popup-window compose_newask" ><i class="mdi mdi-pencil"></i></a>
                     </div>
                  </div>
               </div>
            </div>
            <div id="places-lodge" class="placeslodge-content subtab top_tabs dis-none">
               <div class="content-box hotels-page">
                  <div class="search-area side-area">
                     <a href="javascript:void(0)" class="expand-link" onclick="mng_drop_searcharea(this)"><i class="mdi mdi-tune mdi-20px grey-text"></i></a>
                     <div class="expandable-area">
                        <div class="content-box bshadow">
                           <a href="javascript:void(0)" class="closearea" onclick="mng_drop_searcharea(this)">
                           <img src="images/cross-icon.png" />
                           </a>
                           <div class="cbox-title nborder">
                              Narrow your search results
                           </div>
                           <div class="cbox-desc">
                              <div class="srow">
                                 <h6>Hotel Class</h6>
                                 <ul>
                                    <li>
                                       <div class="h-checkbox entertosend leftbox">
                                          <input id="filter1" type="checkbox">
                                          <label for="filter1">
                                          <span class="stars-holder">
                                          <i class="mdi mdi-star"></i>
                                          <i class="mdi mdi-star"></i>
                                          <i class="mdi mdi-star"></i>
                                          <i class="mdi mdi-star"></i>
                                          <i class="mdi mdi-star"></i>
                                          </span></label>
                                       </div>
                                    </li>
                                    <li>
                                       <div class="h-checkbox entertosend leftbox">
                                          <input id="filter2" type="checkbox">
                                          <label for="filter2">
                                          <span class="stars-holder">
                                          <i class="mdi mdi-star"></i>
                                          <i class="mdi mdi-star"></i>
                                          <i class="mdi mdi-star"></i>
                                          <i class="mdi mdi-star"></i>
                                          </span></label>
                                       </div>
                                    </li>
                                    <li>
                                       <div class="h-checkbox entertosend leftbox">
                                          <input id="filter3" type="checkbox">
                                          <label for="filter3">
                                          <span class="stars-holder">
                                          <i class="mdi mdi-star"></i>
                                          <i class="mdi mdi-star"></i>
                                          <i class="mdi mdi-star"></i>
                                          </span>
                                          </label>
                                       </div>
                                    </li>
                                    <li>
                                       <div class="h-checkbox entertosend leftbox">
                                          <input id="filter4" type="checkbox">
                                          <label for="filter4">
                                          <span class="stars-holder">
                                          <i class="mdi mdi-star"></i>
                                          <i class="mdi mdi-star"></i>                                                                   </span>
                                          </label>
                                       </div>
                                    </li>
                                    <li>
                                       <div class="h-checkbox entertosend leftbox">
                                          <input id="filter5" type="checkbox">
                                          <label for="filter5">
                                          <span class="stars-holder">
                                          <i class="mdi mdi-star"></i>                                                                                                                                                                                                           </span>
                                          </label>
                                       </div>
                                    </li>
                                 </ul>
                              </div>
                              <div class="srow">
                                 <h6>Guest Ratings</h6>
                                 <ul>
                                    <li>
                                       <div class="h-checkbox entertosend leftbox">
                                          <input id="filterstar1" type="checkbox">
                                          <label for="filterstar1">
                                          <span class="checks-holder">
                                          <i class="zmdi zmdi-check-circle active"></i>
                                          <i class="zmdi zmdi-check-circle active"></i>
                                          <i class="zmdi zmdi-check-circle active"></i>
                                          <i class="zmdi zmdi-check-circle active"></i>
                                          <i class="zmdi zmdi-check-circle active"></i>
                                          </span>
                                          </label>
                                       </div>
                                    </li>
                                    <li>
                                       <div class="h-checkbox entertosend leftbox">
                                          <input id="filterstar2" type="checkbox">
                                          <label for="filterstar2">
                                          <span class="checks-holder">
                                          <i class="zmdi zmdi-check-circle active"></i>
                                          <i class="zmdi zmdi-check-circle active"></i>
                                          <i class="zmdi zmdi-check-circle active"></i>
                                          <i class="zmdi zmdi-check-circle active"></i>
                                          <i class="zmdi zmdi-check-circle"></i>
                                          </span>
                                          </label>
                                       </div>
                                    </li>
                                    <li>
                                       <div class="h-checkbox entertosend leftbox">
                                          <input id="filterstar3" type="checkbox">
                                          <label for="filterstar3">
                                          <span class="checks-holder">
                                          <i class="zmdi zmdi-check-circle active"></i>
                                          <i class="zmdi zmdi-check-circle active"></i>
                                          <i class="zmdi zmdi-check-circle active"></i>
                                          <i class="zmdi zmdi-check-circle"></i>
                                          <i class="zmdi zmdi-check-circle"></i>
                                          </span>
                                          </label>
                                       </div>
                                    </li>
                                    <li>
                                       <div class="h-checkbox entertosend leftbox">
                                          <input id="filterstar4" type="checkbox">
                                          <label for="filterstar4">
                                          <span class="checks-holder">
                                          <i class="zmdi zmdi-check-circle active"></i>
                                          <i class="zmdi zmdi-check-circle active"></i>
                                          <i class="zmdi zmdi-check-circle"></i>
                                          <i class="zmdi zmdi-check-circle"></i>
                                          <i class="zmdi zmdi-check-circle"></i>
                                          </span>
                                          </label>
                                       </div>
                                    </li>
                                    <li>
                                       <div class="h-checkbox entertosend leftbox">
                                          <input id="filterstar5" type="checkbox">
                                          <label for="filterstar5">
                                          <span class="checks-holder">
                                          <i class="zmdi zmdi-check-circle active"></i>
                                          <i class="zmdi zmdi-check-circle"></i>
                                          <i class="zmdi zmdi-check-circle"></i>
                                          <i class="zmdi zmdi-check-circle"></i>
                                          <i class="zmdi zmdi-check-circle"></i>
                                          </span>
                                          </label>
                                       </div>
                                    </li>
                                 </ul>
                              </div>
                              <div class="srow">
                                 <h6>Nightly Price</h6>
                                 <!--<div class="range-slider price-slider">
                                    <input type="text" class="amount" readonly
                                    <div class="slider-range"></div>
                                    <div class="min-value">$500</div>
                                    <div class="max-value">$10,000</div>
                                    </div>-->
                              </div>
                              <div class="srow">
                                 <h6>Distance from</h6>
                                 <div class="sliding-middle-out anim-area underlined fullwidth">
                                    <select class="select2" tabindex="-1" >
                                       <option>City Center</option>
                                       <option>Palace</option>
                                       <option>Bus Station</option>
                                       <option>Railway Station</option>
                                    </select>
                                 </div>
                                 <!--<div class="range-slider distance-slider">
                                    <input type="text" class="amount" readonly 
                                    <div class="slider-range"></div>
                                    <div class="min-value">$500</div>
                                    <div class="max-value">$10,000</div>
                                    </div>-->
                              </div>
                              <div class="srow">
                                 <h6>Amenities</h6>
                                 <ul class="ul-amenities">
                                    <li>
                                       <a href="javascript:void(0)"><img src="images/amenity-spa.png" /><span>Spa</span></a>
                                    </li>
                                    <li>
                                       <a href="javascript:void(0)"><img src="images/amenity-beach.png" /><span>Beach</span></a>
                                    </li>
                                    <li>
                                       <a href="javascript:void(0)"><img src="images/amenity-wifi.png" /><span>Wifi</span></a>
                                    </li>
                                    <li>
                                       <a href="javascript:void(0)"><img src="images/amenity-breakfast.png" /><span>Breakfast</span></a>
                                    </li>
                                    <li>
                                       <a href="javascript:void(0)"><img src="images/amenity-pool.png" /><span>Pool</span></a>
                                    </li>
                                    <li>
                                       <a href="javascript:void(0)"><img src="images/amenity-spa.png" /><span>Spa</span></a>
                                    </li>
                                    <li>
                                       <a href="javascript:void(0)"><img src="images/amenity-beach.png" /><span>Beach</span></a>
                                    </li>
                                    <li>
                                       <a href="javascript:void(0)"><img src="images/amenity-breakfast.png" /><span>Breakfast</span></a>
                                    </li>
                                 </ul>
                              </div>
                              <div class="btn-holder">
                                 <a href="javascript:void(0)" class="btn btn-primary btn-md waves-effect waves-light">Reset Filters</a>
                              </div>
                           </div>
                        </div>
                        <div class="content-box bshadow">
                           <div class="cbox-title nborder">
                              Search Hotels
                           </div>
                           <div class="cbox-desc">
                              <div class="srow">
                                 <h6>Search hotel by name</h6>
                                 <div class="sliding-middle-out anim-area underlined fullwidth">
                                    <input type="text" placeholder="Enter hotel name" class="fullwidth">
                                 </div>
                              </div>
                              <div class="srow">
                                 <h6>Search hotel by address</h6>
                                 <div class="sliding-middle-out anim-area underlined fullwidth">
                                    <input type="text" placeholder="Enter hotel address" class="fullwidth">
                                 </div>
                              </div>
                              <div class="btn-holder">
                                 <a href="javascript:void(0)" class="btn btn-primary btn-md waves-effect waves-light">Search</a>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="content-box">
                  <div class="mbl-tabnav">
                     <a href="javascript:void(0)" onclick="openDirectTab('places-all')"><i class="mdi mdi-arrow-left"></i></a> 
                     <h6>Hotels</h6>
                  </div>
                  <div class="placesection redsection">
                     <div class="cbox-desc hotels-page np">
                        <div class="tcontent-holder moreinfo-outer">
                           <div class="top-stuff top-graybg" id="all-hotels">
                              <div class="more-actions">
                                 <div class="sorting left">
                                    <label>Sort by</label>
                                    <div class="select-holder">
                                       <select class="select2" tabindex="-1" >
                                          <option>Pricing</option>
                                          <option>Ratings</option>
                                       </select>
                                    </div>
                                 </div>
                                 <ul class="tabs tabsnew text-right">
                                    <li class="tab"><a href="javascript:void(0)" class="manageMap" onclick="openMapSection(this)"><i class="zmdi zmdi-pin"></i>Map</a></li>
                                    <li class="tab"><a href="javascript:void(0)" onclick="openListSection(this)"><i class="zmdi zmdi-view-list-alt zmdi-hc-lg"></i>List</a></li>
                                 </ul>
                              </div>
                              <h6>23 hotels found in <span><?=$st_nm_S?></span></h6>
                           </div>
                           <div class="places-content-holder">
                              <div class="map-holder">
                                 <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3110.3465133386144!2d-9.167423685010494!3d38.77868997958898!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xd193295d5b45545%3A0x3f9e7b6a5f00e12c!2sPerta!5e0!3m2!1sen!2sin!4v1481089901870" width="600" height="450" frameborder="0" allowfullscreen></iframe>
                                 <a href="javascript:void(0)" class="overlay" onclick="expandMap(this,'#all-hotels')"></a>
                                 <a href="javascript:void(0)" class="closelink" onclick="shrinkMap(this)"><i class="mdi mdi-close"></i> Close</a>
                              </div>
                              <div class="list-holder">
                                 <div class="hotel-list">
                                    <ul>
                                       <li>
                                          <div class="hotel-li expandable-holder dealli mobilelist">
                                             <div class="summery-info">
                                                <div class="imgholder"><img src="images/hotel-demo.png" /></div>
                                                <div class="descholder">
                                                   <a href="javascript:void(0)" class="expand-link" onclick="mng_expandable(this,'hasClose')">
                                                      <h4>Hyatt Regency Dubai Creek
                                                      </h4>
                                                      <div class="clear"></div>
                                                      <div class="reviews-link">
                                                         <span class="review-count">54 reviews</span>
                                                         <span class="checks-holder">
                                                         <i class="zmdi zmdi-check-circle active"></i>
                                                         <i class="zmdi zmdi-check-circle active"></i>
                                                         <i class="zmdi zmdi-check-circle active"></i>
                                                         <i class="zmdi zmdi-check-circle active"></i>
                                                         <i class="zmdi zmdi-check-circle active"></i>
                                                         <label>Excellent - 88/100</label>
                                                         </span>
                                                      </div>
                                                      <span class="address">Dubai, Dubai(Emirates), United Arab Emirates</span>
                                                      <span class="distance-info">2.2 miles to City center</span>
                                                      <div class="more-holder">
                                                         <div class="tagging" onclick="explandTags(this)">
                                                            Popular with:
                                                            <span>point of interest</span>
                                                            <span>establishment</span>
                                                         </div>
                                                      </div>
                                                   </a>
                                                   <div class="info-action">
                                                      <span class="stars-holder">
                                                      <i class="mdi mdi-star active"></i>
                                                      <i class="mdi mdi-star active"></i>
                                                      <i class="mdi mdi-star active"></i>
                                                      <i class="mdi mdi-star active"></i>
                                                      <i class="mdi mdi-star active"></i>
                                                      </span>
                                                      <div class="clear"></div>
                                                      <span class="sitename">booking.com</span>
                                                      <div class="clear"></div>
                                                      <span class="price">JOD 184*</span>
                                                      <div class="clear"></div>
                                                      <a href="javascript:void(0)" class="deal-btn waves-effect waves-light">Book Now <i class="mdi mdi-chevron-right"></i></a>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="expandable-area">
                                                <a href="javascript:void(0)" class="shrink-link" onclick="mng_expandable(this,'closeIt')"><i class="mdi mdi-close"></i> Close</a>
                                                <div class="clear"></div>
                                                <div class="explandable-tabs">
                                                   <ul class="tabs tabsnew subtab-menu">
                                                      <li class="tab"><a class="" href="#subtab-details">Details</a></li>
                                                      <li class="tab"><a href="#subtab-review">Reviews</a></li>
                                                      <li class="tab"><a data-which="photo" href="#subtab-photos" data-tab="subtab-photos">Photos</a></li>
                                                      <!--<li class="tab"><a data-toggle="tab" href="#subtab-amenities">Amenities</a></li>-->
                                                   </ul>
                                                   <div class="tab-content">
                                                      <div id="subtab-details" class="">
                                                         <div class="subdetail-box">
                                                            <div class="infoholder">
                                                               <div class="descholder">
                                                                  <div class="more-holder">
                                                                     <ul class="infoul">
                                                                        <li>
                                                                           <i class="zmdi zmdi-pin"></i>
                                                                           132 Brick Lane | E1 6RU, <?=$st_nm_S?> E1 6RU, <?=$st_nm_S?>
                                                                        </li>
                                                                        <li>
                                                                           <i class="mdi mdi-phone"></i>
                                                                           +44 20 7247 8210
                                                                        </li>
                                                                        <li>
                                                                           <i class="mdi mdi-earth"></i>
                                                                           http://www.yourwebsite.com
                                                                        </li>
                                                                        <li>
                                                                           <i class="mdi mdi-clock-outline"></i>
                                                                           Mon-Fri : 12:00 PM - 10:00 AM
                                                                        </li>
                                                                        <li>
                                                                           <i class="mdi mdi-certificate "></i>
                                                                           Ranked #1 in <?=$st_nm_S?> Hotels
                                                                        </li>
                                                                     </ul>
                                                                     <div class="tagging" onclick="explandTags(this)">
                                                                        Popular with:
                                                                        <span>point of interest</span>
                                                                        <span>establishment</span>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div id="subtab-review" class="">
                                                         <div class="reviews-summery">
                                                            <div class="reviews-people">
                                                               <ul>
                                                                  <li>
                                                                     <div class="reviewpeople-box">
                                                                        <div class="imgholder"><img src="images/people-3.png" /></div>
                                                                        <div class="descholder">
                                                                           <h6>Kelly Mark <span>about 2 weeks ago</span></h6>
                                                                           <div class="stars-holder">
                                                                              <img src="images/filled-star.png" />
                                                                              <img src="images/filled-star.png" />
                                                                              <img src="images/filled-star.png" />
                                                                              <img src="images/blank-star.png" />
                                                                              <img src="images/blank-star.png" />
                                                                           </div>
                                                                           <div class="clear"></div>
                                                                           <p>We enjoyed the lounge and bar at the Ritz where you are offered many choices for drinks and some pretty elaborate looking dishes of food as well.</p>
                                                                        </div>
                                                                     </div>
                                                                  </li>
                                                                  <li>
                                                                     <div class="reviewpeople-box">
                                                                        <div class="imgholder"><img src="images/people-2.png" /></div>
                                                                        <div class="descholder">
                                                                           <h6>John Davior <span>about 8 months ago</span></h6>
                                                                           <div class="stars-holder">
                                                                              <img src="images/filled-star.png" />
                                                                              <img src="images/filled-star.png" />
                                                                              <img src="images/filled-star.png" />
                                                                              <img src="images/filled-star.png" />
                                                                              <img src="images/blank-star.png" />
                                                                           </div>
                                                                           <div class="clear"></div>
                                                                           <p>If you want a fancy London experience than The Ritz is where you need to go! At least budget for High Tea!</p>
                                                                        </div>
                                                                     </div>
                                                                  </li>
                                                                  <li>
                                                                     <div class="reviewpeople-box">
                                                                        <div class="imgholder"><img src="images/people-1.png" /></div>
                                                                        <div class="descholder">
                                                                           <h6>Joe Doe <span>about 11 months ago</span></h6>
                                                                           <div class="stars-holder">
                                                                              <img src="images/filled-star.png" />
                                                                              <img src="images/filled-star.png" />
                                                                              <img src="images/filled-star.png" />
                                                                              <img src="images/blank-star.png" />
                                                                              <img src="images/blank-star.png" />
                                                                           </div>
                                                                           <div class="clear"></div>
                                                                           <p>I am not at all sure this is the best hotel in London, but it does deserve the reputation as one of the most glamourous.</p>
                                                                        </div>
                                                                     </div>
                                                                  </li>
                                                               </ul>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div id="subtab-photos" class=" subtab-photos">
                                                         <div class="photo-gallery">
                                                            <div class="img-preview">
                                                               <img src="images/post-img1.jpg" />
                                                            </div>
                                                            <div class="thumbs-img">
                                                               <ul>
                                                                  <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img class="himg" src="images/post-img1.jpg" /></a></li>
                                                                  <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img src="images/post-img2.jpg" class="himg" /></a></li>
                                                                  <li><a href="javascript:void(0)" onclick="previewImage(this)" class="vimg-box"><img src="images/post-img3.jpg" class="vimg" /></a></li>
                                                                  <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img src="images/post-img4.jpg" class="himg" /></a></li>
                                                                  <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img src="images/post-img5.jpg" class="himg" /></a></li>
                                                                  <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img class="himg" src="images/post-img1.jpg" /></a></li>
                                                                  <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img src="images/post-img2.jpg" class="himg" /></a></li>
                                                                  <li><a href="javascript:void(0)" onclick="previewImage(this)" class="vimg-box"><img src="images/post-img3.jpg" class="vimg" /></a></li>
                                                                  <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img src="images/post-img4.jpg" class="himg" /></a></li>
                                                                  <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img src="images/post-img5.jpg" class="himg" /></a></li>
                                                               </ul>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </li>
                                       <li>
                                          <div class="hotel-li expandable-holder dealli">
                                             <div class="summery-info">
                                                <div class="imgholder"><img src="images/hotel-demo.png" /></div>
                                                <div class="descholder">
                                                   <a href="javascript:void(0)" class="expand-link" onclick="mng_expandable(this,'hasClose')">
                                                      <h4>Hyatt Regency Dubai Creek
                                                      </h4>
                                                      <div class="clear"></div>
                                                      <div class="reviews-link">
                                                         <span class="review-count">54 reviews</span>
                                                         <span class="checks-holder">
                                                         <i class="zmdi zmdi-check-circle active"></i>
                                                         <i class="zmdi zmdi-check-circle active"></i>
                                                         <i class="zmdi zmdi-check-circle active"></i>
                                                         <i class="zmdi zmdi-check-circle active"></i>
                                                         <i class="zmdi zmdi-check-circle active"></i>
                                                         <label>Excellent - 88/100</label>
                                                         </span>
                                                      </div>
                                                      <span class="address">Dubai, Dubai(Emirates), United Arab Emirates</span>
                                                      <span class="distance-info">2.2 miles to City center</span>
                                                      <div class="more-holder">
                                                         <div class="tagging" onclick="explandTags(this)">
                                                            Popular with:
                                                            <span>point of interest</span>
                                                            <span>establishment</span>
                                                         </div>
                                                      </div>
                                                   </a>
                                                   <div class="info-action">
                                                      <span class="stars-holder">
                                                      <i class="mdi mdi-star active"></i>
                                                      <i class="mdi mdi-star active"></i>
                                                      <i class="mdi mdi-star active"></i>
                                                      <i class="mdi mdi-star active"></i>
                                                      <i class="mdi mdi-star active"></i>
                                                      </span>
                                                      <div class="clear"></div>
                                                      <span class="sitename">booking.com</span>
                                                      <div class="clear"></div>
                                                      <span class="price">JOD 184*</span>
                                                      <div class="clear"></div>
                                                      <a href="javascript:void(0)" class="deal-btn">Book Now <i class="mdi mdi-chevron-right"></i></a>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="expandable-area">
                                                <a href="javascript:void(0)" class="shrink-link" onclick="mng_expandable(this,'closeIt')"><i class="mdi mdi-close"></i> Close</a>
                                                <div class="clear"></div>
                                                <div class="explandable-tabs">
                                                   <ul class="tabs tabsnew subtab-menu">
                                                      <li class="active tab"><a href="#subtab-details">Details</a></li>
                                                      <li class="tab"><a href="#subtab-reviews">Reviews</a></li>
                                                      <li><a aria-expanded="false" data-toggle="tab" data-which="photo" href="#subtab-photos" data-tab="subtab-photos">Photos</a></li>
                                                      <li><a aria-expanded="false" data-toggle="tab" href="#subtab-amenities">Amenities</a></li>
                                                   </ul>
                                                   <div class="tab-content">
                                                      <div id="subtab-details" class="tab-pane fade active in">
                                                         <div class="subdetail-box">
                                                            <div class="infoholder">
                                                               <div class="descholder">
                                                                  <div class="more-holder">
                                                                     <ul class="infoul">
                                                                        <li>
                                                                           <i class="zmdi zmdi-pin"></i>
                                                                           132 Brick Lane | E1 6RU, <?=$st_nm_S?> E1 6RU, <?=$st_nm_S?>
                                                                        </li>
                                                                        <li>
                                                                           <i class="mdi mdi-phone"></i>
                                                                           +44 20 7247 8210
                                                                        </li>
                                                                        <li>
                                                                           <i class="mdi mdi-earth"></i>
                                                                           http://www.yourwebsite.com
                                                                        </li>
                                                                        <li>
                                                                           <i class="mdi mdi-clock-outline"></i>
                                                                           Mon-Fri : 12:00 PM - 10:00 AM
                                                                        </li>
                                                                        <li>
                                                                           <i class="mdi mdi-certificate "></i>
                                                                           Ranked #1 in <?=$st_nm_S?> Hotels
                                                                        </li>
                                                                     </ul>
                                                                     <div class="tagging" onclick="explandTags(this)">
                                                                        Popular with:
                                                                        <span>point of interest</span>
                                                                        <span>establishment</span>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div id="subtab-reviews" class="tab-pane fade">
                                                         <div class="reviews-summery">
                                                            <div class="reviews-people">
                                                               <ul>
                                                                  <li>
                                                                     <div class="reviewpeople-box">
                                                                        <div class="imgholder"><img src="images/people-3.png" /></div>
                                                                        <div class="descholder">
                                                                           <h6>Kelly Mark <span>about 2 weeks ago</span></h6>
                                                                           <div class="stars-holder">
                                                                              <img src="images/filled-star.png" />
                                                                              <img src="images/filled-star.png" />
                                                                              <img src="images/filled-star.png" />
                                                                              <img src="images/blank-star.png" />
                                                                              <img src="images/blank-star.png" />
                                                                           </div>
                                                                           <div class="clear"></div>
                                                                           <p>We enjoyed the lounge and bar at the Ritz where you are offered many choices for drinks and some pretty elaborate looking dishes of food as well.</p>
                                                                        </div>
                                                                     </div>
                                                                  </li>
                                                                  <li>
                                                                     <div class="reviewpeople-box">
                                                                        <div class="imgholder"><img src="images/people-2.png" /></div>
                                                                        <div class="descholder">
                                                                           <h6>John Davior <span>about 8 months ago</span></h6>
                                                                           <div class="stars-holder">
                                                                              <img src="images/filled-star.png" />
                                                                              <img src="images/filled-star.png" />
                                                                              <img src="images/filled-star.png" />
                                                                              <img src="images/filled-star.png" />
                                                                              <img src="images/blank-star.png" />
                                                                           </div>
                                                                           <div class="clear"></div>
                                                                           <p>If you want a fancy London experience than The Ritz is where you need to go! At least budget for High Tea!</p>
                                                                        </div>
                                                                     </div>
                                                                  </li>
                                                                  <li>
                                                                     <div class="reviewpeople-box">
                                                                        <div class="imgholder"><img src="images/people-1.png" /></div>
                                                                        <div class="descholder">
                                                                           <h6>Joe Doe <span>about 11 months ago</span></h6>
                                                                           <div class="stars-holder">
                                                                              <img src="images/filled-star.png" />
                                                                              <img src="images/filled-star.png" />
                                                                              <img src="images/filled-star.png" />
                                                                              <img src="images/blank-star.png" />
                                                                              <img src="images/blank-star.png" />
                                                                           </div>
                                                                           <div class="clear"></div>
                                                                           <p>I am not at all sure this is the best hotel in London, but it does deserve the reputation as one of the most glamourous.</p>
                                                                        </div>
                                                                     </div>
                                                                  </li>
                                                               </ul>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div id="subtab-photos" class="tab-pane fade subtab-photos">
                                                         <div class="photo-gallery">
                                                            <div class="img-preview">
                                                               <img src="images/post-img1.jpg" />
                                                            </div>
                                                            <div class="thumbs-img">
                                                               <ul>
                                                                  <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img class="himg" src="images/post-img1.jpg" /></a></li>
                                                                  <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img src="images/post-img2.jpg" class="himg" /></a></li>
                                                                  <li><a href="javascript:void(0)" onclick="previewImage(this)" class="vimg-box"><img src="images/post-img3.jpg" class="vimg" /></a></li>
                                                                  <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img src="images/post-img4.jpg" class="himg" /></a></li>
                                                                  <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img src="images/post-img5.jpg" class="himg" /></a></li>
                                                                  <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img class="himg" src="images/post-img1.jpg" /></a></li>
                                                                  <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img src="images/post-img2.jpg" class="himg" /></a></li>
                                                                  <li><a href="javascript:void(0)" onclick="previewImage(this)" class="vimg-box"><img src="images/post-img3.jpg" class="vimg" /></a></li>
                                                                  <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img src="images/post-img4.jpg" class="himg" /></a></li>
                                                                  <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img src="images/post-img5.jpg" class="himg" /></a></li>
                                                               </ul>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div id="subtab-amenities" class="tab-pane fade">
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </li>
                                       <li>
                                          <div class="hotel-li expandable-holder dealli">
                                             <div class="summery-info">
                                                <div class="imgholder"><img src="images/hotel-demo.png" /></div>
                                                <div class="descholder">
                                                   <a href="javascript:void(0)" class="expand-link" onclick="mng_expandable(this,'hasClose')">
                                                      <h4>Hyatt Regency Dubai Creek
                                                      </h4>
                                                      <div class="clear"></div>
                                                      <div class="reviews-link">
                                                         <span class="review-count">54 reviews</span>
                                                         <span class="checks-holder">
                                                         <i class="zmdi zmdi-check-circle active"></i>
                                                         <i class="zmdi zmdi-check-circle active"></i>
                                                         <i class="zmdi zmdi-check-circle active"></i>
                                                         <i class="zmdi zmdi-check-circle active"></i>
                                                         <i class="zmdi zmdi-check-circle active"></i>
                                                         <label>Excellent - 88/100</label>
                                                         </span>
                                                      </div>
                                                      <span class="address">Dubai, Dubai(Emirates), United Arab Emirates</span>
                                                      <span class="distance-info">2.2 miles to City center</span>
                                                      <div class="more-holder">
                                                         <div class="tagging" onclick="explandTags(this)">
                                                            Popular with:
                                                            <span>point of interest</span>
                                                            <span>establishment</span>
                                                         </div>
                                                      </div>
                                                   </a>
                                                   <div class="info-action">
                                                      <span class="stars-holder">
                                                      <i class="mdi mdi-star active"></i>
                                                      <i class="mdi mdi-star active"></i>
                                                      <i class="mdi mdi-star active"></i>
                                                      <i class="mdi mdi-star active"></i>
                                                      <i class="mdi mdi-star active"></i>
                                                      </span>
                                                      <div class="clear"></div>
                                                      <span class="sitename">booking.com</span>
                                                      <div class="clear"></div>
                                                      <span class="price">JOD 184*</span>
                                                      <div class="clear"></div>
                                                      <a href="javascript:void(0)" class="deal-btn">Book Now <i class="mdi mdi-chevron-right"></i></a>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="expandable-area">
                                                <a href="javascript:void(0)" class="shrink-link" onclick="mng_expandable(this,'closeIt')"><i class="mdi mdi-close"></i> Close</a>
                                                <div class="clear"></div>
                                                <div class="explandable-tabs">
                                                   <ul class="tabs tabsnew subtab-menu">
                                                      <li class="active tab"><a href="#subtab-details">Details</a></li>
                                                      <li class="tab"><a href="#subtab-reviews">Reviews</a></li>
                                                      <li><a aria-expanded="false" data-toggle="tab" data-which="photo" href="#subtab-photos" data-tab="subtab-photos">Photos</a></li>
                                                      <li><a aria-expanded="false" data-toggle="tab" href="#subtab-amenities">Amenities</a></li>
                                                   </ul>
                                                   <div class="tab-content">
                                                      <div id="subtab-details" class="tab-pane fade active in">
                                                         <div class="subdetail-box">
                                                            <div class="infoholder">
                                                               <div class="descholder">
                                                                  <div class="more-holder">
                                                                     <ul class="infoul">
                                                                        <li>
                                                                           <i class="zmdi zmdi-pin"></i>
                                                                           132 Brick Lane | E1 6RU, <?=$st_nm_S?> E1 6RU, <?=$st_nm_S?>
                                                                        </li>
                                                                        <li>
                                                                           <i class="mdi mdi-phone"></i>
                                                                           +44 20 7247 8210
                                                                        </li>
                                                                        <li>
                                                                           <i class="mdi mdi-earth"></i>
                                                                           http://www.yourwebsite.com
                                                                        </li>
                                                                        <li>
                                                                           <i class="mdi mdi-clock-outline"></i>
                                                                           Mon-Fri : 12:00 PM - 10:00 AM
                                                                        </li>
                                                                        <li>
                                                                           <i class="mdi mdi-certificate "></i>
                                                                           Ranked #1 in <?=$st_nm_S?> Hotels
                                                                        </li>
                                                                     </ul>
                                                                     <div class="tagging" onclick="explandTags(this)">
                                                                        Popular with:
                                                                        <span>point of interest</span>
                                                                        <span>establishment</span>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div id="subtab-reviews" class="tab-pane fade">
                                                         <div class="reviews-summery">
                                                            <div class="reviews-people">
                                                               <ul>
                                                                  <li>
                                                                     <div class="reviewpeople-box">
                                                                        <div class="imgholder"><img src="images/people-3.png" /></div>
                                                                        <div class="descholder">
                                                                           <h6>Kelly Mark <span>about 2 weeks ago</span></h6>
                                                                           <div class="stars-holder">
                                                                              <img src="images/filled-star.png" />
                                                                              <img src="images/filled-star.png" />
                                                                              <img src="images/filled-star.png" />
                                                                              <img src="images/blank-star.png" />
                                                                              <img src="images/blank-star.png" />
                                                                           </div>
                                                                           <div class="clear"></div>
                                                                           <p>We enjoyed the lounge and bar at the Ritz where you are offered many choices for drinks and some pretty elaborate looking dishes of food as well.</p>
                                                                        </div>
                                                                     </div>
                                                                  </li>
                                                                  <li>
                                                                     <div class="reviewpeople-box">
                                                                        <div class="imgholder"><img src="images/people-2.png" /></div>
                                                                        <div class="descholder">
                                                                           <h6>John Davior <span>about 8 months ago</span></h6>
                                                                           <div class="stars-holder">
                                                                              <img src="images/filled-star.png" />
                                                                              <img src="images/filled-star.png" />
                                                                              <img src="images/filled-star.png" />
                                                                              <img src="images/filled-star.png" />
                                                                              <img src="images/blank-star.png" />
                                                                           </div>
                                                                           <div class="clear"></div>
                                                                           <p>If you want a fancy London experience than The Ritz is where you need to go! At least budget for High Tea!</p>
                                                                        </div>
                                                                     </div>
                                                                  </li>
                                                                  <li>
                                                                     <div class="reviewpeople-box">
                                                                        <div class="imgholder"><img src="images/people-1.png" /></div>
                                                                        <div class="descholder">
                                                                           <h6>Joe Doe <span>about 11 months ago</span></h6>
                                                                           <div class="stars-holder">
                                                                              <img src="images/filled-star.png" />
                                                                              <img src="images/filled-star.png" />
                                                                              <img src="images/filled-star.png" />
                                                                              <img src="images/blank-star.png" />
                                                                              <img src="images/blank-star.png" />
                                                                           </div>
                                                                           <div class="clear"></div>
                                                                           <p>I am not at all sure this is the best hotel in London, but it does deserve the reputation as one of the most glamourous.</p>
                                                                        </div>
                                                                     </div>
                                                                  </li>
                                                               </ul>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div id="subtab-photos" class="tab-pane fade subtab-photos">
                                                         <div class="photo-gallery">
                                                            <div class="img-preview">
                                                               <img src="images/post-img1.jpg" />
                                                            </div>
                                                            <div class="thumbs-img">
                                                               <ul>
                                                                  <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img class="himg" src="images/post-img1.jpg" /></a></li>
                                                                  <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img src="images/post-img2.jpg" class="himg" /></a></li>
                                                                  <li><a href="javascript:void(0)" onclick="previewImage(this)" class="vimg-box"><img src="images/post-img3.jpg" class="vimg" /></a></li>
                                                                  <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img src="images/post-img4.jpg" class="himg" /></a></li>
                                                                  <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img src="images/post-img5.jpg" class="himg" /></a></li>
                                                                  <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img class="himg" src="images/post-img1.jpg" /></a></li>
                                                                  <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img src="images/post-img2.jpg" class="himg" /></a></li>
                                                                  <li><a href="javascript:void(0)" onclick="previewImage(this)" class="vimg-box"><img src="images/post-img3.jpg" class="vimg" /></a></li>
                                                                  <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img src="images/post-img4.jpg" class="himg" /></a></li>
                                                                  <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img src="images/post-img5.jpg" class="himg" /></a></li>
                                                               </ul>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div id="subtab-amenities" class="tab-pane fade">
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </li>
                                    </ul>
                                    <div class="pagination">
                                       <div class="link-holder">
                                          <a href="javascript:void(0)"><i class="mdi mdi-arrow-left-bold-circle"></i> Prev</a>
                                       </div>
                                       <div class="link-holder">
                                          <a href="javascript:void(0)">Next <i class="mdi mdi-arrow-right-bold-circle"></i></a>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="moreinfo-box">
                              <a href="javascript:void(0)" onclick="closePlacesMoreInfo(this)" class="backarrow"><i class="mdi mdi-arrow-left-bold-circle"></i></a>
                              <div class="infoholder nice-scroll">
                                 <div class="imgholder"><img src="images/hotel1.png" /></div>
                                 <div class="descholder">
                                    <h4>The Guest House</h4>
                                    <div class="clear"></div>
                                    <div class="reviews-link">
                                       <span class="checks-holder">
                                       <i class="mdi mdi-star active"></i>
                                       <i class="mdi mdi-star active"></i>
                                       <i class="mdi mdi-star active"></i>
                                       <i class="mdi mdi-star active"></i>
                                       <i class="mdi mdi-star"></i>
                                       <label>34 Reviews</label>
                                       </span>
                                    </div>
                                    <span class="distance-info">Middle Eastem &amp; African, Mediterranean</span>
                                    <div class="clear"></div>
                                    <div class="more-holder">
                                       <ul class="infoul">
                                          <li>
                                             <i class="zmdi zmdi-pin"></i>
                                             132 Brick Lane | E1 6RU, <?=$st_nm_S?> E1 6RU, <?=$st_nm_S?>
                                          </li>
                                          <li>
                                             <i class="mdi mdi-phone"></i>
                                             +44 20 7247 8210
                                          </li>
                                          <li>
                                             <i class="mdi mdi-earth"></i>
                                             http://www.yourwebsite.com
                                          </li>
                                          <li>
                                             <i class="mdi mdi-clock-outline"></i>
                                             Today, 12:00 PM - 12:00 AM
                                          </li>
                                          <li>
                                             <i class="mdi mdi-certificate "></i>
                                             Ranked #1 in <?=$st_nm_S?> Hotels
                                          </li>
                                       </ul>
                                       <div class="tagging" onclick="explandTags(this)">
                                          Popular with:
                                          <span>Budget</span>
                                          <span>Foodies</span>
                                          <span>Family</span>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div id="places-dine" class="placesdine-content subtab top_tabs dis-none">
               <div class="content-box">
                  <div class="mbl-tabnav">
                     <a href="javascript:void(0)" onclick="openDirectTab('places-all')"><i class="mdi mdi-arrow-left"></i></a> 
                     <h6>Restaurants</h6>
                  </div>
                  <div class="placesection redsection">
                     <div class="cbox-desc hotels-page np">
                        <div class="tcontent-holder moreinfo-outer">
                           <div class="top-stuff top-graybg" id="all-restaurant">
                              <div class="more-actions">
                                 <ul class="tabs tabsnew text-right">
                                    <li class="tab"><a href="javascript:void(0)" class="manageMap" onclick="openMapSection(this)"><i class="zmdi zmdi-pin"></i>Map</a></li>
                                    <li class="tab"><a href="javascript:void(0)" onclick="openListSection(this)"><i class="zmdi zmdi-view-list-alt zmdi-hc-lg"></i>List</a></li>
                                 </ul>
                              </div>
                              <h6>345 restaurants found in <span><?=$st_nm_S?></span></h6>
                           </div>
                           <div class="places-content-holder">
                              <div class="map-holder">
                                 <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3110.3465133386144!2d-9.167423685010494!3d38.77868997958898!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xd193295d5b45545%3A0x3f9e7b6a5f00e12c!2sPerta!5e0!3m2!1sen!2sin!4v1481089901870" width="600" height="450" frameborder="0" allowfullscreen></iframe>
                                 <a href="javascript:void(0)" class="overlay" onclick="expandMap(this,'#all-restaurant')"></a>
                                 <a href="javascript:void(0)" class="closelink" onclick="shrinkMap(this)"><i class="mdi mdi-close"></i> Close</a>
                              </div>
                              <div class="list-holder">
                                 <div class="hotel-list fullw-list">
                                    <ul>
                                       <li>
                                          <div class="hotel-li expandable-holder dealli mobilelist">
                                             <div class="summery-info">
                                                <div class="imgholder himg-box"><img src="images/restaurant1.png" class="himg" /></div>
                                                <div class="descholder">
                                                   <a href="javascript:void(0)" class="expand-link" onclick="mng_expandable(this,'hasClose')">
                                                      <h4>Hashim Restaurant</h4>
                                                      <div class="clear"></div>
                                                      <div class="reviews-link">
                                                         <span class="review-count">30 Reviews</span>
                                                         <span class="checks-holder">
                                                         <i class="mdi mdi-star active"></i>
                                                         <i class="mdi mdi-star active"></i>
                                                         <i class="mdi mdi-star active"></i>
                                                         <i class="mdi mdi-star active"></i>
                                                         <i class="mdi mdi-star"></i>
                                                         <label>Very Good - 70/100</label>
                                                         </span>
                                                      </div>
                                                      <span class="address">Middle Eastem & African, Mediterranean</span>
                                                      <span class="distance-info"><i class="mdi mdi-phone"></i> 999-999-9999</span>
                                                      <div class="more-holder">
                                                         <div class="tagging" onclick="explandTags(this)">
                                                            Popular with:
                                                            <span>Budget</span>
                                                         </div>
                                                      </div>
                                                   </a>
                                                </div>
                                             </div>
                                             <div class="expandable-area">
                                                <a href="javascript:void(0)" class="shrink-link" onclick="mng_expandable(this,'closeIt')"><i class="mdi mdi-close"></i> Close</a>
                                                <div class="clear"></div>
                                                <div class="explandable-tabs">
                                                   <ul class="tabs tabsnew subtab-menu">
                                                      <li class="active tab"><a  href="#subtab-reviews2">Reviews</a></li>
                                                      <li class="tab"><a data-which="photo" href="#subtab-photos2" data-tab="subtab-photos">Photos</a></li>
                                                      <li><a aria-expanded="false" data-toggle="tab" href="#subtab-details2">Details</a></li>
                                                   </ul>
                                                   <div class="tab-content">
                                                      <div id="subtab-reviews2" class="tab-pane fade active in">
                                                         <div class="reviews-summery">
                                                            <div class="reviews-people">
                                                               <ul>
                                                                  <li>
                                                                     <div class="reviewpeople-box">
                                                                        <div class="imgholder"><img src="images/people-3.png" /></div>
                                                                        <div class="descholder">
                                                                           <h6>Kelly Mark <span>about 2 weeks ago</span></h6>
                                                                           <div class="stars-holder">
                                                                              <img src="images/filled-star.png" />
                                                                              <img src="images/filled-star.png" />
                                                                              <img src="images/filled-star.png" />
                                                                              <img src="images/blank-star.png" />
                                                                              <img src="images/blank-star.png" />
                                                                           </div>
                                                                           <div class="clear"></div>
                                                                           <p>We enjoyed the lounge and bar at the Ritz where you are offered many choices for drinks and some pretty elaborate looking dishes of food as well.</p>
                                                                        </div>
                                                                     </div>
                                                                  </li>
                                                                  <li>
                                                                     <div class="reviewpeople-box">
                                                                        <div class="imgholder"><img src="images/people-2.png" /></div>
                                                                        <div class="descholder">
                                                                           <h6>John Davior <span>about 8 months ago</span></h6>
                                                                           <div class="stars-holder">
                                                                              <img src="images/filled-star.png" />
                                                                              <img src="images/filled-star.png" />
                                                                              <img src="images/filled-star.png" />
                                                                              <img src="images/filled-star.png" />
                                                                              <img src="images/blank-star.png" />
                                                                           </div>
                                                                           <div class="clear"></div>
                                                                           <p>If you want a fancy London experience than The Ritz is where you need to go! At least budget for High Tea!</p>
                                                                        </div>
                                                                     </div>
                                                                  </li>
                                                                  <li>
                                                                     <div class="reviewpeople-box">
                                                                        <div class="imgholder"><img src="images/people-1.png" /></div>
                                                                        <div class="descholder">
                                                                           <h6>Joe Doe <span>about 11 months ago</span></h6>
                                                                           <div class="stars-holder">
                                                                              <img src="images/filled-star.png" />
                                                                              <img src="images/filled-star.png" />
                                                                              <img src="images/filled-star.png" />
                                                                              <img src="images/blank-star.png" />
                                                                              <img src="images/blank-star.png" />
                                                                           </div>
                                                                           <div class="clear"></div>
                                                                           <p>I am not at all sure this is the best hotel in London, but it does deserve the reputation as one of the most glamourous.</p>
                                                                        </div>
                                                                     </div>
                                                                  </li>
                                                               </ul>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div id="subtab-photos2" class="tab-pane fade subtab-photos">
                                                         <div class="photo-gallery">
                                                            <div class="img-preview">
                                                               <img src="images/post-img1.jpg" />
                                                            </div>
                                                            <div class="thumbs-img">
                                                               <ul>
                                                                  <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img class="himg" src="images/post-img1.jpg" /></a></li>
                                                                  <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img src="images/post-img2.jpg" class="himg" /></a></li>
                                                                  <li><a href="javascript:void(0)" onclick="previewImage(this)" class="vimg-box"><img src="images/post-img3.jpg" class="vimg" /></a></li>
                                                                  <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img src="images/post-img4.jpg" class="himg" /></a></li>
                                                                  <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img src="images/post-img5.jpg" class="himg" /></a></li>
                                                                  <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img class="himg" src="images/post-img1.jpg" /></a></li>
                                                                  <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img src="images/post-img2.jpg" class="himg" /></a></li>
                                                                  <li><a href="javascript:void(0)" onclick="previewImage(this)" class="vimg-box"><img src="images/post-img3.jpg" class="vimg" /></a></li>
                                                                  <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img src="images/post-img4.jpg" class="himg" /></a></li>
                                                                  <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img src="images/post-img5.jpg" class="himg" /></a></li>
                                                               </ul>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div id="subtab-details2" class="tab-pane fade">
                                                         <div class="subdetail-box">
                                                            <div class="infoholder">
                                                               <div class="descholder">
                                                                  <div class="more-holder">
                                                                     <ul class="infoul">
                                                                        <li>
                                                                           <i class="zmdi zmdi-pin"></i>
                                                                           132 Brick Lane | E1 6RU, <?=$st_nm_S?> E1 6RU, <?=$st_nm_S?>
                                                                        </li>
                                                                        <li>
                                                                           <i class="mdi mdi-phone"></i>
                                                                           +44 20 7247 8210
                                                                        </li>
                                                                        <li>
                                                                           <i class="mdi mdi-earth"></i>
                                                                           http://www.yourwebsite.com
                                                                        </li>
                                                                        <li>
                                                                           <i class="mdi mdi-clock-outline"></i>
                                                                           Mon-Fri : 12:00 PM - 10:00 AM
                                                                        </li>
                                                                        <li>
                                                                           <i class="mdi mdi-certificate "></i>
                                                                           Ranked #1 in <?=$st_nm_S?> Hotels
                                                                        </li>
                                                                     </ul>
                                                                     <div class="tagging" onclick="explandTags(this)">
                                                                        Popular with:
                                                                        <span>point of interest</span>
                                                                        <span>establishment</span>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </li>
                                       <li>
                                          <div class="hotel-li expandable-holder">
                                             <div class="summery-info">
                                                <div class="imgholder himg-box"><img src="images/restaurant2.png" class="himg" /></div>
                                                <div class="descholder">
                                                   <h4>Emirates Grand</h4>
                                                   <div class="clear"></div>
                                                   <div class="reviews-link">
                                                      <span class="checks-holder">
                                                      <i class="mdi mdi-star active"></i>
                                                      <i class="mdi mdi-star active"></i>
                                                      <i class="mdi mdi-star active"></i>
                                                      <i class="mdi mdi-star active"></i>
                                                      <i class="mdi mdi-star"></i>
                                                      <label>34 Reviews</label>
                                                      </span>
                                                   </div>
                                                   <span class="distance-info">Middle Eastem & African, Mediterranean</span>
                                                   <div class="more-holder">
                                                      <div class="tagging" onclick="explandTags(this)">
                                                         Popular with:
                                                         <span>Family</span>
                                                         <span>Luxury</span>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="expandable-area drawer-holder">
                                                <div class="expanded-details">
                                                   Some details goes here...Some details goes here...Some details goes here...Some details goes here...Some details goes here...Some details goes here...Some details goes here...
                                                </div>
                                             </div>
                                          </div>
                                       </li>
                                       <li>
                                          <div class="hotel-li expandable-holder">
                                             <div class="summery-info">
                                                <div class="imgholder himg-box"><img src="images/restaurant3.png" class="himg" /></div>
                                                <div class="descholder">
                                                   <h4>Al-Quds</h4>
                                                   <div class="clear"></div>
                                                   <div class="reviews-link">
                                                      <span class="checks-holder">
                                                      <i class="mdi mdi-star active"></i>
                                                      <i class="mdi mdi-star active"></i>
                                                      <i class="mdi mdi-star active"></i>
                                                      <i class="mdi mdi-star active"></i>
                                                      <i class="mdi mdi-star"></i>
                                                      <label>34 Reviews</label>
                                                      </span>
                                                   </div>
                                                   <span class="distance-info">Middle Eastem & African, Mediterranean</span>
                                                   <div class="more-holder">
                                                      <div class="tagging" onclick="explandTags(this)">
                                                         Popular with:
                                                         <span>Luxury</span>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="expandable-area drawer-holder">
                                                <div class="expanded-details">
                                                   Some details goes here...Some details goes here...Some details goes here...Some details goes here...Some details goes here...Some details goes here...Some details goes here...
                                                </div>
                                             </div>
                                          </div>
                                       </li>
                                    </ul>
                                    <div class="pagination">
                                       <div class="link-holder">
                                          <a href="javascript:void(0)"><i class="mdi mdi-arrow-left-bold-circle"></i> Prev</a>
                                       </div>
                                       <div class="link-holder">
                                          <a href="javascript:void(0)">Next <i class="mdi mdi-arrow-right-bold-circle"></i></a>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="moreinfo-box">
                              <a href="javascript:void(0)" onclick="closePlacesMoreInfo(this)" class="backarrow"><i class="mdi mdi-arrow-left-bold-circle"></i></a>
                              <div class="infoholder nice-scroll">
                                 <div class="imgholder"><img src="images/hotel1.png" /></div>
                                 <div class="descholder wid_0">
                                    <h4>The Guest House</h4>
                                    <div class="clear"></div>
                                    <div class="reviews-link">
                                       <span class="checks-holder">
                                       <i class="mdi mdi-star active"></i>
                                       <i class="mdi mdi-star active"></i>
                                       <i class="mdi mdi-star active"></i>
                                       <i class="mdi mdi-star active"></i>
                                       <i class="mdi mdi-star"></i>
                                       <label>34 Reviews</label>
                                       </span>
                                    </div>
                                    <span class="distance-info">Middle Eastem &amp; African, Mediterranean</span>
                                    <div class="clear"></div>
                                    <div class="more-holder">
                                       <ul class="infoul">
                                          <li>
                                             <i class="zmdi zmdi-pin"></i>
                                             132 Brick Lane | E1 6RU, <?=$st_nm_S?> E1 6RU, <?=$st_nm_S?>
                                          </li>
                                          <li>
                                             <i class="mdi mdi-phone"></i>
                                             +44 20 7247 8210
                                          </li>
                                          <li>
                                             <i class="mdi mdi-earth"></i>
                                             http://www.yourwebsite.com
                                          </li>
                                          <li>
                                             <i class="mdi mdi-clock-outline"></i>
                                             Today, 12:00 PM - 12:00 AM
                                          </li>
                                          <li>
                                             <i class="mdi mdi-certificate "></i>
                                             Ranked #1 in <?=$st_nm_S?> Hotels
                                          </li>
                                       </ul>
                                       <div class="tagging" onclick="explandTags(this)">
                                          Popular with:
                                          <span>Budget</span>
                                          <span>Foodies</span>
                                          <span>Family</span>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div id="places-todo" class="placestodo-content subtab top_tabs dis-none">
               <div class="content-box ">
                  <div class="mbl-tabnav">
                     <a href="javascript:void(0)" onclick="openDirectTab('places-all')"><i class="mdi mdi-arrow-left"></i></a> 
                     <h6>To Do</h6>
                  </div>
                  <div class="placesection bluesection">
                     <div class="cbox-desc hotels-page np">
                        <div class="tcontent-holder moreinfo-outer">
                           <div class="top-stuff top-graybg" id="all-todo">
                              <div class="more-actions">
                                 <div class="sorting left">
                                    <label>Sort by</label>
                                    <div class="select-holder">
                                       <select class="select2" tabindex="-1" >
                                          <option>Shows</option>
                                          <option>Siting</option>
                                          <option>Attractions</option>
                                       </select>
                                    </div>
                                 </div>
                                 <ul class="tabs tabsnew text-right">
                                    <li class="tab"><a href="javascript:void(0)" class="manageMap" onclick="openMapSection(this)"><i class="zmdi zmdi-pin"></i>Map</a></li>
                                    <li class="tab"><a href="javascript:void(0)" onclick="openListSection(this)"><i class="zmdi zmdi-view-list-alt zmdi-hc-lg"></i>List</a></li>
                                 </ul>
                              </div>
                              <h6>300 things to do in <span><?=$st_nm_S?></span></h6>
                           </div>
                           <div class="places-content-holder">
                              <div class="map-holder">
                                 <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3110.3465133386144!2d-9.167423685010494!3d38.77868997958898!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xd193295d5b45545%3A0x3f9e7b6a5f00e12c!2sPerta!5e0!3m2!1sen!2sin!4v1481089901870" width="600" height="450" frameborder="0" allowfullscreen></iframe>
                                 <a href="javascript:void(0)" class="overlay" onclick="expandMap(this,'#all-todo')"></a>
                                 <a href="javascript:void(0)" class="closelink" onclick="shrinkMap(this)"><i class="mdi mdi-close"></i> Close</a>
                              </div>
                              <div class="list-holder">
                                 <div class="hotel-list">
                                    <ul>
                                       <li>
                                          <div class="hotel-li expandable-holder dealli mobilelist">
                                             <div class="summery-info">
                                                <div class="imgholder himg-box"><img src="images/todo1.png" class="himg" /></div>
                                                <div class="descholder">
                                                   <a href="javascript:void(0)" class="expand-link" onclick="mng_expandable(this,'hasClose')">
                                                      <h4>Dead Sea</h4>
                                                      <div class="clear"></div>
                                                      <div class="reviews-link">
                                                         <span class="review-count">54 reviews</span>
                                                         <span class="checks-holder">
                                                         <i class="zmdi zmdi-check-circle active"></i>
                                                         <i class="zmdi zmdi-check-circle active"></i>
                                                         <i class="zmdi zmdi-check-circle active"></i>
                                                         <i class="zmdi zmdi-check-circle active"></i>
                                                         <i class="zmdi zmdi-check-circle active"></i>
                                                         <label>Excellent - 88/100</label>
                                                         </span>
                                                      </div>
                                                      <span class="address">Adventurous Historical Tours</span>
                                                      <span class="distance-info">
                                                         <p class="dpara"><i class="mdi mdi-format-quote-open"></i>
                                                            Lorem Ipsum is simply dummy text of the printing and typesetting industry...
                                                         </p>
                                                      </span>
                                                      <div class="more-holder">
                                                         <div class="tagging" onclick="explandTags(this)">
                                                            Popular with:
                                                            <span>Families</span>
                                                            <span>Wellness</span>
                                                            <span>Outdoorsy</span>
                                                         </div>
                                                      </div>
                                                   </a>
                                                   <div class="info-action">
                                                      <span class="duration">3 hours ( aprx. )</span>
                                                      <div class="clear"></div>
                                                      <span class="price">JOD 184*</span>
                                                      <div class="clear"></div>
                                                      <a href="javascript:void(0)" class="booknow-btn waves-effect waves-light">Book Now <i class="mdi mdi-chevron-right"></i></a>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="expandable-area">
                                                <a href="javascript:void(0)" class="shrink-link" onclick="mng_expandable(this,'closeIt')"><i class="mdi mdi-close"></i> Close</a>
                                                <div class="clear"></div>
                                                <div class="explandable-tabs">
                                                   <ul class="tabs tabsnew subtab-menu">
                                                      <li class="active tab"><a href="#subtab-details1">Details</a></li>
                                                      <li class="tab"><a data-which="photo" href="#subtab-photos1" data-tab="subtab-photos">Photos</a></li>
                                                      <li><a aria-expanded="true" data-toggle="tab" href="#subtab-reviews1">Reviews</a></li>
                                                   </ul>
                                                   <div class="tab-content">
                                                      <div id="subtab-details1" class="tab-pane fade active in">
                                                         <div class="subdetail-box">
                                                            <div class="infoholder">
                                                               <div class="descholder">
                                                                  <div class="more-holder">
                                                                     <ul class="infoul">
                                                                        <li>
                                                                           <i class="zmdi zmdi-pin"></i>
                                                                           132 Brick Lane | E1 6RU, <?=$st_nm_S?> E1 6RU, <?=$st_nm_S?>
                                                                        </li>
                                                                        <li>
                                                                           <i class="mdi mdi-phone"></i>
                                                                           +44 20 7247 8210
                                                                        </li>
                                                                        <li>
                                                                           <i class="mdi mdi-earth"></i>
                                                                           http://www.yourwebsite.com
                                                                        </li>
                                                                        <li>
                                                                           <i class="mdi mdi-clock-outline"></i>
                                                                           Mon-Fri : 12:00 PM - 10:00 AM
                                                                        </li>
                                                                        <li>
                                                                           <i class="mdi mdi-certificate "></i>
                                                                           Ranked #1 in <?=$st_nm_S?> Hotels
                                                                        </li>
                                                                     </ul>
                                                                     <div class="tagging" onclick="explandTags(this)">
                                                                        Popular with:
                                                                        <span>point of interest</span>
                                                                        <span>establishment</span>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div id="subtab-photos1" class="tab-pane fade subtab-photos">
                                                         <div class="photo-gallery">
                                                            <div class="img-preview">
                                                               <img src="images/post-img1.jpg" />
                                                            </div>
                                                            <div class="thumbs-img">
                                                               <ul>
                                                                  <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img class="himg" src="images/post-img1.jpg" /></a></li>
                                                                  <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img src="images/post-img2.jpg" class="himg" /></a></li>
                                                                  <li><a href="javascript:void(0)" onclick="previewImage(this)" class="vimg-box"><img src="images/post-img3.jpg" class="vimg" /></a></li>
                                                                  <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img src="images/post-img4.jpg" class="himg" /></a></li>
                                                                  <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img src="images/post-img5.jpg" class="himg" /></a></li>
                                                                  <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img class="himg" src="images/post-img1.jpg" /></a></li>
                                                                  <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img src="images/post-img2.jpg" class="himg" /></a></li>
                                                                  <li><a href="javascript:void(0)" onclick="previewImage(this)" class="vimg-box"><img src="images/post-img3.jpg" class="vimg" /></a></li>
                                                                  <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img src="images/post-img4.jpg" class="himg" /></a></li>
                                                                  <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img src="images/post-img5.jpg" class="himg" /></a></li>
                                                               </ul>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div id="subtab-reviews1" class="tab-pane fade">
                                                         <div class="reviews-summery">
                                                            <div class="reviews-people">
                                                               <ul>
                                                                  <li>
                                                                     <div class="reviewpeople-box">
                                                                        <div class="imgholder"><img src="images/people-3.png" /></div>
                                                                        <div class="descholder">
                                                                           <h6>Kelly Mark <span>about 2 weeks ago</span></h6>
                                                                           <div class="stars-holder">
                                                                              <img src="images/filled-star.png" />
                                                                              <img src="images/filled-star.png" />
                                                                              <img src="images/filled-star.png" />
                                                                              <img src="images/blank-star.png" />
                                                                              <img src="images/blank-star.png" />
                                                                           </div>
                                                                           <div class="clear"></div>
                                                                           <p>We enjoyed the lounge and bar at the Ritz where you are offered many choices for drinks and some pretty elaborate looking dishes of food as well.</p>
                                                                        </div>
                                                                     </div>
                                                                  </li>
                                                                  <li>
                                                                     <div class="reviewpeople-box">
                                                                        <div class="imgholder"><img src="images/people-2.png" /></div>
                                                                        <div class="descholder">
                                                                           <h6>John Davior <span>about 8 months ago</span></h6>
                                                                           <div class="stars-holder">
                                                                              <img src="images/filled-star.png" />
                                                                              <img src="images/filled-star.png" />
                                                                              <img src="images/filled-star.png" />
                                                                              <img src="images/filled-star.png" />
                                                                              <img src="images/blank-star.png" />
                                                                           </div>
                                                                           <div class="clear"></div>
                                                                           <p>If you want a fancy London experience than The Ritz is where you need to go! At least budget for High Tea!</p>
                                                                        </div>
                                                                     </div>
                                                                  </li>
                                                                  <li>
                                                                     <div class="reviewpeople-box">
                                                                        <div class="imgholder"><img src="images/people-1.png" /></div>
                                                                        <div class="descholder">
                                                                           <h6>Joe Doe <span>about 11 months ago</span></h6>
                                                                           <div class="stars-holder">
                                                                              <img src="images/filled-star.png" />
                                                                              <img src="images/filled-star.png" />
                                                                              <img src="images/filled-star.png" />
                                                                              <img src="images/blank-star.png" />
                                                                              <img src="images/blank-star.png" />
                                                                           </div>
                                                                           <div class="clear"></div>
                                                                           <p>I am not at all sure this is the best hotel in London, but it does deserve the reputation as one of the most glamourous.</p>
                                                                        </div>
                                                                     </div>
                                                                  </li>
                                                               </ul>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </li>
                                       <li>
                                          <div class="hotel-li expandable-holder dealli mobilelist">
                                             <div class="summery-info">
                                                <div class="imgholder himg-box"><img src="images/todo2.png" class="himg" /></div>
                                                <div class="descholder">
                                                   <a href="javascript:void(0)" class="expand-link" onclick="mng_expandable(this,'hasClose')">
                                                      <h4>Temple of Hercules</h4>
                                                      <div class="clear"></div>
                                                      <div class="reviews-link">
                                                         <span class="review-count">54 reviews</span>
                                                         <span class="checks-holder">
                                                         <i class="zmdi zmdi-check-circle active"></i>
                                                         <i class="zmdi zmdi-check-circle active"></i>
                                                         <i class="zmdi zmdi-check-circle active"></i>
                                                         <i class="zmdi zmdi-check-circle active"></i>
                                                         <i class="zmdi zmdi-check-circle active"></i>
                                                         <label>Excellent - 88/100</label>
                                                         </span>
                                                      </div>
                                                      <span class="address">Adventurous Historical Tours</span>
                                                      <span class="distance-info">
                                                         <p class="dpara"><i class="mdi mdi-format-quote-open"></i>
                                                            Lorem Ipsum is simply dummy text of the printing and typesetting industry...
                                                         </p>
                                                      </span>
                                                      <div class="more-holder">
                                                         <div class="tagging" onclick="explandTags(this)">
                                                            Popular with:
                                                            <span>Families</span>
                                                            <span>Wellness</span>
                                                            <span>Outdoorsy</span>
                                                         </div>
                                                      </div>
                                                   </a>
                                                   <div class="info-action">
                                                      <span class="duration">3 hours ( aprx. )</span>
                                                      <div class="clear"></div>
                                                      <span class="price">JOD 184*</span>
                                                      <div class="clear"></div>
                                                      <a href="javascript:void(0)" class="booknow-btn waves-effect waves-light">Book Now <i class="mdi mdi-chevron-right"></i></a>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="expandable-area">
                                                <a href="javascript:void(0)" class="shrink-link" onclick="mng_expandable(this,'closeIt')"><i class="mdi mdi-close"></i> Close</a>
                                                <div class="clear"></div>
                                                <div class="explandable-tabs">
                                                   <ul class="tabs tabsnew subtab-menu">
                                                      <li class="active tab"><a href="#subtab-details1">Details</a></li>
                                                      <li class="tab"><a data-which="photo" href="#subtab-photos1" data-tab="subtab-photos">Photos</a></li>
                                                      <li><a aria-expanded="true" data-toggle="tab" href="#subtab-reviews1">Reviews</a></li>
                                                   </ul>
                                                   <div class="tab-content">
                                                      <div id="subtab-details1" class="tab-pane fade active in">
                                                         <div class="subdetail-box">
                                                            <div class="infoholder">
                                                               <div class="descholder">
                                                                  <div class="more-holder">
                                                                     <ul class="infoul">
                                                                        <li>
                                                                           <i class="zmdi zmdi-pin"></i>
                                                                           132 Brick Lane | E1 6RU, <?=$st_nm_S?> E1 6RU, <?=$st_nm_S?>
                                                                        </li>
                                                                        <li>
                                                                           <i class="mdi mdi-phone"></i>
                                                                           +44 20 7247 8210
                                                                        </li>
                                                                        <li>
                                                                           <i class="mdi mdi-earth"></i>
                                                                           http://www.yourwebsite.com
                                                                        </li>
                                                                        <li>
                                                                           <i class="mdi mdi-clock-outline"></i>
                                                                           Mon-Fri : 12:00 PM - 10:00 AM
                                                                        </li>
                                                                        <li>
                                                                           <i class="mdi mdi-certificate "></i>
                                                                           Ranked #1 in <?=$st_nm_S?> Hotels
                                                                        </li>
                                                                     </ul>
                                                                     <div class="tagging" onclick="explandTags(this)">
                                                                        Popular with:
                                                                        <span>point of interest</span>
                                                                        <span>establishment</span>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div id="subtab-photos1" class="tab-pane fade subtab-photos">
                                                         <div class="photo-gallery">
                                                            <div class="img-preview">
                                                               <img src="images/post-img1.jpg" />
                                                            </div>
                                                            <div class="thumbs-img">
                                                               <ul>
                                                                  <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img class="himg" src="images/post-img1.jpg" /></a></li>
                                                                  <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img src="images/post-img2.jpg" class="himg" /></a></li>
                                                                  <li><a href="javascript:void(0)" onclick="previewImage(this)" class="vimg-box"><img src="images/post-img3.jpg" class="vimg" /></a></li>
                                                                  <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img src="images/post-img4.jpg" class="himg" /></a></li>
                                                                  <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img src="images/post-img5.jpg" class="himg" /></a></li>
                                                                  <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img class="himg" src="images/post-img1.jpg" /></a></li>
                                                                  <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img src="images/post-img2.jpg" class="himg" /></a></li>
                                                                  <li><a href="javascript:void(0)" onclick="previewImage(this)" class="vimg-box"><img src="images/post-img3.jpg" class="vimg" /></a></li>
                                                                  <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img src="images/post-img4.jpg" class="himg" /></a></li>
                                                                  <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img src="images/post-img5.jpg" class="himg" /></a></li>
                                                               </ul>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div id="subtab-reviews1" class="tab-pane fade">
                                                         <div class="reviews-summery">
                                                            <div class="reviews-people">
                                                               <ul>
                                                                  <li>
                                                                     <div class="reviewpeople-box">
                                                                        <div class="imgholder"><img src="images/people-3.png" /></div>
                                                                        <div class="descholder">
                                                                           <h6>Kelly Mark <span>about 2 weeks ago</span></h6>
                                                                           <div class="stars-holder">
                                                                              <img src="images/filled-star.png" />
                                                                              <img src="images/filled-star.png" />
                                                                              <img src="images/filled-star.png" />
                                                                              <img src="images/blank-star.png" />
                                                                              <img src="images/blank-star.png" />
                                                                           </div>
                                                                           <div class="clear"></div>
                                                                           <p>We enjoyed the lounge and bar at the Ritz where you are offered many choices for drinks and some pretty elaborate looking dishes of food as well.</p>
                                                                        </div>
                                                                     </div>
                                                                  </li>
                                                                  <li>
                                                                     <div class="reviewpeople-box">
                                                                        <div class="imgholder"><img src="images/people-2.png" /></div>
                                                                        <div class="descholder">
                                                                           <h6>John Davior <span>about 8 months ago</span></h6>
                                                                           <div class="stars-holder">
                                                                              <img src="images/filled-star.png" />
                                                                              <img src="images/filled-star.png" />
                                                                              <img src="images/filled-star.png" />
                                                                              <img src="images/filled-star.png" />
                                                                              <img src="images/blank-star.png" />
                                                                           </div>
                                                                           <div class="clear"></div>
                                                                           <p>If you want a fancy London experience than The Ritz is where you need to go! At least budget for High Tea!</p>
                                                                        </div>
                                                                     </div>
                                                                  </li>
                                                                  <li>
                                                                     <div class="reviewpeople-box">
                                                                        <div class="imgholder"><img src="images/people-1.png" /></div>
                                                                        <div class="descholder">
                                                                           <h6>Joe Doe <span>about 11 months ago</span></h6>
                                                                           <div class="stars-holder">
                                                                              <img src="images/filled-star.png" />
                                                                              <img src="images/filled-star.png" />
                                                                              <img src="images/filled-star.png" />
                                                                              <img src="images/blank-star.png" />
                                                                              <img src="images/blank-star.png" />
                                                                           </div>
                                                                           <div class="clear"></div>
                                                                           <p>I am not at all sure this is the best hotel in London, but it does deserve the reputation as one of the most glamourous.</p>
                                                                        </div>
                                                                     </div>
                                                                  </li>
                                                               </ul>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </li>
                                       <li>
                                          <div class="hotel-li expandable-holder dealli mobilelist">
                                             <div class="summery-info">
                                                <div class="imgholder himg-box"><img src="images/todo3.png" class="himg" /></div>
                                                <div class="descholder">
                                                   <a href="javascript:void(0)" class="expand-link" onclick="mng_expandable(this,'hasClose')">
                                                      <h4>Roman Theatre</h4>
                                                      <div class="clear"></div>
                                                      <div class="reviews-link">
                                                         <span class="review-count">54 reviews</span>
                                                         <span class="checks-holder">
                                                         <i class="zmdi zmdi-check-circle active"></i>
                                                         <i class="zmdi zmdi-check-circle active"></i>
                                                         <i class="zmdi zmdi-check-circle active"></i>
                                                         <i class="zmdi zmdi-check-circle active"></i>
                                                         <i class="zmdi zmdi-check-circle active"></i>
                                                         <label>Excellent - 88/100</label>
                                                         </span>
                                                      </div>
                                                      <span class="address">Adventurous Historical Tours</span>
                                                      <span class="distance-info">
                                                         <p class="dpara"><i class="mdi mdi-format-quote-open"></i>
                                                            Lorem Ipsum is simply dummy text of the printing and typesetting industry...
                                                         </p>
                                                      </span>
                                                      <div class="more-holder">
                                                         <div class="tagging" onclick="explandTags(this)">
                                                            Popular with:
                                                            <span>Families</span>
                                                            <span>Wellness</span>
                                                            <span>Outdoorsy</span>
                                                         </div>
                                                      </div>
                                                   </a>
                                                   <div class="info-action">
                                                      <span class="duration">3 hours ( aprx. )</span>
                                                      <div class="clear"></div>
                                                      <span class="price">JOD 184*</span>
                                                      <div class="clear"></div>
                                                      <a href="javascript:void(0)" class="booknow-btn waves-effect waves-light">Book Now <i class="mdi mdi-chevron-right"></i></a>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="expandable-area">
                                                <a href="javascript:void(0)" class="shrink-link" onclick="mng_expandable(this,'closeIt')"><i class="mdi mdi-close"></i> Close</a>
                                                <div class="clear"></div>
                                                <div class="explandable-tabs">
                                                   <ul class="tabs tabsnew subtab-menu">
                                                      <li class="active tab"><a href="#subtab-details1">Details</a></li>
                                                      <li class="tab"><a data-which="photo" href="#subtab-photos1" data-tab="subtab-photos">Photos</a></li>
                                                      <li><a aria-expanded="true" data-toggle="tab" href="#subtab-reviews1">Reviews</a></li>
                                                   </ul>
                                                   <div class="tab-content">
                                                      <div id="subtab-details1" class="tab-pane fade active in">
                                                         <div class="subdetail-box">
                                                            <div class="infoholder">
                                                               <div class="descholder">
                                                                  <div class="more-holder">
                                                                     <ul class="infoul">
                                                                        <li>
                                                                           <i class="zmdi zmdi-pin"></i>
                                                                           132 Brick Lane | E1 6RU, <?=$st_nm_S?> E1 6RU, <?=$st_nm_S?>
                                                                        </li>
                                                                        <li>
                                                                           <i class="mdi mdi-phone"></i>
                                                                           +44 20 7247 8210
                                                                        </li>
                                                                        <li>
                                                                           <i class="mdi mdi-earth"></i>
                                                                           http://www.yourwebsite.com
                                                                        </li>
                                                                        <li>
                                                                           <i class="mdi mdi-clock-outline"></i>
                                                                           Mon-Fri : 12:00 PM - 10:00 AM
                                                                        </li>
                                                                        <li>
                                                                           <i class="mdi mdi-certificate "></i>
                                                                           Ranked #1 in <?=$st_nm_S?> Hotels
                                                                        </li>
                                                                     </ul>
                                                                     <div class="tagging" onclick="explandTags(this)">
                                                                        Popular with:
                                                                        <span>point of interest</span>
                                                                        <span>establishment</span>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div id="subtab-photos1" class="tab-pane fade subtab-photos">
                                                         <div class="photo-gallery">
                                                            <div class="img-preview">
                                                               <img src="images/post-img1.jpg" />
                                                            </div>
                                                            <div class="thumbs-img">
                                                               <ul>
                                                                  <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img class="himg" src="images/post-img1.jpg" /></a></li>
                                                                  <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img src="images/post-img2.jpg" class="himg" /></a></li>
                                                                  <li><a href="javascript:void(0)" onclick="previewImage(this)" class="vimg-box"><img src="images/post-img3.jpg" class="vimg" /></a></li>
                                                                  <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img src="images/post-img4.jpg" class="himg" /></a></li>
                                                                  <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img src="images/post-img5.jpg" class="himg" /></a></li>
                                                                  <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img class="himg" src="images/post-img1.jpg" /></a></li>
                                                                  <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img src="images/post-img2.jpg" class="himg" /></a></li>
                                                                  <li><a href="javascript:void(0)" onclick="previewImage(this)" class="vimg-box"><img src="images/post-img3.jpg" class="vimg" /></a></li>
                                                                  <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img src="images/post-img4.jpg" class="himg" /></a></li>
                                                                  <li><a href="javascript:void(0)" onclick="previewImage(this)" class="himg-box"><img src="images/post-img5.jpg" class="himg" /></a></li>
                                                               </ul>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div id="subtab-reviews1" class="tab-pane fade">
                                                         <div class="reviews-summery">
                                                            <div class="reviews-people">
                                                               <ul>
                                                                  <li>
                                                                     <div class="reviewpeople-box">
                                                                        <div class="imgholder"><img src="images/people-3.png" /></div>
                                                                        <div class="descholder">
                                                                           <h6>Kelly Mark <span>about 2 weeks ago</span></h6>
                                                                           <div class="stars-holder">
                                                                              <img src="images/filled-star.png" />
                                                                              <img src="images/filled-star.png" />
                                                                              <img src="images/filled-star.png" />
                                                                              <img src="images/blank-star.png" />
                                                                              <img src="images/blank-star.png" />
                                                                           </div>
                                                                           <div class="clear"></div>
                                                                           <p>We enjoyed the lounge and bar at the Ritz where you are offered many choices for drinks and some pretty elaborate looking dishes of food as well.</p>
                                                                        </div>
                                                                     </div>
                                                                  </li>
                                                                  <li>
                                                                     <div class="reviewpeople-box">
                                                                        <div class="imgholder"><img src="images/people-2.png" /></div>
                                                                        <div class="descholder">
                                                                           <h6>John Davior <span>about 8 months ago</span></h6>
                                                                           <div class="stars-holder">
                                                                              <img src="images/filled-star.png" />
                                                                              <img src="images/filled-star.png" />
                                                                              <img src="images/filled-star.png" />
                                                                              <img src="images/filled-star.png" />
                                                                              <img src="images/blank-star.png" />
                                                                           </div>
                                                                           <div class="clear"></div>
                                                                           <p>If you want a fancy London experience than The Ritz is where you need to go! At least budget for High Tea!</p>
                                                                        </div>
                                                                     </div>
                                                                  </li>
                                                                  <li>
                                                                     <div class="reviewpeople-box">
                                                                        <div class="imgholder"><img src="images/people-1.png" /></div>
                                                                        <div class="descholder">
                                                                           <h6>Joe Doe <span>about 11 months ago</span></h6>
                                                                           <div class="stars-holder">
                                                                              <img src="images/filled-star.png" />
                                                                              <img src="images/filled-star.png" />
                                                                              <img src="images/filled-star.png" />
                                                                              <img src="images/blank-star.png" />
                                                                              <img src="images/blank-star.png" />
                                                                           </div>
                                                                           <div class="clear"></div>
                                                                           <p>I am not at all sure this is the best hotel in London, but it does deserve the reputation as one of the most glamourous.</p>
                                                                        </div>
                                                                     </div>
                                                                  </li>
                                                               </ul>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </li>
                                    </ul>
                                    <div class="pagination">
                                       <div class="link-holder">
                                          <a href="javascript:void(0)"><i class="mdi mdi-arrow-left-bold-circle"></i> Prev</a>
                                       </div>
                                       <div class="link-holder">
                                          <a href="javascript:void(0)">Next <i class="mdi mdi-arrow-right-bold-circle"></i></a>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="moreinfo-box">
                              <a href="javascript:void(0)" onclick="closePlacesMoreInfo(this)" class="backarrow"><i class="mdi mdi-arrow-left-bold-circle"></i></a>
                              <div class="infoholder nice-scroll">
                                 <div class="imgholder"><img src="images/hotel1.png" /></div>
                                 <div class="descholder">
                                    <h4>The Guest House</h4>
                                    <div class="clear"></div>
                                    <div class="reviews-link">
                                       <span class="checks-holder">
                                       <i class="mdi mdi-star active"></i>
                                       <i class="mdi mdi-star active"></i>
                                       <i class="mdi mdi-star active"></i>
                                       <i class="mdi mdi-star active"></i>
                                       <i class="mdi mdi-star"></i>
                                       <label>34 Reviews</label>
                                       </span>
                                    </div>
                                    <span class="distance-info">Middle Eastem &amp; African, Mediterranean</span>
                                    <div class="clear"></div>
                                    <div class="more-holder">
                                       <ul class="infoul">
                                          <li>
                                             <i class="zmdi zmdi-pin"></i>
                                             132 Brick Lane | E1 6RU, <?=$st_nm_S?> E1 6RU, <?=$st_nm_S?>
                                          </li>
                                          <li>
                                             <i class="mdi mdi-phone"></i>
                                             +44 20 7247 8210
                                          </li>
                                          <li>
                                             <i class="mdi mdi-earth"></i>
                                             http://www.yourwebsite.com
                                          </li>
                                          <li>
                                             <i class="mdi mdi-clock-outline"></i>
                                             Today, 12:00 PM - 12:00 AM
                                          </li>
                                          <li>
                                             <i class="mdi mdi-certificate "></i>
                                             Ranked #1 in <?=$st_nm_S?> Hotels
                                          </li>
                                       </ul>
                                       <div class="tagging" onclick="explandTags(this)">
                                          Popular with:
                                          <span>Budget</span>
                                          <span>Foodies</span>
                                          <span>Family</span>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <?php include('common/gen_wall_col.php'); ?>
   </div>
</div>
</div>
</div>
</div>

</div>

<!-- Discuss-->
<!-- compose tool box modal -->
<div id="compose_discus" class="modal compose_tool_box post-popup custom_modal main_modal new-wall-post set_re_height compose_discus_popup">
<div class="hidden_header">  
<div class="content_header">
<button class="close_span cancel_poup waves-effect">
<i class="mdi mdi-close mdi-20px compose_discard_popup"></i>
</button>
<p class="modal_header_xs">Write Post</p>
<a type="button" class="post_btn action_btn post_btn_xs close_modal waves-effect"  onclick="verify()">Post</a>
</div>
</div>
<div class="modal-content">
<div class="new-post active">
<div class="top-stuff">
   <!--<div class="side-user">-->
   <div class="postuser-info">
      <span class="img-holder"><img class="circle" src="images/demo-profile.jpg" /></span>
      <div class="desc-holder">
         <span class="profile_name">Nimish Parekh</span>
         <label id="tag_person" class="tag_person_new"></label>
         <div class="public_dropdown_container">
            <a class="dropdown_text dropdown-button-left normalpostcreateprivacylabel" href="javascript:void(0)" onclick="privacymodal(this)" data-modeltag="normalpostcreateprivacylabel" data-fetch="no" data-label="normalpost">
               <span id="post_privacy2" class="post_privacy_label">Public</span>
               <i class="zmdi zmdi-caret-down"></i>
            </a>
         </div>
      </div>
   </div>
   <div class="settings-icon">
      <a class="dropdown-button "  href="javascript:void(0)" data-activates="newpost_settings">
      <i class="zmdi zmdi-more"></i>
      </a>
      <ul id="newpost_settings" class="dropdown-content custom_dropdown">
         <li>
            <a href="javascript:void(0)">
            <input type="checkbox" id="toolbox_disable_sharing" />
            <label for="toolbox_disable_sharing">Disable Sharing</label>
            </a>
         </li>
         <li>
            <a href="javascript:void(0)">
            <input type="checkbox" id="toolbox_disable_comments" />
            <label for="toolbox_disable_comments">Disable Comments</label>
            </a>
         </li>
         <li>
            <a  onclick="clearPost()">Clear Post</a>
         </li>
      </ul>
   </div>
</div>
<div class="clear"></div>
<div class="scroll_div">
   <div class="npost-content">
      <div class="post-mcontent">
         <div class="npost-title title_post_container">                                    
            <input type="text" class="title" placeholder="Your tip title">                                 
         </div>
         <div class="clear"></div>
         <div class="desc">
            <textarea id="new_post_comment" placeholder="What's new?" class="materialize-textarea comment_textarea new_post_comment"></textarea>
         </div>
         <div class="post-photos">
            <div class="img-row">
            </div>
         </div>
         <div class="post-tag">
            <div class="areatitle">With</div>
            <div class="areadesc">
               <input type="text" class="ptag" placeholder="Who are you with?"/>
            </div>
         </div>
      </div>
   </div>
</div>
</div>
</div>
<div class="modal-footer">
<div class="new-post active">
<div class="post-bcontent">
   <div class="footer_icon_container">
      <button class="comment_footer_icon waves-effect" id="compose_uploadphotomodalAction">
      <i class="zmdi zmdi-camera"></i>
      </button>
      <button class="comment_footer_icon waves-effect" id="compose_addpersonAction">
      <i class="zmdi zmdi-account"></i>
      </button>
      <button class="comment_footer_icon compose_titleAction waves-effect" id="compose_titleAction">
      <img src="images/addtitleBl.png">
      </button>
   </div>
   <div class="public_dropdown_container_xs">
      <a class="dropdown_text dropdown-button-left normalpostcreateprivacylabel" href="javascript:void(0)" onclick="privacymodal(this)" data-modeltag="normalpostcreateprivacylabel" data-fetch="no" data-label="normalpost">
         <span id="post_privacy2" class="post_privacy_label">Public</span>
         <i class="zmdi zmdi-caret-down"></i>
      </a>
   </div>
   <div class="post-bholder">
      <div class="post-loader"><img src="images/home-loader.gif"/></div>
      <div class="hidden_xs">
         <a href="javascript:void(0)" class="btngen-center-align close_modal open_discard_modal waves-effect">cancel</a>
         <a href="javascript:void(0)" class="btngen-center-align waves-effect btn-flat disabled submit">Post</a>
      </div>
   </div>
</div>
</div>
</div>
</div>
<!-- Discuss-->
<!--Edit Discuss-->
<!-- compose tool box modal -->
<div id="edit_discus" class="modal compose_tool_box post-popup custom_modal main_modal new-wall-post set_re_height">
<div class="hidden_header">
<div class="content_header">
<button class="close_span cancel_poup waves-effect">
<i class="mdi mdi-close mdi-20px compose_discard_popup"></i>
</button>
<p class="modal_header_xs">Edit post</p>
<a type="button" class="post_btn action_btn post_btn_xs postbtn savebtn active_post_btn close_modal" onclick="verify()">Save</a>
</div>
</div>
<div class="modal-content">
<div class="new-post active">
<div class="top-stuff">
   <div class="postuser-info">
      <span class="img-holder"><img class="circle" src="images/demo-profile.jpg" /></span>
      <div class="desc-holder">
         <p class="profile_name">User name</p>
         <label id="edit_tag_person"></label>
         <div class="public_dropdown_container">
             <a class="dropdown_text dropdown-button editpostcreateprivacylabel" onclick="privacymodal(this)" href="javascript:void(0)" data-modeltag="editpostcreateprivacylabel" data-fetch="yes" data-label="editpost">
               <span id="post_privacy2" class="post_privacy_label">Public</span>
               <i class="zmdi zmdi-caret-up zmdi-hc-lg"></i>
            </a>
         </div>
      </div>
   </div>
   <div class="settings-icon comment_setting_icon">
      <a class="dropdown-button " href="javascript:void(0)" data-activates="new_edit_btn1">
      <i class="zmdi zmdi-more"></i>
      </a>
      <ul id="new_edit_btn1" class="dropdown-content custom_dropdown">
         <li>
            <a href="javascript:void(0)">
            <input type="checkbox" id="toolbox_disable_sharing" />
            <label for="toolbox_disable_sharing">Disable Sharing</label>
            </a>
         </li>
         <li>
            <a href="javascript:void(0)" class="savepost-link">
            <input type="checkbox" id="toolbox_disable_comments" />
            <label for="toolbox_disable_comments">Disable Comments</label>
            </a>
         </li>
      </ul>
   </div>
</div>
<div class="clear"></div>
<div class="scroll_div">
   <div class="npost-content">
      <div class="post-mcontent">
         <div class="compose_post_title title_post_container">
            <input placeholder="Title of this post" id="post_edittitle" class="post_title" type="text" class="validate">
         </div>
         <div class="clear"></div>
         <div class="desc post_comment_box">
            <textarea id="new_editpost_comment" placeholder="What's new?" class="materialize-textarea comment_textarea"></textarea>
         </div>
         <div class="post-photos">
            <div class="img-row">
            </div>
         </div> 
      </div>
   </div>
</div>
</div>
</div>
<div class="modal-footer">
<div class="new-post active">
<div class="post-bcontent">
   <div class="footer_icon_container">
      <button class="comment_footer_icon waves-effect" id="compose_edituploadphotomodalAction">
      <i class="zmdi zmdi-camera"></i>
      </button>
      <button class="comment_footer_icon waves-effect" id="compose_addpersonAction">
      <i class="zmdi zmdi-account"></i>
      </button>
      <button class="comment_footer_icon compose_titleAction waves-effect" id="compose_edittitleAction">
      <img src="images/addtitleBl.png">
      </button>
   </div>
   <div class="public_dropdown_container_xs">
      <a class="dropdown_text dropdown-button editpostcreateprivacylabel" onclick="privacymodal(this)" href="javascript:void(0)" data-modeltag="editpostcreateprivacylabel" data-fetch="yes" data-label="editpost">
         <span id="post_privacy2" class="post_privacy_label">Public</span>
         <i class="zmdi zmdi-caret-up zmdi-hc-lg"></i>
      </a>
   </div>
   <div class="post-bholder">
      <div class="post-loader"><img src="images/home-loader.gif"/></div>
      <div class="hidden_xs">
         <a class="btngen-center-align close_modal open_discard_modal waves-effect">cancel</a>
         <a class="btngen-center-align waves-effect">Save</a>
      </div>
   </div>
</div>
</div>
</div>
</div>
<!--Edit Discuss-->
<!-- review-->
<!-- compose tool box modal -->
<div id="compose_newreview" class="modal compose_tool_box post-popup custom_modal main_modal new-wall-post set_re_height compose_newreview_popup">
<div class="hidden_header">
<div class="content_header">
<button class="close_span cancel_poup  waves-effect">
<i class="mdi mdi-close mdi-20px compose_discard_popup"></i>
</button>
<p class="modal_header_xs">Write review</p>
<a type="button" class="post_btn action_btn post_btn_xs close_modal waves-effect"  onclick="verify()">Post</a>
</div>
</div>
<div class="modal-content">
<div class="new-post active">
<div class="top-stuff">
   <!--<div class="side-user">-->
   <div class="postuser-info">
      <span class="img-holder"><img class="circle" src="images/demo-profile.jpg" /></span>
      <div class="desc-holder">
         <span class="profile_name">Nimish Parekh</span>
         <label id="tag_person" class="tag_person_new"></label>
         <div class="public_dropdown_container">
            <a class="dropdown_text dropdown-button-left" href="javascript:void(0)" data-activates="post_privacy_compose">
               <span>
                  <!--<i class="zmdi zmdi-globe privacy_icon"></i>-->
                  Public
               </span>
               <i class="zmdi zmdi-caret-down"></i>
            </a>
            <ul id="post_privacy_compose" class="dropdown-privacy dropdown-content custom_dropdown ">
               <li>
                  <a href="javascript:void(0)">
                  Private
                  </a>
               </li>
               <li>
                  <a href="javascript:void(0)">
                  Connections
                  </a>
               </li>
               <li>
                  <a href="javascript:void(0)">
                  Public
                  </a>
               </li>
               <li>
                  <a href="javascript:void(0)" onclick="addpersonmodal()">
                  Custom
                  </a>
               </li>
            </ul>
         </div>
      </div>
   </div>
   <div class="settings-icon">
      <a href="javascript:void(0)" onclick="clearPost()">
      <i class="zmdi zmdi-refresh-alt"></i>
      </a>
   </div>
</div>
<div class="clear"></div>
<div class="scroll_div">
   <div class="npost-content">
      <div class="post-mcontent">
         <div class="npost-title title_post_container">                                
            <input class="title" placeholder="Your tip title" type="text">
         </div>
         <div class="clear"></div> 
         <div class="rating-stars setRating cus_starts">
            <span>Your Rating</span>&nbsp;&nbsp; 
            <span onmouseout="resettostart(this)">
               <i class="mdi mdi-star ratecls1 ratecls2 ratecls3 ratecls4 ratecls5" data-value="1" onmouseover="ratingJustOver(this)" onclick="pickrate(this,1)"></i> 
               <i class="mdi mdi-star ratecls2 ratecls3 ratecls4 ratecls5" data-value="2" onmouseover="ratingJustOver(this)" onclick="pickrate(this,2)"></i>
               <i class="mdi mdi-star ratecls3 ratecls4 ratecls5" data-value="3" onmouseover="ratingJustOver(this)" onclick="pickrate(this,3)"></i>
               <i class="mdi mdi-star ratecls4 ratecls5" data-value="4" onmouseover="ratingJustOver(this)" onclick="pickrate(this,4)"></i>
               <i class="mdi mdi-star ratecls5" data-value="5" onmouseover="ratingJustOver(this)" onclick="pickrate(this,5)"></i>
            </span>&nbsp;&nbsp;
            <span class="star-text">Better</span>
         </div>
         <div class="clear"></div>
         <div class="desc">
            <textarea id="new_post_comment" placeholder="Write your review about <?=$st_nm_S?>" class="materialize-textarea comment_textarea new_post_comment"></textarea>
         </div>
         <div class="post-photos">
            <div class="img-row">
               <div class="img-box">
                  <div class="custom-file addimg-box">
                     <div class="addimg-icon"><i class="mdi mdi-plus"></i></div>
                     <input class="upload custom-upload remove-custom-upload" title="Choose a file to upload" required="" data-class=".post-photos .img-row" multiple="true" type="file">
                  </div>
               </div>
            </div>
         </div>
         <div class="post-tag">
            <div class="areatitle">With</div>
            <div class="areadesc">
               <input type="text" class="ptag" placeholder="Who are you with?"/>
            </div>
         </div>
      </div>
   </div>
</div>
</div>
</div>
<div class="modal-footer">
<div class="new-post active">
<div class="post-bcontent">
   <div class="footer_icon_container">
      <a href="javascript:void(0)" class="comment_footer_icon waves-effect" id="">
      <input type="file" id="upload_pic" name="upload" class="upload_pic_input upload custom-upload" title="Choose a file to upload" required="" data-class=".main_modal.open .post-photos .img-row" multiple="">
      <i class="zmdi zmdi-hc-lg zmdi-camera"></i>
      </a>
      <button class="comment_footer_icon waves-effect" id="compose_addpersonAction">
      <i class="zmdi zmdi-hc-lg zmdi-account"></i>
      </button>
      <button class="comment_footer_icon waves-effect compose_titleAction" id="compose_titleAction">
      <img src="images/addtitleBl.png">
      </button>
   </div>
   <div class="public_dropdown_container_xs">
      <a class="dropdown_text dropdown-button" href="javascript:void(0)" data-activates="post_privacy_compose_xs">
      <span>Public</span>
      <i class="zmdi zmdi-caret-up zmdi-hc-lg"></i>
      </a>
      <ul id="post_privacy_compose_xs" class="dropdown-privacy dropdown-content custom_dropdown public_dropdown_xs">
         <li>
            <a href="javascript:void(0)">
            Private
            </a>
         </li>
         <li>
            <a href="javascript:void(0)">
            Connections
            </a>
         </li>
         <li>
            <a href="javascript:void(0)" onclick="addpersonmodal()">
            Custom
            </a>
         </li>
         <li>
            <a href="javascript:void(0)">
            Public
            </a>
         </li>
      </ul>
   </div>
   <div class="post-bholder">
      <div class="post-loader"><img src="images/home-loader.gif"/></div>
      <div class="hidden_xs">
         <a class="btngen-center-align  close_modal open_discard_modal waves-effect">cancel</a>
         <a type="button" class="btngen-center-align waves-effect">Post</a>
      </div>
   </div>
</div>
</div>
</div>
</div>
<!-- review-->
<!--edit review-->
<!-- compose tool box modal -->
<div id="edit_newreview" class="modal compose_tool_box post-popup custom_modal main_modal new-wall-post set_re_height edit_newreview_new">
<div class="hidden_header">
<div class="content_header">
<button class="close_span cancel_poup  waves-effect">
<i class="mdi mdi-close mdi-20px compose_discard_popup"></i>
</button>
<p class="modal_header_xs">Write review</p>
<a type="button" class="post_btn action_btn post_btn_xs close_modal waves-effect"  onclick="verify()">Post</a>
</div>
</div>
<div class="modal-content">
<div class="new-post active">
<div class="top-stuff">
   <!--<div class="side-user">-->
   <div class="postuser-info">
      <span class="img-holder"><img class="circle" src="images/demo-profile.jpg" /></span>
      <div class="desc-holder">
         <span class="profile_name">Nimish Parekh</span>
         <label id="tag_person" class="tag_person_new"></label>
         <div class="public_dropdown_container">
            <a class="dropdown_text dropdown-button-left" href="javascript:void(0)" data-activates="post_privacy_compose">
               <span>
                  <!--<i class="zmdi zmdi-globe privacy_icon"></i>-->
                  Public
               </span>
               <i class="zmdi zmdi-caret-down"></i>
            </a>
            <ul id="post_privacy_compose" class="dropdown-privacy dropdown-content custom_dropdown ">
               <li>
                  <a href="javascript:void(0)">
                  Private
                  </a>
               </li>
               <li>
                  <a href="javascript:void(0)">
                  Connections
                  </a>
               </li>
               <li>
                  <a href="javascript:void(0)">
                  Public
                  </a>
               </li>
               <li>
                  <a href="javascript:void(0)" onclick="addpersonmodal()">
                  Custom
                  </a>
               </li>
            </ul>
         </div>
      </div>
   </div>
   <div class="settings-icon">
      <a href="javascript:void(0)" onclick="clearPost()">
      <i class="zmdi zmdi-refresh-alt"></i>
      </a>
   </div>
</div>
<div class="clear"></div>
<div class="scroll_div">
   <div class="npost-content">
      <div class="post-mcontent">
         <div class="npost-title title_post_container">                                
            <input class="title" placeholder="Your tip title" type="text">
         </div>
         <div class="clear"></div>
         <div class="rating-stars setRating cus_starts" onmouseout="setStarText(this,6)">
            <span>Your Rating&nbsp;</span>
            <i class="mdi mdi-star active" data-value="1" onmouseover="setStarText(this,1)" onclick="setRating(this,1)"></i>
            <i class="mdi mdi-star" data-value="2" onmouseover="setStarText(this,2)" onclick="setRating(this,2)"></i>
            <i class="mdi mdi-star" data-value="3" onmouseover="setStarText(this,3)" onclick="setRating(this,3)"></i>
            <i class="mdi mdi-star" data-value="4" onmouseover="setStarText(this,4)" onclick="setRating(this,4)"></i>
            <i class="mdi mdi-star" data-value="5" onmouseover="setStarText(this,5)" onclick="setRating(this,5)"></i>
            &nbsp;&nbsp;<span class="star-text"> Poor</span>
         </div>
         <div class="clear"></div>
         <div class="desc">
            <textarea id="new_post_comment" placeholder="Write your review about <?=$st_nm_S?>" class="materialize-textarea comment_textarea new_post_comment"></textarea>
         </div>
         <div class="post-photos">
            <div class="img-row">
               <div class="img-box">
                  <div class="custom-file addimg-box">
                     <div class="addimg-icon"><i class="mdi mdi-plus"></i></div>
                     <input class="upload custom-upload remove-custom-upload" title="Choose a file to upload" required="" data-class=".post-photos .img-row" multiple="true" type="file">
                  </div>
               </div>
            </div>
         </div>
         <div class="post-tag">
            <div class="areatitle">With</div>
            <div class="areadesc">
               <input type="text" class="ptag" placeholder="Who are you with?"/>
            </div>
         </div>
      </div>
   </div>
</div>
</div>
</div>
<div class="modal-footer">
<div class="new-post active">
<div class="post-bcontent">
   <div class="footer_icon_container">
      <a href="javascript:void(0)" class="comment_footer_icon waves-effect" id="">
      <input type="file" id="upload_pic" name="upload" class="upload_pic_input upload custom-upload" title="Choose a file to upload" required="" data-class=".main_modal.open .post-photos .img-row" multiple="">
      <i class="zmdi zmdi-hc-lg zmdi-camera"></i>
      </a>
      <button class="comment_footer_icon waves-effect" id="compose_addpersonAction">
      <i class="zmdi zmdi-hc-lg zmdi-account"></i>
      </button>
      <button class="comment_footer_icon waves-effect compose_titleAction" id="compose_titleAction">
      <img src="images/addtitleBl.png">
      </button>
   </div>
   <div class="public_dropdown_container_xs">
      <a class="dropdown_text dropdown-button" href="javascript:void(0)" data-activates="post_privacy_compose_xs">
      <span>Public</span>
      <i class="zmdi zmdi-caret-up zmdi-hc-lg"></i>
      </a>
      <ul id="post_privacy_compose_xs" class="dropdown-privacy dropdown-content custom_dropdown public_dropdown_xs">
         <li>
            <a href="javascript:void(0)">
            Private
            </a>
         </li>
         <li>
            <a href="javascript:void(0)">
            Connections
            </a>
         </li>
         <li>
            <a href="javascript:void(0)" onclick="addpersonmodal()">
            Custom
            </a>
         </li>
         <li>
            <a href="javascript:void(0)">
            Public
            </a>
         </li>
      </ul>
   </div>
   <div class="post-bholder">
      <div class="post-loader"><img src="images/home-loader.gif"/></div>
      <div class="hidden_xs">
         <a class="btngen-center-align  close_modal open_discard_modal waves-effect">cancel</a>
         <a type="button" class="btngen-center-align waves-effect">Post</a>
      </div>
   </div>
</div>
</div>
</div>
</div>
<!--edit review-->
<!-- trip-->
<!-- compose tool box modal -->
<div id="compose_newtrip" class="modal compose_tool_box post-popup custom_modal main_modal new-wall-post set_re_height">
<div class="hidden_header">
<div class="content_header">
<button class="close_span cancel_poup waves-effect">
<i class="mdi mdi-close mdi-20px compose_discard_popup"></i>
</button>
<p class="modal_header_xs">Write trip</p>
<a type="button" class="post_btn action_btn post_btn_xs close_modal waves-effect"  onclick="verify()">Post</a>
</div>
</div>
<div class="modal-content">
<div class="new-post active">
<div class="top-stuff">
   <div class="postuser-info">
      <span class="img-holder"><img class="circle" src="images/demo-profile.jpg" /></span>
      <div class="desc-holder">
         <span class="profile_name">Nimish Parekh</span>
      </div>
   </div>
   <div class="settings-icon">                     
      <a href="javascript:void(0)" onclick="clearPost()">
      <i class="zmdi zmdi-refresh-alt"></i>
      </a>
   </div>
</div>
<div class="clear"></div>
<div class="scroll_div">
   <div class="npost-content">
      <div class="post-mcontent">
         <div class="npost-title title_post_container">                                    
            <input type="text" class="title" placeholder=" Help others with your valuable tip ">                                   
         </div>
         <div class="clear"></div>
         <div class="desc">
            <textarea id="new_post_comment" placeholder="Write your tip" class="materialize-textarea comment_textarea new_post_comment"></textarea>
         </div>
      </div>
   </div>
</div>
</div>
</div>
<div class="modal-footer">
<div class="new-post active">
<div class="post-bcontent">
   <div class="footer_icon_container">
      <button class="comment_footer_icon waves-effect compose_titleAction" id="compose_titleAction">
      <img src="images/addtitleBl.png">
      </button>
   </div>
   <div class="post-bholder">
      <div class="post-loader"><img src="images/home-loader.gif"/></div>
      <div class="hidden_xs">
         <a class="btngen-center-align close_modal open_discard_modal waves-effect">cancel</a>
         <a type="button" class="btngen-center-align waves-effect">Post</a>
      </div>
   </div>
</div>
</div>
</div>
</div>
<!-- trip-->
<!--edit trip-->
<!-- compose tool box modal -->
<div id="edit_newtrip" class="modal compose_tool_box post-popup custom_modal main_modal new-wall-post set_re_height">
<div class="hidden_header">
<div class="content_header">
<button class="close_span cancel_poup waves-effect">
<i class="mdi mdi-close mdi-20px compose_discard_popup"></i>
</button>
<p class="modal_header_xs">Write Post</p>
<a type="button" class="post_btn action_btn post_btn_xs close_modal waves-effect"  onclick="verify()">Post</a>
</div>
</div>
<div class="modal-content">
<div class="new-post active">
<div class="top-stuff">
   <div class="postuser-info">
      <span class="img-holder"><img class="circle" src="images/demo-profile.jpg" /></span>
      <div class="desc-holder">
         <span class="profile_name">Nimish Parekh</span>
      </div>
   </div>
   <div class="settings-icon">
      <a href="javascript:void(0)" onclick="clearPost()">
      <i class="zmdi zmdi-refresh-alt"></i>
      </a>
   </div>
</div>
<div class="clear"></div>
<div class="scroll_div">
   <div class="npost-content">
      <div class="post-mcontent">
         <div class="npost-title title_post_container title_block">                                    
            <input type="text" class="title" placeholder=""value="trip title">                                 
         </div>
         <div class="clear"></div>
         <div class="desc">
            <textarea id="new_post_comment" placeholder="Write your tip" class="materialize-textarea comment_textarea new_post_comment">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</textarea>
         </div>
      </div>
   </div>
</div>
</div>
</div>
<div class="modal-footer">
<div class="new-post active">
<div class="post-bcontent">
   <div class="footer_icon_container">
      <button class="comment_footer_icon waves-effect compose_titleAction" id="compose_titleAction">
      <img src="images/addtitleBl.png">
      </button>
   </div>
   <div class="post-bholder">
      <div class="post-loader"><img src="images/home-loader.gif"/></div>
      <div class="hidden_xs">
         <a class="btngen-center-align close_modal open_discard_modal waves-effect">cancel</a>
         <a type="button" class="btngen-center-align waves-effect">Post</a>
      </div>
   </div>
</div>
</div>
</div>
</div>
<!--edit trip-->    
<!-- ask-->
<!-- compose tool box modal -->
<div id="compose_newask" class="modal compose_tool_box post-popup custom_modal main_modal new-wall-post set_re_height compose_newask_popup">
<div class="hidden_header">
<div class="content_header">
<button class="close_span cancel_poup waves-effect">
<i class="mdi mdi-close mdi-20px compose_discard_popup"></i>
</button>
<p class="modal_header_xs">Ask question</p>
<a type="button" class="post_btn action_btn post_btn_xs close_modal waves-effect"  onclick="verify()">Post</a>
</div>
</div>
<div class="modal-content">
<div class="new-post active">
<div class="top-stuff">
   <div class="postuser-info">
      <span class="img-holder"><img class="circle" src="images/demo-profile.jpg" /></span>
      <div class="desc-holder">
         <span class="profile_name">Nimish Parekh</span>
      </div>
   </div>
   <div class="settings-icon">
      <a href="javascript:void(0)" onclick="clearPost()">
      <i class="zmdi zmdi-refresh-alt"></i>
      </a>
   </div>
</div>
<div class="clear"></div>
<div class="scroll_div">
   <div class="npost-content">
      <div class="post-mcontent">
         <div class="clear"></div>
         <div class="npost-title title_post_container">    
            <input class="title" placeholder="Title of your question" type="text">             
         </div>
         <div class="clear"></div>
         <div class="desc">
            <textarea id="new_post_comment" placeholder="What's your question?" class="materialize-textarea comment_textarea new_post_comment"></textarea>
         </div>
      </div>
   </div>
</div>
</div>
</div>
<div class="modal-footer">
<div class="new-post active">
<div class="post-bcontent">
   <div class="footer_icon_container">
      <button class="comment_footer_icon waves-effect compose_titleAction" id="compose_titleAction">
      <img src="images/addtitleBl.png">
      </button>
   </div>
   <div class="post-bholder">
      <div class="post-loader"><img src="images/home-loader.gif"></div>
      <div class="hidden_xs">
         <a class="btngen-center-align close_modal open_discard_modal waves-effect">cancel</a>
         <a type="button" class="btngen-center-align waves-effect">Post</a>
      </div>
   </div>
</div>
</div>
</div>
</div>
<!-- ask-->
<!--edit ask-->
<!-- compose tool box modal -->
<div id="edit_newask" class="modal compose_tool_box post-popup custom_modal main_modal new-wall-post set_re_height">
<div class="hidden_header">
<div class="content_header">
<button class="close_span cancel_poup waves-effect">
<i class="mdi mdi-close mdi-20px compose_discard_popup"></i>
</button>
<p class="modal_header_xs">Ask question</p>
<a type="button" class="post_btn action_btn post_btn_xs close_modal waves-effect"  onclick="verify()">Post</a>
</div>
</div>
<div class="modal-content">
<div class="new-post active">
<div class="top-stuff">
   <!--<div class="side-user">-->
   <div class="postuser-info">
      <span class="img-holder"><img class="circle" src="images/demo-profile.jpg" /></span>
      <div class="desc-holder">
         <span class="profile_name">Nimish Parekh</span>
      </div>
   </div>
   <div class="settings-icon">
      <a href="javascript:void(0)"  onclick="clearPost()">
      <i class="zmdi zmdi-refresh-alt"></i>
      </a>
   </div>
</div>
<div class="clear"></div>
<div class="scroll_div">
   <div class="npost-content">
      <div class="post-mcontent">
         <div class="clear"></div>
         <div class="npost-title title_post_container title_block">    
            <input class="title" placeholder="" type="text" value="Title of your question">                
         </div>
         <div class="clear"></div>
         <div class="desc">
            <textarea id="new_post_comment" placeholder="What's your question?" class="materialize-textarea comment_textarea new_post_comment">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</textarea>
         </div>
      </div>
   </div>
</div>
</div>
</div>
<div class="modal-footer">
<div class="new-post active">
<div class="post-bcontent">
   <div class="footer_icon_container">
      <button class="comment_footer_icon waves-effect compose_titleAction" id="compose_titleAction">
      <img src="images/addtitleBl.png">
      </button>
   </div>
   <div class="post-bholder">
      <div class="post-loader"><img src="images/home-loader.gif"></div>
      <div class="hidden_xs">
         <a class="btngen-center-align close_modal open_discard_modal waves-effect">cancel</a>
         <a type="button" class="btngen-center-align waves-effect">Post</a>
      </div>
   </div>
</div>
</div>
</div>
</div>
<!--edit ask-->
<div id="compose_mapmodal" class="modal map_modal compose_inner_modal modalxii_level1 map_modalUniq">
<?php include('common/map_modal.php'); ?>
</div>
<?php include('common/addperson_popup.php'); ?>
<?php include('common/add_photo_popup.php'); ?>
<!-- Add Photo-->
<!-- compose Comment modal -->
<div id="comment_modal_xs" class="modal tbpost_modal custom_modal split-page main_modal cust-pop dicrease-popup-compose compose_Comment_Action">
<div class="modal_content_container">
<div class="modal_content_child modal-content">
<div class="popup-title ">
   <button class="hidden_close_span close_span waves-effect">
   <i class="mdi mdi-close mdi-20px compose_discard_popup"></i>
   </button>           
   <h3>All Comments</h3>
   <a type="button" class="item_done crop_done hidden_close_span waves-effect custom_close" href="javascript:void(0)">Done</a>
   <a type="button" class="item_done crop_done comment-close custom_close waves-effect" href="javascript:void(0)"><i class="mdi mdi-close"></i></a>
</div>
<div class="custom_modal_content modal_content" id="createpopup">
   <div class="comment-box-tab profile-tab">
      <div class="comment-poup-box detail-box">
         <div class="content-holder main-holder">
            <div class="summery">
               <div class="dsection bborder expandable-holder expanded">
                  <div class="form-area expandable-area post-holder">
                     <div class="post-more">
                        <a href="javascript:void(0)" class="view-morec">View more comments</a>
                        <span class="total-comments">3 of 7</span>
                     </div>
                     <div class="post-comments">
                        <div class="pcomments">
                           <div class="pcomment-earlier">
                              <div class="pcomment-holder">
                                 <div class="pcomment main-comment">
                                    <div class="img-holder">
                                       <div class="profiletipholder">
                                          <span class="profile-tooltip">
                                          <img class="circle" src="images/demo-profile.jpg"/>
                                          </span>
                                          <span class="profiletooltip_content slidingpan-holder">
                                             <div class="profile-tip dis-none">
                                                <div class="profile-tip-avatar">
                                                   <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                                 
                                                   <div class="sliding-pan location-span">
                                                      <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                   </div>
                                                </div>
                                                <div class="profile-tip-name">
                                                   <a href="javascript:void(0)">Adel Hasanat</a>
                                                </div>
                                                <div class="profile-tip-info">
                                                   <div class="profiletip-icon">
                                                      <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                   </div>
                                                   <div class="profiletip-icon">
                                                      <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                   </div>
                                                   <div class="profiletip-icon">
                                                      <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class=”mdi mdi-eye”></i></span></a>
                                                   </div>
                                                </div>
                                             </div>
                                          </span>
                                       </div>
                                    </div>
                                    <div class="desc-holder">
                                       <div class="normal-mode">
                                          <div class="desc">
                                             <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                             <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                          </div>
                                          <div class="comment-stuff">
                                             <div class="more-opt">
                                                <span class="likeholder">
                                                <span class="like-tooltip">
                                                <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>">
                                                <i class="zmdi zmdi-thumb-up"></i>
                                                </a>
                                                </span>
                                                </span>
                                                <a href="javascript:void(0)" class="pa-reply reply-comment"><span>icon</span></a>
                                                <a class="dropdown-button more_btn" href="javascript:void(0);" data-activates="comment_ecit32">                               <i class="zmdi zmdi-hc-2x zmdi-more-vert"></i>
                                                </a>
                                                <ul id="comment_ecit32" class="dropdown-content custom_dropdown">
                                                   <li><a class="edit-comment" href="javascript:void(0)">Edit</a></li>
                                                   <li><a class="delete-comment" href="javascript:void(0)">Delete</a></li>
                                                </ul>
                                             </div>
                                             <div class="less-opt">
                                                <div class="timestamp">8h</div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="edit-mode">
                                          <div class="desc">
                                             <textarea class="editcomment-tt materialize-textarea mb0 md_textarea descinput" id="ec-1">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                             <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close"></i></a>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="clear"></div>
                                 <div class="comment-reply-holder comment-addreply">
                                    <div class="addnew-comment valign-wrapper comment-reply">
                                       <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                                       <div class="desc-holder">                                   
                                          <textarea class="materialize-textarea mb0 md_textarea descinput">Write a reply...</textarea>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="pcomment-holder">
                                 <div class="pcomment main-comment">
                                    <div class="img-holder">
                                       <div class="profiletipholder">
                                          <span class="profile-tooltip">
                                          <img class="circle" src="images/demo-profile.jpg"/>
                                          </span>
                                          <span class="profiletooltip_content slidingpan-holder">
                                             <div class="profile-tip dis-none">
                                                <div class="profile-tip-avatar">
                                                   <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                                 
                                                   <div class="sliding-pan location-span">
                                                      <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                   </div>
                                                </div>
                                                <div class="profile-tip-name">
                                                   <a href="javascript:void(0)">Adel Hasanat</a>
                                                </div>
                                                <div class="profile-tip-info">
                                                   <div class="profiletip-icon">
                                                      <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                   </div>
                                                   <div class="profiletip-icon">
                                                      <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                   </div>
                                                   <div class="profiletip-icon">
                                                      <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class=”mdi mdi-eye”></i></span></a>
                                                   </div>
                                                </div>
                                             </div>
                                          </span>
                                       </div>
                                    </div>
                                    <div class="desc-holder">
                                       <div class="normal-mode">
                                          <div class="desc">
                                             <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                             <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                          </div>
                                          <div class="comment-stuff">
                                             <div class="more-opt">
                                                <span class="likeholder">
                                                <span class="like-tooltip">
                                                <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>">
                                                <i class="zmdi zmdi-thumb-up"></i>
                                                </a>
                                                </span>
                                                </span>    
                                                <a href="javascript:void(0)" class="pa-reply reply-comment"><span>icon</span></a>
                                                <a class="dropdown-button more_btn" href="javascript:void(0);" data-activates="comment_ecit33">                               <i class="zmdi zmdi-hc-2x zmdi-more-vert"></i>
                                                </a>
                                                <ul id="comment_ecit33" class="dropdown-content custom_dropdown">
                                                   <li><a class="edit-comment" href="javascript:void(0)">Edit</a></li>
                                                   <li><a class="delete-comment" href="javascript:void(0)">Delete</a></li>
                                                </ul>
                                             </div>
                                             <div class="less-opt">
                                                <div class="timestamp">8h</div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="edit-mode">
                                          <div class="desc">
                                             <textarea class="editcomment-tt materialize-textarea mb0 md_textarea descinput" id="ec-2">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                             <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close"></i></a>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="clear"></div>
                                 <div class="comment-reply-holder comment-addreply">
                                    <div class="addnew-comment valign-wrapper comment-reply">
                                       <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                                       <div class="desc-holder">                                   
                                          <textarea class="materialize-textarea mb0 md_textarea descinput">Write a reply...</textarea>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="pcomment-holder has-comments">
                              <div class="pcomment main-comment">
                                 <div class="img-holder">
                                    <div class="profiletipholder">
                                       <span class="profile-tooltip">
                                       <img class="circle" src="images/demo-profile.jpg"/>
                                       </span>
                                       <span class="profiletooltip_content slidingpan-holder">
                                          <div class="profile-tip dis-none">
                                             <div class="profile-tip-avatar">
                                                <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                                    
                                                <div class="sliding-pan location-span">
                                                   <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                </div>
                                             </div>
                                             <div class="profile-tip-name">
                                                <a href="javascript:void(0)">Adel Hasanat</a>
                                             </div>
                                             <div class="profile-tip-info">
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                </div>
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                </div>
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class=”mdi mdi-eye”></i></span></a>
                                                </div>
                                             </div>
                                          </div>
                                       </span>
                                    </div>
                                 </div>
                                 <div class="desc-holder">
                                    <div class="normal-mode">
                                       <div class="desc">
                                          <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                          <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                       </div>
                                       <div class="comment-stuff">
                                          <div class="more-opt">
                                             <span class="likeholder">
                                             <span class="like-tooltip">
                                             <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>">
                                             <i class="zmdi zmdi-thumb-up"></i>
                                             </a>
                                             </span>
                                             </span>
                                             <a href="javascript:void(0)" class="pa-reply reply-comment"><span>icon</span></a>
                                             <a class="dropdown-button more_btn" href="javascript:void(0);" data-activates="comment_ecit34" >                              <i class="zmdi zmdi-hc-2x zmdi-more-vert"></i>
                                             </a>
                                             <ul id="comment_ecit34" class="dropdown-content custom_dropdown">
                                                <li><a class="edit-comment" href="javascript:void(0)">Edit</a></li>
                                                <li><a class="delete-comment" href="javascript:void(0)">Delete</a></li>
                                             </ul>
                                          </div>
                                          <div class="less-opt">
                                             <div class="timestamp">8h</div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="edit-mode">
                                       <div class="desc">
                                          <textarea class="editcomment-tt materialize-textarea mb0 md_textarea descinput" id="ec-3">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                          <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close"></i></a>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="clear"></div>
                              <div class="comment-reply-holder">
                                 <div class="comments-reply-summery">
                                    <a href="javascript:void(0)" onclick="openReplies(this)">
                                    <i class="mdi mdi-share"></i>
                                    2 Replies                                                  
                                    </a>
                                    <i class="mdi mdi-bullseye dot-i"></i>
                                    Just Now
                                 </div>
                                 <div class="comments-reply-details">
                                    <div class="pcomment comment-reply">
                                       <div class="img-holder">
                                          <div class="profiletipholder">
                                             <span class="profile-tooltip">
                                             <img class="circle" src="images/demo-profile.jpg"/>
                                             </span>
                                             <span class="profiletooltip_content slidingpan-holder">
                                                <div class="profile-tip dis-none">
                                                   <div class="profile-tip-avatar">
                                                      <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                                  
                                                      <div class="sliding-pan location-span">
                                                         <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                      </div>
                                                   </div>
                                                   <div class="profile-tip-name">
                                                      <a href="javascript:void(0)">Adel Hasanat</a>
                                                   </div>
                                                   <div class="profile-tip-info">
                                                      <div class="profiletip-icon">
                                                         <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                      </div>
                                                      <div class="profiletip-icon">
                                                         <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                      </div>
                                                      <div class="profiletip-icon">
                                                         <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class=”mdi mdi-eye”></i></span></a>
                                                      </div>
                                                   </div>
                                                </div>
                                             </span>
                                          </div>
                                       </div>
                                       <div class="desc-holder">
                                          <div class="normal-mode">
                                             <div class="desc">
                                                <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                             </div>
                                             <div class="comment-stuff">
                                                <div class="more-opt">
                                                   <span class="likeholder">
                                                   <span class="like-tooltip">
                                                   <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>">
                                                   <i class="zmdi zmdi-thumb-up"></i>
                                                   </a>
                                                   </span>
                                                   </span>
                                                   <a href="javascript:void(0)" class="pa-reply reply-comment"><span>icon</span></a>
                                                   <a class="dropdown-button more_btn" href="javascript:void(0);" data-activates="comment_ecit35">                                <i class="zmdi zmdi-hc-2x zmdi-more-vert"></i>
                                                   </a>
                                                   <ul id="comment_ecit35" class="dropdown-content custom_dropdown">
                                                      <li><a class="edit-comment" href="javascript:void(0)">Edit</a></li>
                                                      <li><a class="delete-comment" href="javascript:void(0)">Delete</a></li>
                                                   </ul>
                                                </div>
                                                <div class="less-opt">
                                                   <div class="timestamp">8h</div>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="edit-mode">
                                             <div class="desc">
                                                <textarea class="editcomment-tt materialize-textarea mb0 md_textarea descinput" id="ec-4">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                                <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close"></i></a>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="pcomment comment-reply">
                                       <div class="img-holder">
                                          <div class="profiletipholder">
                                             <span class="profile-tooltip">
                                             <img class="circle" src="images/demo-profile.jpg"/>
                                             </span>
                                             <span class="profiletooltip_content slidingpan-holder">
                                                <div class="profile-tip dis-none">
                                                   <div class="profile-tip-avatar">
                                                      <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                                  
                                                      <div class="sliding-pan location-span">
                                                         <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                      </div>
                                                   </div>
                                                   <div class="profile-tip-name">
                                                      <a href="javascript:void(0)">Adel Hasanat</a>
                                                   </div>
                                                   <div class="profile-tip-info">
                                                      <div class="profiletip-icon">
                                                         <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                      </div>
                                                      <div class="profiletip-icon">
                                                         <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                      </div>
                                                      <div class="profiletip-icon">
                                                         <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class=”mdi mdi-eye”></i></span></a>
                                                      </div>
                                                   </div>
                                                </div>
                                             </span>
                                          </div>
                                       </div>
                                       <div class="desc-holder">
                                          <div class="normal-mode">
                                             <div class="desc">
                                                <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit...</p>
                                             </div>
                                             <div class="comment-stuff">
                                                <div class="more-opt">
                                                   <span class="likeholder">
                                                   <span class="like-tooltip">
                                                   <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>">
                                                   <i class="zmdi zmdi-thumb-up"></i>
                                                   </a>
                                                   </span>
                                                   </span> 
                                                   <a href="javascript:void(0)" class="pa-reply reply-comment"><span>icon</span></a>
                                                   <a class="dropdown-button more_btn" href="javascript:void(0);" data-activates="comment_ecit37">                                <i class="zmdi zmdi-hc-2x zmdi-more-vert"></i>
                                                   </a>
                                                   <ul id="comment_ecit37" class="dropdown-content custom_dropdown">
                                                      <li><a class="edit-comment" href="javascript:void(0)">Edit</a></li>
                                                      <li><a class="delete-comment" href="javascript:void(0)">Delete</a></li>
                                                   </ul>
                                                </div>
                                                <div class="less-opt">
                                                   <div class="timestamp">8h</div>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="edit-mode">
                                             <div class="desc">
                                                <textarea class="editcomment-tt materialize-textarea mb0 md_textarea descinput" id="ec-5">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                                <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close"></i></a>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="comment-reply-holder comment-addreply">
                                 <div class="addnew-comment valign-wrapper comment-reply">
                                    <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                                    <div class="desc-holder">                                  
                                       <textarea class="materialize-textarea mb0 md_textarea descinput">Write a reply...</textarea>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="pcomment-holder">
                              <div class="pcomment main-comment">
                                 <div class="img-holder">
                                    <div class="profiletipholder">
                                       <span class="profile-tooltip">
                                       <img class="circle" src="images/demo-profile.jpg"/>
                                       </span>
                                       <span class="profiletooltip_content slidingpan-holder">
                                          <div class="profile-tip dis-none">
                                             <div class="profile-tip-avatar">
                                                <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                                    
                                                <div class="sliding-pan location-span">
                                                   <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                </div>
                                             </div>
                                             <div class="profile-tip-name">
                                                <a href="javascript:void(0)">Adel Hasanat</a>
                                             </div>
                                             <div class="profile-tip-info">
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                </div>
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                </div>
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class=”mdi mdi-eye”></i></span></a>
                                                </div>
                                             </div>
                                          </div>
                                       </span>
                                    </div>
                                 </div>
                                 <div class="desc-holder">
                                    <div class="normal-mode">
                                       <div class="desc">
                                          <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                          <p class="shorten">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.<a a="" href="javascript:void(0)" class="overlay" onclick="explandReadMore(this)"><span class="readlink">Read More</span></a></p>
                                       </div>
                                       <div class="comment-stuff">
                                          <div class="more-opt">
                                             <span class="likeholder">
                                             <span class="like-tooltip">
                                             <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>">
                                             <i class="zmdi zmdi-thumb-up"></i>
                                             </a>
                                             </span>
                                             </span>   
                                             <a href="javascript:void(0)" class="pa-reply reply-comment"><span>icon</span></a>
                                             <a class="dropdown-button more_btn" href="javascript:void(0);" data-activates="comment_ecit36" >                              <i class="zmdi zmdi-hc-2x zmdi-more-vert"></i>
                                             </a>
                                             <ul id="comment_ecit36" class="dropdown-content custom_dropdown">
                                                <li><a class="edit-comment" href="javascript:void(0)">Edit</a></li>
                                                <li><a class="delete-comment" href="javascript:void(0)">Delete</a></li>
                                             </ul>
                                          </div>
                                          <div class="less-opt">
                                             <div class="timestamp">8h</div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="edit-mode">
                                       <div class="desc">
                                          <textarea class="editcomment-tt materialize-textarea mb0 md_textarea descinput" id="ec-6">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                          <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close"></i></a>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="clear"></div>
                              <div class="comment-reply-holder comment-addreply">
                                 <div class="addnew-comment valign-wrapper comment-reply">
                                    <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                                    <div class="desc-holder">                                  
                                       <textarea class="materialize-textarea mb0 md_textarea descinput">Write a reply...</textarea>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
</div>
</div>
<div class="modal-footer">
<div class="new-post active">
<div class="post-bcontent">
   <div class="addnew-comment valign-wrapper">
      <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
      <div class="desc-holder">                                    
         <textarea class="materialize-textarea mb0 md_textarea descinput" id="comment_txt_1">Write a reply...</textarea>
      </div>
   </div>
</div>
</div>
</div>
<div class="additem_modal_footer modal-footer">
<div class="">
<div class="btn-holder">
   <div class="addnew-comment valign-wrapper">
      <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
      <div class="desc-holder">                                    
         <textarea class="materialize-textarea mb0 md_textarea descinput" id="comment_txt_1">Write a reply...</textarea>
      </div>
   </div>
</div>
</div>
</div>
</div>
<link rel="stylesheet" type="text/css" href="css/owl.carousel.css">
<script type="text/javascript" src="js/owl.carousel.js"></script>
<script type="text/javascript" src="js/wall.js"></script>
<script type="text/javascript">
$(document).ready(function(){ 
$w = $(window).width();
if ( $w > 739) {      
$(".places-tabs .sub-tabs li a").click(function(){
   $("body").removeClass("remove_scroller");
}); 
$(".tabs.icon-menu.tabsnew li a").click(function(){
   $("body").removeClass("remove_scroller");
}); 
$(".mbl-tabnav").click(function(){
   $("body").removeClass("remove_scroller");
}); 
$(".clicable.viewall-link").click(function(){
   $("body").removeClass("remove_scroller");
}); 
} else {
$(".places-tabs .sub-tabs li a").click(function(){
   $("body").addClass("remove_scroller");
}); 
$(".clicable.viewall-link").click(function(){
   $("body").addClass("remove_scroller");
});         
$(".tabs.icon-menu.tabsnew li a").click(function(){
   $("body").addClass("remove_scroller");
}); 
$(".mbl-tabnav").click(function(){
   $("body").removeClass("remove_scroller");
});
}

$(".header-icon-tabs .tabsnew .tab a").click(function(){
$(".bottom_tabs").hide();
});

$(".places-tabs .tab a").click(function(){
$(".top_tabs").hide();
});

// footer work for places home page only
$('.footer-section').css('left', '0');
$w = $(window).width();
if($w <= 768) {
$('.main-footer').css({
   'width': '100%',
   'left': '0'
});
} else {
var $_I = $('.places-content.places-all').width();
var $__I = $('.places-content.places-all').find('.container').width();

var $half = parseInt($_I) - parseInt($__I);
$half = parseInt($half) / 2;

$('.main-footer').css({
   'width': $_I+'px',
   'left': '-'+$half+'px'
});
}
});

$(window).resize(function() {
// footer work for places home page only
if($('#places-all').hasClass('active')) {
$('.footer-section').css('left', '0');
$w = $(window).width();
if($w <= 768) {
   $('.main-footer').css({
      'width': '100%',
      'left': '0'
   });
} else {
   var $_I = $('.places-content.places-all').width();
   var $__I = $('.places-content.places-all').find('.container').width();

   var $half = parseInt($_I) - parseInt($__I);
   $half = parseInt($half) / 2;

   $('.main-footer').css({
      'width': $_I+'px',
      'left': '-'+$half+'px'
   });
}
}
});

$(document).on('click', '.tablist .tab a', function(e) {
$href = $(this).attr('href');
$href = $href.replace('#', '');

$('.places-content').removeClass().addClass('places-content '+$href);
   $this = $(this);
});

$(window).resize(function() {    
   setplacetab();
});   
</script>
<?php include('common/discard_popup.php'); ?>
<?php include('common/upload_gallery_popup.php'); ?>
<?php include('common/privacymodal.php'); ?>
<?php include('common/custom_modal.php'); ?>
<?php include('common/addcategories_popup.php'); ?>
<?php include("script.php"); ?>