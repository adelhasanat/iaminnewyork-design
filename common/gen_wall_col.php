<div class="wallcontent-column">
   <div class="sidebar-stuff">
      <div class="content-box bshadow rightmap rp_map">
         <div class="cbox-desc">
            <div class="placeintro-side width-100">
               <h5><?=$st_nm_S?></h5>
               <div class="map-holder">
                  <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3110.3465133386144!2d-9.167423685010494!3d38.77868997958898!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xd193295d5b45545%3A0x3f9e7b6a5f00e12c!2sPerta!5e0!3m2!1sen!2sin!4v1481089901870" width="600" height="450" frameborder="0" allowfullscreen></iframe>
               </div>
            </div>
         </div>
      </div>
      <div class="content-box bshadow rp_travellers">
         <div class="cbox-title nborder">
            <i class="mdi mdi-airplane"></i>
            <a href="javascript:void(0)" onclick="openDirectTab('places-travellers')">Travellers</a>
         </div>
         <div class="cbox-desc">
            <div class="connect-list grid-list">
               <div class="row">
                  <div class="grid-box">
                     <div class="connect-box">
                        <div class="imgholder online-img"><img src="images/people-1.png" /></div>
                        <div class="descholder">
                           <a href="javascript:void(0)">
                           <span class="userlink">John Doe</span>
                           <span class="info">2 posts</span>
                           </a>
                        </div>
                        <span class="online-mark"><i class="zmdi zmdi-check"></i></span>
                     </div>
                  </div>
                  <div class="grid-box">
                     <div class="connect-box">
                        <div class="imgholder online-img"><img src="images/people-1.png" /></div>
                        <div class="descholder">
                           <a href="javascript:void(0)">
                           <span class="userlink">John Doe</span>
                           <span class="info">2 posts</span>
                           </a>
                        </div>
                        <span class="online-mark"><i class="zmdi zmdi-check"></i></span>
                     </div>
                  </div>
                  <div class="grid-box">
                     <div class="connect-box">
                        <div class="imgholder"><img src="images/people-1.png" /></div>
                        <div class="descholder">
                           <a href="javascript:void(0)">
                           <span class="userlink">John Doe</span>
                           <span class="info">2 posts</span>
                           </a>
                        </div>
                     </div>
                  </div>
                  <div class="grid-box">
                     <div class="connect-box">
                        <div class="imgholder"><img src="images/people-1.png" /></div>
                        <div class="descholder">
                           <a href="javascript:void(0)">
                           <span class="userlink">John Doe</span>
                           <span class="info">2 posts</span>
                           </a>
                        </div>
                     </div>
                  </div>
                  <div class="grid-box">
                     <div class="connect-box">
                        <div class="imgholder"><img src="images/people-1.png" /></div>
                        <div class="descholder">
                           <a href="javascript:void(0)">
                           <span class="userlink">John Doe</span>
                           <span class="info">2 posts</span>
                           </a>
                        </div>
                        <span class="online-mark"><i class="zmdi zmdi-check"></i></span>
                     </div>
                  </div>
                  <div class="grid-box">
                     <div class="connect-box">
                        <div class="imgholder"><img src="images/people-1.png" /></div>
                        <div class="descholder">
                           <a href="javascript:void(0)">
                           <span class="userlink">John Doe</span>
                           <span class="info">2 posts</span>
                           </a>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="content-box bshadow rp_locals">
         <div class="cbox-title nborder">
            <i class="mdi mdi-map"></i>
            <a href="javascript:void(0)" onclick="openDirectTab('places-locals')">Local</a>
         </div>
         <div class="cbox-desc rp_locals">
            <div class="connect-list grid-list">
               <div class="row">
                  <div class="grid-box">
                     <div class="connect-box">
                        <div class="imgholder"><img src="images/people-1.png" /></div>
                        <div class="descholder">
                           <a href="javascript:void(0)">
                           <span class="userlink">John Doe</span>
                           <span class="info">2 posts</span>
                           </a>
                        </div>
                        <span class="online-mark"><i class="zmdi zmdi-check"></i></span>
                     </div>
                  </div>
                  <div class="grid-box">
                     <div class="connect-box">
                        <div class="imgholder"><img src="images/people-1.png" /></div>
                        <div class="descholder">
                           <a href="javascript:void(0)">
                           <span class="userlink">John Doe</span>
                           <span class="info">2 posts</span>
                           </a>
                        </div>
                        <span class="online-mark"><i class="zmdi zmdi-check"></i></span>
                     </div>
                  </div>
                  <div class="grid-box">
                     <div class="connect-box">
                        <div class="imgholder"><img src="images/people-1.png" /></div>
                        <div class="descholder">
                           <a href="javascript:void(0)">
                           <span class="userlink">John Doe</span>
                           <span class="info">2 posts</span>
                           </a>
                        </div>
                     </div>
                  </div>
                  <div class="grid-box">
                     <div class="connect-box">
                        <div class="imgholder"><img src="images/people-1.png" /></div>
                        <div class="descholder">
                           <a href="javascript:void(0)">
                           <span class="userlink">John Doe</span>
                           <span class="info">2 posts</span>
                           </a>
                        </div>
                     </div>
                  </div>
                  <div class="grid-box">
                     <div class="connect-box">
                        <div class="imgholder"><img src="images/people-1.png" /></div>
                        <div class="descholder">
                           <a href="javascript:void(0)">
                           <span class="userlink">John Doe</span>
                           <span class="info">2 posts</span>
                           </a>
                        </div>
                        <span class="online-mark"><i class="zmdi zmdi-check"></i></span>
                     </div>
                  </div>
                  <div class="grid-box">
                     <div class="connect-box">
                        <div class="imgholder"><img src="images/people-1.png" /></div>
                        <div class="descholder">
                           <a href="javascript:void(0)">
                           <span class="userlink">John Doe</span>
                           <span class="info">2 posts</span>
                           </a>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="content-box bshadow rp_reviews">
         <div class="cbox-title nborder">
            <i class="zmdi zmdi-view-list-alt zmdi-hc-lg"></i>
            <a href="javascript:void(0)" onclick="openDirectTab('places-reviews')">Review</a>
         </div>
         <div class="cbox-desc">
            <div class="reviews-summery">
               <div class="reviews-add">
                  <div class="stars-holder">
                     <img src="images/blank-star.png" />
                     <img src="images/blank-star.png" />
                     <img src="images/blank-star.png" />
                     <img src="images/blank-star.png" />
                     <img src="images/blank-star.png" />
                  </div>
                  <p>What do you think about this page?</p>
               </div>
               <div class="reviews-people">
                  <ul>
                     <li>
                        <div class="reviewpeople-box">
                           <div class="imgholder"><img src="images/people-3.png" /></div>
                           <div class="descholder">
                              <h6>Kelly Mark <span>about 2 weeks ago</span></h6>
                              <div class="stars-holder">
                                 <img src="images/filled-star.png" />
                                 <img src="images/filled-star.png" />
                                 <img src="images/filled-star.png" />
                                 <img src="images/blank-star.png" />
                                 <img src="images/blank-star.png" />
                                 <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard? Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard?Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard?Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard?Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard?</p>
                              </div>
                           </div>
                        </div>
                     </li>
                     <li>
                        <div class="reviewpeople-box">
                           <div class="imgholder"><img src="images/people-2.png" /></div>
                           <div class="descholder">
                              <h6>John Davior <span>about 8 months ago</span></h6>
                              <div class="stars-holder">
                                 <img src="images/filled-star.png" />
                                 <img src="images/filled-star.png" />
                                 <img src="images/filled-star.png" />
                                 <img src="images/filled-star.png" />
                                 <img src="images/blank-star.png" />
                                 <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard? Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard?Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard?Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard?Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard?</p>
                              </div>
                           </div>
                        </div>
                     </li>
                     <li>
                        <div class="reviewpeople-box">
                           <div class="imgholder"><img src="images/people-1.png" /></div>
                           <div class="descholder">
                              <h6>Joe Doe <span>about 11 months ago</span></h6>
                              <div class="stars-holder">
                                 <img src="images/filled-star.png" />
                                 <img src="images/filled-star.png" />
                                 <img src="images/filled-star.png" />
                                 <img src="images/blank-star.png" />
                                 <img src="images/blank-star.png" />
                                 <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard? Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard?Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard?Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard?Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard?</p>
                              </div>
                           </div>
                        </div>
                     </li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
      <div class="content-box bshadow rp_hotel_deal">
         <div class="cbox-title nborder">
            <i class="mdi mdi-office-building"></i>
            <a href="javascript:void(0)" onclick="openDirectTab('places-lodge')">Hotels in <?=$st_nm_S?></a>
         </div>
         <div class="cbox-desc">
            <div class="places-dealsad">
               <ul>
                  <li>
                     <div class="placebox">
                        <a href="javascript:void(0)">
                           <div class="imgholder himg-box">
                              <img src="images/lodge-img-1.jpg" class="himg imgfix" />
                              <div class="overlay"></div>
                           </div>
                           <div class="descholder">
                              <h5>Moeavenpick Resort <?=$st_nm_S?></h5>
                              <span class="ratings">
                              <i class="mdi mdi-star active"></i>
                              <i class="mdi mdi-star active"></i>
                              <i class="mdi mdi-star active"></i>
                              <i class="mdi mdi-star"></i>
                              <i class="mdi mdi-star"></i>
                              <label>45 Reviews</label>
                              </span>
                              <div class="tags">
                                 <span>Luxury</span>
                                 <span>Families</span>
                              </div>
                           </div>
                        </a>
                     </div>
                  </li>
                  <li>
                     <div class="placebox">
                        <a href="javascript:void(0)">
                           <div class="imgholder himg-box">
                              <img src="images/lodge-img-2.jpg" class="himg" />
                              <div class="overlay"></div>
                           </div>
                           <div class="descholder">
                              <h5><?=$st_nm_S?> Moon Hotel</h5>
                              <span class="ratings">
                              <i class="mdi mdi-star active"></i>
                              <i class="mdi mdi-star active"></i>
                              <i class="mdi mdi-star active"></i>
                              <i class="mdi mdi-star active"></i>
                              <i class="mdi mdi-star"></i>
                              <label>20 Reviews</label>
                              </span>
                              <div class="tags">
                                 <span>Budget</span>
                              </div>
                           </div>
                        </a>
                     </div>
                  </li>
                  <li>
                     <div class="placebox">
                        <a href="javascript:void(0)">
                           <div class="imgholder himg-box">
                              <img src="images/lodge-img-3.jpg" class="himg" />
                              <div class="overlay"></div>
                           </div>
                           <div class="descholder">
                              <h5>Marriott <?=$st_nm_S?> Hotel</h5>
                              <span class="ratings">
                              <i class="mdi mdi-star active"></i>
                              <i class="mdi mdi-star active"></i>
                              <i class="mdi mdi-star active"></i>
                              <i class="mdi mdi-star active"></i>
                              <i class="mdi mdi-star active"></i>
                              <label>65 Reviews</label>
                              </span>
                              <div class="tags">
                                 <span>Luxury</span>
                                 <span>Business</span>
                              </div>
                           </div>
                        </a>
                     </div>
                  </li>
               </ul>
            </div>
         </div>
      </div>
      <div class="content-box bshadow rp_recent_questions">
         <div class="cbox-title nborder">
            <i class="zmdi zmdi-help"></i>
            <a href="javascript:void(0)" onclick="openDirectTab('places-ask')">Recent Questions</a>
         </div>
         <div class="cbox-desc">
            <div class="question-holder">
               <ul>
                  <li>
                     <img src="images/people-1.png" />
                     <h6>Adel Hasanat</h6>
                     <p>
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard? Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard?Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard?Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard?Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard?
                     </p>
                  </li>
                  <li>
                     <img src="images/people-2.png" />
                     <h6>Adel Hasanat</h6>
                     <p>
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard? Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard?Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard?Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard?Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard?
                     </p>
                  </li>
               </ul>
               <div class="btn-holder">
                  <a href="javascript:void(0)" class="btn btn-primary btn-sm waves-effect waves-light">View all questions</a>
               </div>
            </div>
         </div>
      </div>
      <div class="content-box bshadow rp_tips">
         <div class="cbox-title nborder">
            <a href="javascript:void(0)" onclick="openDirectTab('places-tip')">Tips for <?=$st_nm_S?></a>
         </div>
         <div class="cbox-desc nsp">
            <div class="question-holder tips-holder">
               <ul>
                  <li>
                     <img src="images/people-1.png" />
                     <h6><span>Tip by</span> Adel Hasanat</h6>
                     <p>
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard? Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard?Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard?Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard?Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard?
                        <a href="javascript:void(0)" class="arrow-more"><i class="mdi mdi-arrow-right-bold-circle-outline"></i></a>
                     </p>
                  </li>
                  <li>
                     <img src="images/people-2.png" />
                     <h6><span>Tip by</span> Adel Hasanat</h6>
                     <p>
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard? Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard?Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard?Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard?Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard?
                        <a href="javascript:void(0)" class="arrow-more"><i class="mdi mdi-arrow-right-bold-circle-outline"></i></a>
                     </p>
                  </li>
               </ul>
            </div>
         </div>
      </div>
      <div class="places-travad rp_booking">
         <a href="javascript:void(0)">
         <img src="images/booking-ad.jpg" />
         </a>
      </div>
      <div class="content-box bshadow rp_place_explore">
         <img src="images/side-<?=$st_nm_L?>.jpg" class="fullimg" />
         <div class="cbox-title nborder">
            <a href="javascript:void(0)" onclick="openDirectTab('places-all')">Explore <?=$st_nm_S?></a>
         </div>
         <div class="cbox-desc">
            <div class="explore-box">
               <ul class="explore-list">
                  <li><a href="javascript:void(0)" onclick="openDirectTab('places-lodge')"><i class="mdi mdi-menu-right"></i>Hotels</a></li>
                  <li><a href="javascript:void(0)" onclick="openDirectTab('places-todo')"><i class="mdi mdi-menu-right"></i>Attractions</a></li>
                  <li><a href="javascript:void(0)" onclick="openDirectTab('places-dine')"><i class="mdi mdi-menu-right"></i>Restaurants</a></li>
                  <li><a href="javascript:void(0)" onclick="openDirectTab('places-all')"><i class="mdi mdi-menu-right"></i>Vacation Rentals</a></li>
               </ul>
            </div>
         </div>
      </div>
      <div class="content-box bshadow adcontainerRight" id="adcontainerRight">
         <div class="cbox-desc">
            <iframe marginwidth="0" marginheight="0" allowtransparency="true" scrolling="no" style="visibility: visible; height: 862px;" name='{"name": "master-1", "master-1": {"container": "adcontainerRight", "linkTarget": "_blank", "lines": 3, "colorText": "#666666", "colorTitleLink": "#0088cc", "colorBackground": "#ffffff", "fontSizeTitle": "14px", "adsLabel": false, "adIconLocation": "ad-left", "domainLinkAboveDescription": true, "detailedAttribution": true, "type": "ads", "columns": 1, "horizontalAlignment": "left", "resultsPageQueryParam": "query"} }' id="master-1" src="https://www.google.com/afs/ads?q=Hotels%20in%20<?=$place?>&amp;adpage=1&amp;r=m&amp;fexp=21404&amp;client=pub-3667005479230723&amp;channel=5999909305&amp;adtest=off&amp;type=0&amp;oe=UTF-8&amp;ie=UTF-8&amp;format=n4&amp;ad=n4&amp;nocache=5881491977752232&amp;num=0&amp;output=uds_ads_only&amp;v=3&amp;adext=as1%2Csr1&amp;bsl=10&amp;u_his=1&amp;u_tz=330&amp;dt=1491977752236&amp;u_w=1366&amp;u_h=768&amp;biw=1349&amp;bih=605&amp;psw=1349&amp;psh=9337&amp;frm=0&amp;uio=uv3cs1vp1sl1sr1st14va1da1-&amp;jsv=13774&amp;rurl=https%3A%2F%2Fwww.iamin<?=$st_nm_L?>.com#master-1" width="100%" frameborder="0">
            </iframe>
         </div>
      </div>
   </div>
</div>