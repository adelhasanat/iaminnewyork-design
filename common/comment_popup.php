<!--post comment modal for xs view-->
<div id="comment_modal_xs" class="modal comment_modal_xs"
   <div class="content_header">
      <button class="close_span cancel_poup waves-effect">
      <i class="mdi mdi-close mdi-20px material_close"></i>
      </button>
      <p class="modal_header_xs">Comments</p>
   <div class="post-data">
      <div class="post-holder">
         <div class="comments-section panel open">
            <div class="post-more">
               <a href="javascript:void(0)" class="view-morec">View more comments <i class="zmdi zmdi-chevron-right trun90"></i></a>
               <span class="total-comments">3 of 7</span>
            </div>
            <div class="post-comments">
               <div class="pcomments">
                  <div class="pcomment-earlier">
                     <div class="pcomment-holder">
                        <div class="pcomment main-comment">
                           <div class="img-holder">
                              <div class="profiletipholder">
                                 <span class="profile-tooltip">
                                 <img class="circle" src="images/demo-profile.jpg"/>
                                 </span>
                                 <span class="profiletooltip_content">
                                    <div class="profile-tip" style="display:none;">
                                       <div class="slidingpan-holder">
                                          <div class="profile-tip-avatar">
                                             <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">													
                                             <div class="sliding-pan location-span">
                                                <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                             </div>
                                          </div>
                                          <div class="profile-tip-name">
                                             <a href="javascript:void(0)">Adel Hasanat</a>
                                          </div>
                                          <div class="profile-tip-info">
                                             <div class="profiletip-icon">
                                                <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                             </div>
                                             <div class="profiletip-icon">
                                                <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                             </div>
                                             <div class="profiletip-icon">
                                                <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class=”mdi mdi-eye”></i></span></a>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </span>
                              </div>
                           </div>
                           <div class="desc-holder">
                              <div class="normal-mode">
                                 <div class="desc">
                                    <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                 </div>
                                 <div class="comment-stuff">
                                    <div class="more-opt">
                                       <span class="likeholder">
                                       <span class="like-tooltip">
                                       <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                       <i class="zmdi zmdi-thumb-up"></i>
                                       </a>
                                       </span>
                                       </span>	
                                       <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><i class="zmdi zmdi-mail-reply zmdi-hc-flip-horizontal"></i></a>
                                       <!-- Dropdown Trigger -->
                                       <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='drop_down111'>
                                       <i class="zmdi zmdi-more"></i>
                                       </a>
                                       <!-- Dropdown Structure -->
                                       <ul id='drop_down111' class='dropdown-content custom_dropdown'>
                                          <li>
                                             <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                          </li>
                                          <li>
                                             <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                          </li>
                                       </ul>
                                    </div>
                                    <div class="less-opt">
                                       <div class="timestamp">8h</div>
                                    </div>
                                 </div>
                              </div>
                              <div class="edit-mode">
                                 <div class="desc">
                                    <div class="sliding-middle-out anim-area tt-holder underlined fullwidth">
                                       <textarea class="editcomment-tt materialize-textarea">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                    </div>
                                    <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close"></i></a>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="clear"></div>
                        <div class="comment-reply-holder comment-addreply">
                           <div class="addnew-comment valign-wrapper comment-reply">
                              <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                              <div class="desc-holder">
                                 <div class="sliding-middle-out anim-area tt-holder">
                                    <textarea class="materialize-textarea" placeholder="Write a reply..."></textarea>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="pcomment-holder">
                        <div class="pcomment main-comment">
                           <div class="img-holder">
                              <div class="profiletipholder">
                                 <span class="profile-tooltip">
                                 <img class="circle" src="images/demo-profile.jpg"/>
                                 </span>
                                 <span class="profiletooltip_content">
                                    <div class="profile-tip" style="display:none;">
                                       <div class="slidingpan-holder">
                                          <div class="profile-tip-avatar">
                                             <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">													
                                             <div class="sliding-pan location-span">
                                                <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                             </div>
                                          </div>
                                          <div class="profile-tip-name">
                                             <a href="javascript:void(0)">Adel Hasanat</a>
                                          </div>
                                          <div class="profile-tip-info">
                                             <div class="profiletip-icon">
                                                <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                             </div>
                                             <div class="profiletip-icon">
                                                <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                             </div>
                                             <div class="profiletip-icon">
                                                <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class=”mdi mdi-eye”></i></span></a>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </span>
                              </div>
                           </div>
                           <div class="desc-holder">
                              <div class="normal-mode">
                                 <div class="desc">
                                    <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                 </div>
                                 <div class="comment-stuff">
                                    <div class="more-opt">
                                       <span class="likeholder">
                                       <span class="like-tooltip">
                                       <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                       <i class="zmdi zmdi-thumb-up"></i>
                                       </a>
                                       </span>
                                       </span>	
                                       <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><i class="zmdi zmdi-mail-reply zmdi-hc-flip-horizontal"></i></a>
                                       <!-- Dropdown Trigger -->
                                       <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='drop_down112'>
                                       <i class="zmdi zmdi-more"></i>
                                       </a>
                                       <!-- Dropdown Structure -->
                                       <ul id='drop_down112' class='dropdown-content custom_dropdown'>
                                          <li>
                                             <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                          </li>
                                          <li>
                                             <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                          </li>
                                       </ul>
                                    </div>
                                    <div class="less-opt">
                                       <div class="timestamp">8h</div>
                                    </div>
                                 </div>
                              </div>
                              <div class="edit-mode">
                                 <div class="desc">
                                    <div class="sliding-middle-out anim-area tt-holder underlined fullwidth">
                                       <textarea class="editcomment-tt materialize-textarea">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                    </div>
                                    <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close"></i></a>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="clear"></div>
                        <div class="comment-reply-holder comment-addreply">
                           <div class="addnew-comment valign-wrapper comment-reply">
                              <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                              <div class="desc-holder">
                                 <div class="sliding-middle-out anim-area tt-holder">
                                    <textarea class="materialize-textarea" placeholder="Write a reply..."></textarea>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="pcomment-holder">
                        <div class="pcomment main-comment">
                           <div class="img-holder">
                              <div class="profiletipholder">
                                 <span class="profile-tooltip">
                                 <img class="circle" src="images/demo-profile.jpg"/>
                                 </span>
                                 <span class="profiletooltip_content">
                                    <div class="profile-tip" style="display:none;">
                                       <div class="slidingpan-holder">
                                          <div class="profile-tip-avatar">
                                             <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">													
                                             <div class="sliding-pan location-span">
                                                <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                             </div>
                                          </div>
                                          <div class="profile-tip-name">
                                             <a href="javascript:void(0)">Adel Hasanat</a>
                                          </div>
                                          <div class="profile-tip-info">
                                             <div class="profiletip-icon">
                                                <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                             </div>
                                             <div class="profiletip-icon">
                                                <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                             </div>
                                             <div class="profiletip-icon">
                                                <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class=”mdi mdi-eye”></i></span></a>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </span>
                              </div>
                           </div>
                           <div class="desc-holder">
                              <div class="normal-mode">
                                 <div class="desc">
                                    <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                 </div>
                                 <div class="comment-stuff">
                                    <div class="more-opt">
                                       <span class="likeholder">
                                       <span class="like-tooltip">
                                       <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                       <i class="zmdi zmdi-thumb-up"></i>
                                       </a>
                                       </span>
                                       </span>	
                                       <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><i class="zmdi zmdi-mail-reply zmdi-hc-flip-horizontal"></i></a>
                                       <!-- Dropdown Trigger -->
                                       <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='drop_down113'>
                                       <i class="zmdi zmdi-more"></i>
                                       </a>
                                       <!-- Dropdown Structure -->
                                       <ul id='drop_down113' class='dropdown-content custom_dropdown'>
                                          <li>
                                             <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                          </li>
                                          <li>
                                             <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                          </li>
                                       </ul>
                                    </div>
                                    <div class="less-opt">
                                       <div class="timestamp">8h</div>
                                    </div>
                                 </div>
                              </div>
                              <div class="edit-mode">
                                 <div class="desc">
                                    <div class="sliding-middle-out anim-area tt-holder underlined fullwidth">
                                       <textarea class="editcomment-tt materialize-textarea">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                    </div>
                                    <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close"></i></a>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="clear"></div>
                        <div class="comment-reply-holder comment-addreply">
                           <div class="addnew-comment valign-wrapper comment-reply">
                              <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                              <div class="desc-holder">
                                 <div class="sliding-middle-out anim-area tt-holder">
                                    <textarea class="materialize-textarea" placeholder="Write a reply..."></textarea>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="pcomment-holder">
                        <div class="pcomment main-comment">
                           <div class="img-holder">
                              <div class="profiletipholder">
                                 <span class="profile-tooltip">
                                 <img class="circle" src="images/demo-profile.jpg"/>
                                 </span>
                                 <span class="profiletooltip_content">
                                    <div class="profile-tip" style="display:none;">
                                       <div class="slidingpan-holder">
                                          <div class="profile-tip-avatar">
                                             <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">													
                                             <div class="sliding-pan location-span">
                                                <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                             </div>
                                          </div>
                                          <div class="profile-tip-name">
                                             <a href="javascript:void(0)">Adel Hasanat</a>
                                          </div>
                                          <div class="profile-tip-info">
                                             <div class="profiletip-icon">
                                                <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                             </div>
                                             <div class="profiletip-icon">
                                                <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                             </div>
                                             <div class="profiletip-icon">
                                                <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class=”mdi mdi-eye”></i></span></a>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </span>
                              </div>
                           </div>
                           <div class="desc-holder">
                              <div class="normal-mode">
                                 <div class="desc">
                                    <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                 </div>
                                 <div class="comment-stuff">
                                    <div class="more-opt">
                                       <span class="likeholder">
                                       <span class="like-tooltip">
                                       <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                       <i class="zmdi zmdi-thumb-up"></i>
                                       </a>
                                       </span>
                                       </span>	
                                       <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><i class="zmdi zmdi-mail-reply zmdi-hc-flip-horizontal"></i></a>
                                       <!-- Dropdown Trigger -->
                                       <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='drop_down114'>
                                       <i class="zmdi zmdi-more"></i>
                                       </a>
                                       <!-- Dropdown Structure -->
                                       <ul id='drop_down114' class='dropdown-content custom_dropdown'>
                                          <li>
                                             <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                          </li>
                                          <li>
                                             <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                          </li>
                                       </ul>
                                    </div>
                                    <div class="less-opt">
                                       <div class="timestamp">8h</div>
                                    </div>
                                 </div>
                              </div>
                              <div class="edit-mode">
                                 <div class="desc">
                                    <div class="sliding-middle-out anim-area tt-holder underlined fullwidth">
                                       <textarea class="editcomment-tt materialize-textarea">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                    </div>
                                    <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close"></i></a>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="clear"></div>
                        <div class="comment-reply-holder comment-addreply">
                           <div class="addnew-comment valign-wrapper comment-reply">
                              <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                              <div class="desc-holder">
                                 <div class="sliding-middle-out anim-area tt-holder">
                                    <textarea class="materialize-textarea" placeholder="Write a reply..."></textarea>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="pcomment-holder">
                        <div class="pcomment main-comment">
                           <div class="img-holder">
                              <div class="profiletipholder">
                                 <span class="profile-tooltip">
                                 <img class="circle" src="images/demo-profile.jpg"/>
                                 </span>
                                 <span class="profiletooltip_content">
                                    <div class="profile-tip" style="display:none;">
                                       <div class="slidingpan-holder">
                                          <div class="profile-tip-avatar">
                                             <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">													
                                             <div class="sliding-pan location-span">
                                                <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                             </div>
                                          </div>
                                          <div class="profile-tip-name">
                                             <a href="javascript:void(0)">Adel Hasanat</a>
                                          </div>
                                          <div class="profile-tip-info">
                                             <div class="profiletip-icon">
                                                <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                             </div>
                                             <div class="profiletip-icon">
                                                <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                             </div>
                                             <div class="profiletip-icon">
                                                <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class=”mdi mdi-eye”></i></span></a>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </span>
                              </div>
                           </div>
                           <div class="desc-holder">
                              <div class="normal-mode">
                                 <div class="desc">
                                    <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                 </div>
                                 <div class="comment-stuff">
                                    <div class="more-opt">
                                       <span class="likeholder">
                                       <span class="like-tooltip">
                                       <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                       <i class="zmdi zmdi-thumb-up"></i>
                                       </a>
                                       </span>
                                       </span>	
                                       <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><i class="zmdi zmdi-mail-reply zmdi-hc-flip-horizontal"></i></a>
                                       <!-- Dropdown Trigger -->
                                       <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='drop_down115'>
                                       <i class="zmdi zmdi-more"></i>
                                       </a>
                                       <!-- Dropdown Structure -->
                                       <ul id='drop_down115' class='dropdown-content custom_dropdown'>
                                          <li>
                                             <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                          </li>
                                          <li>
                                             <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                          </li>
                                       </ul>
                                    </div>
                                    <div class="less-opt">
                                       <div class="timestamp">8h</div>
                                    </div>
                                 </div>
                              </div>
                              <div class="edit-mode">
                                 <div class="desc">
                                    <div class="sliding-middle-out anim-area tt-holder underlined fullwidth">
                                       <textarea class="editcomment-tt materialize-textarea">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                    </div>
                                    <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="clear"></div>
                        <div class="comment-reply-holder comment-addreply">
                           <div class="addnew-comment valign-wrapper comment-reply">
                              <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                              <div class="desc-holder">
                                 <div class="sliding-middle-out anim-area tt-holder">
                                    <textarea class="materialize-textarea" placeholder="Write a reply..."></textarea>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="pcomment-holder has-comments">
                     <div class="pcomment main-comment">
                        <div class="img-holder">
                           <div class="profiletipholder">
                              <span class="profile-tooltip">
                              <img class="circle" src="images/demo-profile.jpg"/>
                              </span>
                              <span class="profiletooltip_content">
                                 <div class="profile-tip" style="display:none;">
                                    <div class="slidingpan-holder">
                                       <div class="profile-tip-avatar">
                                          <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">													
                                          <div class="sliding-pan location-span">
                                             <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                          </div>
                                       </div>
                                       <div class="profile-tip-name">
                                          <a href="javascript:void(0)">Adel Hasanat</a>
                                       </div>
                                       <div class="profile-tip-info">
                                          <div class="profiletip-icon">
                                             <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                          </div>
                                          <div class="profiletip-icon">
                                             <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                          </div>
                                          <div class="profiletip-icon">
                                             <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class=”mdi mdi-eye”></i></span></a>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </span>
                           </div>
                        </div>
                        <div class="desc-holder">
                           <div class="normal-mode">
                              <div class="desc">
                                 <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                 <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                              </div>
                              <div class="comment-stuff">
                                 <div class="more-opt">
                                    <span class="likeholder">
                                    <span class="like-tooltip">
                                    <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                    <i class="zmdi zmdi-thumb-up"></i>
                                    </a>
                                    </span>
                                    </span>	
                                    <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><i class="zmdi zmdi-mail-reply zmdi-hc-flip-horizontal"></i></a>
                                    <!-- Dropdown Trigger -->
                                    <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='drop_down116'>
                                    <i class="zmdi zmdi-more"></i>
                                    </a>
                                    <!-- Dropdown Structure -->
                                    <ul id='drop_down116' class='dropdown-content custom_dropdown'>
                                       <li>
                                          <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                       </li>
                                       <li>
                                          <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                       </li>
                                    </ul>
                                 </div>
                                 <div class="less-opt">
                                    <div class="timestamp">8h</div>
                                 </div>
                              </div>
                           </div>
                           <div class="edit-mode">
                              <div class="desc">
                                 <div class="sliding-middle-out anim-area tt-holder underlined fullwidth">
                                    <textarea class="editcomment-tt materialize-textarea">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                 </div>
                                 <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close"></i></a>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="clear"></div>
                     <div class="comment-reply-holder">
                        <div class="pcomment comment-reply">
                           <div class="img-holder">
                              <div class="profiletipholder">
                                 <span class="profile-tooltip">
                                 <img class="circle" src="images/demo-profile.jpg"/>
                                 </span>
                                 <span class="profiletooltip_content">
                                    <div class="profile-tip" style="display:none;">
                                       <div class="slidingpan-holder">
                                          <div class="profile-tip-avatar">
                                             <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">													
                                             <div class="sliding-pan location-span">
                                                <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                             </div>
                                          </div>
                                          <div class="profile-tip-name">
                                             <a href="javascript:void(0)">Adel Hasanat</a>
                                          </div>
                                          <div class="profile-tip-info">
                                             <div class="profiletip-icon">
                                                <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                             </div>
                                             <div class="profiletip-icon">
                                                <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                             </div>
                                             <div class="profiletip-icon">
                                                <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class=”mdi mdi-eye”></i></span></a>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </span>
                              </div>
                           </div>
                           <div class="desc-holder">
                              <div class="normal-mode">
                                 <div class="desc">
                                    <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                 </div>
                                 <div class="comment-stuff">
                                    <div class="more-opt">
                                       <span class="likeholder">
                                       <span class="like-tooltip">
                                       <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                       <i class="zmdi zmdi-thumb-up"></i>
                                       </a>
                                       </span>
                                       </span>	
                                       <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><i class="zmdi zmdi-mail-reply zmdi-hc-flip-horizontal"></i></a>
                                       <!-- Dropdown Trigger -->
                                       <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='drop_down117'>
                                       <i class="zmdi zmdi-more"></i>
                                       </a>
                                       <!-- Dropdown Structure -->
                                       <ul id='drop_down117' class='dropdown-content custom_dropdown'>
                                          <li>
                                             <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                          </li>
                                          <li>
                                             <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                          </li>
                                       </ul>
                                    </div>
                                    <div class="less-opt">
                                       <div class="timestamp">8h</div>
                                    </div>
                                 </div>
                              </div>
                              <div class="edit-mode">
                                 <div class="desc">
                                    <div class="sliding-middle-out anim-area tt-holder underlined fullwidth">
                                       <textarea class="editcomment-tt materialize-textarea">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                    </div>
                                    <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close"></i></a>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="pcomment comment-reply">
                           <div class="img-holder">
                              <div class="profiletipholder">
                                 <span class="profile-tooltip">
                                 <img class="circle" src="images/demo-profile.jpg"/>
                                 </span>
                                 <span class="profiletooltip_content">
                                    <div class="profile-tip" style="display:none;">
                                       <div class="slidingpan-holder">
                                          <div class="profile-tip-avatar">
                                             <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">													
                                             <div class="sliding-pan location-span">
                                                <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                             </div>
                                          </div>
                                          <div class="profile-tip-name">
                                             <a href="javascript:void(0)">Adel Hasanat</a>
                                          </div>
                                          <div class="profile-tip-info">
                                             <div class="profiletip-icon">
                                                <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                             </div>
                                             <div class="profiletip-icon">
                                                <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                             </div>
                                             <div class="profiletip-icon">
                                                <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class=”mdi mdi-eye”></i></span></a>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </span>
                              </div>
                           </div>
                           <div class="desc-holder">
                              <div class="normal-mode">
                                 <div class="desc">
                                    <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit...</p>
                                 </div>
                                 <div class="comment-stuff">
                                    <div class="more-opt">
                                       <span class="likeholder">
                                       <span class="like-tooltip">
                                       <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                       <i class="zmdi zmdi-thumb-up"></i>
                                       </a>
                                       </span>
                                       </span>	
                                       <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><i class="zmdi zmdi-mail-reply zmdi-hc-flip-horizontal"></i></a>
                                       <!-- Dropdown Trigger -->
                                       <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='drop_down118'>
                                       <i class="zmdi zmdi-more"></i>
                                       </a>
                                       <!-- Dropdown Structure -->
                                       <ul id='drop_down118' class='dropdown-content custom_dropdown'>
                                          <li>
                                             <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                          </li>
                                          <li>
                                             <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                          </li>
                                       </ul>
                                    </div>
                                    <div class="less-opt">
                                       <div class="timestamp">8h</div>
                                    </div>
                                 </div>
                              </div>
                              <div class="edit-mode">
                                 <div class="desc">
                                    <div class="sliding-middle-out anim-area tt-holder underlined fullwidth">
                                       <textarea class="editcomment-tt materialize-textarea">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                    </div>
                                    <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close"></i></a>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="comment-reply-holder comment-addreply">
                        <div class="addnew-comment valign-wrapper comment-reply">
                           <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                           <div class="desc-holder">
                              <div class="sliding-middle-out anim-area tt-holder">
                                 <textarea class="materialize-textarea" placeholder="Write a reply..."></textarea>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="pcomment-holder">
                     <div class="pcomment main-comment">
                        <div class="img-holder">
                           <div class="profiletipholder">
                              <span class="profile-tooltip">
                              <img class="circle" src="images/demo-profile.jpg"/>
                              </span>
                              <span class="profiletooltip_content">
                                 <div class="profile-tip" style="display:none;">
                                    <div class="slidingpan-holder">
                                       <div class="profile-tip-avatar">
                                          <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">													
                                          <div class="sliding-pan location-span">
                                             <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                          </div>
                                       </div>
                                       <div class="profile-tip-name">
                                          <a href="javascript:void(0)">Adel Hasanat</a>
                                       </div>
                                       <div class="profile-tip-info">
                                          <div class="profiletip-icon">
                                             <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                          </div>
                                          <div class="profiletip-icon">
                                             <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                          </div>
                                          <div class="profiletip-icon">
                                             <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class=”mdi mdi-eye”></i></span></a>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </span>
                           </div>
                        </div>
                        <div class="desc-holder">
                           <div class="normal-mode">
                              <div class="desc">
                                 <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                 <p class="shorten">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.<a a="" href="javascript:void(0)" class="overlay" onclick="explandReadMore(this)" style="position: absolute;top: 0;bottom: -6px;left: 0;right: 0;"><span class="readlink">Read More</span></a></p>
                              </div>
                              <div class="comment-stuff">
                                 <div class="more-opt">
                                    <span class="likeholder">
                                    <span class="like-tooltip">
                                    <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                    <i class="zmdi zmdi-thumb-up"></i>			
                                    </a>
                                    </span>
                                    </span>	
                                    <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><i class="zmdi zmdi-mail-reply zmdi-hc-flip-horizontal"></i></a>
                                    <!-- Dropdown Trigger -->
                                    <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='drop_down119'>
                                    <i class="zmdi zmdi-more"></i>
                                    </a>
                                    <!-- Dropdown Structure -->
                                    <ul id='drop_down119' class='dropdown-content custom_dropdown'>
                                       <li>
                                          <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                       </li>
                                       <li>
                                          <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                       </li>
                                    </ul>
                                 </div>
                                 <div class="less-opt">
                                    <div class="timestamp">8h</div>
                                 </div>
                              </div>
                           </div>
                           <div class="edit-mode">
                              <div class="desc">
                                 <div class="sliding-middle-out anim-area tt-holder fullwidth">
                                    <textarea class="editcomment-tt materialize-textarea">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                 </div>
                                 <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close"></i></a>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="clear"></div>
                     <div class="comment-reply-holder comment-addreply">
                        <div class="addnew-comment valign-wrapper comment-reply">
                           <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                           <div class="desc-holder">
                              <div class="sliding-middle-out anim-area tt-holder">
                                 <textarea class="materialize-textarea" placeholder="Write a reply..."></textarea>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="addnew-comment valign-wrapper">
                  <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                  <div class="desc-holder">
                     <div class="sliding-middle-out anim-area tt-holder">
                        <textarea class="materialize-textarea" id="comment_txt_3" placeholder="Write a comment..."></textarea>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
</div>
