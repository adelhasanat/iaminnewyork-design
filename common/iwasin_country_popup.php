<!--attachment modal-->
<div id="compose_iwasincountry" class="modal compose_inner_modal modalxii_level1 visit-country">
   <div class="content_header">
      <button class="close_span waves-effect">
      <i class="mdi mdi-close mdi-20px material_close"></i>
      </button>
      <p class="selected_person_text"></p>
   </div>
   <div class="person_box">
      <div class="collection visit-country-list">
         <a href="https://www.iamin<?=$st_nm_L?>.com/frontend/web/index.php?r=site%2Fmainfeed" target="_blank" class="collection-item">I was in <?=$st_nm_S?></a>
         <a href="https://www.iaminaqaba.com/frontend/web/index.php?r=site%2Fmainfeed" target="_blank" class="collection-item">I was in Aqaba</a>
         <a href="https://www.iaminmadain.com/frontend/web/index.php?r=site%2Fmainfeed" target="_blank" class="collection-item">I was in Madain</a>
         <a href="https://www.iaminsaudi.com/frontend/web/index.php?r=site%2Fmainfeed" target="_blank" class="collection-item">I was in Saudi</a>
         <a href="https://www.iaminfrance.com/frontend/web/index.php?r=site%2Fmainfeed" target="_blank" class="collection-item">I was in France</a>
         <a href="https://www.iamindubai.com/frontend/web/index.php?r=site%2Fmainfeed" target="_blank" class="collection-item">I was in Dubai</a>
         <a href="https://www.iaminspain.com/frontend/web/index.php?r=site%2Fmainfeed" target="_blank" class="collection-item">I was in Spain</a>
         <a href="https://www.iaminqatar.com/frontend/web/index.php?r=site%2Fmainfeed" target="_blank" class="collection-item">I was in Qatar</a>
         <a href="https://www.iaminbahrain.com/frontend/web/index.php?r=site%2Fmainfeed" target="_blank" class="collection-item">I was in Bahrain</a>
         <a href="https://www.iaminabudhabi.com/frontend/web/index.php?r=site%2Fmainfeed" target="_blank" class="collection-item">I was in Abu Dhabi</a>
         <a href="#!" class="collection-item">I was in USA</a>
         <a href="#!" class="collection-item">I was in Italy</a>
         <a href="#!" class="collection-item">I was in Mexico</a>
         <a href="#!" class="collection-item">I was in Istanbul</a>
         <a href="#!" class="collection-item">I was in China</a>
         <a href="#!" class="collection-item">I was in Germany</a>
         <a href="#!" class="collection-item">I was in Thailand</a>
         <a href="#!" class="collection-item">I was in Austria</a>
         <a href="#!" class="collection-item">I was in Hong Kong</a>
         <a href="#!" class="collection-item">I was in Greece</a>
         <a href="#!" class="collection-item">I was in Russia</a>
         <a href="#!" class="collection-item">I was in <?=$st_nm_S?></a>
         <a href="#!" class="collection-item">I was in Japan</a>
         <a href="#!" class="collection-item">I was in Cyprus</a>
         <a href="#!" class="collection-item">I was in Jamaica</a>
         <a href="#!" class="collection-item">I was in Poland</a>
         <a href="#!" class="collection-item">I was in Zimbabwe</a>
         <a href="#!" class="collection-item">I was in Ukraine</a>
         <a href="#!" class="collection-item">I was in Netherlands</a>
         <a href="#!" class="collection-item">I was in India</a>
         <a href="#!" class="collection-item">I was in Singapore</a>
         <a href="#!" class="collection-item">I was in Morocco</a>
         <a href="#!" class="collection-item">I was in Egypt</a>
         <a href="#!" class="collection-item">I was in Brazil</a>
         <a href="#!" class="collection-item">I was in Australia</a>
         <a href="#!" class="collection-item">I was in Malaysia</a>
         <a href="#!" class="collection-item">I was in Indonesia</a>
         <a href="#!" class="collection-item">I was in Argentina</a>
         <a href="#!" class="collection-item">I was in Zimbabwe</a>
         <a href="#!" class="collection-item">I was in Macao</a>
         <a href="#!" class="collection-item">I was in Croatia</a>
         <a href="#!" class="collection-item">I was in Southkorea</a>
      </div>
   </div>
</div>
