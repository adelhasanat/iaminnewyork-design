<!-- Post detail modal -->
<div id="postopenmodal" class="modal modal_main compose_tool_box custom_modal postopenmodal_main postopenmodal_new">
   <div class="hidden_header">
      <div class="content_header">
         <button class="close_span cancel_poup waves-effect">
         <i class="mdi mdi-close mdi-20px material_close"></i>
         </button>
         <p class="modal_header_xs">Post detail</p>
         <!--<a type="button" class="post_btn action_btn post_active_btn post_btn_xs postbtn savebtn"  onclick="verify()">Save</a>-->
      </div>
   </div>
   <div class="modal-content">
      <div class="mobile-screen mobile-all-comments mobile-popup">
         <div class="main-pcontent spadding">
            <div class="post-column">
               <div class="post-list">
                  <div class="post-holder bshadow">
                     <div class="post-topbar">
                        <div class="post-userinfo">
                           <div class="img-holder">
                              <div id="profiletip-1" class="profiletipholder">
                                 <span class="profile-tooltip tooltipstered">
                                 <img class="circle" src="images/demo-profile.jpg" />
                                 </span>
                              </div>
                           </div>
                           <div class="desc-holder">
                              <a href="javascript:void(0)">Nimish Parekh</a>
                              <span class="timestamp">2 hrs<span class="glyphicon glyphicon-globe"></span></span>
                           </div>
                        </div>
                        <div class="settings-icon">
                           <!-- Dropdown Trigger -->
                           <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='setting_btn2'>
                           <i class="zmdi zmdi-more"></i>
                           </a>
                           <!-- Dropdown Structure -->
                           <ul id='setting_btn2' class='dropdown-content custom_dropdown'>
                              <li>
                                 <a href="javascript:void(0)">Hide post</a>
                              </li>
                              <li>
                                 <a href="javascript:void(0)" class="savepost-link">Save post</a>
                              </li>
                              <li>
                                 <a href="javascript:void(0)">Mute notification for this post</a>
                              </li>
                              <li>
                                 <a href="javascript:void(0)">Mute connect post</a>
                              </li>
                              <li>
                                 <a href="javascript:void(0)">Report post</a>
                              </li>
                              <li>
                                 <a href="javascript:void(0)" class="composeeditpostAction">Edit post</a>
                              </li>
                           </ul>
                        </div>
                     </div>
                     <div class="post-content">
                        <div class="post-details">
                           <div class="post-title"><?=$st_nm_S?> is Nice</div>
                           <div class="post-desc">
                              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sed varius risus. Duis rhoncus eros et pellentesque imperdiet. Praesent pharetra rutrum eros. In nec nulla id enim faucibus pellentesque quis at dolor. Aenean a cursus quam, et fringilla tellus. Vivamus eu lorem est. Donec non urna sit amet arcu dictum dapibus.</p>
                           </div>
                        </div>
                        <div class="post-img-holder">
                           <div class="lgt-gallery post-img one-img">
                              <a href="images/post-img.jpg" data-size="1600x1600" data-med="images/post-img.jpg" data-med-size="1024x1024" data-author="Folkert Gorter" class="pimg-holder himg-box">
                              <img class="himg" src="images/post-img.jpg" alt="" />
                              </a>
                           </div>
                           <a href="javascript:void(0)" class="pinlink"><i class="mdi mdi-nature"></i></a>
                        </div>
                     </div>
                     <div class="clear"></div>
                     <div class="post-data">
                        <div class="post-actions">
                           <span id="likeholder-1" class="likeholder">
                           <span class="like-tooltip tooltipstered">
                           <a href="javascript:void(0)" class="pa-like"><i class="zmdi zmdi-thumb-up"></i></a>
                           </span>
                           </span>							
                           <a href="javascript:void(0)" class="sharepostmodalAction pa-share" title="Share"><i class="zmdi zmdi-mail-reply zmdi-hc-flip-horizontal"></i></a>
                           <a href="javascript:void(0)" class="pa-comment" title="Comment" data-id="1"><i class="zmdi zmdi-comment"></i><span class="lcount">4</span></a>
                        </div>
                        <div class="comments-section panel open">
                           <div class="post-more">
                              <a href="javascript:void(0)" class="view-morec">View more comments <i class="zmdi zmdi-chevron-right"></i></a>
                              <span class="total-comments">3 of 7</span>
                           </div>
                           <div class="post-comments">
                              <div class="pcomments">
                                 <div class="pcomment-earlier">
                                    <div class="pcomment-holder">
                                       <div class="pcomment main-comment">
                                          <div class="img-holder">
                                             <div class="profiletipholder">
                                                <span class="profile-tooltip">
                                                <img class="circle" src="images/demo-profile.jpg"/>
                                                </span>
                                                <span class="profiletooltip_content">
                                                   <div class="profile-tip" style="display:none;">
                                                      <div class="slidingpan-holder">
                                                         <div class="profile-tip-avatar">
                                                            <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">													
                                                            <div class="sliding-pan location-span">
                                                               <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                            </div>
                                                         </div>
                                                         <div class="profile-tip-name">
                                                            <a href="javascript:void(0)">Adel Hasanat</a>
                                                         </div>
                                                         <div class="profile-tip-info">
                                                            <div class="profiletip-icon">
                                                               <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                            </div>
                                                            <div class="profiletip-icon">
                                                               <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                            </div>
                                                            <div class="profiletip-icon">
                                                               <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class=”mdi mdi-eye”></i></span></a>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </span>
                                             </div>
                                          </div>
                                          <div class="desc-holder">
                                             <div class="normal-mode">
                                                <div class="desc">
                                                   <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                                   <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                                </div>
                                                <div class="comment-stuff">
                                                   <div class="more-opt">
                                                      <span class="likeholder">
                                                      <span class="like-tooltip">
                                                      <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                                      <i class="zmdi zmdi-thumb-up"></i>
                                                      </a>
                                                      </span>
                                                      </span>	
                                                      <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><i class="zmdi zmdi-mail-reply zmdi-hc-flip-horizontal"></i></a>
                                                      <!-- Dropdown Trigger -->
                                                      <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='drop_down120'>
                                                      <i class="zmdi zmdi-more"></i>
                                                      </a>
                                                      <!-- Dropdown Structure -->
                                                      <ul id='drop_down120' class='dropdown-content custom_dropdown'>
                                                         <li>
                                                            <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                                         </li>
                                                         <li>
                                                            <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                                         </li>
                                                      </ul>
                                                   </div>
                                                   <div class="less-opt">
                                                      <div class="timestamp">8h</div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="edit-mode">
                                                <div class="desc">
                                                   <div class="sliding-middle-out anim-area tt-holder underlined fullwidth">
                                                      <textarea class="editcomment-tt materialize-textarea">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                                   </div>
                                                   <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="clear"></div>
                                       <div class="comment-reply-holder comment-addreply">
                                          <div class="addnew-comment valign-wrapper comment-reply">
                                             <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                                             <div class="desc-holder">
                                                <div class="sliding-middle-out anim-area tt-holder">
                                                   <textarea class="materialize-textarea" placeholder="Write a reply..."></textarea>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="pcomment-holder">
                                       <div class="pcomment main-comment">
                                          <div class="img-holder">
                                             <div class="profiletipholder">
                                                <span class="profile-tooltip">
                                                <img class="circle" src="images/demo-profile.jpg"/>
                                                </span>
                                                <span class="profiletooltip_content">
                                                   <div class="profile-tip" style="display:none;">
                                                      <div class="slidingpan-holder">
                                                         <div class="profile-tip-avatar">
                                                            <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">													
                                                            <div class="sliding-pan location-span">
                                                               <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                            </div>
                                                         </div>
                                                         <div class="profile-tip-name">
                                                            <a href="javascript:void(0)">Adel Hasanat</a>
                                                         </div>
                                                         <div class="profile-tip-info">
                                                            <div class="profiletip-icon">
                                                               <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                            </div>
                                                            <div class="profiletip-icon">
                                                               <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                            </div>
                                                            <div class="profiletip-icon">
                                                               <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class=”mdi mdi-eye”></i></span></a>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </span>
                                             </div>
                                          </div>
                                          <div class="desc-holder">
                                             <div class="normal-mode">
                                                <div class="desc">
                                                   <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                                   <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                                </div>
                                                <div class="comment-stuff">
                                                   <div class="more-opt">
                                                      <span class="likeholder">
                                                      <span class="like-tooltip">
                                                      <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                                      <i class="zmdi zmdi-thumb-up"></i>
                                                      </a>
                                                      </span>
                                                      </span>	
                                                      <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><i class="zmdi zmdi-mail-reply zmdi-hc-flip-horizontal"></i></a>
                                                      <!-- Dropdown Trigger -->
                                                      <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='drop_down121'>
                                                      <i class="zmdi zmdi-more"></i>
                                                      </a>
                                                      <!-- Dropdown Structure -->
                                                      <ul id='drop_down121' class='dropdown-content custom_dropdown'>
                                                         <li>
                                                            <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                                         </li>
                                                         <li>
                                                            <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                                         </li>
                                                      </ul>
                                                   </div>
                                                   <div class="less-opt">
                                                      <div class="timestamp">8h</div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="edit-mode">
                                                <div class="desc">
                                                   <div class="sliding-middle-out anim-area tt-holder underlined fullwidth">
                                                      <textarea class="editcomment-tt materialize-textarea">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                                   </div>
                                                   <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="clear"></div>
                                       <div class="comment-reply-holder comment-addreply">
                                          <div class="addnew-comment valign-wrapper comment-reply">
                                             <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                                             <div class="desc-holder">
                                                <div class="sliding-middle-out anim-area tt-holder">
                                                   <textarea class="materialize-textarea" placeholder="Write a reply..."></textarea>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="pcomment-holder has-comments">
                                    <div class="pcomment main-comment">
                                       <div class="img-holder">
                                          <div class="profiletipholder">
                                             <span class="profile-tooltip">
                                             <img class="circle" src="images/demo-profile.jpg"/>
                                             </span>
                                             <span class="profiletooltip_content">
                                                <div class="profile-tip" style="display:none;">
                                                   <div class="slidingpan-holder">
                                                      <div class="profile-tip-avatar">
                                                         <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">													
                                                         <div class="sliding-pan location-span">
                                                            <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                         </div>
                                                      </div>
                                                      <div class="profile-tip-name">
                                                         <a href="javascript:void(0)">Adel Hasanat</a>
                                                      </div>
                                                      <div class="profile-tip-info">
                                                         <div class="profiletip-icon">
                                                            <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                         </div>
                                                         <div class="profiletip-icon">
                                                            <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                         </div>
                                                         <div class="profiletip-icon">
                                                            <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class=”mdi mdi-eye”></i></span></a>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </span>
                                          </div>
                                       </div>
                                       <div class="desc-holder">
                                          <div class="normal-mode">
                                             <div class="desc">
                                                <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                             </div>
                                             <div class="comment-stuff">
                                                <div class="more-opt">
                                                   <span class="likeholder">
                                                   <span class="like-tooltip">
                                                   <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                                   <i class="zmdi zmdi-thumb-up"></i>
                                                   </a>
                                                   </span>
                                                   </span>	
                                                   <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><i class="zmdi zmdi-mail-reply zmdi-hc-flip-horizontal"></i></a>
                                                   <!-- Dropdown Trigger -->
                                                   <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='drop_down122'>
                                                   <i class="zmdi zmdi-more"></i>
                                                   </a>
                                                   <!-- Dropdown Structure -->
                                                   <ul id='drop_down112' class='dropdown-content custom_dropdown'>
                                                      <li>
                                                         <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                                      </li>
                                                      <li>
                                                         <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                                      </li>
                                                   </ul>
                                                </div>
                                                <div class="less-opt">
                                                   <div class="timestamp">8h</div>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="edit-mode">
                                             <div class="desc">
                                                <div class="sliding-middle-out anim-area tt-holder underlined fullwidth">
                                                   <textarea class="editcomment-tt materialize-textarea">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                                </div>
                                                <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="clear"></div>
                                    <div class="comment-reply-holder">
                                       <div class="comments-reply-summery">
                                          <a href="javascript:void(0)" onclick="openReplies(this)">
                                          <i class="mdi mdi-share"></i>
                                          2 Replies													
                                          </a>
                                          <i class="mdi mdi-bullseye dot-i"></i>
                                          Just Now
                                       </div>
                                       <div class="comments-reply-details">
                                          <div class="pcomment comment-reply">
                                             <div class="img-holder">
                                                <div class="profiletipholder">
                                                   <span class="profile-tooltip">
                                                   <img class="circle" src="images/demo-profile.jpg"/>
                                                   </span>
                                                   <span class="profiletooltip_content">
                                                      <div class="profile-tip" style="display:none;">
                                                         <div class="slidingpan-holder">
                                                            <div class="profile-tip-avatar">
                                                               <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">													
                                                               <div class="sliding-pan location-span">
                                                                  <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                               </div>
                                                            </div>
                                                            <div class="profile-tip-name">
                                                               <a href="javascript:void(0)">Adel Hasanat</a>
                                                            </div>
                                                            <div class="profile-tip-info">
                                                               <div class="profiletip-icon">
                                                                  <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                               </div>
                                                               <div class="profiletip-icon">
                                                                  <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                               </div>
                                                               <div class="profiletip-icon">
                                                                  <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class=”mdi mdi-eye”></i></span></a>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </span>
                                                </div>
                                             </div>
                                             <div class="desc-holder">
                                                <div class="normal-mode">
                                                   <div class="desc">
                                                      <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                                      <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                                   </div>
                                                   <div class="comment-stuff">
                                                      <div class="more-opt">
                                                         <span class="likeholder">
                                                         <span class="like-tooltip">
                                                         <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                                         <i class="zmdi zmdi-thumb-up"></i>
                                                         </a>
                                                         </span>
                                                         </span>	
                                                         <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><i class="zmdi zmdi-mail-reply zmdi-hc-flip-horizontal"></i></a>
                                                         <!-- Dropdown Trigger -->
                                                         <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='drop_down123'>
                                                         <i class="zmdi zmdi-more"></i>
                                                         </a>
                                                         <!-- Dropdown Structure -->
                                                         <ul id='drop_down123' class='dropdown-content custom_dropdown'>
                                                            <li>
                                                               <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                                            </li>
                                                            <li>
                                                               <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                                            </li>
                                                         </ul>
                                                      </div>
                                                      <div class="less-opt">
                                                         <div class="timestamp">8h</div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="edit-mode">
                                                   <div class="desc">
                                                      <div class="sliding-middle-out anim-area tt-holder underlined fullwidth">
                                                         <textarea class="editcomment-tt materialize-textarea">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                                      </div>
                                                      <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="pcomment comment-reply">
                                             <div class="img-holder">
                                                <div class="profiletipholder">
                                                   <span class="profile-tooltip">
                                                   <img class="circle" src="images/demo-profile.jpg"/>
                                                   </span>
                                                   <span class="profiletooltip_content">
                                                      <div class="profile-tip" style="display:none;">
                                                         <div class="slidingpan-holder">
                                                            <div class="profile-tip-avatar">
                                                               <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">													
                                                               <div class="sliding-pan location-span">
                                                                  <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                               </div>
                                                            </div>
                                                            <div class="profile-tip-name">
                                                               <a href="javascript:void(0)">Adel Hasanat</a>
                                                            </div>
                                                            <div class="profile-tip-info">
                                                               <div class="profiletip-icon">
                                                                  <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                               </div>
                                                               <div class="profiletip-icon">
                                                                  <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                               </div>
                                                               <div class="profiletip-icon">
                                                                  <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class=”mdi mdi-eye”></i></span></a>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </span>
                                                </div>
                                             </div>
                                             <div class="desc-holder">
                                                <div class="normal-mode">
                                                   <div class="desc">
                                                      <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                                      <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit...</p>
                                                   </div>
                                                   <div class="comment-stuff">
                                                      <div class="more-opt">
                                                         <span class="likeholder">
                                                         <span class="like-tooltip">
                                                         <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                                         <i class="zmdi zmdi-thumb-up"></i>
                                                         </a>
                                                         </span>
                                                         </span>	
                                                         <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><i class="zmdi zmdi-mail-reply zmdi-hc-flip-horizontal"></i></a>
                                                         <!-- Dropdown Trigger -->
                                                         <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='drop_down124'>
                                                         <i class="zmdi zmdi-more"></i>
                                                         </a>
                                                         <!-- Dropdown Structure -->
                                                         <ul id='drop_down124' class='dropdown-content custom_dropdown'>
                                                            <li>
                                                               <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                                            </li>
                                                            <li>
                                                               <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                                            </li>
                                                         </ul>
                                                      </div>
                                                      <div class="less-opt">
                                                         <div class="timestamp">8h</div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="edit-mode">
                                                   <div class="desc">
                                                      <div class="sliding-middle-out anim-area tt-holder underlined fullwidth">
                                                         <textarea class="editcomment-tt materialize-textarea">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                                      </div>
                                                      <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="comment-reply-holder comment-addreply">
                                       <div class="addnew-comment valign-wrapper comment-reply">
                                          <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                                          <div class="desc-holder">
                                             <div class="sliding-middle-out anim-area tt-holder">
                                                <textarea class="materialize-textarea" placeholder="Write a reply..."></textarea>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="pcomment-holder">
                                    <div class="pcomment main-comment">
                                       <div class="img-holder">
                                          <div class="profiletipholder">
                                             <span class="profile-tooltip">
                                             <img class="circle" src="images/demo-profile.jpg"/>
                                             </span>
                                             <span class="profiletooltip_content">
                                                <div class="profile-tip" style="display:none;">
                                                   <div class="slidingpan-holder">
                                                      <div class="profile-tip-avatar">
                                                         <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">													
                                                         <div class="sliding-pan location-span">
                                                            <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                         </div>
                                                      </div>
                                                      <div class="profile-tip-name">
                                                         <a href="javascript:void(0)">Adel Hasanat</a>
                                                      </div>
                                                      <div class="profile-tip-info">
                                                         <div class="profiletip-icon">
                                                            <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                         </div>
                                                         <div class="profiletip-icon">
                                                            <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                         </div>
                                                         <div class="profiletip-icon">
                                                            <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class=”mdi mdi-eye”></i></span></a>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </span>
                                          </div>
                                       </div>
                                       <div class="desc-holder">
                                          <div class="normal-mode">
                                             <div class="desc">
                                                <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                                <p class="shorten">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.<a a="" href="javascript:void(0)" class="overlay" onclick="explandReadMore(this)" style="position: absolute;top: 0;bottom: -6px;left: 0;right: 0;"><span class="readlink">Read More</span></a></p>
                                             </div>
                                             <div class="comment-stuff">
                                                <div class="more-opt">
                                                   <span class="likeholder">
                                                   <span class="like-tooltip">
                                                   <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                                   <i class="zmdi zmdi-thumb-up"></i>			
                                                   </a>
                                                   </span>
                                                   </span>	
                                                   <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><i class="zmdi zmdi-mail-reply zmdi-hc-flip-horizontal"></i></a>
                                                   <!-- Dropdown Trigger -->
                                                   <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='drop_down125'>
                                                   <i class="zmdi zmdi-more"></i>
                                                   </a>
                                                   <!-- Dropdown Structure -->
                                                   <ul id='drop_down125' class='dropdown-content custom_dropdown'>
                                                      <li>
                                                         <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                                      </li>
                                                      <li>
                                                         <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                                      </li>
                                                   </ul>
                                                </div>
                                                <div class="less-opt">
                                                   <div class="timestamp">8h</div>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="edit-mode">
                                             <div class="desc">
                                                <div class="sliding-middle-out anim-area tt-holder fullwidth">
                                                   <textarea class="editcomment-tt materialize-textarea">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                                </div>
                                                <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="clear"></div>
                                    <div class="comment-reply-holder comment-addreply">
                                       <div class="addnew-comment valign-wrapper comment-reply">
                                          <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                                          <div class="desc-holder">
                                             <div class="sliding-middle-out anim-area tt-holder">
                                                <textarea class="materialize-textarea" placeholder="Write a reply..."></textarea>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="addnew-comment valign-wrapper">
                                 <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                                 <div class="desc-holder">
                                    <div class="sliding-middle-out anim-area tt-holder">
                                       <textarea class="materialize-textarea" id="comment_txt_3" placeholder="Write a comment..."></textarea>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>