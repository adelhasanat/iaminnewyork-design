<div class="row">
   <div class="col s12 m12">
      <div class="new-post base-newpost">
         <form action="">
            <div class="npost-content">
               <div class="post-mcontent">
                  <i class="mdi mdi-pencil-box-outline main-icon"></i>
                  <div class="desc">
                     <div class="input-field comments_box">
                        <input placeholder="What's new?" type="text" class="validate commentmodalAction_form" />
                     </div>
                  </div>
               </div>
            </div>
         </form>
         <div class="overlay" id="composetoolboxAction"></div>
      </div>
   </div>
</div>
<div class="post-list margint15">
   <div class="row">
      <!-- main post -->
      <div class="col s12 m12">
         <div class="post-holder">
            <div class="joined-tb">
               <i class="mdi mdi-home"></i>                      
               <h4>Welcome to Iamin<?=$st_nm_L?></h4>
               <p>Add connections to see more posts and photos in your feed</p>
            </div>
         </div>
      </div>
      <div class="col s12 m12">
         <!-- shared trip -->
         <div class="post-holder">
            <div class="post-topbar">
               <div class="post-userinfo">
                  <div class="img-holder">
                     <div class="profiletipholder">
                        <span class="profile-tooltip">
                        <img class="circle" src="images/demo-profile.jpg"/>
                        </span>
                        <span class="profiletooltip_content">
                           <div class="profile-tip" style="display:none;">
                              <div class="slidingpan-holder">
                                 <div class="profile-tip-avatar">
                                    <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                    <div class="sliding-pan location-span">
                                       <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                    </div>
                                 </div>
                                 <div class="profile-tip-name">
                                    <a href="javascript:void(0)">Adel Hasanat</a>
                                 </div>
                                 <div class="profile-tip-info">
                                    <div class="profiletip-icon">
                                       <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                    </div>
                                    <div class="profiletip-icon">
                                       <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                    </div>
                                    <div class="profiletip-icon">
                                       <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </span>
                     </div>
                  </div>
                  <div class="desc-holder">
                     <a href="javascript:void(0)">Nimish Parekh</a> is travelling to Germany with <a href="javascript:void(0)" class="sub-link">Smith Patel</a> and
                     <span class="likeholder">
                     <span class="like-tooltip">
                     <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name">
                     3 others
                     </a>
                     </span>
                     </span>
                     from <?=$st_nm_S?>
                     <span class="timestamp">2 hrs<span class="glyphicon glyphicon-globe"></span></span>
                  </div>
               </div>
			   
               <div class="settings-icon">
			   <div class="dropdown dropdown-custom dropdown-xxsmall">
				  <a class="dropdown-button more_btn" href="javascript:void(0);" data-activates="setting_btn1" style="">					
                  <i class="zmdi zmdi-more"></i>
				  </a>
				  <ul id="setting_btn1" class="dropdown-content custom_dropdown" >
					 <li>
                        <a href="javascript:void(0)">Hide post</a>
                     </li>
                     <li>
                        <a href="javascript:void(0)" class="savepost-link">Save post</a>
                     </li>
                     <li>
                        <a href="javascript:void(0)">Mute notification for this post</a>
                     </li>
                     <li>
                        <a href="javascript:void(0)">Mute connect post</a>
                     </li>
                     <li>
                        <a href="javascript:void(0)">Report post</a>
                     </li>
                     <li>
                        <a href="javascript:void(0)" class="composeeditpostAction">Edit post</a>
                     </li>
				  </ul>				 
				</div>
                  <!-- Dropdown Structure -->
               </div>
            </div>
            <div class="post-content">
               <div class="pdetail-holder">
                  <div class="post-details">
                     <div class="post-desc">
                        <p>I am going have an awesome trip experience. Checkout this!</p>
                     </div>
                  </div>
                  <div class="shared-box">
                     <div class="post-holder">
                        <div class="post-topbar">
                           <div class="post-userinfo">
                              <div class="img-holder">
                                 <div class="profiletipholder">
                                    <span class="profile-tooltip">
                                    <img class="circle" src="images/demo-profile.jpg"/>
                                    </span>
                                    <span class="profiletooltip_content">
                                       <div class="profile-tip" style="display:none;">
                                          <div class="slidingpan-holder">
                                             <div class="profile-tip-avatar">
                                                <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                                <div class="sliding-pan location-span">
                                                   <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                </div>
                                             </div>
                                             <div class="profile-tip-name">
                                                <a href="javascript:void(0)">Adel Hasanat</a>
                                             </div>
                                             <div class="profile-tip-info">
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                </div>
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                </div>
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </span>
                                 </div>
                              </div>
                              <div class="desc-holder">
                                 <span>By </span><a href="javascript:void(0)">Nimish Parekh</a> <span class="withtext">-- with <a href="javascript:void(0)">Alap Shah</a> and <a href="javascript:void(0)">4 others</a></span>
                                 <span class="timestamp">2 hrs<span class="glyphicon glyphicon-globe"></span></span>
                              </div>
                           </div>
                        </div>
                        <div class="post-content tripexperince-post">
                           <div class="post-details">
                              <div class="post-title">
                                 Trip to Germany
                              </div>
                           </div>
                           <div class="trip-summery">
                              <div class="route-holder">
                                 <label>Stops :</label>
                                 <ul class="triproute">
                                    <li>Nuremberg</li>
                                    <li>Frankfurt</li>
                                    <li>Hanover</li>
                                 </ul>
                              </div>
                              <div class="location-info">
                                 <h5><i class="zmdi zmdi-pin"></i> Trip Route</h5>
                                 <i class="mdi mdi-menu-right"></i>

                                 <a href="javascript:void(0)" onclick="openViewMap(this)">View on map</a>
                              </div>
                           </div>
                           <div class="map-holder">
                              <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d5133587.5929341!2d5.968402169270499!3d51.07829557865965!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x479a721ec2b1be6b%3A0x75e85d6b8e91e55b!2sGermany!5e0!3m2!1sen!2sin!4v1490074760301" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                           </div>
                           <div class="post-details">
                              <div class="post-desc">
                                 <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sed varius risus. Duis rhoncus eros et pellentesque imperdiet. Praesent pharetra rutrum eros. In nec nulla id enim faucibus pellentesque quis at dolor. Aenean a cursus quam, et fringilla tellus. Vivamus eu lorem est. Donec non urna sit amet arcu dictum dapibus.</p>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="clear"></div>
            <div class="post-data">
               <div class="post-actions">
                  <span class="likeholder">
                  <span class="like-tooltip">
                  <a href="javascript:void(0)" class="btn waves-teal ramani bhadresh pa-like tooltipstered liveliketooltip active" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                  <i class="zmdi zmdi-thumb-up"></i>
                  </a>
                  <span class="lcount">7</span>
                  </span>
                  </span>                    
                  <a href="javascript:void(0)" class="sharepostmodalAction pa-share" title="Share"><i class="zmdi zmdi-mail-reply zmdi-hc-flip-horizontal"></i></a><span class="lcount">4</span>
                  <a href="javascript:void(0)" class="pa-publish" title="Publish"><i class="zmdi zmdi-upload"></i></a>
                  <span class="right">
                     <a href="javascript:void(0)" class="pa-comment" title="Comment" data-id="1"><i class="zmdi zmdi-comment"></i></a><span class="comment-lcount">4</span>
                  </span>
               </div>
               <div class="comments-section panel">
                  <div class="post-more">
                     <a href="javascript:void(0)" class="view-morec">View more comments <i class="zmdi zmdi-chevron-right"></i></a>
                     <span class="total-comments">3 of 7</span>
                  </div>
                  <div class="post-comments">
                     <div class="pcomments">
                        <div class="pcomment-earlier">
                           <div class="pcomment-holder">
                              <div class="pcomment main-comment">
                                 <div class="img-holder">
                                    <div class="profiletipholder">
                                       <span class="profile-tooltip">
                                       <img class="circle" src="images/demo-profile.jpg"/>
                                       </span>
                                       <span class="profiletooltip_content">
                                          <div class="profile-tip" style="display:none;">
                                             <div class="slidingpan-holder">
                                                <div class="profile-tip-avatar">
                                                   <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                                   <div class="sliding-pan location-span">
                                                      <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                   </div>
                                                </div>
                                                <div class="profile-tip-name">
                                                   <a href="javascript:void(0)">Adel Hasanat</a>
                                                </div>
                                                <div class="profile-tip-info">
                                                   <div class="profiletip-icon">
                                                      <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                   </div>
                                                   <div class="profiletip-icon">
                                                      <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                   </div>
                                                   <div class="profiletip-icon">
                                                      <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </span>
                                    </div>
                                 </div>
                                 <div class="desc-holder">
                                    <div class="normal-mode">
                                       <div class="desc">
                                          <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                          <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                       </div>
                                       <div class="comment-stuff">
                                          <div class="more-opt">
                                             <span class="likeholder">
                                             <span class="like-tooltip">
                                             <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                             <i class="zmdi zmdi-thumb-up"></i>
                                             </a>
                                             </span>
                                             </span>  
                                             <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><i class="zmdi zmdi-mail-reply zmdi-hc-flip-horizontal"></i></a>
                                             <!-- Dropdown Trigger -->
                                             <a class='dropdown-button more_btn' href="javascript:void(0)" data-activates='drop_down2'>
                                             <i class="zmdi zmdi-more"></i>
                                             </a>
                                             <!-- Dropdown Structure -->
                                             <ul id='drop_down2' class='dropdown-content custom_dropdown'>
                                                <li>
                                                   <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                                </li>
                                                <li>
                                                   <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                                </li>
                                             </ul>
                                          </div>
                                          <div class="less-opt">
                                             <div class="timestamp">8h</div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="edit-mode">
                                       <div class="desc">
                                          <div class="sliding-middle-out anim-area tt-holder underlined fullwidth">
                                             <textarea class="editcomment-tt materialize-textarea">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                          </div>
                                          <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="clear"></div>
                              <div class="comment-reply-holder comment-addreply">
                                 <div class="addnew-comment valign-wrapper  comment-reply">
                                    <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                                    <div class="desc-holder">
                                       <div class="sliding-middle-out anim-area tt-holder">
                                          <textarea class="materialize-textarea" placeholder="Write a reply..."></textarea>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="pcomment-holder">
                              <div class="pcomment main-comment">
                                 <div class="img-holder">
                                    <div class="profiletipholder">
                                       <span class="profile-tooltip">
                                       <img class="circle" src="images/demo-profile.jpg"/>
                                       </span>
                                       <span class="profiletooltip_content">
                                          <div class="profile-tip" style="display:none;">
                                             <div class="slidingpan-holder">
                                                <div class="profile-tip-avatar">
                                                   <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                                   <div class="sliding-pan location-span">
                                                      <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                   </div>
                                                </div>
                                                <div class="profile-tip-name">
                                                   <a href="javascript:void(0)">Adel Hasanat</a>
                                                </div>
                                                <div class="profile-tip-info">
                                                   <div class="profiletip-icon">
                                                      <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                   </div>
                                                   <div class="profiletip-icon">
                                                      <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                   </div>
                                                   <div class="profiletip-icon">
                                                      <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </span>
                                    </div>
                                 </div>
                                 <div class="desc-holder">
                                    <div class="normal-mode">
                                       <div class="desc">
                                          <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                          <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                       </div>
                                       <div class="comment-stuff">
                                          <div class="more-opt">
                                             <span class="likeholder">
                                             <span class="like-tooltip">
                                             <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                             <i class="zmdi zmdi-thumb-up"></i>
                                             </a>
                                             </span>
                                             </span>  
                                             <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><i class="zmdi zmdi-mail-reply zmdi-hc-flip-horizontal"></i></a>
                                             <!-- Dropdown Trigger -->
                                             <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='drop_down3'>
                                             <i class="zmdi zmdi-more"></i>
                                             </a>
                                             <!-- Dropdown Structure -->
                                             <ul id='drop_down3' class='dropdown-content custom_dropdown'>
                                                <li>
                                                   <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                                </li>
                                                <li>
                                                   <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                                </li>
                                             </ul>
                                          </div>
                                          <div class="less-opt">
                                             <div class="timestamp">8h</div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="edit-mode">
                                       <div class="desc">
                                          <div class="sliding-middle-out anim-area tt-holder underlined fullwidth">
                                             <textarea class="editcomment-tt materialize-textarea">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                          </div>
                                          <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="clear"></div>
                              <div class="comment-reply-holder comment-addreply">
                                 <div class="addnew-comment valign-wrapper  comment-reply">
                                    <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                                    <div class="desc-holder">
                                       <div class="sliding-middle-out anim-area tt-holder">
                                          <textarea class="materialize-textarea" placeholder="Write a reply..."></textarea>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="pcomment-holder has-comments">
                           <div class="pcomment main-comment">
                              <div class="img-holder">
                                 <div class="profiletipholder">
                                    <span class="profile-tooltip">
                                    <img class="circle" src="images/demo-profile.jpg"/>
                                    </span>
                                    <span class="profiletooltip_content">
                                       <div class="profile-tip" style="display:none;">
                                          <div class="slidingpan-holder">
                                             <div class="profile-tip-avatar">
                                                <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                                <div class="sliding-pan location-span">
                                                   <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                </div>
                                             </div>
                                             <div class="profile-tip-name">
                                                <a href="javascript:void(0)">Adel Hasanat</a>
                                             </div>
                                             <div class="profile-tip-info">
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                </div>
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                </div>
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </span>
                                 </div>
                              </div>
                              <div class="desc-holder">
                                 <div class="normal-mode">
                                    <div class="desc">
                                       <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                       <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                    </div>
                                    <div class="comment-stuff">
                                       <div class="more-opt">
                                          <span class="likeholder">
                                          <span class="like-tooltip">
                                          <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                          <i class="zmdi zmdi-thumb-up"></i>
                                          </a>
                                          </span>
                                          </span>  
                                          <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><i class="zmdi zmdi-mail-reply zmdi-hc-flip-horizontal"></i></a>
                                          <!-- Dropdown Trigger -->
                                          <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='drop_down4'>
                                          <i class="zmdi zmdi-more"></i>
                                          </a>
                                          <!-- Dropdown Structure -->
                                          <ul id='drop_down4' class='dropdown-content custom_dropdown'>
                                             <li>
                                                <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                             </li>
                                             <li>
                                                <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                             </li>
                                          </ul>
                                       </div>
                                       <div class="less-opt">
                                          <div class="timestamp">8h</div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="edit-mode">
                                    <div class="desc">
                                       <div class="sliding-middle-out anim-area tt-holder underlined fullwidth">
                                          <textarea class="editcomment-tt materialize-textarea">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                       </div>
                                       <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="clear"></div>
                           <div class="comment-reply-holder">
                              <div class="comments-reply-summery">
                                 <a href="javascript:void(0)" onclick="openReplies(this)">
                                 <i class="mdi mdi-share"></i>
                                 2 Replies                                       
                                 </a>
                                 <i class="mdi mdi-bullseye dot-i"></i>
                                 Just Now
                              </div>
                              <div class="comments-reply-details">
                                 <div class="pcomment comment-reply">
                                    <div class="img-holder">
                                       <div class="profiletipholder">
                                          <span class="profile-tooltip">
                                          <img class="circle" src="images/demo-profile.jpg"/>
                                          </span>
                                          <span class="profiletooltip_content">
                                             <div class="profile-tip" style="display:none;">
                                                <div class="slidingpan-holder">
                                                   <div class="profile-tip-avatar">
                                                      <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                                      <div class="sliding-pan location-span">
                                                         <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                      </div>
                                                   </div>
                                                   <div class="profile-tip-name">
                                                      <a href="javascript:void(0)">Adel Hasanat</a>
                                                   </div>
                                                   <div class="profile-tip-info">
                                                      <div class="profiletip-icon">
                                                         <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                      </div>
                                                      <div class="profiletip-icon">
                                                         <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                      </div>
                                                      <div class="profiletip-icon">
                                                         <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </span>
                                       </div>
                                    </div>
                                    <div class="desc-holder">
                                       <div class="normal-mode">
                                          <div class="desc">
                                             <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                             <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                          </div>
                                          <div class="comment-stuff">
                                             <div class="more-opt">
                                                <span class="likeholder">
                                                <span class="like-tooltip">
                                                <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                                <i class="zmdi zmdi-thumb-up"></i>
                                                </a>
                                                </span>
                                                </span>  
                                                <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><i class="zmdi zmdi-mail-reply zmdi-hc-flip-horizontal"></i></a>
                                                <!-- Dropdown Trigger -->
                                                <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='drop_down5'>
                                                <i class="zmdi zmdi-more"></i>
                                                </a>
                                                <!-- Dropdown Structure -->
                                                <ul id='drop_down5' class='dropdown-content custom_dropdown'>
                                                   <li>
                                                      <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                                   </li>
                                                   <li>
                                                      <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                                   </li>
                                                </ul>
                                             </div>
                                             <div class="less-opt">
                                                <div class="timestamp">8h</div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="edit-mode">
                                          <div class="desc">
                                             <div class="sliding-middle-out anim-area tt-holder underlined fullwidth">
                                                <textarea class="editcomment-tt materialize-textarea">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                             </div>
                                             <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="pcomment comment-reply">
                                    <div class="img-holder">
                                       <div class="profiletipholder">
                                          <span class="profile-tooltip">
                                          <img class="circle" src="images/demo-profile.jpg"/>
                                          </span>
                                          <span class="profiletooltip_content">
                                             <div class="profile-tip" style="display:none;">
                                                <div class="slidingpan-holder">
                                                   <div class="profile-tip-avatar">
                                                      <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                                      <div class="sliding-pan location-span">
                                                         <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                      </div>
                                                   </div>
                                                   <div class="profile-tip-name">
                                                      <a href="javascript:void(0)">Adel Hasanat</a>
                                                   </div>
                                                   <div class="profile-tip-info">
                                                      <div class="profiletip-icon">
                                                         <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                      </div>
                                                      <div class="profiletip-icon">
                                                         <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                      </div>
                                                      <div class="profiletip-icon">
                                                         <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </span>
                                       </div>
                                    </div>
                                    <div class="desc-holder">
                                       <div class="normal-mode">
                                          <div class="desc">
                                             <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                             <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit...</p>
                                          </div>
                                          <div class="comment-stuff">
                                             <div class="more-opt">
                                                <span class="likeholder">
                                                <span class="like-tooltip">
                                                <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                                <i class="zmdi zmdi-thumb-up"></i>
                                                </a>
                                                </span>
                                                </span>  
                                                <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><i class="zmdi zmdi-mail-reply zmdi-hc-flip-horizontal"></i></a>
                                                <!-- Dropdown Trigger -->
                                                <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='drop_down6'>
                                                <i class="zmdi zmdi-more"></i>
                                                </a>
                                                <!-- Dropdown Structure -->
                                                <ul id='drop_down6' class='dropdown-content custom_dropdown'>
                                                   <li>
                                                      <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                                   </li>
                                                   <li>
                                                      <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                                   </li>
                                                </ul>
                                             </div>
                                             <div class="less-opt">
                                                <div class="timestamp">8h</div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="edit-mode">
                                          <div class="desc">
                                             <div class="sliding-middle-out anim-area tt-holder underlined fullwidth">
                                                <textarea class="editcomment-tt materialize-textarea">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                             </div>
                                             <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="comment-reply-holder comment-addreply">
                              <div class="addnew-comment valign-wrapper  comment-reply">
                                 <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                                 <div class="desc-holder">
                                    <div class="sliding-middle-out anim-area tt-holder">
                                       <textarea class="materialize-textarea" placeholder="Write a reply..."></textarea>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="pcomment-holder">
                           <div class="pcomment main-comment">
                              <div class="img-holder">
                                 <div class="profiletipholder">
                                    <span class="profile-tooltip">
                                    <img class="circle" src="images/demo-profile.jpg"/>
                                    </span>
                                    <span class="profiletooltip_content">
                                       <div class="profile-tip" style="display:none;">
                                          <div class="slidingpan-holder">
                                             <div class="profile-tip-avatar">
                                                <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                                <div class="sliding-pan location-span">
                                                   <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                </div>
                                             </div>
                                             <div class="profile-tip-name">
                                                <a href="javascript:void(0)">Adel Hasanat</a>
                                             </div>
                                             <div class="profile-tip-info">
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                </div>
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                </div>
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </span>
                                 </div>
                              </div>
                              <div class="desc-holder">
                                 <div class="normal-mode">
                                    <div class="desc">
                                       <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                       <p class="shorten">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.<a a="" href="javascript:void(0)" class="overlay" onclick="explandReadMore(this)" style="position: absolute;top: 0;bottom: -6px;left: 0;right: 0;"><span class="readlink">Read More</span></a></p>
                                    </div>
                                    <div class="comment-stuff">
                                       <div class="more-opt">
                                          <span class="likeholder">
                                          <span class="like-tooltip">
                                          <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                          <i class="zmdi zmdi-thumb-up"></i>       
                                          </a>
                                          </span>
                                          </span>  
                                          <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><i class="zmdi zmdi-mail-reply zmdi-hc-flip-horizontal"></i></a>
                                          <!-- Dropdown Trigger -->
                                          <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='drop_down7'>
                                          <i class="zmdi zmdi-more"></i>
                                          </a>
                                          <!-- Dropdown Structure -->
                                          <ul id='drop_down7' class='dropdown-content custom_dropdown'>
                                             <li>
                                                <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                             </li>
                                             <li>
                                                <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                             </li>
                                          </ul>
                                       </div>
                                       <div class="less-opt">
                                          <div class="timestamp">8h</div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="edit-mode">
                                    <div class="desc">
                                       <div class="sliding-middle-out anim-area tt-holder fullwidth">
                                          <textarea class="editcomment-tt materialize-textarea">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                       </div>
                                       <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="clear"></div>
                           <div class="comment-reply-holder comment-addreply">
                              <div class="addnew-comment valign-wrapper  comment-reply">
                                 <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                                 <div class="desc-holder">
                                    <div class="sliding-middle-out anim-area tt-holder">
                                       <textarea class="materialize-textarea" placeholder="Write a reply..."></textarea>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="addnew-comment valign-wrapper ">
                        <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                        <div class="desc-holder">
                           <div class="sliding-middle-out anim-area tt-holder">
                              <textarea class="materialize-textarea" id="comment_txt_3" placeholder="Write a comment..."></textarea>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="col s12 m12">
         <!-- shared trip experience -->
         <div class="post-holder">
            <div class="post-topbar">
               <div class="post-userinfo">
                  <div class="img-holder">
                     <div class="profiletipholder">
                        <span class="profile-tooltip">
                        <img class="circle" src="images/demo-profile.jpg"/>
                        </span>
                        <span class="profiletooltip_content">
                           <div class="profile-tip" style="display:none;">
                              <div class="slidingpan-holder">
                                 <div class="profile-tip-avatar">
                                    <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                    <div class="sliding-pan location-span">
                                       <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                    </div>
                                 </div>
                                 <div class="profile-tip-name">
                                    <a href="javascript:void(0)">Adel Hasanat</a>
                                 </div>
                                 <div class="profile-tip-info">
                                    <div class="profiletip-icon">
                                       <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                    </div>
                                    <div class="profiletip-icon">
                                       <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                    </div>
                                    <div class="profiletip-icon">
                                       <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </span>
                     </div>
                  </div>
                  <div class="desc-holder">
                     <a href="javascript:void(0)">Nimish Parekh</a>
                     <div class="user-to">
                        <i class="mdi mdi-menu-right"></i>

                        <a href="javascript:void(0)">Adel Hasanat</a>
                     </div>
                     <div class="extra-info">
                        -- at <a href="javascript:void(0)">New York</a> with <a href="javascript:void(0)">Alap Shah</a> and <a href="javascript:void(0)">4 others</a>
                     </div>
                     <span class="timestamp">2 hrs<span class="glyphicon glyphicon-globe"></span></span>
                  </div>
               </div>			   
               <div class="settings-icon">
			   <div class="dropdown dropdown-custom dropdown-xxsmall">
				  <a class="dropdown-button more_btn" href="javascript:void(0);" data-activates="setting_btn2" style="">					
                  <i class="zmdi zmdi-more"></i>
				  </a>
				  <ul id="setting_btn2" class="dropdown-content custom_dropdown" >
					 <li>
                        <a href="javascript:void(0)">Hide post</a>
                     </li>
                     <li>
                        <a href="javascript:void(0)" class="savepost-link">Save post</a>
                     </li>
                     <li>
                        <a href="javascript:void(0)">Mute notification for this post</a>
                     </li>
                     <li>
                        <a href="javascript:void(0)">Mute connect post</a>
                     </li>
                     <li>
                        <a href="javascript:void(0)">Report post</a>
                     </li>
                     <li>
                        <a href="javascript:void(0)" class="composeeditpostAction">Edit post</a>
                     </li>
				  </ul>				 
				</div>
                  <!-- Dropdown Structure -->
               </div>
            </div>
            <div class="post-content">
               <div class="pdetail-holder">
                  <div class="post-details">
                     <div class="post-desc">
                        <p>The guy had an awesome trip experience. Checkout this!</p>
                     </div>
                  </div>
                  <div class="shared-box">
                     <div class="post-holder">
                        <div class="post-topbar">
                           <div class="post-userinfo">
                              <div class="img-holder">
                                 <div class="profiletipholder">
                                    <span class="profile-tooltip">
                                    <img class="circle" src="images/demo-profile.jpg"/>
                                    </span>
                                    <span class="profiletooltip_content">
                                       <div class="profile-tip" style="display:none;">
                                          <div class="slidingpan-holder">
                                             <div class="profile-tip-avatar">
                                                <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                                <div class="sliding-pan location-span">
                                                   <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                </div>
                                             </div>
                                             <div class="profile-tip-name">
                                                <a href="javascript:void(0)">Adel Hasanat</a>
                                             </div>
                                             <div class="profile-tip-info">
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                </div>
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                </div>
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </span>
                                 </div>
                              </div>
                              <div class="desc-holder">
                                 <span>By </span><a href="javascript:void(0)">Nimish Parekh</a> <span class="withtext">-- with <a href="javascript:void(0)">Alap Shah</a> and <a href="javascript:void(0)">4 others</a></span>
                                 <span class="timestamp">2 hrs<span class="glyphicon glyphicon-globe"></span></span>
                              </div>
                           </div>
                        </div>
                        <div class="post-content tripexperince-post">
                           <div class="post-details">
                              <div class="post-title">
                                 Visiting <?=$st_nm_S?>
                              </div>
                           </div>
                           <div class="trip-summery">
                              <div class="location-info">
                                 <h5><i class="zmdi zmdi-pin"></i> <?=$st_nm_S?></h5>
                                 <i class="mdi mdi-menu-right"></i>

                                 <a href="javascript:void(0)" onclick="openViewMap(this)">View on map</a>
                              </div>
                           </div>
                           <div class="map-holder">
                              <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3110.3465133386144!2d-9.167423685010494!3d38.77868997958898!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xd193295d5b45545%3A0x3f9e7b6a5f00e12c!2sPerta!5e0!3m2!1sen!2sin!4v1481089901870" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                           </div>
                           <div class="post-img-holder"> 
                              <div class="lgt-gallery lgt-gallery-photo post-img one-img dis-none">
                                 <a href="images/post-img.jpg" data-size="1600x1600" data-med="images/post-img.jpg" data-med-size="1024x1024" data-author="Folkert Gorter"  id="JADAAction">
                                 <img class="himg" src="images/post-img.jpg" alt="" />
                                 </a>
                              </div>
                              <a href="javascript:void(0)" class="pinlink"><i class="mdi mdi-nature"></i></a>
                           </div>
                           <div class="post-details">
                              <div class="post-desc">
                                 <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sed varius risus. Duis rhoncus eros et pellentesque imperdiet. Praesent pharetra rutrum eros. In nec nulla id enim faucibus pellentesque quis at dolor. Aenean a cursus quam, et fringilla tellus. Vivamus eu lorem est. Donec non urna sit amet arcu dictum dapibus.</p>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="clear"></div>
            <div class="post-data">
               <div class="post-actions">
                  <span class="likeholder">
                  <span class="like-tooltip">
                  <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                  <i class="zmdi zmdi-thumb-up"></i>
                  </a>
                  </span>
                  </span>                             
                  <a href="javascript:void(0)" class="sharepostmodalAction pa-share" title="Share"><i class="zmdi zmdi-mail-reply zmdi-hc-flip-horizontal"></i></a>
                  <span class="right">
                     <a href="javascript:void(0)" class="pa-comment" title="Comment" data-id="1"><i class="zmdi zmdi-comment"></i></a><span class="comment-lcount">4</span>
                  </span>
               </div>
               <div class="comments-section panel">
                  <div class="post-more">
                     <a href="javascript:void(0)" class="view-morec">View more comments <i class="zmdi zmdi-chevron-right"></i></a>
                     <span class="total-comments">3 of 7</span>
                  </div>
                  <div class="post-comments">
                     <div class="pcomments">
                        <div class="pcomment-earlier">
                           <div class="pcomment-holder">
                              <div class="pcomment main-comment">
                                 <div class="img-holder">
                                    <div class="profiletipholder">
                                       <span class="profile-tooltip">
                                       <img class="circle" src="images/demo-profile.jpg"/>
                                       </span>
                                       <span class="profiletooltip_content">
                                          <div class="profile-tip" style="display:none;">
                                             <div class="slidingpan-holder">
                                                <div class="profile-tip-avatar">
                                                   <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                                   <div class="sliding-pan location-span">
                                                      <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                   </div>
                                                </div>
                                                <div class="profile-tip-name">
                                                   <a href="javascript:void(0)">Adel Hasanat</a>
                                                </div>
                                                <div class="profile-tip-info">
                                                   <div class="profiletip-icon">
                                                      <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                   </div>
                                                   <div class="profiletip-icon">
                                                      <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                   </div>
                                                   <div class="profiletip-icon">
                                                      <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </span>
                                    </div>
                                 </div>
                                 <div class="desc-holder">
                                    <div class="normal-mode">
                                       <div class="desc">
                                          <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                          <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                       </div>
                                       <div class="comment-stuff">
                                          <div class="more-opt">
                                             <span class="likeholder">
                                             <span class="like-tooltip">
                                             <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                             <span class="icon">icon</span>
                                             </a>
                                             </span>
                                             </span>  
                                             <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><span>icon</span></a>
                                             <!-- Dropdown Trigger -->
                                             <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='drop_down8'>
                                             <i class="zmdi zmdi-more"></i>
                                             </a>
                                             <!-- Dropdown Structure -->
                                             <ul id='drop_down8' class='dropdown-content custom_dropdown'>
                                                <li>
                                                   <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                                </li>
                                                <li>
                                                   <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                                </li>
                                             </ul>
                                          </div>
                                          <div class="less-opt">
                                             <div class="timestamp">8h</div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="edit-mode">
                                       <div class="desc">
                                          <textarea class="editcomment-tt materialize-textarea" id="ec-12">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                          <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="clear"></div>
                              <div class="comment-reply-holder comment-addreply">
                                 <div class="addnew-comment valign-wrapper  comment-reply">
                                    <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                                    <div class="desc-holder">                          
                                       <textarea class="materialize-textarea">Write a reply...</textarea>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="pcomment-holder">
                              <div class="pcomment main-comment">
                                 <div class="img-holder">
                                    <div class="profiletipholder">
                                       <span class="profile-tooltip">
                                       <img class="circle" src="images/demo-profile.jpg"/>
                                       </span>
                                       <span class="profiletooltip_content">
                                          <div class="profile-tip" style="display:none;">
                                             <div class="slidingpan-holder">
                                                <div class="profile-tip-avatar">
                                                   <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                                   <div class="sliding-pan location-span">
                                                      <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                   </div>
                                                </div>
                                                <div class="profile-tip-name">
                                                   <a href="javascript:void(0)">Adel Hasanat</a>
                                                </div>
                                                <div class="profile-tip-info">
                                                   <div class="profiletip-icon">
                                                      <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                   </div>
                                                   <div class="profiletip-icon">
                                                      <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                   </div>
                                                   <div class="profiletip-icon">
                                                      <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </span>
                                    </div>
                                 </div>
                                 <div class="desc-holder">
                                    <div class="normal-mode">
                                       <div class="desc">
                                          <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                          <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                       </div>
                                       <div class="comment-stuff">
                                          <div class="more-opt">
                                             <span class="likeholder">
                                             <span class="like-tooltip">
                                             <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                             <span class="icon">icon</span>
                                             </a>
                                             </span>
                                             </span>  
                                             <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><span>icon</span></a>
                                             <!-- Dropdown Trigger -->
                                             <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='drop_down9'>
                                             <i class="zmdi zmdi-more"></i>
                                             </a>
                                             <!-- Dropdown Structure -->
                                             <ul id='drop_down9' class='dropdown-content custom_dropdown'>
                                                <li>
                                                   <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                                </li>
                                                <li>
                                                   <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                                </li>
                                             </ul>
                                          </div>
                                          <div class="less-opt">
                                             <div class="timestamp">8h</div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="edit-mode">
                                       <div class="desc">
                                          <textarea class="editcomment-tt materialize-textarea" id="ec-13">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                          <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="clear"></div>
                              <div class="comment-reply-holder comment-addreply">
                                 <div class="addnew-comment valign-wrapper  comment-reply">
                                    <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                                    <div class="desc-holder">                          
                                       <textarea class="materialize-textarea">Write a reply...</textarea>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="pcomment-holder has-comments">
                           <div class="pcomment main-comment">
                              <div class="img-holder">
                                 <div class="profiletipholder">
                                    <span class="profile-tooltip">
                                    <img class="circle" src="images/demo-profile.jpg"/>
                                    </span>
                                    <span class="profiletooltip_content">
                                       <div class="profile-tip" style="display:none;">
                                          <div class="slidingpan-holder">
                                             <div class="profile-tip-avatar">
                                                <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                                <div class="sliding-pan location-span">
                                                   <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                </div>
                                             </div>
                                             <div class="profile-tip-name">
                                                <a href="javascript:void(0)">Adel Hasanat</a>
                                             </div>
                                             <div class="profile-tip-info">
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                </div>
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                </div>
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </span>
                                 </div>
                              </div>
                              <div class="desc-holder">
                                 <div class="normal-mode">
                                    <div class="desc">
                                       <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                       <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                    </div>
                                    <div class="comment-stuff">
                                       <div class="more-opt">
                                          <span class="likeholder">
                                          <span class="like-tooltip">
                                          <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                          <span class="icon">icon</span>
                                          </a>
                                          </span>
                                          </span>  
                                          <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><span>icon</span></a>
                                          <!-- Dropdown Trigger -->
                                          <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='drop_down10'>
                                          <i class="zmdi zmdi-more"></i>
                                          </a>
                                          <!-- Dropdown Structure -->
                                          <ul id='drop_down10' class='dropdown-content custom_dropdown'>
                                             <li>
                                                <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                             </li>
                                             <li>
                                                <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                             </li>
                                          </ul>
                                       </div>
                                       <div class="less-opt">
                                          <div class="timestamp">8h</div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="edit-mode">
                                    <div class="desc">
                                       <textarea class="editcomment-tt materialize-textarea" id="ec-14">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                       <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="clear"></div>
                           <div class="comment-reply-holder">
                              <div class="pcomment comment-reply">
                                 <div class="img-holder">
                                    <div class="profiletipholder">
                                       <span class="profile-tooltip">
                                       <img class="circle" src="images/demo-profile.jpg"/>
                                       </span>
                                       <span class="profiletooltip_content">
                                          <div class="profile-tip" style="display:none;">
                                             <div class="slidingpan-holder">
                                                <div class="profile-tip-avatar">
                                                   <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                                   <div class="sliding-pan location-span">
                                                      <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                   </div>
                                                </div>
                                                <div class="profile-tip-name">
                                                   <a href="javascript:void(0)">Adel Hasanat</a>
                                                </div>
                                                <div class="profile-tip-info">
                                                   <div class="profiletip-icon">
                                                      <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                   </div>
                                                   <div class="profiletip-icon">
                                                      <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                   </div>
                                                   <div class="profiletip-icon">
                                                      <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </span>
                                    </div>
                                 </div>
                                 <div class="desc-holder">
                                    <div class="normal-mode">
                                       <div class="desc">
                                          <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                          <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                       </div>
                                       <div class="comment-stuff">
                                          <div class="more-opt">
                                             <span class="likeholder">
                                             <span class="like-tooltip">
                                             <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                             <span class="icon">icon</span>
                                             </a>
                                             </span>
                                             </span>  
                                             <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><span>icon</span></a>
                                             <!-- Dropdown Trigger -->
                                             <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='drop_down11'>
                                             <i class="zmdi zmdi-more"></i>
                                             </a>
                                             <!-- Dropdown Structure -->
                                             <ul id='drop_down11' class='dropdown-content custom_dropdown'>
                                                <li>
                                                   <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                                </li>
                                                <li>
                                                   <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                                </li>
                                             </ul>
                                          </div>
                                          <div class="less-opt">
                                             <div class="timestamp">8h</div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="edit-mode">
                                       <div class="desc">
                                          <textarea class="editcomment-tt materialize-textarea" id="ec-15">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                          <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="pcomment comment-reply">
                                 <div class="img-holder">
                                    <div class="profiletipholder">
                                       <span class="profile-tooltip">
                                       <img class="circle" src="images/demo-profile.jpg"/>
                                       </span>
                                       <span class="profiletooltip_content">
                                          <div class="profile-tip" style="display:none;">
                                             <div class="slidingpan-holder">
                                                <div class="profile-tip-avatar">
                                                   <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                                   <div class="sliding-pan location-span">
                                                      <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                   </div>
                                                </div>
                                                <div class="profile-tip-name">
                                                   <a href="javascript:void(0)">Adel Hasanat</a>
                                                </div>
                                                <div class="profile-tip-info">
                                                   <div class="profiletip-icon">
                                                      <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                   </div>
                                                   <div class="profiletip-icon">
                                                      <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                   </div>
                                                   <div class="profiletip-icon">
                                                      <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </span>
                                    </div>
                                 </div>
                                 <div class="desc-holder">
                                    <div class="normal-mode">
                                       <div class="desc">
                                          <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                          <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit...</p>
                                       </div>
                                       <div class="comment-stuff">
                                          <div class="more-opt">
                                             <span class="likeholder">
                                             <span class="like-tooltip">
                                             <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                             <span class="icon">icon</span>
                                             </a>
                                             </span>
                                             </span>  
                                             <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><span>icon</span></a>
                                             <!-- Dropdown Trigger -->
                                             <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='drop_down12'>
                                             <i class="zmdi zmdi-more"></i>
                                             </a>
                                             <!-- Dropdown Structure -->
                                             <ul id='drop_down12' class='dropdown-content custom_dropdown'>
                                                <li>
                                                   <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                                </li>
                                                <li>
                                                   <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                                </li>
                                             </ul>
                                          </div>
                                          <div class="less-opt">
                                             <div class="timestamp">8h</div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="edit-mode">
                                       <div class="desc">
                                          <textarea class="editcomment-tt materialize-textarea" id="ec-16">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                          <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="comment-reply-holder comment-addreply">
                              <div class="addnew-comment valign-wrapper  comment-reply">
                                 <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                                 <div class="desc-holder">                          
                                    <textarea class="materialize-textarea">Write a reply...</textarea>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="pcomment-holder">
                           <div class="pcomment main-comment">
                              <div class="img-holder">
                                 <div class="profiletipholder">
                                    <span class="profile-tooltip">
                                    <img class="circle" src="images/demo-profile.jpg"/>
                                    </span>
                                    <span class="profiletooltip_content">
                                       <div class="profile-tip" style="display:none;">
                                          <div class="slidingpan-holder">
                                             <div class="profile-tip-avatar">
                                                <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                                <div class="sliding-pan location-span">
                                                   <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                </div>
                                             </div>
                                             <div class="profile-tip-name">
                                                <a href="javascript:void(0)">Adel Hasanat</a>
                                             </div>
                                             <div class="profile-tip-info">
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                </div>
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                </div>
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </span>
                                 </div>
                              </div>
                              <div class="desc-holder">
                                 <div class="normal-mode">
                                    <div class="desc">
                                       <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>                                           
                                       <p class="shorten">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.<a a="" href="javascript:void(0)" class="overlay" onclick="explandReadMore(this)" style="position: absolute;top: 0;bottom: -6px;left: 0;right: 0;"><span class="readlink">Read More</span></a></p>
                                    </div>
                                    <div class="comment-stuff">
                                       <div class="more-opt">
                                          <span class="likeholder">
                                          <span class="like-tooltip">
                                          <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                          <span class="icon">icon</span>
                                          </a>
                                          </span>
                                          </span>  
                                          <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><span>icon</span></a>
                                          <!-- Dropdown Trigger -->
                                          <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='drop_down13'>
                                          <i class="zmdi zmdi-more"></i>
                                          </a>
                                          <!-- Dropdown Structure -->
                                          <ul id='drop_down13' class='dropdown-content custom_dropdown'>
                                             <li>
                                                <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                             </li>
                                             <li>
                                                <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                             </li>
                                          </ul>
                                       </div>
                                       <div class="less-opt">
                                          <div class="timestamp">8h</div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="edit-mode">
                                    <div class="desc">
                                       <textarea class="editcomment-tt materialize-textarea" id="ec-17">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                       <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="clear"></div>
                           <div class="comment-reply-holder comment-addreply">
                              <div class="addnew-comment valign-wrapper  comment-reply">
                                 <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                                 <div class="desc-holder">                          
                                    <textarea class="materialize-textarea">Write a reply...</textarea>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="addnew-comment valign-wrapper ">
                        <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                        <div class="desc-holder">                          
                           <textarea class="materialize-textarea" id="comment_txt_3">Write a reply...</textarea>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <!-- end shared trip experience -->
      </div>
      <div class="col s12 m12">
         <!-- brand awareness -->
         <div class="post-holder travad-box brand-travad">
            <div class="post-topbar">
               <div class="post-userinfo">
                  Best coffee in the world!
               </div>
               <div class="settings-icon">
			   <div class="dropdown dropdown-custom dropdown-xxsmall">
				  <a class="dropdown-button more_btn" href="javascript:void(0);" data-activates="setting_btn3" style="">					
                  <i class="zmdi zmdi-more"></i>
				  </a>
				  <ul id="setting_btn3" class="dropdown-content custom_dropdown" > <li>
                        <a href="javascript:void(0)">Hide ad</a>
                     </li>
                     <li>
                        <a href="javascript:void(0)">Save ad</a>
                     </li>
                     <li>
                        <a href="javascript:void(0)">Mute this seller ads</a>
                     </li>
                     <li>
                        <a href="javascript:void(0)">Report ad</a>
                     </li>
				  </ul>				 
				</div>
                  <!-- Dropdown Structure -->
               </div>               
            </div>
            <div class="post-content">
               <div class="pdetail-holder">
                  <div class="shared-box shared-category">
                     <div class="post-holder">
                        <div class="post-content">
                           <div class="post-img-holder">
                              <div class="post-img one-img">
                                 <div class="pimg-holder"><img src="images/brand-p.jpg"/></div>
                              </div>
                           </div>
                           <div class="share-summery">
                              <div class="travad-subtitle">We just get new starbucks coffee that is double in caffine that everybody is calling it a boost!</div>
                              <a href="javascript:void(0)" target="_blank" class="btn btn-primary btn-sm adbtn waves-effect waves-light">Explore</a>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="clear"></div>
            <div class="post-data">
               <div class="post-actions">
                  <span class="likeholder">
                  <span class="like-tooltip">
                  <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                  <i class="zmdi zmdi-thumb-up"></i>
                  </a>
                  </span>
                  </span>                    
                  <a href="javascript:void(0)" class="sharepostmodalAction pa-share" title="Share"><i class="zmdi zmdi-mail-reply zmdi-hc-flip-horizontal"></i></a>
                  <span class="right">
                     <a href="javascript:void(0)" class="pa-comment" title="Comment" data-id="1"><i class="zmdi zmdi-comment"></i></a><span class="comment-lcount">4</span>
                  </span>
               </div>
               <div class="comments-section panel">
                  <div class="post-more">
                     <a href="javascript:void(0)" class="view-morec">View more comments <i class="zmdi zmdi-chevron-right"></i></a>
                     <span class="total-comments">3 of 7</span>
                  </div>
                  <div class="post-comments">
                     <div class="pcomments">
                        <div class="pcomment-earlier">
                           <div class="pcomment-holder">
                              <div class="pcomment main-comment">
                                 <div class="img-holder">
                                    <div class="profiletipholder">
                                       <span class="profile-tooltip">
                                       <img class="circle" src="images/demo-profile.jpg"/>
                                       </span>
                                       <span class="profiletooltip_content">
                                          <div class="profile-tip" style="display:none;">
                                             <div class="slidingpan-holder">
                                                <div class="profile-tip-avatar">
                                                   <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                                   <div class="sliding-pan location-span">
                                                      <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                   </div>
                                                </div>
                                                <div class="profile-tip-name">
                                                   <a href="javascript:void(0)">Adel Hasanat</a>
                                                </div>
                                                <div class="profile-tip-info">
                                                   <div class="profiletip-icon">
                                                      <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                   </div>
                                                   <div class="profiletip-icon">
                                                      <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                   </div>
                                                   <div class="profiletip-icon">
                                                      <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </span>
                                    </div>
                                 </div>
                                 <div class="desc-holder">
                                    <div class="normal-mode">
                                       <div class="desc">
                                          <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                          <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                       </div>
                                       <div class="comment-stuff">
                                          <div class="more-opt">
                                             <span class="likeholder">
                                             <span class="like-tooltip">
                                             <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                             <span class="icon">icon</span>
                                             </a>
                                             </span>
                                             </span>  
                                             <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><span>icon</span></a>
                                             <!-- Dropdown Trigger -->
                                             <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='drop_down14'>
                                             <i class="zmdi zmdi-more"></i>
                                             </a>
                                             <!-- Dropdown Structure -->
                                             <ul id='drop_down14' class='dropdown-content custom_dropdown'>
                                                <li>
                                                   <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                                </li>
                                                <li>
                                                   <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                                </li>
                                             </ul>
                                          </div>
                                          <div class="less-opt">
                                             <div class="timestamp">8h</div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="edit-mode">
                                       <div class="desc">
                                          <textarea class="editcomment-tt materialize-textarea" id="ec-12">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                          <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="clear"></div>
                              <div class="comment-reply-holder comment-addreply">
                                 <div class="addnew-comment valign-wrapper  comment-reply">
                                    <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                                    <div class="desc-holder">                          
                                       <textarea class="materialize-textarea">Write a reply...</textarea>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="pcomment-holder">
                              <div class="pcomment main-comment">
                                 <div class="img-holder">
                                    <div class="profiletipholder">
                                       <span class="profile-tooltip">
                                       <img class="circle" src="images/demo-profile.jpg"/>
                                       </span>
                                       <span class="profiletooltip_content">
                                          <div class="profile-tip" style="display:none;">
                                             <div class="slidingpan-holder">
                                                <div class="profile-tip-avatar">
                                                   <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                                   <div class="sliding-pan location-span">
                                                      <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                   </div>
                                                </div>
                                                <div class="profile-tip-name">
                                                   <a href="javascript:void(0)">Adel Hasanat</a>
                                                </div>
                                                <div class="profile-tip-info">
                                                   <div class="profiletip-icon">
                                                      <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                   </div>
                                                   <div class="profiletip-icon">
                                                      <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                   </div>
                                                   <div class="profiletip-icon">
                                                      <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </span>
                                    </div>
                                 </div>
                                 <div class="desc-holder">
                                    <div class="normal-mode">
                                       <div class="desc">
                                          <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                          <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                       </div>
                                       <div class="comment-stuff">
                                          <div class="more-opt">
                                             <span class="likeholder">
                                             <span class="like-tooltip">
                                             <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                             <span class="icon">icon</span>
                                             </a>
                                             </span>
                                             </span>  
                                             <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><span>icon</span></a>
                                             <!-- Dropdown Trigger -->
                                             <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='drop_down15'>
                                             <i class="zmdi zmdi-more"></i>
                                             </a>
                                             <!-- Dropdown Structure -->
                                             <ul id='drop_down15' class='dropdown-content custom_dropdown'>
                                                <li>
                                                   <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                                </li>
                                                <li>
                                                   <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                                </li>
                                             </ul>
                                          </div>
                                          <div class="less-opt">
                                             <div class="timestamp">8h</div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="edit-mode">
                                       <div class="desc">
                                          <textarea class="editcomment-tt materialize-textarea" id="ec-13">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                          <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="clear"></div>
                              <div class="comment-reply-holder comment-addreply">
                                 <div class="addnew-comment valign-wrapper  comment-reply">
                                    <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                                    <div class="desc-holder">                          
                                       <textarea class="materialize-textarea">Write a reply...</textarea>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="pcomment-holder has-comments">
                              <div class="pcomment main-comment">
                                 <div class="img-holder">
                                    <div class="profiletipholder">
                                       <span class="profile-tooltip">
                                       <img class="circle" src="images/demo-profile.jpg"/>
                                       </span>
                                       <span class="profiletooltip_content">
                                          <div class="profile-tip" style="display:none;">
                                             <div class="slidingpan-holder">
                                                <div class="profile-tip-avatar">
                                                   <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                                   <div class="sliding-pan location-span">
                                                      <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                   </div>
                                                </div>
                                                <div class="profile-tip-name">
                                                   <a href="javascript:void(0)">Adel Hasanat</a>
                                                </div>
                                                <div class="profile-tip-info">
                                                   <div class="profiletip-icon">
                                                      <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                   </div>
                                                   <div class="profiletip-icon">
                                                      <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                   </div>
                                                   <div class="profiletip-icon">
                                                      <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </span>
                                    </div>
                                 </div>
                                 <div class="desc-holder">
                                    <div class="normal-mode">
                                       <div class="desc">
                                          <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                          <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                       </div>
                                       <div class="comment-stuff">
                                          <div class="more-opt">
                                             <span class="likeholder">
                                             <span class="like-tooltip">
                                             <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                             <span class="icon">icon</span>
                                             </a>
                                             </span>
                                             </span>  
                                             <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><span>icon</span></a>
                                             <!-- Dropdown Trigger -->
                                             <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='drop_down16'>
                                             <i class="zmdi zmdi-more"></i>
                                             </a>
                                             <!-- Dropdown Structure -->
                                             <ul id='drop_down16' class='dropdown-content custom_dropdown'>
                                                <li>
                                                   <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                                </li>
                                                <li>
                                                   <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                                </li>
                                             </ul>
                                          </div>
                                          <div class="less-opt">
                                             <div class="timestamp">8h</div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="edit-mode">
                                       <div class="desc">
                                          <textarea class="editcomment-tt materialize-textarea" id="ec-14">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                          <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="clear"></div>
                              <div class="comment-reply-holder">
                                 <div class="pcomment comment-reply">
                                    <div class="img-holder">
                                       <div class="profiletipholder">
                                          <span class="profile-tooltip">
                                          <img class="circle" src="images/demo-profile.jpg"/>
                                          </span>
                                          <span class="profiletooltip_content">
                                             <div class="profile-tip" style="display:none;">
                                                <div class="slidingpan-holder">
                                                   <div class="profile-tip-avatar">
                                                      <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                                      <div class="sliding-pan location-span">
                                                         <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                      </div>
                                                   </div>
                                                   <div class="profile-tip-name">
                                                      <a href="javascript:void(0)">Adel Hasanat</a>
                                                   </div>
                                                   <div class="profile-tip-info">
                                                      <div class="profiletip-icon">
                                                         <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                      </div>
                                                      <div class="profiletip-icon">
                                                         <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                      </div>
                                                      <div class="profiletip-icon">
                                                         <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </span>
                                       </div>
                                    </div>
                                    <div class="desc-holder">
                                       <div class="normal-mode">
                                          <div class="desc">
                                             <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                             <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                          </div>
                                          <div class="comment-stuff">
                                             <div class="more-opt">
                                                <span class="likeholder">
                                                <span class="like-tooltip">
                                                <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                                <span class="icon">icon</span>
                                                </a>
                                                </span>
                                                </span>  
                                                <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><span>icon</span></a>
                                                <!-- Dropdown Trigger -->
                                                <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='drop_down17'>
                                                <i class="zmdi zmdi-more"></i>
                                                </a>
                                                <!-- Dropdown Structure -->
                                                <ul id='drop_down17' class='dropdown-content custom_dropdown'>
                                                   <li>
                                                      <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                                   </li>
                                                   <li>
                                                      <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                                   </li>
                                                </ul>
                                             </div>
                                             <div class="less-opt">
                                                <div class="timestamp">8h</div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="edit-mode">
                                          <div class="desc">
                                             <textarea class="editcomment-tt materialize-textarea" id="ec-15">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                             <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="pcomment comment-reply">
                                    <div class="img-holder">
                                       <div class="profiletipholder">
                                          <span class="profile-tooltip">
                                          <img class="circle" src="images/demo-profile.jpg"/>
                                          </span>
                                          <span class="profiletooltip_content">
                                             <div class="profile-tip" style="display:none;">
                                                <div class="slidingpan-holder">
                                                   <div class="profile-tip-avatar">
                                                      <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                                      <div class="sliding-pan location-span">
                                                         <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                      </div>
                                                   </div>
                                                   <div class="profile-tip-name">
                                                      <a href="javascript:void(0)">Adel Hasanat</a>
                                                   </div>
                                                   <div class="profile-tip-info">
                                                      <div class="profiletip-icon">
                                                         <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                      </div>
                                                      <div class="profiletip-icon">
                                                         <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                      </div>
                                                      <div class="profiletip-icon">
                                                         <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </span>
                                       </div>
                                    </div>
                                    <div class="desc-holder">
                                       <div class="normal-mode">
                                          <div class="desc">
                                             <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                             <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit...</p>
                                          </div>
                                          <div class="comment-stuff">
                                             <div class="more-opt">
                                                <span class="likeholder">
                                                <span class="like-tooltip">
                                                <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                                <span class="icon">icon</span>
                                                </a>
                                                </span>
                                                </span>  
                                                <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><span>icon</span></a>
                                                <!-- Dropdown Trigger -->
                                                <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='drop_down18'>
                                                <i class="zmdi zmdi-more"></i>
                                                </a>
                                                <!-- Dropdown Structure -->
                                                <ul id='drop_down18' class='dropdown-content custom_dropdown'>
                                                   <li>
                                                      <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                                   </li>
                                                   <li>
                                                      <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                                   </li>
                                                </ul>
                                             </div>
                                             <div class="less-opt">
                                                <div class="timestamp">8h</div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="edit-mode">
                                          <div class="desc">
                                             <textarea class="editcomment-tt materialize-textarea" id="ec-16">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                             <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="comment-reply-holder comment-addreply">
                                 <div class="addnew-comment valign-wrapper  comment-reply">
                                    <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                                    <div class="desc-holder">                          
                                       <textarea class="materialize-textarea">Write a reply...</textarea>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="pcomment-holder">
                              <div class="pcomment main-comment">
                                 <div class="img-holder">
                                    <div class="profiletipholder">
                                       <span class="profile-tooltip">
                                       <img class="circle" src="images/demo-profile.jpg"/>
                                       </span>
                                       <span class="profiletooltip_content slidingpan-holder">
                                          <div class="profile-tip" style="display:none;">
                                             <div class="profile-tip-avatar">
                                                <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                                <div class="sliding-pan location-span">
                                                   <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                </div>
                                             </div>
                                             <div class="profile-tip-name">
                                                <a href="javascript:void(0)">Adel Hasanat</a>
                                             </div>
                                             <div class="profile-tip-info">
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                </div>
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                </div>
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                                </div>
                                             </div>
                                          </div>
                                       </span>
                                    </div>
                                 </div>
                                 <div class="desc-holder">
                                    <div class="normal-mode">
                                       <div class="desc">
                                          <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                          <p class="shorten">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.<a a="" href="javascript:void(0)" class="overlay" onclick="explandReadMore(this)" style="position: absolute;top: 0;bottom: -6px;left: 0;right: 0;"><span class="readlink">Read More</span></a></p>
                                       </div>
                                       <div class="comment-stuff">
                                          <div class="more-opt">
                                             <span class="likeholder">
                                             <span class="like-tooltip">
                                             <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                             <span class="icon">icon</span>
                                             </a>
                                             </span>
                                             </span>  
                                             <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><span>icon</span></a>
                                             <!-- Dropdown Trigger -->
                                             <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='drop_down19'>
                                             <i class="zmdi zmdi-more"></i>
                                             </a>
                                             <!-- Dropdown Structure -->
                                             <ul id='drop_down19' class='dropdown-content custom_dropdown'>
                                                <li>
                                                   <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                                </li>
                                                <li>
                                                   <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                                </li>
                                             </ul>
                                          </div>
                                          <div class="less-opt">
                                             <div class="timestamp">8h</div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="edit-mode">
                                       <div class="desc">
                                          <textarea class="editcomment-tt materialize-textarea" id="ec-17">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                          <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="clear"></div>
                              <div class="comment-reply-holder comment-addreply">
                                 <div class="addnew-comment valign-wrapper  comment-reply">
                                    <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                                    <div class="desc-holder">                          
                                       <textarea class="materialize-textarea">Write a reply...</textarea>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="addnew-comment valign-wrapper ">
                        <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                        <div class="desc-holder">                          
                           <textarea class="materialize-textarea" id="comment_txt_3">Write a reply...</textarea>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <!-- end brand awareness -->
      </div>
      <div class="col s12 m12">
         <!-- advertise action -->
         <div class="post-holder travad-box action-travad">
            <div class="post-topbar">
               <div class="post-userinfo">
                  <div class="img-holder">
                     <div class="profiletipholder">
                        <span class="profile-tooltip">
                           <div class="travad-maintitle"><span><i class=”mdi mdi-account-group”></i></i></span></div>
                        </span>
                        <span class="profiletooltip_content slidingpan-holder">
                           <div class="profile-tip" style="display:none;">
                              <div class="profile-tip-avatar">
                                 <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                 <div class="sliding-pan location-span">
                                    <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                 </div>
                              </div>
                              <div class="profile-tip-name">
                                 <a href="javascript:void(0)">Adel Hasanat</a>
                              </div>
                              <div class="profile-tip-info">
                                 <div class="profiletip-icon">
                                    <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                 </div>
                                 <div class="profiletip-icon">
                                    <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                 </div>
                                 <div class="profiletip-icon">
                                    <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                 </div>
                              </div>
                           </div>
                        </span>
                     </div>
                  </div>
                  <div class="desc-holder">
                     <div class="travad-maintitle">Heal Well</div>
                     <span class="timestamp">Sponsored Ad</span>
                  </div>
               </div>
               <div class="settings-icon">
			   <div class="dropdown dropdown-custom dropdown-xxsmall">
				  <a class="dropdown-button more_btn" href="javascript:void(0);" data-activates="setting_btn4" style="">					
                  <i class="zmdi zmdi-more"></i>
				  </a>
				  <ul id="setting_btn4" class="dropdown-content custom_dropdown" > <li>
                        <a href="javascript:void(0)">Hide ad</a>
                     </li>
                     <li>
                        <a href="javascript:void(0)">Save ad</a>
                     </li>
                     <li>
                        <a href="javascript:void(0)">Mute this seller ads</a>
                     </li>
                     <li>
                        <a href="javascript:void(0)">Report ad</a>
                     </li>
				  </ul>				 
				</div>
                  <!-- Dropdown Structure -->
               </div>
            </div>
            <div class="post-content">
               <div class="pdetail-holder">
                  <div class="shared-box shared-category">
                     <div class="post-holder">
                        <div class="post-content">
                           <div class="post-desc">
                              <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                           </div>
                           <div class="post-img-holder">
                              <div class="post-img one-img">
                                 <div class="pimg-holder"><img src="images/groupad-actionvideo.jpg"/></div>
                              </div>
                           </div>
                           <div class="share-summery">
                              <div class="travad-title">Medical Research Methodolgy</div>
                              <div class="travad-subtitle">Checkout the new video on our website exploring the latest techniques of medicine research</div>
                              <a href="javascript:void(0)" target="_blank" class="btn btn-primary btn-sm adbtn waves-effect waves-light">Learn More</a>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="clear"></div>
            <div class="post-data">
               <div class="post-actions">
                  <span class="likeholder">
                  <span class="like-tooltip">
                  <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                  <i class="zmdi zmdi-thumb-up"></i>
                  </a>
                  </span>
                  </span>
                  <a href="javascript:void(0)" class="sharepostmodalAction pa-share" title="Share"><i class="zmdi zmdi-mail-reply zmdi-hc-flip-horizontal"></i></a>
                  <span class="right">
                     <a href="javascript:void(0)" class="pa-comment" title="Comment" data-id="1"><i class="zmdi zmdi-comment"></i></a><span class="comment-lcount">4</span>
                  </span>
               </div>
               <div class="comments-section panel">
                  <div class="post-more">
                     <a href="javascript:void(0)" class="view-morec">View more comments <i class="zmdi zmdi-chevron-right"></i></a>
                     <span class="total-comments">3 of 7</span>
                  </div>
                  <div class="post-comments">
                     <div class="pcomments">
                        <div class="pcomment-earlier">
                           <div class="pcomment-holder">
                              <div class="pcomment main-comment">
                                 <div class="img-holder">
                                    <div class="profiletipholder">
                                       <span class="profile-tooltip">
                                       <img class="circle" src="images/demo-profile.jpg"/>
                                       </span>
                                       <span class="profiletooltip_content slidingpan-holder">
                                          <div class="profile-tip" style="display:none;">
                                             <div class="profile-tip-avatar">
                                                <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                                <div class="sliding-pan location-span">
                                                   <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                </div>
                                             </div>
                                             <div class="profile-tip-name">
                                                <a href="javascript:void(0)">Adel Hasanat</a>
                                             </div>
                                             <div class="profile-tip-info">
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                </div>
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                </div>
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                                </div>
                                             </div>
                                          </div>
                                       </span>
                                    </div>
                                 </div>
                                 <div class="desc-holder">
                                    <div class="normal-mode">
                                       <div class="desc">
                                          <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                          <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                       </div>
                                       <div class="comment-stuff">
                                          <div class="more-opt">
                                             <span class="likeholder">
                                             <span class="like-tooltip">
                                             <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                             <span class="icon">icon</span>
                                             </a>
                                             </span>
                                             </span>  
                                             <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><span>icon</span></a>
                                             <!-- Dropdown Trigger -->
                                             <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='drop_down20'>
                                             <i class="zmdi zmdi-more"></i>
                                             </a>
                                             <!-- Dropdown Structure -->
                                             <ul id='drop_down20' class='dropdown-content custom_dropdown'>
                                                <li>
                                                   <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                                </li>
                                                <li>
                                                   <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                                </li>
                                             </ul>
                                          </div>
                                          <div class="less-opt">
                                             <div class="timestamp">8h</div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="edit-mode">
                                       <div class="desc">
                                          <textarea class="editcomment-tt materialize-textarea" id="ec-12">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                          <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="clear"></div>
                              <div class="comment-reply-holder comment-addreply">
                                 <div class="addnew-comment valign-wrapper  comment-reply">
                                    <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                                    <div class="desc-holder">                          
                                       <textarea class="materialize-textarea">Write a reply...</textarea>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="pcomment-holder">
                              <div class="pcomment main-comment">
                                 <div class="img-holder">
                                    <div class="profiletipholder">
                                       <span class="profile-tooltip">
                                       <img class="circle" src="images/demo-profile.jpg"/>
                                       </span>
                                       <span class="profiletooltip_content slidingpan-holder">
                                          <div class="profile-tip" style="display:none;">
                                             <div class="profile-tip-avatar">
                                                <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                                <div class="sliding-pan location-span">
                                                   <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                </div>
                                             </div>
                                             <div class="profile-tip-name">
                                                <a href="javascript:void(0)">Adel Hasanat</a>
                                             </div>
                                             <div class="profile-tip-info">
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                </div>
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                </div>
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                                </div>
                                             </div>
                                          </div>
                                       </span>
                                    </div>
                                 </div>
                                 <div class="desc-holder">
                                    <div class="normal-mode">
                                       <div class="desc">
                                          <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                          <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                       </div>
                                       <div class="comment-stuff">
                                          <div class="more-opt">
                                             <span class="likeholder">
                                             <span class="like-tooltip">
                                             <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                             <span class="icon">icon</span>
                                             </a>
                                             </span>
                                             </span>  
                                             <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><span>icon</span></a>
                                             <!-- Dropdown Trigger -->
                                             <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='drop_down21'>
                                             <i class="zmdi zmdi-more"></i>
                                             </a>
                                             <!-- Dropdown Structure -->
                                             <ul id='drop_down21' class='dropdown-content custom_dropdown'>
                                                <li>
                                                   <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                                </li>
                                                <li>
                                                   <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                                </li>
                                             </ul>
                                          </div>
                                          <div class="less-opt">
                                             <div class="timestamp">8h</div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="edit-mode">
                                       <div class="desc">
                                          <textarea class="editcomment-tt materialize-textarea" id="ec-13">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                          <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="clear"></div>
                              <div class="comment-reply-holder comment-addreply">
                                 <div class="addnew-comment valign-wrapper  comment-reply">
                                    <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                                    <div class="desc-holder">                          
                                       <textarea class="materialize-textarea">Write a reply...</textarea>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="pcomment-holder has-comments">
                              <div class="pcomment main-comment">
                                 <div class="img-holder">
                                    <div class="profiletipholder">
                                       <span class="profile-tooltip">
                                       <img class="circle" src="images/demo-profile.jpg"/>
                                       </span>
                                       <span class="profiletooltip_content slidingpan-holder">
                                          <div class="profile-tip" style="display:none;">
                                             <div class="profile-tip-avatar">
                                                <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                                <div class="sliding-pan location-span">
                                                   <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                </div>
                                             </div>
                                             <div class="profile-tip-name">
                                                <a href="javascript:void(0)">Adel Hasanat</a>
                                             </div>
                                             <div class="profile-tip-info">
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                </div>
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                </div>
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                                </div>
                                             </div>
                                          </div>
                                       </span>
                                    </div>
                                 </div>
                                 <div class="desc-holder">
                                    <div class="normal-mode">
                                       <div class="desc">
                                          <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                          <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                       </div>
                                       <div class="comment-stuff">
                                          <div class="more-opt">
                                             <span class="likeholder">
                                             <span class="like-tooltip">
                                             <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                             <span class="icon">icon</span>
                                             </a>
                                             </span>
                                             </span>  
                                             <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><span>icon</span></a>
                                             <!-- Dropdown Trigger -->
                                             <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='drop_down22'>
                                             <i class="zmdi zmdi-more"></i>
                                             </a>
                                             <!-- Dropdown Structure -->
                                             <ul id='drop_down22' class='dropdown-content custom_dropdown'>
                                                <li>
                                                   <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                                </li>
                                                <li>
                                                   <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                                </li>
                                             </ul>
                                          </div>
                                          <div class="less-opt">
                                             <div class="timestamp">8h</div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="edit-mode">
                                       <div class="desc">
                                          <textarea class="editcomment-tt materialize-textarea" id="ec-14">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                          <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="clear"></div>
                              <div class="comment-reply-holder">
                                 <div class="pcomment comment-reply">
                                    <div class="img-holder">
                                       <div class="profiletipholder">
                                          <span class="profile-tooltip">
                                          <img class="circle" src="images/demo-profile.jpg"/>
                                          </span>
                                          <span class="profiletooltip_content">
                                             <div class="profile-tip" style="display:none;">
                                                <div class="slidingpan-holder">
                                                   <div class="profile-tip-avatar">
                                                      <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                                      <div class="sliding-pan location-span">
                                                         <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                      </div>
                                                   </div>
                                                   <div class="profile-tip-name">
                                                      <a href="javascript:void(0)">Adel Hasanat</a>
                                                   </div>
                                                   <div class="profile-tip-info">
                                                      <div class="profiletip-icon">
                                                         <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                      </div>
                                                      <div class="profiletip-icon">
                                                         <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                      </div>
                                                      <div class="profiletip-icon">
                                                         <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </span>
                                       </div>
                                    </div>
                                    <div class="desc-holder">
                                       <div class="normal-mode">
                                          <div class="desc">
                                             <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                             <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                          </div>
                                          <div class="comment-stuff">
                                             <div class="more-opt">
                                                <span class="likeholder">
                                                <span class="like-tooltip">
                                                <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                                <span class="icon">icon</span>
                                                </a>
                                                </span>
                                                </span>  
                                                <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><span>icon</span></a>
                                                <!-- Dropdown Trigger -->
                                                <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='drop_down23'>
                                                <i class="zmdi zmdi-more"></i>
                                                </a>
                                                <!-- Dropdown Structure -->
                                                <ul id='drop_down23' class='dropdown-content custom_dropdown'>
                                                   <li>
                                                      <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                                   </li>
                                                   <li>
                                                      <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                                   </li>
                                                </ul>
                                             </div>
                                             <div class="less-opt">
                                                <div class="timestamp">8h</div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="edit-mode">
                                          <div class="desc">
                                             <textarea class="editcomment-tt materialize-textarea" id="ec-15">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                             <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="pcomment comment-reply">
                                    <div class="img-holder">
                                       <div class="profiletipholder">
                                          <span class="profile-tooltip">
                                          <img class="circle" src="images/demo-profile.jpg"/>
                                          </span>
                                          <span class="profiletooltip_content">
                                             <div class="profile-tip" style="display:none;">
                                                <div class="slidingpan-holder">
                                                   <div class="profile-tip-avatar">
                                                      <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                                      <div class="sliding-pan location-span">
                                                         <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                      </div>
                                                   </div>
                                                   <div class="profile-tip-name">
                                                      <a href="javascript:void(0)">Adel Hasanat</a>
                                                   </div>
                                                   <div class="profile-tip-info">
                                                      <div class="profiletip-icon">
                                                         <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                      </div>
                                                      <div class="profiletip-icon">
                                                         <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                      </div>
                                                      <div class="profiletip-icon">
                                                         <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </span>
                                       </div>
                                    </div>
                                    <div class="desc-holder">
                                       <div class="normal-mode">
                                          <div class="desc">
                                             <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                             <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit...</p>
                                          </div>
                                          <div class="comment-stuff">
                                             <div class="more-opt">
                                                <span class="likeholder">
                                                <span class="like-tooltip">
                                                <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                                <span class="icon">icon</span>
                                                </a>
                                                </span>
                                                </span>  
                                                <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><span>icon</span></a>
                                                <!-- Dropdown Trigger -->
                                                <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='drop_down24'>
                                                <i class="zmdi zmdi-more"></i>
                                                </a>
                                                <!-- Dropdown Structure -->
                                                <ul id='drop_down24' class='dropdown-content custom_dropdown'>
                                                   <li>
                                                      <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                                   </li>
                                                   <li>
                                                      <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                                   </li>
                                                </ul>
                                             </div>
                                             <div class="less-opt">
                                                <div class="timestamp">8h</div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="edit-mode">
                                          <div class="desc">
                                             <textarea class="editcomment-tt materialize-textarea" id="ec-16">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                             <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="comment-reply-holder comment-addreply">
                                 <div class="addnew-comment valign-wrapper  comment-reply">
                                    <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                                    <div class="desc-holder">                          
                                       <textarea class="materialize-textarea">Write a reply...</textarea>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="pcomment-holder">
                              <div class="pcomment main-comment">
                                 <div class="img-holder">
                                    <div class="profiletipholder">
                                       <span class="profile-tooltip">
                                       <img class="circle" src="images/demo-profile.jpg"/>
                                       </span>
                                       <span class="profiletooltip_content slidingpan-holder">
                                          <div class="profile-tip" style="display:none;">
                                             <div class="profile-tip-avatar">
                                                <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                                <div class="sliding-pan location-span">
                                                   <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                </div>
                                             </div>
                                             <div class="profile-tip-name">
                                                <a href="javascript:void(0)">Adel Hasanat</a>
                                             </div>
                                             <div class="profile-tip-info">
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                </div>
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                </div>
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                                </div>
                                             </div>
                                          </div>
                                       </span>
                                    </div>
                                 </div>
                                 <div class="desc-holder">
                                    <div class="normal-mode">
                                       <div class="desc">
                                          <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                          <p class="shorten">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.<a a="" href="javascript:void(0)" class="overlay" onclick="explandReadMore(this)" style="position: absolute;top: 0;bottom: -6px;left: 0;right: 0;"><span class="readlink">Read More</span></a></p>
                                       </div>
                                       <div class="comment-stuff">
                                          <div class="more-opt">
                                             <span class="likeholder">
                                             <span class="like-tooltip">
                                             <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                             <span class="icon">icon</span>
                                             </a>
                                             </span>
                                             </span>  
                                             <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><span>icon</span></a>
                                             <!-- Dropdown Trigger -->
                                             <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='drop_down25'>
                                             <i class="zmdi zmdi-more"></i>
                                             </a>
                                             <!-- Dropdown Structure -->
                                             <ul id='drop_down25' class='dropdown-content custom_dropdown'>
                                                <li>
                                                   <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                                </li>
                                                <li>
                                                   <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                                </li>
                                             </ul>
                                          </div>
                                          <div class="less-opt">
                                             <div class="timestamp">8h</div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="edit-mode">
                                       <div class="desc">
                                          <textarea class="editcomment-tt materialize-textarea" id="ec-17">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                          <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="clear"></div>
                              <div class="comment-reply-holder comment-addreply">
                                 <div class="addnew-comment valign-wrapper  comment-reply">
                                    <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                                    <div class="desc-holder">                          
                                       <textarea class="materialize-textarea">Write a reply...</textarea>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="addnew-comment valign-wrapper ">
                        <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                        <div class="desc-holder">                          
                           <textarea class="materialize-textarea" id="comment_txt_3">Write a reply...</textarea>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <!-- end advertise action -->
      </div>
      <div class="col s12 m12">
         <!-- advertise page endorsement -->
         <div class="post-holder travad-box page-travad">
            <div class="post-topbar">
               <div class="post-userinfo">
                  <div class="img-holder">
                     <div class="profiletipholder">
                        <span class="profile-tooltip">
                        <img class="circle" src="images/hyattprofile.jpg"/>
                        </span>
                        <span class="profiletooltip_content slidingpan-holder">
                           <div class="profile-tip" style="display:none;">
                              <div class="profile-tip-avatar">
                                 <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                 <div class="sliding-pan location-span">
                                    <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                 </div>
                              </div>
                              <div class="profile-tip-name">
                                 <a href="javascript:void(0)">Adel Hasanat</a>
                              </div>
                              <div class="profile-tip-info">
                                 <div class="profiletip-icon">
                                    <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                 </div>
                                 <div class="profiletip-icon">
                                    <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                 </div>
                                 <div class="profiletip-icon">
                                    <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                 </div>
                              </div>
                           </div>
                        </span>
                     </div>
                  </div>
                  <div class="desc-holder">
                     <a href="javascript:void(0)">Hyatt Hotel</a>
                     <span class="timestamp">Sponsored Ad</span>
                  </div>
               </div>
               <div class="settings-icon">
			   <div class="dropdown dropdown-custom dropdown-xxsmall">
				  <a class="dropdown-button more_btn" href="javascript:void(0);" data-activates="setting_btn5" style="">					
                  <i class="zmdi zmdi-more"></i>
				  </a>
				  <ul id="setting_btn5" class="dropdown-content custom_dropdown" > <li>
                        <a href="javascript:void(0)">Hide ad</a>
                     </li>
                     <li>
                        <a href="javascript:void(0)">Save ad</a>
                     </li>
                     <li>
                        <a href="javascript:void(0)">Mute this seller ads</a>
                     </li>
                     <li>
                        <a href="javascript:void(0)">Report ad</a>
                     </li>
				  </ul>				 
				</div>
                  <!-- Dropdown Structure -->
               </div>
            </div>
            <div class="post-content">
               <div class="pdetail-holder">
                  <div class="shared-box shared-category">
                     <div class="post-holder">
                        <div class="post-content">
                           <div class="post-desc">
                              <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                           </div>
                           <div class="post-img-holder">
                              <div class="post-img one-img">
                                 <div class="pimg-holder"><img src="images/pagead-endorse.jpg"/></div>
                              </div>
                           </div>
                           <div class="share-summery">
                              <div class="travad-title">Best facilites you ever found!</div>
                              <div class="travad-subtitle">Endorse us for the best hospitality services we provide.</div>
                              <div class="travad-info">78 people endorsed this page</div>
                              <a href="javascript:void(0)" class="btn btn-primary btn-sm adbtn waves-effect waves-light">Endorse</a>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="clear"></div>
            <div class="post-data">
               <div class="post-actions">
                  <span class="likeholder">
                  <span class="like-tooltip">
                  <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                  <i class="zmdi zmdi-thumb-up"></i>
                  </a>
                  </span>
                  </span>
                  <a href="javascript:void(0)" class="sharepostmodalAction pa-share" title="Share"><i class="zmdi zmdi-mail-reply zmdi-hc-flip-horizontal"></i></a>
                  <span class="right">
                     <a href="javascript:void(0)" class="pa-comment" title="Comment" data-id="1"><i class="zmdi zmdi-comment"></i></a><span class="comment-lcount">4</span>
                  </span>
               </div>
               <div class="comments-section panel">
                  <div class="post-more">
                     <a href="javascript:void(0)" class="view-morec">View more comments <i class="zmdi zmdi-chevron-right"></i></a>
                     <span class="total-comments">3 of 7</span>
                  </div>
                  <div class="post-comments">
                     <div class="pcomments">
                        <div class="pcomment-earlier">
                           <div class="pcomment-holder">
                              <div class="pcomment main-comment">
                                 <div class="img-holder">
                                    <div class="profiletipholder">
                                       <span class="profile-tooltip">
                                       <img class="circle" src="images/demo-profile.jpg"/>
                                       </span>
                                       <span class="profiletooltip_content slidingpan-holder">
                                          <div class="profile-tip" style="display:none;">
                                             <div class="profile-tip-avatar">
                                                <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                                <div class="sliding-pan location-span">
                                                   <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                </div>
                                             </div>
                                             <div class="profile-tip-name">
                                                <a href="javascript:void(0)">Adel Hasanat</a>
                                             </div>
                                             <div class="profile-tip-info">
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                </div>
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                </div>
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                                </div>
                                             </div>
                                          </div>
                                       </span>
                                    </div>
                                 </div>
                                 <div class="desc-holder">
                                    <div class="normal-mode">
                                       <div class="desc">
                                          <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                          <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                       </div>
                                       <div class="comment-stuff">
                                          <div class="more-opt">
                                             <span class="likeholder">
                                             <span class="like-tooltip">
                                             <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                             <span class="icon">icon</span>
                                             </a>
                                             </span>
                                             </span>  
                                             <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><span>icon</span></a>
                                             <!-- Dropdown Trigger -->
                                             <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='drop_down26'>
                                             <i class="zmdi zmdi-more"></i>
                                             </a>
                                             <!-- Dropdown Structure -->
                                             <ul id='drop_down26' class='dropdown-content custom_dropdown'>
                                                <li>
                                                   <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                                </li>
                                                <li>
                                                   <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                                </li>
                                             </ul>
                                          </div>
                                          <div class="less-opt">
                                             <div class="timestamp">8h</div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="edit-mode">
                                       <div class="desc">
                                          <textarea class="editcomment-tt materialize-textarea" id="ec-12">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                          <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="clear"></div>
                              <div class="comment-reply-holder comment-addreply">
                                 <div class="addnew-comment valign-wrapper  comment-reply">
                                    <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                                    <div class="desc-holder">                          
                                       <textarea class="materialize-textarea">Write a reply...</textarea>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="pcomment-holder">
                              <div class="pcomment main-comment">
                                 <div class="img-holder">
                                    <div class="profiletipholder">
                                       <span class="profile-tooltip">
                                       <img class="circle" src="images/demo-profile.jpg"/>
                                       </span>
                                       <span class="profiletooltip_content slidingpan-holder">
                                          <div class="profile-tip" style="display:none;">
                                             <div class="profile-tip-avatar">
                                                <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                                <div class="sliding-pan location-span">
                                                   <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                </div>
                                             </div>
                                             <div class="profile-tip-name">
                                                <a href="javascript:void(0)">Adel Hasanat</a>
                                             </div>
                                             <div class="profile-tip-info">
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                </div>
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                </div>
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                                </div>
                                             </div>
                                          </div>
                                       </span>
                                    </div>
                                 </div>
                                 <div class="desc-holder">
                                    <div class="normal-mode">
                                       <div class="desc">
                                          <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                          <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                       </div>
                                       <div class="comment-stuff">
                                          <div class="more-opt">
                                             <span class="likeholder">
                                             <span class="like-tooltip">
                                             <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                             <span class="icon">icon</span>
                                             </a>
                                             </span>
                                             </span>  
                                             <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><span>icon</span></a>
                                             <!-- Dropdown Trigger -->
                                             <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='drop_down27'>
                                             <i class="zmdi zmdi-more"></i>
                                             </a>
                                             <!-- Dropdown Structure -->
                                             <ul id='drop_down27' class='dropdown-content custom_dropdown'>
                                                <li>
                                                   <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                                </li>
                                                <li>
                                                   <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                                </li>
                                             </ul>
                                          </div>
                                          <div class="less-opt">
                                             <div class="timestamp">8h</div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="edit-mode">
                                       <div class="desc">
                                          <textarea class="editcomment-tt materialize-textarea" id="ec-13">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                          <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="clear"></div>
                              <div class="comment-reply-holder comment-addreply">
                                 <div class="addnew-comment valign-wrapper  comment-reply">
                                    <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                                    <div class="desc-holder">                          
                                       <textarea class="materialize-textarea">Write a reply...</textarea>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="pcomment-holder has-comments">
                              <div class="pcomment main-comment">
                                 <div class="img-holder">
                                    <div class="profiletipholder">
                                       <span class="profile-tooltip">
                                       <img class="circle" src="images/demo-profile.jpg"/>
                                       </span>
                                       <span class="profiletooltip_content slidingpan-holder">
                                          <div class="profile-tip" style="display:none;">
                                             <div class="profile-tip-avatar">
                                                <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                                <div class="sliding-pan location-span">
                                                   <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                </div>
                                             </div>
                                             <div class="profile-tip-name">
                                                <a href="javascript:void(0)">Adel Hasanat</a>
                                             </div>
                                             <div class="profile-tip-info">
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                </div>
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                </div>
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                                </div>
                                             </div>
                                          </div>
                                       </span>
                                    </div>
                                 </div>
                                 <div class="desc-holder">
                                    <div class="normal-mode">
                                       <div class="desc">
                                          <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                          <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                       </div>
                                       <div class="comment-stuff">
                                          <div class="more-opt">
                                             <span class="likeholder">
                                             <span class="like-tooltip">
                                             <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                             <span class="icon">icon</span>
                                             </a>
                                             </span>
                                             </span>  
                                             <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><span>icon</span></a>
                                             <!-- Dropdown Trigger -->
                                             <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='drop_down28'>
                                             <i class="zmdi zmdi-more"></i>
                                             </a>
                                             <!-- Dropdown Structure -->
                                             <ul id='drop_down28' class='dropdown-content custom_dropdown'>
                                                <li>
                                                   <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                                </li>
                                                <li>
                                                   <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                                </li>
                                             </ul>
                                          </div>
                                          <div class="less-opt">
                                             <div class="timestamp">8h</div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="edit-mode">
                                       <div class="desc">
                                          <textarea class="editcomment-tt materialize-textarea" id="ec-14">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                          <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="clear"></div>
                              <div class="comment-reply-holder">
                                 <div class="pcomment comment-reply">
                                    <div class="img-holder">
                                       <div class="profiletipholder">
                                          <span class="profile-tooltip">
                                          <img class="circle" src="images/demo-profile.jpg"/>
                                          </span>
                                          <span class="profiletooltip_content">
                                             <div class="profile-tip" style="display:none;">
                                                <div class="slidingpan-holder">
                                                   <div class="profile-tip-avatar">
                                                      <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                                      <div class="sliding-pan location-span">
                                                         <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                      </div>
                                                   </div>
                                                   <div class="profile-tip-name">
                                                      <a href="javascript:void(0)">Adel Hasanat</a>
                                                   </div>
                                                   <div class="profile-tip-info">
                                                      <div class="profiletip-icon">
                                                         <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                      </div>
                                                      <div class="profiletip-icon">
                                                         <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                      </div>
                                                      <div class="profiletip-icon">
                                                         <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </span>
                                       </div>
                                    </div>
                                    <div class="desc-holder">
                                       <div class="normal-mode">
                                          <div class="desc">
                                             <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                             <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                          </div>
                                          <div class="comment-stuff">
                                             <div class="more-opt">
                                                <span class="likeholder">
                                                <span class="like-tooltip">
                                                <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                                <span class="icon">icon</span>
                                                </a>
                                                </span>
                                                </span>  
                                                <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><span>icon</span></a>
                                                <!-- Dropdown Trigger -->
                                                <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='drop_down29'>
                                                <i class="zmdi zmdi-more"></i>
                                                </a>
                                                <!-- Dropdown Structure -->
                                                <ul id='drop_down29' class='dropdown-content custom_dropdown'>
                                                   <li>
                                                      <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                                   </li>
                                                   <li>
                                                      <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                                   </li>
                                                </ul>
                                             </div>
                                             <div class="less-opt">
                                                <div class="timestamp">8h</div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="edit-mode">
                                          <div class="desc">
                                             <textarea class="editcomment-tt materialize-textarea" id="ec-15">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                             <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="pcomment comment-reply">
                                    <div class="img-holder">
                                       <div class="profiletipholder">
                                          <span class="profile-tooltip">
                                          <img class="circle" src="images/demo-profile.jpg"/>
                                          </span>
                                          <span class="profiletooltip_content">
                                             <div class="profile-tip" style="display:none;">
                                                <div class="slidingpan-holder">
                                                   <div class="profile-tip-avatar">
                                                      <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                                      <div class="sliding-pan location-span">
                                                         <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                      </div>
                                                   </div>
                                                   <div class="profile-tip-name">
                                                      <a href="javascript:void(0)">Adel Hasanat</a>
                                                   </div>
                                                   <div class="profile-tip-info">
                                                      <div class="profiletip-icon">
                                                         <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                      </div>
                                                      <div class="profiletip-icon">
                                                         <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                      </div>
                                                      <div class="profiletip-icon">
                                                         <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </span>
                                       </div>
                                    </div>
                                    <div class="desc-holder">
                                       <div class="normal-mode">
                                          <div class="desc">
                                             <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                             <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit...</p>
                                          </div>
                                          <div class="comment-stuff">
                                             <div class="more-opt">
                                                <span class="likeholder">
                                                <span class="like-tooltip">
                                                <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                                <span class="icon">icon</span>
                                                </a>
                                                </span>
                                                </span>  
                                                <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><span>icon</span></a>
                                                <!-- Dropdown Trigger -->
                                                <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='drop_down30'>
                                                <i class="zmdi zmdi-more"></i>
                                                </a>
                                                <!-- Dropdown Structure -->
                                                <ul id='drop_down30' class='dropdown-content custom_dropdown'>
                                                   <li>
                                                      <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                                   </li>
                                                   <li>
                                                      <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                                   </li>
                                                </ul>
                                             </div>
                                             <div class="less-opt">
                                                <div class="timestamp">8h</div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="edit-mode">
                                          <div class="desc">
                                             <textarea class="editcomment-tt materialize-textarea" id="ec-16">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                             <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="comment-reply-holder comment-addreply">
                                 <div class="addnew-comment valign-wrapper  comment-reply">
                                    <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                                    <div class="desc-holder">                          
                                       <textarea class="materialize-textarea">Write a reply...</textarea>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="pcomment-holder">
                              <div class="pcomment main-comment">
                                 <div class="img-holder">
                                    <div class="profiletipholder">
                                       <span class="profile-tooltip">
                                       <img class="circle" src="images/demo-profile.jpg"/>
                                       </span>
                                       <span class="profiletooltip_content slidingpan-holder">
                                          <div class="profile-tip" style="display:none;">
                                             <div class="profile-tip-avatar">
                                                <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                                <div class="sliding-pan location-span">
                                                   <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                </div>
                                             </div>
                                             <div class="profile-tip-name">
                                                <a href="javascript:void(0)">Adel Hasanat</a>
                                             </div>
                                             <div class="profile-tip-info">
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                </div>
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                </div>
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                                </div>
                                             </div>
                                          </div>
                                       </span>
                                    </div>
                                 </div>
                                 <div class="desc-holder">
                                    <div class="normal-mode">
                                       <div class="desc">
                                          <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                          <p class="shorten">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.<a a="" href="javascript:void(0)" class="overlay" onclick="explandReadMore(this)" style="position: absolute;top: 0;bottom: -6px;left: 0;right: 0;"><span class="readlink">Read More</span></a></p>
                                       </div>
                                       <div class="comment-stuff">
                                          <div class="more-opt">
                                             <span class="likeholder">
                                             <span class="like-tooltip">
                                             <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                             <span class="icon">icon</span>
                                             </a>
                                             </span>
                                             </span>  
                                             <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><span>icon</span></a>
                                             <!-- Dropdown Trigger -->
                                             <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='drop_down32'>
                                             <i class="zmdi zmdi-more"></i>
                                             </a>
                                             <!-- Dropdown Structure -->
                                             <ul id='drop_down32' class='dropdown-content custom_dropdown'>
                                                <li>
                                                   <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                                </li>
                                                <li>
                                                   <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                                </li>
                                             </ul>
                                          </div>
                                          <div class="less-opt">
                                             <div class="timestamp">8h</div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="edit-mode">
                                       <div class="desc">
                                          <textarea class="editcomment-tt materialize-textarea" id="ec-17">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                          <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="clear"></div>
                              <div class="comment-reply-holder comment-addreply">
                                 <div class="addnew-comment valign-wrapper  comment-reply">
                                    <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                                    <div class="desc-holder">                          
                                       <textarea class="materialize-textarea">Write a reply...</textarea>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="addnew-comment valign-wrapper ">
                        <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                        <div class="desc-holder">                          
                           <textarea class="materialize-textarea" id="comment_txt_3">Write a reply...</textarea>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <!-- end advertise page endorsement -->
      </div>
      <div class="col s12 m12">
         <!-- advertise action link -->
         <div class="post-holder travad-box actionlink-travad">
            <div class="post-topbar">
               <div class="post-userinfo">
                  <div class="img-holder">
                     <div class="profiletipholder">
                        <span class="profile-tooltip">
                        <img src="images/adimg-flight.jpg"/>
                        </span>
                        <span class="profiletooltip_content slidingpan-holder">
                           <div class="profile-tip" style="display:none;">
                              <div class="profile-tip-avatar">
                                 <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                 <div class="sliding-pan location-span">
                                    <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                 </div>
                              </div>
                              <div class="profile-tip-name">
                                 <a href="javascript:void(0)">Adel Hasanat</a>
                              </div>
                              <div class="profile-tip-info">
                                 <div class="profiletip-icon">
                                    <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                 </div>
                                 <div class="profiletip-icon">
                                    <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                 </div>
                                 <div class="profiletip-icon">
                                    <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                 </div>
                              </div>
                           </div>
                        </span>
                     </div>
                  </div>
                  <div class="desc-holder">
                     <div class="travad-maintitle">Jet Airways</div>
                     <span class="timestamp">Sponsored Ad</span>
                  </div>
               </div>
               <div class="settings-icon">
			   <div class="dropdown dropdown-custom dropdown-xxsmall">
				  <a class="dropdown-button more_btn" href="javascript:void(0);" data-activates="setting_btn6" style="">					
                  <i class="zmdi zmdi-more"></i>
				  </a>
				  <ul id="setting_btn6" class="dropdown-content custom_dropdown" > <li>
                        <a href="javascript:void(0)">Hide ad</a>
                     </li>
                     <li>
                        <a href="javascript:void(0)">Save ad</a>
                     </li>
                     <li>
                        <a href="javascript:void(0)">Mute this seller ads</a>
                     </li>
                     <li>
                        <a href="javascript:void(0)">Report ad</a>
                     </li>
				  </ul>				 
				</div>
                  <!-- Dropdown Structure -->
               </div>
            </div>
            <div class="post-content">
               <div class="pdetail-holder">
                  <div class="shared-box shared-category">
                     <div class="post-holder">
                        <div class="post-content">
                           <div class="post-desc">
                              <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                           </div>
                           <div class="post-img-holder">
                              <div class="post-img one-img">
                                 <div class="pimg-holder"><img src="images/admain-flight.jpg"/></div>
                              </div>
                           </div>
                           <div class="share-summery">
                              <div class="travad-title">Daily flight to London</div>
                              <div class="travad-subtitle">Now we introduce a daily flight to London from the major cities of your country.</div>
                              <a href="javascript:void(0)" class="btn btn-primary adbtn waves-effect waves-light">Book Now</a>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="clear"></div>
            <div class="post-data">
               <div class="post-actions">
                  <span class="likeholder">
                  <span class="like-tooltip">
                  <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                  <i class="zmdi zmdi-thumb-up"></i>
                  </a>
                  </span>
                  </span>
                  <a href="javascript:void(0)" class="sharepostmodalAction pa-share" title="Share"><i class="zmdi zmdi-mail-reply zmdi-hc-flip-horizontal"></i></a>
                  <span class="right">
                     <a href="javascript:void(0)" class="pa-comment" title="Comment" data-id="1"><i class="zmdi zmdi-comment"></i></a><span class="comment-lcount">4</span>
                  </span>
               </div>
               <div class="comments-section panel">
                  <div class="post-more">
                     <a href="javascript:void(0)" class="view-morec">View more comments <i class="zmdi zmdi-chevron-right"></i></a>
                     <span class="total-comments">3 of 7</span>
                  </div>
                  <div class="post-comments">
                     <div class="pcomments">
                        <div class="pcomment-earlier">
                           <div class="pcomment-holder">
                              <div class="pcomment main-comment">
                                 <div class="img-holder">
                                    <div class="profiletipholder">
                                       <span class="profile-tooltip">
                                       <img class="circle" src="images/demo-profile.jpg"/>
                                       </span>
                                       <span class="profiletooltip_content slidingpan-holder">
                                          <div class="profile-tip" style="display:none;">
                                             <div class="profile-tip-avatar">
                                                <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                                <div class="sliding-pan location-span">
                                                   <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                </div>
                                             </div>
                                             <div class="profile-tip-name">
                                                <a href="javascript:void(0)">Adel Hasanat</a>
                                             </div>
                                             <div class="profile-tip-info">
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                </div>
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                </div>
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                                </div>
                                             </div>
                                          </div>
                                       </span>
                                    </div>
                                 </div>
                                 <div class="desc-holder">
                                    <div class="normal-mode">
                                       <div class="desc">
                                          <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                          <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                       </div>
                                       <div class="comment-stuff">
                                          <div class="more-opt">
                                             <span class="likeholder">
                                             <span class="like-tooltip">
                                             <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                             <span class="icon">icon</span>
                                             </a>
                                             </span>
                                             </span>  
                                             <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><span>icon</span></a>
                                             <!-- Dropdown Trigger -->
                                             <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='drop_down33'>
                                             <i class="zmdi zmdi-more"></i>
                                             </a>
                                             <!-- Dropdown Structure -->
                                             <ul id='drop_down33' class='dropdown-content custom_dropdown'>
                                                <li>
                                                   <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                                </li>
                                                <li>
                                                   <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                                </li>
                                             </ul>
                                          </div>
                                          <div class="less-opt">
                                             <div class="timestamp">8h</div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="edit-mode">
                                       <div class="desc">
                                          <textarea class="editcomment-tt materialize-textarea" id="ec-12">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                          <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="clear"></div>
                              <div class="comment-reply-holder comment-addreply">
                                 <div class="addnew-comment valign-wrapper  comment-reply">
                                    <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                                    <div class="desc-holder">                          
                                       <textarea class="materialize-textarea">Write a reply...</textarea>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="pcomment-holder">
                              <div class="pcomment main-comment">
                                 <div class="img-holder">
                                    <div class="profiletipholder">
                                       <span class="profile-tooltip">
                                       <img class="circle" src="images/demo-profile.jpg"/>
                                       </span>
                                       <span class="profiletooltip_content slidingpan-holder">
                                          <div class="profile-tip" style="display:none;">
                                             <div class="profile-tip-avatar">
                                                <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                                <div class="sliding-pan location-span">
                                                   <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                </div>
                                             </div>
                                             <div class="profile-tip-name">
                                                <a href="javascript:void(0)">Adel Hasanat</a>
                                             </div>
                                             <div class="profile-tip-info">
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                </div>
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                </div>
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                                </div>
                                             </div>
                                          </div>
                                       </span>
                                    </div>
                                 </div>
                                 <div class="desc-holder">
                                    <div class="normal-mode">
                                       <div class="desc">
                                          <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                          <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                       </div>
                                       <div class="comment-stuff">
                                          <div class="more-opt">
                                             <span class="likeholder">
                                             <span class="like-tooltip">
                                             <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                             <span class="icon">icon</span>
                                             </a>
                                             </span>
                                             </span>  
                                             <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><span>icon</span></a>
                                             <!-- Dropdown Trigger -->
                                             <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='drop_down34'>
                                             <i class="zmdi zmdi-more"></i>
                                             </a>
                                             <!-- Dropdown Structure -->
                                             <ul id='drop_down34' class='dropdown-content custom_dropdown'>
                                                <li>
                                                   <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                                </li>
                                                <li>
                                                   <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                                </li>
                                             </ul>
                                          </div>
                                          <div class="less-opt">
                                             <div class="timestamp">8h</div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="edit-mode">
                                       <div class="desc">
                                          <textarea class="editcomment-tt materialize-textarea" id="ec-13">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                          <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="clear"></div>
                              <div class="comment-reply-holder comment-addreply">
                                 <div class="addnew-comment valign-wrapper  comment-reply">
                                    <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                                    <div class="desc-holder">                          
                                       <textarea class="materialize-textarea">Write a reply...</textarea>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="pcomment-holder has-comments">
                              <div class="pcomment main-comment">
                                 <div class="img-holder">
                                    <div class="profiletipholder">
                                       <span class="profile-tooltip">
                                       <img class="circle" src="images/demo-profile.jpg"/>
                                       </span>
                                       <span class="profiletooltip_content slidingpan-holder">
                                          <div class="profile-tip" style="display:none;">
                                             <div class="profile-tip-avatar">
                                                <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                                <div class="sliding-pan location-span">
                                                   <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                </div>
                                             </div>
                                             <div class="profile-tip-name">
                                                <a href="javascript:void(0)">Adel Hasanat</a>
                                             </div>
                                             <div class="profile-tip-info">
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                </div>
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                </div>
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                                </div>
                                             </div>
                                          </div>
                                       </span>
                                    </div>
                                 </div>
                                 <div class="desc-holder">
                                    <div class="normal-mode">
                                       <div class="desc">
                                          <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                          <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                       </div>
                                       <div class="comment-stuff">
                                          <div class="more-opt">
                                             <span class="likeholder">
                                             <span class="like-tooltip">
                                             <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                             <span class="icon">icon</span>
                                             </a>
                                             </span>
                                             </span>  
                                             <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><span>icon</span></a>
                                             <!-- Dropdown Trigger -->
                                             <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='drop_down35'>
                                             <i class="zmdi zmdi-more"></i>
                                             </a>
                                             <!-- Dropdown Structure -->
                                             <ul id='drop_down35' class='dropdown-content custom_dropdown'>
                                                <li>
                                                   <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                                </li>
                                                <li>
                                                   <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                                </li>
                                             </ul>
                                          </div>
                                          <div class="less-opt">
                                             <div class="timestamp">8h</div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="edit-mode">
                                       <div class="desc">
                                          <textarea class="editcomment-tt materialize-textarea" id="ec-14">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                          <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="clear"></div>
                              <div class="comment-reply-holder">
                                 <div class="pcomment comment-reply">
                                    <div class="img-holder">
                                       <div class="profiletipholder">
                                          <span class="profile-tooltip">
                                          <img class="circle" src="images/demo-profile.jpg"/>
                                          </span>
                                          <span class="profiletooltip_content">
                                             <div class="profile-tip" style="display:none;">
                                                <div class="slidingpan-holder">
                                                   <div class="profile-tip-avatar">
                                                      <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                                      <div class="sliding-pan location-span">
                                                         <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                      </div>
                                                   </div>
                                                   <div class="profile-tip-name">
                                                      <a href="javascript:void(0)">Adel Hasanat</a>
                                                   </div>
                                                   <div class="profile-tip-info">
                                                      <div class="profiletip-icon">
                                                         <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                      </div>
                                                      <div class="profiletip-icon">
                                                         <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                      </div>
                                                      <div class="profiletip-icon">
                                                         <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </span>
                                       </div>
                                    </div>
                                    <div class="desc-holder">
                                       <div class="normal-mode">
                                          <div class="desc">
                                             <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                             <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                          </div>
                                          <div class="comment-stuff">
                                             <div class="more-opt">
                                                <span class="likeholder">
                                                <span class="like-tooltip">
                                                <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                                <span class="icon">icon</span>
                                                </a>
                                                </span>
                                                </span>  
                                                <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><span>icon</span></a>
                                                <!-- Dropdown Trigger -->
                                                <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='drop_down36'>
                                                <i class="zmdi zmdi-more"></i>
                                                </a>
                                                <!-- Dropdown Structure -->
                                                <ul id='drop_down36' class='dropdown-content custom_dropdown'>
                                                   <li>
                                                      <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                                   </li>
                                                   <li>
                                                      <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                                   </li>
                                                </ul>
                                             </div>
                                             <div class="less-opt">
                                                <div class="timestamp">8h</div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="edit-mode">
                                          <div class="desc">
                                             <textarea class="editcomment-tt materialize-textarea" id="ec-15">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                             <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="pcomment comment-reply">
                                    <div class="img-holder">
                                       <div class="profiletipholder">
                                          <span class="profile-tooltip">
                                          <img class="circle" src="images/demo-profile.jpg"/>
                                          </span>
                                          <span class="profiletooltip_content">
                                             <div class="profile-tip" style="display:none;">
                                                <div class="slidingpan-holder">
                                                   <div class="profile-tip-avatar">
                                                      <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                                      <div class="sliding-pan location-span">
                                                         <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                      </div>
                                                   </div>
                                                   <div class="profile-tip-name">
                                                      <a href="javascript:void(0)">Adel Hasanat</a>
                                                   </div>
                                                   <div class="profile-tip-info">
                                                      <div class="profiletip-icon">
                                                         <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                      </div>
                                                      <div class="profiletip-icon">
                                                         <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                      </div>
                                                      <div class="profiletip-icon">
                                                         <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </span>
                                       </div>
                                    </div>
                                    <div class="desc-holder">
                                       <div class="normal-mode">
                                          <div class="desc">
                                             <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                             <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit...</p>
                                          </div>
                                          <div class="comment-stuff">
                                             <div class="more-opt">
                                                <span class="likeholder">
                                                <span class="like-tooltip">
                                                <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                                <span class="icon">icon</span>
                                                </a>
                                                </span>
                                                </span>  
                                                <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><span>icon</span></a>
                                                <!-- Dropdown Trigger -->
                                                <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='drop_down37'>
                                                <i class="zmdi zmdi-more"></i>
                                                </a>
                                                <!-- Dropdown Structure -->
                                                <ul id='drop_down37' class='dropdown-content custom_dropdown'>
                                                   <li>
                                                      <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                                   </li>
                                                   <li>
                                                      <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                                   </li>
                                                </ul>
                                             </div>
                                             <div class="less-opt">
                                                <div class="timestamp">8h</div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="edit-mode">
                                          <div class="desc">
                                             <textarea class="editcomment-tt materialize-textarea" id="ec-16">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                             <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="comment-reply-holder comment-addreply">
                                 <div class="addnew-comment valign-wrapper  comment-reply">
                                    <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                                    <div class="desc-holder">                          
                                       <textarea class="materialize-textarea">Write a reply...</textarea>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="pcomment-holder">
                              <div class="pcomment main-comment">
                                 <div class="img-holder">
                                    <div class="profiletipholder">
                                       <span class="profile-tooltip">
                                       <img class="circle" src="images/demo-profile.jpg"/>
                                       </span>
                                       <span class="profiletooltip_content slidingpan-holder">
                                          <div class="profile-tip" style="display:none;">
                                             <div class="profile-tip-avatar">
                                                <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                                <div class="sliding-pan location-span">
                                                   <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                </div>
                                             </div>
                                             <div class="profile-tip-name">
                                                <a href="javascript:void(0)">Adel Hasanat</a>
                                             </div>
                                             <div class="profile-tip-info">
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                </div>
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                </div>
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                                </div>
                                             </div>
                                          </div>
                                       </span>
                                    </div>
                                 </div>
                                 <div class="desc-holder">
                                    <div class="normal-mode">
                                       <div class="desc">
                                          <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                          <p class="shorten">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.<a a="" href="javascript:void(0)" class="overlay" onclick="explandReadMore(this)" style="position: absolute;top: 0;bottom: -6px;left: 0;right: 0;"><span class="readlink">Read More</span></a></p>
                                       </div>
                                       <div class="comment-stuff">
                                          <div class="more-opt">
                                             <span class="likeholder">
                                             <span class="like-tooltip">
                                             <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                             <span class="icon">icon</span>
                                             </a>
                                             </span>
                                             </span>  
                                             <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><span>icon</span></a>
                                             <!-- Dropdown Trigger -->
                                             <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='drop_down38'>
                                             <i class="zmdi zmdi-more"></i>
                                             </a>
                                             <!-- Dropdown Structure -->
                                             <ul id='drop_down38' class='dropdown-content custom_dropdown'>
                                                <li>
                                                   <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                                </li>
                                                <li>
                                                   <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                                </li>
                                             </ul>
                                          </div>
                                          <div class="less-opt">
                                             <div class="timestamp">8h</div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="edit-mode">
                                       <div class="desc">
                                          <textarea class="editcomment-tt materialize-textarea" id="ec-17">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                          <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="clear"></div>
                              <div class="comment-reply-holder comment-addreply">
                                 <div class="addnew-comment valign-wrapper  comment-reply">
                                    <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                                    <div class="desc-holder">                          
                                       <textarea class="materialize-textarea">Write a reply...</textarea>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="addnew-comment valign-wrapper ">
                        <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                        <div class="desc-holder">                          
                           <textarea class="materialize-textarea" id="comment_txt_3">Write a reply...</textarea>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <!-- end advertise action link -->
      </div>
      <div class="col s12 m12">
         <!-- advertise website link -->
         <div class="post-holder travad-box weblink-travad">
            <div class="post-topbar">
               <div class="post-userinfo">
                  <div class="img-holder">
                     <div class="profiletipholder">
                        <span class="profile-tooltip">
                        <img class="circle" src="images/adimg-food.jpg"/>
                        </span>
                        <span class="profiletooltip_content slidingpan-holder">
                           <div class="profile-tip" style="display:none;">
                              <div class="profile-tip-avatar">
                                 <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                 <div class="sliding-pan location-span">
                                    <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                 </div>
                              </div>
                              <div class="profile-tip-name">
                                 <a href="javascript:void(0)">Adel Hasanat</a>
                              </div>
                              <div class="profile-tip-info">
                                 <div class="profiletip-icon">
                                    <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                 </div>
                                 <div class="profiletip-icon">
                                    <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                 </div>
                                 <div class="profiletip-icon">
                                    <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                 </div>
                              </div>
                           </div>
                        </span>
                     </div>
                  </div>
                  <div class="desc-holder">
                     <div class="travad-maintitle">Avida Food Hunt</div>
                     <span class="timestamp">Sponsored Ad</span>
                  </div>
               </div>
               <div class="settings-icon">
			   <div class="dropdown dropdown-custom dropdown-xxsmall">
				  <a class="dropdown-button more_btn" href="javascript:void(0);" data-activates="setting_btn7" style="">					
                  <i class="zmdi zmdi-more"></i>
				  </a>
				  <ul id="setting_btn7" class="dropdown-content custom_dropdown" > <li>
                        <a href="javascript:void(0)">Hide ad</a>
                     </li>
                     <li>
                        <a href="javascript:void(0)">Save ad</a>
                     </li>
                     <li>
                        <a href="javascript:void(0)">Mute this seller ads</a>
                     </li>
                     <li>
                        <a href="javascript:void(0)">Report ad</a>
                     </li>
				  </ul>				 
				</div>
                  <!-- Dropdown Structure -->
               </div>
            </div>
            <div class="post-content">
               <div class="pdetail-holder">
                  <div class="shared-box shared-category">
                     <div class="post-holder">
                        <div class="post-content">
                           <div class="post-desc">
                              <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                           </div>
                           <div class="post-img-holder">
                              <div class="post-img one-img">
                                 <div class="pimg-holder"><img src="images/admain-food.jpg"/></div>
                              </div>
                           </div>
                           <div class="share-summery">
                              <div class="travad-title">30% off on special pizza this weekend!</div>
                              <div class="travad-subtitle">We bring you with flat 30% off on Avida special pizza this festive weekend.</div>
                              <a href="javascript:void(0)" class="adlink"><i class="mdi mdi-earth"></i><span>www.avidafoodhunt.com</span></a>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="clear"></div>
            <div class="post-data">
               <div class="post-actions">
                  <span class="likeholder">
                  <span class="like-tooltip">
                  <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                  <i class="zmdi zmdi-thumb-up"></i>
                  </a>
                  </span>
                  </span>
                  <a href="javascript:void(0)" class="sharepostmodalAction pa-share" title="Share"><i class="zmdi zmdi-mail-reply zmdi-hc-flip-horizontal"></i></a>
                  <span class="right">
                     <a href="javascript:void(0)" class="pa-comment" title="Comment" data-id="1"><i class="zmdi zmdi-comment"></i></a><span class="comment-lcount">4</span>
                  </span>
               </div>
               <div class="comments-section panel">
                  <div class="post-more">
                     <a href="javascript:void(0)" class="view-morec">View more comments <i class="zmdi zmdi-chevron-right"></i></a>
                     <span class="total-comments">3 of 7</span>
                  </div>
                  <div class="post-comments">
                     <div class="pcomments">
                        <div class="pcomment-earlier">
                           <div class="pcomment-holder">
                              <div class="pcomment main-comment">
                                 <div class="img-holder">
                                    <div class="profiletipholder">
                                       <span class="profile-tooltip">
                                       <img class="circle" src="images/demo-profile.jpg"/>
                                       </span>
                                       <span class="profiletooltip_content slidingpan-holder">
                                          <div class="profile-tip" style="display:none;">
                                             <div class="profile-tip-avatar">
                                                <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                                <div class="sliding-pan location-span">
                                                   <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                </div>
                                             </div>
                                             <div class="profile-tip-name">
                                                <a href="javascript:void(0)">Adel Hasanat</a>
                                             </div>
                                             <div class="profile-tip-info">
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                </div>
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                </div>
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                                </div>
                                             </div>
                                          </div>
                                       </span>
                                    </div>
                                 </div>
                                 <div class="desc-holder">
                                    <div class="normal-mode">
                                       <div class="desc">
                                          <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                          <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                       </div>
                                       <div class="comment-stuff">
                                          <div class="more-opt">
                                             <span class="likeholder">
                                             <span class="like-tooltip">
                                             <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                             <span class="icon">icon</span>
                                             </a>
                                             </span>
                                             </span>  
                                             <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><span>icon</span></a>
                                             <!-- Dropdown Trigger -->
                                             <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='drop_down39'>
                                             <i class="zmdi zmdi-more"></i>
                                             </a>
                                             <!-- Dropdown Structure -->
                                             <ul id='drop_down39' class='dropdown-content custom_dropdown'>
                                                <li>
                                                   <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                                </li>
                                                <li>
                                                   <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                                </li>
                                             </ul>
                                          </div>
                                          <div class="less-opt">
                                             <div class="timestamp">8h</div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="edit-mode">
                                       <div class="desc">
                                          <textarea class="editcomment-tt materialize-textarea" id="ec-12">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                          <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="clear"></div>
                              <div class="comment-reply-holder comment-addreply">
                                 <div class="addnew-comment valign-wrapper  comment-reply">
                                    <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                                    <div class="desc-holder">                          
                                       <textarea class="materialize-textarea">Write a reply...</textarea>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="pcomment-holder">
                              <div class="pcomment main-comment">
                                 <div class="img-holder">
                                    <div class="profiletipholder">
                                       <span class="profile-tooltip">
                                       <img class="circle" src="images/demo-profile.jpg"/>
                                       </span>
                                       <span class="profiletooltip_content slidingpan-holder">
                                          <div class="profile-tip" style="display:none;">
                                             <div class="profile-tip-avatar">
                                                <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                                <div class="sliding-pan location-span">
                                                   <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                </div>
                                             </div>
                                             <div class="profile-tip-name">
                                                <a href="javascript:void(0)">Adel Hasanat</a>
                                             </div>
                                             <div class="profile-tip-info">
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                </div>
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                </div>
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                                </div>
                                             </div>
                                          </div>
                                       </span>
                                    </div>
                                 </div>
                                 <div class="desc-holder">
                                    <div class="normal-mode">
                                       <div class="desc">
                                          <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                          <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                       </div>
                                       <div class="comment-stuff">
                                          <div class="more-opt">
                                             <span class="likeholder">
                                             <span class="like-tooltip">
                                             <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                             <span class="icon">icon</span>
                                             </a>
                                             </span>
                                             </span>  
                                             <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><span>icon</span></a>
                                             <!-- Dropdown Trigger -->
                                             <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='drop_down40'>
                                             <i class="zmdi zmdi-more"></i>
                                             </a>
                                             <!-- Dropdown Structure -->
                                             <ul id='drop_down40' class='dropdown-content custom_dropdown'>
                                                <li>
                                                   <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                                </li>
                                                <li>
                                                   <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                                </li>
                                             </ul>
                                          </div>
                                          <div class="less-opt">
                                             <div class="timestamp">8h</div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="edit-mode">
                                       <div class="desc">
                                          <textarea class="editcomment-tt materialize-textarea" id="ec-13">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                          <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="clear"></div>
                              <div class="comment-reply-holder comment-addreply">
                                 <div class="addnew-comment valign-wrapper  comment-reply">
                                    <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                                    <div class="desc-holder">                          
                                       <textarea class="materialize-textarea">Write a reply...</textarea>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="pcomment-holder has-comments">
                              <div class="pcomment main-comment">
                                 <div class="img-holder">
                                    <div class="profiletipholder">
                                       <span class="profile-tooltip">
                                       <img class="circle" src="images/demo-profile.jpg"/>
                                       </span>
                                       <span class="profiletooltip_content slidingpan-holder">
                                          <div class="profile-tip" style="display:none;">
                                             <div class="profile-tip-avatar">
                                                <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                                <div class="sliding-pan location-span">
                                                   <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                </div>
                                             </div>
                                             <div class="profile-tip-name">
                                                <a href="javascript:void(0)">Adel Hasanat</a>
                                             </div>
                                             <div class="profile-tip-info">
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                </div>
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                </div>
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                                </div>
                                             </div>
                                          </div>
                                       </span>
                                    </div>
                                 </div>
                                 <div class="desc-holder">
                                    <div class="normal-mode">
                                       <div class="desc">
                                          <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                          <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                       </div>
                                       <div class="comment-stuff">
                                          <div class="more-opt">
                                             <span class="likeholder">
                                             <span class="like-tooltip">
                                             <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                             <span class="icon">icon</span>
                                             </a>
                                             </span>
                                             </span>  
                                             <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><span>icon</span></a>
                                             <!-- Dropdown Trigger -->
                                             <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='drop_down42'>
                                             <i class="zmdi zmdi-more"></i>
                                             </a>
                                             <!-- Dropdown Structure -->
                                             <ul id='drop_down42' class='dropdown-content custom_dropdown'>
                                                <li>
                                                   <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                                </li>
                                                <li>
                                                   <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                                </li>
                                             </ul>
                                          </div>
                                          <div class="less-opt">
                                             <div class="timestamp">8h</div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="edit-mode">
                                       <div class="desc">
                                          <textarea class="editcomment-tt materialize-textarea" id="ec-14">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                          <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="clear"></div>
                              <div class="comment-reply-holder">
                                 <div class="pcomment comment-reply">
                                    <div class="img-holder">
                                       <div class="profiletipholder">
                                          <span class="profile-tooltip">
                                          <img class="circle" src="images/demo-profile.jpg"/>
                                          </span>
                                          <span class="profiletooltip_content">
                                             <div class="profile-tip" style="display:none;">
                                                <div class="slidingpan-holder">
                                                   <div class="profile-tip-avatar">
                                                      <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                                      <div class="sliding-pan location-span">
                                                         <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                      </div>
                                                   </div>
                                                   <div class="profile-tip-name">
                                                      <a href="javascript:void(0)">Adel Hasanat</a>
                                                   </div>
                                                   <div class="profile-tip-info">
                                                      <div class="profiletip-icon">
                                                         <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                      </div>
                                                      <div class="profiletip-icon">
                                                         <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                      </div>
                                                      <div class="profiletip-icon">
                                                         <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </span>
                                       </div>
                                    </div>
                                    <div class="desc-holder">
                                       <div class="normal-mode">
                                          <div class="desc">
                                             <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                             <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                          </div>
                                          <div class="comment-stuff">
                                             <div class="more-opt">
                                                <span class="likeholder">
                                                <span class="like-tooltip">
                                                <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                                <span class="icon">icon</span>
                                                </a>
                                                </span>
                                                </span>  
                                                <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><span>icon</span></a>
                                                <!-- Dropdown Trigger -->
                                                <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='drop_down43'>
                                                <i class="zmdi zmdi-more"></i>
                                                </a>
                                                <!-- Dropdown Structure -->
                                                <ul id='drop_down43' class='dropdown-content custom_dropdown'>
                                                   <li>
                                                      <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                                   </li>
                                                   <li>
                                                      <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                                   </li>
                                                </ul>
                                             </div>
                                             <div class="less-opt">
                                                <div class="timestamp">8h</div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="edit-mode">
                                          <div class="desc">
                                             <textarea class="editcomment-tt materialize-textarea" id="ec-15">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                             <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="pcomment comment-reply">
                                    <div class="img-holder">
                                       <div class="profiletipholder">
                                          <span class="profile-tooltip">
                                          <img class="circle" src="images/demo-profile.jpg"/>
                                          </span>
                                          <span class="profiletooltip_content">
                                             <div class="profile-tip" style="display:none;">
                                                <div class="slidingpan-holder">
                                                   <div class="profile-tip-avatar">
                                                      <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                                      <div class="sliding-pan location-span">
                                                         <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                      </div>
                                                   </div>
                                                   <div class="profile-tip-name">
                                                      <a href="javascript:void(0)">Adel Hasanat</a>
                                                   </div>
                                                   <div class="profile-tip-info">
                                                      <div class="profiletip-icon">
                                                         <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                      </div>
                                                      <div class="profiletip-icon">
                                                         <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                      </div>
                                                      <div class="profiletip-icon">
                                                         <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </span>
                                       </div>
                                    </div>
                                    <div class="desc-holder">
                                       <div class="normal-mode">
                                          <div class="desc">
                                             <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                             <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit...</p>
                                          </div>
                                          <div class="comment-stuff">
                                             <div class="more-opt">
                                                <span class="likeholder">
                                                <span class="like-tooltip">
                                                <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                                <span class="icon">icon</span>
                                                </a>
                                                </span>
                                                </span>  
                                                <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><span>icon</span></a>
                                                <!-- Dropdown Trigger -->
                                                <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='drop_down44'>
                                                <i class="zmdi zmdi-more"></i>
                                                </a>
                                                <!-- Dropdown Structure -->
                                                <ul id='drop_down44' class='dropdown-content custom_dropdown'>
                                                   <li>
                                                      <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                                   </li>
                                                   <li>
                                                      <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                                   </li>
                                                </ul>
                                             </div>
                                             <div class="less-opt">
                                                <div class="timestamp">8h</div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="edit-mode">
                                          <div class="desc">
                                             <textarea class="editcomment-tt materialize-textarea" id="ec-16">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                             <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="comment-reply-holder comment-addreply">
                                 <div class="addnew-comment valign-wrapper  comment-reply">
                                    <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                                    <div class="desc-holder">                          
                                       <textarea class="materialize-textarea">Write a reply...</textarea>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="pcomment-holder">
                              <div class="pcomment main-comment">
                                 <div class="img-holder">
                                    <div class="profiletipholder">
                                       <span class="profile-tooltip">
                                       <img class="circle" src="images/demo-profile.jpg"/>
                                       </span>
                                       <span class="profiletooltip_content slidingpan-holder">
                                          <div class="profile-tip" style="display:none;">
                                             <div class="profile-tip-avatar">
                                                <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                                <div class="sliding-pan location-span">
                                                   <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                </div>
                                             </div>
                                             <div class="profile-tip-name">
                                                <a href="javascript:void(0)">Adel Hasanat</a>
                                             </div>
                                             <div class="profile-tip-info">
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                </div>
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                </div>
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                                </div>
                                             </div>
                                          </div>
                                       </span>
                                    </div>
                                 </div>
                                 <div class="desc-holder">
                                    <div class="normal-mode">
                                       <div class="desc">
                                          <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                          <p class="shorten">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.<a a="" href="javascript:void(0)" class="overlay" onclick="explandReadMore(this)" style="position: absolute;top: 0;bottom: -6px;left: 0;right: 0;"><span class="readlink">Read More</span></a></p>
                                       </div>
                                       <div class="comment-stuff">
                                          <div class="more-opt">
                                             <span class="likeholder">
                                             <span class="like-tooltip">
                                             <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                             <span class="icon">icon</span>
                                             </a>
                                             </span>
                                             </span>  
                                             <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><span>icon</span></a>
                                             <!-- Dropdown Trigger -->
                                             <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='drop_down45'>
                                             <i class="zmdi zmdi-more"></i>
                                             </a>
                                             <!-- Dropdown Structure -->
                                             <ul id='drop_down45' class='dropdown-content custom_dropdown'>
                                                <li>
                                                   <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                                </li>
                                                <li>
                                                   <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                                </li>
                                             </ul>
                                          </div>
                                          <div class="less-opt">
                                             <div class="timestamp">8h</div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="edit-mode">
                                       <div class="desc">
                                          <textarea class="editcomment-tt materialize-textarea" id="ec-17">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                          <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="clear"></div>
                              <div class="comment-reply-holder comment-addreply">
                                 <div class="addnew-comment valign-wrapper  comment-reply">
                                    <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                                    <div class="desc-holder">                          
                                       <textarea class="materialize-textarea">Write a reply...</textarea>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="addnew-comment valign-wrapper  ">
                        <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                        <div class="desc-holder">
                           <div class="sliding-middle-out anim-area underlined">
                              <textarea class="materialize-textarea" id="comment_txt_3">Write a reply...</textarea>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <!-- end advertise website link -->
      </div>
      <div class="col s12 m12">
         <!-- advertise page -->
         <div class="post-holder travad-box page-travad">
            <div class="post-topbar">
               <div class="post-userinfo">
                  <div class="img-holder">
                     <div class="profiletipholder">
                        <span class="profile-tooltip">
                        <img class="circle" src="images/hyattprofile.jpg"/>
                        </span>
                        <span class="profiletooltip_content slidingpan-holder">
                           <div class="profile-tip" style="display:none;">
                              <div class="profile-tip-avatar">
                                 <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                 <div class="sliding-pan location-span">
                                    <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                 </div>
                              </div>
                              <div class="profile-tip-name">
                                 <a href="javascript:void(0)">Adel Hasanat</a>
                              </div>
                              <div class="profile-tip-info">
                                 <div class="profiletip-icon">
                                    <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                 </div>
                                 <div class="profiletip-icon">
                                    <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                 </div>
                                 <div class="profiletip-icon">
                                    <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                 </div>
                              </div>
                           </div>
                        </span>
                     </div>
                  </div>
                  <div class="desc-holder">
                     <a href="javascript:void(0)">Hyatt Hotel</a>
                     <span class="timestamp">Sponsored Ad</span>
                  </div>
               </div>
               <div class="settings-icon">
			   <div class="dropdown dropdown-custom dropdown-xxsmall">
				  <a class="dropdown-button more_btn" href="javascript:void(0);" data-activates="setting_btn8" style="">					
                  <i class="zmdi zmdi-more"></i>
				  </a>
				  <ul id="setting_btn8" class="dropdown-content custom_dropdown" > <li>
                        <a href="javascript:void(0)">Hide ad</a>
                     </li>
                     <li>
                        <a href="javascript:void(0)">Save ad</a>
                     </li>
                     <li>
                        <a href="javascript:void(0)">Mute this seller ads</a>
                     </li>
                     <li>
                        <a href="javascript:void(0)">Report ad</a>
                     </li>
				  </ul>				 
				</div>
                  <!-- Dropdown Structure -->
               </div>
            </div>
            <div class="post-content">
               <div class="pdetail-holder">
                  <div class="shared-box shared-category">
                     <div class="post-holder">
                        <div class="post-content">
                           <div class="post-desc">
                              <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                           </div>
                           <div class="post-img-holder">
                              <div class="post-img one-img">
                                 <div class="pimg-holder"><img src="images/pagead.jpg"/></div>
                              </div>
                           </div>
                           <div class="share-summery">
                              <div class="travad-title">Get a luxurious experience with us!</div>
                              <div class="travad-subtitle">We are here to provide you high class services with variety of amenities.</div>
                              <div class="travad-info">345 people liked this</div>
                              <a href="javascript:void(0)" class="btn btn-primary btn-sm adbtn waves-effect waves-light"><i class="zmdi zmdi-thumb-up"></i>Like</a>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="clear"></div>
            <div class="post-data">
               <div class="post-actions">
                  <span class="likeholder">
                  <span class="like-tooltip">
                  <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                  <i class="zmdi zmdi-thumb-up"></i>
                  </a>
                  </span>
                  </span>
                  <a href="javascript:void(0)" class="sharepostmodalAction pa-share" title="Share"><i class="zmdi zmdi-mail-reply zmdi-hc-flip-horizontal"></i></a>
                  <span class="right">
                     <a href="javascript:void(0)" class="pa-comment" title="Comment" data-id="1"><i class="zmdi zmdi-comment"></i></a><span class="comment-lcount">4</span>
                  </span>
               </div>
               <div class="comments-section panel">
                  <div class="post-more">
                     <a href="javascript:void(0)" class="view-morec">View more comments <i class="zmdi zmdi-chevron-right"></i></a>
                     <span class="total-comments">3 of 7</span>
                  </div>
                  <div class="post-comments">
                     <div class="pcomments">
                        <div class="pcomment-earlier">
                           <div class="pcomment-holder">
                              <div class="pcomment main-comment">
                                 <div class="img-holder">
                                    <div class="profiletipholder">
                                       <span class="profile-tooltip">
                                       <img class="circle" src="images/demo-profile.jpg"/>
                                       </span>
                                       <span class="profiletooltip_content slidingpan-holder">
                                          <div class="profile-tip" style="display:none;">
                                             <div class="profile-tip-avatar">
                                                <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                                <div class="sliding-pan location-span">
                                                   <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                </div>
                                             </div>
                                             <div class="profile-tip-name">
                                                <a href="javascript:void(0)">Adel Hasanat</a>
                                             </div>
                                             <div class="profile-tip-info">
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                </div>
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                </div>
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                                </div>
                                             </div>
                                          </div>
                                       </span>
                                    </div>
                                 </div>
                                 <div class="desc-holder">
                                    <div class="normal-mode">
                                       <div class="desc">
                                          <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                          <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                       </div>
                                       <div class="comment-stuff">
                                          <div class="more-opt">
                                             <span class="likeholder">
                                             <span class="like-tooltip">
                                             <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                             <span class="icon">icon</span>
                                             </a>
                                             </span>
                                             </span>  
                                             <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><span>icon</span></a>
                                             <!-- Dropdown Trigger -->
                                             <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='drop_down46'>
                                             <i class="zmdi zmdi-more"></i>
                                             </a>
                                             <!-- Dropdown Structure -->
                                             <ul id='drop_down46' class='dropdown-content custom_dropdown'>
                                                <li>
                                                   <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                                </li>
                                                <li>
                                                   <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                                </li>
                                             </ul>
                                          </div>
                                          <div class="less-opt">
                                             <div class="timestamp">8h</div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="edit-mode">
                                       <div class="desc">
                                          <textarea class="editcomment-tt materialize-textarea" id="ec-12">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                          <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="clear"></div>
                              <div class="comment-reply-holder comment-addreply">
                                 <div class="addnew-comment valign-wrapper  comment-reply">
                                    <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                                    <div class="desc-holder">                          
                                       <textarea class="materialize-textarea">Write a reply...</textarea>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="pcomment-holder">
                              <div class="pcomment main-comment">
                                 <div class="img-holder">
                                    <div class="profiletipholder">
                                       <span class="profile-tooltip">
                                       <img class="circle" src="images/demo-profile.jpg"/>
                                       </span>
                                       <span class="profiletooltip_content slidingpan-holder">
                                          <div class="profile-tip" style="display:none;">
                                             <div class="profile-tip-avatar">
                                                <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                                <div class="sliding-pan location-span">
                                                   <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                </div>
                                             </div>
                                             <div class="profile-tip-name">
                                                <a href="javascript:void(0)">Adel Hasanat</a>
                                             </div>
                                             <div class="profile-tip-info">
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                </div>
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                </div>
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                                </div>
                                             </div>
                                          </div>
                                       </span>
                                    </div>
                                 </div>
                                 <div class="desc-holder">
                                    <div class="normal-mode">
                                       <div class="desc">
                                          <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                          <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                       </div>
                                       <div class="comment-stuff">
                                          <div class="more-opt">
                                             <span class="likeholder">
                                             <span class="like-tooltip">
                                             <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                             <span class="icon">icon</span>
                                             </a>
                                             </span>
                                             </span>  
                                             <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><span>icon</span></a>
                                             <!-- Dropdown Trigger -->
                                             <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='drop_down47'>
                                             <i class="zmdi zmdi-more"></i>
                                             </a>
                                             <!-- Dropdown Structure -->
                                             <ul id='drop_down47' class='dropdown-content custom_dropdown'>
                                                <li>
                                                   <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                                </li>
                                                <li>
                                                   <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                                </li>
                                             </ul>
                                          </div>
                                          <div class="less-opt">
                                             <div class="timestamp">8h</div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="edit-mode">
                                       <div class="desc">
                                          <textarea class="editcomment-tt materialize-textarea" id="ec-13">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                          <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="clear"></div>
                              <div class="comment-reply-holder comment-addreply">
                                 <div class="addnew-comment valign-wrapper  comment-reply">
                                    <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                                    <div class="desc-holder">                          
                                       <textarea class="materialize-textarea">Write a reply...</textarea>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="pcomment-holder has-comments">
                              <div class="pcomment main-comment">
                                 <div class="img-holder">
                                    <div class="profiletipholder">
                                       <span class="profile-tooltip">
                                       <img class="circle" src="images/demo-profile.jpg"/>
                                       </span>
                                       <span class="profiletooltip_content slidingpan-holder">
                                          <div class="profile-tip" style="display:none;">
                                             <div class="profile-tip-avatar">
                                                <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                                <div class="sliding-pan location-span">
                                                   <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                </div>
                                             </div>
                                             <div class="profile-tip-name">
                                                <a href="javascript:void(0)">Adel Hasanat</a>
                                             </div>
                                             <div class="profile-tip-info">
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                </div>
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                </div>
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                                </div>
                                             </div>
                                          </div>
                                       </span>
                                    </div>
                                 </div>
                                 <div class="desc-holder">
                                    <div class="normal-mode">
                                       <div class="desc">
                                          <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                          <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                       </div>
                                       <div class="comment-stuff">
                                          <div class="more-opt">
                                             <span class="likeholder">
                                             <span class="like-tooltip">
                                             <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                             <span class="icon">icon</span>
                                             </a>
                                             </span>
                                             </span>  
                                             <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><span>icon</span></a>
                                             <!-- Dropdown Trigger -->
                                             <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='drop_down48'>
                                             <i class="zmdi zmdi-more"></i>
                                             </a>
                                             <!-- Dropdown Structure -->
                                             <ul id='drop_down48' class='dropdown-content custom_dropdown'>
                                                <li>
                                                   <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                                </li>
                                                <li>
                                                   <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                                </li>
                                             </ul>
                                          </div>
                                          <div class="less-opt">
                                             <div class="timestamp">8h</div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="edit-mode">
                                       <div class="desc">
                                          <textarea class="editcomment-tt materialize-textarea" id="ec-14">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                          <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="clear"></div>
                              <div class="comment-reply-holder">
                                 <div class="pcomment comment-reply">
                                    <div class="img-holder">
                                       <div class="profiletipholder">
                                          <span class="profile-tooltip">
                                          <img class="circle" src="images/demo-profile.jpg"/>
                                          </span>
                                          <span class="profiletooltip_content">
                                             <div class="profile-tip" style="display:none;">
                                                <div class="slidingpan-holder">
                                                   <div class="profile-tip-avatar">
                                                      <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                                      <div class="sliding-pan location-span">
                                                         <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                      </div>
                                                   </div>
                                                   <div class="profile-tip-name">
                                                      <a href="javascript:void(0)">Adel Hasanat</a>
                                                   </div>
                                                   <div class="profile-tip-info">
                                                      <div class="profiletip-icon">
                                                         <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                      </div>
                                                      <div class="profiletip-icon">
                                                         <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                      </div>
                                                      <div class="profiletip-icon">
                                                         <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </span>
                                       </div>
                                    </div>
                                    <div class="desc-holder">
                                       <div class="normal-mode">
                                          <div class="desc">
                                             <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                             <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                          </div>
                                          <div class="comment-stuff">
                                             <div class="more-opt">
                                                <span class="likeholder">
                                                <span class="like-tooltip">
                                                <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                                <span class="icon">icon</span>
                                                </a>
                                                </span>
                                                </span>
                                                <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><span>icon</span></a>
                                                <!-- Dropdown Trigger -->
                                                <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='drop_down49'>
                                                <i class="zmdi zmdi-more"></i>
                                                </a>
                                                <!-- Dropdown Structure -->
                                                <ul id='drop_down49' class='dropdown-content custom_dropdown'>
                                                   <li>
                                                      <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                                   </li>
                                                   <li>
                                                      <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                                   </li>
                                                </ul>
                                             </div>
                                             <div class="less-opt">
                                                <div class="timestamp">8h</div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="edit-mode">
                                          <div class="desc">
                                             <textarea class="editcomment-tt materialize-textarea" id="ec-15">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                             <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="pcomment comment-reply">
                                    <div class="img-holder">
                                       <div class="profiletipholder">
                                          <span class="profile-tooltip">
                                          <img class="circle" src="images/demo-profile.jpg"/>
                                          </span>
                                          <span class="profiletooltip_content">
                                             <div class="profile-tip" style="display:none;">
                                                <div class="slidingpan-holder">
                                                   <div class="profile-tip-avatar">
                                                      <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                                      <div class="sliding-pan location-span">
                                                         <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                      </div>
                                                   </div>
                                                   <div class="profile-tip-name">
                                                      <a href="javascript:void(0)">Adel Hasanat</a>
                                                   </div>
                                                   <div class="profile-tip-info">
                                                      <div class="profiletip-icon">
                                                         <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                      </div>
                                                      <div class="profiletip-icon">
                                                         <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                      </div>
                                                      <div class="profiletip-icon">
                                                         <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </span>
                                       </div>
                                    </div>
                                    <div class="desc-holder">
                                       <div class="normal-mode">
                                          <div class="desc">
                                             <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                             <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit...</p>
                                          </div>
                                          <div class="comment-stuff">
                                             <div class="more-opt">
                                                <span class="likeholder">
                                                <span class="like-tooltip">
                                                <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                                <span class="icon">icon</span>
                                                </a>
                                                </span>
                                                </span>
                                                <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><span>icon</span></a>
                                                <!-- Dropdown Trigger -->
                                                <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='drop_down50'>
                                                <i class="zmdi zmdi-more"></i>
                                                </a>
                                                <!-- Dropdown Structure -->
                                                <ul id='drop_down50' class='dropdown-content custom_dropdown'>
                                                   <li>
                                                      <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                                   </li>
                                                   <li>
                                                      <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                                   </li>
                                                </ul>
                                             </div>
                                             <div class="less-opt">
                                                <div class="timestamp">8h</div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="edit-mode">
                                          <div class="desc">
                                             <textarea class="editcomment-tt materialize-textarea" id="ec-16">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                             <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="comment-reply-holder comment-addreply">
                                 <div class="addnew-comment valign-wrapper  comment-reply">
                                    <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                                    <div class="desc-holder">                          
                                       <textarea class="materialize-textarea">Write a reply...</textarea>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="pcomment-holder">
                              <div class="pcomment main-comment">
                                 <div class="img-holder">
                                    <div class="profiletipholder">
                                       <span class="profile-tooltip">
                                       <img class="circle" src="images/demo-profile.jpg"/>
                                       </span>
                                       <span class="profiletooltip_content slidingpan-holder">
                                          <div class="profile-tip" style="display:none;">
                                             <div class="profile-tip-avatar">
                                                <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                                <div class="sliding-pan location-span">
                                                   <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                </div>
                                             </div>
                                             <div class="profile-tip-name">
                                                <a href="javascript:void(0)">Adel Hasanat</a>
                                             </div>
                                             <div class="profile-tip-info">
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                </div>
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                </div>
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                                </div>
                                             </div>
                                          </div>
                                       </span>
                                    </div>
                                 </div>
                                 <div class="desc-holder">
                                    <div class="normal-mode">
                                       <div class="desc">
                                          <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                          <p class="shorten">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.<a a="" href="javascript:void(0)" class="overlay" onclick="explandReadMore(this)" style="position: absolute;top: 0;bottom: -6px;left: 0;right: 0;"><span class="readlink">Read More</span></a></p>
                                       </div>
                                       <div class="comment-stuff">
                                          <div class="more-opt">
                                             <span class="likeholder">
                                             <span class="like-tooltip">
                                             <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                             <span class="icon">icon</span>
                                             </a>
                                             </span>
                                             </span>  
                                             <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><span>icon</span></a>
                                             <!-- Dropdown Trigger -->
                                             <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='drop_down51'>
                                             <i class="zmdi zmdi-more"></i>
                                             </a>
                                             <!-- Dropdown Structure -->
                                             <ul id='drop_down51' class='dropdown-content custom_dropdown'>
                                                <li>
                                                   <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                                </li>
                                                <li>
                                                   <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                                </li>
                                             </ul>
                                          </div>
                                          <div class="less-opt">
                                             <div class="timestamp">8h</div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="edit-mode">
                                       <div class="desc">
                                          <textarea class="editcomment-tt materialize-textarea" id="ec-17">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                          <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="clear"></div>
                              <div class="comment-reply-holder comment-addreply">
                                 <div class="addnew-comment valign-wrapper  comment-reply">
                                    <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                                    <div class="desc-holder">                          
                                       <textarea class="materialize-textarea">Write a reply...</textarea>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="addnew-comment valign-wrapper  ">
                        <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                        <div class="desc-holder">
                           <div class="sliding-middle-out anim-area underlined">
                              <textarea class="materialize-textarea" id="comment_txt_3">Write a reply...</textarea>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <!-- end advertise page -->
      </div>
      <div class="col s12 m12">
         <!-- advertise event -->
         <div class="post-holder travad-box event-travad">
            <div class="post-topbar">
               <div class="post-userinfo">
                  <div class="img-holder">
                     <div class="profiletipholder">
                        <span class="profile-tooltip">
                           <div class="travad-maintitle"><span><i class="mdi mdi-calendar"></i></span></div>
                        </span>
                        <span class="profiletooltip_content slidingpan-holder">
                           <div class="profile-tip" style="display:none;">
                              <div class="profile-tip-avatar">
                                 <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                 <div class="sliding-pan location-span">
                                    <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                 </div>
                              </div>
                              <div class="profile-tip-name">
                                 <a href="javascript:void(0)">Adel Hasanat</a>
                              </div>
                              <div class="profile-tip-info">
                                 <div class="profiletip-icon">
                                    <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                 </div>
                                 <div class="profiletip-icon">
                                    <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                 </div>
                                 <div class="profiletip-icon">
                                    <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                 </div>
                              </div>
                           </div>
                        </span>
                     </div>
                  </div>
                  <div class="desc-holder">
                     <div class="travad-maintitle">Opera Music Night</div>
                     <span class="timestamp">Sponsored Ad</span>
                  </div>
               </div>
               <div class="settings-icon">
			   <div class="dropdown dropdown-custom dropdown-xxsmall">
				  <a class="dropdown-button more_btn" href="javascript:void(0);" data-activates="setting_btn9" style="">					
                  <i class="zmdi zmdi-more"></i>
				  </a>
				  <ul id="setting_btn9" class="dropdown-content custom_dropdown" > <li>
                        <a href="javascript:void(0)">Hide ad</a>
                     </li>
                     <li>
                        <a href="javascript:void(0)">Save ad</a>
                     </li>
                     <li>
                        <a href="javascript:void(0)">Mute this seller ads</a>
                     </li>
                     <li>
                        <a href="javascript:void(0)">Report ad</a>
                     </li>
				  </ul>				 
				</div>
                  <!-- Dropdown Structure -->
               </div>
            </div>
            <div class="post-content">
               <div class="pdetail-holder">
                  <div class="shared-box shared-category">
                     <div class="post-holder">
                        <div class="post-content">
                           <div class="post-desc">
                              <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                           </div>
                           <div class="post-img-holder">
                              <div class="post-img one-img">
                                 <div class="pimg-holder"><img src="images/eventad.jpg"/></div>
                              </div>
                           </div>
                           <div class="share-summery">
                              <div class="datebox"><span class="month">Nov</span><span class="date">17</span></div>
                              <div class="travad-title">We are gonna party hard!</div>
                              <div class="travad-subtitle">Barcelona, Spain</div>
                              <div class="travad-info">45 people attending</div>
                              <a href="javascript:void(0)" class="btn btn-primary btn-sm adbtn waves-effect waves-light">Going</a>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="clear"></div>
            <div class="post-data">
               <div class="post-actions">
                  <span class="likeholder">
                  <span class="like-tooltip">
                  <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                  <i class="zmdi zmdi-thumb-up"></i>
                  </a>
                  </span>
                  </span>
                  <a href="javascript:void(0)" class="sharepostmodalAction pa-share" title="Share"><i class="zmdi zmdi-mail-reply zmdi-hc-flip-horizontal"></i></a>
                  <span class="right">
                     <a href="javascript:void(0)" class="pa-comment" title="Comment" data-id="1"><i class="zmdi zmdi-comment"></i></a><span class="comment-lcount">4</span>
                  </span>
               </div>
               <div class="comments-section panel">
                  <div class="post-more">
                     <a href="javascript:void(0)" class="view-morec">View more comments <i class="zmdi zmdi-chevron-right"></i></a>
                     <span class="total-comments">3 of 7</span>
                  </div>
                  <div class="post-comments">
                     <div class="pcomments">
                        <div class="pcomment-earlier">
                           <div class="pcomment-holder">
                              <div class="pcomment main-comment">
                                 <div class="img-holder">
                                    <div class="profiletipholder">
                                       <span class="profile-tooltip">
                                       <img class="circle" src="images/demo-profile.jpg"/>
                                       </span>
                                       <span class="profiletooltip_content slidingpan-holder">
                                          <div class="profile-tip" style="display:none;">
                                             <div class="profile-tip-avatar">
                                                <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                                <div class="sliding-pan location-span">
                                                   <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                </div>
                                             </div>
                                             <div class="profile-tip-name">
                                                <a href="javascript:void(0)">Adel Hasanat</a>
                                             </div>
                                             <div class="profile-tip-info">
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                </div>
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                </div>
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                                </div>
                                             </div>
                                          </div>
                                       </span>
                                    </div>
                                 </div>
                                 <div class="desc-holder">
                                    <div class="normal-mode">
                                       <div class="desc">
                                          <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                          <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                       </div>
                                       <div class="comment-stuff">
                                          <div class="more-opt">
                                             <span class="likeholder">
                                             <span class="like-tooltip">
                                             <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                             <span class="icon">icon</span>
                                             </a>
                                             </span>
                                             </span>  
                                             <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><span>icon</span></a>
                                             <!-- Dropdown Trigger -->
                                             <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='drop_down52'>
                                             <i class="zmdi zmdi-more"></i>
                                             </a>
                                             <!-- Dropdown Structure -->
                                             <ul id='drop_down52' class='dropdown-content custom_dropdown'>
                                                <li>
                                                   <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                                </li>
                                                <li>
                                                   <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                                </li>
                                             </ul>
                                          </div>
                                          <div class="less-opt">
                                             <div class="timestamp">8h</div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="edit-mode">
                                       <div class="desc">
                                          <textarea class="editcomment-tt materialize-textarea" id="ec-12">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                          <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="clear"></div>
                              <div class="comment-reply-holder comment-addreply">
                                 <div class="addnew-comment valign-wrapper  comment-reply">
                                    <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                                    <div class="desc-holder">                          
                                       <textarea class="materialize-textarea">Write a reply...</textarea>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="pcomment-holder">
                              <div class="pcomment main-comment">
                                 <div class="img-holder">
                                    <div class="profiletipholder">
                                       <span class="profile-tooltip">
                                       <img class="circle" src="images/demo-profile.jpg"/>
                                       </span>
                                       <span class="profiletooltip_content slidingpan-holder">
                                          <div class="profile-tip" style="display:none;">
                                             <div class="profile-tip-avatar">
                                                <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                                <div class="sliding-pan location-span">
                                                   <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                </div>
                                             </div>
                                             <div class="profile-tip-name">
                                                <a href="javascript:void(0)">Adel Hasanat</a>
                                             </div>
                                             <div class="profile-tip-info">
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                </div>
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                </div>
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                                </div>
                                             </div>
                                          </div>
                                       </span>
                                    </div>
                                 </div>
                                 <div class="desc-holder">
                                    <div class="normal-mode">
                                       <div class="desc">
                                          <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                          <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                       </div>
                                       <div class="comment-stuff">
                                          <div class="more-opt">
                                             <span class="likeholder">
                                             <span class="like-tooltip">
                                             <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                             <span class="icon">icon</span>
                                             </a>
                                             </span>
                                             </span>  
                                             <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><span>icon</span></a>
                                             <!-- Dropdown Trigger -->
                                             <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='drop_down53'>
                                             <i class="zmdi zmdi-more"></i>
                                             </a>
                                             <!-- Dropdown Structure -->
                                             <ul id='drop_down53' class='dropdown-content custom_dropdown'>
                                                <li>
                                                   <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                                </li>
                                                <li>
                                                   <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                                </li>
                                             </ul>
                                          </div>
                                          <div class="less-opt">
                                             <div class="timestamp">8h</div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="edit-mode">
                                       <div class="desc">
                                          <textarea class="editcomment-tt materialize-textarea" id="ec-13">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                          <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="clear"></div>
                              <div class="comment-reply-holder comment-addreply">
                                 <div class="addnew-comment valign-wrapper  comment-reply">
                                    <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                                    <div class="desc-holder">                          
                                       <textarea class="materialize-textarea">Write a reply...</textarea>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="pcomment-holder has-comments">
                              <div class="pcomment main-comment">
                                 <div class="img-holder">
                                    <div class="profiletipholder">
                                       <span class="profile-tooltip">
                                       <img class="circle" src="images/demo-profile.jpg"/>
                                       </span>
                                       <span class="profiletooltip_content slidingpan-holder">
                                          <div class="profile-tip" style="display:none;">
                                             <div class="profile-tip-avatar">
                                                <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                                <div class="sliding-pan location-span">
                                                   <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                </div>
                                             </div>
                                             <div class="profile-tip-name">
                                                <a href="javascript:void(0)">Adel Hasanat</a>
                                             </div>
                                             <div class="profile-tip-info">
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                </div>
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                </div>
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                                </div>
                                             </div>
                                          </div>
                                       </span>
                                    </div>
                                 </div>
                                 <div class="desc-holder">
                                    <div class="normal-mode">
                                       <div class="desc">
                                          <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                          <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                       </div>
                                       <div class="comment-stuff">
                                          <div class="more-opt">
                                             <span class="likeholder">
                                             <span class="like-tooltip">
                                             <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                             <span class="icon">icon</span>
                                             </a>
                                             </span>
                                             </span>  
                                             <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><span>icon</span></a>
                                             <!-- Dropdown Trigger -->
                                             <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='drop_down54'>
                                             <i class="zmdi zmdi-more"></i>
                                             </a>
                                             <!-- Dropdown Structure -->
                                             <ul id='drop_down54' class='dropdown-content custom_dropdown'>
                                                <li>
                                                   <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                                </li>
                                                <li>
                                                   <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                                </li>
                                             </ul>
                                          </div>
                                          <div class="less-opt">
                                             <div class="timestamp">8h</div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="edit-mode">
                                       <div class="desc">
                                          <textarea class="editcomment-tt materialize-textarea" id="ec-14">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                          <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="clear"></div>
                              <div class="comment-reply-holder">
                                 <div class="pcomment comment-reply">
                                    <div class="img-holder">
                                       <div class="profiletipholder">
                                          <span class="profile-tooltip">
                                          <img class="circle" src="images/demo-profile.jpg"/>
                                          </span>
                                          <span class="profiletooltip_content">
                                             <div class="profile-tip" style="display:none;">
                                                <div class="slidingpan-holder">
                                                   <div class="profile-tip-avatar">
                                                      <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                                      <div class="sliding-pan location-span">
                                                         <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                      </div>
                                                   </div>
                                                   <div class="profile-tip-name">
                                                      <a href="javascript:void(0)">Adel Hasanat</a>
                                                   </div>
                                                   <div class="profile-tip-info">
                                                      <div class="profiletip-icon">
                                                         <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                      </div>
                                                      <div class="profiletip-icon">
                                                         <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                      </div>
                                                      <div class="profiletip-icon">
                                                         <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </span>
                                       </div>
                                    </div>
                                    <div class="desc-holder">
                                       <div class="normal-mode">
                                          <div class="desc">
                                             <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                             <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                          </div>
                                          <div class="comment-stuff">
                                             <div class="more-opt">
                                                <span class="likeholder">
                                                <span class="like-tooltip">
                                                <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                                <span class="icon">icon</span>
                                                </a>
                                                </span>
                                                </span>
                                                <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><span>icon</span></a>
                                                <!-- Dropdown Trigger -->
                                                <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='drop_down55'>
                                                <i class="zmdi zmdi-more"></i>
                                                </a>
                                                <!-- Dropdown Structure -->
                                                <ul id='drop_down55' class='dropdown-content custom_dropdown'>
                                                   <li>
                                                      <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                                   </li>
                                                   <li>
                                                      <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                                   </li>
                                                </ul>
                                             </div>
                                             <div class="less-opt">
                                                <div class="timestamp">8h</div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="edit-mode">
                                          <div class="desc">
                                             <textarea class="editcomment-tt materialize-textarea" id="ec-15">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                             <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="pcomment comment-reply">
                                    <div class="img-holder">
                                       <div class="profiletipholder">
                                          <span class="profile-tooltip">
                                          <img class="circle" src="images/demo-profile.jpg"/>
                                          </span>
                                          <span class="profiletooltip_content">
                                             <div class="profile-tip" style="display:none;">
                                                <div class="slidingpan-holder">
                                                   <div class="profile-tip-avatar">
                                                      <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                                      <div class="sliding-pan location-span">
                                                         <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                      </div>
                                                   </div>
                                                   <div class="profile-tip-name">
                                                      <a href="javascript:void(0)">Adel Hasanat</a>
                                                   </div>
                                                   <div class="profile-tip-info">
                                                      <div class="profiletip-icon">
                                                         <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                      </div>
                                                      <div class="profiletip-icon">
                                                         <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                      </div>
                                                      <div class="profiletip-icon">
                                                         <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </span>
                                       </div>
                                    </div>
                                    <div class="desc-holder">
                                       <div class="normal-mode">
                                          <div class="desc">
                                             <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                             <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit...</p>
                                          </div>
                                          <div class="comment-stuff">
                                             <div class="more-opt">
                                                <span class="likeholder">
                                                <span class="like-tooltip">
                                                <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                                <span class="icon">icon</span>
                                                </a>
                                                </span>
                                                </span>  
                                                <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><span>icon</span></a>
                                                <!-- Dropdown Trigger -->
                                                <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='drop_down56'>
                                                <i class="zmdi zmdi-more"></i>
                                                </a>
                                                <!-- Dropdown Structure -->
                                                <ul id='drop_down56' class='dropdown-content custom_dropdown'>
                                                   <li>
                                                      <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                                   </li>
                                                   <li>
                                                      <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                                   </li>
                                                </ul>
                                             </div>
                                             <div class="less-opt">
                                                <div class="timestamp">8h</div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="edit-mode">
                                          <div class="desc">
                                             <textarea class="editcomment-tt materialize-textarea" id="ec-16">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                             <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="comment-reply-holder comment-addreply">
                                 <div class="addnew-comment valign-wrapper  comment-reply">
                                    <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                                    <div class="desc-holder">                          
                                       <textarea class="materialize-textarea">Write a reply...</textarea>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="pcomment-holder">
                              <div class="pcomment main-comment">
                                 <div class="img-holder">
                                    <div class="profiletipholder">
                                       <span class="profile-tooltip">
                                       <img class="circle" src="images/demo-profile.jpg"/>
                                       </span>
                                       <span class="profiletooltip_content slidingpan-holder">
                                          <div class="profile-tip" style="display:none;">
                                             <div class="profile-tip-avatar">
                                                <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                                <div class="sliding-pan location-span">
                                                   <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                </div>
                                             </div>
                                             <div class="profile-tip-name">
                                                <a href="javascript:void(0)">Adel Hasanat</a>
                                             </div>
                                             <div class="profile-tip-info">
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                </div>
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                </div>
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                                </div>
                                             </div>
                                          </div>
                                       </span>
                                    </div>
                                 </div>
                                 <div class="desc-holder">
                                    <div class="normal-mode">
                                       <div class="desc">
                                          <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                          <p class="shorten">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.<a a="" href="javascript:void(0)" class="overlay" onclick="explandReadMore(this)" style="position: absolute;top: 0;bottom: -6px;left: 0;right: 0;"><span class="readlink">Read More</span></a></p>
                                       </div>
                                       <div class="comment-stuff">
                                          <div class="more-opt">
                                             <span class="likeholder">
                                             <span class="like-tooltip">
                                             <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                             <span class="icon">icon</span>
                                             </a>
                                             </span>
                                             </span>  
                                             <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><span>icon</span></a>
                                             <!-- Dropdown Trigger -->
                                             <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='drop_down57'>
                                             <i class="zmdi zmdi-more"></i>
                                             </a>
                                             <!-- Dropdown Structure -->
                                             <ul id='drop_down57' class='dropdown-content custom_dropdown'>
                                                <li>
                                                   <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                                </li>
                                                <li>
                                                   <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                                </li>
                                             </ul>
                                          </div>
                                          <div class="less-opt">
                                             <div class="timestamp">8h</div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="edit-mode">
                                       <div class="desc">
                                          <textarea class="editcomment-tt materialize-textarea" id="ec-17">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                          <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="clear"></div>
                              <div class="comment-reply-holder comment-addreply">
                                 <div class="addnew-comment valign-wrapper  comment-reply">
                                    <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                                    <div class="desc-holder">                          
                                       <textarea class="materialize-textarea">Write a reply...</textarea>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="addnew-comment valign-wrapper  ">
                        <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                        <div class="desc-holder">                          
                           <textarea class="materialize-textarea" id="comment_txt_3">Write a reply...</textarea>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <!-- end advertise event -->
      </div>
      <div class="col s12 m12">
         <!-- advertise group -->
         <div class="post-holder travad-box group-travad">
            <div class="post-topbar">
               <div class="post-userinfo">
                  <div class="img-holder">
                     <div class="profiletipholder">
                        <span class="profile-tooltip">
                           <div class="travad-maintitle"><span><i class=”mdi mdi-account-group”></i></i></span></div>
                        </span>
                        <span class="profiletooltip_content slidingpan-holder">
                           <div class="profile-tip" style="display:none;">
                              <div class="profile-tip-avatar">
                                 <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                 <div class="sliding-pan location-span">
                                    <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                 </div>
                              </div>
                              <div class="profile-tip-name">
                                 <a href="javascript:void(0)">Adel Hasanat</a>
                              </div>
                              <div class="profile-tip-info">
                                 <div class="profiletip-icon">
                                    <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                 </div>
                                 <div class="profiletip-icon">
                                    <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                 </div>
                                 <div class="profiletip-icon">
                                    <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                 </div>
                              </div>
                           </div>
                        </span>
                     </div>
                  </div>
                  <div class="desc-holder">
                     <div class="travad-maintitle">Heal Well</div>
                     <span class="timestamp">Sponsored Ad</span>
                  </div>
               </div>
               <div class="settings-icon">
			   <div class="dropdown dropdown-custom dropdown-xxsmall">
				  <a class="dropdown-button more_btn" href="javascript:void(0);" data-activates="setting_btn10" style="">					
                  <i class="zmdi zmdi-more"></i>
				  </a>
				  <ul id="setting_btn10" class="dropdown-content custom_dropdown" > <li>
                        <a href="javascript:void(0)">Hide ad</a>
                     </li>
                     <li>
                        <a href="javascript:void(0)">Save ad</a>
                     </li>
                     <li>
                        <a href="javascript:void(0)">Mute this seller ads</a>
                     </li>
                     <li>
                        <a href="javascript:void(0)">Report ad</a>
                     </li>
				  </ul>				 
				</div>
                  <!-- Dropdown Structure -->
               </div>
            </div>
            <div class="post-content">
               <div class="pdetail-holder">
                  <div class="shared-box shared-category">
                     <div class="post-holder">
                        <div class="post-content">
                           <div class="post-desc">
                              <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                           </div>
                           <div class="post-img-holder">
                              <div class="post-img one-img">
                                 <div class="pimg-holder"><img src="images/groupad.jpg"/></div>
                              </div>
                           </div>
                           <div class="share-summery">
                              <div class="travad-title">Latest research in medicines</div>
                              <div class="travad-subtitle">Keep yourself posted for the researches done recently on a wide range of medicine.</div>
                              <div class="travad-info">67 memebers</div>
                              <a href="javascript:void(0)" class="btn btn-primary btn-sm adbtn waves-effect waves-light">Join</a>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="clear"></div>
            <div class="post-data">
               <div class="post-actions">
                  <span class="likeholder">
                  <span class="like-tooltip">
                  <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                  <i class="zmdi zmdi-thumb-up"></i>
                  </a>
                  </span>
                  </span>
                  <a href="javascript:void(0)" class="sharepostmodalAction pa-share" title="Share"><i class="zmdi zmdi-mail-reply zmdi-hc-flip-horizontal"></i></a>
                  <span class="right">
                     <a href="javascript:void(0)" class="pa-comment" title="Comment" data-id="1"><i class="zmdi zmdi-comment"></i></a><span class="comment-lcount">4</span>
                  </span>
               </div>
               <div class="comments-section panel">
                  <div class="post-more">
                     <a href="javascript:void(0)" class="view-morec">View more comments <i class="zmdi zmdi-chevron-right"></i></a>
                     <span class="total-comments">3 of 7</span>
                  </div>
                  <div class="post-comments">
                     <div class="pcomments">
                        <div class="pcomment-earlier">
                           <div class="pcomment-holder">
                              <div class="pcomment main-comment">
                                 <div class="img-holder">
                                    <div class="profiletipholder">
                                       <span class="profile-tooltip">
                                       <img class="circle" src="images/demo-profile.jpg"/>
                                       </span>
                                       <span class="profiletooltip_content slidingpan-holder">
                                          <div class="profile-tip" style="display:none;">
                                             <div class="profile-tip-avatar">
                                                <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                                <div class="sliding-pan location-span">
                                                   <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                </div>
                                             </div>
                                             <div class="profile-tip-name">
                                                <a href="javascript:void(0)">Adel Hasanat</a>
                                             </div>
                                             <div class="profile-tip-info">
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                </div>
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                </div>
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                                </div>
                                             </div>
                                          </div>
                                       </span>
                                    </div>
                                 </div>
                                 <div class="desc-holder">
                                    <div class="normal-mode">
                                       <div class="desc">
                                          <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                          <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                       </div>
                                       <div class="comment-stuff">
                                          <div class="more-opt">
                                             <span class="likeholder">
                                             <span class="like-tooltip">
                                             <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                             <span class="icon">icon</span>
                                             </a>
                                             </span>
                                             </span>  
                                             <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><span>icon</span></a>
                                             <!-- Dropdown Trigger -->
                                             <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='drop_down58'>
                                             <i class="zmdi zmdi-more"></i>
                                             </a>
                                             <!-- Dropdown Structure -->
                                             <ul id='drop_down58' class='dropdown-content custom_dropdown'>
                                                <li>
                                                   <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                                </li>
                                                <li>
                                                   <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                                </li>
                                             </ul>
                                          </div>
                                          <div class="less-opt">
                                             <div class="timestamp">8h</div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="edit-mode">
                                       <div class="desc">
                                          <textarea class="editcomment-tt materialize-textarea" id="ec-12">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                          <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="clear"></div>
                              <div class="comment-reply-holder comment-addreply">
                                 <div class="addnew-comment valign-wrapper  comment-reply">
                                    <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                                    <div class="desc-holder">                          
                                       <textarea class="materialize-textarea">Write a reply...</textarea>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="pcomment-holder">
                              <div class="pcomment main-comment">
                                 <div class="img-holder">
                                    <div class="profiletipholder">
                                       <span class="profile-tooltip">
                                       <img class="circle" src="images/demo-profile.jpg"/>
                                       </span>
                                       <span class="profiletooltip_content slidingpan-holder">
                                          <div class="profile-tip" style="display:none;">
                                             <div class="profile-tip-avatar">
                                                <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                                <div class="sliding-pan location-span">
                                                   <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                </div>
                                             </div>
                                             <div class="profile-tip-name">
                                                <a href="javascript:void(0)">Adel Hasanat</a>
                                             </div>
                                             <div class="profile-tip-info">
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                </div>
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                </div>
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                                </div>
                                             </div>
                                          </div>
                                       </span>
                                    </div>
                                 </div>
                                 <div class="desc-holder">
                                    <div class="normal-mode">
                                       <div class="desc">
                                          <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                          <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                       </div>
                                       <div class="comment-stuff">
                                          <div class="more-opt">
                                             <span class="likeholder">
                                             <span class="like-tooltip">
                                             <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                             <span class="icon">icon</span>
                                             </a>
                                             </span>
                                             </span>  
                                             <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><span>icon</span></a>
                                             <!-- Dropdown Trigger -->
                                             <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='drop_down59'>
                                             <i class="zmdi zmdi-more"></i>
                                             </a>
                                             <!-- Dropdown Structure -->
                                             <ul id='drop_down59' class='dropdown-content custom_dropdown'>
                                                <li>
                                                   <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                                </li>
                                                <li>
                                                   <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                                </li>
                                             </ul>
                                          </div>
                                          <div class="less-opt">
                                             <div class="timestamp">8h</div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="edit-mode">
                                       <div class="desc">
                                          <textarea class="editcomment-tt materialize-textarea" id="ec-13">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                          <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="clear"></div>
                              <div class="comment-reply-holder comment-addreply">
                                 <div class="addnew-comment valign-wrapper  comment-reply">
                                    <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                                    <div class="desc-holder">                          
                                       <textarea class="materialize-textarea">Write a reply...</textarea>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="pcomment-holder has-comments">
                              <div class="pcomment main-comment">
                                 <div class="img-holder">
                                    <div class="profiletipholder">
                                       <span class="profile-tooltip">
                                       <img class="circle" src="images/demo-profile.jpg"/>
                                       </span>
                                       <span class="profiletooltip_content slidingpan-holder">
                                          <div class="profile-tip" style="display:none;">
                                             <div class="profile-tip-avatar">
                                                <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                                <div class="sliding-pan location-span">
                                                   <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                </div>
                                             </div>
                                             <div class="profile-tip-name">
                                                <a href="javascript:void(0)">Adel Hasanat</a>
                                             </div>
                                             <div class="profile-tip-info">
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                </div>
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                </div>
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                                </div>
                                             </div>
                                          </div>
                                       </span>
                                    </div>
                                 </div>
                                 <div class="desc-holder">
                                    <div class="normal-mode">
                                       <div class="desc">
                                          <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                          <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                       </div>
                                       <div class="comment-stuff">
                                          <div class="more-opt">
                                             <span class="likeholder">
                                             <span class="like-tooltip">
                                             <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                             <span class="icon">icon</span>
                                             </a>
                                             </span>
                                             </span>  
                                             <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><span>icon</span></a>
                                             <!-- Dropdown Trigger -->
                                             <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='drop_down60'>
                                             <i class="zmdi zmdi-more"></i>
                                             </a>
                                             <!-- Dropdown Structure -->
                                             <ul id='drop_down60' class='dropdown-content custom_dropdown'>
                                                <li>
                                                   <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                                </li>
                                                <li>
                                                   <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                                </li>
                                             </ul>
                                          </div>
                                          <div class="less-opt">
                                             <div class="timestamp">8h</div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="edit-mode">
                                       <div class="desc">
                                          <textarea class="editcomment-tt materialize-textarea" id="ec-14">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                          <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="clear"></div>
                              <div class="comment-reply-holder">
                                 <div class="pcomment comment-reply">
                                    <div class="img-holder">
                                       <div class="profiletipholder">
                                          <span class="profile-tooltip">
                                          <img class="circle" src="images/demo-profile.jpg"/>
                                          </span>
                                          <span class="profiletooltip_content">
                                             <div class="profile-tip" style="display:none;">
                                                <div class="slidingpan-holder">
                                                   <div class="profile-tip-avatar">
                                                      <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                                      <div class="sliding-pan location-span">
                                                         <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                      </div>
                                                   </div>
                                                   <div class="profile-tip-name">
                                                      <a href="javascript:void(0)">Adel Hasanat</a>
                                                   </div>
                                                   <div class="profile-tip-info">
                                                      <div class="profiletip-icon">
                                                         <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                      </div>
                                                      <div class="profiletip-icon">
                                                         <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                      </div>
                                                      <div class="profiletip-icon">
                                                         <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </span>
                                       </div>
                                    </div>
                                    <div class="desc-holder">
                                       <div class="normal-mode">
                                          <div class="desc">
                                             <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                             <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                          </div>
                                          <div class="comment-stuff">
                                             <div class="more-opt">
                                                <span class="likeholder">
                                                <span class="like-tooltip">
                                                <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                                <span class="icon">icon</span>
                                                </a>
                                                </span>
                                                </span>
                                                <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><span>icon</span></a>
                                                <!-- Dropdown Trigger -->
                                                <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='drop_down61'>
                                                <i class="zmdi zmdi-more"></i>
                                                </a>
                                                <!-- Dropdown Structure -->
                                                <ul id='drop_down61' class='dropdown-content custom_dropdown'>
                                                   <li>
                                                      <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                                   </li>
                                                   <li>
                                                      <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                                   </li>
                                                </ul>
                                             </div>
                                             <div class="less-opt">
                                                <div class="timestamp">8h</div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="edit-mode">
                                          <div class="desc">
                                             <textarea class="editcomment-tt materialize-textarea" id="ec-15">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                             <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="pcomment comment-reply">
                                    <div class="img-holder">
                                       <div class="profiletipholder">
                                          <span class="profile-tooltip">
                                          <img class="circle" src="images/demo-profile.jpg"/>
                                          </span>
                                          <span class="profiletooltip_content">
                                             <div class="profile-tip" style="display:none;">
                                                <div class="slidingpan-holder">
                                                   <div class="profile-tip-avatar">
                                                      <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                                      <div class="sliding-pan location-span">
                                                         <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                      </div>
                                                   </div>
                                                   <div class="profile-tip-name">
                                                      <a href="javascript:void(0)">Adel Hasanat</a>
                                                   </div>
                                                   <div class="profile-tip-info">
                                                      <div class="profiletip-icon">
                                                         <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                      </div>
                                                      <div class="profiletip-icon">
                                                         <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                      </div>
                                                      <div class="profiletip-icon">
                                                         <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </span>
                                       </div>
                                    </div>
                                    <div class="desc-holder">
                                       <div class="normal-mode">
                                          <div class="desc">
                                             <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                             <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit...</p>
                                          </div>
                                          <div class="comment-stuff">
                                             <div class="more-opt">
                                                <span class="likeholder">
                                                <span class="like-tooltip">
                                                <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                                <span class="icon">icon</span>
                                                </a>
                                                </span>
                                                </span>
                                                <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><span>icon</span></a>
                                                <!-- Dropdown Trigger -->
                                                <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='drop_down62'>
                                                <i class="zmdi zmdi-more"></i>
                                                </a>
                                                <!-- Dropdown Structure -->
                                                <ul id='drop_down62' class='dropdown-content custom_dropdown'>
                                                   <li>
                                                      <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                                   </li>
                                                   <li>
                                                      <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                                   </li>
                                                </ul>
                                             </div>
                                             <div class="less-opt">
                                                <div class="timestamp">8h</div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="edit-mode">
                                          <div class="desc">
                                             <textarea class="editcomment-tt materialize-textarea" id="ec-16">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                             <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="comment-reply-holder comment-addreply">
                                 <div class="addnew-comment valign-wrapper  comment-reply">
                                    <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                                    <div class="desc-holder">                          
                                       <textarea class="materialize-textarea">Write a reply...</textarea>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="pcomment-holder">
                              <div class="pcomment main-comment">
                                 <div class="img-holder">
                                    <div class="profiletipholder">
                                       <span class="profile-tooltip">
                                       <img class="circle" src="images/demo-profile.jpg"/>
                                       </span>
                                       <span class="profiletooltip_content slidingpan-holder">
                                          <div class="profile-tip" style="display:none;">
                                             <div class="profile-tip-avatar">
                                                <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                                <div class="sliding-pan location-span">
                                                   <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                </div>
                                             </div>
                                             <div class="profile-tip-name">
                                                <a href="javascript:void(0)">Adel Hasanat</a>
                                             </div>
                                             <div class="profile-tip-info">
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                </div>
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                </div>
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                                </div>
                                             </div>
                                          </div>
                                       </span>
                                    </div>
                                 </div>
                                 <div class="desc-holder">
                                    <div class="normal-mode">
                                       <div class="desc">
                                          <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                          <p class="shorten">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.<a a="" href="javascript:void(0)" class="overlay" onclick="explandReadMore(this)" style="position: absolute;top: 0;bottom: -6px;left: 0;right: 0;"><span class="readlink">Read More</span></a></p>
                                       </div>
                                       <div class="comment-stuff">
                                          <div class="more-opt">
                                             <span class="likeholder">
                                             <span class="like-tooltip">
                                             <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                             <span class="icon">icon</span>
                                             </a>
                                             </span>
                                             </span>  
                                             <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><span>icon</span></a>
                                             <!-- Dropdown Trigger -->
                                             <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='drop_down63'>
                                             <i class="zmdi zmdi-more"></i>
                                             </a>
                                             <!-- Dropdown Structure -->
                                             <ul id='drop_down63' class='dropdown-content custom_dropdown'>
                                                <li>
                                                   <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                                </li>
                                                <li>
                                                   <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                                </li>
                                             </ul>
                                          </div>
                                          <div class="less-opt">
                                             <div class="timestamp">8h</div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="edit-mode">
                                       <div class="desc">
                                          <textarea class="editcomment-tt materialize-textarea" id="ec-17">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                          <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="clear"></div>
                              <div class="comment-reply-holder comment-addreply">
                                 <div class="addnew-comment valign-wrapper  comment-reply">
                                    <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                                    <div class="desc-holder">                          
                                       <textarea class="materialize-textarea">Write a reply...</textarea>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="addnew-comment valign-wrapper  ">
                        <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                        <div class="desc-holder">                          
                           <textarea class="materialize-textarea" id="comment_txt_3">Write a reply...</textarea>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <!-- end advertise group -->
      </div>
      <div class="col s12 m12">
         <!-- advertise collection -->
         <div class="post-holder travad-box collection-travad">
            <div class="post-topbar">
               <div class="post-userinfo">
                  <div class="img-holder">
                     <div class="profiletipholder">
                        <span class="profile-tooltip">
                           <div class="travad-maintitle"><span><i class="mdi mdi-folder-open"></i></span></div>
                        </span>
                        <span class="profiletooltip_content slidingpan-holder">
                           <div class="profile-tip" style="display:none;">
                              <div class="profile-tip-avatar">
                                 <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                 <div class="sliding-pan location-span">
                                    <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                 </div>
                              </div>
                              <div class="profile-tip-name">
                                 <a href="javascript:void(0)">Adel Hasanat</a>
                              </div>
                              <div class="profile-tip-info">
                                 <div class="profiletip-icon">
                                    <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                 </div>
                                 <div class="profiletip-icon">
                                    <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                 </div>
                                 <div class="profiletip-icon">
                                    <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                 </div>
                              </div>
                           </div>
                        </span>
                     </div>
                  </div>
                  <div class="desc-holder">
                     <div class="travad-maintitle">Flower Velly</div>
                     <span class="timestamp">Sponsored Ad</span>
                  </div>
               </div> 
               <div class="settings-icon">
			   <div class="dropdown dropdown-custom dropdown-xxsmall">
				  <a class="dropdown-button more_btn" href="javascript:void(0);" data-activates="setting_btn11" style="">					
                  <i class="zmdi zmdi-more"></i>
				  </a>
				  <ul id="setting_btn11" class="dropdown-content custom_dropdown" > <li>
                        <a href="javascript:void(0)">Hide ad</a>
                     </li>
                     <li>
                        <a href="javascript:void(0)">Save ad</a>
                     </li>
                     <li>
                        <a href="javascript:void(0)">Mute this seller ads</a>
                     </li>
                     <li>
                        <a href="javascript:void(0)">Report ad</a>
                     </li>
				  </ul>				 
				</div>
                  <!-- Dropdown Structure -->
               </div>
            </div>
            <div class="post-content">
               <div class="pdetail-holder">
                  <div class="shared-box shared-category">
                     <div class="post-holder">
                        <div class="post-content">
                           <div class="post-desc">
                              <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                           </div>
                           <div class="post-img-holder">
                              <div class="post-img one-img">
                                 <div class="pimg-holder"><img src="images/collectionad.jpg"/></div>
                              </div>
                           </div>
                           <div class="share-summery">
                              <div class="travad-title">World's most beautiful flower</div>
                              <div class="travad-subtitle">Checkout the images of most beautiful flower of the world here.</div>
                              <div class="travad-info">100 Followers</div>
                              <a href="javascript:void(0)" class="btn btn-primary btn-sm adbtn waves-effect waves-light">Follow</a>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="clear"></div>
            <div class="post-data">
               <div class="post-actions">
                  <span class="likeholder">
                  <span class="like-tooltip">
                  <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                  <i class="zmdi zmdi-thumb-up"></i>
                  </a>
                  </span>
                  </span>
                  <a href="javascript:void(0)" class="sharepostmodalAction pa-share" title="Share"><i class="zmdi zmdi-mail-reply zmdi-hc-flip-horizontal"></i></a>
                  <span class="right">
                     <a href="javascript:void(0)" class="pa-comment" title="Comment" data-id="1"><i class="zmdi zmdi-comment"></i></a><span class="comment-lcount">4</span>
                  </span>
               </div>
               <div class="comments-section panel">
                  <div class="post-more">
                     <a href="javascript:void(0)" class="view-morec">View more comments <i class="zmdi zmdi-chevron-right"></i></a>
                     <span class="total-comments">3 of 7</span>
                  </div>
                  <div class="post-comments">
                     <div class="pcomments">
                        <div class="pcomment-earlier">
                           <div class="pcomment-holder">
                              <div class="pcomment main-comment">
                                 <div class="img-holder">
                                    <div class="profiletipholder">
                                       <span class="profile-tooltip">
                                       <img class="circle" src="images/demo-profile.jpg"/>
                                       </span>
                                       <span class="profiletooltip_content slidingpan-holder">
                                          <div class="profile-tip" style="display:none;">
                                             <div class="profile-tip-avatar">
                                                <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                                <div class="sliding-pan location-span">
                                                   <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                </div>
                                             </div>
                                             <div class="profile-tip-name">
                                                <a href="javascript:void(0)">Adel Hasanat</a>
                                             </div>
                                             <div class="profile-tip-info">
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                </div>
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                </div>
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                                </div>
                                             </div>
                                          </div>
                                       </span>
                                    </div>
                                 </div>
                                 <div class="desc-holder">
                                    <div class="normal-mode">
                                       <div class="desc">
                                          <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                          <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                       </div>
                                       <div class="comment-stuff">
                                          <div class="more-opt">
                                             <span class="likeholder">
                                             <span class="like-tooltip">
                                             <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                             <span class="icon">icon</span>
                                             </a>
                                             </span>
                                             </span>  
                                             <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><span>icon</span></a>
                                             <!-- Dropdown Trigger -->
                                             <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='drop_down64'>
                                             <i class="zmdi zmdi-more"></i>
                                             </a>
                                             <!-- Dropdown Structure -->
                                             <ul id='drop_down64' class='dropdown-content custom_dropdown'>
                                                <li>
                                                   <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                                </li>
                                                <li>
                                                   <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                                </li>
                                             </ul>
                                          </div>
                                          <div class="less-opt">
                                             <div class="timestamp">8h</div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="edit-mode">
                                       <div class="desc">
                                          <textarea class="editcomment-tt materialize-textarea" id="ec-12">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                          <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="clear"></div>
                              <div class="comment-reply-holder comment-addreply">
                                 <div class="addnew-comment valign-wrapper  comment-reply">
                                    <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                                    <div class="desc-holder">                          
                                       <textarea class="materialize-textarea">Write a reply...</textarea>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="pcomment-holder">
                              <div class="pcomment main-comment">
                                 <div class="img-holder">
                                    <div class="profiletipholder">
                                       <span class="profile-tooltip">
                                       <img class="circle" src="images/demo-profile.jpg"/>
                                       </span>
                                       <span class="profiletooltip_content slidingpan-holder">
                                          <div class="profile-tip" style="display:none;">
                                             <div class="profile-tip-avatar">
                                                <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                                <div class="sliding-pan location-span">
                                                   <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                </div>
                                             </div>
                                             <div class="profile-tip-name">
                                                <a href="javascript:void(0)">Adel Hasanat</a>
                                             </div>
                                             <div class="profile-tip-info">
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                </div>
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                </div>
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                                </div>
                                             </div>
                                          </div>
                                       </span>
                                    </div>
                                 </div>
                                 <div class="desc-holder">
                                    <div class="normal-mode">
                                       <div class="desc">
                                          <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                          <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                       </div>
                                       <div class="comment-stuff">
                                          <div class="more-opt">
                                             <span class="likeholder">
                                             <span class="like-tooltip">
                                             <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                             <span class="icon">icon</span>
                                             </a>
                                             </span>
                                             </span>  
                                             <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><span>icon</span></a>
                                             <!-- Dropdown Trigger -->
                                             <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='drop_down65'>
                                             <i class="zmdi zmdi-more"></i>
                                             </a>
                                             <!-- Dropdown Structure -->
                                             <ul id='drop_down65' class='dropdown-content custom_dropdown'>
                                                <li>
                                                   <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                                </li>
                                                <li>
                                                   <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                                </li>
                                             </ul>
                                          </div>
                                          <div class="less-opt">
                                             <div class="timestamp">8h</div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="edit-mode">
                                       <div class="desc">
                                          <textarea class="editcomment-tt materialize-textarea" id="ec-13">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                          <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="clear"></div>
                              <div class="comment-reply-holder comment-addreply">
                                 <div class="addnew-comment valign-wrapper  comment-reply">
                                    <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                                    <div class="desc-holder">                          
                                       <textarea class="materialize-textarea">Write a reply...</textarea>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="pcomment-holder has-comments">
                              <div class="pcomment main-comment">
                                 <div class="img-holder">
                                    <div class="profiletipholder">
                                       <span class="profile-tooltip">
                                       <img class="circle" src="images/demo-profile.jpg"/>
                                       </span>
                                       <span class="profiletooltip_content slidingpan-holder">
                                          <div class="profile-tip" style="display:none;">
                                             <div class="profile-tip-avatar">
                                                <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                                <div class="sliding-pan location-span">
                                                   <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                </div>
                                             </div>
                                             <div class="profile-tip-name">
                                                <a href="javascript:void(0)">Adel Hasanat</a>
                                             </div>
                                             <div class="profile-tip-info">
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                </div>
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                </div>
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                                </div>
                                             </div>
                                          </div>
                                       </span>
                                    </div>
                                 </div>
                                 <div class="desc-holder">
                                    <div class="normal-mode">
                                       <div class="desc">
                                          <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                          <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                       </div>
                                       <div class="comment-stuff">
                                          <div class="more-opt">
                                             <span class="likeholder">
                                             <span class="like-tooltip">
                                             <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                             <span class="icon">icon</span>
                                             </a>
                                             </span>
                                             </span>  
                                             <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><span>icon</span></a>
                                             <!-- Dropdown Trigger -->
                                             <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='drop_down66'>
                                             <i class="zmdi zmdi-more"></i>
                                             </a>
                                             <!-- Dropdown Structure -->
                                             <ul id='drop_down66' class='dropdown-content custom_dropdown'>
                                                <li>
                                                   <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                                </li>
                                                <li>
                                                   <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                                </li>
                                             </ul>
                                          </div>
                                          <div class="less-opt">
                                             <div class="timestamp">8h</div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="edit-mode">
                                       <div class="desc">
                                          <textarea class="editcomment-tt materialize-textarea" id="ec-14">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                          <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="clear"></div>
                              <div class="comment-reply-holder">
                                 <div class="pcomment comment-reply">
                                    <div class="img-holder">
                                       <div class="profiletipholder">
                                          <span class="profile-tooltip">
                                          <img class="circle" src="images/demo-profile.jpg"/>
                                          </span>
                                          <span class="profiletooltip_content">
                                             <div class="profile-tip" style="display:none;">
                                                <div class="slidingpan-holder">
                                                   <div class="profile-tip-avatar">
                                                      <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                                      <div class="sliding-pan location-span">
                                                         <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                      </div>
                                                   </div>
                                                   <div class="profile-tip-name">
                                                      <a href="javascript:void(0)">Adel Hasanat</a>
                                                   </div>
                                                   <div class="profile-tip-info">
                                                      <div class="profiletip-icon">
                                                         <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                      </div>
                                                      <div class="profiletip-icon">
                                                         <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                      </div>
                                                      <div class="profiletip-icon">
                                                         <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </span>
                                       </div>
                                    </div>
                                    <div class="desc-holder">
                                       <div class="normal-mode">
                                          <div class="desc">
                                             <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                             <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                          </div>
                                          <div class="comment-stuff">
                                             <div class="more-opt">
                                                <span class="likeholder">
                                                <span class="like-tooltip">
                                                <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                                <span class="icon">icon</span>
                                                </a>
                                                </span>
                                                </span>
                                                <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><span>icon</span></a>
                                                <!-- Dropdown Trigger -->
                                                <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='drop_down67'>
                                                <i class="zmdi zmdi-more"></i>
                                                </a>
                                                <!-- Dropdown Structure -->
                                                <ul id='drop_down67' class='dropdown-content custom_dropdown'>
                                                   <li>
                                                      <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                                   </li>
                                                   <li>
                                                      <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                                   </li>
                                                </ul>
                                             </div>
                                             <div class="less-opt">
                                                <div class="timestamp">8h</div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="edit-mode">
                                          <div class="desc">
                                             <textarea class="editcomment-tt materialize-textarea" id="ec-15">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                             <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="pcomment comment-reply">
                                    <div class="img-holder">
                                       <div class="profiletipholder">
                                          <span class="profile-tooltip">
                                          <img class="circle" src="images/demo-profile.jpg"/>
                                          </span>
                                          <span class="profiletooltip_content">
                                             <div class="profile-tip" style="display:none;">
                                                <div class="slidingpan-holder">
                                                   <div class="profile-tip-avatar">
                                                      <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                                      <div class="sliding-pan location-span">
                                                         <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                      </div>
                                                   </div>
                                                   <div class="profile-tip-name">
                                                      <a href="javascript:void(0)">Adel Hasanat</a>
                                                   </div>
                                                   <div class="profile-tip-info">
                                                      <div class="profiletip-icon">
                                                         <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                      </div>
                                                      <div class="profiletip-icon">
                                                         <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                      </div>
                                                      <div class="profiletip-icon">
                                                         <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </span>
                                       </div>
                                    </div>
                                    <div class="desc-holder">
                                       <div class="normal-mode">
                                          <div class="desc">
                                             <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                             <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit...</p>
                                          </div>
                                          <div class="comment-stuff">
                                             <div class="more-opt">
                                                <span class="likeholder">
                                                <span class="like-tooltip">
                                                <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                                <span class="icon">icon</span>
                                                </a>
                                                </span>
                                                </span>
                                                <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><span>icon</span></a>
                                                <!-- Dropdown Trigger -->
                                                <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='drop_down68'>
                                                <i class="zmdi zmdi-more"></i>
                                                </a>
                                                <!-- Dropdown Structure -->
                                                <ul id='drop_down68' class='dropdown-content custom_dropdown'>
                                                   <li>
                                                      <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                                   </li>
                                                   <li>
                                                      <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                                   </li>
                                                </ul>
                                             </div>
                                             <div class="less-opt">
                                                <div class="timestamp">8h</div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="edit-mode">
                                          <div class="desc">
                                             <textarea class="editcomment-tt materialize-textarea" id="ec-16">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                             <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="comment-reply-holder comment-addreply">
                                 <div class="addnew-comment valign-wrapper  comment-reply">
                                    <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                                    <div class="desc-holder">                          
                                       <textarea class="materialize-textarea">Write a reply...</textarea>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="pcomment-holder">
                              <div class="pcomment main-comment">
                                 <div class="img-holder">
                                    <div class="profiletipholder">
                                       <span class="profile-tooltip">
                                       <img class="circle" src="images/demo-profile.jpg"/>
                                       </span>
                                       <span class="profiletooltip_content slidingpan-holder">
                                          <div class="profile-tip" style="display:none;">
                                             <div class="profile-tip-avatar">
                                                <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                                <div class="sliding-pan location-span">
                                                   <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                </div>
                                             </div>
                                             <div class="profile-tip-name">
                                                <a href="javascript:void(0)">Adel Hasanat</a>
                                             </div>
                                             <div class="profile-tip-info">
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                </div>
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                </div>
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                                </div>
                                             </div>
                                          </div>
                                       </span>
                                    </div>
                                 </div>
                                 <div class="desc-holder">
                                    <div class="normal-mode">
                                       <div class="desc">
                                          <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                          <p class="shorten">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.<a a="" href="javascript:void(0)" class="overlay" onclick="explandReadMore(this)" style="position: absolute;top: 0;bottom: -6px;left: 0;right: 0;"><span class="readlink">Read More</span></a></p>
                                       </div>
                                       <div class="comment-stuff">
                                          <div class="more-opt">
                                             <span class="likeholder">
                                             <span class="like-tooltip">
                                             <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                             <span class="icon">icon</span>
                                             </a>
                                             </span>
                                             </span>  
                                             <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><span>icon</span></a>
                                             <!-- Dropdown Trigger -->
                                             <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='drop_down69'>
                                             <i class="zmdi zmdi-more"></i>
                                             </a>
                                             <!-- Dropdown Structure -->
                                             <ul id='drop_down69' class='dropdown-content custom_dropdown'>
                                                <li>
                                                   <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                                </li>
                                                <li>
                                                   <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                                </li>
                                             </ul>
                                          </div>
                                          <div class="less-opt">
                                             <div class="timestamp">8h</div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="edit-mode">
                                       <div class="desc">
                                          <textarea class="editcomment-tt materialize-textarea" id="ec-17">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                          <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="clear"></div>
                              <div class="comment-reply-holder comment-addreply">
                                 <div class="addnew-comment valign-wrapper  comment-reply">
                                    <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                                    <div class="desc-holder">                          
                                       <textarea class="materialize-textarea">Write a reply...</textarea>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="addnew-comment valign-wrapper  ">
                        <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                        <div class="desc-holder">                          
                           <textarea class="materialize-textarea" id="comment_txt_3">Write a reply...</textarea>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <!-- end advertise collection -->
      </div>
      <div class="col s12 m12">
         <!-- shared page -->
         <div class="post-holder">
            <div class="post-topbar">
               <div class="post-userinfo">
                  <div class="img-holder">
                     <div class="profiletipholder">
                        <span class="profile-tooltip">
                        <img class="circle" src="images/demo-profile.jpg"/>
                        </span>
                        <span class="profiletooltip_content slidingpan-holder">
                           <div class="profile-tip" style="display:none;">
                              <div class="profile-tip-avatar">
                                 <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                 <div class="sliding-pan location-span">
                                    <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                 </div>
                              </div>
                              <div class="profile-tip-name">
                                 <a href="javascript:void(0)">Adel Hasanat</a>
                              </div>
                              <div class="profile-tip-info">
                                 <div class="profiletip-icon">
                                    <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                 </div>
                                 <div class="profiletip-icon">
                                    <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                 </div>
                                 <div class="profiletip-icon">
                                    <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                 </div>
                              </div>
                           </div>
                        </span>
                     </div>
                  </div>
                  <div class="desc-holder">
                     <a href="javascript:void(0)">Nimish Parekh</a> shared <a href="javascript:void(0)" class="sub-link">Hyatt Hotel</a>'s page.
                     <span class="timestamp">2 hrs<span class="glyphicon glyphicon-globe"></span></span>
                  </div>
               </div> 
               <div class="settings-icon">
			   <div class="dropdown dropdown-custom dropdown-xxsmall">
				  <a class="dropdown-button more_btn" href="javascript:void(0);" data-activates="setting_btn12" style="">					
                  <i class="zmdi zmdi-more"></i>
				  </a>
				  <ul id="setting_btn12" class="dropdown-content custom_dropdown" ><li>
                        <a href="javascript:void(0)">Hide post</a>
                     </li>
                     <li>
                        <a href="javascript:void(0)" class="savepost-link">Save post</a>
                     </li>
                     <li>
                        <a href="javascript:void(0)">Mute notification for this post</a>
                     </li>
                     <li>
                        <a href="javascript:void(0)">Mute connect post</a>
                     </li>
                     <li>
                        <a href="javascript:void(0)">Report post</a>
                     </li>
                     <li>
                        <a href="javascript:void(0)" class="composeeditpostAction">Edit post</a>
                     </li>
				  </ul>				 
				</div>
                  <!-- Dropdown Structure -->
               </div>
            </div>
            <div class="post-content">
               <div class="pdetail-holder">
                  <div class="post-details">
                     <div class="post-desc">
                        <p>Nice place to stay in.</p>
                     </div>
                  </div>
                  <div class="shared-box shared-category">
                     <div class="post-holder">
                        <div class="post-content">
                           <div class="post-img-holder">
                              <div class="post-img">
                                 <div class="pimg-holder">
                                    <div class="bannerimg" style="background:url('images/businessbanner.jpg') center top no-repeat;background-size:cover;"></div>
                                    <div class="profileimg"><img class="circle width-100" src="images/hyattprofile.jpg"/></div>
                                 </div>
                              </div>
                           </div>
                           <div class="share-summery">
                              <div class="sharedpost-title"><a href="javascript:void(0)">Hyatt Hotel</a></div>
                              <div class="sharedpost-tagline">An international hotel</div>
                              <div class="sharedpost-desc">56 people liked this</div>
                              <a href="javascript:void(0)" class="btn btn-primary btn-sm actionbtn waves-effect waves-light">Like</a>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="clear"></div>
            <div class="post-data">
               <div class="post-actions">
                  <span class="likeholder">
                  <span class="like-tooltip">
                  <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                  <i class="zmdi zmdi-thumb-up"></i>
                  </a>
                  </span>
                  </span>
                  <span class="right">
                     <a href="javascript:void(0)" class="pa-comment" title="Comment" data-id="1"><i class="zmdi zmdi-comment"></i></a><span class="comment-lcount">4</span>
                  </span>
               </div>
               <div class="comments-section panel">
                  <div class="post-more">
                     <a href="javascript:void(0)" class="view-morec">View more comments <i class="zmdi zmdi-chevron-right"></i></a>
                     <span class="total-comments">3 of 7</span>
                  </div>
                  <div class="post-comments">
                     <div class="pcomments">
                        <div class="pcomment-earlier">
                           <div class="pcomment-holder">
                              <div class="pcomment main-comment">
                                 <div class="img-holder">
                                    <div class="profiletipholder">
                                       <span class="profile-tooltip">
                                       <img class="circle" src="images/demo-profile.jpg"/>
                                       </span>
                                       <span class="profiletooltip_content slidingpan-holder">
                                          <div class="profile-tip" style="display:none;">
                                             <div class="profile-tip-avatar">
                                                <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                                <div class="sliding-pan location-span">
                                                   <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                </div>
                                             </div>
                                             <div class="profile-tip-name">
                                                <a href="javascript:void(0)">Adel Hasanat</a>
                                             </div>
                                             <div class="profile-tip-info">
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                </div>
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                </div>
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                                </div>
                                             </div>
                                          </div>
                                       </span>
                                    </div>
                                 </div>
                                 <div class="desc-holder">
                                    <div class="normal-mode">
                                       <div class="desc">
                                          <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                          <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                       </div>
                                       <div class="comment-stuff">
                                          <div class="more-opt">
                                             <span class="likeholder">
                                             <span class="like-tooltip">
                                             <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                             <span class="icon">icon</span>
                                             </a>
                                             </span>
                                             </span>  
                                             <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><span>icon</span></a>
                                             <!-- Dropdown Trigger -->
                                             <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='drop_down70'>
                                             <i class="zmdi zmdi-more"></i>
                                             </a>
                                             <!-- Dropdown Structure -->
                                             <ul id='drop_down70' class='dropdown-content custom_dropdown'>
                                                <li>
                                                   <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                                </li>
                                                <li>
                                                   <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                                </li>
                                             </ul>
                                          </div>
                                          <div class="less-opt">
                                             <div class="timestamp">8h</div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="edit-mode">
                                       <div class="desc">
                                          <textarea class="editcomment-tt materialize-textarea" id="ec-12">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                          <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="clear"></div>
                              <div class="comment-reply-holder comment-addreply">
                                 <div class="addnew-comment valign-wrapper  comment-reply">
                                    <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                                    <div class="desc-holder">                          
                                       <textarea class="materialize-textarea">Write a reply...</textarea>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="pcomment-holder">
                              <div class="pcomment main-comment">
                                 <div class="img-holder">
                                    <div class="profiletipholder">
                                       <span class="profile-tooltip">
                                       <img class="circle" src="images/demo-profile.jpg"/>
                                       </span>
                                       <span class="profiletooltip_content slidingpan-holder">
                                          <div class="profile-tip" style="display:none;">
                                             <div class="profile-tip-avatar">
                                                <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                                <div class="sliding-pan location-span">
                                                   <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                </div>
                                             </div>
                                             <div class="profile-tip-name">
                                                <a href="javascript:void(0)">Adel Hasanat</a>
                                             </div>
                                             <div class="profile-tip-info">
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                </div>
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                </div>
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                                </div>
                                             </div>
                                          </div>
                                       </span>
                                    </div>
                                 </div>
                                 <div class="desc-holder">
                                    <div class="normal-mode">
                                       <div class="desc">
                                          <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                          <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                       </div>
                                       <div class="comment-stuff">
                                          <div class="more-opt">
                                             <span class="likeholder">
                                             <span class="like-tooltip">
                                             <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                             <span class="icon">icon</span>
                                             </a>
                                             </span>
                                             </span>  
                                             <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><span>icon</span></a>
                                             <!-- Dropdown Trigger -->
                                             <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='drop_down71'>
                                             <i class="zmdi zmdi-more"></i>
                                             </a>
                                             <!-- Dropdown Structure -->
                                             <ul id='drop_down71' class='dropdown-content custom_dropdown'>
                                                <li>
                                                   <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                                </li>
                                                <li>
                                                   <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                                </li>
                                             </ul>
                                          </div>
                                          <div class="less-opt">
                                             <div class="timestamp">8h</div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="edit-mode">
                                       <div class="desc">
                                          <textarea class="editcomment-tt materialize-textarea" id="ec-13">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                          <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="clear"></div>
                              <div class="comment-reply-holder comment-addreply">
                                 <div class="addnew-comment valign-wrapper  comment-reply">
                                    <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                                    <div class="desc-holder">                          
                                       <textarea class="materialize-textarea">Write a reply...</textarea>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="pcomment-holder has-comments">
                           <div class="pcomment main-comment">
                              <div class="img-holder">
                                 <div class="profiletipholder">
                                    <span class="profile-tooltip">
                                    <img class="circle" src="images/demo-profile.jpg"/>
                                    </span>
                                    <span class="profiletooltip_content slidingpan-holder">
                                       <div class="profile-tip" style="display:none;">
                                          <div class="profile-tip-avatar">
                                             <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                             <div class="sliding-pan location-span">
                                                <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                             </div>
                                          </div>
                                          <div class="profile-tip-name">
                                             <a href="javascript:void(0)">Adel Hasanat</a>
                                          </div>
                                          <div class="profile-tip-info">
                                             <div class="profiletip-icon">
                                                <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                             </div>
                                             <div class="profiletip-icon">
                                                <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                             </div>
                                             <div class="profiletip-icon">
                                                <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                             </div>
                                          </div>
                                       </div>
                                    </span>
                                 </div>
                              </div>
                              <div class="desc-holder">
                                 <div class="normal-mode">
                                    <div class="desc">
                                       <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                       <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                    </div>
                                    <div class="comment-stuff">
                                       <div class="more-opt">
                                          <span class="likeholder">
                                          <span class="like-tooltip">
                                          <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                          <span class="icon">icon</span>
                                          </a>
                                          </span>
                                          </span>
                                          <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><span>icon</span></a>
                                          <!-- Dropdown Trigger -->
                                          <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='drop_down72'>
                                          <i class="zmdi zmdi-more"></i>
                                          </a>
                                          <!-- Dropdown Structure -->
                                          <ul id='drop_down72' class='dropdown-content custom_dropdown'>
                                             <li>
                                                <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                             </li>
                                             <li>
                                                <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                             </li>
                                          </ul>
                                       </div>
                                       <div class="less-opt">
                                          <div class="timestamp">8h</div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="edit-mode">
                                    <div class="desc">
                                       <textarea class="editcomment-tt materialize-textarea" id="ec-14">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                       <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="clear"></div>
                           <div class="comment-reply-holder">
                              <div class="pcomment comment-reply">
                                 <div class="img-holder">
                                    <div class="profiletipholder">
                                       <span class="profile-tooltip">
                                       <img class="circle" src="images/demo-profile.jpg"/>
                                       </span>
                                       <span class="profiletooltip_content slidingpan-holder">
                                          <div class="profile-tip" style="display:none;">
                                             <div class="profile-tip-avatar">
                                                <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                                <div class="sliding-pan location-span">
                                                   <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                </div>
                                             </div>
                                             <div class="profile-tip-name">
                                                <a href="javascript:void(0)">Adel Hasanat</a>
                                             </div>
                                             <div class="profile-tip-info">
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                </div>
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                </div>
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                                </div>
                                             </div>
                                          </div>
                                       </span>
                                    </div>
                                 </div>
                                 <div class="desc-holder">
                                    <div class="normal-mode">
                                       <div class="desc">
                                          <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                          <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                       </div>
                                       <div class="comment-stuff">
                                          <div class="more-opt">
                                             <span class="likeholder">
                                             <span class="like-tooltip">
                                             <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                             <span class="icon">icon</span>
                                             </a>
                                             </span>
                                             </span>
                                             <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><span>icon</span></a>
                                             <!-- Dropdown Trigger -->
                                             <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='drop_down73'>
                                             <i class="zmdi zmdi-more"></i>
                                             </a>
                                             <!-- Dropdown Structure -->
                                             <ul id='drop_down73' class='dropdown-content custom_dropdown'>
                                                <li>
                                                   <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                                </li>
                                                <li>
                                                   <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                                </li>
                                             </ul>
                                          </div>
                                          <div class="less-opt">
                                             <div class="timestamp">8h</div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="edit-mode">
                                       <div class="desc">
                                          <textarea class="editcomment-tt materialize-textarea" id="ec-15">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                          <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="pcomment comment-reply">
                                 <div class="img-holder">
                                    <div class="profiletipholder">
                                       <span class="profile-tooltip">
                                       <img class="circle" src="images/demo-profile.jpg"/>
                                       </span>
                                       <span class="profiletooltip_content slidingpan-holder">
                                          <div class="profile-tip" style="display:none;">
                                             <div class="profile-tip-avatar">
                                                <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                                <div class="sliding-pan location-span">
                                                   <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                </div>
                                             </div>
                                             <div class="profile-tip-name">
                                                <a href="javascript:void(0)">Adel Hasanat</a>
                                             </div>
                                             <div class="profile-tip-info">
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                </div>
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                </div>
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                                </div>
                                             </div>
                                          </div>
                                       </span>
                                    </div>
                                 </div>
                                 <div class="desc-holder">
                                    <div class="normal-mode">
                                       <div class="desc">
                                          <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                          <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit...</p>
                                       </div>
                                       <div class="comment-stuff">
                                          <div class="more-opt">
                                             <span class="likeholder">
                                             <span class="like-tooltip">
                                             <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                             <span class="icon">icon</span>
                                             </a>
                                             </span>
                                             </span>
                                             <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><span>icon</span></a>
                                             <!-- Dropdown Trigger -->
                                             <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='drop_down74'>
                                             <i class="zmdi zmdi-more"></i>
                                             </a>
                                             <!-- Dropdown Structure -->
                                             <ul id='drop_down74' class='dropdown-content custom_dropdown'>
                                                <li>
                                                   <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                                </li>
                                                <li>
                                                   <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                                </li>
                                             </ul>
                                          </div>
                                          <div class="less-opt">
                                             <div class="timestamp">8h</div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="edit-mode">
                                       <div class="desc">
                                          <textarea class="editcomment-tt materialize-textarea" id="ec-16">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                          <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="comment-reply-holder comment-addreply">
                              <div class="addnew-comment valign-wrapper  comment-reply">
                                 <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                                 <div class="desc-holder">                          
                                    <textarea class="materialize-textarea">Write a reply...</textarea>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="pcomment-holder">
                           <div class="pcomment main-comment">
                              <div class="img-holder">
                                 <div class="profiletipholder">
                                    <span class="profile-tooltip">
                                    <img class="circle" src="images/demo-profile.jpg"/>
                                    </span>
                                    <span class="profiletooltip_content slidingpan-holder">
                                       <div class="profile-tip" style="display:none;">
                                          <div class="profile-tip-avatar">
                                             <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                             <div class="sliding-pan location-span">
                                                <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                             </div>
                                          </div>
                                          <div class="profile-tip-name">
                                             <a href="javascript:void(0)">Adel Hasanat</a>
                                          </div>
                                          <div class="profile-tip-info">
                                             <div class="profiletip-icon">
                                                <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                             </div>
                                             <div class="profiletip-icon">
                                                <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                             </div>
                                             <div class="profiletip-icon">
                                                <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                             </div>
                                          </div>
                                       </div>
                                    </span>
                                 </div>
                              </div>
                              <div class="desc-holder">
                                 <div class="normal-mode">
                                    <div class="desc">
                                       <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                       <p class="shorten">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.<a a="" href="javascript:void(0)" class="overlay" onclick="explandReadMore(this)" style="position: absolute;top: 0;bottom: -6px;left: 0;right: 0;"><span class="readlink">Read More</span></a></p>
                                    </div>
                                    <div class="comment-stuff">
                                       <div class="more-opt">
                                          <span class="likeholder">
                                          <span class="like-tooltip">
                                          <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                          <span class="icon">icon</span>
                                          </a>
                                          </span>
                                          </span>  
                                          <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><span>icon</span></a>
                                          <!-- Dropdown Trigger -->
                                          <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='drop_down75'>
                                          <i class="zmdi zmdi-more"></i>
                                          </a>
                                          <!-- Dropdown Structure -->
                                          <ul id='drop_down75' class='dropdown-content custom_dropdown'>
                                             <li>
                                                <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                             </li>
                                             <li>
                                                <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                             </li>
                                          </ul>
                                       </div>
                                       <div class="less-opt">
                                          <div class="timestamp">8h</div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="edit-mode">
                                    <div class="desc">
                                       <textarea class="editcomment-tt materialize-textarea" id="ec-17">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                       <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="clear"></div>
                           <div class="comment-reply-holder comment-addreply">
                              <div class="addnew-comment valign-wrapper  comment-reply">
                                 <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                                 <div class="desc-holder">                          
                                    <textarea class="materialize-textarea">Write a reply...</textarea>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="addnew-comment valign-wrapper  ">
                        <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                        <div class="desc-holder">                          
                           <textarea class="materialize-textarea" id="comment_txt_3">Write a reply...</textarea>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <!-- end shared page -->
      </div>
      <div class="col s12 m12">
         <!-- shared event -->
         <div class="post-holder">
            <div class="post-topbar">
               <div class="post-userinfo">
                  <div class="img-holder">
                     <div class="profiletipholder">
                        <span class="profile-tooltip">
                        <img class="circle" src="images/demo-profile.jpg"/>
                        </span>
                        <span class="profiletooltip_content slidingpan-holder">
                           <div class="profile-tip" style="display:none;">
                              <div class="profile-tip-avatar">
                                 <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                 <div class="sliding-pan location-span">
                                    <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                 </div>
                              </div>
                              <div class="profile-tip-name">
                                 <a href="javascript:void(0)">Adel Hasanat</a>
                              </div>
                              <div class="profile-tip-info">
                                 <div class="profiletip-icon">
                                    <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                 </div>
                                 <div class="profiletip-icon">
                                    <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                 </div>
                                 <div class="profiletip-icon">
                                    <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                 </div>
                              </div>
                           </div>
                        </span>
                     </div>
                  </div>
                  <div class="desc-holder">
                     <a href="javascript:void(0)">Nimish Parekh</a> shared <a href="javascript:void(0)" class="sub-link">Juice BCN</a>'s event.
                     <span class="timestamp">2 hrs<span class="glyphicon glyphicon-globe"></span></span>
                  </div>
               </div> 
               <div class="settings-icon">
			   <div class="dropdown dropdown-custom dropdown-xxsmall">
				  <a class="dropdown-button more_btn" href="javascript:void(0);" data-activates="setting_btn13" style="">					
                  <i class="zmdi zmdi-more"></i>
				  </a>
				  <ul id="setting_btn13" class="dropdown-content custom_dropdown" ><li>
                        <a href="javascript:void(0)">Hide post</a>
                     </li>
                     <li>
                        <a href="javascript:void(0)" class="savepost-link">Save post</a>
                     </li>
                     <li>
                        <a href="javascript:void(0)">Mute notification for this post</a>
                     </li>
                     <li>
                        <a href="javascript:void(0)">Mute connect post</a>
                     </li>
                     <li>
                        <a href="javascript:void(0)">Report post</a>
                     </li>
                     <li>
                        <a href="javascript:void(0)" class="composeeditpostAction">Edit post</a>
                     </li>
				  </ul>				 
				</div>
                  <!-- Dropdown Structure -->
               </div>
            </div>
            <div class="post-content">
               <div class="pdetail-holder">
                  <div class="post-details">
                     <div class="post-desc">
                        <p>I'm exciting to attend.</p>
                     </div>
                  </div>
                  <div class="shared-box shared-category">
                     <div class="post-holder">
                        <div class="post-content">
                           <div class="post-img-holder">
                              <div class="post-img one-img">
                                 <div class="pimg-holder"><img src="images/musicevent.jpg"/></div>
                              </div>
                           </div>
                           <div class="share-summery">
                              <div class="datebox"><span class="month">Nov</span><span class="date">17</span></div>
                              <div class="sharedpost-title"><a href="javascript:void(0)">Rock Music Night</a></div>
                              <div class="sharedpost-subtitle">Barcelona, Spain</div>
                              <div class="sharedpost-desc">26 people attending</div>
                              <a href="javascript:void(0)" class="btn btn-primary btn-sm actionbtn waves-effect waves-light">Attend</a>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="clear"></div>
            <div class="post-data">
               <div class="post-actions">
                  <span class="likeholder">
                  <span class="like-tooltip">
                  <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                  <i class="zmdi zmdi-thumb-up"></i>
                  </a>
                  </span>
                  </span>
                  <span class="right">
                     <a href="javascript:void(0)" class="pa-comment" title="Comment" data-id="1"><i class="zmdi zmdi-comment"></i></a><span class="comment-lcount">4</span>
                  </span>
               </div>
               <div class="comments-section panel">
                  <div class="post-more">
                     <a href="javascript:void(0)" class="view-morec">View more comments <i class="zmdi zmdi-chevron-right"></i></a>
                     <span class="total-comments">3 of 7</span>
                  </div>
                  <div class="post-comments">
                     <div class="pcomments">
                        <div class="pcomment-earlier">
                           <div class="pcomment-holder">
                              <div class="pcomment main-comment">
                                 <div class="img-holder">
                                    <div class="profiletipholder">
                                       <span class="profile-tooltip">
                                       <img class="circle" src="images/demo-profile.jpg"/>
                                       </span>
                                       <span class="profiletooltip_content slidingpan-holder">
                                          <div class="profile-tip" style="display:none;">
                                             <div class="profile-tip-avatar">
                                                <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                                <div class="sliding-pan location-span">
                                                   <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                </div>
                                             </div>
                                             <div class="profile-tip-name">
                                                <a href="javascript:void(0)">Adel Hasanat</a>
                                             </div>
                                             <div class="profile-tip-info">
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                </div>
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                </div>
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                                </div>
                                             </div>
                                          </div>
                                       </span>
                                    </div>
                                 </div>
                                 <div class="desc-holder">
                                    <div class="normal-mode">
                                       <div class="desc">
                                          <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                          <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                       </div>
                                       <div class="comment-stuff">
                                          <div class="more-opt">
                                             <span class="likeholder">
                                             <span class="like-tooltip">
                                             <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                             <span class="icon">icon</span>
                                             </a>
                                             </span>
                                             </span>  
                                             <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><span>icon</span></a>
                                             <!-- Dropdown Trigger -->
                                             <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='drop_down76'>
                                             <i class="zmdi zmdi-more"></i>
                                             </a>
                                             <!-- Dropdown Structure -->
                                             <ul id='drop_down76' class='dropdown-content custom_dropdown'>
                                                <li>
                                                   <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                                </li>
                                                <li>
                                                   <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                                </li>
                                             </ul>
                                          </div>
                                          <div class="less-opt">
                                             <div class="timestamp">8h</div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="edit-mode">
                                       <div class="desc">
                                          <textarea class="editcomment-tt materialize-textarea" id="ec-12">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                          <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="clear"></div>
                              <div class="comment-reply-holder comment-addreply">
                                 <div class="addnew-comment valign-wrapper  comment-reply">
                                    <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                                    <div class="desc-holder">                          
                                       <textarea class="materialize-textarea">Write a reply...</textarea>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="pcomment-holder">
                              <div class="pcomment main-comment">
                                 <div class="img-holder">
                                    <div class="profiletipholder">
                                       <span class="profile-tooltip">
                                       <img class="circle" src="images/demo-profile.jpg"/>
                                       </span>
                                       <span class="profiletooltip_content slidingpan-holder">
                                          <div class="profile-tip" style="display:none;">
                                             <div class="profile-tip-avatar">
                                                <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                                <div class="sliding-pan location-span">
                                                   <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                </div>
                                             </div>
                                             <div class="profile-tip-name">
                                                <a href="javascript:void(0)">Adel Hasanat</a>
                                             </div>
                                             <div class="profile-tip-info">
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                </div>
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                </div>
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                                </div>
                                             </div>
                                          </div>
                                       </span>
                                    </div>
                                 </div>
                                 <div class="desc-holder">
                                    <div class="normal-mode">
                                       <div class="desc">
                                          <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                          <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                       </div>
                                       <div class="comment-stuff">
                                          <div class="more-opt">
                                             <span class="likeholder">
                                             <span class="like-tooltip">
                                             <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                             <span class="icon">icon</span>
                                             </a>
                                             </span>
                                             </span>  
                                             <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><span>icon</span></a>
                                             <!-- Dropdown Trigger -->
                                             <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='drop_down77'>
                                             <i class="zmdi zmdi-more"></i>
                                             </a>
                                             <!-- Dropdown Structure -->
                                             <ul id='drop_down77' class='dropdown-content custom_dropdown'>
                                                <li>
                                                   <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                                </li>
                                                <li>
                                                   <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                                </li>
                                             </ul>
                                          </div>
                                          <div class="less-opt">
                                             <div class="timestamp">8h</div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="edit-mode">
                                       <div class="desc">
                                          <textarea class="editcomment-tt materialize-textarea" id="ec-13">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                          <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="clear"></div>
                              <div class="comment-reply-holder comment-addreply">
                                 <div class="addnew-comment valign-wrapper  comment-reply">
                                    <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                                    <div class="desc-holder">                          
                                       <textarea class="materialize-textarea">Write a reply...</textarea>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="pcomment-holder has-comments">
                           <div class="pcomment main-comment">
                              <div class="img-holder">
                                 <div class="profiletipholder">
                                    <span class="profile-tooltip">
                                    <img class="circle" src="images/demo-profile.jpg"/>
                                    </span>
                                    <span class="profiletooltip_content slidingpan-holder">
                                       <div class="profile-tip" style="display:none;">
                                          <div class="profile-tip-avatar">
                                             <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                             <div class="sliding-pan location-span">
                                                <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                             </div>
                                          </div>
                                          <div class="profile-tip-name">
                                             <a href="javascript:void(0)">Adel Hasanat</a>
                                          </div>
                                          <div class="profile-tip-info">
                                             <div class="profiletip-icon">
                                                <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                             </div>
                                             <div class="profiletip-icon">
                                                <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                             </div>
                                             <div class="profiletip-icon">
                                                <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                             </div>
                                          </div>
                                       </div>
                                    </span>
                                 </div>
                              </div>
                              <div class="desc-holder">
                                 <div class="normal-mode">
                                    <div class="desc">
                                       <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                       <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                    </div>
                                    <div class="comment-stuff">
                                       <div class="more-opt">
                                          <span class="likeholder">
                                          <span class="like-tooltip">
                                          <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                          <span class="icon">icon</span>
                                          </a>
                                          </span>
                                          </span>
                                          <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><span>icon</span></a>
                                          <!-- Dropdown Trigger -->
                                          <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='drop_down78'>
                                          <i class="zmdi zmdi-more"></i>
                                          </a>
                                          <!-- Dropdown Structure -->
                                          <ul id='drop_down78' class='dropdown-content custom_dropdown'>
                                             <li>
                                                <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                             </li>
                                             <li>
                                                <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                             </li>
                                          </ul>
                                       </div>
                                       <div class="less-opt">
                                          <div class="timestamp">8h</div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="edit-mode">
                                    <div class="desc">
                                       <textarea class="editcomment-tt materialize-textarea" id="ec-14">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                       <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="clear"></div>
                           <div class="comment-reply-holder">
                              <div class="pcomment comment-reply">
                                 <div class="img-holder">
                                    <div class="profiletipholder">
                                       <span class="profile-tooltip">
                                       <img class="circle" src="images/demo-profile.jpg"/>
                                       </span>
                                       <span class="profiletooltip_content slidingpan-holder">
                                          <div class="profile-tip" style="display:none;">
                                             <div class="profile-tip-avatar">
                                                <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                                <div class="sliding-pan location-span">
                                                   <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                </div>
                                             </div>
                                             <div class="profile-tip-name">
                                                <a href="javascript:void(0)">Adel Hasanat</a>
                                             </div>
                                             <div class="profile-tip-info">
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                </div>
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                </div>
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                                </div>
                                             </div>
                                          </div>
                                       </span>
                                    </div>
                                 </div>
                                 <div class="desc-holder">
                                    <div class="normal-mode">
                                       <div class="desc">
                                          <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                          <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                       </div>
                                       <div class="comment-stuff">
                                          <div class="more-opt">
                                             <span class="likeholder">
                                             <span class="like-tooltip">
                                             <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                             <span class="icon">icon</span>
                                             </a>
                                             </span>
                                             </span>
                                             <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><span>icon</span></a>
                                             <!-- Dropdown Trigger -->
                                             <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='drop_down79'>
                                             <i class="zmdi zmdi-more"></i>
                                             </a>
                                             <!-- Dropdown Structure -->
                                             <ul id='drop_down79' class='dropdown-content custom_dropdown'>
                                                <li>
                                                   <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                                </li>
                                                <li>
                                                   <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                                </li>
                                             </ul>
                                          </div>
                                          <div class="less-opt">
                                             <div class="timestamp">8h</div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="edit-mode">
                                       <div class="desc">
                                          <textarea class="editcomment-tt materialize-textarea" id="ec-15">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                          <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="pcomment comment-reply">
                                 <div class="img-holder">
                                    <div class="profiletipholder">
                                       <span class="profile-tooltip">
                                       <img class="circle" src="images/demo-profile.jpg"/>
                                       </span>
                                       <span class="profiletooltip_content slidingpan-holder">
                                          <div class="profile-tip" style="display:none;">
                                             <div class="profile-tip-avatar">
                                                <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                                <div class="sliding-pan location-span">
                                                   <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                </div>
                                             </div>
                                             <div class="profile-tip-name">
                                                <a href="javascript:void(0)">Adel Hasanat</a>
                                             </div>
                                             <div class="profile-tip-info">
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                </div>
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                </div>
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                                </div>
                                             </div>
                                          </div>
                                       </span>
                                    </div>
                                 </div>
                                 <div class="desc-holder">
                                    <div class="normal-mode">
                                       <div class="desc">
                                          <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                          <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit...</p>
                                       </div>
                                       <div class="comment-stuff">
                                          <div class="more-opt">
                                             <span class="likeholder">
                                             <span class="like-tooltip">
                                             <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                             <span class="icon">icon</span>
                                             </a>
                                             </span>
                                             </span>
                                             <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><span>icon</span></a>
                                             <!-- Dropdown Trigger -->
                                             <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='drop_down80'>
                                             <i class="zmdi zmdi-more"></i>
                                             </a>
                                             <!-- Dropdown Structure -->
                                             <ul id='drop_down80' class='dropdown-content custom_dropdown'>
                                                <li>
                                                   <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                                </li>
                                                <li>
                                                   <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                                </li>
                                             </ul>
                                          </div>
                                          <div class="less-opt">
                                             <div class="timestamp">8h</div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="edit-mode">
                                       <div class="desc">
                                          <textarea class="editcomment-tt materialize-textarea" id="ec-16">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                          <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="comment-reply-holder comment-addreply">
                              <div class="addnew-comment valign-wrapper  comment-reply">
                                 <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                                 <div class="desc-holder">                          
                                    <textarea class="materialize-textarea">Write a reply...</textarea>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="pcomment-holder">
                           <div class="pcomment main-comment">
                              <div class="img-holder">
                                 <div class="profiletipholder">
                                    <span class="profile-tooltip">
                                    <img class="circle" src="images/demo-profile.jpg"/>
                                    </span>
                                    <span class="profiletooltip_content slidingpan-holder">
                                       <div class="profile-tip" style="display:none;">
                                          <div class="profile-tip-avatar">
                                             <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                             <div class="sliding-pan location-span">
                                                <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                             </div>
                                          </div>
                                          <div class="profile-tip-name">
                                             <a href="javascript:void(0)">Adel Hasanat</a>
                                          </div>
                                          <div class="profile-tip-info">
                                             <div class="profiletip-icon">
                                                <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                             </div>
                                             <div class="profiletip-icon">
                                                <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                             </div>
                                             <div class="profiletip-icon">
                                                <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                             </div>
                                          </div>
                                       </div>
                                    </span>
                                 </div>
                              </div>
                              <div class="desc-holder">
                                 <div class="normal-mode">
                                    <div class="desc">
                                       <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                       <p class="shorten">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.<a a="" href="javascript:void(0)" class="overlay" onclick="explandReadMore(this)" style="position: absolute;top: 0;bottom: -6px;left: 0;right: 0;"><span class="readlink">Read More</span></a></p>
                                    </div>
                                    <div class="comment-stuff">
                                       <div class="more-opt">
                                          <span class="likeholder">
                                          <span class="like-tooltip">
                                          <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                          <span class="icon">icon</span>
                                          </a>
                                          </span>
                                          </span>  
                                          <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><span>icon</span></a>
                                          <!-- Dropdown Trigger -->
                                          <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='drop_down81'>
                                          <i class="zmdi zmdi-more"></i>
                                          </a>
                                          <!-- Dropdown Structure -->
                                          <ul id='drop_down81' class='dropdown-content custom_dropdown'>
                                             <li>
                                                <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                             </li>
                                             <li>
                                                <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                             </li>
                                          </ul>
                                       </div>
                                       <div class="less-opt">
                                          <div class="timestamp">8h</div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="edit-mode">
                                    <div class="desc">
                                       <textarea class="editcomment-tt materialize-textarea" id="ec-17">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                       <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="clear"></div>
                           <div class="comment-reply-holder comment-addreply">
                              <div class="addnew-comment valign-wrapper  comment-reply">
                                 <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                                 <div class="desc-holder">                          
                                    <textarea class="materialize-textarea">Write a reply...</textarea>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="addnew-comment valign-wrapper  ">
                        <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                        <div class="desc-holder">                       
                           <textarea class="materialize-textarea" id="comment_txt_3">Write a reply...</textarea>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <!-- end shared event -->
      </div>
      <div class="col s12 m12">
         <!-- shared group -->
         <div class="post-holder">
            <div class="post-topbar">
               <div class="post-userinfo">
                  <div class="img-holder">
                     <div class="profiletipholder">
                        <span class="profile-tooltip">
                        <img class="circle" src="images/demo-profile.jpg"/>
                        </span>
                        <span class="profiletooltip_content slidingpan-holder">
                           <div class="profile-tip" style="display:none;">
                              <div class="profile-tip-avatar">
                                 <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                 <div class="sliding-pan location-span">
                                    <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                 </div>
                              </div>
                              <div class="profile-tip-name">
                                 <a href="javascript:void(0)">Adel Hasanat</a>
                              </div>
                              <div class="profile-tip-info">
                                 <div class="profiletip-icon">
                                    <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                 </div>
                                 <div class="profiletip-icon">
                                    <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                 </div>
                                 <div class="profiletip-icon">
                                    <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                 </div>
                              </div>
                           </div>
                        </span>
                     </div>
                  </div>
                  <div class="desc-holder">
                     <a href="javascript:void(0)">Nimish Parekh</a> shared <a href="javascript:void(0)" class="sub-link">Dr. Bell</a>'s group.
                     <span class="timestamp">2 hrs<span class="glyphicon glyphicon-globe"></span></span>
                  </div>
               </div> 
               <div class="settings-icon">
			   <div class="dropdown dropdown-custom dropdown-xxsmall">
				  <a class="dropdown-button more_btn" href="javascript:void(0);" data-activates="setting_btn14" style="">					
                  <i class="zmdi zmdi-more"></i>
				  </a>
				  <ul id="setting_btn14" class="dropdown-content custom_dropdown" ><li>
                        <a href="javascript:void(0)">Hide post</a>
                     </li>
                     <li>
                        <a href="javascript:void(0)" class="savepost-link">Save post</a>
                     </li>
                     <li>
                        <a href="javascript:void(0)">Mute notification for this post</a>
                     </li>
                     <li>
                        <a href="javascript:void(0)">Mute connect post</a>
                     </li>
                     <li>
                        <a href="javascript:void(0)">Report post</a>
                     </li>
                     <li>
                        <a href="javascript:void(0)" class="composeeditpostAction">Edit post</a>
                     </li>
				  </ul>				 
				</div>
                  <!-- Dropdown Structure -->
               </div>
            </div>
            <div class="post-content">
               <div class="pdetail-holder">
                  <div class="post-details">
                     <div class="post-desc">
                        <p>Informative with latest updates of medical treatments.</p>
                     </div>
                  </div>
                  <div class="shared-box shared-category">
                     <div class="post-holder">
                        <div class="post-content">
                           <div class="post-img-holder">
                              <div class="post-img one-img">
                                 <div class="pimg-holder"><img src="images/healthgroup.jpg"/>
                                 </div>
                              </div>
                           </div>
                           <div class="share-summery">
                              <div class="sharedpost-title"><a href="javascript:void(0)">Heal Well</a></div>
                              <div class="sharedpost-subtitle">Barcelona, Spain</div>
                              <div class="sharedpost-desc">123 members<i class="mdi mdi-circle"></i>35 posts</div>
                              <a href="javascript:void(0)" class="btn btn-primary btn-sm actionbtn waves-effect waves-light">Join</a>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="clear"></div>
            <div class="post-data">
               <div class="post-actions">
                  <span class="likeholder">
                  <span class="like-tooltip">
                  <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                  <i class="zmdi zmdi-thumb-up"></i>
                  </a>
                  </span>
                  </span>
                  <span class="right">
                     <a href="javascript:void(0)" class="pa-comment" title="Comment" data-id="1"><i class="zmdi zmdi-comment"></i></a><span class="comment-lcount">4</span>
                  </span>
               </div>
               <div class="comments-section panel">
                  <div class="post-more">
                     <a href="javascript:void(0)" class="view-morec">View more comments <i class="zmdi zmdi-chevron-right"></i></a>
                     <span class="total-comments">3 of 7</span>
                  </div>
                  <div class="post-comments">
                     <div class="pcomments">
                        <div class="pcomment-earlier">
                           <div class="pcomment-holder">
                              <div class="pcomment main-comment">
                                 <div class="img-holder">
                                    <div class="profiletipholder">
                                       <span class="profile-tooltip">
                                       <img class="circle" src="images/demo-profile.jpg"/>
                                       </span>
                                       <span class="profiletooltip_content slidingpan-holder">
                                          <div class="profile-tip" style="display:none;">
                                             <div class="profile-tip-avatar">
                                                <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                                <div class="sliding-pan location-span">
                                                   <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                </div>
                                             </div>
                                             <div class="profile-tip-name">
                                                <a href="javascript:void(0)">Adel Hasanat</a>
                                             </div>
                                             <div class="profile-tip-info">
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                </div>
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                </div>
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                                </div>
                                             </div>
                                          </div>
                                       </span>
                                    </div>
                                 </div>
                                 <div class="desc-holder">
                                    <div class="normal-mode">
                                       <div class="desc">
                                          <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                          <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                       </div>
                                       <div class="comment-stuff">
                                          <div class="more-opt">
                                             <span class="likeholder">
                                             <span class="like-tooltip">
                                             <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                             <span class="icon">icon</span>
                                             </a>
                                             </span>
                                             </span>  
                                             <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><span>icon</span></a>
                                             <!-- Dropdown Trigger -->
                                             <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='drop_down82'>
                                             <i class="zmdi zmdi-more"></i>
                                             </a>
                                             <!-- Dropdown Structure -->
                                             <ul id='drop_down82' class='dropdown-content custom_dropdown'>
                                                <li>
                                                   <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                                </li>
                                                <li>
                                                   <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                                </li>
                                             </ul>
                                          </div>
                                          <div class="less-opt">
                                             <div class="timestamp">8h</div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="edit-mode">
                                       <div class="desc">
                                          <textarea class="editcomment-tt materialize-textarea" id="ec-12">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                          <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="clear"></div>
                              <div class="comment-reply-holder comment-addreply">
                                 <div class="addnew-comment valign-wrapper  comment-reply">
                                    <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                                    <div class="desc-holder">                          
                                       <textarea class="materialize-textarea">Write a reply...</textarea>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="pcomment-holder">
                              <div class="pcomment main-comment">
                                 <div class="img-holder">
                                    <div class="profiletipholder">
                                       <span class="profile-tooltip">
                                       <img class="circle" src="images/demo-profile.jpg"/>
                                       </span>
                                       <span class="profiletooltip_content slidingpan-holder">
                                          <div class="profile-tip" style="display:none;">
                                             <div class="profile-tip-avatar">
                                                <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                                <div class="sliding-pan location-span">
                                                   <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                </div>
                                             </div>
                                             <div class="profile-tip-name">
                                                <a href="javascript:void(0)">Adel Hasanat</a>
                                             </div>
                                             <div class="profile-tip-info">
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                </div>
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                </div>
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                                </div>
                                             </div>
                                          </div>
                                       </span>
                                    </div>
                                 </div>
                                 <div class="desc-holder">
                                    <div class="normal-mode">
                                       <div class="desc">
                                          <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                          <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                       </div>
                                       <div class="comment-stuff">
                                          <div class="more-opt">
                                             <span class="likeholder">
                                             <span class="like-tooltip">
                                             <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                             <span class="icon">icon</span>
                                             </a>
                                             </span>
                                             </span>  
                                             <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><span>icon</span></a>
                                             <!-- Dropdown Trigger -->
                                             <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='drop_down83'>
                                             <i class="zmdi zmdi-more"></i>
                                             </a>
                                             <!-- Dropdown Structure -->
                                             <ul id='drop_down83' class='dropdown-content custom_dropdown'>
                                                <li>
                                                   <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                                </li>
                                                <li>
                                                   <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                                </li>
                                             </ul>
                                          </div>
                                          <div class="less-opt">
                                             <div class="timestamp">8h</div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="edit-mode">
                                       <div class="desc">
                                          <textarea class="editcomment-tt materialize-textarea" id="ec-13">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                          <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="clear"></div>
                              <div class="comment-reply-holder comment-addreply">
                                 <div class="addnew-comment valign-wrapper  comment-reply">
                                    <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                                    <div class="desc-holder">                          
                                       <textarea class="materialize-textarea">Write a reply...</textarea>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="pcomment-holder has-comments">
                           <div class="pcomment main-comment">
                              <div class="img-holder">
                                 <div class="profiletipholder">
                                    <span class="profile-tooltip">
                                    <img class="circle" src="images/demo-profile.jpg"/>
                                    </span>
                                    <span class="profiletooltip_content slidingpan-holder">
                                       <div class="profile-tip" style="display:none;">
                                          <div class="profile-tip-avatar">
                                             <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                             <div class="sliding-pan location-span">
                                                <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                             </div>
                                          </div>
                                          <div class="profile-tip-name">
                                             <a href="javascript:void(0)">Adel Hasanat</a>
                                          </div>
                                          <div class="profile-tip-info">
                                             <div class="profiletip-icon">
                                                <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                             </div>
                                             <div class="profiletip-icon">
                                                <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                             </div>
                                             <div class="profiletip-icon">
                                                <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                             </div>
                                          </div>
                                       </div>
                                    </span>
                                 </div>
                              </div>
                              <div class="desc-holder">
                                 <div class="normal-mode">
                                    <div class="desc">
                                       <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                       <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                    </div>
                                    <div class="comment-stuff">
                                       <div class="more-opt">
                                          <span class="likeholder">
                                          <span class="like-tooltip">
                                          <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                          <span class="icon">icon</span>
                                          </a>
                                          </span>
                                          </span>
                                          <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><span>icon</span></a>
                                          <!-- Dropdown Trigger -->
                                          <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='drop_down84'>
                                          <i class="zmdi zmdi-more"></i>
                                          </a>
                                          <!-- Dropdown Structure -->
                                          <ul id='drop_down84' class='dropdown-content custom_dropdown'>
                                             <li>
                                                <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                             </li>
                                             <li>
                                                <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                             </li>
                                          </ul>
                                       </div>
                                       <div class="less-opt">
                                          <div class="timestamp">8h</div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="edit-mode">
                                    <div class="desc">
                                       <textarea class="editcomment-tt materialize-textarea" id="ec-14">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                       <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="clear"></div>
                           <div class="comment-reply-holder">
                              <div class="pcomment comment-reply">
                                 <div class="img-holder">
                                    <div class="profiletipholder">
                                       <span class="profile-tooltip">
                                       <img class="circle" src="images/demo-profile.jpg"/>
                                       </span>
                                       <span class="profiletooltip_content slidingpan-holder">
                                          <div class="profile-tip" style="display:none;">
                                             <div class="profile-tip-avatar">
                                                <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                                <div class="sliding-pan location-span">
                                                   <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                </div>
                                             </div>
                                             <div class="profile-tip-name">
                                                <a href="javascript:void(0)">Adel Hasanat</a>
                                             </div>
                                             <div class="profile-tip-info">
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                </div>
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                </div>
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                                </div>
                                             </div>
                                          </div>
                                       </span>
                                    </div>
                                 </div>
                                 <div class="desc-holder">
                                    <div class="normal-mode">
                                       <div class="desc">
                                          <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                          <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                       </div>
                                       <div class="comment-stuff">
                                          <div class="more-opt">
                                             <span class="likeholder">
                                             <span class="like-tooltip">
                                             <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                             <span class="icon">icon</span>
                                             </a>
                                             </span>
                                             </span>
                                             <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><span>icon</span></a>
                                             <!-- Dropdown Trigger -->
                                             <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='drop_down85'>
                                             <i class="zmdi zmdi-more"></i>
                                             </a>
                                             <!-- Dropdown Structure -->
                                             <ul id='drop_down85' class='dropdown-content custom_dropdown'>
                                                <li>
                                                   <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                                </li>
                                                <li>
                                                   <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                                </li>
                                             </ul>
                                          </div>
                                          <div class="less-opt">
                                             <div class="timestamp">8h</div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="edit-mode">
                                       <div class="desc">
                                          <textarea class="editcomment-tt materialize-textarea" id="ec-15">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                          <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="pcomment comment-reply">
                                 <div class="img-holder">
                                    <div class="profiletipholder">
                                       <span class="profile-tooltip">
                                       <img class="circle" src="images/demo-profile.jpg"/>
                                       </span>
                                       <span class="profiletooltip_content slidingpan-holder">
                                          <div class="profile-tip" style="display:none;">
                                             <div class="profile-tip-avatar">
                                                <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                                <div class="sliding-pan location-span">
                                                   <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                </div>
                                             </div>
                                             <div class="profile-tip-name">
                                                <a href="javascript:void(0)">Adel Hasanat</a>
                                             </div>
                                             <div class="profile-tip-info">
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                </div>
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                </div>
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                                </div>
                                             </div>
                                          </div>
                                       </span>
                                    </div>
                                 </div>
                                 <div class="desc-holder">
                                    <div class="normal-mode">
                                       <div class="desc">
                                          <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                          <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit...</p>
                                       </div>
                                       <div class="comment-stuff">
                                          <div class="more-opt">
                                             <span class="likeholder">
                                             <span class="like-tooltip">
                                             <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                             <span class="icon">icon</span>
                                             </a>
                                             </span>
                                             </span>
                                             <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><span>icon</span></a>
                                             <!-- Dropdown Trigger -->
                                             <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='drop_down86'>
                                             <i class="zmdi zmdi-more"></i>
                                             </a>
                                             <!-- Dropdown Structure -->
                                             <ul id='drop_down86' class='dropdown-content custom_dropdown'>
                                                <li>
                                                   <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                                </li>
                                                <li>
                                                   <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                                </li>
                                             </ul>
                                          </div>
                                          <div class="less-opt">
                                             <div class="timestamp">8h</div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="edit-mode">
                                       <div class="desc">
                                          <textarea class="editcomment-tt materialize-textarea" id="ec-16">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                          <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="comment-reply-holder comment-addreply">
                              <div class="addnew-comment valign-wrapper  comment-reply">
                                 <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                                 <div class="desc-holder">                          
                                    <textarea class="materialize-textarea">Write a reply...</textarea>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="pcomment-holder">
                           <div class="pcomment main-comment">
                              <div class="img-holder">
                                 <div class="profiletipholder">
                                    <span class="profile-tooltip">
                                    <img class="circle" src="images/demo-profile.jpg"/>
                                    </span>
                                    <span class="profiletooltip_content slidingpan-holder">
                                       <div class="profile-tip" style="display:none;">
                                          <div class="profile-tip-avatar">
                                             <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                             <div class="sliding-pan location-span">
                                                <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                             </div>
                                          </div>
                                          <div class="profile-tip-name">
                                             <a href="javascript:void(0)">Adel Hasanat</a>
                                          </div>
                                          <div class="profile-tip-info">
                                             <div class="profiletip-icon">
                                                <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                             </div>
                                             <div class="profiletip-icon">
                                                <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                             </div>
                                             <div class="profiletip-icon">
                                                <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                             </div>
                                          </div>
                                       </div>
                                    </span>
                                 </div>
                              </div>
                              <div class="desc-holder">
                                 <div class="normal-mode">
                                    <div class="desc">
                                       <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                       <p class="shorten">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.<a a="" href="javascript:void(0)" class="overlay" onclick="explandReadMore(this)" style="position: absolute;top: 0;bottom: -6px;left: 0;right: 0;"><span class="readlink">Read More</span></a></p>
                                    </div>
                                    <div class="comment-stuff">
                                       <div class="more-opt">
                                          <span class="likeholder">
                                          <span class="like-tooltip">
                                          <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                          <span class="icon">icon</span>
                                          </a>
                                          </span>
                                          </span>  
                                          <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><span>icon</span></a>
                                          <!-- Dropdown Trigger -->
                                          <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='drop_down87'>
                                          <i class="zmdi zmdi-more"></i>
                                          </a>
                                          <!-- Dropdown Structure -->
                                          <ul id='drop_down87' class='dropdown-content custom_dropdown'>
                                             <li>
                                                <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                             </li>
                                             <li>
                                                <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                             </li>
                                          </ul>
                                       </div>
                                       <div class="less-opt">
                                          <div class="timestamp">8h</div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="edit-mode">
                                    <div class="desc">
                                       <textarea class="editcomment-tt materialize-textarea" id="ec-17">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                       <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="clear"></div>
                           <div class="comment-reply-holder comment-addreply">
                              <div class="addnew-comment valign-wrapper  comment-reply">
                                 <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                                 <div class="desc-holder">                          
                                    <textarea class="materialize-textarea">Write a reply...</textarea>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="addnew-comment valign-wrapper  ">
                        <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                        <div class="desc-holder">        
                           <textarea class="materialize-textarea" id="comment_txt_3">Write a reply...</textarea>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <!-- end shared group -->
      </div>
      <div class="col s12 m12">
         <!-- shared collection -->
         <div class="post-holder">
            <div class="post-topbar">
               <div class="post-userinfo">
                  <div class="img-holder">
                     <div class="profiletipholder">
                        <span class="profile-tooltip">
                        <img class="circle" src="images/demo-profile.jpg"/>
                        </span>
                        <span class="profiletooltip_content slidingpan-holder">
                           <div class="profile-tip" style="display:none;">
                              <div class="profile-tip-avatar">
                                 <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                 <div class="sliding-pan location-span">
                                    <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                 </div>
                              </div>
                              <div class="profile-tip-name">
                                 <a href="javascript:void(0)">Adel Hasanat</a>
                              </div>
                              <div class="profile-tip-info">
                                 <div class="profiletip-icon">
                                    <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                 </div>
                                 <div class="profiletip-icon">
                                    <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                 </div>
                                 <div class="profiletip-icon">
                                    <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                 </div>
                              </div>
                           </div>
                        </span>
                     </div>
                  </div>
                  <div class="desc-holder">
                     <a href="javascript:void(0)">Nimish Parekh</a> shared <a href="javascript:void(0)" class="sub-link">Adel Hasanat</a>'s collection.
                     <span class="timestamp">2 hrs<span class="glyphicon glyphicon-globe"></span></span>
                  </div>
               </div> 
               <div class="settings-icon">
			   <div class="dropdown dropdown-custom dropdown-xxsmall">
				  <a class="dropdown-button more_btn" href="javascript:void(0);" data-activates="setting_btn15" style="">					
                  <i class="zmdi zmdi-more"></i>
				  </a>
				  <ul id="setting_btn15" class="dropdown-content custom_dropdown" ><li>
                        <a href="javascript:void(0)">Hide post</a>
                     </li>
                     <li>
                        <a href="javascript:void(0)" class="savepost-link">Save post</a>
                     </li>
                     <li>
                        <a href="javascript:void(0)">Mute notification for this post</a>
                     </li>
                     <li>
                        <a href="javascript:void(0)">Mute connect post</a>
                     </li>
                     <li>
                        <a href="javascript:void(0)">Report post</a>
                     </li>
                     <li>
                        <a href="javascript:void(0)" class="composeeditpostAction">Edit post</a>
                     </li>
				  </ul>				 
				</div>
                  <!-- Dropdown Structure -->
               </div>
            </div>
            <div class="post-content">
               <div class="pdetail-holder">
                  <div class="post-details">
                     <div class="post-desc">
                        <p>Nice flower collection</p>
                     </div>
                  </div>
                  <div class="shared-box shared-category">
                     <div class="post-holder">
                        <div class="post-content">
                           <div class="post-img-holder">
                              <div class="post-img one-img">
                                 <div class="pimg-holder"><img src="images/flowerscollection.jpg"/></div>
                              </div>
                           </div>
                           <div class="share-summery">
                              <div class="sharedpost-title"><a href="javascript:void(0)">Pretty Flowers</a></div>
                              <div class="sharedpost-tagline">the beautiful creation of nature!</div>
                              <div class="sharedpost-desc">345 followers</div>
                              <a href="javascript:void(0)" class="btn btn-primary btn-sm actionbtn waves-effect waves-light">Follow</a>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="clear"></div>
            <div class="post-data">
               <div class="post-actions">
                  <span class="likeholder">
                  <span class="like-tooltip">
                  <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                  <i class="zmdi zmdi-thumb-up"></i>
                  </a>
                  </span>
                  </span>
                  <span class="right">
                     <a href="javascript:void(0)" class="pa-comment" title="Comment" data-id="1"><i class="zmdi zmdi-comment"></i></a><span class="comment-lcount">4</span>
                  </span>
               </div>
               <div class="comments-section panel">
                  <div class="post-more">
                     <a href="javascript:void(0)" class="view-morec">View more comments <i class="zmdi zmdi-chevron-right"></i></a>
                     <span class="total-comments">3 of 7</span>
                  </div>
                  <div class="post-comments">
                     <div class="pcomments">
                        <div class="pcomment-earlier">
                           <div class="pcomment-holder">
                              <div class="pcomment main-comment">
                                 <div class="img-holder">
                                    <div class="profiletipholder">
                                       <span class="profile-tooltip">
                                       <img class="circle" src="images/demo-profile.jpg"/>
                                       </span>
                                       <span class="profiletooltip_content slidingpan-holder">
                                          <div class="profile-tip" style="display:none;">
                                             <div class="profile-tip-avatar">
                                                <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                                <div class="sliding-pan location-span">
                                                   <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                </div>
                                             </div>
                                             <div class="profile-tip-name">
                                                <a href="javascript:void(0)">Adel Hasanat</a>
                                             </div>
                                             <div class="profile-tip-info">
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                </div>
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                </div>
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                                </div>
                                             </div>
                                          </div>
                                       </span>
                                    </div>
                                 </div>
                                 <div class="desc-holder">
                                    <div class="normal-mode">
                                       <div class="desc">
                                          <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                          <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                       </div>
                                       <div class="comment-stuff">
                                          <div class="more-opt">
                                             <span class="likeholder">
                                             <span class="like-tooltip">
                                             <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                             <span class="icon">icon</span>
                                             </a>
                                             </span>
                                             </span>  
                                             <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><span>icon</span></a>
                                             <!-- Dropdown Trigger -->
                                             <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='drop_down88'>
                                             <i class="zmdi zmdi-more"></i>
                                             </a>
                                             <!-- Dropdown Structure -->
                                             <ul id='drop_down88' class='dropdown-content custom_dropdown'>
                                                <li>
                                                   <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                                </li>
                                                <li>
                                                   <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                                </li>
                                             </ul>
                                          </div>
                                          <div class="less-opt">
                                             <div class="timestamp">8h</div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="edit-mode">
                                       <div class="desc">
                                          <textarea class="editcomment-tt materialize-textarea" id="ec-12">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                          <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="clear"></div>
                              <div class="comment-reply-holder comment-addreply">
                                 <div class="addnew-comment valign-wrapper  comment-reply">
                                    <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                                    <div class="desc-holder">                          
                                       <textarea class="materialize-textarea">Write a reply...</textarea>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="pcomment-holder">
                              <div class="pcomment main-comment">
                                 <div class="img-holder">
                                    <div class="profiletipholder">
                                       <span class="profile-tooltip">
                                       <img class="circle" src="images/demo-profile.jpg"/>
                                       </span>
                                       <span class="profiletooltip_content slidingpan-holder">
                                          <div class="profile-tip" style="display:none;">
                                             <div class="profile-tip-avatar">
                                                <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                                <div class="sliding-pan location-span">
                                                   <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                </div>
                                             </div>
                                             <div class="profile-tip-name">
                                                <a href="javascript:void(0)">Adel Hasanat</a>
                                             </div>
                                             <div class="profile-tip-info">
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                </div>
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                </div>
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                                </div>
                                             </div>
                                          </div>
                                       </span>
                                    </div>
                                 </div>
                                 <div class="desc-holder">
                                    <div class="normal-mode">
                                       <div class="desc">
                                          <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                          <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                       </div>
                                       <div class="comment-stuff">
                                          <div class="more-opt">
                                             <span class="likeholder">
                                             <span class="like-tooltip">
                                             <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                             <span class="icon">icon</span>
                                             </a>
                                             </span>
                                             </span>  
                                             <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><span>icon</span></a>
                                             <!-- Dropdown Trigger -->
                                             <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='drop_down89'>
                                             <i class="zmdi zmdi-more"></i>
                                             </a>
                                             <!-- Dropdown Structure -->
                                             <ul id='drop_down89' class='dropdown-content custom_dropdown'>
                                                <li>
                                                   <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                                </li>
                                                <li>
                                                   <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                                </li>
                                             </ul>
                                          </div>
                                          <div class="less-opt">
                                             <div class="timestamp">8h</div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="edit-mode">
                                       <div class="desc">
                                          <textarea class="editcomment-tt materialize-textarea" id="ec-13">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                          <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="clear"></div>
                              <div class="comment-reply-holder comment-addreply">
                                 <div class="addnew-comment valign-wrapper  comment-reply">
                                    <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                                    <div class="desc-holder">                          
                                       <textarea class="materialize-textarea">Write a reply...</textarea>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="pcomment-holder has-comments">
                           <div class="pcomment main-comment">
                              <div class="img-holder">
                                 <div class="profiletipholder">
                                    <span class="profile-tooltip">
                                    <img class="circle" src="images/demo-profile.jpg"/>
                                    </span>
                                    <span class="profiletooltip_content slidingpan-holder">
                                       <div class="profile-tip" style="display:none;">
                                          <div class="profile-tip-avatar">
                                             <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                             <div class="sliding-pan location-span">
                                                <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                             </div>
                                          </div>
                                          <div class="profile-tip-name">
                                             <a href="javascript:void(0)">Adel Hasanat</a>
                                          </div>
                                          <div class="profile-tip-info">
                                             <div class="profiletip-icon">
                                                <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                             </div>
                                             <div class="profiletip-icon">
                                                <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                             </div>
                                             <div class="profiletip-icon">
                                                <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                             </div>
                                          </div>
                                       </div>
                                    </span>
                                 </div>
                              </div>
                              <div class="desc-holder">
                                 <div class="normal-mode">
                                    <div class="desc">
                                       <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                       <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                    </div>
                                    <div class="comment-stuff">
                                       <div class="more-opt">
                                          <span class="likeholder">
                                          <span class="like-tooltip">
                                          <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                          <span class="icon">icon</span>
                                          </a>
                                          </span>
                                          </span>
                                          <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><span>icon</span></a>
                                          <!-- Dropdown Trigger -->
                                          <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='drop_down90'>
                                          <i class="zmdi zmdi-more"></i>
                                          </a>
                                          <!-- Dropdown Structure -->
                                          <ul id='drop_down90' class='dropdown-content custom_dropdown'>
                                             <li>
                                                <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                             </li>
                                             <li>
                                                <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                             </li>
                                          </ul>
                                       </div>
                                       <div class="less-opt">
                                          <div class="timestamp">8h</div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="edit-mode">
                                    <div class="desc">
                                       <textarea class="editcomment-tt materialize-textarea" id="ec-14">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                       <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="clear"></div>
                           <div class="comment-reply-holder">
                              <div class="pcomment comment-reply">
                                 <div class="img-holder">
                                    <div class="profiletipholder">
                                       <span class="profile-tooltip">
                                       <img class="circle" src="images/demo-profile.jpg"/>
                                       </span>
                                       <span class="profiletooltip_content slidingpan-holder">
                                          <div class="profile-tip" style="display:none;">
                                             <div class="profile-tip-avatar">
                                                <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                                <div class="sliding-pan location-span">
                                                   <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                </div>
                                             </div>
                                             <div class="profile-tip-name">
                                                <a href="javascript:void(0)">Adel Hasanat</a>
                                             </div>
                                             <div class="profile-tip-info">
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                </div>
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                </div>
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                                </div>
                                             </div>
                                          </div>
                                       </span>
                                    </div>
                                 </div>
                                 <div class="desc-holder">
                                    <div class="normal-mode">
                                       <div class="desc">
                                          <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                          <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                       </div>
                                       <div class="comment-stuff">
                                          <div class="more-opt">
                                             <span class="likeholder">
                                             <span class="like-tooltip">
                                             <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                             <span class="icon">icon</span>
                                             </a>
                                             </span>
                                             </span>
                                             <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><span>icon</span></a>
                                             <!-- Dropdown Trigger -->
                                             <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='drop_down91'>
                                             <i class="zmdi zmdi-more"></i>
                                             </a>
                                             <!-- Dropdown Structure -->
                                             <ul id='drop_down91' class='dropdown-content custom_dropdown'>
                                                <li>
                                                   <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                                </li>
                                                <li>
                                                   <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                                </li>
                                             </ul>
                                          </div>
                                          <div class="less-opt">
                                             <div class="timestamp">8h</div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="edit-mode">
                                       <div class="desc">
                                          <textarea class="editcomment-tt materialize-textarea" id="ec-15">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                          <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="pcomment comment-reply">
                                 <div class="img-holder">
                                    <div class="profiletipholder">
                                       <span class="profile-tooltip">
                                       <img class="circle" src="images/demo-profile.jpg"/>
                                       </span>
                                       <span class="profiletooltip_content slidingpan-holder">
                                          <div class="profile-tip" style="display:none;">
                                             <div class="profile-tip-avatar">
                                                <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                                <div class="sliding-pan location-span">
                                                   <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                </div>
                                             </div>
                                             <div class="profile-tip-name">
                                                <a href="javascript:void(0)">Adel Hasanat</a>
                                             </div>
                                             <div class="profile-tip-info">
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                </div>
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                </div>
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                                </div>
                                             </div>
                                          </div>
                                       </span>
                                    </div>
                                 </div>
                                 <div class="desc-holder">
                                    <div class="normal-mode">
                                       <div class="desc">
                                          <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                          <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit...</p>
                                       </div>
                                       <div class="comment-stuff">
                                          <div class="more-opt">
                                             <span class="likeholder">
                                             <span class="like-tooltip">
                                             <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                             <span class="icon">icon</span>
                                             </a>
                                             </span>
                                             </span>
                                             <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><span>icon</span></a>
                                             <!-- Dropdown Trigger -->
                                             <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='drop_down92'>
                                             <i class="zmdi zmdi-more"></i>
                                             </a>
                                             <!-- Dropdown Structure -->
                                             <ul id='drop_down92' class='dropdown-content custom_dropdown'>
                                                <li>
                                                   <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                                </li>
                                                <li>
                                                   <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                                </li>
                                             </ul>
                                          </div>
                                          <div class="less-opt">
                                             <div class="timestamp">8h</div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="edit-mode">
                                       <div class="desc">
                                          <textarea class="editcomment-tt materialize-textarea" id="ec-16">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                          <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="comment-reply-holder comment-addreply">
                              <div class="addnew-comment valign-wrapper  comment-reply">
                                 <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                                 <div class="desc-holder">                          
                                    <textarea class="materialize-textarea">Write a reply...</textarea>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="pcomment-holder">
                           <div class="pcomment main-comment">
                              <div class="img-holder">
                                 <div class="profiletipholder">
                                    <span class="profile-tooltip">
                                    <img class="circle" src="images/demo-profile.jpg"/>
                                    </span>
                                    <span class="profiletooltip_content slidingpan-holder">
                                       <div class="profile-tip" style="display:none;">
                                          <div class="profile-tip-avatar">
                                             <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                             <div class="sliding-pan location-span">
                                                <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                             </div>
                                          </div>
                                          <div class="profile-tip-name">
                                             <a href="javascript:void(0)">Adel Hasanat</a>
                                          </div>
                                          <div class="profile-tip-info">
                                             <div class="profiletip-icon">
                                                <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                             </div>
                                             <div class="profiletip-icon">
                                                <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                             </div>
                                             <div class="profiletip-icon">
                                                <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                             </div>
                                          </div>
                                       </div>
                                    </span>
                                 </div>
                              </div>
                              <div class="desc-holder">
                                 <div class="normal-mode">
                                    <div class="desc">
                                       <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                       <p class="shorten">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.<a a="" href="javascript:void(0)" class="overlay" onclick="explandReadMore(this)" style="position: absolute;top: 0;bottom: -6px;left: 0;right: 0;"><span class="readlink">Read More</span></a></p>
                                    </div>
                                    <div class="comment-stuff">
                                       <div class="more-opt">
                                          <span class="likeholder">
                                          <span class="like-tooltip">
                                          <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                          <span class="icon">icon</span>
                                          </a>
                                          </span>
                                          </span>  
                                          <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><span>icon</span></a>
                                          <!-- Dropdown Trigger -->
                                          <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='drop_down93'>
                                          <i class="zmdi zmdi-more"></i>
                                          </a>
                                          <!-- Dropdown Structure -->
                                          <ul id='drop_down93' class='dropdown-content custom_dropdown'>
                                             <li>
                                                <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                             </li>
                                             <li>
                                                <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                             </li>
                                          </ul>
                                       </div>
                                       <div class="less-opt">
                                          <div class="timestamp">8h</div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="edit-mode">
                                    <div class="desc">
                                       <textarea class="editcomment-tt materialize-textarea" id="ec-17">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                       <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="clear"></div>
                           <div class="comment-reply-holder comment-addreply">
                              <div class="addnew-comment valign-wrapper  comment-reply">
                                 <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                                 <div class="desc-holder">                          
                                    <textarea class="materialize-textarea">Write a reply...</textarea>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="addnew-comment valign-wrapper  ">
                        <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                        <div class="desc-holder">                          
                           <textarea class="materialize-textarea" id="comment_txt_3">Write a reply...</textarea>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <!-- end shared collection -->
      </div>
      <div class="col s12 m12">
         <!-- main post -->
         <div class="post-holder">
            <div class="post-topbar">
               <div class="post-userinfo">
                  <div class="img-holder">
                     <div class="profiletipholder">
                        <span class="profile-tooltip">
                        <img class="circle" src="images/demo-profile.jpg"/>
                        </span>
                        <span class="profiletooltip_content slidingpan-holder">
                           <div class="profile-tip" style="display:none;">
                              <div class="profile-tip-avatar">
                                 <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                 <div class="sliding-pan location-span">
                                    <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                 </div>
                              </div>
                              <div class="profile-tip-name">
                                 <a href="javascript:void(0)">Adel Hasanat</a>
                              </div>
                              <div class="profile-tip-info">
                                 <div class="profiletip-icon">
                                    <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                 </div>
                                 <div class="profiletip-icon">
                                    <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                 </div>
                                 <div class="profiletip-icon">
                                    <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                 </div>
                              </div>
                           </div>
                        </span>
                     </div>
                  </div>
                  <div class="desc-holder">
                     <a href="javascript:void(0)">Nimish Parekh</a>
                     <span class="timestamp">2 hrs<span class="glyphicon glyphicon-globe"></span></span>
                  </div>
               </div> 
               <div class="settings-icon">
			   <div class="dropdown dropdown-custom dropdown-xxsmall">
				  <a class="dropdown-button more_btn" href="javascript:void(0);" data-activates="setting_btn16" style="">					
                  <i class="zmdi zmdi-more"></i>
				  </a>
				  <ul id="setting_btn16" class="dropdown-content custom_dropdown" ><li>
                        <a href="javascript:void(0)">Hide post</a>
                     </li>
                     <li>
                        <a href="javascript:void(0)" class="savepost-link">Save post</a>
                     </li>
                     <li>
                        <a href="javascript:void(0)">Mute notification for this post</a>
                     </li>
                     <li>
                        <a href="javascript:void(0)">Mute connect post</a>
                     </li>
                     <li>
                        <a href="javascript:void(0)">Report post</a>
                     </li>
                     <li>
                        <a href="javascript:void(0)" class="composeeditpostAction">Edit post</a>
                     </li>
				  </ul>				 
				</div>
                  <!-- Dropdown Structure -->
               </div>
            </div>
            <div class="post-content">
               <div class="pdetail-holder">
                  <div class="post-details">
                     <div class="post-title"><?=$st_nm_S?> is Nice</div>
                     <div class="post-desc">
                        <div class="para-section">
                           <div class="para">
                              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sed varius risus. Duis rhoncus eros et pellentesque imperdiet.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sed varius risus. Duis rhoncus eros et pellentesque imperdiet.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sed varius risus. Duis rhoncus eros et pellentesque imperdiet.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sed varius risus. Duis rhoncus eros et pellentesque imperdiet.
                                 <br /><br />
                                 Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sed varius risus. Duis rhoncus eros et pellentesque imperdiet.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sed varius risus. Duis rhoncus eros et pellentesque imperdiet.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sed varius risus. Duis rhoncus eros et pellentesque imperdiet.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sed varius risus. Duis rhoncus eros et pellentesque imperdiet. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sed varius risus. Duis rhoncus eros et pellentesque imperdiet.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sed varius risus. Duis rhoncus eros et pellentesque imperdiet.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sed varius risus. Duis rhoncus eros et pellentesque imperdiet.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sed varius risus. Duis rhoncus eros et pellentesque imperdiet.
                              </p>
                           </div>
                           <a href="javascript:void(0)" class="readlink">Read More</a>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="post-img-holder">
                  <div class="lgt-gallery post-img one-img">
                     <a href="images/post-img.jpg" data-size="1600x1600" data-med="images/post-img.jpg" data-med-size="1024x1024" data-author="Folkert Gorter" class="pimg-holder himg-box">
                     <img class="himg" src="images/post-img.jpg" alt="" />
                     </a>
                  </div>
                  <a href="javascript:void(0)" class="pinlink"><i class="mdi mdi-nature"></i></a>
               </div>
            </div>
            <div class="clear"></div>
            <div class="post-data">
               <div class="post-actions">
                  <span class="likeholder">
                  <span class="like-tooltip">
                  <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                  <i class="zmdi zmdi-thumb-up"></i>
                  </a>
                  </span>
                  </span>
                  <a href="javascript:void(0)" class="sharepostmodalAction pa-share" title="Share"><i class="zmdi zmdi-mail-reply zmdi-hc-flip-horizontal"></i></a>
                  <span class="right">
                     <a href="javascript:void(0)" class="pa-comment" title="Comment" data-id="1"><i class="zmdi zmdi-comment"></i></a><span class="comment-lcount">4</span>
                  </span>
               </div>
               <div class="comments-section panel">
                  <div class="post-more">
                     <a href="javascript:void(0)" class="view-morec">View more comments <i class="zmdi zmdi-chevron-right"></i></a>
                     <span class="total-comments">3 of 7</span>
                  </div>
                  <div class="post-comments">
                     <div class="pcomments">
                        <div class="pcomment-earlier">
                           <div class="pcomment-holder">
                              <div class="pcomment main-comment">
                                 <div class="img-holder">
                                    <div class="profiletipholder">
                                       <span class="profile-tooltip">
                                       <img class="circle" src="images/demo-profile.jpg"/>
                                       </span>
                                       <span class="profiletooltip_content slidingpan-holder">
                                          <div class="profile-tip" style="display:none;">
                                             <div class="profile-tip-avatar">
                                                <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                                <div class="sliding-pan location-span">
                                                   <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                </div>
                                             </div>
                                             <div class="profile-tip-name">
                                                <a href="javascript:void(0)">Adel Hasanat</a>
                                             </div>
                                             <div class="profile-tip-info">
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                </div>
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                </div>
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                                </div>
                                             </div>
                                          </div>
                                       </span>
                                    </div>
                                 </div>
                                 <div class="desc-holder">
                                    <div class="normal-mode">
                                       <div class="desc">
                                          <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                          <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                       </div>
                                       <div class="comment-stuff">
                                          <div class="more-opt">
                                             <span class="likeholder">
                                             <span class="like-tooltip">
                                             <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                             <span class="icon">icon</span>
                                             </a>
                                             </span>
                                             </span>
                                             <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><span>icon</span></a>
                                             <!-- Dropdown Trigger -->
                                             <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='drop_down94'>
                                             <i class="zmdi zmdi-more"></i>
                                             </a>
                                             <!-- Dropdown Structure -->
                                             <ul id='drop_down94' class='dropdown-content custom_dropdown'>
                                                <li>
                                                   <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                                </li>
                                                <li>
                                                   <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                                </li>
                                             </ul>
                                          </div>
                                          <div class="less-opt">
                                             <div class="timestamp">8h</div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="edit-mode">
                                       <div class="desc">
                                          <textarea class="editcomment-tt materialize-textarea" id="ec-1">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                          <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="clear"></div>
                              <div class="comment-reply-holder comment-addreply">
                                 <div class="addnew-comment valign-wrapper  comment-reply">
                                    <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                                    <div class="desc-holder">                          
                                       <textarea class="materialize-textarea">Write a reply...</textarea>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="pcomment-holder">
                              <div class="pcomment main-comment">
                                 <div class="img-holder">
                                    <div class="profiletipholder">
                                       <span class="profile-tooltip">
                                       <img class="circle" src="images/demo-profile.jpg"/>
                                       </span>
                                       <span class="profiletooltip_content slidingpan-holder">
                                          <div class="profile-tip" style="display:none;">
                                             <div class="profile-tip-avatar">
                                                <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                                <div class="sliding-pan location-span">
                                                   <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                </div>
                                             </div>
                                             <div class="profile-tip-name">
                                                <a href="javascript:void(0)">Adel Hasanat</a>
                                             </div>
                                             <div class="profile-tip-info">
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                </div>
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                </div>
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                                </div>
                                             </div>
                                          </div>
                                       </span>
                                    </div>
                                 </div>
                                 <div class="desc-holder">
                                    <div class="normal-mode">
                                       <div class="desc">
                                          <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                          <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                       </div>
                                       <div class="comment-stuff">
                                          <div class="more-opt">
                                             <span class="likeholder">
                                             <span class="like-tooltip">
                                             <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                             <span class="icon">icon</span>
                                             </a>
                                             </span>
                                             </span>  
                                             <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><span>icon</span></a>
                                             <!-- Dropdown Trigger -->
                                             <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='drop_down95'>
                                             <i class="zmdi zmdi-more"></i>
                                             </a>
                                             <!-- Dropdown Structure -->
                                             <ul id='drop_down95' class='dropdown-content custom_dropdown'>
                                                <li>
                                                   <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                                </li>
                                                <li>
                                                   <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                                </li>
                                             </ul>
                                          </div>
                                          <div class="less-opt">
                                             <div class="timestamp">8h</div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="edit-mode">
                                       <div class="desc">
                                          <textarea class="editcomment-tt materialize-textarea" id="ec-2">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                          <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="clear"></div>
                              <div class="comment-reply-holder comment-addreply">
                                 <div class="addnew-comment valign-wrapper  comment-reply">
                                    <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                                    <div class="desc-holder">                          
                                       <textarea class="materialize-textarea">Write a reply...</textarea>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="pcomment-holder has-comments">
                           <div class="pcomment main-comment">
                              <div class="img-holder">
                                 <div class="profiletipholder">
                                    <span class="profile-tooltip">
                                    <img class="circle" src="images/demo-profile.jpg"/>
                                    </span>
                                    <span class="profiletooltip_content slidingpan-holder">
                                       <div class="profile-tip" style="display:none;">
                                          <div class="profile-tip-avatar">
                                             <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                             <div class="sliding-pan location-span">
                                                <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                             </div>
                                          </div>
                                          <div class="profile-tip-name">
                                             <a href="javascript:void(0)">Adel Hasanat</a>
                                          </div>
                                          <div class="profile-tip-info">
                                             <div class="profiletip-icon">
                                                <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                             </div>
                                             <div class="profiletip-icon">
                                                <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                             </div>
                                             <div class="profiletip-icon">
                                                <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                             </div>
                                          </div>
                                       </div>
                                    </span>
                                 </div>
                              </div>
                              <div class="desc-holder">
                                 <div class="normal-mode">
                                    <div class="desc">
                                       <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                       <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                    </div>
                                    <div class="comment-stuff">
                                       <div class="more-opt">
                                          <span class="likeholder">
                                          <span class="like-tooltip">
                                          <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                          <span class="icon">icon</span>
                                          </a>
                                          </span>
                                          </span>
                                          <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><span>icon</span></a>
                                          <!-- Dropdown Trigger -->
                                          <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='drop_down96'>
                                          <i class="zmdi zmdi-more"></i>
                                          </a>
                                          <!-- Dropdown Structure -->
                                          <ul id='drop_down96' class='dropdown-content custom_dropdown'>
                                             <li>
                                                <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                             </li>
                                             <li>
                                                <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                             </li>
                                          </ul>
                                       </div>
                                       <div class="less-opt">
                                          <div class="timestamp">8h</div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="edit-mode">
                                    <div class="desc">
                                       <textarea class="editcomment-tt materialize-textarea" id="ec-3">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                       <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="clear"></div>
                           <div class="comment-reply-holder">
                              <div class="comments-reply-summery">
                                 <a href="javascript:void(0)" onclick="openReplies(this)">
                                 <i class="mdi mdi-share"></i>
                                 2 Replies                                       
                                 </a>
                                 <i class="mdi mdi-bullseye dot-i"></i>
                                 Just Now
                              </div>
                              <div class="comments-reply-details">
                                 <div class="pcomment comment-reply">
                                    <div class="img-holder">
                                       <div class="profiletipholder">
                                          <span class="profile-tooltip">
                                          <img class="circle" src="images/demo-profile.jpg"/>
                                          </span>
                                          <span class="profiletooltip_content">
                                             <div class="profile-tip" style="display:none;">
                                                <div class="slidingpan-holder">
                                                   <div class="profile-tip-avatar">
                                                      <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                                      <div class="sliding-pan location-span">
                                                         <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                      </div>
                                                   </div>
                                                   <div class="profile-tip-name">
                                                      <a href="javascript:void(0)">Adel Hasanat</a>
                                                   </div>
                                                   <div class="profile-tip-info">
                                                      <div class="profiletip-icon">
                                                         <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                      </div>
                                                      <div class="profiletip-icon">
                                                         <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                      </div>
                                                      <div class="profiletip-icon">
                                                         <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </span>
                                       </div>
                                    </div>
                                    <div class="desc-holder">
                                       <div class="normal-mode">
                                          <div class="desc">
                                             <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                             <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                          </div>
                                          <div class="comment-stuff">
                                             <div class="more-opt">
                                                <span class="likeholder">
                                                <span class="like-tooltip">
                                                <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                                <span class="icon">icon</span>
                                                </a>
                                                </span>
                                                </span>
                                                <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><span>icon</span></a>
                                                <!-- Dropdown Trigger -->
                                                <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='drop_down97'>
                                                <i class="zmdi zmdi-more"></i>
                                                </a>
                                                <!-- Dropdown Structure -->
                                                <ul id='drop_down97' class='dropdown-content custom_dropdown'>
                                                   <li>
                                                      <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                                   </li>
                                                   <li>
                                                      <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                                   </li>
                                                </ul>
                                             </div>
                                             <div class="less-opt">
                                                <div class="timestamp">8h</div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="edit-mode">
                                          <div class="desc">
                                             <textarea class="editcomment-tt materialize-textarea" id="ec-4">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                             <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="pcomment comment-reply">
                                    <div class="img-holder">
                                       <div class="profiletipholder">
                                          <span class="profile-tooltip">
                                          <img class="circle" src="images/demo-profile.jpg"/>
                                          </span>
                                          <span class="profiletooltip_content">
                                             <div class="profile-tip" style="display:none;">
                                                <div class="slidingpan-holder">
                                                   <div class="profile-tip-avatar">
                                                      <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                                      <div class="sliding-pan location-span">
                                                         <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                      </div>
                                                   </div>
                                                   <div class="profile-tip-name">
                                                      <a href="javascript:void(0)">Adel Hasanat</a>
                                                   </div>
                                                   <div class="profile-tip-info">
                                                      <div class="profiletip-icon">
                                                         <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                      </div>
                                                      <div class="profiletip-icon">
                                                         <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                      </div>
                                                      <div class="profiletip-icon">
                                                         <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </span>
                                       </div>
                                    </div>
                                    <div class="desc-holder">
                                       <div class="normal-mode">
                                          <div class="desc">
                                             <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                             <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit...</p>
                                          </div>
                                          <div class="comment-stuff">
                                             <div class="more-opt">
                                                <span class="likeholder">
                                                <span class="like-tooltip">
                                                <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                                <span class="icon">icon</span>
                                                </a>
                                                </span>
                                                </span>  
                                                <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><span>icon</span></a>
                                                <!-- Dropdown Trigger -->
                                                <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='drop_down98'>
                                                <i class="zmdi zmdi-more"></i>
                                                </a>
                                                <!-- Dropdown Structure -->
                                                <ul id='drop_down98' class='dropdown-content custom_dropdown'>
                                                   <li>
                                                      <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                                   </li>
                                                   <li>
                                                      <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                                   </li>
                                                </ul>
                                             </div>
                                             <div class="less-opt">
                                                <div class="timestamp">8h</div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="edit-mode">
                                          <div class="desc">
                                             <textarea class="editcomment-tt materialize-textarea" id="ec-5">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                             <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="comment-reply-holder comment-addreply">
                              <div class="addnew-comment valign-wrapper  comment-reply">
                                 <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                                 <div class="desc-holder">                          
                                    <textarea class="materialize-textarea">Write a reply...</textarea>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="pcomment-holder">
                           <div class="pcomment main-comment">
                              <div class="img-holder">
                                 <div class="profiletipholder">
                                    <span class="profile-tooltip">
                                    <img class="circle" src="images/demo-profile.jpg"/>
                                    </span>
                                    <span class="profiletooltip_content slidingpan-holder">
                                       <div class="profile-tip" style="display:none;">
                                          <div class="profile-tip-avatar">
                                             <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                             <div class="sliding-pan location-span">
                                                <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                             </div>
                                          </div>
                                          <div class="profile-tip-name">
                                             <a href="javascript:void(0)">Adel Hasanat</a>
                                          </div>
                                          <div class="profile-tip-info">
                                             <div class="profiletip-icon">
                                                <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                             </div>
                                             <div class="profiletip-icon">
                                                <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                             </div>
                                             <div class="profiletip-icon">
                                                <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                             </div>
                                          </div>
                                       </div>
                                    </span>
                                 </div>
                              </div>
                              <div class="desc-holder">
                                 <div class="normal-mode">
                                    <div class="desc">
                                       <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                       <p class="shorten">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.<a a="" href="javascript:void(0)" class="overlay" onclick="explandReadMore(this)" style="position: absolute;top: 0;bottom: -6px;left: 0;right: 0;"><span class="readlink">Read More</span></a></p>
                                    </div>
                                    <div class="comment-stuff">
                                       <div class="more-opt">
                                          <span class="likeholder">
                                          <span class="like-tooltip">
                                          <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                          <span class="icon">icon</span>
                                          </a>
                                          </span>
                                          </span>  
                                          <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><span>icon</span></a>
                                          <!-- Dropdown Trigger -->
                                          <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='drop_down99'>
                                          <i class="zmdi zmdi-more"></i>
                                          </a>
                                          <!-- Dropdown Structure -->
                                          <ul id='drop_down99' class='dropdown-content custom_dropdown'>
                                             <li>
                                                <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                             </li>
                                             <li>
                                                <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                             </li>
                                          </ul>
                                       </div>
                                       <div class="less-opt">
                                          <div class="timestamp">8h</div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="edit-mode">
                                    <div class="desc">
                                       <textarea class="editcomment-tt materialize-textarea" id="ec-6">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                       <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="clear"></div>
                           <div class="comment-reply-holder comment-addreply">
                              <div class="addnew-comment valign-wrapper  comment-reply">
                                 <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                                 <div class="desc-holder">                          
                                    <textarea class="materialize-textarea">Write a reply...</textarea>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="addnew-comment valign-wrapper  ">
                        <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                        <div class="desc-holder">                          
                           <textarea class="materialize-textarea" id="comment_txt_1">Write a reply...</textarea>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="col s12 m12">
         <div class="post-holder">
            <div class="post-topbar">
               <div class="post-userinfo">
                  <div class="img-holder">
                     <div class="profiletipholder">
                        <span class="profile-tooltip">
                        <img class="circle" src="images/demo-profile.jpg"/>
                        </span>
                        <span class="profiletooltip_content slidingpan-holder">
                           <div class="profile-tip" style="display:none;">
                              <div class="profile-tip-avatar">
                                 <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                 <div class="sliding-pan location-span">
                                    <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                 </div>
                              </div>
                              <div class="profile-tip-name">
                                 <a href="javascript:void(0)">Adel Hasanat</a>
                              </div>
                              <div class="profile-tip-info">
                                 <div class="profiletip-icon">
                                    <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                 </div>
                                 <div class="profiletip-icon">
                                    <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                 </div>
                                 <div class="profiletip-icon">
                                    <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                 </div>
                              </div>
                           </div>
                        </span>
                     </div>
                  </div>
                  <div class="desc-holder">
                     <a href="javascript:void(0)">Nimish Parekh</a>
                     <span class="timestamp">1 hr
                        <a class="dropdown-button" href="javascript:void(0);" data-activates="ddlPrivacy">
                           <i class="mdi mdi-lock mdi-13px"></i>
                           <i class="mdi mdi-menu-down"></i>
                       </a>
                        <ul id="ddlPrivacy" class="dropdown-content">
                           <li>
                              <a href="javascript:void(0)">
                              Private
                              </a>
                           </li>
                           <li>
                              <a href="javascript:void(0)">
                              Connections
                              </a>
                           </li>
                           <li>
                              <a href="javascript:void(0)">
                              Public
                              </a>
                           </li>
                       </ul>
                     </span>
                  </div>
               </div> 
               <div class="settings-icon">
      			   <div class="dropdown dropdown-custom dropdown-xxsmall">
      				  <a class="dropdown-button more_btn" href="javascript:void(0);" data-activates="setting_btn17" style="">					
                        <i class="zmdi zmdi-more"></i>
      				  </a>
      				  <ul id="setting_btn17" class="dropdown-content custom_dropdown" ><li>
                           <a href="javascript:void(0)">Hide post</a>
                        </li>
                        <li>
                           <a href="javascript:void(0)" class="savepost-link">Save post</a>
                        </li>
                        <li>
                           <a href="javascript:void(0)">Mute notification for this post</a>
                        </li>
                        <li>
                           <a href="javascript:void(0)">Mute connect post</a>
                        </li>
                        <li>
                           <a href="javascript:void(0)">Report post</a>
                        </li>
                        <li>
                           <a href="javascript:void(0)" class="composeeditpostAction">Edit post</a>
                        </li>
      				  </ul>				 
      				</div>
                  <!-- Dropdown Structure -->
               </div>               
            </div>
            <div class="post-content">
               <div class="pdetail-holder">
                  <div class="post-details">
                     <div class="post-title">Must Watch!</div>
                     <div class="post-desc">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sed varius risus. Duis rhoncus eros et pellentesque imperdiet. Praesent pharetra rutrum eros. In nec nulla id enim faucibus pellentesque quis at dolor. Aenean a cursus quam, et fringilla tellus. Vivamus eu lorem est. Donec non urna sit amet arcu dictum dapibus.</p>
                     </div>
                  </div>
                  <div class="pvideo-holder">
                     <div class="img-holder"><img src="images/vimg.jpg"/></div>
                     <div class="desc-holder">
                        <h4><a href="javascript:void(0)">A simple way to break a bad habit | Judson Brewer</a></h4>
                        <p>
                           Can we break bad habits by being more curious about them? Psychiatrist Judson Brewer studies the relationship between mindfulness and addiction — from smokin...
                        </p>
                     </div>
                  </div>
               </div>
            </div>
            <div class="clear"></div>
            <div class="post-data">
               <div class="post-actions">
                  <span class="likeholder">
                  <span class="like-tooltip">
                  <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                  <i class="zmdi zmdi-thumb-up"></i>
                  </a>
                  </span>
                  </span>
                  <a href="javascript:void(0)" class="sharepostmodalAction pa-share" title="Share"><i class="zmdi zmdi-mail-reply zmdi-hc-flip-horizontal"></i></a>
                  <span class="right">
                     <a href="javascript:void(0)" class="pa-comment" title="Comment" data-id="1"><i class="zmdi zmdi-comment"></i></a><span class="comment-lcount">4</span>
                  </span>
               </div>
               <div class="comments-section panel">
                  <div class="post-more">
                     <a href="javascript:void(0)" class="view-morec">View more comments <i class="zmdi zmdi-chevron-right"></i></a>
                     <span class="total-comments">3 of 7</span>
                  </div>
                  <div class="post-comments">
                     <div class="pcomments">
                        <div class="pcomment-earlier">
                           <div class="pcomment-holder">
                              <div class="pcomment main-comment">
                                 <div class="img-holder">
                                    <div class="profiletipholder">
                                       <span class="profile-tooltip">
                                       <img class="circle" src="images/demo-profile.jpg"/>
                                       </span>
                                       <span class="profiletooltip_content slidingpan-holder">
                                          <div class="profile-tip" style="display:none;">
                                             <div class="profile-tip-avatar">
                                                <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                                <div class="sliding-pan location-span">
                                                   <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                </div>
                                             </div>
                                             <div class="profile-tip-name">
                                                <a href="javascript:void(0)">Adel Hasanat</a>
                                             </div>
                                             <div class="profile-tip-info">
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                </div>
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                </div>
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                                </div>
                                             </div>
                                          </div>
                                       </span>
                                    </div>
                                 </div>
                                 <div class="desc-holder">
                                    <div class="normal-mode">
                                       <div class="desc">
                                          <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                          <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                       </div>
                                       <div class="comment-stuff">
                                          <div class="more-opt">
                                             <span class="likeholder">
                                             <span class="like-tooltip">
                                             <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                             <span class="icon">icon</span>
                                             </a>
                                             </span>
                                             </span>  
                                             <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><span>icon</span></a>
                                             <!-- Dropdown Trigger -->
                                             <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='drop_down100'>
                                             <i class="zmdi zmdi-more"></i>
                                             </a>
                                             <!-- Dropdown Structure -->
                                             <ul id='drop_down100' class='dropdown-content custom_dropdown'>
                                                <li>
                                                   <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                                </li>
                                                <li>
                                                   <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                                </li>
                                             </ul>
                                          </div>
                                          <div class="less-opt">
                                             <div class="timestamp">8h</div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="edit-mode">
                                       <div class="desc">
                                          <textarea class="editcomment-tt materialize-textarea" id="ec-7">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                          <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="clear"></div>
                              <div class="comment-reply-holder comment-addreply">
                                 <div class="addnew-comment valign-wrapper  comment-reply">
                                    <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                                    <div class="desc-holder">                          
                                       <textarea class="materialize-textarea">Write a reply...</textarea>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="pcomment-holder">
                              <div class="pcomment main-comment">
                                 <div class="img-holder">
                                    <div class="profiletipholder">
                                       <span class="profile-tooltip">
                                       <img class="circle" src="images/demo-profile.jpg"/>
                                       </span>
                                       <span class="profiletooltip_content slidingpan-holder">
                                          <div class="profile-tip" style="display:none;">
                                             <div class="profile-tip-avatar">
                                                <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                                <div class="sliding-pan location-span">
                                                   <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                </div>
                                             </div>
                                             <div class="profile-tip-name">
                                                <a href="javascript:void(0)">Adel Hasanat</a>
                                             </div>
                                             <div class="profile-tip-info">
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                </div>
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                </div>
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                                </div>
                                             </div>
                                          </div>
                                       </span>
                                    </div>
                                 </div>
                                 <div class="desc-holder">
                                    <div class="normal-mode">
                                       <div class="desc">
                                          <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                          <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                       </div>
                                       <div class="comment-stuff">
                                          <div class="more-opt">
                                             <span class="likeholder">
                                             <span class="like-tooltip">
                                             <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                             <span class="icon">icon</span>
                                             </a>
                                             </span>
                                             </span>  
                                             <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><span>icon</span></a>
                                             <!-- Dropdown Trigger -->
                                             <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='drop_down101'>
                                             <i class="zmdi zmdi-more"></i>
                                             </a>
                                             <!-- Dropdown Structure -->
                                             <ul id='drop_down101' class='dropdown-content custom_dropdown'>
                                                <li>
                                                   <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                                </li>
                                                <li>
                                                   <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                                </li>
                                             </ul>
                                          </div>
                                          <div class="less-opt">
                                             <div class="timestamp">8h</div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="edit-mode">
                                       <div class="desc">
                                          <textarea class="editcomment-tt materialize-textarea" id="ec-8">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                          <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="clear"></div>
                              <div class="comment-reply-holder comment-addreply">
                                 <div class="addnew-comment valign-wrapper  comment-reply">
                                    <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                                    <div class="desc-holder">                          
                                       <textarea class="materialize-textarea">Write a reply...</textarea>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="pcomment-holder">
                           <div class="pcomment main-comment">
                              <div class="img-holder">
                                 <div class="profiletipholder">
                                    <span class="profile-tooltip">
                                    <img class="circle" src="images/demo-profile.jpg"/>
                                    </span>
                                    <span class="profiletooltip_content slidingpan-holder">
                                       <div class="profile-tip" style="display:none;">
                                          <div class="profile-tip-avatar">
                                             <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                             <div class="sliding-pan location-span">
                                                <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                             </div>
                                          </div>
                                          <div class="profile-tip-name">
                                             <a href="javascript:void(0)">Adel Hasanat</a>
                                          </div>
                                          <div class="profile-tip-info">
                                             <div class="profiletip-icon">
                                                <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                             </div>
                                             <div class="profiletip-icon">
                                                <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                             </div>
                                             <div class="profiletip-icon">
                                                <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                             </div>
                                          </div>
                                       </div>
                                    </span>
                                 </div>
                              </div>
                              <div class="desc-holder">
                                 <div class="normal-mode">
                                    <div class="desc">
                                       <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                       <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                    </div>
                                    <div class="comment-stuff">
                                       <div class="more-opt">
                                          <span class="likeholder">
                                          <span class="like-tooltip">
                                          <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                          <span class="icon">icon</span>
                                          </a>
                                          </span>
                                          </span>  
                                          <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><span>icon</span></a>
                                          <!-- Dropdown Trigger -->
                                          <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='drop_down102'>
                                          <i class="zmdi zmdi-more"></i>
                                          </a>
                                          <!-- Dropdown Structure -->
                                          <ul id='drop_down102' class='dropdown-content custom_dropdown'>
                                             <li>
                                                <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                             </li>
                                             <li>
                                                <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                             </li>
                                          </ul>
                                       </div>
                                       <div class="less-opt">
                                          <div class="timestamp">8h</div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="edit-mode">
                                    <div class="desc">
                                       <textarea class="editcomment-tt materialize-textarea" id="ec-9">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                       <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="clear"></div>
                           <div class="comment-reply-holder comment-addreply">
                              <div class="addnew-comment valign-wrapper  comment-reply">
                                 <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                                 <div class="desc-holder">                          
                                    <textarea class="materialize-textarea">Write a reply...</textarea>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="pcomment-holder">
                           <div class="pcomment main-comment">
                              <div class="img-holder">
                                 <div class="profiletipholder">
                                    <span class="profile-tooltip">
                                    <img class="circle" src="images/demo-profile.jpg"/>
                                    </span>
                                    <span class="profiletooltip_content slidingpan-holder">
                                       <div class="profile-tip" style="display:none;">
                                          <div class="profile-tip-avatar">
                                             <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                             <div class="sliding-pan location-span">
                                                <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                             </div>
                                          </div>
                                          <div class="profile-tip-name">
                                             <a href="javascript:void(0)">Adel Hasanat</a>
                                          </div>
                                          <div class="profile-tip-info">
                                             <div class="profiletip-icon">
                                                <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                             </div>
                                             <div class="profiletip-icon">
                                                <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                             </div>
                                             <div class="profiletip-icon">
                                                <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                             </div>
                                          </div>
                                       </div>
                                    </span>
                                 </div>
                              </div>
                              <div class="desc-holder">
                                 <div class="normal-mode">
                                    <div class="desc">
                                       <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                       <p class="shorten">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.<a a="" href="javascript:void(0)" class="overlay" onclick="explandReadMore(this)" style="position: absolute;top: 0;bottom: -6px;left: 0;right: 0;"><span class="readlink">Read More</span></a></p>
                                    </div>
                                    <div class="comment-stuff">
                                       <div class="more-opt">
                                          <span class="likeholder">
                                          <span class="like-tooltip">
                                          <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                          <span class="icon">icon</span>
                                          </a>
                                          </span>
                                          </span>  
                                          <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><span>icon</span></a>
                                          <!-- Dropdown Trigger -->
                                          <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='drop_down103'>
                                          <i class="zmdi zmdi-more"></i>
                                          </a>
                                          <!-- Dropdown Structure -->
                                          <ul id='drop_down103' class='dropdown-content custom_dropdown'>
                                             <li>
                                                <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                             </li>
                                             <li>
                                                <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                             </li>
                                          </ul>
                                       </div>
                                       <div class="less-opt">
                                          <div class="timestamp">8h</div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="edit-mode">
                                    <div class="desc">
                                       <textarea class="editcomment-tt materialize-textarea" id="ec-10">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                       <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="clear"></div>
                           <div class="comment-reply-holder comment-addreply">
                              <div class="addnew-comment valign-wrapper  comment-reply">
                                 <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                                 <div class="desc-holder">                          
                                    <textarea class="materialize-textarea">Write a reply...</textarea>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="addnew-comment valign-wrapper  ">
                        <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                        <div class="desc-holder">                          
                           <textarea class="materialize-textarea">Write a reply...</textarea>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="col s12 m12">
         <div class="post-holder">
            <div class="post-topbar">
               <div class="post-userinfo">
                  <div class="img-holder">
                     <div class="profiletipholder">
                        <span class="profile-tooltip">
                        <img class="circle" src="images/demo-profile.jpg"/>
                        </span>
                        <span class="profiletooltip_content">
                           <div class="profile-tip" style="display:none;">
                              <div class="profile-tip-avatar">
                                 <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                 <div class="sliding-pan location-span">
                                    <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                 </div>
                              </div>
                              <div class="profile-tip-name">
                                 <a href="javascript:void(0)">Adel Hasanat</a>
                              </div>
                              <div class="profile-tip-info">
                                 <div class="profiletip-icon">
                                    <a href="javascript:void(0)" class="title-tooltip" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                 </div>
                                 <div class="profiletip-icon">
                                    <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                 </div>
                                 <div class="profiletip-icon">
                                    <a href="javascript:void(0)" class="title-tooltip" title="Web Developer"><span class="ptip-icon"><i class="mdi mdi-view-grid"></i></span></a>
                                 </div>
                              </div>
                           </div>
                        </span>
                     </div>
                  </div>
                  <div class="desc-holder">
                     <a href="javascript:void(0)">Nimish Parekh</a> with1 <a href="javascript:void(0)" class="sub-link">Smith Patel</a> and
                     <span class="likeholder">
                     <span class="like-tooltip">
                     <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name">
                     3 others
                     </a>
                     </span>
                     </span>
                     at Ahmedabad
                     <span class="timestamp">2 hrs<span class="glyphicon glyphicon-globe"></span></span>
                  </div>
               </div> 
               <div class="settings-icon">
			   <div class="dropdown dropdown-custom dropdown-xxsmall">
				  <a class="dropdown-button more_btn" href="javascript:void(0);" data-activates="setting_btn18" style="">					
                  <i class="zmdi zmdi-more"></i>
				  </a>
				  <ul id="setting_btn18" class="dropdown-content custom_dropdown" ><li>
                        <a href="javascript:void(0)">Hide post</a>
                     </li>
                     <li>
                        <a href="javascript:void(0)" class="savepost-link">Save post</a>
                     </li>
                     <li>
                        <a href="javascript:void(0)">Mute notification for this post</a>
                     </li>
                     <li>
                        <a href="javascript:void(0)">Mute connect post</a>
                     </li>
                     <li>
                        <a href="javascript:void(0)">Report post</a>
                     </li>
                     <li>
                        <a href="javascript:void(0)" class="composeeditpostAction">Edit post</a>
                     </li>
				  </ul>				 
				</div>
                  <!-- Dropdown Structure -->
               </div>               
            </div>
            <div class="post-content">
               <div class="pdetail-holder">
                  <div class="post-details">
                     <div class="post-title"></div>
                     <div class="post-desc">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sed varius risus. Duis rhoncus eros et pellentesque imperdiet. Praesent pharetra rutrum eros. In nec nulla id enim.</p>
                     </div>
                  </div>
               </div>
               <div class="post-img-holder">
                  <div class="lgt-gallery post-img two-img">
                     <a href="images/post-img2.jpg" data-size="1600x1600" data-med="images/post-img2.jpg" data-med-size="1024x1024" data-author="Folkert Gorter" class="pimg-holder himg-box">
                     <img class="himg" src="images/post-img2.jpg" alt="" />
                     </a>
                     <a href="images/post-img3.jpg" data-size="1600x1068" data-med="images/post-img3.jpg" data-med-size="1024x683" data-author="Samuel Rohl" class="pimg-holder vimg-box">
                     <img class="vimg" src="images/post-img3.jpg" alt="" />
                     </a>
                  </div>
               </div>
            </div>
            <div class="clear"></div>
            <div class="post-data">
               <div class="post-actions">
                  <span class="likeholder">
                  <span class="like-tooltip">
                  <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                  <i class="zmdi zmdi-thumb-up"></i>
                  </a>
                  </span>
                  </span>
                  <a href="javascript:void(0)" class="sharepostmodalAction pa-share" title="Share"><i class="zmdi zmdi-mail-reply zmdi-hc-flip-horizontal"></i></a>
                  <span class="right">
                     <a href="javascript:void(0)" class="pa-comment" title="Comment" data-id="1"><i class="zmdi zmdi-comment"></i></a><span class="comment-lcount">4</span>
                  </span>
               </div>
               <div class="comments-section panel">
                  <div class="post-more">                         
                     <span class="total-comments">1 of 1</span>
                  </div>
                  <div class="post-comments">
                     <div class="pcomments">
                        <div class="pcomment-holder">
                           <div class="pcomment main-comment">
                              <div class="img-holder">
                                 <div class="profiletipholder">
                                    <span class="profile-tooltip">
                                    <img class="circle" src="images/demo-profile.jpg"/>
                                    </span>
                                    <span class="profiletooltip_content">
                                       <div class="profile-tip" style="display:none;">
                                          <div class="profile-tip-avatar">
                                             <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                             <div class="sliding-pan location-span">
                                                <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                             </div>
                                          </div>
                                          <div class="profile-tip-name">
                                             <a href="javascript:void(0)">Adel Hasanat</a>
                                          </div>
                                          <div class="profile-tip-info">
                                             <div class="profiletip-icon">
                                                <a href="javascript:void(0)" class="title-tooltip" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                             </div>
                                             <div class="profiletip-icon">
                                                <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                             </div>
                                             <div class="profiletip-icon">
                                                <a href="javascript:void(0)" class="title-tooltip" title="Web Developer"><span class="ptip-icon"><i class="mdi mdi-view-grid"></i></span></a>
                                             </div>
                                          </div>
                                       </div>
                                    </span>
                                 </div>
                              </div>
                              <div class="desc-holder">
                                 <div class="normal-mode">
                                    <div class="desc">
                                       <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                       <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                    </div>
                                    <div class="comment-stuff">
                                       <div class="more-opt">
                                          <span class="likeholder">
                                          <span class="like-tooltip">
                                          <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                          <span class="icon">icon</span>
                                          </a>
                                          </span>
                                          </span>  
                                          <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><span>icon</span></a>
                                          <!-- Dropdown Trigger -->
                                          <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='drop_down104'>
                                          <i class="zmdi zmdi-more"></i>
                                          </a>
                                          <!-- Dropdown Structure -->
                                          <ul id='drop_down104' class='dropdown-content custom_dropdown'>
                                             <li>
                                                <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                             </li>
                                             <li>
                                                <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                             </li>
                                          </ul>
                                       </div>
                                       <div class="less-opt">
                                          <div class="timestamp">8h</div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="edit-mode">
                                    <div class="desc">
                                       <textarea class="editcomment-tt materialize-textarea" id="ec-11">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                       <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="clear"></div>
                           <div class="comment-reply-holder comment-addreply">
                              <div class="addnew-comment valign-wrapper  comment-reply">
                                 <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                                 <div class="desc-holder">                          
                                    <textarea class="materialize-textarea">Write a reply...</textarea>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="addnew-comment valign-wrapper  ">
                        <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                        <div class="desc-holder">                          
                           <textarea class="materialize-textarea" id="comment_txt_2">Write a reply...</textarea>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="col s12 m12">
         <!-- shared -->
         <div class="post-holder">
            <div class="post-topbar">
               <div class="post-userinfo">
                  <div class="img-holder">
                     <div class="profiletipholder">
                        <span class="profile-tooltip">
                        <img class="circle" src="images/demo-profile.jpg"/>
                        </span>
                        <span class="profiletooltip_content slidingpan-holder">
                           <div class="profile-tip" style="display:none;">
                              <div class="profile-tip-avatar">
                                 <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                 <div class="sliding-pan location-span">
                                    <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                 </div>
                              </div>
                              <div class="profile-tip-name">
                                 <a href="javascript:void(0)">Adel Hasanat</a>
                              </div>
                              <div class="profile-tip-info">
                                 <div class="profiletip-icon">
                                    <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                 </div>
                                 <div class="profiletip-icon">
                                    <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                 </div>
                                 <div class="profiletip-icon">
                                    <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                 </div>
                              </div>
                           </div>
                        </span>
                     </div>
                  </div>
                  <div class="desc-holder">
                     <a href="javascript:void(0)">Nimish Parekh</a>
                     <div class="user-to">
                        <i class="mdi mdi-menu-right"></i>

                        <a href="javascript:void(0)">Adel Hasanat</a>
                     </div>
                     <div class="extra-info">
                        -- at <a href="javascript:void(0)">New York</a> with <a href="javascript:void(0)">Alap Shah</a> and <a href="javascript:void(0)">4 others</a>
                     </div>
                     <span class="timestamp">2 hrs<span class="glyphicon glyphicon-globe"></span></span>
                  </div>
               </div> 
               <div class="settings-icon">
			   <div class="dropdown dropdown-custom dropdown-xxsmall">
				  <a class="dropdown-button more_btn" href="javascript:void(0);" data-activates="setting_btn19" style="">					
                  <i class="zmdi zmdi-more"></i>
				  </a>
				  <ul id="setting_btn19" class="dropdown-content custom_dropdown" ><li>
                        <a href="javascript:void(0)">Hide post</a>
                     </li>
                     <li>
                        <a href="javascript:void(0)" class="savepost-link">Save post</a>
                     </li>
                     <li>
                        <a href="javascript:void(0)">Mute notification for this post</a>
                     </li>
                     <li>
                        <a href="javascript:void(0)">Mute connect post</a>
                     </li>
                     <li>
                        <a href="javascript:void(0)">Report post</a>
                     </li>
                     <li>
                        <a href="javascript:void(0)" class="composeeditpostAction">Edit post</a>
                     </li>
				  </ul>				 
				</div>
                  <!-- Dropdown Structure -->
               </div>               
            </div>
            <div class="post-content">
               <div class="pdetail-holder">
                  <div class="post-details">
                     <div class="post-desc">
                        <p>Worth visiting!</p>
                     </div>
                  </div>
                  <div class="shared-box">
                     <div class="post-holder">
                        <div class="post-topbar">
                           <div class="post-userinfo">
                              <div class="img-holder">
                                 <div class="profiletipholder">
                                    <span class="profile-tooltip">
                                    <img class="circle" src="images/demo-profile.jpg"/>
                                    </span>
                                    <span class="profiletooltip_content slidingpan-holder">
                                       <div class="profile-tip" style="display:none;">
                                          <div class="profile-tip-avatar">
                                             <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                             <div class="sliding-pan location-span">
                                                <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                             </div>
                                          </div>
                                          <div class="profile-tip-name">
                                             <a href="javascript:void(0)">Adel Hasanat</a>
                                          </div>
                                          <div class="profile-tip-info">
                                             <div class="profiletip-icon">
                                                <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                             </div>
                                             <div class="profiletip-icon">
                                                <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                             </div>
                                             <div class="profiletip-icon">
                                                <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                             </div>
                                          </div>
                                       </div>
                                    </span>
                                 </div>
                              </div>
                              <div class="desc-holder">
                                 <a href="javascript:void(0)">Nimish Parekh</a>
                                 <span class="timestamp">2 hrs<span class="glyphicon glyphicon-globe"></span></span>
                              </div>
                           </div>
                        </div>
                        <div class="post-content">
                           <div class="post-details">
                              <div class="post-title"><?=$st_nm_S?> is Nice</div>
                              <div class="post-desc">
                                 <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sed varius risus. Duis rhoncus eros et pellentesque imperdiet. Praesent pharetra rutrum eros. In nec nulla id enim faucibus pellentesque quis at dolor. Aenean a cursus quam, et fringilla tellus. Vivamus eu lorem est. Donec non urna sit amet arcu dictum dapibus.</p>
                              </div>
                           </div>
                           <div class="post-img-holder">
                              <div class="lgt-gallery post-img one-img">
                                 <a href="images/post-img.jpg" data-size="1600x1600" data-med="images/post-img.jpg" data-med-size="1024x1024" data-author="Folkert Gorter" class="pimg-holder himg-box">
                                 <img class="himg" src="images/post-img.jpg" alt="" />
                                 </a>
                              </div>
                              <a href="javascript:void(0)" class="pinlink"><i class="mdi mdi-nature"></i></a>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="clear"></div>
            <div class="post-data">
               <div class="post-actions">
                  <span class="likeholder">
                  <span class="like-tooltip">
                  <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                  <i class="zmdi zmdi-thumb-up"></i>
                  </a>
                  </span>
                  </span>
                  <a href="javascript:void(0)" class="sharepostmodalAction pa-share" title="Share"><i class="zmdi zmdi-mail-reply zmdi-hc-flip-horizontal"></i></a>
                  <span class="right">
                     <a href="javascript:void(0)" class="pa-comment" title="Comment" data-id="1"><i class="zmdi zmdi-comment"></i></a><span class="comment-lcount">4</span>
                  </span>
               </div>
               <div class="comments-section panel">
                  <div class="post-more">
                     <a href="javascript:void(0)" class="view-morec">View more comments <i class="zmdi zmdi-chevron-right"></i></a>
                     <span class="total-comments">3 of 7</span>
                  </div>
                  <div class="post-comments">
                     <div class="pcomments">
                        <div class="pcomment-earlier">
                           <div class="pcomment-holder">
                              <div class="pcomment main-comment">
                                 <div class="img-holder">
                                    <div class="profiletipholder">
                                       <span class="profile-tooltip">
                                       <img class="circle" src="images/demo-profile.jpg"/>
                                       </span>
                                       <span class="profiletooltip_content slidingpan-holder">
                                          <div class="profile-tip" style="display:none;">
                                             <div class="profile-tip-avatar">
                                                <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                                <div class="sliding-pan location-span">
                                                   <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                </div>
                                             </div>
                                             <div class="profile-tip-name">
                                                <a href="javascript:void(0)">Adel Hasanat</a>
                                             </div>
                                             <div class="profile-tip-info">
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                </div>
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                </div>
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                                </div>
                                             </div>
                                          </div>
                                       </span>
                                    </div>
                                 </div>
                                 <div class="desc-holder">
                                    <div class="normal-mode">
                                       <div class="desc">
                                          <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                          <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                       </div>
                                       <div class="comment-stuff">
                                          <div class="more-opt">
                                             <span class="likeholder">
                                             <span class="like-tooltip">
                                             <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                             <span class="icon">icon</span>
                                             </a>
                                             </span>
                                             </span>  
                                             <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><span>icon</span></a>
                                             <!-- Dropdown Trigger -->
                                             <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='drop_down105'>
                                             <i class="zmdi zmdi-more"></i>
                                             </a>
                                             <!-- Dropdown Structure -->
                                             <ul id='drop_down105' class='dropdown-content custom_dropdown'>
                                                <li>
                                                   <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                                </li>
                                                <li>
                                                   <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                                </li>
                                             </ul>
                                          </div>
                                          <div class="less-opt">
                                             <div class="timestamp">8h</div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="edit-mode">
                                       <div class="desc">
                                          <textarea class="editcomment-tt materialize-textarea" id="ec-12">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                          <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="clear"></div>
                              <div class="comment-reply-holder comment-addreply">
                                 <div class="addnew-comment valign-wrapper  comment-reply">
                                    <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                                    <div class="desc-holder">                          
                                       <textarea class="materialize-textarea">Write a reply...</textarea>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="pcomment-holder">
                              <div class="pcomment main-comment">
                                 <div class="img-holder">
                                    <div class="profiletipholder">
                                       <span class="profile-tooltip">
                                       <img class="circle" src="images/demo-profile.jpg"/>
                                       </span>
                                       <span class="profiletooltip_content slidingpan-holder">
                                          <div class="profile-tip" style="display:none;">
                                             <div class="profile-tip-avatar">
                                                <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                                <div class="sliding-pan location-span">
                                                   <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                </div>
                                             </div>
                                             <div class="profile-tip-name">
                                                <a href="javascript:void(0)">Adel Hasanat</a>
                                             </div>
                                             <div class="profile-tip-info">
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                </div>
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                </div>
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                                </div>
                                             </div>
                                          </div>
                                       </span>
                                    </div>
                                 </div>
                                 <div class="desc-holder">
                                    <div class="normal-mode">
                                       <div class="desc">
                                          <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                          <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                       </div>
                                       <div class="comment-stuff">
                                          <div class="more-opt">
                                             <span class="likeholder">
                                             <span class="like-tooltip">
                                             <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                             <span class="icon">icon</span>
                                             </a>
                                             </span>
                                             </span>  
                                             <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><span>icon</span></a>
                                             <!-- Dropdown Trigger -->
                                             <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='drop_down106'>
                                             <i class="zmdi zmdi-more"></i>
                                             </a>
                                             <!-- Dropdown Structure -->
                                             <ul id='drop_down106' class='dropdown-content custom_dropdown'>
                                                <li>
                                                   <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                                </li>
                                                <li>
                                                   <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                                </li>
                                             </ul>
                                          </div>
                                          <div class="less-opt">
                                             <div class="timestamp">8h</div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="edit-mode">
                                       <div class="desc">
                                          <textarea class="editcomment-tt materialize-textarea" id="ec-13">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                          <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="clear"></div>
                              <div class="comment-reply-holder comment-addreply">
                                 <div class="addnew-comment valign-wrapper  comment-reply">
                                    <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                                    <div class="desc-holder">                          
                                       <textarea class="materialize-textarea">Write a reply...</textarea>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="pcomment-holder has-comments">
                           <div class="pcomment main-comment">
                              <div class="img-holder">
                                 <div class="profiletipholder">
                                    <span class="profile-tooltip">
                                    <img class="circle" src="images/demo-profile.jpg"/>
                                    </span>
                                    <span class="profiletooltip_content slidingpan-holder">
                                       <div class="profile-tip" style="display:none;">
                                          <div class="profile-tip-avatar">
                                             <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                             <div class="sliding-pan location-span">
                                                <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                             </div>
                                          </div>
                                          <div class="profile-tip-name">
                                             <a href="javascript:void(0)">Adel Hasanat</a>
                                          </div>
                                          <div class="profile-tip-info">
                                             <div class="profiletip-icon">
                                                <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                             </div>
                                             <div class="profiletip-icon">
                                                <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                             </div>
                                             <div class="profiletip-icon">
                                                <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                             </div>
                                          </div>
                                       </div>
                                    </span>
                                 </div>
                              </div>
                              <div class="desc-holder">
                                 <div class="normal-mode">
                                    <div class="desc">
                                       <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                       <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                    </div>
                                    <div class="comment-stuff">
                                       <div class="more-opt">
                                          <span class="likeholder">
                                          <span class="like-tooltip">
                                          <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                          <span class="icon">icon</span>
                                          </a>
                                          </span>
                                          </span>
                                          <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><span>icon</span></a>
                                          <!-- Dropdown Trigger -->
                                          <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='drop_down107'>
                                          <i class="zmdi zmdi-more"></i>
                                          </a>
                                          <!-- Dropdown Structure -->
                                          <ul id='drop_down107' class='dropdown-content custom_dropdown'>
                                             <li>
                                                <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                             </li>
                                             <li>
                                                <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                             </li>
                                          </ul>
                                       </div>
                                       <div class="less-opt">
                                          <div class="timestamp">8h</div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="edit-mode">
                                    <div class="desc">
                                       <textarea class="editcomment-tt materialize-textarea" id="ec-14">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                       <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="clear"></div>
                           <div class="comment-reply-holder">
                              <div class="pcomment comment-reply">
                                 <div class="img-holder">
                                    <div class="profiletipholder">
                                       <span class="profile-tooltip">
                                       <img class="circle" src="images/demo-profile.jpg"/>
                                       </span>
                                       <span class="profiletooltip_content slidingpan-holder">
                                          <div class="profile-tip" style="display:none;">
                                             <div class="profile-tip-avatar">
                                                <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                                <div class="sliding-pan location-span">
                                                   <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                </div>
                                             </div>
                                             <div class="profile-tip-name">
                                                <a href="javascript:void(0)">Adel Hasanat</a>
                                             </div>
                                             <div class="profile-tip-info">
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                </div>
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                </div>
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                                </div>
                                             </div>
                                          </div>
                                       </span>
                                    </div>
                                 </div>
                                 <div class="desc-holder">
                                    <div class="normal-mode">
                                       <div class="desc">
                                          <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                          <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                       </div>
                                       <div class="comment-stuff">
                                          <div class="more-opt">
                                             <span class="likeholder">
                                             <span class="like-tooltip">
                                             <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                             <span class="icon">icon</span>
                                             </a>
                                             </span>
                                             </span>
                                             <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><span>icon</span></a>
                                             <!-- Dropdown Trigger -->
                                             <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='drop_down108'>
                                             <i class="zmdi zmdi-more"></i>
                                             </a>
                                             <!-- Dropdown Structure -->
                                             <ul id='drop_down108' class='dropdown-content custom_dropdown'>
                                                <li>
                                                   <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                                </li>
                                                <li>
                                                   <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                                </li>
                                             </ul>
                                          </div>
                                          <div class="less-opt">
                                             <div class="timestamp">8h</div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="edit-mode">
                                       <div class="desc">
                                          <textarea class="editcomment-tt materialize-textarea" id="ec-15">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                          <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="pcomment comment-reply">
                                 <div class="img-holder">
                                    <div class="profiletipholder">
                                       <span class="profile-tooltip">
                                       <img class="circle" src="images/demo-profile.jpg"/>
                                       </span>
                                       <span class="profiletooltip_content slidingpan-holder">
                                          <div class="profile-tip" style="display:none;">
                                             <div class="profile-tip-avatar">
                                                <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                                <div class="sliding-pan location-span">
                                                   <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                </div>
                                             </div>
                                             <div class="profile-tip-name">
                                                <a href="javascript:void(0)">Adel Hasanat</a>
                                             </div>
                                             <div class="profile-tip-info">
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                </div>
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                </div>
                                                <div class="profiletip-icon">
                                                   <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                                </div>
                                             </div>
                                          </div>
                                       </span>
                                    </div>
                                 </div>
                                 <div class="desc-holder">
                                    <div class="normal-mode">
                                       <div class="desc">
                                          <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                          <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit...</p>
                                       </div>
                                       <div class="comment-stuff">
                                          <div class="more-opt">
                                             <span class="likeholder">
                                             <span class="like-tooltip">
                                             <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                             <span class="icon">icon</span>
                                             </a>
                                             </span>
                                             </span>
                                             <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><span>icon</span></a>
                                             <!-- Dropdown Trigger -->
                                             <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='drop_down109'>
                                             <i class="zmdi zmdi-more"></i>
                                             </a>
                                             <!-- Dropdown Structure -->
                                             <ul id='drop_down109' class='dropdown-content custom_dropdown'>
                                                <li>
                                                   <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                                </li>
                                                <li>
                                                   <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                                </li>
                                             </ul>
                                          </div>
                                          <div class="less-opt">
                                             <div class="timestamp">8h</div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="edit-mode">
                                       <div class="desc">
                                          <textarea class="editcomment-tt materialize-textarea" id="ec-16">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                          <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="comment-reply-holder comment-addreply">
                              <div class="addnew-comment valign-wrapper  comment-reply">
                                 <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                                 <div class="desc-holder">                          
                                    <textarea class="materialize-textarea">Write a reply...</textarea>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="pcomment-holder">
                           <div class="pcomment main-comment">
                              <div class="img-holder">
                                 <div class="profiletipholder">
                                    <span class="profile-tooltip">
                                    <img class="circle" src="images/demo-profile.jpg"/>
                                    </span>
                                    <span class="profiletooltip_content slidingpan-holder">
                                       <div class="profile-tip" style="display:none;">
                                          <div class="profile-tip-avatar">
                                             <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                             <div class="sliding-pan location-span">
                                                <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                             </div>
                                          </div>
                                          <div class="profile-tip-name">
                                             <a href="javascript:void(0)">Adel Hasanat</a>
                                          </div>
                                          <div class="profile-tip-info">
                                             <div class="profiletip-icon">
                                                <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                             </div>
                                             <div class="profiletip-icon">
                                                <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                             </div>
                                             <div class="profiletip-icon">
                                                <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                             </div>
                                          </div>
                                       </div>
                                    </span>
                                 </div>
                              </div>
                              <div class="desc-holder">
                                 <div class="normal-mode">
                                    <div class="desc">
                                       <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                       <p class="shorten">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.<a a="" href="javascript:void(0)" class="overlay" onclick="explandReadMore(this)" style="position: absolute;top: 0;bottom: -6px;left: 0;right: 0;"><span class="readlink">Read More</span></a></p>
                                    </div>
                                    <div class="comment-stuff">
                                       <div class="more-opt">
                                          <span class="likeholder">
                                          <span class="like-tooltip">
                                          <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                          <span class="icon">icon</span>
                                          </a>
                                          </span>
                                          </span>  
                                          <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><span>icon</span></a>
                                          <!-- Dropdown Trigger -->
                                          <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='drop_down110'>
                                          <i class="zmdi zmdi-more"></i>
                                          </a>
                                          <!-- Dropdown Structure -->
                                          <ul id='drop_down110' class='dropdown-content custom_dropdown'>
                                             <li>
                                                <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                             </li>
                                             <li>
                                                <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                             </li>
                                          </ul>
                                       </div>
                                       <div class="less-opt">
                                          <div class="timestamp">8h</div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="edit-mode">
                                    <div class="desc">
                                       <textarea class="editcomment-tt materialize-textarea" id="ec-17">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                       <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="clear"></div>
                           <div class="comment-reply-holder comment-addreply">
                              <div class="addnew-comment valign-wrapper  comment-reply">
                                 <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                                 <div class="desc-holder">                          
                                    <textarea class="materialize-textarea">Write a reply...</textarea>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="addnew-comment valign-wrapper  ">
                        <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                        <div class="desc-holder">                       
                           <textarea class="materialize-textarea" id="comment_txt_3">Write a reply...</textarea>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <!-- end shared -->
      </div>
      <div class="col s12 m12">
         <div class="post-holder">
            <div class="post-topbar">
               <div class="post-userinfo">
                  <div class="img-holder">
                     <div id="profiletip-5" class="profiletipholder">
                        <span class="profile-tooltip">
                        <img class="circle" src="images/demo-profile.jpg"/>
                        </span>
                        <span class="profiletooltip_content slidingpan-holder">
                           <div class="profile-tip" style="display:none;">
                              <div class="profile-tip-avatar">
                                 <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                 <div class="sliding-pan location-span">
                                    <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                 </div>
                              </div>
                              <div class="profile-tip-name">
                                 <a href="javascript:void(0)">Adel Hasanat</a>
                              </div>
                              <div class="profile-tip-info">
                                 <div class="profiletip-icon">
                                    <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                 </div>
                                 <div class="profiletip-icon">
                                    <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                 </div>
                                 <div class="profiletip-icon">
                                    <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                 </div>
                              </div>
                           </div>
                        </span>
                     </div>
                  </div>
                  <div class="desc-holder">
                     <a href="javascript:void(0)">Nimish Parekh</a>
                     <span class="timestamp">2 hrs<span class="glyphicon glyphicon-globe"></span></span>
                  </div>
               </div>
               <div class="settings-icon">
			   <div class="dropdown dropdown-custom dropdown-xxsmall">
				  <a class="dropdown-button more_btn" href="javascript:void(0);" data-activates="setting_btn20" style="">					
                  <i class="zmdi zmdi-more"></i>
				  </a>
				  <ul id="setting_btn20" class="dropdown-content custom_dropdown" ><li>
                        <a href="javascript:void(0)">Hide post</a>
                     </li>
                     <li>
                        <a href="javascript:void(0)" class="savepost-link">Save post</a>
                     </li>
                     <li>
                        <a href="javascript:void(0)">Mute notification for this post</a>
                     </li>
                     <li>
                        <a href="javascript:void(0)">Mute connect post</a>
                     </li>
                     <li>
                        <a href="javascript:void(0)">Report post</a>
                     </li>
                     <li>
                        <a href="javascript:void(0)" class="composeeditpostAction">Edit post</a>
                     </li>
				  </ul>				 
				</div>
                  <!-- Dropdown Structure -->
               </div>
            </div>
            <div class="post-content">
               <div class="post-img-holder">
                  <div class="lgt-gallery post-img three-img">
                     <a href="images/post-img2.jpg" data-size="1600x1600" data-med="images/post-img2.jpg" data-med-size="1024x1024" data-author="Folkert Gorter" class="pimg-holder himg-box">
                     <img class="himg" src="images/post-img2.jpg" alt="" />
                     </a>
                     <a href="images/post-img3.jpg" data-size="1600x1068" data-med="images/post-img3.jpg" data-med-size="1024x683" data-author="Samuel Rohl" class="pimg-holder vimg-box">
                     <img class="vimg" src="images/post-img3.jpg" alt="" />
                     </a>
                     <a href="images/post-img1.jpg" data-size="1600x1068" data-med="images/post-img1.jpg" data-med-size="1024x683" data-author="Samuel Rohl" class="pimg-holder himg-box">
                     <img class="himg" src="images/post-img1.jpg" alt="" />
                     </a>
                  </div>
               </div>
            </div>
            <div class="clear"></div>
            <div class="post-data">
               <div class="post-actions">
                  <span class="likeholder">
                  <span class="like-tooltip">
                  <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                  <i class="zmdi zmdi-thumb-up"></i>
                  </a>
                  </span>
                  </span>
                  <a href="javascript:void(0)" class="sharepostmodalAction pa-share" title="Share"><i class="zmdi zmdi-mail-reply zmdi-hc-flip-horizontal"></i></a>
                  <span class="right">
                     <a href="javascript:void(0)" class="pa-comment" title="Comment" data-id="1"><i class="zmdi zmdi-comment"></i></a><span class="comment-lcount">4</span>
                  </span>
               </div>
               <div class="comments-section panel">
                  <div class="post-comments">
                     <div class="addnew-comment valign-wrapper  ">
                        <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                        <div class="desc-holder">                          
                           <textarea class="materialize-textarea" id="comment_txt_4">Write a reply...</textarea>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="col s12 m12">
         <div class="post-holder">
            <div class="post-topbar">
               <div class="post-userinfo">
                  <div class="img-holder">
                     <div id="profiletip-6" class="profiletipholder">
                        <span class="profile-tooltip">
                        <img class="circle" src="images/demo-profile.jpg"/>
                        </span>
                        <span class="profiletooltip_content slidingpan-holder">
                           <div class="profile-tip" style="display:none;">
                              <div class="profile-tip-avatar">
                                 <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                 <div class="sliding-pan location-span">
                                    <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                 </div>
                              </div>
                              <div class="profile-tip-name">
                                 <a href="javascript:void(0)">Adel Hasanat</a>
                              </div>
                              <div class="profile-tip-info">
                                 <div class="profiletip-icon">
                                    <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                 </div>
                                 <div class="profiletip-icon">
                                    <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                 </div>
                                 <div class="profiletip-icon">
                                    <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                 </div>
                              </div>
                           </div>
                        </span>
                     </div>
                  </div>
                  <div class="desc-holder">
                     <a href="javascript:void(0)">Nimish Parekh</a>
                     <span class="timestamp">2 hrs<span class="glyphicon glyphicon-globe"></span></span>
                  </div>
               </div> 
               <div class="settings-icon">
			   <div class="dropdown dropdown-custom dropdown-xxsmall">
				  <a class="dropdown-button more_btn" href="javascript:void(0);" data-activates="setting_btn21" style="">					
                  <i class="zmdi zmdi-more"></i>
				  </a>
				  <ul id="setting_btn21" class="dropdown-content custom_dropdown" ><li>
                        <a href="javascript:void(0)">Hide post</a>
                     </li>
                     <li>
                        <a href="javascript:void(0)" class="savepost-link">Save post</a>
                     </li>
                     <li>
                        <a href="javascript:void(0)">Mute notification for this post</a>
                     </li>
                     <li>
                        <a href="javascript:void(0)">Mute connect post</a>
                     </li>
                     <li>
                        <a href="javascript:void(0)">Report post</a>
                     </li>
                     <li>
                        <a href="javascript:void(0)" class="composeeditpostAction">Edit post</a>
                     </li>
				  </ul>				 
				</div>
                  <!-- Dropdown Structure -->
               </div>
            </div>
            <div class="post-content">
               <div class="post-img-holder">
                  <div class="lgt-gallery post-img four-img">
                     <a href="images/post-img2.jpg" data-size="1600x1600" data-med="images/post-img2.jpg" data-med-size="1024x1024" data-author="Folkert Gorter" class="pimg-holder himg-box">
                     <img class="himg" src="images/post-img2.jpg" alt="" />
                     </a>
                     <a href="images/post-img3.jpg" data-size="1600x1068" data-med="images/post-img3.jpg" data-med-size="1024x683" data-author="Samuel Rohl" class="pimg-holder vimg-box">
                     <img class="vimg" src="images/post-img3.jpg" alt="" />
                     </a>
                     <a href="images/post-img.jpg" data-size="1600x1068" data-med="images/post-img.jpg" data-med-size="1024x683" data-author="Samuel Rohl" class="pimg-holder himg-box">
                     <img class="himg" src="images/post-img.jpg" alt="" />
                     </a>
                     <a href="images/post-img3.jpg" data-size="1600x1068" data-med="images/post-img3.jpg" data-med-size="1024x683" data-author="Samuel Rohl" class="pimg-holder vimg-box">
                     <img class="vimg" src="images/post-img3.jpg" alt="" />
                     </a>
                  </div>
               </div>
            </div>
            <div class="clear"></div>
            <div class="post-data">
               <div class="post-actions">
                  <span class="likeholder">
                  <span class="like-tooltip">
                  <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                  <i class="zmdi zmdi-thumb-up"></i>
                  </a>
                  </span>
                  </span>
                  <a href="javascript:void(0)" class="sharepostmodalAction pa-share" title="Share"><i class="zmdi zmdi-mail-reply zmdi-hc-flip-horizontal"></i></a>
                  <span class="right">
                     <a href="javascript:void(0)" class="pa-comment" title="Comment" data-id="1"><i class="zmdi zmdi-comment"></i></a><span class="comment-lcount">4</span>
                  </span>
               </div>
               <div class="comments-section panel">
                  <div class="post-comments">
                     <div class="addnew-comment valign-wrapper  ">
                        <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                        <div class="desc-holder">                          
                           <textarea class="materialize-textarea" id="comment_txt_5">Write a reply...</textarea>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="col s12 m12">
         <div class="post-holder">
            <div class="post-topbar">
               <div class="post-userinfo">
                  <div class="img-holder">
                     <div id="profiletip-7" class="profiletipholder">
                        <span class="profile-tooltip">
                        <img class="circle" src="images/demo-profile.jpg"/>
                        </span>
                        <span class="profiletooltip_content slidingpan-holder">
                           <div class="profile-tip" style="display:none;">
                              <div class="profile-tip-avatar">
                                 <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                 <div class="sliding-pan location-span">
                                    <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                 </div>
                              </div>
                              <div class="profile-tip-name">
                                 <a href="javascript:void(0)">Adel Hasanat</a>
                              </div>
                              <div class="profile-tip-info">
                                 <div class="profiletip-icon">
                                    <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                 </div>
                                 <div class="profiletip-icon">
                                    <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                 </div>
                                 <div class="profiletip-icon">
                                    <a href="javascript:void(0)" class="title-tooltip" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                 </div>
                              </div>
                           </div>
                        </span>
                     </div>
                  </div>
                  <div class="desc-holder">
                     <a href="javascript:void(0)">Nimish Parekh</a>
                     <span class="timestamp">2 hrs<span class="glyphicon glyphicon-globe"></span></span>
                  </div>
               </div> 
               <div class="settings-icon">
			   <div class="dropdown dropdown-custom dropdown-xxsmall">
				  <a class="dropdown-button more_btn" href="javascript:void(0);" data-activates="setting_btn22" style="">					
                  <i class="zmdi zmdi-more"></i>
				  </a>
				  <ul id="setting_btn22" class="dropdown-content custom_dropdown" ><li>
                        <a href="javascript:void(0)">Hide post</a>
                     </li>
                     <li>
                        <a href="javascript:void(0)" class="savepost-link">Save post</a>
                     </li>
                     <li>
                        <a href="javascript:void(0)">Mute notification for this post</a>
                     </li>
                     <li>
                        <a href="javascript:void(0)">Mute connect post</a>
                     </li>
                     <li>
                        <a href="javascript:void(0)">Report post</a>
                     </li>
                     <li>
                        <a href="javascript:void(0)" class="composeeditpostAction">Edit post</a>
                     </li>
				  </ul>				 
				</div>
                  <!-- Dropdown Structure -->
               </div>
            </div>
            <div class="post-content">
               <div class="post-img-holder">
                  <div class="lgt-gallery lgt-gallery-photo post-img five-img dis-none">
                     <a href="images/post-img2.jpg" data-size="1600x1600" data-med="images/post-img2.jpg" data-med-size="1024x1024" data-author="Folkert Gorter" class="pimg-holder himg-box">
                     <img class="himg" src="images/post-img2.jpg" alt="" />
                     </a>
                     <a href="images/post-img3.jpg" data-size="1600x1068" data-med="images/post-img3.jpg" data-med-size="1024x683" data-author="Samuel Rohl" class="pimg-holder vimg-box">
                     <img class="vimg" src="images/post-img3.jpg" alt="" />
                     </a>
                     <a href="images/post-img.jpg" data-size="1600x1068" data-med="images/post-img.jpg" data-med-size="1024x683" data-author="Samuel Rohl" class="pimg-holder himg-box">
                     <img class="himg" src="images/post-img.jpg" alt="" />
                     </a>
                     <a href="images/post-img3.jpg" data-size="1600x1068" data-med="images/post-img3.jpg" data-med-size="1024x683" data-author="Samuel Rohl" class="pimg-holder vimg-box">
                     <img class="vimg" src="images/post-img3.jpg" alt="" />
                     </a>
                     <a href="images/post-img.jpg" data-size="1600x1068" data-med="images/post-img.jpg" data-med-size="1024x683" data-author="Samuel Rohl" class="pimg-holder himg-box">
                     <img class="himg" src="images/post-img.jpg" alt="" />
                     </a>
                  </div>
               </div>
            </div>
            <div class="clear"></div>
            <div class="post-data">
               <div class="post-actions">
                  <span class="likeholder">
                  <span class="like-tooltip">
                  <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                  <i class="zmdi zmdi-thumb-up"></i>
                  </a>
                  </span>
                  </span>
                  <a href="javascript:void(0)" class="sharepostmodalAction pa-share" title="Share"><i class="zmdi zmdi-mail-reply zmdi-hc-flip-horizontal"></i></a>
                  <span class="right">
                     <a href="javascript:void(0)" class="pa-comment" title="Comment" data-id="1"><i class="zmdi zmdi-comment"></i></a><span class="comment-lcount">4</span>
                  </span>
               </div>
               <div class="comments-section panel">
                  <div class="post-comments">
                     <div class="addnew-comment valign-wrapper  ">
                        <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                        <div class="desc-holder">                          
                           <textarea class="materialize-textarea" id="comment_txt_6">Write a reply...</textarea>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="col s12 m12">
         <div class="post-holder">
            <div class="post-topbar">
               <div class="post-userinfo">
                  <div class="img-holder">
                     <div id="profiletip-8" class="profiletipholder">
                        <span class="profile-tooltip">
                        <img class="circle" src="images/demo-profile.jpg"/>
                        </span>
                        <span class="profiletooltip_content slidingpan-holder">
                           <div class="profile-tip" style="display:none;">
                              <div class="profile-tip-avatar">
                                 <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                 <div class="sliding-pan location-span">
                                    <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                 </div>
                              </div>
                              <div class="profile-tip-name">
                                 <a href="javascript:void(0)">Adel Hasanat</a>
                              </div>
                              <div class="profile-tip-info">
                                 <div class="profiletip-icon">
                                    <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                 </div>
                                 <div class="profiletip-icon">
                                    <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                 </div>
                                 <div class="profiletip-icon">
                                    <a href="javascript:void(0)" class="title-tooltip" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                 </div>
                              </div>
                           </div>
                        </span>
                     </div>
                  </div>
                  <div class="desc-holder">
                     <a href="javascript:void(0)">Nimish Parekh</a>
                     <span class="timestamp">2 hrs<span class="glyphicon glyphicon-globe"></span></span>
                  </div>
               </div>	   
               <div class="settings-icon">
			   <div class="dropdown dropdown-custom dropdown-xxsmall">
				  <a class="dropdown-button more_btn" href="javascript:void(0);" data-activates="setting_btn23" style="">					
                  <i class="zmdi zmdi-more"></i>
				  </a>
				  <ul id="setting_btn23" class="dropdown-content custom_dropdown" ><li>
                        <a href="javascript:void(0)">Hide post</a>
                     </li>
                     <li>
                        <a href="javascript:void(0)" class="savepost-link">Save post</a>
                     </li>
                     <li>
                        <a href="javascript:void(0)">Mute notification for this post</a>
                     </li>
                     <li>
                        <a href="javascript:void(0)">Mute connect post</a>
                     </li>
                     <li>
                        <a href="javascript:void(0)">Report post</a>
                     </li>
                     <li>
                        <a href="javascript:void(0)" class="composeeditpostAction">Edit post</a>
                     </li>
				  </ul>				 
				</div>
                  <!-- Dropdown Structure -->
               </div>
            </div>
            <div class="post-content">
               <div class="post-img-holder">
                  <div class="lgt-gallery post-img more-img">
                     <a href="images/post-img2.jpg" data-size="1600x1600" data-med="images/post-img2.jpg" data-med-size="1024x1024" data-author="Folkert Gorter" class="pimg-holder himg-box">
                     <img class="himg" src="images/post-img2.jpg" alt="" />
                     </a>
                     <a href="images/post-img3.jpg" data-size="1600x1068" data-med="images/post-img3.jpg" data-med-size="1024x683" data-author="Samuel Rohl" class="pimg-holder vimg-box">
                     <img class="vimg" src="images/post-img3.jpg" alt="" />
                     </a>
                     <a href="images/post-img.jpg" data-size="1600x1068" data-med="images/post-img.jpg" data-med-size="1024x683" data-author="Samuel Rohl" class="pimg-holder himg-box">
                     <img class="himg" src="images/post-img.jpg" alt="" />
                     </a>
                     <a href="images/post-img3.jpg" data-size="1600x1068" data-med="images/post-img3.jpg" data-med-size="1024x683" data-author="Samuel Rohl" class="pimg-holder vimg-box">
                     <img class="vimg" src="images/post-img3.jpg" alt="" />
                     </a>
                     <a href="images/post-img.jpg" data-size="1600x1068" data-med="images/post-img.jpg" data-med-size="1024x683" data-author="Samuel Rohl" class="pimg-holder himg-box">
                     <img class="himg" src="images/post-img.jpg" alt="" />
                     <span class="more-box"><i class="mdi mdi-plus"></i>2</span>
                     </a>
                     <a href="images/post-img2.jpg" data-size="1600x1600" data-med="images/post-img2.jpg" data-med-size="1024x1024" data-author="Folkert Gorter" class="pimg-holder himg-box extraimg">
                     <img class="himg" src="images/post-img2.jpg" alt="" />
                     </a>
                  </div>
               </div>
            </div>
            <div class="clear"></div>
            <div class="post-data">
               <div class="post-actions">
                  <span class="likeholder">
                  <span class="like-tooltip">
                  <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                  <i class="zmdi zmdi-thumb-up"></i>
                  </a>
                  </span>
                  </span>
                  <a href="javascript:void(0)" class="sharepostmodalAction pa-share" title="Share"><i class="zmdi zmdi-mail-reply zmdi-hc-flip-horizontal"></i></a>
                  <span class="right">
                     <a href="javascript:void(0)" class="pa-comment" title="Comment" data-id="1"><i class="zmdi zmdi-comment"></i></a><span class="comment-lcount">4</span>
                  </span>
               </div>
               <div class="comments-section panel">
                  <div class="post-comments">
                     <div class="addnew-comment valign-wrapper  ">
                        <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                        <div class="desc-holder">                       
                           <textarea class="materialize-textarea" id="comment_txt_7">Write a reply...</textarea>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<div class="clear"></div>
