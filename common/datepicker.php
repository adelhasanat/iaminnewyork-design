<div id="datepickerDropdown" class="modal tbpost_modal modal-datepicker modalxii_level1 nice-scroll">
	<div class="content_header">
	    <button class="close_span waves-effect">
	    <i class="mdi mdi-close mdi-20px material_close resetdatepicker"></i>
	    </button>
	    <p class="selected_photo_text">Select Date</p>
	    <a href="javascript:void(0)" class="done_btn action_btn closedatepicker">Done</a>
	</div> 
    <div class="modal-content">
      <div id="datepickerBlock"></div>
    </div>
</div>   
