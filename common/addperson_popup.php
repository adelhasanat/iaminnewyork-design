<!--attachment modal-->
<div id="compose_addperson" class="modal compose_inner_modal modalxii_level1">
   <div class="content_header">
      <button class="close_span waves-effect">
      <i class="mdi mdi-close mdi-20px material_close"></i>
      </button>
      <p class="selected_person_text">person selected</p>
      <a href="javascript:void(0)" id="chk_person_done" class="done_btn action_btn">Done</a>
   </div>
   <nav class="search_for_tag">
      <div class="nav-wrapper">
         <form>
            <div class="input-field">
               <input id="addperson_search_box" type="search" class="search_box" required="">
               <label class="label-icon" for="addperson_search_box">
               <i class="zmdi zmdi-search"></i>
               </label>
            </div>
         </form>
      </div>
   </nav>
   <div class="person_box">
   </div>
</div>
