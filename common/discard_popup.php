<div id="compose_discard_modal" class="modal discard_md_modal custom_modal">
	<div class="modal-content">
		<p class="discard_modal_msg">Discard Changes ?</p>
	</div>
	<div class="modal-footer">
		<a class="modal_keep btngen-center-align waves-effect" >Keep</a>
		<a type="button" class="modal_discard btngen-center-align waves-effect" onclick="clearPost()">Discard</a>
	</div>
</div>