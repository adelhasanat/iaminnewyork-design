
<div id="upload_img_box" class="modal crop_upload_modal">
   <div class="upload_img_title">
      <h3>Crop and Reposition image</h3>
      <a class="popup-modal-dismiss crossicon" data-dismiss="modal" href="javascript:void(0)">
      <i class="mdi mdi-close"></i>
      </a>
   </div>
   <div class="jc-demo-box">
      <form id="formCrop">
         <div id="views"></div>
         <!--<input id="file"  type="file" />-->
         <br />
         <button id="cropbutton" type="button">Crop</button>
         <button id="saveimg" type="button">Save</button>
      </form>
   </div>
</div>