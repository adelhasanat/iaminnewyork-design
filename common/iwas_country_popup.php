
<!--attachment modal-->
<div id="compose_iwasincountry" class="modal compose_inner_modal modalxii_level1 visit-country">
   <div class="content_header">
      <button class="close_span waves-effect">
      <i class="mdi mdi-close mdi-20px material_close"></i>
      </button>
      <p class="selected_person_text">Select Destination</p>
   </div>
   <div class="person_box">
      <div class="collection visit-country-list">
         <ul class="collection">
            <li class="collection-item avatar">
               <img src="images/loc-icons/usa.jpg" alt="" class="circle">
               <span class="title">
                  <a href="https://www.iamin<?=$st_nm_L?>.com/frontend/web/index.php?r=site%2Fmainfeed" target="_blank" class="collection-item">I am in <?=$st_nm_S?></a>
               </span>
            </li>
            <li class="collection-item avatar">
               <img src="images/loc-icons/chicago.jpg" alt="" class="circle">
               <span class="title">
                  <a href="https://www.iaminchicago.com/frontend/web/index.php?r=site%2Fmainfeed" target="_blank" class="collection-item">I am in Chicago</a>
               </span>
            </li>
            <li class="collection-item avatar">
               <img src="images/loc-icons/jordan.jpg" alt="" class="circle">
               <span class="title">
                  <a href="https://www.iaminjordan.com/frontend/web/index.php?r=site%2Fmainfeed" target="_blank" class="collection-item">I am in Jordan</a>
               </span>
            </li>
            <li class="collection-item avatar">
               <img src="images/loc-icons/japan.jpg" alt="" class="circle">
               <span class="title">
                  <a href="https://www.iaminjapan.com/frontend/web/index.php?r=site%2Fmainfeed" target="_blank" class="collection-item">I am in Japan</a>
               </span>
            </li>
            <li class="collection-item avatar">
               <img src="images/loc-icons/spain.jpg" alt="" class="circle">
               <span class="title">
                  <a href="https://www.iaminspain.com/frontend/web/index.php?r=site%2Fmainfeed" target="_blank" class="collection-item">I am in Spain</a>
               </span>
            </li>
            <li class="collection-item avatar">
               <img src="images/loc-icons/petra.jpg" alt="" class="circle">
               <span class="title">
                  <a href="https://www.iaminpetra.com/frontend/web/index.php?r=site%2Fmainfeed" target="_blank" class="collection-item">I am in Petra</a>
               </span>
            </li>
            <li class="collection-item avatar">1
               <img src="images/loc-icons/france.jpg" alt="" class="circle">
               <span class="title">
                  <a href="https://www.iaminfrance.com/frontend/web/index.php?r=site%2Fmainfeed" target="_blank" class="collection-item">I am in France</a>
               </span>
            </li>
            <li class="collection-item avatar">
               <img src="images/whereto.jpg" alt="" class="circle">
               <span class="title">
                  <a href="https://www.iaminaqaba.com/frontend/web/index.php?r=site%2Fmainfeed" target="_blank" class="collection-item">I am in Aqaba</a>
               </span>
            </li>
            <li class="collection-item avatar">
               <img src="images/whereto.jpg" alt="" class="circle">
               <span class="title">
                  <a href="https://www.iaminmadain.com/frontend/web/index.php?r=site%2Fmainfeed" target="_blank" class="collection-item">I am in Madain</a>
               </span>
            </li>
            <li class="collection-item avatar">
               <img src="images/whereto.jpg" alt="" class="circle">
               <span class="title">
                  <a href="https://www.iaminsaudi.com/frontend/web/index.php?r=site%2Fmainfeed" target="_blank" class="collection-item">I am in Saudi</a>
               </span>
            </li>
            <li class="collection-item avatar">
               <img src="images/whereto.jpg" alt="" class="circle">
               <span class="title">
                  <a href="https://www.iamindubai.com/frontend/web/index.php?r=site%2Fmainfeed" target="_blank" class="collection-item">I am in Dubai</a>
               </span>
            </li>
            <li class="collection-item avatar">
               <img src="images/whereto.jpg" alt="" class="circle">
               <span class="title">
                  <a href="https://www.iaminqatar.com/frontend/web/index.php?r=site%2Fmainfeed" target="_blank" class="collection-item">I am in Qatar</a>
               </span>
            </li>
            <li class="collection-item avatar">
               <img src="images/whereto.jpg" alt="" class="circle">
               <span class="title">
                  <a href="https://www.iaminbahrain.com/frontend/web/index.php?r=site%2Fmainfeed" target="_blank" class="collection-item">I am in Bahrain</a>
               </span>
            </li>
            <li class="collection-item avatar">
               <img src="images/whereto.jpg" alt="" class="circle">
               <span class="title">
                  <a href="https://www.iaminabudhabi.com/frontend/web/index.php?r=site%2Fmainfeed" target="_blank" class="collection-item">I am in Abu Dhabi</a>
               </span>
            </li>
            <li class="collection-item avatar">
               <img src="images/whereto.jpg" alt="" class="circle">
               <span class="title">
                  <a href="#!" class="collection-item">I am in Chicago</a>
               </span>
            </li>
            <li class="collection-item avatar">
               <img src="images/whereto.jpg" alt="" class="circle">
               <span class="title">
                  <a href="#!" class="collection-item">I am in Italy</a>
               </span>
            </li>
            <li class="collection-item avatar">
               <img src="images/whereto.jpg" alt="" class="circle">
               <span class="title">
                  <a href="#!" class="collection-item">I am in Mexico</a>
               </span>
            </li>
            <li class="collection-item avatar">
               <img src="images/whereto.jpg" alt="" class="circle">
               <span class="title">
                  <a href="#!" class="collection-item">I am in Istanbul</a>
               </span>
            </li>
            <li class="collection-item avatar">
               <img src="images/whereto.jpg" alt="" class="circle">
               <span class="title">
                  <a href="#!" class="collection-item">I am in China</a>
               </span>
            </li>
            <li class="collection-item avatar">
               <img src="images/whereto.jpg" alt="" class="circle">
               <span class="title">
                  <a href="#!" class="collection-item">I am in Germany</a>
               </span>
            </li>
            <li class="collection-item avatar">
               <img src="images/whereto.jpg" alt="" class="circle">
               <span class="title">
                  <a href="#!" class="collection-item">I am in Thailand</a>
               </span>
            </li>
            <li class="collection-item avatar">
               <img src="images/whereto.jpg" alt="" class="circle">
               <span class="title">
                  <a href="#!" class="collection-item">I am in Austria</a>
               </span>
            </li>
            <li class="collection-item avatar">
               <img src="images/whereto.jpg" alt="" class="circle">
               <span class="title">
                  <a href="#!" class="collection-item">I am in Hong Kong</a>
               </span>
            </li>
            <li class="collection-item avatar">
               <img src="images/whereto.jpg" alt="" class="circle">
               <span class="title">
                  <a href="#!" class="collection-item">I am in Greece</a>
               </span>
            </li>
            <li class="collection-item avatar">
               <img src="images/whereto.jpg" alt="" class="circle">
               <span class="title">
                  <a href="#!" class="collection-item">I am in Russia</a>
               </span>
            </li>
            <li class="collection-item avatar">
               <img src="images/whereto.jpg" alt="" class="circle">
               <span class="title">
                  <a href="#!" class="collection-item">I am in Japan</a>
               </span>
            </li>
            <li class="collection-item avatar">
               <img src="images/whereto.jpg" alt="" class="circle">
               <span class="title">
                  <a href="#!" class="collection-item">I am in Cyprus</a>
               </span>
            </li>
            <li class="collection-item avatar">
               <img src="images/whereto.jpg" alt="" class="circle">
               <span class="title">
                  <a href="#!" class="collection-item">I am in Jamaica</a>
               </span>
            </li>
            <li class="collection-item avatar">
               <img src="images/whereto.jpg" alt="" class="circle">
               <span class="title">
                  <a href="#!" class="collection-item">I am in Poland</a>
               </span>
            </li>
            <li class="collection-item avatar">
               <img src="images/whereto.jpg" alt="" class="circle">
               <span class="title">
                  <a href="#!" class="collection-item">I am in Zimbabwe</a>
               </span>
            </li>
            <li class="collection-item avatar">
               <img src="images/whereto.jpg" alt="" class="circle">
               <span class="title">
                  <a href="#!" class="collection-item">I am in Ukraine</a>
               </span>
            </li>
            <li class="collection-item avatar">
               <img src="images/whereto.jpg" alt="" class="circle">
               <span class="title">
                  <a href="#!" class="collection-item">I am in Netherlands</a>
               </span>
            </li>
            <li class="collection-item avatar">
               <img src="images/whereto.jpg" alt="" class="circle">
               <span class="title">
                  <a href="#!" class="collection-item">I am in India</a>
               </span>
            </li>
            <li class="collection-item avatar">
               <img src="images/whereto.jpg" alt="" class="circle">
               <span class="title">
                  <a href="#!" class="collection-item">I am in Singapore</a>
               </span>
            </li>
            <li class="collection-item avatar">
               <img src="images/whereto.jpg" alt="" class="circle">
               <span class="title">
                  <a href="#!" class="collection-item">I am in Morocco</a>
               </span>
            </li>
            <li class="collection-item avatar">
               <img src="images/whereto.jpg" alt="" class="circle">
               <span class="title">
                  <a href="#!" class="collection-item">I am in Egypt</a>
               </span>
            </li>
            <li class="collection-item avatar">
               <img src="images/whereto.jpg" alt="" class="circle">
               <span class="title">
                  <a href="#!" class="collection-item">I am in Brazil</a>
               </span>
            </li>
            <li class="collection-item avatar">
               <img src="images/whereto.jpg" alt="" class="circle">
               <span class="title">
                  <a href="#!" class="collection-item">I am in Australia</a>
               </span>
            </li>
            <li class="collection-item avatar">
               <img src="images/whereto.jpg" alt="" class="circle">
               <span class="title">
                  <a href="#!" class="collection-item">I am in Malaysia</a>
               </span>
            </li>
            <li class="collection-item avatar">
               <img src="images/whereto.jpg" alt="" class="circle">
               <span class="title">
                  <a href="#!" class="collection-item">I am in Indonesia</a>
               </span>
            </li>
            <li class="collection-item avatar">
               <img src="images/whereto.jpg" alt="" class="circle">
               <span class="title">
                  <a href="#!" class="collection-item">I am in Argentina</a>
               </span>
            </li>
            <li class="collection-item avatar">
               <img src="images/whereto.jpg" alt="" class="circle">
               <span class="title">
                  <a href="#!" class="collection-item">I am in Zimbabwe</a>
               </span>
            </li>
            <li class="collection-item avatar">
               <img src="images/whereto.jpg" alt="" class="circle">
               <span class="title">
                  <a href="#!" class="collection-item">I am in Macao</a>
               </span>
            </li>
            <li class="collection-item avatar">
               <img src="images/whereto.jpg" alt="" class="circle">
               <span class="title">
                  <a href="#!" class="collection-item">I am in Croatia</a>
               </span>
            </li>
            <li class="collection-item avatar">
               <img src="images/whereto.jpg" alt="" class="circle">
               <span class="title">
                  <a href="#!" class="collection-item">I am in Southkorea</a>
               </span>
            </li>
         </ul>
      </div>
   </div>
</div>
