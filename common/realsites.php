<?php
$realsites	= array(
		array(
			"src" => "https://www.visittheusa.com", 
			"name" => "Visit USA",
		    	"image" => "usa.jpg"
		),
		array(
			"src" => "https://www.choosechicago.com/", 
			"name" => "Visit Chicago",
		    	"image" => "chicago.jpg"
		),
		array(
			"src" => "http://ee.france.fr", 
			"name" => "Visit France",
		    	"image" => "france.jpg"
		),
		array(
			"src" => "https://www.visitcalifornia.com/", 
			"name" => "Visit California",
		    	"image" => "california.jpg"
		),
		array(
			"src" => "http://www.tourspain.org", 
			"name" => "Visit Spain",
		    	"image" => "spain.jpg"
		),
		array(
			"src" => "javascript:void(0)", 
			"name" => "Visit London",
		    	"image" => "london.jpg"
		),
		array(
			"src" => "https://www.visitflorida.com/", 
			"name" => "Visit Florida",
		    	"image" => "florida.jpg"
		),
		array(
			"src" => "https://us.jnto.go.jp", 
			"name" => "Visit Japan",
		    	"image" => "japan.jpg"
		),
		array(
			"src" => "https://www.visitdubai.com", 
			"name" => "Visit Dubai",
		    	"image" => "dubai.jpg"
		),
		array(
			"src" => "https://www.nycgo.com/", 
			"name" => "Visit New York",
		    	"image" => "newyork.jpg"
		),
		array(
			"src" => "https://www.visitsaudi.com", 
			"name" => "Visit Saudi",
		    	"image" => "saudi.jpg"
		),
		array(
			"src" => "http://www.visitjordan.com", 
			"name" => "Visit Jordan",
		    	"image" => "jordan.jpg"
		),
		array(
			"src" => "http://www.visitpetra.jo", 
			"name" => "Visit Petra",
		    	"image" => "petra.jpg"
		),
		array(
			"src" => "http://na.visitjordan.com/Wheretogo/Aqaba.aspx", 
			"name" => "Visit Aqaba",
		    	"image" => "whereto.jpg"
		),
		array(
			"src" => "https://rcu.gov.sa/", 
			"name" => "Visit Madain",
		    	"image" => "whereto.jpg"
		),
		array(
			"src" => "https://www.visitqatar.qa", 
			"name" => "Visit Qatar",
		    	"image" => "whereto.jpg"
		),
		array(
			"src" => "https://visitbahrain.bh", 
			"name" => "Visit Bahrain",
		    	"image" => "whereto.jpg"
		),
		array(
			"src" => "http://www.visitgreece.gr", 
			"name" => "Visit Greece",
		    	"image" => "whereto.jpg"
		),
		array(
			"src" => "https://visitabudhabi.ae", 
			"name" => "Visit Abu Dhabi",
		    	"image" => "whereto.jpg"
		),
		array(
			"src" => "https://www.visititaly.eu", 
			"name" => "Visit Italy",
		    	"image" => "whereto.jpg"
		),
		array(
			"src" => "https://www.visitmexico.com", 
			"name" => "Visit Mexico",
		    	"image" => "whereto.jpg"
		),
		array(
			"src" => "https://visit.istanbul", 
			"name" => "Visit Istanbul",
		    	"image" => "whereto.jpg"
		),
		array(
			"src" => "https://chinatour.net", 
			"name" => "Visit China",
		    	"image" => "whereto.jpg"
		),
		array(
			"src" => "https://www.germany.travel", 
			"name" => "Visit Germany",
		    	"image" => "whereto.jpg"
		),
		array(
			"src" => "https://www.tourismthailand.org", 
			"name" => "Visit Thailand",
		    	"image" => "whereto.jpg"
		),
		array(
			"src" => "https://www.austria.info", 
			"name" => "Visit Austria",
		    	"image" => "whereto.jpg"
		),
		array(
			"src" => "https://www.visithongkong.gov.hk", 
			"name" => "Visit Hong Kong",
		    	"image" => "whereto.jpg"
		),
		array(
			"src" => "https://www.visitrussia.com", 
			"name" => "Visit Russia",
		    	"image" => "whereto.jpg"
		),
		array(
			"src" => "https://www.visitcyprus.com", 
			"name" => "Visit Cyprus",
		    	"image" => "whereto.jpg"
		),
		array(
			"src" => "https://www.visitjamaica.com", 
			"name" => "Visit Jamaica",
		    	"image" => "whereto.jpg"
		),
		array(
			"src" => "https://www.visitpoland.com", 
			"name" => "Visit Poland",
		    	"image" => "whereto.jpg"
		),
		array(
			"src" => "https://www.zimbabwetourism.net", 
			"name" => "Visit Zimbabwe",
		    	"image" => "whereto.jpg"
		),
		array(
			"src" => "http://visittoukraine.com/en", 
			"name" => "Visit Ukraine",
		    	"image" => "whereto.jpg"
		),
		array(
			"src" => "https://www.holland.com/", 
			"name" => "Visit Netherlands",
		    	"image" => "whereto.jpg"
		),
		array(
			"src" => "https://www.indianvisit.com/", 
			"name" => "Visit India",
		    	"image" => "whereto.jpg"
		),
		array(
			"src" => "https://www.visitsingapore.com/en/", 
			"name" => "Visit Singapore",
		    	"image" => "whereto.jpg"
		),
		array(
			"src" => "http://www.tourism.gov.pk/", 
			"name" => "Visit Pakistan",
		    	"image" => "whereto.jpg"
		),
		array(
			"src" => "https://www.visitmorocco.com/en", 
			"name" => "Visit Morocco",
		    	"image" => "whereto.jpg"
		),
		array(
			"src" => "http://www.egypt.travel/", 
			"name" => "Visit Egypt",
		    	"image" => "whereto.jpg"
		),
		array(
			"src" => "https://www.visitbrasil.com/", 
			"name" => "Visit Brazil",
		    	"image" => "whereto.jpg"
		),
		array(
			"src" => "https://www.australia.com/en", 
			"name" => "Visit Australia",
		    	"image" => "whereto.jpg"
		),
		array(
			"src" => "http://www.visit-malaysia.com/", 
			"name" => "Visit Malaysia",
		    	"image" => "whereto.jpg"
		),
		array(
			"src" => "https://www.indonesia.travel/gb/en/home", 
			"name" => "Visit Indonesia",
		    	"image" => "whereto.jpg"
		),
		array(
			"src" => "https://www.argentina.travel/", 
			"name" => "Visit Argentina",
		    	"image" => "whereto.jpg"
		),
		array(
			"src" => "https://www.zimbabwetourism.net/", 
			"name" => "Visit Zimbabwe",
		    	"image" => "whereto.jpg"
		),
		array(
			"src" => "https://www.visitmacao.com.au/", 
			"name" => "Visit Macao",
		    	"image" => "whereto.jpg"
		),
		array(
			"src" => "https://www.visit-croatia.co.uk/", 
			"name" => "Visit Croatia",
		    	"image" => "whereto.jpg"
		),
		array(
			"src" => "http://english.visitkorea.or.kr/enu/index.kto", 
			"name" => "Visit Southkorea",
		    	"image" => "whereto.jpg"
		),
		array(
			"src" => "https://www.visitiran.ir/", 
			"name" => "Visit Iran",
		    	"image" => "whereto.jpg"
		)
	);
?>