<?php
$selfsites	= array(
    array(
    	"src" => "https://www.iaminusa.com/frontend/web/index.php?r=site%2Fmainfeed", 
    	"name" => "I am in USA",
    	"image" => "usa.jpg"
    ),
    array(
    	"src" => "https://www.iaminchicago.com/frontend/web/index.php?r=site%2Fmainfeed", 
    	"name" => "I am in Chicago",
    	"image" => "chicago.jpg"
    ),
    array(
    	"src" => "https://www.iaminfrance.com/frontend/web/index.php?r=site%2Fmainfeed", 
    	"name" => "I am in France",
    	"image" => "france.jpg"
    ),
    array(
    	"src" => "https://www.iamincalifornia.com/frontend/web/index.php?r=site%2Fmainfeed", 
    	"name" => "I am in California",
    	"image" => "california.jpg"
    ),
    array(
    	"src" => "https://www.iaminspain.com/frontend/web/index.php?r=site%2Fmainfeed", 
    	"name" => "I am in Spain",
    	"image" => "spain.jpg"
    ),
    array(
    	"src" => "https://www.iaminlondon.com/frontend/web/index.php?r=site%2Fmainfeed", 
    	"name" => "I am in London",
    	"image" => "london.jpg"
    ),
    array(
    	"src" => "https://www.iaminflorida.com/frontend/web/index.php?r=site%2Fmainfeed", 
    	"name" => "I am in Florida",
    	"image" => "florida.jpg"
    ),
    array(
    	"src" => "https://www.iaminjapan.com/frontend/web/index.php?r=site%2Fmainfeed", 
    	"name" => "I am in Japan",
    	"image" => "japan.jpg"
    ),
    array(
    	"src" => "https://www.iamindubai.com/frontend/web/index.php?r=site%2Fmainfeed", 
    	"name" => "I am in Dubai",
    	"image" => "dubai.jpg"
    ),
    array(
    	"src" => "https://www.iaminnewyork.com/frontend/web/index.php?r=site%2Fmainfeed", 
    	"name" => "I am in New York",
    	"image" => "newyork.jpg"
    ),
    array(
    	"src" => "https://www.iaminsaudi.com/frontend/web/index.php?r=site%2Fmainfeed", 
    	"name" => "I am in Saudi",
    	"image" => "saudi.jpg"
    ),
    array(
    	"src" => "https://www.iaminjordan.com/frontend/web/index.php?r=site%2Fmainfeed", 
    	"name" => "I am in Jordan",
    	"image" => "jordan.jpg"
    ),
    array(
    	"src" => "https://www.iaminpetra.com/frontend/web/index.php?r=site%2Fmainfeed", 
    	"name" => "I am in Petra",
    	"image" => "petra.jpg"
    ),
    array(
    	"src" => "javascript:void(0)", 
    	"name" => "I am in Aqaba",
    	"image" => "whereto.jpg"
    ),
    array(
    	"src" => "javascript:void(0)", 
    	"name" => "I am in Madain",
    	"image" => "whereto.jpg"
    ),
    array(
    	"src" => "javascript:void(0)", 
    	"name" => "I am in Qatar",
    	"image" => "whereto.jpg"
    ),
    array(
    	"src" => "javascript:void(0)", 
    	"name" => "I am in Bahrain",
    	"image" => "whereto.jpg"
    ),
    array(
    	"src" => "javascript:void(0)", 
    	"name" => "I am in Abu Dhabi",
    	"image" => "whereto.jpg"
    ),
    array(
    	"src" => "javascript:void(0)", 
    	"name" => "I am in Greece",
    	"image" => "whereto.jpg"
    ),
    array(
    	"src" => "javascript:void(0)", 
    	"name" => "I am in Italy",
    	"image" => "whereto.jpg"
    ),
    array(
    	"src" => "javascript:void(0)", 
    	"name" => "I am in Mexico",
    	"image" => "whereto.jpg"
    ),
    array(
    	"src" => "javascript:void(0)", 
    	"name" => "I am in Istanbul",
    	"image" => "whereto.jpg"
    ),
    array(
    	"src" => "javascript:void(0)", 
    	"name" => "I am in China",
    	"image" => "whereto.jpg"
    ),
    array(
    	"src" => "javascript:void(0)", 
    	"name" => "I am in Germany",
    	"image" => "whereto.jpg"
    ),
    array(
    	"src" => "javascript:void(0)", 
    	"name" => "I am in Thailand",
    	"image" => "whereto.jpg"
    ),
    array(
    	"src" => "javascript:void(0)", 
    	"name" => "I am in Austria",
    	"image" => "whereto.jpg"
    ),
    array(
    	"src" => "javascript:void(0)", 
    	"name" => "I am in Hong Kong",
    	"image" => "whereto.jpg"
    ),
    array(
    	"src" => "javascript:void(0)", 
    	"name" => "I am in Russia",
    	"image" => "whereto.jpg"
    ),
    array(
    	"src" => "javascript:void(0)", 
    	"name" => "I am in Cyprus",
    	"image" => "whereto.jpg"
    ),
    array(
    	"src" => "javascript:void(0)", 
    	"name" => "I am in Jamaica",
    	"image" => "whereto.jpg"
    ),
    array(
    	"src" => "javascript:void(0)", 
    	"name" => "I am in Poland",
    	"image" => "whereto.jpg"
    ),
    array(
    	"src" => "javascript:void(0)", 
    	"name" => "I am in Zimbabwe",
    	"image" => "whereto.jpg"
    ),
    array(
    	"src" => "javascript:void(0)", 
    	"name" => "I am in Ukraine",
    	"image" => "whereto.jpg"
    ),
    array(
    	"src" => "javascript:void(0)", 
    	"name" => "I am in Netherlands",
    	"image" => "whereto.jpg"
    ),
    array(
    	"src" => "javascript:void(0)", 
    	"name" => "I am in India",
    	"image" => "whereto.jpg"
    ),
    array(
    	"src" => "javascript:void(0)", 
    	"name" => "I am in Singapore",
    	"image" => "whereto.jpg"
    ),
    array(
    	"src" => "javascript:void(0)", 
    	"name" => "I am in Morocco",
    	"image" => "whereto.jpg"
    ),
    array(
    	"src" => "javascript:void(0)", 
    	"name" => "I am in Egypt",
    	"image" => "whereto.jpg"
    ),
    array(
    	"src" => "javascript:void(0)", 
    	"name" => "I am in Brazil",
    	"image" => "whereto.jpg"
    ),
    array(
    	"src" => "javascript:void(0)", 
    	"name" => "I am in Australia",
    	"image" => "whereto.jpg"
    ),
    array(
    	"src" => "javascript:void(0)", 
    	"name" => "I am in Malaysia",
    	"image" => "whereto.jpg"
    ),
    array(
    	"src" => "javascript:void(0)", 
    	"name" => "I am in Indonesia",
    	"image" => "whereto.jpg"
    ),
    array(
    	"src" => "javascript:void(0)", 
    	"name" => "I am in Argentina",
    	"image" => "whereto.jpg"
    ),
    array(
    	"src" => "javascript:void(0)", 
    	"name" => "I am in Zimbabwe",
    	"image" => "whereto.jpg"
    ),
    array(
    	"src" => "javascript:void(0)", 
    	"name" => "I am in Macao",
    	"image" => "whereto.jpg"
    ),
    array(
    	"src" => "javascript:void(0)", 
    	"name" => "I am in Croatia",
    	"image" => "whereto.jpg"
    ),
    array(
    	"src" => "javascript:void(0)", 
    	"name" => "I am in Southkorea",
    	"image" => "whereto.jpg"
    )
);
?>