<div class="person_search_slide_xs side_modal left_side_modal leftsearchbarmodal">
   <div class="custom_side_header">
      <span class="slide_out_btn slide_out_right_btn close_side_slider waves-effect">
      <i class="zmdi zmdi-arrow-left"></i>
      </span>
      <h3>Search</h3>
   </div>
   <div class="side_modal_container side_modal_content">
      <div  class="custom_search group_modal_search">
         <div>
            <button class="close_message_search arrow_back_icon" >
            <i class="zmdi zmdi-arrow-left"></i>
            </button>
            <button class="close_message_search search_messages_icon" >
            <i class="zmdi zmdi-search"></i>
            </button>
            <input id="add_to_group_search"  class="custom_search_input" type="text" placeholder="Type name..." autocomplete="off" />
            <span class="remove_focus_text_icon">
            <i class="mdi mdi-close"></i>
            </span>
         </div>
      </div>
      <div class="suggested_person_addto_group">
         <a href="javascript:void(0)" data-id="user1" onclick="openThread(this)" class="add_to_group_container">
            <span class="add_to_group_personprofile">
            <img src="images/whoisaround-img.png"/>
            </span>
            <div class="add_to_group__personlabel">
               <p class="group_person_name" id="checkPerson0">Vipul Patel</p>
               <span><?=$st_nm_S?></span>
            </div>
         </a>
         <a href="javascript:void(0)" data-id="user2" onclick="openThread(this)" class="add_to_group_container">
            <span class="add_to_group_personprofile">
            <img src="images/whoisaround-img1.png"/>
            </span>
            <div class="add_to_group__personlabel">
               <p class="group_person_name" id="checkPerson0">Bhadresh Ramani</p>
               <span><?=$st_nm_S?></span>
            </div>
         </a>
         <a href="javascript:void(0)" data-id="user3" onclick="openThread(this)" class="add_to_group_container">
            <span class="add_to_group_personprofile">
            <img class="circle" src="images/demo-profile.jpg"/>
            </span>
            <div class="add_to_group__personlabel">
               <p class="group_person_name" id="checkPerson0">Vipul Patel, Nimish Parekh</p>
               <span><?=$st_nm_S?></span>
            </div>
         </a>
      </div>
   </div>
</div>
