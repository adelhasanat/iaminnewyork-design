<!--attachment modal-->
<div id="compose_visitcountry" class="modal compose_inner_modal modalxii_level1 visit-country">
   <div class="content_header">
      <button class="close_span waves-effect">
      <i class="mdi mdi-close mdi-20px material_close"></i>
      </button>
      <p class="selected_person_text">Select Country</p>
   </div>
   <div class="person_box">
      <div class="collection visit-country-list">
            <ul class="collection">
               <li class="collection-item avatar">
                  <img src="images/loc-icons/usa.jpg" alt="" class="circle">
                  <span class="title">
                     <a href="http://www.visit<?=$st_nm_L?>.com" target="_blank" class="collection-item">Visit <?=$st_nm_S?></a>
                  </span>
               </li>
               <li class="collection-item avatar">
                  <img src="images/loc-icons/chicago.jpg" alt="" class="circle">
                  <span class="title"><a href="https://www.choosechicago.com/" target="_blank" class="collection-item">Visit Chicago</a>
                  </span>
               </li>
               <li class="collection-item avatar">
                  <img src="images/loc-icons/london.jpg" alt="" class="circle">
                  <span class="title"><a href="https://www.visitlondon.com/" target="_blank" class="collection-item">Visit London</a>
                  </span>
               </li>
               <li class="collection-item avatar">
                  <img src="images/loc-icons/jordan.jpg" alt="" class="circle">
                  <span class="title"><a href="http://www.visitjordan.com/" target="_blank" class="collection-item">Visit Jodan</a>
                  </span>
               </li>
               <li class="collection-item avatar">
                  <img src="images/loc-icons/japan.jpg" alt="" class="circle">
                  <span class="title"><a href="https://us.jnto.go.jp" target="_blank" class="collection-item">Visit Japan</a>
                  </span>
               </li>
               <li class="collection-item avatar">
                  <img src="images/loc-icons/spain.jpg" alt="" class="circle">
                  <span class="title"><a href="http://www.tourspain.org" target="_blank" class="collection-item">Visit Spain</a>
                  </span>
               </li>
               <li class="collection-item avatar">
                  <img src="images/loc-icons/petra.jpg" alt="" class="circle">
                  <span class="title"><a href="https://www.visitpetra.jo/" target="_blank" class="collection-item">Visit Petra</a>
                  </span>
               </li>
               <li class="collection-item avatar">
                  <img src="images/loc-icons/france.jpg" alt="" class="circle">
                  <span class="title"><a href="http://ee.france.fr" target="_blank" class="collection-item">Visit France</a>
                  </span>
               </li>
               <li class="collection-item avatar">
                  <img src="images/whereto.jpg" alt="" class="circle">
                  <span class="title"><a href="http://na.visit<?=$st_nm_L?>.com/Wheretogo/Aqaba.aspx" target="_blank" class="collection-item">Visit Aqaba</a>
                  </span>
               </li>
               <li class="collection-item avatar">
                  <img src="images/whereto.jpg" alt="" class="circle">
                  <span class="title"><a href="https://rcu.gov.sa/" target="_blank" class="collection-item">Visit Madain</a>
                  </span>
               </li>
               <li class="collection-item avatar">
                  <img src="images/whereto.jpg" alt="" class="circle">
                  <span class="title"><a href="https://www.visitsaudi.com" target="_blank" class="collection-item">Visit Saudi</a>
                  </span>
               </li>
               <li class="collection-item avatar">
                  <img src="images/whereto.jpg" alt="" class="circle">
                  <span class="title"><a href="https://www.visitdubai.com" target="_blank" class="collection-item">Visit Dubai</a>
                  </span>
               </li>
               <li class="collection-item avatar">
                  <img src="images/whereto.jpg" alt="" class="circle">
                  <span class="title"><a href="https://www.visitqatar.qa" target="_blank" class="collection-item">Visit Qatar</a>
                  </span>
               </li>
               <li class="collection-item avatar">
                  <img src="images/whereto.jpg" alt="" class="circle">
                  <span class="title"><a href="https://visitbahrain.bh" target="_blank" class="collection-item">Visit Bahrain</a>
                  </span>
               </li>
               <li class="collection-item avatar">
                  <img src="images/whereto.jpg" alt="" class="circle">
                  <span class="title"><a href="https://visitabudhabi.ae" target="_blank" class="collection-item">Visit Abu Dhabi</a>
                  </span>
               </li>
               <li class="collection-item avatar">
                  <img src="images/whereto.jpg" alt="" class="circle">
                  <span class="title"><a href="https://www.visittheusa.com" target="_blank" class="collection-item">Visit USA</a>
                  </span>
               </li>
               <li class="collection-item avatar">
                  <img src="images/whereto.jpg" alt="" class="circle">
                  <span class="title"><a href="https://www.visititaly.eu" target="_blank" class="collection-item">Visit Italy</a>
                  </span>
               </li>
               <li class="collection-item avatar">
                  <img src="images/whereto.jpg" alt="" class="circle">
                  <span class="title"><a href="https://www.visitmexico.com" target="_blank" class="collection-item">Visit Mexico</a>
                  </span>
               </li>
               <li class="collection-item avatar">
                  <img src="images/whereto.jpg" alt="" class="circle">
                  <span class="title"><a href="https://visit.istanbul target="_blank" class="collection-item">Visit Istanbul</a>
                  </span>
               </li>
               <li class="collection-item avatar">
                  <img src="images/whereto.jpg" alt="" class="circle">
                  <span class="title"><a href="https://chinatour.net" target="_blank" class="collection-item">Visit China</a>
                  </span>
               </li>
               <li class="collection-item avatar">
                  <img src="images/whereto.jpg" alt="" class="circle">
                  <span class="title"><a href="https://www.germany.travel" target="_blank" class="collection-item">Visit Germany</a>
                  </span>
               </li>
               <li class="collection-item avatar">
                  <img src="images/whereto.jpg" alt="" class="circle">
                  <span class="title"><a href="https://www.tourismthailand.org" target="_blank" class="collection-item">Visit Thailand</a>
                  </span>
               </li>
               <li class="collection-item avatar">
                  <img src="images/whereto.jpg" alt="" class="circle">
                  <span class="title"><a href="https://www.austria.info" target="_blank" class="collection-item">Visit Austria</a>
                  </span>
               </li>
               <li class="collection-item avatar">
                  <img src="images/whereto.jpg" alt="" class="circle">
                  <span class="title"><a href="https://www.visithongkong.gov.hk" target="_blank" class="collection-item">Visit Hong Kong</a>
                  </span>
               </li>
               <li class="collection-item avatar">
                  <img src="images/whereto.jpg" alt="" class="circle">
                  <span class="title"><a href="http://www.visitgreece.gr" target="_blank" class="collection-item">Visit Greece</a>
                  </span>
               </li>
               <li class="collection-item avatar">
                  <img src="images/whereto.jpg" alt="" class="circle">
                  <span class="title"><a href="https://www.visitrussia.com" target="_blank" class="collection-item">Visit Russia</a>
                  </span>
               </li>
               <li class="collection-item avatar">
                  <img src="images/whereto.jpg" alt="" class="circle">
                  <span class="title"><a href="https://www.visitcyprus.com" target="_blank" class="collection-item">Visit Cyprus</a>
                  </span>
               </li>
               <li class="collection-item avatar">
                  <img src="images/whereto.jpg" alt="" class="circle">
                  <span class="title"><a href="https://www.visitjamaica.com" target="_blank" class="collection-item">Visit Jamaica</a>
                  </span>
               </li>
               <li class="collection-item avatar">
                  <img src="images/whereto.jpg" alt="" class="circle">
                  <span class="title"><a href="https://www.visitpoland.com" target="_blank" class="collection-item">Visit Poland</a>
                  </span>
               </li>
               <li class="collection-item avatar">
                  <img src="images/whereto.jpg" alt="" class="circle">
                  <span class="title"><a href="https://www.zimbabwetourism.net" target="_blank" class="collection-item">Visit Zimbabwe</a>
                  </span>
               </li>
               <li class="collection-item avatar">
                  <img src="images/whereto.jpg" alt="" class="circle">
                  <span class="title"><a href="http://visittoukraine.com/en" target="_blank" class="collection-item">Visit Ukraine</a>
                  </span>
               </li>
               <li class="collection-item avatar">
                  <img src="images/whereto.jpg" alt="" class="circle">
                  <span class="title"><a href="https://www.holland.com/" target="_blank" class="collection-item">Visit Netherlands</a>
                  </span>
               </li>
               <li class="collection-item avatar">
                  <img src="images/whereto.jpg" alt="" class="circle">
                  <span class="title"><a href="https://www.indianvisit.com/" target="_blank" class="collection-item">Visit India</a>
                  </span>
               </li>
               <li class="collection-item avatar">
                  <img src="images/whereto.jpg" alt="" class="circle">
                  <span class="title"><a href="https://www.visitsingapore.com/en/" target="_blank" class="collection-item">Visit Singapore</a>
                  </span>
               </li>
               <li class="collection-item avatar">
                  <img src="images/whereto.jpg" alt="" class="circle">
                  <span class="title"><a href="http://www.tourism.gov.pk/" target="_blank" class="collection-item">Visit Pakistan</a>
                  </span>
               </li>
               <li class="collection-item avatar">
                  <img src="images/whereto.jpg" alt="" class="circle">
                  <span class="title"><a href="https://www.visitmorocco.com/en" target="_blank" class="collection-item">Visit Morocco</a>
                  </span>
               </li>
               <li class="collection-item avatar">
                  <img src="images/whereto.jpg" alt="" class="circle">
                  <span class="title"><a href="http://www.egypt.travel/" target="_blank" class="collection-item">Visit Egypt</a>
                  </span>
               </li>
               <li class="collection-item avatar">
                  <img src="images/whereto.jpg" alt="" class="circle">
                  <span class="title"><a href="https://www.visitbrasil.com/" target="_blank" class="collection-item">Visit Brazil</a>
                  </span>
               </li>
               <li class="collection-item avatar">
                  <img src="images/whereto.jpg" alt="" class="circle">
                  <span class="title"><a href="https://www.australia.com/en" target="_blank" class="collection-item">Visit Australia</a>
                  </span>
               </li>
               <li class="collection-item avatar">
                  <img src="images/whereto.jpg" alt="" class="circle">
                  <span class="title"><a href="http://www.visit-malaysia.com/" target="_blank" class="collection-item">Visit Malaysia</a>
                  </span>
               </li>
               <li class="collection-item avatar">
                  <img src="images/whereto.jpg" alt="" class="circle">
                  <span class="title"><a href="https://www.indonesia.travel/gb/en/home" target="_blank" class="collection-item">Visit Indonesia</a>
                  </span>
               </li>
               <li class="collection-item avatar">
                  <img src="images/whereto.jpg" alt="" class="circle">
                  <span class="title"><a href="https://www.argentina.travel/" target="_blank" class="collection-item">Visit Argentina</a>
                  </span>
               </li>
               <li class="collection-item avatar">
                  <img src="images/whereto.jpg" alt="" class="circle">
                  <span class="title"><a href="https://www.zimbabwetourism.net/" target="_blank" class="collection-item">Visit Zimbabwe</a>
                  </span>
               </li>
               <li class="collection-item avatar">
                  <img src="images/whereto.jpg" alt="" class="circle">
                  <span class="title"><a href="https://www.visitmacao.com.au/" target="_blank" class="collection-item">Visit Macao</a>
                  </span>
               </li>
               <li class="collection-item avatar">
                  <img src="images/whereto.jpg" alt="" class="circle">
                  <span class="title"><a href="https://www.visit-croatia.co.uk/" target="_blank" class="collection-item">Visit Croatia</a>
                  </span>
               </li>
               <li class="collection-item avatar">
                  <img src="images/whereto.jpg" alt="" class="circle">
                  <span class="title"><a href="http://english.visitkorea.or.kr/enu/index.kto" target="_blank" class="collection-item">Visit Southkorea</a>
                  </span>
               </li>
            </ul>
      </div>
   </div>
</div>
