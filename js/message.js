/*=========================================================================================
---------------------------- Code from modal.js file -------------------------
===========================================================================================*/
//**************FUN for message page**************

//FUN for contact info modal
function contactInfo() {
	$('.contact_info_modal').addClass('showsidemodal_right');
	$('.contact_info_modal').removeClass('hidesidemodal_right');
}

//FUN for dicard modal 

function muteAction() {
	$('#mute_modal').modal('open');
}
function archiveChat() {
	$('#Archieve_modal').modal('open');
}
function UnreadMessages() {
	$('#UnreadMessages_modal').modal('open');
}
function DeleteChat() {
	$('#DeteletChat_modal').modal('open');
}

//Fun for message selectMessageBox
if ($(window).width() <= 568) {
	$('.msgdetail-box .descholder p').click(function () {
		$(this).css('background-color', '#e0dfdf');
		selectMessageBox();
	})

}

//FUN for add person for new Group slide in message
function addPerson() {
	$('.add_person_newgroup').removeClass('showinnersidemodal');
	$('.add_person_newgroup').addClass('hideinnersidemodal');
}
function closeInnerSlide() {
	$('.add_person_newgroup').removeClass('hideinnersidemodal');
	$('.add_person_newgroup').addClass('showinnersidemodal');
}
function savedPersonMessages() {
	$('.person_message_save_modal').removeClass('showsidemodal_right');
	$('.person_message_save_modal').addClass('hidesidemodal_right');
}
function closeInnerRightSlide() {
	$('.person_message_save_modal').removeClass('hidesidemodal_right');
	$('.person_message_save_modal').addClass('showsidemodal_right');
}
//**FUN for new groud modal
function new_group_action() {
	$('.new_group_modal').removeClass('showsidemodal');
	$('.new_group_modal').addClass('hidesidemodal');
}; 
function add_person_group() {
	$('.add_to_group_modal').removeClass('showsidemodal_right');
	$('.add_to_group_modal').addClass('hidesidemodal_right');
};
//****Fun for add to group modal
function add_to_group_close() {
	$('.add_to_group_modal').removeClass('hidesidemodal_right');
	$('.add_to_group_modal').addClass('showsidemodal_right');
	$('.search_messages_icon').show();
	$('.arrow_back_icon').hide();
	$('.remove_focus_text_icon').hide();
	$('.custom_search_input').val("");
}
function add_to_group_done() {
	$('.add_to_group_modal').removeClass('hidesidemodal_right');
	$('.add_to_group_modal').addClass('showsidemodal_right');
	$('.search_messages_icon').show();
	$('.arrow_back_icon').hide();
	$('.remove_focus_text_icon').hide();
	$('.custom_search_input').val("");
}
$('.group_name_text').hide();

//**FUN for archieved modal
function archieved_action() {
	$('.archieved_chat_modal').removeClass('showsidemodal');
	$('.archieved_chat_modal').addClass('hidesidemodal');
}
//**FUN for message save modal
function message_save_action() {
	$('.message_save_modal').removeClass('showsidemodal');
	$('.message_save_modal').addClass('hidesidemodal');
};
//**FUN setting message modal
function message_setting_action() {
	$('.msg_setting_modal').removeClass('showsidemodal');
	$('.msg_setting_modal').addClass('hidesidemodal');
};
//**FUN for blocked contact modal
function blocked_contact_action() {
	$('.blocked_contact_modal').removeClass('showsidemodal');
	$('.blocked_contact_modal').addClass('hidesidemodal');
};
//FUN for selected msg box
function selectMessageBox() {
	$('.selected_messages_box').show();
	$('.select_msg_checkbox').show();
	$('.addnew-msg').hide();
	$('.attachment_add_icon').hide();
	$('.msgdetail-box .imgholder img').hide();
}


function closeSelectedMessage() {
	$('.selected_messages_box').hide();
	$('.select_msg_checkbox').hide();
	$('.addnew-msg').show();
	$('.attachment_add_icon').show();
	$('.msgdetail-box .imgholder img').show();
	$('.msgdetail-box .descholder p').css('background-color', '#f3f3f3');
}

//send message icon 
if ($(window).width() <= 568) {
	$('#inputMessageWall').focus(function () {
		$('.sendMessageBtn').show();
	});
}

function personSearchSlide() {
	$('.person_search_slide_xs').removeClass('showsidemodal');
	$('.person_search_slide_xs').addClass('hidesidemodal');
}

function messageSearchSlide() {

	$('.msgsearch_messages_modal').removeClass('showsidemodal');
	$('.msgsearch_messages_modal').addClass('hidesidemodal');
}

function closeMessageSearchSlide() {
	$('.search_messages_modal').removeClass('hidesidemodal_right');
	$('.search_messages_modal').addClass('showsidemodal_right');
}
//FUN of create group action

function create_group() {
	$('.create_group_modal').removeClass('showsidemodal_right');
	$('.create_group_modal').addClass('hidesidemodal_right');
};
//FUN for add person for new Group slide in message
function CreateGroupaddPerson() {
	$('.add_person_creategroup').removeClass('showinnersiderightmodal');
	$('.add_person_creategroup').addClass('hideinnersiderightmodal');
}
function add_to_createdgroup_done() {
	$('.add_person_creategroup').removeClass('hideinnersiderightmodal');
	$('.add_person_creategroup').addClass('showinnersiderightmodal');
	$('.search_messages_icon').show();
	$('.arrow_back_icon').hide();
	$('.remove_focus_text_icon').hide();
	$('.custom_search_input').val("");
}
function add_to_Creategroup_close() {
	$('.create_group_modal').removeClass('hidesidemodal_right');
	$('.create_group_modal').addClass('showsidemodal_right');
	$('.search_messages_icon').show();
	$('.arrow_back_icon').hide();
	$('.remove_focus_text_icon').hide();
	$('.custom_search_input').val("");
	$('.group_name_edit').show();
	$('.group_name_remove').hide();
	$('.group_name_text input').val("");
	$('.group_name_text').hide();
}

//FUN of add group action
function group_name_edit() {
	$('.group_name_text').slideDown();
	$('.group_name_text input').focus();
	$('.group_name_edit').hide();
	$('.group_name_remove').show();
}
function group_name_remove() {
	$('.group_name_edit').show();
	$('.group_name_remove').hide();
	$('.group_name_text input').val("");
	$('.group_name_text').hide();

}
function ProfileNameChange() {
	$('.setting_profile_name').slideDown();
	$('.setting_profile_name input').focus();
}
//FUN of add block action
function AddBlockModal() {
	$('.add_to_block_modal').removeClass('showinnersidemodal');
	$('.add_to_block_modal').addClass('hideinnersidemodal');
}
function BlockCancel() {
	$('.add_to_block_modal').removeClass('hideinnersidemodal');
	$('.add_to_block_modal').addClass('showinnersidemodal');
	$('.search_messages_icon').show();
	$('.arrow_back_icon').hide();
}

//FUN of request message action
function MessageRequest() {
	$('.message_request_modal').removeClass('showsidemodal');
	$('.message_request_modal').addClass('hidesidemodal');

}
function PersonBoxClose() {

	$('.person_search_slide_xs').addClass('showsidemodal');
	$('.person_search_slide_xs').removeClass('hidesidemodal');
}

function giftModalAction() {
	//setModelGiftMaxHeight();
	
	$('.gift_modal').modal('open');
}

function gift_modal() {
	$('.gift_modal').modal('close');
}
//FUN for user gift modal
function usegift_modal() {

	$('.usegift_modal').modal('close');
}


//group info modal
function CloseGroupInfo() {
	$('.group_info_modal').removeClass('hidesidemodal_right');
	$('.group_info_modal').addClass('showsidemodal_right');
	$('.group_name_edit').show();
	$('.group_name_remove').hide();
	$('.group_name_text input').val("");
	$('.group_name_text').hide();
}

/* ------------------ */

/*=======================================================================================
---------------------------- Code from custom-functions.js file -------------------------
=======================================================================================*/

/************ MESSAGE FUNCTIONS ************/
	$(window).resize(function() {
		$w = $(window).width();
		if($w > 799) {
			$('.messages-wrapper').addClass('bigscreen');
		} else {
			$('.messages-wrapper').removeClass('bigscreen');
		}
		closeattachmentbox();
		setMsgTextArea();
		generalCaseStudy();
	});
	
	$(document).ready(function() {
		$w = $(window).width();
		if($w > 799) {
			$('.messages-wrapper').addClass('bigscreen');
		} else {
			$('.messages-wrapper').removeClass('bigscreen');
		}
		
		// js to show/hide send msg icon
		$(".btn-msg-send").hide();
		$("#inputMessageWall").keyup(function(){
		    if($(this).val().length>0){
		        $(".btn-msg-send").show("100");
		    }else{$(".btn-msg-send").hide("500");}
		});

		//setModelGift_sendMaxHeight();
		//FUN for msg section height 
		
		//gift modal carousel
		$('.carousel.carousel-slider').carousel({
			fullWidth: true
		});

		$('.pac-container:nth-child(2)').css('width', '270px');

		// move next carousel
		$('.moveNextCarousel').click(function(e) {
			e.preventDefault();
			e.stopPropagation();
			$('.carousel').carousel('next');
		});

		// move prev carousel
		$('.movePrevCarousel').click(function(e) {
			e.preventDefault();
			e.stopPropagation();
			$('.carousel').carousel('prev');
		});

		//**FUN for message page side modals js
		var custom_side_header_height = $('.custom_side_header').height();

		var mobile_header_height = $('.mobile-footer').height();
		var msg_side_modal = $(window).height();
		//$('.side_modal').css('height', msg_side_modal);

		//****Fun for left open modal
		$('.slide_out_btn').click(function() {
			$('.left_side_modal').removeClass('hidesidemodal');
			$('.left_side_modal').addClass('showsidemodal');
		});

		//****Fun for right open modal
		/*$('.slide_out_right_btn').click(function() {
			$('.right_side_modal').removeClass('hidesidemodal_right');
			$('.right_side_modal').addClass('showsidemodal_right');
			$('.search_messages_icon').show();
			$('.arrow_back_icon').hide();
			$('.remove_focus_text_icon').hide();
			$('.custom_search_input').val("");
			$('.group_name_text').slideUp();
			$('.group_name_text input').val("");
			$('.setting_profile_name').slideUp();
			$('.setting_profile_name input').val("");
		});*/

		//**FUN search messages modal
		$(document).on('click', '#search_messages_Action', function() {
		  openCustomSearchMain();
		  /*$('.search_messages_modal').removeClass('showsidemodal_right');
		  $('.search_messages_modal').addClass('hidesidemodal_right');*/
		});

		//FUN for search box
		$('.msg_search_box .custom_search_input').focus(function() {
			$('.msg_search_box .search_messages_icon').hide();
			$('.msg_search_box .arrow_back_icon').show();
		});
		$('.msg_search_box .arrow_back_icon').click(function() {
			$('.msg_search_box .search_messages_icon').show();
			$('.msg_search_box .arrow_back_icon').hide();
			$('.msg_search_box .remove_focus_text_icon').hide();
			$('.msg_search_box .custom_search_input').blur();
			$('.msg_search_box .custom_search_input').val("");
		});
		$('.msg_search_box .custom_search_input').keyup(function() {
			if ($('.msg_search_box .custom_search_input').val() != "") {
				$('.msg_search_box .remove_focus_text_icon').show();
			} else {
				$('.msg_search_box .remove_focus_text_icon').hide();
			}
		});
		$('.msg_search_box .remove_focus_text_icon').click(function() {
			$('.msg_search_box .custom_search_input').val("");
		});

		//FUN for find person search box
		$('.findperson_search_box .custom_search_input').focus(function() {
			$('.findperson_search_box .search_messages_icon').hide();
			$('.findperson_search_box .arrow_back_icon').show();
		});
		$('.findperson_search_box .arrow_back_icon').click(function() {
			$('.findperson_search_box .search_messages_icon').show();
			$('.findperson_search_box .arrow_back_icon').hide();
			$('.findperson_search_box .remove_focus_text_icon').hide();
			$('.findperson_search_box .custom_search_input').blur();
			$('.findperson_search_box .custom_search_input').val("");
		});
		$('.findperson_search_box .custom_search_input').keyup(function() {
			if ($('.findperson_search_box .custom_search_input').val() != "") {
				$('.findperson_search_box .remove_focus_text_icon').show();
			} else {
				$('.findperson_search_box .remove_focus_text_icon').hide();
			}
		});
		$('.findperson_search_box .remove_focus_text_icon').click(function() {
			$('.findperson_search_box .custom_search_input').val("");
		})
		$('.findperson_search_box .remove_focus_text_icon').click(function() {
			$('.findperson_search_box .remove_focus_text_icon').hide();
		});


		//**FUN for add to groud modal
		//$('#add_to_group_Action').click(function () {
		//    $('.add_to_group_modal').removeClass('showsidemodal_right');
		//    $('.add_to_group_modal').addClass('hidesidemodal_right');
		//});
		//FUN for search box
		$('.group_modal_search .custom_search_input').focus(function() {
			$('.group_modal_search .search_messages_icon').hide();
			$('.group_modal_search .arrow_back_icon').show();
		});
		$('.group_modal_search .arrow_back_icon').click(function() {
			$('.group_modal_search .search_messages_icon').show();
			$('.group_modal_search .arrow_back_icon').hide();
			$('.group_modal_search .remove_focus_text_icon').hide();
			$('.group_modal_search .custom_search_input').blur();
			$('.group_modal_search .custom_search_input').val("");
		});
		$('.group_modal_search .custom_search_input').keyup(function() {
			if ($(this).val() != "") {

				$('.group_modal_search .remove_focus_text_icon').show();
			} else {

				$('.group_modal_search .remove_focus_text_icon').hide();
			}
		});
		$('.group_modal_search .remove_focus_text_icon').click(function() {
			$('.group_modal_search .custom_search_input').val("");
		})

		var add_to_group_height = $('.add_to_group_modal').height();
		//$('.suggested_person_addto_group').css('height', add_to_group_height - 246);


		//**FUN setting message modal

		var msg_setting_height = $('.msg_setting_modal').height();
		//$('.msg_setting_content').css('height', add_to_group_height - custom_side_header_height);

		//**FUN for blocked contact modal

		var block_contact_hight = $('.blocked_contact_modal').height();
		//$('.block_contact_container').css('height', add_to_group_height - custom_side_header_height);


		var new_group_hight = $('.new_group_modal').height();
		//$('.new_group_content').css('height', new_group_hight - custom_side_header_height);

		//**FUN for contact info modal

		var contact_info_hight = $('.contact_info_modal').height();
		//$('.contact_info_container').css('height', contact_info_hight - custom_side_header_height);


		//**FUN for group info modal
		$('.group_info_action').click(function() {
			$('.group_info_modal').removeClass('showsidemodal_right');
			$('.group_info_modal').addClass('hidesidemodal_right');
		});

		$('.contact_info_action').click(function() {
			$('.contact_info_modal').removeClass('showsidemodal_right');
			$('.contact_info_modal').addClass('hidesidemodal_right');
		});

		//var msgWindowXs = $(window).height();
		//$('.main-msgwindow').css('height', msgWindowXs - 198)

		//**FUN of create group modal
		var create_group_hight = $('.create_group_modal').height();
		//$('.create_group_content').css('height', create_group_hight - custom_side_header_height);

		//FUN for Block side box to remove person from block
		$('.blocked_remove').click(function() {
			$(this).parents('.blocked_person_box').hide();
		});

		//Fun of gift modal js
		if ($(window).width() >= 568) {
			var stickers_height = $(window).height();
			//$('.emostickers').css('height', stickers_height - 105)
		}

		/* manage message list for mobile */
			manageMessagePage();				
		/* end manage message list for mobile */
		
		/* manage message dateshower */			
		$('.msgdetail-list').on("scroll resize", function(){
			
			var orgPos=0;
			var dateCurrText = "";

			var pos=$('.dateshower').offset();
			var count=0;
			$('.date-divider').each(function(){
				count++;
				if(count==1){
					orgPos = $(this).offset().top;
				}
				if(pos.top >= $(this).offset().top && pos.top >= $(this).prev().offset().top)
				{
					if(dateCurrText != $(this).html()){
						dateCurrText = $(this).html();
						$('.dateshower').html($(this).html()); //or any other way you want to get the date
					}
					return; //break the loop
				}
				if(pos.top <= orgPos ){
					dateCurrText = "";
					$('.dateshower').html(dateCurrText); //or any other way you want to get the date
				}
				
			});
		});			
		/* end manage message dateshower */	
		
		$('#addnewchat').click(function(){
			 $('.messages-page').find('.messages-list').addClass("newmsg-mode");	 
		  });
		  
		  $('#canceladdchat').click(function(){
			closeAddNewMsg();	
		  });
		  
		  $('#doneaddchat').click(function(){
			 
			closeAddNewMsg();

		});

		setMsgTextArea();
		generalCaseStudy();
	});

/* open-close message */
	function resetAllMessageLink(){
		$(".message-userlist ul > li").each(function(){
			var atag=$(this).find("a");
			if(atag.hasClass("active")) atag.removeClass("active");
		});
	}
	function resetMessageHolder(){
		$(".current-messages li").removeClass("active");
	}
	function openThread(obj){
		var userid=$(obj).attr("data-id");
		PersonBoxClose();
		openMessage("#"+userid);
	}		
	function openMessage(obj){
	
		if ($(window).width() <= 799) {
			$('.search-xs-btn').hide();
			$('.globel_setting').hide();
			$('.messagesearch-xs-btn').show();
			$('.person_dropdown').show();
		}
	
		resetAllMessageLink();
		$(obj).addClass("active");
		
		var userid=$(obj).attr("id");
		
		/* AJAX call to replace messages part */
		var exists=false;
		$(".current-messages li.mainli").each(function(){
			var li_id=$(this).attr("id");
			li_id=li_id.replace("li-","");
			
			if(li_id==userid){
				exists=true;				
			}
		});
		if(exists){
			resetMessageHolder();
			$(".current-messages li#li-"+userid).addClass("active");
		}
		else{
			// generate new li
			resetMessageHolder();
			if(userid=="ad"){
				$(".right-section .topstuff .msgwindow-name .desc-holder").html("Puma Shoes <span>Sponsored Ad</span>");
				$(".allmsgs-holder").parents(".right-section").addClass("inbox-advert");			
			}
			else{
				$(".right-section .topstuff .msgwindow-name .desc-holder").html($(obj).find("h6").html());
				var chatimg = $(obj).find(".imgholder").html();
				$(".right-section .topstuff .msgwindow-name .imgholder").html(chatimg);
				
				if($(".allmsgs-holder").parents(".right-section").hasClass("inbox-advert"))
					$(".allmsgs-holder").parents(".right-section").removeClass("inbox-advert");
				
			}
			if(userid=="ad"){
				$(".current-messages").append("<li class='mainli active' id='li-"+userid+"'> <div class='msgdetail-list nice-scroll'><div class='inbox-travad'><img src='images/inboxad-img.png'/><h6>Special discount this christmas</h6><p>On this christmas, we are bringing you flat 20% discount on our latest shoe ranges! Enjoy your christmas shopping with Puma.</p><a href='javascript:void(0)' class='btn btn-primary btn-sm right'>Shop Now</a></div></div></li>");
				
			}
			else{
				$(".current-messages").append("<li class='mainli active' id='li-"+userid+"'><div class='msgdetail-list nice-scroll'><div class='msglist-holder images-container'><ul class='outer'><li class='msgli receivedmsg-income'><div class='checkbox-holder'><div class='h-checkbox entertosend msg-checkbox'><input type='checkbox' id='test1'><label>&nbsp;</label></div></div><div class='msgdetail-box'><div class='imgholder'>"+chatimg+"</div><div class='descholder'><div class='msg-handle'><p>Hi, Adel How are you? <span class='timestamp'>1:30 PM</span></p><span class='select_msg_checkbox'><input type='checkbox' class='filled-in' id='select_msg1' /><label for='select_msg1'></label></span><span class='settings-icon'><a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='chatmsg_"+userid+"_1_setting'><i class='mdi mdi-dots-horizontal'></i></a><ul id='chatmsg_"+userid+"_1_setting' class='dropdown-content custom_dropdown individiual_chat_setting' style='opacity: 1; left: -204px; position: absolute; top: 9px; display: none;'><li><a>Reply</a></li><li><a>Forward message</a></li><li><a>Star message</a></li><li><a>Delete message</a></li></ul></span></div><div class='msg-handle'><p>I hope you’d have gone through my previous message. We’d like to discuss this project in details so that we can nullify your concerns. <span class='timestamp'>1:30 PM</span></p><span class='select_msg_checkbox'><input type='checkbox' class='filled-in' id='select_msg2' /><label for='select_msg2'></label></span><span class='settings-icon'><a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='chatmsg_"+userid+"_2_setting'><i class='mdi mdi-dots-horizontal'></i></a><ul id='chatmsg_"+userid+"_2_setting' class='dropdown-content custom_dropdown individiual_chat_setting' style='opacity: 1; left: -204px; position: absolute; top: 9px; display: none;'><li><a>Reply</a></li><li><a>Forward message</a></li><li><a>Star message</a></li><li><a>Delete message</a></li></ul></span></div></div></div></li><li class='date-divider'><span>31 Jan 2016</span></li><li class='msgli received msg-income'><div class='checkbox-holder'><div class='h-checkbox entertosend msg-checkbox'><input type='checkbox' id='test1'><label>&nbsp;</label></div></div><div class='msgdetail-box'><div class='imgholder'>"+chatimg+"</div><div class='descholder'><div class='msg-handle'><p> Hi, Adel How are you?	<br /> I hope you’d have gone through my previous message. We’d like to discuss this project in details so that we can nullify your concerns. <span class='timestamp'>1:30 PM</span></p><span class='select_msg_checkbox'><input type='checkbox' class='filled-in' id='select_msg3' /><label for='select_msg3'></label></span><span class='settings-icon'><a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='chatmsg_"+userid+"_3_setting'><i class='mdi mdi-dots-horizontal'></i></a><ul id='chatmsg_"+userid+"_3_setting' class='dropdown-content custom_dropdown individiual_chat_setting' style='opacity: 1; left: -204px; position: absolute; top: 9px; display: none;'><li><a>Reply</a></li><li><a>Forward message</a></li><li><a>Star message</a></li><li><a>Delete message</a></li></ul></span></div></div></div></li><li class='msgli msg-outgoing'><div class='checkbox-holder'>	 <div class='h-checkbox entertosend msg-checkbox'><input type='checkbox' id='test1'><label>&nbsp;</label></div></div><div class='msgdetail-box'><div class='descholder'><div class='msg-handle'><img class='msg-status' src='images/msg-read.png'><span class='msg-img'><figure><a href='images/msg2.jpg' data-size='1600x1600' data-med='images/msg2.jpg' data-med-size='1024x1024' data-author='Folkert Gorter'><img src='images/msg2.jpg'/></a><a href='javascript:void(0)' class='download'><i class='mdi mdi-download'></i></a><span class='timestamp'>1:30 PM</span></figure></span><span class='select_msg_checkbox'><input type='checkbox' class='filled-in' id='select_msg4' /><label for='select_msg4'></label></span><span class='settings-icon'><a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='chatmsg_"+userid+"_4_setting'><i class='mdi mdi-dots-horizontal'></i></a><ul id='chatmsg_"+userid+"_4_setting' class='dropdown-content custom_dropdown individiual_chat_setting' style='opacity: 1; left: -204px; position: absolute; top: 9px; display: none;'><li><a>Reply</a></li><li><a>Forward message</a></li><li><a>Star message</a></li><li><a>Delete message</a></li></ul></span></div></div></div></li></ul></div></div></li>");
				
			}
		}		
		
		var isMblMessagePage=$(".main-content").hasClass("mblMessagePage");
		if(isMblMessagePage){
			$(".messages-list").find(".left-section").addClass("hidden");
			if($(".messages-list").find(".right-section").hasClass("hidden")){
				$(".messages-list").find(".right-section").removeClass("hidden");
				$(".messages-list").find(".right-section").addClass("shown");
				var getusername = $('.msgwindow-name').find('.desc-holder').html();
				
				$(".header-themebar .mbl-innerhead").show();
				$(".header-themebar .mbl-innerhead").find('.page-name').html(getusername);
				$('.mobile-footer').hide();
				$(".mblMessagePage").addClass("innerpage");				
			}
		}		
		
		var isBusinessPage=$(".page-wrapper").hasClass("businesspage");
		if(isBusinessPage){
			var isMblMessageTab=$(".messages-content").hasClass("mblMessagePage");
			if(isMblMessageTab){
				$(".messages-list").find(".left-section").addClass("hidden");
				if($(".messages-list").find(".right-section").hasClass("hidden")){
					$(".messages-list").find(".right-section").removeClass("hidden");
					$(".messages-list").find(".right-section").addClass("shown");
					var getusername = $('.msgwindow-name').find('.desc-holder').html();
					
					$(".header-themebar .mbl-innerhead").show();
					$(".header-themebar .mbl-innerhead").find('.page-name').html(getusername);
					$('.mobile-footer').hide();
					$(".mblMessagePage").addClass("innerpage");				
				}				
			}
		}
		
		scrollMessageBottom();
		initDropdown();
		setMsgTextArea();
		generalCaseStudy();
	}
	function openNewMessagemobile(obj){		
	
		resetAllMessageLink();
	 
		resetMessageHolder();
		
		$(".allmsgs-holder .newmessage").show();
		
		var isMblMessagePage=$(".main-content").hasClass("mblMessagePage");
		if(isMblMessagePage){
			$(".messages-list").find(".left-section").addClass("hidden");
			if($(".messages-list").find(".right-section").hasClass("hidden")){
				$(".messages-list").find(".right-section").removeClass("hidden");
				$(".messages-list").find(".right-section").addClass("shown");
				
				$(".header-themebar .mbl-innerhead").show();
				$(".header-themebar .mbl-innerhead").find('.page-name').html("New Message");
				
				$('.mobile-footer').hide();
				$(".mblMessagePage").addClass("innerpage");
			}
		}
		$('.messages-page').find('.messages-list').addClass("newmsg-mode");
		
		var isBusinessPage=$(".page-wrapper").hasClass("businesspage");
		if(isBusinessPage){
			var isMblMessageTab=$(".messages-content").hasClass("mblMessagePage");
			if(isMblMessageTab){
				$(".messages-list").find(".left-section").addClass("hidden");
				if($(".messages-list").find(".right-section").hasClass("hidden")){
					$(".messages-list").find(".right-section").removeClass("hidden");
					$(".messages-list").find(".right-section").addClass("shown");
					
					$(".header-themebar .mbl-innerhead").show();
					$(".header-themebar .mbl-innerhead").find('.page-name').html("New Message");
					
					$('.mobile-footer').hide();
					$(".mblMessagePage").addClass("innerpage");
				}				
			}
		}
		
	}
	
	function closeMessage(obj){
		
		var isMblMessagePage;
		
		var isBusinessPage=$(".page-wrapper").hasClass("businesspage");
		if(isBusinessPage){
			isMblMessageTab=$(".messages-content").hasClass("mblMessagePage");
		}else{
			isMblMessagePage=$(".main-content").hasClass("mblMessagePage");
		}
		
		if(isMblMessagePage){
			$(".messages-list").find(".right-section").addClass("hidden");
			if($(".messages-list").find(".left-section").hasClass("hidden")){
				$(".messages-list").find(".left-section").removeClass("hidden");
				$(".messages-list").find(".left-section").addClass("shown");
				$('.page-name').html('Home');
				$('.gotohome').find('a').html('<i class="mdi mdi-menu"></i>');
				$('.gotohome').find('a').removeAttr('onclick');
				$('.gotohome').find('a').attr('href', 'messages.html');
				$('.mobile-footer').show();
			}			
		}
		
	}	
/* end open-close message */

/* FUN manage mute conversation */
	function deleteConversation(){
		hideMsgPhotos();
		var org_liid = $(".current-messages").find("li.active").attr("id");
		org_liid = org_liid.replace("li-","");
		
		$(".current-messages li.active").remove();
		
		$(".message-userlist li").find("a[id='"+org_liid+"']").parents("li").remove();
		
		if($(".current-messages").find("li").length > 0){
			var liid = $(".current-messages").find("li").attr("id");
			liid = liid.replace("li-","");
			
			$(".message-userlist li").find("a[id='"+liid+"']").trigger("click");
		}
		
	}
	function manageMuteConverasion(doWhat){		
		hideMsgPhotos();
		if(doWhat=="mute"){					
			if($(".block-notice").css("display")=="none"){
				$(".mute-notice").slideDown().delay(3000).slideUp();
				var settingObj=$(".mute-setting");
				if(settingObj!=undefined){
					settingObj.html("Unmute Coversation");
					settingObj.attr("onclick","manageMuteConverasion('unmute')");
				}
			}
			// add code to mute conversation
		}else{			
			if($(".block-notice").css("display")=="none"){
				$(".mute-notice").hide();
				var settingObj=$(".mute-setting");
				if(settingObj!=undefined){
					settingObj.html("Mute Coversation");
					settingObj.attr("onclick","manageMuteConverasion('mute')");
				}
			}
			// add code to unmute conversation
		}
	}
/* FUN end manage mute conversation */

/* FUN manage block conversation */
	function manageBlockConverasion(doWhat){		
		hideMsgPhotos();
		if(doWhat=="block"){						
			$(".block-notice").slideDown();
			var settingObj=$(".block-setting");
			if(settingObj!=undefined){
				settingObj.html("Unblock Messages");
				settingObj.attr("onclick","manageBlockConverasion('unblock')");
			}
			// add code to mute conversation
		}else{			
			$(".block-notice").hide();
			var settingObj=$(".block-setting");
			if(settingObj!=undefined){
				settingObj.html("Block Messages");
				settingObj.attr("onclick","manageBlockConverasion('block')");
			}
			// add code to unmute conversation
		}
	}
/* FUN end manage block conversation */

/* FUN manage message checkbox */
	function showMsgCheckbox(){
		hideMsgPhotos();
		$(".main-msgwindow").addClass("have_checkbox");
	}
	function hideMsgCheckbox(){
		$(".main-msgwindow").removeClass("have_checkbox");
	}
/* FUN end manage message checkbox */

/* FUN manage photo thread */
	function showMsgPhotos(){
		$(".main-msgwindow").addClass("have_photos");
		fixMessageImagesPopup();
	}
	function hideMsgPhotos(){
		$(".main-msgwindow").removeClass("have_photos");
	}
/* FUN end manage photo thread */

/* FUN manage emoticons */
	function changeEmoCategory(obj){
		
		var esCat=$(obj).val();
		manageEmoStickersBox(esCat);
	}
	function manageEmoStickersBox(esCat){
		// show scrollbar
		$(".giftlist-popup").find(".nice-scroll.emostickers").niceScroll({horizrailenabled:false,cursorcolor:"#bbb",cursorwidth:"8px",cursorborderradius:"0",cursorborder:"0px solid #fff",background:"rgba(255,255,255,0.6)"});
		$(".giftlist-popup").find(".nice-scroll.emostickers").getNiceScroll(0).rail.addClass('popupScroller');
		
		var catname = esCat;
		var sparent=$(".giftlist-popup").find(".emotion-holder");
		initNiceScroll(".nice-scroll");
		
		//ajax call.... fire and get emotions for particular category..
		var $faces="";
		$faces = ['(wine)','(icecream)','(coffee)','(heart)','(flower)','(cake)','(handshake)','(gift)','(goodmorning)','(goodnight)','(backpack)','(parasailing)','(train)','(flipflop)','(airplane)','(sunbed)','(happyhalloween)','(merrychristmas)','(eidmubarak)','(happyyear)','(happymothers)','(happyfathers)','(happyaniversary)','(happybirthday)'];
		
		$(".emostickers-box ul.emostickers-list").html("");
		if($faces.length!=0){
			
			$.each($faces, function(i, v){
				
				var faces = $.emostickers.replace(v);			
				$(".giftlist-popup").find(".emostickers-box").find("ul.emostickers-list").append('<li><a href="javascript:void(0)" data-class="'+v+'" onclick="useGift(\''+ v + '\',\''+catname+'\',\'editmode\');">'+faces+'</a></li>');
			});
			
		}else{
			$(".giftlist-popup").find(".emostickers-box").find("ul.emostickers-list").append("<div class='no-listcontent'>No Stickers Found</div>");
		}
		
		var winw = $(window).width();
		if(winw < 1024){
			removeNiceScroll(".nice-scroll");
		}
	}
	function resetEmoCarousels(catName,emovalue,mode){		
		
		var $emoList="";
		
		if(mode=="editmode"){
			
			var $emoList="";
			$emoList = ['(wine)','(icecream)','(coffee)','(heart)','(flower)','(cake)','(handshake)','(gift)','(goodmorning)','(goodnight)','(backpack)','(parasailing)','(train)','(flipflop)','(airplane)','(sunbed)','(happyhalloween)','(merrychristmas)','(eidmubarak)','(happyyear)','(happymothers)','(happyfathers)','(happyaniversary)','(happybirthday)'];
			
			var mholder="<div class='carousel slide' id='emoCarousel'><div class='carousel-inner'>";
			var litext="";
			if($emoList.length!=0){
				
				$.each($emoList, function(i, v){
					
					var faces = $.emostickers.replace(v);
					if(emovalue!="none"){
						if(emovalue==v)
							litext +="<div class='item active' data-class='"+v+"' data-cat='"+catName+"'>"+faces+"</div>";
						else
							litext +="<div class='item' data-class='"+v+"' data-cat='"+catName+"'>"+faces+"</div>";
					} else {
						if(i==0)
							litext +="<div class='item active' data-class='"+v+"' data-cat='"+catName+"'>"+faces+"</div>";
						else
							litext +="<div class='item' data-class='"+v+"' data-cat='"+catName+"'>"+faces+"</div>";
					}
				});
			}
			mholder += litext + "</div><a class='left carousel-control' href='#emoCarousel' data-slide='prev'><i class='mdi mdi-chevron-left'></i></a><a class='right carousel-control' href='#emoCarousel' data-slide='next'><i class='mdi mdi-chevron-right'></i></a></div>";
			$("#usegift-popup").find(".emostickers-slider").find(".loading-holder").html(mholder);
		}
		else{
			
			var htmltext = $.emostickers.replace(emovalue);
			$("#usegift-popup").find(".emostickers-holder").find(".loading-holder").html(htmltext);
		}
	}
	function useGift(emovalue,catName,mode){
		
		$("#usegift-popup").find(".emostickers-slider").find(".loading-holder").html("");
		$("#usegift-popup").find(".emostickers-holder").find(".loading-holder").html("");
		
		if(mode=="editmode"){
			$("#usegift-popup").find(".preview-emosticker").hide();
			$("#usegift-popup").find(".edit-emosticker").show();
		}
		else{
			$("#usegift-popup").find(".edit-emosticker").hide();
			$("#usegift-popup").find(".preview-emosticker").show();
		}
		$('.usegift_modal').modal('open');
		//$('#usegift-popup').modal('show');
		
		setTimeout(function(){
				
				resetEmoCarousels(catName,emovalue,mode);
				if(mode=="editmode"){					
					$("#usegift-popup").find(".emostickers-slider").find(".loading-holder").find('.carousel#emoCarousel').carousel({ interval: false });					
					$("#usegift-popup").find(".emostickers-slider.data-loading").addClass("data-loaded");					
				}
				else{					
					$("#usegift-popup").find(".emostickers-holder.data-loading").addClass("data-loaded");
				}				
			
		},400);
	}

	function closeattachmentbox() {
		$custW = $(window).width();
		if($custW <=799) {
            $('.hidden-attachment-box').removeClass('shown').addClass('hidden');
        } else {
          $('.hidden-attachment-box').hide();
        }
		$('.fixed-action-btn.docustomize').removeClass('active');	
	}

	function setMsgTextArea() {
	  $w = $(window).width();
	  $left = $('.left-section');
	  $right = $('.right-section');
	  $textarea = $('.inputMessageWall');
	  $textareaBlock = $('.addnew-msg');
	  $messagespagefixed = $('.messages-page-fixed').width();
	  
	  if($w < 800) {
	  	$SPACE = 118;
	    $SDJSDI = $w;
	  } else {
	  	$SPACE = 130;
	  	$SDJSDI = $right.width();
	  }

	  $textareaBlock.css('width', $SDJSDI+'px');
	  $textareaSpace = $textareaBlock.width() - $SPACE;
	  $textarea.css('width', $textareaSpace+'px');
	}

	function generalCaseStudy() {
	  $w = $(window).width();
	  if($w > 799) {
			$('.mbl-innerhead').hide();
			//$('.hidemenu-wrapper').find('.header-section').find('.header-themebar').find('.mobile-menu').css('display', 'block');
			//$('.hidemenu-wrapper').find('.header-section').find('.header-themebar').find('.mobile-menu.topicon').css('display', 'none');
	  } else {
	  		//$('.hidemenu-wrapper').find('.header-section').find('.header-themebar').find('.mobile-menu.topicon').css('display', 'block');
	  }

	  if($w < 800) {
	    $('.page-name.mainpage-name').show();
	  } else {	
	    $('.page-name.mainpage-name').hide();
	  }

	  if($('.mblMessagePage').length) {
	    if($('.mblMessagePage').hasClass('innerpage')) {
	      if ($w <= 799) {
	        $('.messages-wrapper').find('.header-section').find('.header-themebar').find('.mbl-innerhead').find('.logo-holder').css('margin-top', '-38px');
	        $('.hidemenu-wrapper').find('.header-section').find('.header-themebar').find('.mobile-menu').css('display', 'none');
	      	$('.messagesearch-xs-btn').show();
	      } else {
	      	$('.messagesearch-xs-btn').hide();
	      }
	    } else {
	    	$('.messagesearch-xs-btn').hide();
	    } 
	  }
	}
	
/* FUN end manage emoticons */

/************ END MESSAGE FUNCTIONS ************/
