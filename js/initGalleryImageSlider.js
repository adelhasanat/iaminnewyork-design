$(document).ready(function() {
	justifiedGalleryinitialize();
    $('.collection-card-body').find('img').css('opacity', '1');
    $('.collectiondetail-page').find('#placebox').removeClass('dis-none');
    lightGalleryinitialize();
});

// js to prevent open gallery on custom links
var hideGallery = function(){
    $('.lg-backdrop').addClass('hidden');
    $('.lg-outer').addClass('hidden');
};
var showGallery = function(){
    $('.lg-backdrop').removeClass('hidden');
    $('.lg-outer').removeClass('hidden');
};
var preventGalleryShow = function(event, $galleryObj){ 
    event.preventDefault();
    event.stopPropagation();

    var $lgp = $('.lgt-gallery-photoGallery');
    if($lgp.length) {
        $lgp.lightGallery();
        $lgp.data('lightGallery').destroy();
    }

    var $lg = $('.lgt-gallery-photo');
    if($lg.length) {
        $lg.lightGallery();
        $lg.data('lightGallery').destroy();
    }

    hideGallery();
    $galleryObj.trigger('onAfterOpen.lg');
};
$('.prevent-gallery').on('click', function(event){
	$this = $(this);
    preventGalleryShow(event,$this);
});
$('.allow-gallery').on('click', function(event){
    showGallery();
});

function lightGalleryinitialize() {
	var $lg = $('.lgt-gallery-photo');
	$lg.lightGallery({
	    thumbnail:false,
	    closable: false,
	    mousewheel: false,
	    loop: true 
	});

	$lg.show();
	 
	// Perform any action just before opening the gallery
	$lg.on('onAfterOpen.lg',function(event) {
	  if($('.lg-outer').length) 
	  {
	  	$("body").css("overflow-y", "hidden");

	    $block = "<div id='extraHtmlBlock' class='extraHtmlBlock gallery-container'> </div>";

	    if($('#extraHtmlBlock').length) {
	    	$('#extraHtmlBlock').remove();
	    }
	    
	    $('.lg-outer').append($block);

	    $bottomIcons = '<a class="lg-like lg-icon" href="javascript: void(0)"><i class="mdi mdi-thumb-up-outline" onclick="toggleIcons(this)"></i></a><span class="lcount">7</span><a class="lg-comment lg-icon" href="javascript: void(0)"><i class="zmdi zmdi-mail-reply zmdi-hc-flip-horizontal mdi-23px"></i></a><a class="lg-comment lg-icon" href="javascript: void(0)" onclick="scrollToComment()"><i class="mdi mdi-comment-outline"></i></a><span class="lcount">7</span>';
		$('.photo-bottom-icons').html($bottomIcons);

	  }
	});

	$lg.on('onBeforeClose.lg',function(event) {
		$("body").css("overflow-y", "auto");
	});


	$lg.on('onAfterSlide.lg',function(event) {
	  setTimeout(function(){ 
	    var slide = getUrlVars()["slide"];
	    if(slide) {
	      if(slide == '' || slide == 0 || slide == '0') {
	        $block ='<div class="sub-photo-view row mx-0">' +
	        			'<div class="col col s12 m10 offset-m1">' +
	        				'<div class="col m6 leftbox">' +
	        					'<div class="left-sec">' +
		        					'<div class="people-box">' +
		        						'<div class="img-holder">' +
		        							'<img src="images/people-1.png">' +
										'</div>' +
										'<div class="desc-holder">' +
											'<a href="javascript:void(0)" class="userlink">Iamin'+$st_nm_S+' Photography</a>' +
											'<a href="javascript:void(0)" class="edit-icon waves-effect waves-theme edit-gallery"><i class="mdi mdi-pencil"></i></a>' +
											'<span class="info">By Adel Ahasanat</span>' +
										'</div>' +
									'</div>'+
									'<div class="photo-para">' +
										'<p>A small boy from the Entoto hills outside Addis Ababa</p>' +
									'</div>'+
									'<div class="joined-tb p-0 vilicoareanew">' +
										'<ul>' +
											'<li><p>Views</p><h4>19,92</h4></li>' +
											'<li><p>Likes</p><h4>60,34</h4></li>' +
											'<li class="notmarginright"><p>Comments</p><h4>2,303</h4></li>' +
											'<li class="longtimelinesent"><p class="date">Taken on November 28, 2018</p></li>' +
										'</ul>' +
									'</div>' +
									'<div class="photo-location">' +
										'<i class="zmdi zmdi-pin"></i>' +
										'<span class="city">Amman</span>, <span class="country">'+$st_nm_S+'</span>' +
									'</div>'+
									'<div class="photo-faved">' +
										'<a href="javascript:void(0)" title="fave"><i class="mdi mdi-thumb-up-outline fave-icon"></i></a>' +
										'<div class="faved-by">' +
											'<a href="javascript:void(0)">Ahmad Ali</a>, <a href="javascript:void(0)" title="fave">Ammad Ahmad</a> and <a href="javascript:void(0)" >38 more people</a> liked this' +
										'</div>'+
									'</div>'+
									'<div class="photo-tagged">' +
										'<i class="mdi mdi-account-multiple-outline"></i>' +
										'<div class="tagged-by">' +
											'<span class="">Muhammad Ammad</span> with <a href="javascript:void(0)">Saad Hasanat</a> and <a href="javascript:void(0)" >38 others</a>' +
										'</div>'+
									'</div>'+
									'<div class="additional-info">' +
										'<h5>Additional info</h5>' +	
										'<ul>' +
											'<li>' +
												'<i class="mdi mdi-lock-open-outline"></i>' +
												'<span class="privacy-label">Photo privacy</span> ' +
												'<span class="privacy-value right">Public</span>' +
											'</li>' +
											'<li>' +
												'<i class="mdi mdi-eye-off"></i>' +
												'<a href="javascript:void(0)" onclick="generateDiscard(\'dis_hidePhoto\')">Hide Photo</a>' +
											'</li>' +
											'<li>' +
												'<i class="mdi mdi-flag-outline"></i>' +
												'<a href="javascript:void(0)" onclick="privacymodal(this)">Report Photo</a>' +
											'</li>' +
										'</ul>'+
									'</div>'+
									'<div class="photo-tags">' +
										'<h5><span class="cat">Category</span> <span class="para">People</span></h5>' +
										'<ul class="tag-list">' +
											'<li>' +
												'<a href="javascript:void(0)">Nikon</a>' +
											'</li>' +
											'<li>' +
												'<a href="javascript:void(0)">landscape</a>' +
											'</li>' +
											'<li>' +
												'<a href="javascript:void(0)">nature</a>' +
											'</li>' +
											'<li>' +
												'<a href="javascript:void(0)">iceland</a>' +
											'</li>' +
											'<li>' +
												'<a href="javascript:void(0)">iseland</a>' +
											'</li>' +
											'<li>' +
												'<a href="javascript:void(0)">kosajdso</a>' +
											'</li>' +
											'<li>' +
												'<a href="javascript:void(0)">sunset</a>' +
											'</li>' +
											'<li>' +
												'<a href="javascript:void(0)">seljalandsfoss</a>' +
											'</li>' +
											'<li>' +
												'<a href="javascript:void(0)">www.daniel-photography.eu</a>' +
											'</li>' +
										'</ul>'+
									'</div>'+
								'</div>' +
							'</div>' +
							'<div class="col m6 rightbox">' +
								'<div class="joined-tb p-0 vilicoarea">' +
									'<ul>' +
										'<li><p>Views</p><h4>19,92</h4></li>' +
										'<li><p>Likes</p><h4>60,34</h4></li>' +
										'<li class="notmarginright"><p>Comments</p><h4>2,303</h4></li>' +
										'<li class="longtimelinesent"><p class="date">Taken on November 28, 2018</p></li>' +
									'</ul>' +
								'</div>' +
								'<div class="comment-sec">' + 
									'<div class="comment-count"><h4>10 Comments</h4></div>' +
									'<div class="addnew-comment valign-wrapper ">' +
										'<div class="img-holder">' +
											'<a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"></a>' +
										'</div>' +
										'<div class="desc-holder">' +
											'<div class="sliding-middle-out anim-area tt-holder">' +
												'<textarea class="materialize-textarea" id="comment_txt_3" placeholder="Add a comment..."></textarea>' +
											'</div>' +
										'</div>' +
									'</div>' +
									'<div class="comment-list">' +
										'<ul>' +
											'<li>' +
												'<div class="ranker-box">' +
			                						'<div class="img-holder">' +
			                							'<img src="images/people-3.png">' +
			        								'</div>' +
			        								'<div class="desc-holder">' +
			        									'<a href="javascript:void(0)" class="userlink">Muhammad Anam</a>' +
			        									'<span class="comment-date">May 26, 2018</span>' +
														'<span class="info">Beautiful, moody photo. Well done! Great gallery as well!.Beautiful, moody photo. Well done! Great gallery as well!</span>' +
													'</div>' +
												'</div>'+
											'</li>' +
											'<li>' +
												'<div class="ranker-box">' +
			                						'<div class="img-holder">' +
			                							'<img src="images/people-3.png">' +
			        								'</div>' +
			        								'<div class="desc-holder">' +
			        									'<a href="javascript:void(0)" class="userlink">Muhammad Anam</a>' +
			        									'<span class="comment-date">May 26, 2017</span>' +
														'<span class="info">Beautiful, moody photo. Well done! Great gallery as well!</span>' +
													'</div>' +
												'</div>'+
											'</li>' +
											'<li>' +
												'<div class="ranker-box">' +
			                						'<div class="img-holder">' +
			                							'<img src="images/people-3.png">' +
			        								'</div>' +
			        								'<div class="desc-holder">' +
			        									'<a href="javascript:void(0)" class="userlink">Muhammad Anam</a>' +
			        									'<span class="comment-date">May 26, 2015</span>' +
														'<span class="info">Beautiful, moody photo. Well done! Great gallery as well!</span>' +
													'</div>' +
												'</div>'+
											'</li>' +
										'</ul>' +
									'</div>' +
								'</div>' +
							'</div>' +
						'</div>' +
					'</div>';
	        $('.lg-outer').find('.extraHtmlBlock').html($block);
	      } else if(slide == '1' || slide == 1) {
	        $block ='<div class="sub-photo-view row mx-0">' +
	        			'<div class="col col s12 m10 offset-m1">' +
	        				'<div class="col m6 leftbox">' +
	        					'<div class="left-sec mode-holder">' +
		        					'<div class="people-box">' +
		        						'<div class="img-holder">' +
		        							'<img src="images/people-2.png">' +
										'</div>' +
										'<div class="desc-holder">' +
											'<a href="javascript:void(0)" class="userlink">Iamin'+$st_nm_S+' Photography</a>' +
											'<a href="javascript:void(0)" class="edit-icon waves-effect waves-theme"><i class="mdi mdi-pencil"></i></a>' +
											'<span class="info">By Adel Ahasanat</span>' +
										'</div>' +
									'</div>'+
									'<div class="photo-para">' +
										'<p>A small boy from the Entoto hills outside Addis Ababa</p>' +
									'</div>'+
									'<div class="joined-tb p-0 vilicoareanew">' +
										'<ul>' +
											'<li><p>Views</p><h4>19,92</h4></li>' +
											'<li><p>Likes</p><h4>60,34</h4></li>' +
											'<li class="notmarginright"><p>Comments</p><h4>2,303</h4></li>' +
											'<li class="longtimelinesent"><p class="date">Taken on November 28, 2018</p></li>' +
										'</ul>' +
									'</div>' +
									'<div class="photo-location">' +
										'<i class="zmdi zmdi-pin"></i>' +
										'<span class="city">Amman</span>, <span class="country">'+$st_nm_S+'</span>' +
									'</div>'+
									'<div class="photo-faved">' +
										'<a href="javascript:void(0)" title="fave"><i class="mdi mdi-thumb-up-outline fave-icon"></i></a>' +
										'<div class="faved-by">' +
											'<a href="javascript:void(0)">Ahmad ALi</a>, <a href="javascript:void(0)" title="fave">Ammad Ahmad</a> and <a href="javascript:void(0)" >38 more people</a> liked this' +
										'</div>'+
									'</div>'+
									'<div class="photo-tagged">' +
										'<i class="mdi mdi-account-multiple-outline"></i>' +
										'<div class="tagged-by">' +
											'<span class="">Muhammad Ammad</span> with <a href="javascript:void(0)">Saad Hasanat</a> and <a href="javascript:void(0)" >38 others</a>' +
										'</div>'+
									'</div>'+
									'<div class="additional-info">' +
										'<h5>Additional info</h5>' +
										'<ul>' +
											'<li>' +
												'<i class="mdi mdi-lock-open-outline"></i>' +
												'<span class="privacy-label">Photo privacy</span> ' +
												'<span class="privacy-value right">Public</span>' +
											'</li>' +
											'<li>' +
												'<i class="mdi mdi-eye-off"></i>' +
												'<a href="javascript:void(0)" onclick="generateDiscard(\'dis_hidePhoto\')">Hide Photo</a>' +
											'</li>' +
											'<li>' +
												'<i class="mdi mdi-flag-outline"></i>' +
												'<a href="javascript:void(0)" onclick="privacymodal(this)">Report Photo</a>' +
											'</li>' +
										'</ul>'+
									'</div>'+
									'<div class="photo-tags">' +
										'<h5><span class="cat">Category</span> <span class="para">People</span></h5>' +
										'<ul class="tag-list">' +
											'<li>' +
												'<a href="javascript:void(0)">Nikon</a>' +
											'</li>' +
											'<li>' +
												'<a href="javascript:void(0)">landscape</a>' +
											'</li>' +
											'<li>' +
												'<a href="javascript:void(0)">nature</a>' +
											'</li>' +
											'<li>' +
												'<a href="javascript:void(0)">iceland</a>' +
											'</li>' +
											'<li>' +
												'<a href="javascript:void(0)">iseland</a>' +
											'</li>' +
											'<li>' +
												'<a href="javascript:void(0)">kosajdso</a>' +
											'</li>' +
											'<li>' +
												'<a href="javascript:void(0)">sunset</a>' +
											'</li>' +
											'<li>' +
												'<a href="javascript:void(0)">seljalandsfoss</a>' +
											'</li>' +
											'<li>' +
												'<a href="javascript:void(0)">www.daniel-photography.eu</a>' +
											'</li>' +
										'</ul>'+
									'</div>'+
								'</div>' +
							'</div>' +
							'<div class="col m6 rightbox">' +
								'<div class="joined-tb p-0 vilicoarea">' +
									'<ul>' +
										'<li><p>Views</p><h4>19,92</h4></li>' +
										'<li><p>Likes</p><h4>60,34</h4></li>' +
										'<li class="notmarginright"><p>Comments</p><h4>2,303</h4></li>' +
										'<li class="longtimelinesent"><p class="date">Taken on November 28, 2018</p></li>' +
									'</ul>' +
								'</div>' +
								'<div class="comment-sec">' + 
									'<div class="comment-count"><h4>10 Comments</h4></div>' +
									'<div class="addnew-comment valign-wrapper ">' +
										'<div class="img-holder">' +
											'<a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"></a>' +
										'</div>' +
										'<div class="desc-holder">' +
											'<div class="sliding-middle-out anim-area tt-holder">' +
												'<textarea class="materialize-textarea" id="comment_txt_3" placeholder="Add a comment..."></textarea>' +
											'</div>' +
										'</div>' +
									'</div>' +
									'<div class="comment-list">' +
										'<ul>' +
											'<li>' +
												'<div class="ranker-box">' +
			                						'<div class="img-holder">' +
			                							'<img src="images/people-3.png">' +
			        								'</div>' +
			        								'<div class="desc-holder">' +
			        									'<a href="javascript:void(0)" class="userlink">Muhammad Anam</a>' +
			        									'<span class="comment-date">May 26, 2018</span>' +
														'<span class="info">Beautiful, moody photo. Well done! Great gallery as well!.Beautiful, moody photo. Well done! Great gallery as well!</span>' +
													'</div>' +
												'</div>'+
											'</li>' +
											'<li>' +
												'<div class="ranker-box">' +
			                						'<div class="img-holder">' +
			                							'<img src="images/people-3.png">' +
			        								'</div>' +
			        								'<div class="desc-holder">' +
			        									'<a href="javascript:void(0)" class="userlink">Muhammad Anam</a>' +
			        									'<span class="comment-date">May 26, 2017</span>' +
														'<span class="info">Beautiful, moody photo. Well done! Great gallery as well!</span>' +
													'</div>' +
												'</div>'+
											'</li>' +
											'<li>' +
												'<div class="ranker-box">' +
			                						'<div class="img-holder">' +
			                							'<img src="images/people-3.png">' +
			        								'</div>' +
			        								'<div class="desc-holder">' +
			        									'<a href="javascript:void(0)" class="userlink">Muhammad Anam</a>' +
			        									'<span class="comment-date">May 26, 2015</span>' +
														'<span class="info">Beautiful, moody photo. Well done! Great gallery as well!</span>' +
													'</div>' +
												'</div>'+
											'</li>' +
										'</ul>' +
									'</div>' +
								'</div>' +
							'</div>' +
						'</div>' +
					'</div>';
	        $('.lg-outer').find('.extraHtmlBlock').html($block);
	      } else if(slide == '2' || slide == 2) {
	        $block = '<div class="sub-photo-view row mx-0">' +
	        			'<div class="col col s12 m10 offset-m1">' +
	        				'<div class="col m6 leftbox">' +
	        					'<div class="left-sec">' +
		        					'<div class="people-box">' +
		        						'<div class="img-holder">' +
		        							'<img src="images/people-3.png">' +
										'</div>' +
										'<div class="desc-holder">' +
											'<a href="javascript:void(0)" class="userlink">Iamin'+$st_nm_L+' Photography</a>' +
											'<a href="javascript:void(0)" class="edit-icon waves-effect waves-theme"><i class="mdi mdi-pencil"></i></a>' +
											'<span class="info">By Adel Ahasanat</span>' +
										'</div>' +
									'</div>'+
									'<div class="photo-para">' +
										'<p>A small boy from the Entoto hills outside Addis Ababa</p>' +
									'</div>'+
									'<div class="joined-tb p-0 vilicoareanew">' +
										'<ul>' +
											'<li><p>Views</p><h4>19,92</h4></li>' +
											'<li><p>Likes</p><h4>60,34</h4></li>' +
											'<li class="notmarginright"><p>Comments</p><h4>2,303</h4></li>' +
											'<li class="longtimelinesent"><p class="date">Taken on November 28, 2018</p></li>' +
										'</ul>' +
									'</div>' +
									'<div class="photo-location">' +
										'<i class="zmdi zmdi-pin"></i>' +
										'<span class="city">Amman</span>, <span class="country">'+$st_nm_S+'</span>' +
									'</div>'+
									'<div class="photo-faved">' +
										'<a href="javascript:void(0)" title="fave"><i class="mdi mdi-thumb-up-outline fave-icon"></i></a>' +
										'<div class="faved-by">' +
											'<a href="javascript:void(0)">Ahmad ALi</a>, <a href="javascript:void(0)" title="fave">Ammad Ahmad</a> and <a href="javascript:void(0)" >38 more people</a> liked this' +
										'</div>'+
									'</div>'+
									'<div class="photo-tagged">' +
										'<i class="mdi mdi-account-multiple-outline"></i>' +
										'<div class="tagged-by">' +
											'<span class="">Muhammad Ammad</span> with <a href="javascript:void(0)">Saad Hasanat</a> and <a href="javascript:void(0)" >38 others</a>' +
										'</div>'+
									'</div>'+
									'<div class="additional-info">' +
										'<h5>Additional info</h5>' +
										'<ul>' +
											'<li>' +
												'<i class="mdi mdi-lock-open-outline"></i>' +
												'<span class="privacy-label">Photo privacy</span> ' +
												'<span class="privacy-value right">Public</span>' +
											'</li>' +
											'<li>' +
												'<i class="mdi mdi-eye-off"></i>' +
												'<a href="javascript:void(0)" onclick="generateDiscard(\'dis_hidePhoto\')">Hide Photo</a>' +
											'</li>' +
											'<li>' +
												'<i class="mdi mdi-flag-outline"></i>' +
												'<a href="javascript:void(0)" onclick="privacymodal(this)">Report Photo</a>' +
											'</li>' +
										'</ul>'+
									'</div>'+
									'<div class="photo-tags">' +
										'<h5><span class="cat">Category</span> <span class="para">People</span></h5>' +
										'<ul class="tag-list">' +
											'<li>' +
												'<a href="javascript:void(0)">Nikon</a>' +
											'</li>' +
											'<li>' +
												'<a href="javascript:void(0)">landscape</a>' +
											'</li>' +
											'<li>' +
												'<a href="javascript:void(0)">nature</a>' +
											'</li>' +
											'<li>' +
												'<a href="javascript:void(0)">iceland</a>' +
											'</li>' +
											'<li>' +
												'<a href="javascript:void(0)">iseland</a>' +
											'</li>' +
											'<li>' +
												'<a href="javascript:void(0)">kosajdso</a>' +
											'</li>' +
											'<li>' +
												'<a href="javascript:void(0)">sunset</a>' +
											'</li>' +
											'<li>' +
												'<a href="javascript:void(0)">seljalandsfoss</a>' +
											'</li>' +
											'<li>' +
												'<a href="javascript:void(0)">www.daniel-photography.eu</a>' +
											'</li>' +
										'</ul>'+
									'</div>'+
								'</div>' +
							'</div>' +
							'<div class="col m6 rightbox">' +
								'<div class="joined-tb p-0 vilicoarea">' +
									'<ul>' +
										'<li><p>Views</p><h4>19,92</h4></li>' +
										'<li><p>Likes</p><h4>60,34</h4></li>' +
										'<li class="notmarginright"><p>Comments</p><h4>2,303</h4></li>' +
										'<li class="longtimelinesent"><p class="date">Taken on November 28, 2018</p></li>' +
									'</ul>' +
								'</div>' +
								'<div class="comment-sec">' + 
									'<div class="comment-count"><h4>10 Comments</h4></div>' +
									'<div class="addnew-comment valign-wrapper ">' +
										'<div class="img-holder">' +
											'<a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"></a>' +
										'</div>' +
										'<div class="desc-holder">' +
											'<div class="sliding-middle-out anim-area tt-holder">' +
												'<textarea class="materialize-textarea" id="comment_txt_3" placeholder="Add a comment..."></textarea>' +
											'</div>' +
										'</div>' +
									'</div>' +
									'<div class="comment-list">' +
										'<ul>' +
											'<li>' +
												'<div class="ranker-box">' +
			                						'<div class="img-holder">' +
			                							'<img src="images/people-3.png">' +
			        								'</div>' +
			        								'<div class="desc-holder">' +
			        									'<a href="javascript:void(0)" class="userlink">Muhammad Anam</a>' +
			        									'<span class="comment-date">May 26, 2018</span>' +
														'<span class="info">Beautiful, moody photo. Well done! Great gallery as well!.Beautiful, moody photo. Well done! Great gallery as well!</span>' +
													'</div>' +
												'</div>'+
											'</li>' +
											'<li>' +
												'<div class="ranker-box">' +
			                						'<div class="img-holder">' +
			                							'<img src="images/people-3.png">' +
			        								'</div>' +
			        								'<div class="desc-holder">' +
			        									'<a href="javascript:void(0)" class="userlink">Muhammad Anam</a>' +
			        									'<span class="comment-date">May 26, 2017</span>' +
														'<span class="info">Beautiful, moody photo. Well done! Great gallery as well!</span>' +
													'</div>' +
												'</div>'+
											'</li>' +
											'<li>' +
												'<div class="ranker-box">' +
			                						'<div class="img-holder">' +
			                							'<img src="images/people-3.png">' +
			        								'</div>' +
			        								'<div class="desc-holder">' +
			        									'<a href="javascript:void(0)" class="userlink">Muhammad Anam</a>' +
			        									'<span class="comment-date">May 26, 2015</span>' +
														'<span class="info">Beautiful, moody photo. Well done! Great gallery as well!</span>' +
													'</div>' +
												'</div>'+
											'</li>' +
										'</ul>' +
									'</div>' +
								'</div>' +
							'</div>' +
						'</div>' +
					'</div>';
	        $('.lg-outer').find('.extraHtmlBlock').html($block);
	      }
	    }
	    
	  }, 400);

	});

}

function justifiedGalleryinitialize() {
	// js for justified gallery + lightgallery	
	$('.lgt-gallery-justified').justifiedGallery({
	    lastRow: 'nojustify',
	    rowHeight: 220,
	    maxRowHeight: 220,
	    margins: 10,
	    sizeRangeSuffixes: {
	        lt100: '_t',
	        lt240: '_m',
	        lt320: '_n',
	        lt500: '',
	        lt640: '_z',
	        lt1024: '_b'
	    }
	});
}

function initGalleryImageSlider() {
	// execute above function
	$( ".images-container" ).each(function( index ) {
		$(this).removeClass (function (index, css) {
			return (css.match (/(^|\s)images-container\S+/g) || []).join(' ');
		});
		$(this).addClass('images-container images-container'+index);
	});
}

function getUrlVars() {
  var vars = {};
  var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
    vars[key] = value;
  });
  return vars;
} 

function toggleIcons(obj){
	$(obj).toggleClass('mdi-thumb-up-outline mdi-thumb-up');
}

function scrollToComment(){
	$('#slidercommenttextarea').focus();
}
