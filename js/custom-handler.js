var current = 0;	
var interval;
var photocount = 0;

$(document).on('click', '.notificationfrdrqt', function() {
	$('#frdrqtdebugger').click();
});

$(document).on('click', '.places-photos', function() {
	setTimeout(function() {
		justifiedGalleryinitialize();
	}, 600);
});
			
$(document).on('click', '.footer_booking', function() {
	$('.footer_alert').removeClass('active');
	$('.master_alert').removeClass('active');
});

/* document ready */
	$(document).ready(function() {
			var winw=$(window).width();
			$headernav = $('.header-nav').width();
			$__s = parseInt(winw) - parseInt($headernav);
			$__s = parseInt($__s) / 2;
			$('.sticky-nav').find('.col').css('width', $headernav+'px');
			$('.sticky-nav').find('.col').css('margin-left', $__s+'px');
		/************ CHAT FUNCTIONS ************/
			
			/* manage chat box */
			manageChatbox();
			/* end manage chat box */
			
			/* set chat window height */
			setChatHeight();
			/* end set chat window height */
			
			/* set floating chat */
			setFloatChat();		
			/* end set floating chat */
			
			/* set floating chat */
			$(".chat-button,.close-chat").click(function(){
				
				$(".float-chat").toggleClass("chat-open");
				
				var win_w=$(window).width();
				if(win_w<=1024){
					removeNiceScroll(".nice-scroll");
				}
				if(!$(".float-chat").hasClass("chat-open")){
					$(".chat-window").removeClass("shown");
				}
			});
			/* end set floating chat */
		
		/************ END CHAT FUNCTIONS ************/
		
		/************ ESSENTIALS FUNCTIONS ************/
		
			/* nice scroll */		
			var win_w=$(window).width();
			var fromTop1=$(window).scrollTop();
			if($(".page-wrapper").hasClass("transheadereffect")) {
				if(fromTop1 > 0){
					if($(".page-wrapper").hasClass("transheadereffectall")) {
						$(".page-wrapper").addClass('page-scrolled');
						$(".page-wrapper").removeClass('JIS3829');
					} else if(win_w<=742) {
						$(".page-wrapper").addClass('page-scrolled');
					}
				} else {
					if($(".page-wrapper").hasClass("transheadereffectall")) {
						$(".page-wrapper").removeClass('page-scrolled');
						$(".page-wrapper").addClass('JIS3829');
					} else if(win_w<=742){
						$(".page-wrapper").removeClass('page-scrolled');
					}
				}
			}

			if(win_w>2000){
				
				var isHome=$(".home-page.bodydup").length;				
				if(isHome){
					initNiceScroll("homebody");				
				}
				
				initNiceScroll(".nice-scroll");
					
			}else{
				$(".sidemenu").css("opacity",1);				
				removeNiceScroll(".nice-scroll");
			}
			/* end nice scroll */		
			
			/* select-2 */
			/*$(".select2").select2({minimumResultsForSearch:-1});			*/
			/* end select-2 */
								
			var win_w=$(window).width();		
			if(win_w>767){
				if($('body').hasClass("hideHeader")){
					$('body').removeClass("hideHeader");
				}
			}
			
			/* set new post bubble */
			var top=$(window).scrollTop();
			var winw=$(window).width();
			if(!$(".home-page.bodydup").length) {
				if(winw > 767){
					top=$(window).scrollTop();			
				}else{
					top = $('.page-wrapper').offset().top;
				}
				setNewPostBubble(winw,top,'docready');
			}
			/* end set new post bubble */
				
			/* manage left menu */
			setLeftMenu();
			/* end manage left menu */
			
			/* fix centering image */
			setTimeout(function(){fixImageUI();},400);
			/* end fix centering image */
			
			/* mobile screen : manage innerpage and header with page name */
			$(document).on('click', '.xmdjkv li.tab', function() {
				$width = $(window).width();
				if($width < 767) {  
					$labelNameArray = ['General', 'Notifications', 'Page Admins', 'Block List', 'Activity Log', 'Review Posts', 'Review Photos'];
					$labelName = $(this).find('a').text().trim();
					
					if($.inArray($labelName, $labelNameArray) !== -1) {
						$specId = $(this).find('a').attr('href');
						if($width <767) {
							$('.innerpage-name').show().html($labelName);
							$('.mainpage-name').hide();
						} else {
							$('.innerpage-name').hide();
							$('.mainpage-name').show();
						}
						$('.mobile-menu.topicon').find('.mbl-menuicon1').hide();
						$('.profile-top').hide();
						$('.search-holder.main-sholder').hide();

						$('.mobile-menu.topicon').find('.gotohome').show();
						$('.mobile-menu.topicon').find('.gotohome').find('i').removeClass().addClass('mdi mdi-arrow-left');
						$('.mobile-menu.topicon').find('.gotohome').find('a').attr("onclick", "openPageSettings()");
						$('.mobile-menu.topicon').find('.gotohome').find('a').addClass('gobacksetting');						
						$('.mobile-menu.topicon').find('.gotohome').find('a').attr("href", "javascript:void(0)")
						
						$('.tabs-detail').removeClass('gone').show();
						$('.tabs-detail').find('.tab-content').find('.tab-pane.fade').removeClass('active in');
						
						$($specId).addClass('active in');

						$('.gobacksetting').on('click', function() {
							if($('.action-icon.activity-link').length) {
								$width = $(window).width();
								if($width <767) {
									$('.innerpage-name').show();
									$('.mainpage-name').hide();
								} else {
									$('.innerpage-name').hide();
									$('.mainpage-name').show();
								}
								$('#settings-content').find('.tabs-list').show();
								$('#settings-content').find('.tabs-detail').hide();

								$('.mobile-menu.topicon').find('.gotohome').find('a').removeClass('gobacksetting');						
								$('.mobile-menu.topicon').find('.gotohome').find('a').attr("onclick", "location.reload();");
							}
						});
					}
				}
			});

			/* START Places tab clicking changed */
			$(document).on('click', '.place-wrapper .tab', function() {
				setplacetab();
			});

			/* STOP Places tab clicking changed */

			/*$(document).on('click', '.open-innerpage .tabs li a',function(){
				var win_w = $(window).width();
				if(win_w < 768) {
					var getusername=$(this).attr("pagename");
					
					$(".header-themebar .mbl-innerhead").show();
					$(".header-themebar .mbl-innerhead").find('.page-name').html(getusername);
					
					$(this).parents(".open-innerpage").find(".tabs-detail").removeClass("gone");
				}
			});*/
			/* end mobile screen : manage innerpage and header with page name */
				
			/* side icons visibility */
			$(".scrollup-float").hide();
			/* end side icons visibility */
			
			/* side icons visibility */
			$(".scrollup-float").click(function(){ 
				$('html,body').animate({ scrollTop: 0 }, 'slow');
				return false; 
			});
			/* end side icons visibility */
					
			/* percentage page loader */	
			runLoad();
			/* end percentage page loader */
						
			$('.slide_out_btn').click(function() {
				$('.left_side_modal').removeClass('hidesidemodal');
				$('.left_side_modal').addClass('showsidemodal');
			});
			 
			/* search result */	
			$(".search-btn").click(function (e) {	
				// if screen is mobile then open leftsearch bar 
				var win_w = $(window).width();
				if(win_w<=767) {
					$('.person_search_slide_xs').removeClass('showsidemodal');
					$('.person_search_slide_xs').addClass('hidesidemodal');
				} else {
					manageSearch(this);		
				}
			});
			
			$(".search-input").keyup(function() {
			  var textValue = $(this).val();
			  var isSearchResult=$(".search-result").css("display");
				  
			  if(isSearchResult=="none"){
				$(".search-result").slideDown();
			  }
		  
			});
			/* end search result */

			/* connections search result */	
			$(".search-input-connections").keyup(function() {
			  var textValue = $(this).val();
			  var isSearchResult=$(".search-connections-result").css("display");
				  
			  if(isSearchResult=="none"){
				$(".search-connections-result").slideDown();
			  }
		  
			});
			/* end connections search result*/			


			/* side float icons */
			$(".btnbox").click(function(){
				var cls=$(this).attr("class");
				cls=cls.replace("box-open","").trim();		
				closeAllSideDrawer(cls);
				$(this).toggleClass("box-open");
				
			});
			/* end side float icons */
			
		/************ END ESSENTIALS FUNCTIONS ************/
		
		/************ GENERAL PAGE FUNCTIONS ************/
		
			$("body").on("click", ".addnewchat", function(e){
				resetGeneralPageTabs(this);	
			});
			
			/* settings up newpost bubble - general page */
			$("body").on("click", ".general-page .right-tabs.pagetabs .nav-tabs li a", function(e){resetGeneralPageTabs(this);	});
			$("body").on("click", ".general-page .right-tabs.pagetabs .tabs li a", function(e){resetGeneralPageTabs(this);	});
			/* end settings up newpost bubble - general page */
					
			/* show general-details page summery expandable */
			reset_gdetails_expandable();
			/* end general-details page summery expandable */
			
			/* groups/events detail page : reset menu scrollbar */
			if($(".main-page").hasClass("generaldetails-page") && win_w<768){
				if($(".main-page").hasClass("groups-page") || $(".main-page").hasClass("commevents-page")){
					$(".hori-menus").niceScroll({horizrailenabled:true,cursorcolor:"#bbb",cursorwidth:"6px",cursorborderradius:"0",cursorborder:"0px solid #fff",background:"rgba(255,255,255,0.6)"});
				}
			}	
			
		/************ END GENERAL PAGE FUNCTIONS ************/
		
		/************ ADVERTISE FUNCTIONS ************/
			// ad manager : estimated audience slider
			if($(".range-slider.fixmin-slider").length>0)
				initRangeSlider("fixmin-slider");
			
		/************ END ADVERTISE FUNCTIONS ************/
		
		/************ COMMON FUNCTIONS ************/
			
			/* search input text entered */
				$("body").on('keyup', '.search-input',searchStringEntered);
			/* end search input text entered */

			/* search connections input text entered */
				//$("body").on('keyup', '.search-input-connections', searchStringEnteredConnections);
			/* end search connections input text entered */
						
			/* custom tooltip */
			$('.custom-tooltip').tooltipster({
				contentAsHTML: true,
				side: ['top', 'bottom'],theme: ['tooltipster-borderless'],
				contentCloning: true,
				interactive:true
			});
			/* end custom tooltip */
			
			/* view likes and comments of an album photo */
			$("body").on("click", ".view-likes", function(){
				var tid = $(this).attr('data-id');
				var tsec = $(this).attr('data-section');
			
				if(tid && tsec)
					openLikePopup(tid,tsec);
			});			
			$("body").on("click", ".view-comments", function(){
				var tid = $(this).attr('data-id');
				var tsec = $(this).attr('data-section');
			
				if(tid && tsec)
					openCommentPopup(tid,tsec);
			});		
			/* end view likes and comments of an album photo */
							
			/* set security option dropdown */		
			$("body").on("click", ".custom-drop .dropdown-menu li a", function(){setDropVal(this);});
			$("body").on("click", ".setDropVal .dropdown-menu li a", function(){setDropVal(this);});
			/* end set security option dropdown */
			
			/* invite section scrollbar */
			$(".suggest-connections .cbox-desc").niceScroll({horizrailenabled:true,cursorcolor:"#bbb",cursorwidth:"8px",cursorborderradius:"0",cursorborder:"0px solid #fff",background:"rgba(255,255,255,0.6)",autohidemode:"false"});
			/* end invite section scrollbar */
			
			/* custom placeholder */
				// Invoke the plugin
				//$('input, textarea').placeholder();
				// That’s it, really.			
			/* end custom placeholder */
			
			/* search closable */
			$(document).on('click', '.closable-search a', function (e) {	
				$(this).parents('.closable-search').toggleClass('open');
			});
			/* end search closable */
			
			if($('#price-slider').length) {
			   var slider = document.getElementById('price-slider');
			   noUiSlider.create(slider, {start: [0, 5000],
			   connect: true,  
			   step: 1,
			   orientation: 'horizontal', // 'horizontal' or 'vertical'
				range: {'min': 0,'max': 5000 },
				format: wNumb({ decimals: 0})
			   });
			} 

			if($('#takeoff-slider').length) {
			   var slider = document.getElementById('takeoff-slider');
			   noUiSlider.create(slider, {start: [0, 12],
			   connect: true,  
			   step: 1,
			   orientation: 'horizontal', // 'horizontal' or 'vertical'
				range: {'min': 0,'max': 12 },
				format: wNumb({ decimals: 0})
			   });
			}   

			if($('#distance-slider').length) {
				var slider = document.getElementById('distance-slider');
				noUiSlider.create(slider, { start: [0, 100],
				connect: true,
				step: 1,
				orientation: 'horizontal', // 'horizontal' or 'vertical'
				range: {'min': 0, 'max': 100},
				format: wNumb({decimals: 0})
				});
			}
			
			/* range slider initalization */			
			if($(".range-slider.range-slider1").length>0)
				initRangeSlider("range-slider1");
			if($(".range-slider.range-slider2").length>0)
				initRangeSlider("range-slider2");
			if($(".range-slider.distance-slider").length>0)
				initDistSlider("distance-slider",1,20,5);
			if($(".range-slider.price-slider").length>0)
				initPriceSlider("price-slider",500,10000,2500);	

			/* end range slider initalization */
									
			/* manage expandable -photo slider : hotels/places : more info */			
				$(".subtab-menu > li").click(function(e){ initSubtabSlider(this); });
			/* end manage expandable -photo slider : hotels/places : more info */
			
			/* accordian initialization */		
			/*
			$( ".trip-accordion" ).accordion({
			  collapsible: true
			});
			$( ".preview-accordion" ).accordion({
			  collapsible: true,
			  active: 2
			});
			$( "#ih-accrodion").accordion({
			  collapsible: true,
			  active: 1
			});		
			*/
			/* end accordian initialization */
			
			// reset split page : mbl-fiter-icon
			resetMblFilterIcon();
			
			/*credit plan selection*/
			$('input[name="credit-add"]').click(function () {
			   $('input:not(:checked)').parent('li').removeClass("active");
			   $('input:checked').parent('li').addClass("active");
			});
			/*End credit plan selection*/
			
			/* init image gallery slider for hotels page */
			if(!$(".main-page").hasClass("hotels-page") && !$(".main-page").hasClass("places-page")){			
				initGalleryImageSlider();
			}
			/* end init image gallery slider for hotels page */

			/* autogrow textarea */
			//autosize(document.getElementsByClassName("autogrow-tt"));	
			/* end autogrow textarea */
			
			/* add photos to new post */	
			$(document.body).on("change", ".custom-upload", function () {
				var cls = $(this).data("class");
				if(cls==undefined || cls==null) return false;
				
				if($(this).parents(".new-post").length>0)
					managePostButton(postbtn_hide);
				changePhotoFileInput(cls, this, true);        
			});
			$(document.body).on("change", ".hidden_uploader", function () {
				var cls = $(this).data("class");
				var getId=$(this).parents(".divrel").find(".add-photo").attr("id");
				//$('#'+getId).trigger('click');
				$("#add-photo-popup").modal("open");
				changePhotoFileInput(cls, this, true);        
			});		
			/* end add photos to new post */
			
			/* dropdown resist click */
			$("body").on("click", ".dropdown.resist a.dropdown-toggle", function(e){			
				$(this).parent().toggleClass('open');
			});
			/* end dropdown resist click */
			
			/* text input animation */
				$("body").on("click", ".sliding-middle-out", function(){titleUnderline(this);});					
			/* end text input animation */
			
			if($('.timeText').length>0) {
				$('.timeText').timepicker({ 'step': 5 });
			}
			
			/* like icon : list popup */
				$('body').on('mouseenter', '.liveliketooltip', function(){
					var likecon = $(this).attr('data-title');
					if(likecon) {
						
						$(this).tooltipster({ 
								contentAsHTML: true,
								theme: 'tooltipster-borderless',
								contentCloning: true
							   })
							.tooltipster('content', likecon)
							.tooltipster('open');
						
					} else {
						$(this).tooltipster('destroy'); 
					}
				});
			/* end like icon : list popup */
			
			/* FUN profile tooltip creator */	
				$('body').on('mouseenter', '.profiletipholder:not(.tooltipstered)', function(){
					var id = $(this).find('.profiletooltip_content').find('.profile-tip');
					if(id) {
						$(id).css('display', 'block');
						$(this).tooltipster({
								content: $(id).detach(),
								multiple: false,
								side: ['top', 'bottom'],
								theme: 'tooltipster-profiletip',
								contentCloning: true,
								interactive: true,
								trigger: 'click'
							})
							.tooltipster('open');
					}
				});
			/* FUN end profile tooltip creator */
					
		/************ END COMMON FUNCTIONS ************/
		
		/************ POST FUNCTIONS ************/

			/* new post btn disabled on off if content is exist or not */
		    $(document).on('keyup', '#compose_tool_box textarea', function() {
		    	var $textarea = $('#compose_tool_box textarea');
		    	var $submitBtn = $('#compose_tool_box .submit');
		        var $textareaValue = $textarea.val().trim();
		        if($textareaValue != '') {
		            $submitBtn.removeClass('disabled');
		        } else {
		        	$submitBtn.addClass('disabled');
		        }
		    });						

			/* readmore click */
			$("body").on("click", ".readmore", openReadMore);
			/* end readmore click */
			
			/* remove video link */
			$("body").on("click", ".remove-vlink", removeVideoLink);
			/* end remove video link */
			
			/* remove added photo */
			$("body").on("click", ".removePhotoFile", function(e){
				
				if($(this).attr("data-class")=="ep-delpic"){
				}
				else{
					removePhotoFile(this);
				}
			});
			/* end remove added photo */
			
			/* edit post popup pre-setup */
			$("body").on("click", ".editpost-link", preEditPostSetup);
			/* end edit post popup pre-setup */
			
			/* share post popup iamge fix */
			//$("body").on("click", ".pa-share", fixImageSharePopup);
			/* end share post popup iamge fix */
			
			/* delete post popup pre-setup */
			//$("body").on("click", ".deletepost-link", deletePost);
			/* end delete post popup pre-setup */
			
			/* save post popup pre-setup */
			$("body").on("click", ".savepost-link", savePost);
			/* end save post popup pre-setup */
		
			/* show fullpost click */
			$("body").on("click", ".show-fullpost", showFullPost);		
			/* end show fullpost click */

			/* open reply comment textarea */
			$("body").on("click", ".reply-comment", openReplyComment);
			/* end open reply comment textarea */
			
			/* open edit comment textarea */
			$("body").on("click", ".edit-comment", openEditComment);
			/* end open edit comment textarea */
			
			/* open delete comment */
			$("body").on("click", ".delete-comment", deleteEditComment);
			/* end delete edit comment */
			
			/* enter on edit comment textarea */
			$("body").on("keyup", "textarea.editcomment-tt",function(){			
				var ecid=$(this).attr("id");
				saveEditComment(this,ecid);
			} );
			/* end enter on edit comment textarea */
			
			/* enter on edit comment textarea */
			$("body").on("click", ".editcomment-cancel", cancelEditComment);
			/* enter on edit comment textarea */
			
			/* like clicked */
			$("body").on("click", ".pa-like", likeClicked);
			/* end like clicked */		
			
			/* share to connections */
				$("body").on("click", ".share-to-connections", openShareToConnections);		
			/* end share to connections */
			
			/* share to group wall */
				$("body").on("click", ".share-to-groupwall", openShareToGroupwall);		
			/* end share to group wall */
			
			/* share as message */
				$("body").on("click", ".share-as-message", openShareAsMessage);		
			/* end share as message */
			
			/* share privacy clicked */
				$("body").on("click", ".share-privacy", sharePrivacyClicked);				
			/* end share privacy clicked */
			
			/* comment icon clicked */
			//$("body").on("click", ".pa-comment", commentFocus);
			/* end comment icon clicked */

			/* load more comments */
				$(".view-morec").click(function(){
					$(this).parents(".post-holder").find(".pcomment-earlier").slideDown();
					$(this).fadeOut();
				});
			/* end load more comments */
			
			/* new post options */
			$("body").on("click", "ul.echeck-list > li", function(e){
				var isSelected=$(this).hasClass("selected");
				if(isSelected)
					$(this).removeClass("selected");
				else
					$(this).addClass("selected");
			});
			/* end new post options */

			/* new post clicked */
			$(".new-post").click(function(){
				
				var win_w = $(window).width();
				if(win_w >= 768){
					//expandNewpost(this);
				}
				else{	
					if($(this).parents().hasClass('post-pages')){
						return true;
					}
				}
			});
			/* end new post clicked */

			/* open post location */
			$(".add-location").click(function(){
				$(this).parents(".npost-content").find(".post-location").toggle("slow");
			});
			/* end open post location */

			/* open post photos */
			$(".add-photos").click(function(){	
				$(this).parents(".npost-content").find(".post-photos").slideDown();
			});
			/* end open post photos */

			/* open post tag */
			$(".add-tag").click(function(){	
				$(this).parents(".npost-content").find(".post-tag").toggle("slow");
			});
			/* end open post tag */

			/* open post title */
			$(".add-title").click(function(e){
				openTitle(this);
			});
			/* end open post title */
			
			/* open comments section */			
			/*
			$(document).on('click', '.post-actions .pa-comment', function (e) {		
				$(this).parents(".post-holder").find(".comments-section").slideToggle("slow");				
			});
			*/
			/* end open comments section */
			
		/************ END POST FUNCTIONS ************/
		
		/************ SETTINGS FUNCTIONS ************/
			
			/* Settings Side Menu */
			var isSettings=$(".settings-page").length;
			
			if(isSettings>0){
				$("body").on("click", "ul.submenu li a", function(){setPageMainTab("settings",this);});
				$("body").on("click", "ul#settings-menu li.has_sub a.mainlink", function(){setSettingsMenu(this);});
			}
			/* End Settings Side Menu */
			
			/*Show password on click*/
			$('.showPass').mousedown(function(){
			
				var getParent = $(this).parent();
				
				var obj = getParent.find("input").first();						
				obj.attr("type","text");

				
				}).mouseup(function() {
					
					var getParent = $(this).parent();
					
					var obj = getParent.find("input").first();				
					obj.attr("type","password");
				
			});		
			/*End showpass*/
			
			/* theme box : togle body theme	*/
			$( ".theme-drawer a" ).bind( "click", function() {
				
				var clickedClass = $(this).attr('body-color'); // or var clickedBtnID = this.id
				
				var bodyclass=$("body").attr("class");
				var arr = bodyclass.split(' ');
				for(var i=0;i<arr.length;i++){
				 if(arr[i].toLowerCase().indexOf("theme-") >= 0){
				  arr=$.grep(arr, function(value) {
					return value != arr[i];
				  });
				  break;
				 }     
				}
				var newBclass= arr.toString();
				newBclass=newBclass.replace(/,/g,' ');
				
				$("body").attr("class",newBclass);
				$("body").addClass(clickedClass);
				setCookie('bodyClass',clickedClass);
			});
			/* end theme box : togle body theme	*/
			
		/************ END SETTINGS FUNCTIONS ************/
		
		/************ PAGES FUNCTIONS ************/
				
			/* pages page  : vertical tabs manage - create page : mobile dropdown */
			$("body").on("click", ".pages-page .vertical-tabs .nav-tabs li a", function(e){
				
				var sparent = $(this).parents(".tabs-list");
				var ref = $(this).attr("href");
				var text = $(this).html();
				
				if($(this).parents(".dropdown").length > 0){
					resetPageCreateTabs();
					$(".pages-page .vertical-tabs .text-menu").find("a[href='"+ref+"']").parents("li").addClass("active");
				}else{
					sparent.find(".dropdown").find('.dropdown-toggle').html(text+ " <span class='caret'></span>");
					sparent.find(".dropdown").find('.dropdown-toggle').val(text);
				}
			});
			/* end pages page  : vertical tabs manage - create page : mobile dropdown */
		
			/* manage create page - vertical tab dropdown responsive */
			$("body").on("click", ".createform .text-menu .nav-tabs li", function(e){
				var thisHref = $(this).find("a").attr("href");		
				$(".createform .mbl-menu .nav-tabs li a").filter("[href='" + thisHref + "']").trigger("click");
			});
			/* end manage create page - vertical tab dropdown responsive */

		/************ END PAGES FUNCTIONS ************/
						
		/************ PLACES FUNCTIONS ************/
						
			/* swich places main tabs */
			
				$("body").on("click", ".tabicon .icon-menu li", function(){
					resetPlaceTabs(".tablist .text-menu");
					
					if($(this).find("a").attr("href")=="#places-lodge"){
						setPlacesSidebar("hide");
					} else {
						setPlacesSidebar("show");
					}
					
					$("body").getNiceScroll().resize();
					
					setHideHeader(this,'places','none');
					
					manageNewPostBubble("hideit");
				});
				
			/* end swich places main tabs */
		
			/* swich places sub tabs */
				$("body").on("click", ".tablist .text-menu li", function(){
					
					resetTabs(".tabicon .icon-menu");
					
					setPlacesSidebar("show");
					
					if($(this).find("a").html()=="Photos"){			
						setTimeout(function(){
							$(".gloader").hide();
							initGalleryImageSlider();
						},700);			
					}
					if($(this).find("a").html()=="Reviews"){				
						setTimeout(function(){fixImageUI();},400);
					}
					if($(this).find("a").html()=="Tip"){				
						setTimeout(function(){fixImageUI();},400);
					}
					
					if($(this).find("a").html()=="Reviews" || $(this).find("a").html()=="Hangout" || $(this).find("a").html()=="Tips" || $(this).find("a").html()=="Ask"){				
						manageNewPostBubble("showit");
					} else {
						manageNewPostBubble("hideit");
					}
					
					setHideHeader(this,'places','none');
				});

				$("body").on("click", ".tablist .text-menu li", function(event){
					$href = $(this).find('a').attr('href');
					$hrefBulk = ['#places-discussion','#places-tripstory','#places-reviews', '#places-tip', '#places-ask'];
					
					if($.inArray($href, $hrefBulk) !== -1) {
						$('.cshfsiput').addClass('cshfsi');
					} else {
						$('.cshfsiput').removeClass('cshfsi');
					}
				});

			/* end swich places sub tabs */
			
			/* fix images on places */
			if($(".main-content").hasClass("places-page")){	fixPlacesImageUI("all");}
			/* end fix images on places */
			
			/* set Wall tab default for mobile dropdown */
			/*if($(window).width()<=450){				
				$(".select2places").val("all").change();
			}*/
			/* set end Wall tab default for mobile dropdown */
			
			/* select-2 */			
			/*var $eventLog = $(".select2places");
			$eventLog.select2({minimumResultsForSearch: Infinity,containerCssClass : "walltabs"});			
			$eventLog.on("select2:select", function (e) { placesTabDropdown("select2:select", e); });*/
			/* end select-2 */
			
			/* Ask */
			$(document.body).on('click', '.customeditpopup-modal-ask', function(e) {
				var editpostid = $(this).data('editpostid');
				if(editpostid != '') {
					var fullidnm = '#editpost-popup-'+editpostid;
				}

				$.ajax({
					type: 'POST',
					url: '?r=places/edit-post-pre-set-ask', 
					data:'editpostid='+editpostid,
					success: function(data) {
						if(data) {
							if(data == 'checkuserauthclassnv') {
								checkuserauthclassnv();
							} 
							else if(data == 'checkuserauthclassg') {
								checkuserauthclassg();
							} 
						}
					}
				});        	
			});
			/* Ask */
		
			/* Tip */
			$(document.body).on('click', '.customeditpopup-modal-tip', function(e) {
				var editpostid = $(this).data('editpostid');
				if(editpostid != '') {
					var fullidnm = '#editpost-popup-'+editpostid;
				}

				$.ajax({
					type: 'POST',
					url: '?r=places/edit-post-pre-set-tip', 
					data:'editpostid='+editpostid,
					success: function(data) {
						if(data) {
							if(data == 'checkuserauthclassnv') {
								checkuserauthclassnv();
							} 
							else if(data == 'checkuserauthclassg') {
								checkuserauthclassg();
							} 
						}
					}
				});        	
			});
			/* Tip */
		
			/* Place Review */
			$(document.body).on('click', '.customeditpopup-modal-place-review', function(e) {
				var editpostid = $(this).data('editpostid');
				if(editpostid != '') {
					var fullidnm = '#editpost-popup-'+editpostid;
				}

				$.ajax({
					type: 'POST',
					url: '?r=places/edit-post-pre-set-place-review', 
					data:'editpostid='+editpostid,
					success: function(data) {
						if(data) {
							if(data == 'checkuserauthclassnv') {
								checkuserauthclassnv();
							} 
							else if(data == 'checkuserauthclassg') {
								checkuserauthclassg();
							}
						}
					}
				});        	
			});
			/* Place Review */
		
		/************ END PLACES FUNCTIONS ************/
		
		/************ HIRE A GUIDE FUNCTIONS ************/
			
			$( ".activity-checkboxes .subcheck" ).bind( "click", function() {
				if($( ".activity-checkboxes .check-all").is(':checked')){
					$( ".activity-checkboxes .check-all").prop("checked",false);
				}
			});
			$( ".activity-checkboxes .check-all").bind( "click", function() {
				
				if ($(this).is(':checked')){
					$('.activity-checkboxes .h-checkbox').each(function(){
						$(this).find("input[type='checkbox']").prop("checked",true);
					});				
				}else{
					$('.activity-checkboxes .h-checkbox').each(function(){
						$(this).find("input[type='checkbox']").prop("checked",false);
					});		
				}
			});

		/************ END HIRE A GUIDE FUNCTIONS ************/
	
		$(document).on('click', '.toggleSlider', function (e) {		
			var detailMode=$(this).parents(".mode-holder").find(".detail-mode");
			if(detailMode.css("display")=="none"){
				$(this).parents(".mode-holder").find(".normal-mode").fadeOut();
				//$(this).parents(".mode-holder").find(".normal-mode").css("opacity",0);
			}else{				
				$(this).parents(".mode-holder").find(".normal-mode").fadeIn();
				//$(this).parents(".mode-holder").find(".normal-mode").css("opacity",1);
			}
			detailMode.slideToggle();			
		});
	});
/* end document ready */

/************ ESSENTIALS FUNCTIONS ************/

/* window scrolled */

	$(window).scroll(function() {		
		var fromTop=$(window).scrollTop();
		var winh=$(window).height();
		var top=$(window).scrollTop();
		var winw=$(window).width();
		var win_w = $(window).width();
		
		setNewPostBubble(winw,top,'window');
		
		if(fromTop > 100 && winw > 767){
			$(".scrollup-float").show();			
			if(!$(".floating .chat-button").parent().hasClass("chat-open"))
				$(".floating .chat-button").addClass("chathide");			
		}
		else{
			$(".scrollup-float").hide();			
			if(!$(".floating .chat-button").parent().hasClass("chat-open"))
				$(".floating .chat-button").removeClass("chathide");			
		}
				
		/* fixed header */				 
		if(fromTop > 0){
			if($(".home-page.bodydup").length) {
				$("header").addClass("fixed-header");
			}
			if(winw>1024){				
				$(".header-section").addClass("fixed-header");
				$(".page-wrapper").addClass('fixed-wrapper');			
			}
			
			if(winw<=568){
				if($(".page-wrapper").hasClass("menuhideicons-wrapper")){
					$(".header-section .mainpage-name").show();
				}
			}

			if($(".page-wrapper").hasClass("transheadereffectall")) {
				$(".page-wrapper").addClass('page-scrolled');
				$(".page-wrapper").removeClass('JIS3829');
			} else if(winw<=742){
				if($(".page-wrapper").hasClass("transheadereffect")){
					$(".page-wrapper").addClass('page-scrolled');
				}
			}
		} else {
			if($(".home-page.bodydup").length) {
				$("header").removeClass("fixed-header");
			}
			$(".header-section").removeClass("fixed-header");
			$(".page-wrapper").removeClass('fixed-wrapper');
			
			if(winw<=568){
				if($(".page-wrapper").hasClass("menuhideicons-wrapper")){
					$(".header-section .mainpage-name").hide();
				}
			}

			if($(".page-wrapper").hasClass("transheadereffectall")) {
				$(".page-wrapper").removeClass('page-scrolled');
				$(".page-wrapper").addClass('JIS3829');
			} else if(winw<=742){
				if($(".page-wrapper").hasClass("transheadereffect")){
					$(".page-wrapper").removeClass('page-scrolled');
				}
			}
		}
		/* end fixed header */
		
		/* bottom of the page reached */		
		if($(window).scrollTop() + $(window).height() == $(document).height()) {		   
		   $(".page-wrapper").addClass('page-bottom');
	    }else{
		   $(".page-wrapper").removeClass('page-bottom');
	    }
		/* end bottom of the page reached */
		
		/* colored header */
		if(fromTop > 0){
			if($(".page-wrapper").hasClass("place-wrapper") || $(".page-wrapper").hasClass("transheader-wrapper"))
				$(".header-section").addClass("colored");
		}else{
			if($(".page-wrapper").hasClass("place-wrapper") || $(".page-wrapper").hasClass("transheader-wrapper"))
				$(".header-section").removeClass("colored");				
		}
		/* end colored header */
		
		/* fixed header chat*/		
		if(fromTop > 0){
			if($(".float-chat").hasClass("chat-open")){
				$(".float-chat").addClass("stickToTop");
			}
		} else {
			if($(".float-chat").hasClass("stickToTop")){			
				$(".float-chat").removeClass("stickToTop");
			}
		}
		/* end fixed header chat*/
		
		/* fixed header */		
		
		if(fromTop > 78){
			if(win_w<1170){								
				$(".float-chat").addClass("fixed-chat");
			}
			if(win_w <= 992 && win_w > 767){
				
				if($(".main-page").hasClass("places-page")){
					$(".search-area").addClass("fixed-filter");
				}
			}			
		}
		else if(fromTop > 50){
			if(win_w<1170){
				$(".sidemenu-holder").addClass("fixed-menu");				
			}
			if(win_w <= 992){
				
				if($(".main-page").hasClass("hotels-page")){
					$(".search-area").addClass("fixed-filter");
				}
			}			
			if(win_w <= 800){
				$(".gdetails-summery .search-area").addClass("fixed-filter");
			}
		}
		else if(fromTop > 5){
			
			if(win_w <= 767){
				
				if($(".main-page").hasClass("hotels-page")){
					$(".search-area").addClass("fixed-filter");
				}				
			}
			
		}else {			
			$(".sidemenu-holder").removeClass("fixed-menu");
			
			$(".float-chat").removeClass("fixed-chat");
			
			$(".gdetails-summery .search-area").removeClass("fixed-filter");
			
			if($(".main-page").hasClass("hotels-page") || $(".main-page").hasClass("places-page")){
				$(".search-area").removeClass("fixed-filter");
			}
		}
		/* end fixed header */
		
		var winw=$(window).width();
		if(winw>1024){
		   if($(window).scrollTop() + $(window).height() > $(document).height() - 40) {
			   $(".chat-window").addClass('stick-to-footer');
		   }else{
			   $(".chat-window").removeClass('stick-to-footer');		   
		   }
		}else{
			if($(".chat-window").hasClass('stick-to-footer')) $(".chat-window").removeClass('stick-to-footer');
		}
	});
	
/* end window scrolled */

/* fixed layout scrolled */

	$(".fixed-layout").scroll(function() {
		
		var top = $('.page-wrapper').offset().top;		
		var win_w = $(window).width();
		
		setNewPostBubble(win_w,top,'fixed-layout');
		
		var fromTop=$('.fixed-layout').scrollTop();
		
		/* fixed header */				
	
		if(fromTop > 0){
			if(win_w>1024){				
				$(".header-section").addClass("fixed-header");
				$(".page-wrapper").addClass('fixed-wrapper');			
			}
			
			if(win_w<=568){
				if($(".page-wrapper").hasClass("menuhideicons-wrapper")){
					$(".header-section .mainpage-name").show();
				}
			}

			if($(".page-wrapper").hasClass("transheadereffectall")) {
				$(".page-wrapper").addClass('page-scrolled');
				$(".page-wrapper").removeClass('JIS3829');
			} else if(winw<=742){
				if($(".page-wrapper").hasClass("transheadereffect")){
					$(".page-wrapper").addClass('page-scrolled');
				}
			}
		} else {
			$(".header-section").removeClass("fixed-header");
			$(".page-wrapper").removeClass('fixed-wrapper');
			
			if(win_w<=568){
				if($(".page-wrapper").hasClass("menuhideicons-wrapper")){
					$(".header-section .mainpage-name").hide();
				}
			}

			if($(".page-wrapper").hasClass("transheadereffectall")) {
				$(".page-wrapper").removeClass('page-scrolled');
				$(".page-wrapper").addClass('JIS3829');
			} else if(winw<=742){
				if($(".page-wrapper").hasClass("transheadereffect")){
					$(".page-wrapper").removeClass('page-scrolled');
				}
			}
		}
		/* end fixed header */
		
		/* bottom of the page reached */		
		if($(window).scrollTop() + $(window).height() == $(document).height()) {		   
		   $(".page-wrapper").addClass('page-bottom');
	    }else{
		   $(".page-wrapper").removeClass('page-bottom');
	    }
		/* end bottom of the page reached */
		
		/* fixed header */		
		if(fromTop > 78) {
			if(win_w<1170){								
				$(".float-chat").addClass("fixed-chat");
			}
			if(win_w <= 992 && win_w > 767){
				
				if($(".main-page").hasClass("places-page")){
					$(".search-area").addClass("fixed-filter");
				}
			}			
		}
		else if(fromTop > 50) {
			if(win_w<1170){
				$(".sidemenu-holder").addClass("fixed-menu");				
			}
			if(win_w <= 992){
				
				if($(".main-page").hasClass("hotels-page")){
					$(".search-area").addClass("fixed-filter");
				}
			}			
			if(win_w <= 800){
				$(".gdetails-summery .search-area").addClass("fixed-filter");
			}
		}
		else if(fromTop > 5) {
			
			if(win_w <= 767) {
				
				if($(".main-page").hasClass("hotels-page")){
					$(".search-area").addClass("fixed-filter");
				}				
			}
			
		}else {			
			$(".sidemenu-holder").removeClass("fixed-menu");
			
			$(".float-chat").removeClass("fixed-chat");
			
			$(".gdetails-summery .search-area").removeClass("fixed-filter");
			
			if($(".main-page").hasClass("hotels-page") || $(".main-page").hasClass("places-page")){
				$(".search-area").removeClass("fixed-filter");
			}
		}
		/* end fixed header */
		
	});
/* end fixed layout scrolled */

/* body scrolled */
	
	$('body').scroll(function() {
	
		var top = $('.page-wrapper').offset().top;
		var win_w = $(window).width();
		
		setNewPostBubble(win_w,top,'body');
		
		var fromTop=$('body').scrollTop();
		
		/* fixed header */				
		if(fromTop > 0 || top < 0){
			if(win_w>1024){				
				$(".header-section").addClass("fixed-header");
				$(".page-wrapper").addClass('fixed-wrapper');			
			}
			if(win_w<=568){
				if($(".page-wrapper").hasClass("menuhideicons-wrapper")){
					$(".header-section .mainpage-name").show();
				}
			}

			if($(".page-wrapper").hasClass("transheadereffectall")) {
				$(".page-wrapper").addClass('page-scrolled');
				$(".page-wrapper").removeClass('JIS3829');
			} else if(winw<=742){
				if($(".page-wrapper").hasClass("transheadereffect")){
					$(".page-wrapper").addClass('page-scrolled');
				}
			}
		} else {
			$(".header-section").removeClass("fixed-header");
			$(".page-wrapper").removeClass('fixed-wrapper');
			
			if(win_w<=568){
				if($(".page-wrapper").hasClass("menuhideicons-wrapper")){
					$(".header-section .mainpage-name").hide();
				}
			}

			if($(".page-wrapper").hasClass("transheadereffectall")) {
				$(".page-wrapper").removeClass('page-scrolled');
				$(".page-wrapper").addClass('JIS3829');
			} else if(winw<=742){
				if($(".page-wrapper").hasClass("transheadereffect")){
					$(".page-wrapper").removeClass('page-scrolled');
				}
			}
		}
		/* end fixed header */
		
		/* colored header */
		if(fromTop > 0){
			if($(".page-wrapper").hasClass("place-wrapper") || $(".page-wrapper").hasClass("transheader-wrapper"))
				$(".header-section").addClass("colored");
		}else{
			if($(".page-wrapper").hasClass("place-wrapper") || $(".page-wrapper").hasClass("transheader-wrapper"))
				$(".header-section").removeClass("colored");				
		}
		/* end colored header */
		
		/* bottom of the page reached */		
		if($(window).scrollTop() + $(window).height() == $(document).height()) {		   
		   $(".page-wrapper").addClass('page-bottom');
	    }else{
		   $(".page-wrapper").removeClass('page-bottom');
	    }
		/* end bottom of the page reached */
		
		/* fixed header */	
		if(fromTop > 78){
			if(win_w<1170){				
				$(".float-chat").addClass("fixed-chat");
			}
			if(win_w <= 992 && win_w > 767){
				
				if($(".main-page").hasClass("places-page")){
					$(".search-area").addClass("fixed-filter");
				}
			}			
		}
		else if(fromTop > 50){
			if(win_w<1170){
				$(".sidemenu-holder").addClass("fixed-menu");				
			}
			if(win_w <= 992){
				
				if($(".main-page").hasClass("hotels-page")){
					$(".search-area").addClass("fixed-filter");
				}
			}			
			if(win_w <= 800){
				$(".gdetails-summery .search-area").addClass("fixed-filter");
			}
		}
		else if(fromTop > 5){
			
			if(win_w <= 767){
				
				if($(".main-page").hasClass("hotels-page")){
					$(".search-area").addClass("fixed-filter");
				}				
			}
			
		}else {			
			$(".sidemenu-holder").removeClass("fixed-menu");			
			$(".float-chat").removeClass("fixed-chat");			
			$(".gdetails-summery .search-area").removeClass("fixed-filter");
			
			if($(".main-page").hasClass("hotels-page") || $(".main-page").hasClass("places-page")){
				$(".search-area").removeClass("fixed-filter");
			}
		}
		/* end fixed header */
		
		if($(".footer-section").isVisible()) {
			$(".chat-window").addClass('chat-with-footer');
			var chatH = $('.chat-conversation .chat-area').height();
			chatHU = chatH - 40;
			$('.chat-conversation .chat-area').height(chatHU);
		} else {			
			if($('.chat-window').hasClass('chat-with-footer')){
				var chatH = $('.chat-conversation .chat-area').height();
				chatHU = chatH + 40;
				$('.chat-conversation .chat-area').height(chatHU);
			}
			$(".chat-window").removeClass('chat-with-footer');
			
		}
	});
	
/* end body scrolled */

/* document click */

	$(document).click(function(event) { 

		if(!$(event.target).closest('.btnbox').length) {
			if($('.btnbox').is(":visible")) {
				$('.btnbox').removeClass('box-open');
			}
		}		
		closeSearchSection(event);
		$('.carousel-albums .carousel-inner').css("overflow","hidden");		
	})

/* end document click */

/* body click */
	
	$('body').on('click', function (e) {
		/* close resist dropdrown */
		var rtrgt = $('.dropdown.resist');
		var rotrgt = $('.dropdown.resist.open');
		
		if ((!rtrgt.is(e.target) & rtrgt.has(e.target).length === 0) ||
			(!rotrgt.is(e.target) & rotrgt.has(e.target).length === 0) ||
			$(e.target).parents("li").attr("class")=="cancel_post selected"
		){
			$('.dropdown.resist').removeClass('open');
		}
		/* end close resist dropdrown */
		if (!$('.sliding-middle-out').is(e.target) & $('.sliding-middle-out').has(e.target).length === 0 & !$('.pa-comment').is(e.target))
			clearUnderline();

		var isSettings=$("settings-page").length;
		var winw = $(window).width();		

		if(isSettings>0 && winw < 1024){
			/* Settings Side Menu */
			if(e.target.className == 'mdi mdi-menu' || $(e.target).parents(".sidemenu-holder").length > 0) {
			   if(e.target.className == 'mdi mdi-menu') {
				   setMobileMenu('alt');			  
			   }else if($(e.target).parents("a").attr("class")=="closemenu" || $(e.target).parents("ul").hasClass('submenu')){
				   setMobileMenu('close');			
			   } else {
					setMobileMenu('open');			
			   }
			   initNiceScroll(".nice-scroll");
			} 
			else {
				   setMobileMenu('close');			
			}
			/* End Settings Side Menu */
		}else{
			
			if(e.target.className == 'mdi mdi-menu' || e.target.className == ".sidemenu-holder") {
			
			   if(e.target.className == 'mdi mdi-menu') {
				   setMobileMenu('alt');			  
			   } else {
					setMobileMenu('open');			
			   }
			   initNiceScroll(".nice-scroll");
			   
			} else {
				
				   setMobileMenu('close');			
			}
		}
		
		/* close emotion-box */
		
		var emotrgt = $('.emotion-box');
				
		if(e.target.className != 'emotion-box' && e.target.className != 'zmdi zmdi-mood' && emotrgt.has(e.target).length === 0){			
			$(".emotion-box").hide();
		}
		
		/* end close emotion-box */
		
  });
/* end body click */
	
/* window resize */
	var resizeId;
	$(window).resize(function() {		
		clearTimeout(resizeId);				
		resizeId = setTimeout(doneResizing, 300);
		
		/* set chat window height */					
		setChatHeight();
		/* end set chat window height */

		if (window.location.href.indexOf("collections-detail") > -1) { 
			var win_w=$(window).width();
			if(win_w>480 && win_w<=568) {
				$('.page-wrapper').removeClass('menutransheader-wrapper menuhideicons-wrapper');		
			} else {
				$('.page-wrapper').addClass('menutransheader-wrapper menuhideicons-wrapper');		
			}
		}

		var winw = $(window).width();
		if(winw <= 767) {
			if($('#pagesettings-general').hasClass('active')) {
				$('#pagesettings-general .editicon1').css('display', 'none');
				$('#pagesettings-general .editicon2').css('display', 'block');
			}

			if($('#pagesettings-blocking').hasClass('active')) {
				$('#pagesettings-blocking .editicon1').css('display', 'none');
				$('#pagesettings-blocking .editicon2').css('display', 'block');
			}
		} else {
			if($('#pagesettings-general').hasClass('active')) {
				$('#pagesettings-general .editicon1').css('display', 'block');
				$('#pagesettings-general .editicon2').css('display', 'none');
			}

			if($('#pagesettings-blocking').hasClass('active')) {
				$('#pagesettings-blocking .editicon1').css('display', 'block');
				$('#pagesettings-blocking .editicon2').css('display', 'none');
			}
		}

		$headernav = $('.header-nav').width();
		$__s = parseInt(winw) - parseInt($headernav);
		$__s = parseInt($__s) / 2;
		$('.sticky-nav').find('.col').css('width', $headernav+'px');
		$('.sticky-nav').find('.col').css('margin-left', $__s+'px');
	});	
/* end window resize */

/* percentage page loader function */
	function runLoad(){		
		interval = setInterval(increment,100);
	}	
	function increment(){
		current++;
		
		var onHold=$('body').hasClass("hold");
		if($('body').hasClass("hold")){
			if(current>0){
				clearInterval(interval);				
			}			
		}		
		if($('.hcontent-holder.banner-section').is(':visible') || current >= 50){ //if the container is visible on the page
			hideLoader();
		}
		
	 }
/* end percentage page loader function */

/************ END ESSENTIALS FUNCTIONS ************/

$(window).load(function(){
	/************ GENERAL FUNCTIONS ************/
		
		$('.added-tags').tooltipster({
			contentAsHTML: true,
			side: ['top', 'bottom'],theme: ['tooltipster-borderless'],
		});
		$('.title-tooltip').tooltipster({
			contentAsHTML: true
		});
		$('.simple-tooltip').tooltipster({
			contentAsHTML: true,
			trigger:'click',
			theme: ['tooltipster-borderless']
		});
		$('.inline-tooltip').tooltipster({
			contentAsHTML: true,
			theme: ['tooltipster-borderless']
		});
	/************ END GENERAL FUNCTIONS ************/
});

/*$('.modal').modal({
    endingTop: '50%' // Ending top style attribute
});*/

$( function() {
	$( "#tabs" ).tabs();
});

$(document).on('click', '.img-row.layered .img-box', function (e) {
	$this = $(this);

	if($this.find('.custom-file').length) {
	} else {
		$uploadpopupJIDSphototitle = $('.upload-popupJIDS-phototitle').val();
		$uploadpopupJIDSdescription = $('.upload-popupJIDS-description').val();
		$uploadpopupJIDSlocation = $('.upload-popupJIDS-location').val();
		$uploadpopupJIDStaggedconnections = $('.upload-popupJIDS-taggedconnections').val();
		$uploadpopupJIDSphotocategories = $('.upload-popupJIDS-photocategories').val();

		var puttedData = {
			'$uploadpopupJIDSphototitle' : $uploadpopupJIDSphototitle,
			'$uploadpopupJIDSdescription' : $uploadpopupJIDSdescription,
			'$uploadpopupJIDSlocation' : $uploadpopupJIDSlocation,
			'$uploadpopupJIDStaggedconnections' : $uploadpopupJIDStaggedconnections,
			'$uploadpopupJIDSphotocategories' : $uploadpopupJIDSphotocategories				
		}

		if($('.img-row.layered').find('.img-box.activelayered').length) {
			$oldindex = $('.img-row.layered').find('.img-box.activelayered').index();
			photoUpload[$oldindex] = puttedData;			
		} else {
			photoUpload[0] = puttedData;			
		}

		
		$('.img-row.layered').find('.img-box').removeClass('activelayered');
		$this.addClass('activelayered');

		$index = $('.img-row.layered .img-box').index($this);

		$('#layeredform')[0].reset();
		if(typeof photoUpload[$index] === 'undefined') {

		} else {
			$storeduploadpopupJIDSphototitle = photoUpload[$index].$uploadpopupJIDSphototitle;
			$storeduploadpopupJIDSdescription = photoUpload[$index].$uploadpopupJIDSdescription;
			$storeduploadpopupJIDSlocation = photoUpload[$index].$uploadpopupJIDSlocation;
			$storeduploadpopupJIDStaggedconnections = photoUpload[$index].$uploadpopupJIDStaggedconnections;
			$storeduploadpopupJIDSphotocategories = photoUpload[$index].$uploadpopupJIDSphotocategories;

			$('.upload-popupJIDS-phototitle').val($storeduploadpopupJIDSphototitle);
			$('.upload-popupJIDS-description').val($storeduploadpopupJIDSdescription);
			$('.upload-popupJIDS-location').val($storeduploadpopupJIDSlocation);
			$('.upload-popupJIDS-taggedconnections').val($storeduploadpopupJIDStaggedconnections);
			$('.upload-popupJIDS-photocategories').val($storeduploadpopupJIDSphotocategories);
		}

	}

});

$(document).on('click', '.customli_modal', function () {
    $('#custom_user_modal').find('.person_box').html('');
    $('#custom_user_modal').modal('open');
});
 
$('#customdone').on('click', function (event) {
	if($('#privacymodal').length) {
		$('#custom_user_modal').modal('close');
		if($('#privacymodal').hasClass('open')) {
			$("#privacymodal").find('#customdoneprivacymodal').trigger("click");
		}
	} else {
    	$('#custom_user_modal').modal('close');
	}

});

$('#customdoneprivacymodal').on('click', function (event) {
	if($('input[name=privacyradiomodal]:checked').length) {
		var $privacyValue = $('input[name=privacyradiomodal]:checked').val();
		if($.inArray($privacyValue, $post_privacy_array) !== -1) {
			$id = $(this).attr('data-privacy');
			$('.'+$id).find('span').html($privacyValue);
			$('#privacymodal').modal('close');
		}
	}
});

$(document).on('mouseover', '.justified-gallery .allow-gallery', function() {
	$(this).find('.removeicon').show();
});

$(document).on('mouseout', '.justified-gallery .allow-gallery', function() {
	$(this).find('.removeicon').hide();
});

$(document).on('mouseover', '.collection-item.avatar', function() {
	$(this).find('.zmdi.zmdi-delete').show();
	$(this).find('.zmdi.zmdi-edit').show();
});

$(document).on('mouseout', '.collection-item.avatar', function() {
	$(this).find('.zmdi.zmdi-delete').hide();
	$(this).find('.zmdi.zmdi-edit').hide();
});

$(document).on('mouseover', '.photobox', function() {
	$(this).find('.photosdelete').css('visibility', 'visible');
});

$(document).on('mouseout', '.photobox', function() {
	$(this).find('.photosdelete').css('visibility', 'hidden');
});