<?php include("header.php"); ?>
<div class="floating-icon">
   <div class="scrollup-btnbox anim-side btnbox scrollup-float">
      <div class="scrollup-button float-icon"><span class="icon-holder ispan"><i class="mdi mdi-arrow-up-bold-circle"></i>
</span></div>
   </div>
</div>
<div class="clear"></div>
<?php include("common/leftmenu.php"); ?>
<div class="fixed-layout ipad-mfix">
   <div class="main-content sub-page credit-page main-page p-0">
      <div class="combined-column wide-open">
         <div class="content-box m-0">
            <div class="cbox-desc">
               <div class="ribbon-section">
                  <h3 class="ribbon-img">i am in <?=$st_nm_L?> credits</h3>
                  <p>I am in <?=$st_nm_L?> credits are virtual currency, <br />which can be used to buy extended services on I am in <?=$st_nm_L?></p>
               </div>
               <div class="container">
                  <div class="credit-summery">
                     <div class="credit-balance left">
                        <h4>Your Iamin<?=$st_nm_L?> Credit Balance</h4>
                        <span class="badge">
                        <span>3</span>
                        <span>5</span>
                        <span>7</span>
                        </span>
                     </div>
                     <div class="add-credit">
                        <a class="waves-effect waves-light btn modal-trigger" href="#payment-popup">Add Credit</a>
                        <div class="clear"></div>
                        <div class="dropdown dropdown-custom lmenu">
                           <a href="javascript:void(0)" class="dropdown-toggle dropdown-button" data-activates='dropdown1'>
                           See History <i class="mdi mdi-chevron-down"></i>
                           </a>
                           <ul id='dropdown1' class='dropdown-content'>
                              <li><a href="credit.php">Credit Benifits</a></li>
                              <li><a href="credit-update.php">See History</a></li>
                              <li><a href="credit-transfer.php">Tranfer Credits</a></li>
                           </ul>
                        </div>
                     </div>
                  </div>
                  <div class="clear"></div>
                  <div class="credit-details">
                     <div class="divider"></div>
                  </div>
                  <div class="credit-details">
                     <h5>Credit Benifits</h5>
                     <ul class="cbenifits-ul">
                        <li><span><i class="zmdi zmdi-check"></i></span>Boost your popularity</li>
                        <li><span><i class="zmdi zmdi-check"></i></span>Send eGift for 100 credits</li>
                        <li><span><i class="zmdi zmdi-check"></i></span>Transfer Credits to your connections</li>
                        <li><span><i class="zmdi zmdi-check"></i></span>First in search list</li>
                     </ul>
                  </div>
                  <div class="credit-details">
                     <h5>Members can earn credits and increase their popularity by doing the following actions</h5>
                     <ul class="cdetails-ul">
                        <li><i class="zmdi zmdi-sign-in"></i>Member signup<span>Member initial signup, earns 20 credits</span></li>
                        <li><i class="zmdi zmdi-plus-square"></i>Add to site<span>Create a collection, earns 3 credits</span></li>
                        <li><i class="zmdi zmdi-camera-add"></i>Add Profile Photo<span>Add profile and cover photo, earns 10 credits</span></li>
                        <li><i class="zmdi zmdi-copy"></i>Promote business<span>Create a business page, earn 5 credits</span></li>
                        <li><i class="zmdi zmdi-account-add"></i>Connect invitation<span>Member accepts your invitation, earns 1 credit</span></li>
                        <li><i class="zmdi zmdi-thumb-up"></i>Like<span>Like a page, earn 1 credit</span></li>
                        <li><i class="zmdi zmdi-share"></i>Share an ad,<span>Share sponsored ad or page, earns 2 credits</span></li>
                        <li><i class="zmdi zmdi-airplane"></i>Travel experience<span>Post your travel experience, earns 2 credits</span></li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <?php include('common/chat.php'); ?>
   </div> 
</div>
<?php include("common/footer.php"); ?>
</div>	
<div id="payment-popup" class="modal payment-popup credit-payment-modal fullpopup">
   <?php include('common/credit_payment_popup.php'); ?>
</div>
<?php include("script.php"); ?>
</body>
</html>