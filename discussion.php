<?php include("header.php"); ?>
<?php include("common/menu.php"); ?>
<div class="floating-icon">
   <div class="scrollup-btnbox anim-side btnbox scrollup-float">
      <div class="scrollup-button float-icon">
         <span class="icon-holder ispan">
            <i class="mdi mdi-arrow-up-bold-circle"></i>
         </span>
      </div>
   </div>
</div>
<div class="clear"></div>
<div class="">
   <?php include("common/leftmenu.php"); ?>
   <div class="fixed-layout">
      <div class="main-content main-page places-page pb-0 m-t-50">
         <div class="combined-column wide-open main-page full-page">
         <div class="tablist sub-tabs">
            <ul class="tabs tabs-fixed-width text-menu left tabsnew">
               <li class="tab"><a tabname="Wall" href="#places-all"></a></li>
            </ul>
         </div>
         <div class="places-content places-all">
            <div class="container cshfsiput cshfsi">
               <div class="places-column cshfsiput cshfsi m-top">
                  <div class="tab-content">
                     <div id="places-discussion" class="placesdiscussion-content subtab bottom_tabs">
                        <div class="row cshfsiput cshfsi">
                           <div class="social-section profile-box detailBox">
                              <div class="user-profile">
                                 <div class="avatar-left">
                                    <img src="images/recent-3.png">
                                 </div>
                                 <div class="avatar-middle">
                                    <img src="images/recent-3.png">
                                 </div>
                                 <div class="avatar-right">
                                    <img src="images/recent-3.png">
                                 </div>
                              </div>
                              <div class="item titlelabel user-info">
                               <p class="center-align u-name mb0">Discussion for <?=$st_nm_S?> (<span class="count">10</span>)</p>
                               <p class="user-desg center-align"></p>
                              </div>
                             <div class="item profile-info row mx-0 width-100">
                                 <div class="sub-item">
                                    <h5 class="">Tips</h5>
                                    <p class="">20</p>
                                 </div>
                                 <div class="sub-item">
                                    <h5 class="">Reviews</h5>
                                    <p class="">20</p>
                                 </div>
                                 <div class="sub-item">
                                    <h5 class="">Questions</h5>
                                    <p class="">20</p>
                                 </div>
                                 <div class="sub-item">
                                    <h5 class="">Photostream</PB_photosh5>
                                    <p class="">20</p>
                                 </div>
                                 <div class="sub-item">
                                    <h5 class="">Locals</h5>
                                    <p class="">20</p>
                                 </div>
                                 <div class="sub-item">
                                    <h5 class="">Travellers</h5>
                                    <p class="">20</p>
                                 </div>
                              </div>
                           </div>
                           <div class="postBox">
                              <div class="content-box ">
                                 <div class="new-post base-newpost cshfsiput cshfsi compose_discus" id="composetoolboxAction">
                                    <div class="npost-content">
                                       <div class="post-mcontent">
                                          <i class="mdi mdi-pencil-box-outline main-icon"></i>
                                          <div class="desc">
                                             <div class="input-field comments_box">
                                                <p>Discussion for <?=$st_nm_S?></p>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="cbox-desc nm-postlist post-list cshfsiput cshfsi">
                                    <div class="post-holder bborder tippost-holder">
                                       <div class="post-topbar">
                                          <div class="post-userinfo">
                                             <div class="img-holder">
                                                <div id="profiletip-1" class="profiletipholder">
                                                   <span class="profile-tooltip">
                                                   <img class="circle" src="images/demo-profile.jpg" />
                                                   </span>
                                                   <span class="profiletooltip_content">
                                                      <div class="profile-tip">
                                                         <div class="profile-tip-cover"><img src="images/cover.jpg"></div>
                                                         <div class="profile-tip-avatar">
                                                            <a href="javascript:void(0)">
                                                            <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">
                                                            </a>
                                                         </div>
                                                         <div class="profile-tip-info">
                                                            <div class="cover-username"><a href="javascript:void(0)">Adel Hasanat</a></div>
                                                            <div class="cover-headline">
                                                               <span class="ptip-icon"><i class="fa  fa-suitcase"></i></span>
                                                               Web Designer, Cricketer
                                                            </div>
                                                            <div class="profiletip-bio">
                                                               <span class="ptip-icon"><i class="mdi mdi-home"></i></span>
                                                               Lives in : <span>Gariyadhar</span>
                                                            </div>
                                                            <div class="profiletip-bio">
                                                               <span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span>
                                                               Currently in : <span>Gariyadhar, Gujarat, India</span>
                                                            </div>
                                                         </div>
                                                         <div class="profile-tip-divider"></div>
                                                         <div class="profile-tip-btn">
                                                            <a href="javascript:void(0)" class="btn btn-primary btn-sm"><i class=”mdi mdi-eye”></i>View Profile</a>
                                                         </div>
                                                      </div>
                                                   </span>
                                                </div>
                                             </div>
                                             <div class="desc-holder">
                                                <a href="javascript:void(0)">Adel Hasanat</a> tip for <a class="sub-link" href="javascript:void(0)"><?=$st_nm_S?>, <?=$st_nm_S?></a>
                                                <span class="timestamp">August 31 at 08:45 pm<span class="glyphicon glyphicon-globe"></span></span>
                                             </div>
                                          </div>
                                          <div class="settings-icon">
                                             <div class="dropdown">
                                                <a class="dropdown-button" href="javascript:void(0)" data-activates="dropdown-editdisc">
                                                <i class="zmdi zmdi-more zmdi-hc-2x"></i>
                                                </a>
                                                <ul class="dropdown-content" id="dropdown-editdisc">
                                                   <li>
                                                      <a href="javascript:void(0)" class="edit_discus">Edit Discussion</a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)" class="edit_discus">Edit Discussion</a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)" class="edit_discus">Edit Discussion</a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)" class="edit_discus">Edit Discussion</a>
                                                   </li>
                                                </ul>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="post-content">
                                          <div class="post-details">
                                             <div class="post-title">Random Title</div>
                                             <div class="post-desc">
                                                <div class="para-section">
                                                   <div class="para">
                                                      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sed varius risus. Duis rhoncus eros et pellentesque imperdiet.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sed varius risus. Duis rhoncus eros et pellentesque imperdiet.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sed varius risus. Duis rhoncus eros et pellentesque imperdiet.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sed varius risus. Duis rhoncus eros et pellentesque imperdiet.</p>
                                                   </div>
                                                   <a href="javascript:void(0)" onclick="showAllContent(this)">Read More</a>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="post-img-holder">
                                             <div class="lgt-gallery post-img two-img lgt-gallery-photo dis-none">
                                                <a href="images/post-img2.jpg" data-size="1600x1600" data-med="images/post-img2.jpg" data-med-size="1024x1024" data-author="Folkert Gorter" class="pimg-holder himg-box">
                                                <img class="himg" src="images/post-img2.jpg" alt="" />
                                                </a>
                                                <a href="images/post-img3.jpg" data-size="1600x1068" data-med="images/post-img3.jpg" data-med-size="1024x683" data-author="Samuel Rohl" class="pimg-holder vimg-box">
                                                <img class="himg" src="images/post-img3.jpg" alt="" />
                                                </a>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="clear"></div>
                                       <div class="post-data">
                                          <div class="post-actions">
                                             <a href="javascript:void(0)" class="sharepostmodalAction pa-share waves-effect" title="Share"><i class="zmdi zmdi-mail-reply zmdi-hc-flip-horizontal"></i></a>
                                             <span class="lcount">7</span>
                                             <div class="right like-tooltip">
                                                <a href="javascript:void(0)" class="pa-like active" data-title="User Name"><i class="zmdi zmdi-thumb-up"></i></a>
                                                <span class="lcount">4</span>
                                                <a href="javascript:void(0)" class="pa-comment"><i class="zmdi zmdi-comment"></i></a>
                                                <span class="comment-lcount">4</span>
                                             </div>
                                          </div>
                                          <div class="comments-section panel">
                                             <div class="post-more">
                                                <a href="javascript:void(0)" class="view-morec">View more comments</a>
                                                <span class="total-comments">3 of 7</span>
                                             </div>
                                             <div class="post-comments">
                                                <div class="pcomments">
                                                   <div class="pcomment-earlier">
                                                      <div class="pcomment-holder">
                                                         <div class="pcomment main-comment">
                                                            <div class="img-holder">
                                                               <div class="profiletipholder">
                                                                  <span class="profile-tooltip">
                                                                  <img class="circle" src="images/demo-profile.jpg"/>
                                                                  </span>
                                                                  <span class="profiletooltip_content slidingpan-holder">
                                                                     <div class="profile-tip" style="display:none;">
                                                                        <div class="profile-tip-avatar">
                                                                           <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                                                           <div class="sliding-pan location-span">
                                                                              <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                                           </div>
                                                                        </div>
                                                                        <div class="profile-tip-name">
                                                                           <a href="javascript:void(0)">Markand Trivedi</a>
                                                                        </div>
                                                                        <div class="profile-tip-info">
                                                                           <div class="profiletip-icon">
                                                                              <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                                           </div>
                                                                           <div class="profiletip-icon">
                                                                              <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                                           </div>
                                                                           <div class="profiletip-icon">
                                                                              <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                                                           </div>
                                                                        </div>
                                                                     </div>
                                                                  </span>
                                                               </div>
                                                            </div>
                                                            <div class="desc-holder">
                                                               <div class="normal-mode">
                                                                  <div class="desc">
                                                                     <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                                                     <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                                                  </div>
                                                                  <div class="comment-stuff">
                                                                     <div class="more-opt">
                                                                        <span class="likeholder">
                                                                        <span class="like-tooltip">
                                                                        <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                                                           <i class="zmdi zmdi-thumb-up"></i>
                                                                        </a>
                                                                        </span>
                                                                        </span>  
                                                                        <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply">
                                                                           <i class="zmdi zmdi-mail-reply"></i>
                                                                        </a>
                                                                        <!-- Dropdown Trigger -->
                                                                        <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='drop_down100'>
                                                                        <i class="zmdi zmdi-more"></i>
                                                                        </a>
                                                                        <!-- Dropdown Structure -->
                                                                        <ul id='drop_down100' class='dropdown-content custom_dropdown'>
                                                                           <li>
                                                                              <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                                                           </li>
                                                                           <li>
                                                                              <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                                                           </li>
                                                                        </ul>
                                                                     </div>
                                                                     <div class="less-opt">
                                                                        <div class="timestamp">8h</div>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                               <div class="edit-mode">
                                                                  <div class="desc">
                                                                     <textarea class="editcomment-tt materialize-textarea" id="ec-7">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                                                     <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                         <div class="clear"></div>
                                                         <div class="comment-reply-holder comment-addreply">
                                                            <div class="addnew-comment valign-wrapper  comment-reply">
                                                               <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                                                               <div class="desc-holder">                          
                                                                  <textarea class="materialize-textarea">Write a reply...</textarea>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div class="pcomment-holder">
                                                         <div class="pcomment main-comment">
                                                            <div class="img-holder">
                                                               <div class="profiletipholder">
                                                                  <span class="profile-tooltip">
                                                                  <img class="circle" src="images/demo-profile.jpg"/>
                                                                  </span>
                                                                  <span class="profiletooltip_content slidingpan-holder">
                                                                     <div class="profile-tip" style="display:none;">
                                                                        <div class="profile-tip-avatar">
                                                                           <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                                                           <div class="sliding-pan location-span">
                                                                              <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                                           </div>
                                                                        </div>
                                                                        <div class="profile-tip-name">
                                                                           <a href="javascript:void(0)">Markand Trivedi</a>
                                                                        </div>
                                                                        <div class="profile-tip-info">
                                                                           <div class="profiletip-icon">
                                                                              <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                                           </div>
                                                                           <div class="profiletip-icon">
                                                                              <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                                           </div>
                                                                           <div class="profiletip-icon">
                                                                              <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                                                           </div>
                                                                        </div>
                                                                     </div>
                                                                  </span>
                                                               </div>
                                                            </div>
                                                            <div class="desc-holder">
                                                               <div class="normal-mode">
                                                                  <div class="desc">
                                                                     <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                                                     <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                                                  </div>
                                                                  <div class="comment-stuff">
                                                                     <div class="more-opt">
                                                                        <span class="likeholder">
                                                                        <span class="like-tooltip">
                                                                        <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                                                           <i class="zmdi zmdi-thumb-up"></i>
                                                                        </a>
                                                                        </span>
                                                                        </span>  
                                                                        <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply">
                                                                           <i class="zmdi zmdi-mail-reply"></i>
                                                                        </a>
                                                                        <!-- Dropdown Trigger -->
                                                                        <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='drop_down101'>
                                                                        <i class="zmdi zmdi-more"></i>
                                                                        </a>
                                                                        <!-- Dropdown Structure -->
                                                                        <ul id='drop_down101' class='dropdown-content custom_dropdown'>
                                                                           <li>
                                                                              <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                                                           </li>
                                                                           <li>
                                                                              <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                                                           </li>
                                                                        </ul>
                                                                     </div>
                                                                     <div class="less-opt">
                                                                        <div class="timestamp">8h</div>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                               <div class="edit-mode">
                                                                  <div class="desc">
                                                                     <textarea class="editcomment-tt materialize-textarea" id="ec-8">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                                                     <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                         <div class="clear"></div>
                                                         <div class="comment-reply-holder comment-addreply">
                                                            <div class="addnew-comment valign-wrapper  comment-reply">
                                                               <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                                                               <div class="desc-holder">                          
                                                                  <textarea class="materialize-textarea">Write a reply...</textarea>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div class="pcomment-holder">
                                                      <div class="pcomment main-comment">
                                                         <div class="img-holder">
                                                            <div class="profiletipholder">
                                                               <span class="profile-tooltip">
                                                               <img class="circle" src="images/demo-profile.jpg"/>
                                                               </span>
                                                               <span class="profiletooltip_content slidingpan-holder">
                                                                  <div class="profile-tip" style="display:none;">
                                                                     <div class="profile-tip-avatar">
                                                                        <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                                                        <div class="sliding-pan location-span">
                                                                           <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                                        </div>
                                                                     </div>
                                                                     <div class="profile-tip-name">
                                                                        <a href="javascript:void(0)">Markand Trivedi</a>
                                                                     </div>
                                                                     <div class="profile-tip-info">
                                                                        <div class="profiletip-icon">
                                                                           <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                                        </div>
                                                                        <div class="profiletip-icon">
                                                                           <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                                        </div>
                                                                        <div class="profiletip-icon">
                                                                           <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                                                        </div>
                                                                     </div>
                                                                  </div>
                                                               </span>
                                                            </div>
                                                         </div>
                                                         <div class="desc-holder">
                                                            <div class="normal-mode">
                                                               <div class="desc">
                                                                  <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                                                  <p>
                                                                     Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.
                                                                     <a href="javascript:void(0)" class="cmnt-react">
                                                                        <i class="zmdi zmdi-thumb-up"></i>
                                                                        <span class="react-count">1</span>
                                                                     </a>
                                                                  </p>
                                                               </div>
                                                               <div class="comment-stuff">
                                                                  <div class="more-opt">
                                                                     <!-- Dropdown Trigger -->
                                                                     <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='drop_down102'>
                                                                     <i class="zmdi zmdi-more"></i>
                                                                     </a>
                                                                     <!-- Dropdown Structure -->
                                                                     <ul id='drop_down102' class='dropdown-content custom_dropdown'>
                                                                        <li>
                                                                           <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                                                        </li>
                                                                        <li>
                                                                           <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                                                        </li>
                                                                     </ul>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                            <div class="normode-action">
                                                               <span class="likeholder">
                                                                  <span class="like-tooltip">
                                                                     <div class="fixed-action-btn horizontal direction-top direction-left" >
                                                                       <a href="javascript:void(0)" class="post-like">Like</a>
                                                                       <ul>
                                                                         <li>
                                                                           <a class="btn-floating red" style="opacity: 0; transform: scale(0.4) translateY(0px) translateX(40px);">
                                                                             <i class="mdi mdi-thumb-up"></i>
                                                                           </a>
                                                                         </li>
                                                                       </ul>
                                                                     </div>
                                                                  </span>
                                                               </span>  
                                                               <a href="javascript:void(0)" class="pa-reply reply-comment post-reply" title="Reply">
                                                                  Reply
                                                               </a>
                                                               <div class="post-time">8h</div>
                                                            </div>
                                                            <div class="edit-mode">
                                                               <div class="desc">
                                                                  <div class="cmntarea">
                                                                     <textarea class="editcomment-tt materialize-textarea" id="ec-9">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                                                     <ul>
                                                                        <li><a href=""><i class="mdi mdi-emoticon-happy"></i></a></li>
                                                                        <li><a href=""><i class="mdi mdi-camera"></i></a></li>
                                                                     </ul>
                                                                  </div>
                                                                  <a href="javascript:void(0)" class="editcomment-cancel editcomment-action">Update</a>
                                                                  <a href="javascript:void(0)" class="editcomment-cancel editcomment-action">Cancel</a>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div class="clear"></div>
                                                      <div class="comment-reply-holder comment-addreply">
                                                         <div class="addnew-comment valign-wrapper  comment-reply">
                                                            <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                                                            <div class="desc-holder">   
                                                               <div class="cmntarea">                       
                                                                  <textarea class="materialize-textarea">Write a reply...</textarea>
                                                                  <ul>
                                                                     <li><a href=""><i class="mdi mdi-emoticon-happy"></i></a></li>
                                                                     <li><a href=""><i class="mdi mdi-camera"></i></a></li>
                                                                  </ul>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div class="pcomment-holder">
                                                      <div class="pcomment main-comment">
                                                         <div class="img-holder">
                                                            <div class="profiletipholder">
                                                               <span class="profile-tooltip">
                                                               <img class="circle" src="images/demo-profile.jpg"/>
                                                               </span>
                                                               <span class="profiletooltip_content slidingpan-holder">
                                                                  <div class="profile-tip" style="display:none;">
                                                                     <div class="profile-tip-avatar">
                                                                        <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                                                        <div class="sliding-pan location-span">
                                                                           <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                                        </div>
                                                                     </div>
                                                                     <div class="profile-tip-name">
                                                                        <a href="javascript:void(0)">Markand Trivedi</a>
                                                                     </div>
                                                                     <div class="profile-tip-info">
                                                                        <div class="profiletip-icon">
                                                                           <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                                        </div>
                                                                        <div class="profiletip-icon">
                                                                           <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                                        </div>
                                                                        <div class="profiletip-icon">
                                                                           <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                                                        </div>
                                                                     </div>
                                                                  </div>
                                                               </span>
                                                            </div>
                                                         </div>
                                                         <div class="desc-holder">
                                                            <div class="normal-mode">
                                                               <div class="desc">
                                                                  <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                                                  <p class="shorten">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.<a a="" href="javascript:void(0)" class="overlay" onclick="explandReadMore(this)" style="position: absolute;top: 0;bottom: -6px;left: 0;right: 0;"><span class="readlink">Read More</span></a>
                                                                     <a href="javascript:void(0)" class="cmnt-react">
                                                                        <i class="zmdi zmdi-thumb-up"></i>
                                                                        <span class="react-count">1</span>
                                                                     </a>
                                                                  </p>
                                                               </div>
                                                               <div class="comment-stuff">
                                                                  <div class="more-opt">
                                                                     <span class="likeholder">
                                                                     <span class="like-tooltip">
                                                                     <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                                                        <i class="zmdi zmdi-thumb-up"></i>
                                                                     </a>
                                                                     </span>
                                                                     </span>  
                                                                     <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply">
                                                                        <i class="zmdi zmdi-mail-reply"></i>
                                                                     </a>
                                                                     <!-- Dropdown Trigger -->
                                                                     <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='drop_down103'>
                                                                     <i class="zmdi zmdi-more"></i>
                                                                     </a>
                                                                     <!-- Dropdown Structure -->
                                                                     <ul id='drop_down103' class='dropdown-content custom_dropdown'>
                                                                        <li>
                                                                           <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                                                        </li>
                                                                        <li>
                                                                           <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                                                        </li>
                                                                     </ul>
                                                                  </div>
                                                                  <div class="less-opt">
                                                                     <div class="timestamp">8h</div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                            <div class="edit-mode">
                                                               <div class="desc">
                                                                  <textarea class="editcomment-tt materialize-textarea" id="ec-10">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                                                  <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div class="clear"></div>
                                                      <div class="comment-reply-holder comment-addreply">
                                                         <div class="addnew-comment valign-wrapper  comment-reply">
                                                            <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                                                            <div class="desc-holder">                          
                                                               <textarea class="materialize-textarea">Write a reply...</textarea>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="addnew-comment valign-wrapper  ">
                                                   <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                                                   <div class="desc-holder"> 
                                                      <div class="cmntarea">                         
                                                         <textarea class="materialize-textarea">Write a reply...</textarea>
                                                         <ul>
                                                            <li><a href=""><i class="mdi mdi-emoticon-happy"></i></a></li>
                                                            <li><a href=""><i class="mdi mdi-camera"></i></a></li>
                                                         </ul>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="post-holder bborder tippost-holder">
                                       <div class="post-topbar">
                                          <div class="post-userinfo">
                                             <div class="img-holder">
                                                <div id="profiletip-1" class="profiletipholder">
                                                   <span class="profile-tooltip">
                                                   <img class="circle" src="images/demo-profile.jpg" />
                                                   </span>
                                                   <span class="profiletooltip_content">
                                                      <div class="profile-tip">
                                                         <div class="profile-tip-cover"><img src="images/cover.jpg"></div>
                                                         <div class="profile-tip-avatar">
                                                            <a href="javascript:void(0)">
                                                            <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">
                                                            </a>
                                                         </div>
                                                         <div class="profile-tip-info">
                                                            <div class="cover-username"><a href="javascript:void(0)">Adel Hasanat</a></div>
                                                            <div class="cover-headline">
                                                               <span class="ptip-icon"><i class="fa  fa-suitcase"></i></span>
                                                               Web Designer, Cricketer
                                                            </div>
                                                            <div class="profiletip-bio">
                                                               <span class="ptip-icon"><i class="mdi mdi-home"></i></span>
                                                               Lives in : <span>Gariyadhar</span>
                                                            </div>
                                                            <div class="profiletip-bio">
                                                               <span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span>
                                                               Currently in : <span>Gariyadhar, Gujarat, India</span>
                                                            </div>
                                                         </div>
                                                         <div class="profile-tip-divider"></div>
                                                         <div class="profile-tip-btn">
                                                            <a href="javascript:void(0)" class="btn btn-primary btn-sm"><i class=”mdi mdi-eye”></i>View Profile</a>
                                                         </div>
                                                      </div>
                                                   </span>
                                                </div>
                                             </div>
                                             <div class="desc-holder">
                                                <a href="javascript:void(0)">Nimish Parekh</a> tip for <a class="sub-link" href="javascript:void(0)"><?=$st_nm_S?>, <?=$st_nm_S?></a>
                                                <span class="timestamp">August 31 at 08:45 pm<span class="glyphicon glyphicon-globe"></span></span>
                                             </div>
                                          </div>
                                          <div class="settings-icon">
                                             <div class="dropdown">
                                                <a class="dropdown-button" href="javascript:void(0)" data-activates="dropdown-editdisc2">
                                                <i class="zmdi zmdi-more zmdi-hc-2x"></i>
                                                </a>
                                                <ul class="dropdown-content" id="dropdown-editdisc2">
                                                   <li>
                                                      <a href="javascript:void(0)" class="edit_discus">Edit Discussion</a>
                                                   </li>
                                                </ul>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="post-content">
                                          <div class="post-details">
                                             <div class="post-desc">
                                                <div class="para-section">
                                                   <div class="para">
                                                      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sed varius risus. Duis rhoncus eros et pellentesque imperdiet.<br />Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sed varius risus. Duis rhoncus eros et pellentesque imperdiet.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sed varius risus. Duis rhoncus eros et pellentesque imperdiet.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sed varius risus. Duis rhoncus eros et pellentesque imperdiet.</p>
                                                   </div>
                                                   <a href="javascript:void(0)" onclick="showAllContent(this)">Read More</a>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="clear"></div>
                                       <div class="post-data">
                                          <div class="post-actions">
                                             <a href="javascript:void(0)" class="sharepostmodalAction pa-share waves-effect" title="Share"><i class="zmdi zmdi-mail-reply zmdi-hc-flip-horizontal"></i></a>
                                             <span class="lcount">7</span>
                                             <div class="right like-tooltip">
                                                <a href="javascript:void(0)" class="pa-like active" data-title="User Name"><i class="zmdi zmdi-thumb-up"></i></a>
                                                <span class="lcount">4</span>
                                                <a href="javascript:void(0)" class="pa-comment"><i class="zmdi zmdi-comment"></i></a>
                                                <span class="comment-lcount">4</span>
                                             </div>
                                          </div>
                                          <div class="comments-section panel">
                                             <div class="post-more">
                                                <a href="javascript:void(0)" class="view-morec">View more comments</a>
                                                <span class="total-comments">3 of 7</span>
                                             </div>
                                             <div class="post-comments">
                                                <div class="pcomments">
                                                   <div class="pcomment-earlier">
                                                      <div class="pcomment-holder">
                                                         <div class="pcomment main-comment">
                                                            <div class="img-holder">
                                                               <div class="profiletipholder">
                                                                  <span class="profile-tooltip">
                                                                  <img class="circle" src="images/demo-profile.jpg"/>
                                                                  </span>
                                                                  <span class="profiletooltip_content slidingpan-holder">
                                                                     <div class="profile-tip" style="display:none;">
                                                                        <div class="profile-tip-avatar">
                                                                           <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                                                           <div class="sliding-pan location-span">
                                                                              <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                                           </div>
                                                                        </div>
                                                                        <div class="profile-tip-name">
                                                                           <a href="javascript:void(0)">Markand Trivedi</a>
                                                                        </div>
                                                                        <div class="profile-tip-info">
                                                                           <div class="profiletip-icon">
                                                                              <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                                           </div>
                                                                           <div class="profiletip-icon">
                                                                              <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                                           </div>
                                                                           <div class="profiletip-icon">
                                                                              <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                                                           </div>
                                                                        </div>
                                                                     </div>
                                                                  </span>
                                                               </div>
                                                            </div>
                                                            <div class="desc-holder">
                                                               <div class="normal-mode">
                                                                  <div class="desc">
                                                                     <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                                                     <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                                                  </div>
                                                                  <div class="comment-stuff">
                                                                     <div class="more-opt">
                                                                        <span class="likeholder">
                                                                        <span class="like-tooltip">
                                                                        <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                                                           <i class="zmdi zmdi-thumb-up"></i>
                                                                        </a>
                                                                        </span>
                                                                        </span>  
                                                                        <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply">
                                                                           <i class="zmdi zmdi-mail-reply"></i>
                                                                        </a>
                                                                        <!-- Dropdown Trigger -->
                                                                        <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='drop_down100'>
                                                                        <i class="zmdi zmdi-more"></i>
                                                                        </a>
                                                                        <!-- Dropdown Structure -->
                                                                        <ul id='drop_down100' class='dropdown-content custom_dropdown'>
                                                                           <li>
                                                                              <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                                                           </li>
                                                                           <li>
                                                                              <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                                                           </li>
                                                                        </ul>
                                                                     </div>
                                                                     <div class="less-opt">
                                                                        <div class="timestamp">8h</div>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                               <div class="edit-mode">
                                                                  <div class="desc">
                                                                     <textarea class="editcomment-tt materialize-textarea" id="ec-7">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                                                     <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                         <div class="clear"></div>
                                                         <div class="comment-reply-holder comment-addreply">
                                                            <div class="addnew-comment valign-wrapper  comment-reply">
                                                               <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                                                               <div class="desc-holder">                          
                                                                  <textarea class="materialize-textarea">Write a reply...</textarea>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div class="pcomment-holder">
                                                         <div class="pcomment main-comment">
                                                            <div class="img-holder">
                                                               <div class="profiletipholder">
                                                                  <span class="profile-tooltip">
                                                                  <img class="circle" src="images/demo-profile.jpg"/>
                                                                  </span>
                                                                  <span class="profiletooltip_content slidingpan-holder">
                                                                     <div class="profile-tip" style="display:none;">
                                                                        <div class="profile-tip-avatar">
                                                                           <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                                                           <div class="sliding-pan location-span">
                                                                              <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                                           </div>
                                                                        </div>
                                                                        <div class="profile-tip-name">
                                                                           <a href="javascript:void(0)">Markand Trivedi</a>
                                                                        </div>
                                                                        <div class="profile-tip-info">
                                                                           <div class="profiletip-icon">
                                                                              <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                                           </div>
                                                                           <div class="profiletip-icon">
                                                                              <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                                           </div>
                                                                           <div class="profiletip-icon">
                                                                              <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                                                           </div>
                                                                        </div>
                                                                     </div>
                                                                  </span>
                                                               </div>
                                                            </div>
                                                            <div class="desc-holder">
                                                               <div class="normal-mode">
                                                                  <div class="desc">
                                                                     <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                                                     <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                                                  </div>
                                                                  <div class="comment-stuff">
                                                                     <div class="more-opt">
                                                                        <span class="likeholder">
                                                                        <span class="like-tooltip">
                                                                        <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                                                           <i class="zmdi zmdi-thumb-up"></i>
                                                                        </a>
                                                                        </span>
                                                                        </span>  
                                                                        <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply">
                                                                           <i class="zmdi zmdi-mail-reply"></i>
                                                                        </a>
                                                                        <!-- Dropdown Trigger -->
                                                                        <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='drop_down101'>
                                                                        <i class="zmdi zmdi-more"></i>
                                                                        </a>
                                                                        <!-- Dropdown Structure -->
                                                                        <ul id='drop_down101' class='dropdown-content custom_dropdown'>
                                                                           <li>
                                                                              <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                                                           </li>
                                                                           <li>
                                                                              <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                                                           </li>
                                                                        </ul>
                                                                     </div>
                                                                     <div class="less-opt">
                                                                        <div class="timestamp">8h</div>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                               <div class="edit-mode">
                                                                  <div class="desc">
                                                                     <textarea class="editcomment-tt materialize-textarea" id="ec-8">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                                                     <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                         <div class="clear"></div>
                                                         <div class="comment-reply-holder comment-addreply">
                                                            <div class="addnew-comment valign-wrapper  comment-reply">
                                                               <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                                                               <div class="desc-holder">                          
                                                                  <textarea class="materialize-textarea">Write a reply...</textarea>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div class="pcomment-holder">
                                                      <div class="pcomment main-comment">
                                                         <div class="img-holder">
                                                            <div class="profiletipholder">
                                                               <span class="profile-tooltip">
                                                               <img class="circle" src="images/demo-profile.jpg"/>
                                                               </span>
                                                               <span class="profiletooltip_content slidingpan-holder">
                                                                  <div class="profile-tip" style="display:none;">
                                                                     <div class="profile-tip-avatar">
                                                                        <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                                                        <div class="sliding-pan location-span">
                                                                           <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                                        </div>
                                                                     </div>
                                                                     <div class="profile-tip-name">
                                                                        <a href="javascript:void(0)">Markand Trivedi</a>
                                                                     </div>
                                                                     <div class="profile-tip-info">
                                                                        <div class="profiletip-icon">
                                                                           <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                                        </div>
                                                                        <div class="profiletip-icon">
                                                                           <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                                        </div>
                                                                        <div class="profiletip-icon">
                                                                           <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                                                        </div>
                                                                     </div>
                                                                  </div>
                                                               </span>
                                                            </div>
                                                         </div>
                                                         <div class="desc-holder">
                                                            <div class="normal-mode">
                                                               <div class="desc">
                                                                  <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                                                  <p>
                                                                     Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.
                                                                     <a href="javascript:void(0)" class="cmnt-react">
                                                                        <i class="zmdi zmdi-thumb-up"></i>
                                                                        <span class="react-count">1</span>
                                                                     </a>
                                                                  </p>
                                                               </div>
                                                               <div class="comment-stuff">
                                                                  <div class="more-opt">
                                                                     <!-- Dropdown Trigger -->
                                                                     <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='drop_down102'>
                                                                     <i class="zmdi zmdi-more"></i>
                                                                     </a>
                                                                     <!-- Dropdown Structure -->
                                                                     <ul id='drop_down102' class='dropdown-content custom_dropdown'>
                                                                        <li>
                                                                           <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                                                        </li>
                                                                        <li>
                                                                           <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                                                        </li>
                                                                     </ul>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                            <div class="normode-action">
                                                               <span class="likeholder">
                                                                  <span class="like-tooltip">
                                                                     <div class="fixed-action-btn horizontal direction-top direction-left" >
                                                                       <a href="javascript:void(0)" class="post-like">Like</a>
                                                                       <ul>
                                                                         <li>
                                                                           <a class="btn-floating red" style="opacity: 0; transform: scale(0.4) translateY(0px) translateX(40px);">
                                                                             <i class="mdi mdi-thumb-up"></i>
                                                                           </a>
                                                                         </li>
                                                                       </ul>
                                                                     </div>
                                                                  </span>
                                                               </span>  
                                                               <a href="javascript:void(0)" class="pa-reply reply-comment post-reply" title="Reply">
                                                                  Reply
                                                               </a>
                                                               <div class="post-time">8h</div>
                                                            </div>
                                                            <div class="edit-mode">
                                                               <div class="desc">
                                                                  <div class="cmntarea">
                                                                     <textarea class="editcomment-tt materialize-textarea" id="ec-9">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                                                     <ul>
                                                                        <li><a href=""><i class="mdi mdi-emoticon-happy"></i></a></li>
                                                                        <li><a href=""><i class="mdi mdi-camera"></i></a></li>
                                                                     </ul>
                                                                  </div>
                                                                  <a href="javascript:void(0)" class="editcomment-cancel editcomment-action">Update</a>
                                                                  <a href="javascript:void(0)" class="editcomment-cancel editcomment-action">Cancel</a>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div class="clear"></div>
                                                      <div class="comment-reply-holder comment-addreply">
                                                         <div class="addnew-comment valign-wrapper  comment-reply">
                                                            <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                                                            <div class="desc-holder">   
                                                               <div class="cmntarea">                       
                                                                  <textarea class="materialize-textarea">Write a reply...</textarea>
                                                                  <ul>
                                                                     <li><a href=""><i class="mdi mdi-emoticon-happy"></i></a></li>
                                                                     <li><a href=""><i class="mdi mdi-camera"></i></a></li>
                                                                  </ul>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div class="pcomment-holder">
                                                      <div class="pcomment main-comment">
                                                         <div class="img-holder">
                                                            <div class="profiletipholder">
                                                               <span class="profile-tooltip">
                                                               <img class="circle" src="images/demo-profile.jpg"/>
                                                               </span>
                                                               <span class="profiletooltip_content slidingpan-holder">
                                                                  <div class="profile-tip" style="display:none;">
                                                                     <div class="profile-tip-avatar">
                                                                        <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                                                        <div class="sliding-pan location-span">
                                                                           <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                                        </div>
                                                                     </div>
                                                                     <div class="profile-tip-name">
                                                                        <a href="javascript:void(0)">Markand Trivedi</a>
                                                                     </div>
                                                                     <div class="profile-tip-info">
                                                                        <div class="profiletip-icon">
                                                                           <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                                        </div>
                                                                        <div class="profiletip-icon">
                                                                           <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                                        </div>
                                                                        <div class="profiletip-icon">
                                                                           <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class="mdi mdi-eye"></i></span></a>
                                                                        </div>
                                                                     </div>
                                                                  </div>
                                                               </span>
                                                            </div>
                                                         </div>
                                                         <div class="desc-holder">
                                                            <div class="normal-mode">
                                                               <div class="desc">
                                                                  <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                                                  <p class="shorten">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.<a a="" href="javascript:void(0)" class="overlay" onclick="explandReadMore(this)" style="position: absolute;top: 0;bottom: -6px;left: 0;right: 0;"><span class="readlink">Read More</span></a>
                                                                     <a href="javascript:void(0)" class="cmnt-react">
                                                                        <i class="zmdi zmdi-thumb-up"></i>
                                                                        <span class="react-count">1</span>
                                                                     </a>
                                                                  </p>
                                                               </div>
                                                               <div class="comment-stuff">
                                                                  <div class="more-opt">
                                                                     <span class="likeholder">
                                                                     <span class="like-tooltip">
                                                                     <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>" title="Like">
                                                                        <i class="zmdi zmdi-thumb-up"></i>
                                                                     </a>
                                                                     </span>
                                                                     </span>  
                                                                     <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply">
                                                                        <i class="zmdi zmdi-mail-reply"></i>
                                                                     </a>
                                                                     <!-- Dropdown Trigger -->
                                                                     <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='drop_down104'>
                                                                     <i class="zmdi zmdi-more"></i>
                                                                     </a>
                                                                     <!-- Dropdown Structure -->
                                                                     <ul id='drop_down104' class='dropdown-content custom_dropdown'>
                                                                        <li>
                                                                           <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                                                        </li>
                                                                        <li>
                                                                           <a class="delete-comment" href="javascript:void(0)">Delete</a>
                                                                        </li>
                                                                     </ul>
                                                                  </div>
                                                                  <div class="less-opt">
                                                                     <div class="timestamp">8h</div>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                            <div class="edit-mode">
                                                               <div class="desc">
                                                                  <textarea class="editcomment-tt materialize-textarea" id="ec-10">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                                                  <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close mdi-20px"></i></a>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div class="clear"></div>
                                                      <div class="comment-reply-holder comment-addreply">
                                                         <div class="addnew-comment valign-wrapper  comment-reply">
                                                            <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                                                            <div class="desc-holder">                          
                                                               <textarea class="materialize-textarea">Write a reply...</textarea>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="addnew-comment valign-wrapper  ">
                                                   <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                                                   <div class="desc-holder"> 
                                                      <div class="cmntarea">                         
                                                         <textarea class="materialize-textarea">Write a reply...</textarea>
                                                         <ul>
                                                            <li><a href=""><i class="mdi mdi-emoticon-happy"></i></a></li>
                                                            <li><a href=""><i class="mdi mdi-camera"></i></a></li>
                                                         </ul>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div> 
                              <div class="new-post-mobile clear disscu_show">
                                 <a href="javascript:void(0)" class="popup-window compose_discus" ><i class="mdi mdi-pencil"></i></a>
                              </div>
                           </div> 
                        </div>
                     </div>
                  </div>
               </div>
               <?php include('common/gen_wall_col.php'); ?>
            </div>
         </div>
         </div>
         </div>
         </div>

         </div>

         
  
         <div id="compose_mapmodal" class="modal map_modal compose_inner_modal modalxii_level1 map_modalUniq">
         <?php include('common/map_modal.php'); ?>
         </div>
         <?php include('common/addperson_popup.php'); ?>
         <?php include('common/add_photo_popup.php'); ?>
         <!-- Add Photo-->
         <!-- compose Comment modal -->
         <div id="comment_modal_xs" class="modal tbpost_modal custom_modal split-page main_modal cust-pop dicrease-popup-compose compose_Comment_Action">
         <div class="modal_content_container">
         <div class="modal_content_child modal-content">
         <div class="popup-title ">
            <button class="hidden_close_span close_span waves-effect">
            <i class="mdi mdi-close mdi-20px compose_discard_popup"></i>
            </button>           
            <h3>All Comments</h3>
            <a type="button" class="item_done crop_done hidden_close_span waves-effect custom_close" href="javascript:void(0)">Done</a>
            <a type="button" class="item_done crop_done comment-close custom_close waves-effect" href="javascript:void(0)"><i class="mdi mdi-close"></i></a>
         </div>
         <div class="custom_modal_content modal_content" id="createpopup">
            <div class="comment-box-tab profile-tab">
               <div class="comment-poup-box detail-box">
                  <div class="content-holder main-holder">
                     <div class="summery">
                        <div class="dsection bborder expandable-holder expanded">
                           <div class="form-area expandable-area post-holder">
                              <div class="post-more">
                                 <a href="javascript:void(0)" class="view-morec">View more comments</a>
                                 <span class="total-comments">3 of 7</span>
                              </div>
                              <div class="post-comments">
                                 <div class="pcomments">
                                    <div class="pcomment-earlier">
                                       <div class="pcomment-holder">
                                          <div class="pcomment main-comment">
                                             <div class="img-holder">
                                                <div class="profiletipholder">
                                                   <span class="profile-tooltip">
                                                   <img class="circle" src="images/demo-profile.jpg"/>
                                                   </span>
                                                   <span class="profiletooltip_content slidingpan-holder">
                                                      <div class="profile-tip dis-none">
                                                         <div class="profile-tip-avatar">
                                                            <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                                 
                                                            <div class="sliding-pan location-span">
                                                               <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                            </div>
                                                         </div>
                                                         <div class="profile-tip-name">
                                                            <a href="javascript:void(0)">Adel Hasanat</a>
                                                         </div>
                                                         <div class="profile-tip-info">
                                                            <div class="profiletip-icon">
                                                               <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                            </div>
                                                            <div class="profiletip-icon">
                                                               <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                            </div>
                                                            <div class="profiletip-icon">
                                                               <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class=”mdi mdi-eye”></i></span></a>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </span>
                                                </div>
                                             </div>
                                             <div class="desc-holder">
                                                <div class="normal-mode">
                                                   <div class="desc">
                                                      <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                                      <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                                   </div>
                                                   <div class="comment-stuff">
                                                      <div class="more-opt">
                                                         <span class="likeholder">
                                                         <span class="like-tooltip">
                                                         <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>">
                                                         <i class="zmdi zmdi-thumb-up"></i>
                                                         </a>
                                                         </span>
                                                         </span>
                                                         <a href="javascript:void(0)" class="pa-reply reply-comment"><span>icon</span></a>
                                                         <a class="dropdown-button more_btn" href="javascript:void(0);" data-activates="comment_ecit32">                               <i class="zmdi zmdi-hc-2x zmdi-more-vert"></i>
                                                         </a>
                                                         <ul id="comment_ecit32" class="dropdown-content custom_dropdown">
                                                            <li><a class="edit-comment" href="javascript:void(0)">Edit</a></li>
                                                            <li><a class="delete-comment" href="javascript:void(0)">Delete</a></li>
                                                         </ul>
                                                      </div>
                                                      <div class="less-opt">
                                                         <div class="timestamp">8h</div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="edit-mode">
                                                   <div class="desc">
                                                      <textarea class="editcomment-tt materialize-textarea mb0 md_textarea descinput" id="ec-1">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                                      <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close"></i></a>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="clear"></div>
                                          <div class="comment-reply-holder comment-addreply">
                                             <div class="addnew-comment valign-wrapper comment-reply">
                                                <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                                                <div class="desc-holder">                                   
                                                   <textarea class="materialize-textarea mb0 md_textarea descinput">Write a reply...</textarea>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="pcomment-holder">
                                          <div class="pcomment main-comment">
                                             <div class="img-holder">
                                                <div class="profiletipholder">
                                                   <span class="profile-tooltip">
                                                   <img class="circle" src="images/demo-profile.jpg"/>
                                                   </span>
                                                   <span class="profiletooltip_content slidingpan-holder">
                                                      <div class="profile-tip dis-none">
                                                         <div class="profile-tip-avatar">
                                                            <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                                 
                                                            <div class="sliding-pan location-span">
                                                               <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                            </div>
                                                         </div>
                                                         <div class="profile-tip-name">
                                                            <a href="javascript:void(0)">Adel Hasanat</a>
                                                         </div>
                                                         <div class="profile-tip-info">
                                                            <div class="profiletip-icon">
                                                               <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                            </div>
                                                            <div class="profiletip-icon">
                                                               <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                            </div>
                                                            <div class="profiletip-icon">
                                                               <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class=”mdi mdi-eye”></i></span></a>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </span>
                                                </div>
                                             </div>
                                             <div class="desc-holder">
                                                <div class="normal-mode">
                                                   <div class="desc">
                                                      <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                                      <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                                   </div>
                                                   <div class="comment-stuff">
                                                      <div class="more-opt">
                                                         <span class="likeholder">
                                                         <span class="like-tooltip">
                                                         <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>">
                                                         <i class="zmdi zmdi-thumb-up"></i>
                                                         </a>
                                                         </span>
                                                         </span>    
                                                         <a href="javascript:void(0)" class="pa-reply reply-comment"><span>icon</span></a>
                                                         <a class="dropdown-button more_btn" href="javascript:void(0);" data-activates="comment_ecit33">                               <i class="zmdi zmdi-hc-2x zmdi-more-vert"></i>
                                                         </a>
                                                         <ul id="comment_ecit33" class="dropdown-content custom_dropdown">
                                                            <li><a class="edit-comment" href="javascript:void(0)">Edit</a></li>
                                                            <li><a class="delete-comment" href="javascript:void(0)">Delete</a></li>
                                                         </ul>
                                                      </div>
                                                      <div class="less-opt">
                                                         <div class="timestamp">8h</div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="edit-mode">
                                                   <div class="desc">
                                                      <textarea class="editcomment-tt materialize-textarea mb0 md_textarea descinput" id="ec-2">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                                      <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close"></i></a>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="clear"></div>
                                          <div class="comment-reply-holder comment-addreply">
                                             <div class="addnew-comment valign-wrapper comment-reply">
                                                <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                                                <div class="desc-holder">                                   
                                                   <textarea class="materialize-textarea mb0 md_textarea descinput">Write a reply...</textarea>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="pcomment-holder has-comments">
                                       <div class="pcomment main-comment">
                                          <div class="img-holder">
                                             <div class="profiletipholder">
                                                <span class="profile-tooltip">
                                                <img class="circle" src="images/demo-profile.jpg"/>
                                                </span>
                                                <span class="profiletooltip_content slidingpan-holder">
                                                   <div class="profile-tip dis-none">
                                                      <div class="profile-tip-avatar">
                                                         <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                                    
                                                         <div class="sliding-pan location-span">
                                                            <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                         </div>
                                                      </div>
                                                      <div class="profile-tip-name">
                                                         <a href="javascript:void(0)">Adel Hasanat</a>
                                                      </div>
                                                      <div class="profile-tip-info">
                                                         <div class="profiletip-icon">
                                                            <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                         </div>
                                                         <div class="profiletip-icon">
                                                            <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                         </div>
                                                         <div class="profiletip-icon">
                                                            <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class=”mdi mdi-eye”></i></span></a>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </span>
                                             </div>
                                          </div>
                                          <div class="desc-holder">
                                             <div class="normal-mode">
                                                <div class="desc">
                                                   <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                                   <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                                </div>
                                                <div class="comment-stuff">
                                                   <div class="more-opt">
                                                      <span class="likeholder">
                                                      <span class="like-tooltip">
                                                      <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>">
                                                      <i class="zmdi zmdi-thumb-up"></i>
                                                      </a>
                                                      </span>
                                                      </span>
                                                      <a href="javascript:void(0)" class="pa-reply reply-comment"><span>icon</span></a>
                                                      <a class="dropdown-button more_btn" href="javascript:void(0);" data-activates="comment_ecit34" >                              <i class="zmdi zmdi-hc-2x zmdi-more-vert"></i>
                                                      </a>
                                                      <ul id="comment_ecit34" class="dropdown-content custom_dropdown">
                                                         <li><a class="edit-comment" href="javascript:void(0)">Edit</a></li>
                                                         <li><a class="delete-comment" href="javascript:void(0)">Delete</a></li>
                                                      </ul>
                                                   </div>
                                                   <div class="less-opt">
                                                      <div class="timestamp">8h</div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="edit-mode">
                                                <div class="desc">
                                                   <textarea class="editcomment-tt materialize-textarea mb0 md_textarea descinput" id="ec-3">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                                   <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close"></i></a>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="clear"></div>
                                       <div class="comment-reply-holder">
                                          <div class="comments-reply-summery">
                                             <a href="javascript:void(0)" onclick="openReplies(this)">
                                             <i class="mdi mdi-share"></i>
                                             2 Replies                                                  
                                             </a>
                                             <i class="mdi mdi-bullseye dot-i"></i>
                                             Just Now
                                          </div>
                                          <div class="comments-reply-details">
                                             <div class="pcomment comment-reply">
                                                <div class="img-holder">
                                                   <div class="profiletipholder">
                                                      <span class="profile-tooltip">
                                                      <img class="circle" src="images/demo-profile.jpg"/>
                                                      </span>
                                                      <span class="profiletooltip_content slidingpan-holder">
                                                         <div class="profile-tip dis-none">
                                                            <div class="profile-tip-avatar">
                                                               <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                                  
                                                               <div class="sliding-pan location-span">
                                                                  <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                               </div>
                                                            </div>
                                                            <div class="profile-tip-name">
                                                               <a href="javascript:void(0)">Adel Hasanat</a>
                                                            </div>
                                                            <div class="profile-tip-info">
                                                               <div class="profiletip-icon">
                                                                  <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                               </div>
                                                               <div class="profiletip-icon">
                                                                  <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                               </div>
                                                               <div class="profiletip-icon">
                                                                  <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class=”mdi mdi-eye”></i></span></a>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </span>
                                                   </div>
                                                </div>
                                                <div class="desc-holder">
                                                   <div class="normal-mode">
                                                      <div class="desc">
                                                         <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                                         <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                                      </div>
                                                      <div class="comment-stuff">
                                                         <div class="more-opt">
                                                            <span class="likeholder">
                                                            <span class="like-tooltip">
                                                            <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>">
                                                            <i class="zmdi zmdi-thumb-up"></i>
                                                            </a>
                                                            </span>
                                                            </span>
                                                            <a href="javascript:void(0)" class="pa-reply reply-comment"><span>icon</span></a>
                                                            <a class="dropdown-button more_btn" href="javascript:void(0);" data-activates="comment_ecit35">                                <i class="zmdi zmdi-hc-2x zmdi-more-vert"></i>
                                                            </a>
                                                            <ul id="comment_ecit35" class="dropdown-content custom_dropdown">
                                                               <li><a class="edit-comment" href="javascript:void(0)">Edit</a></li>
                                                               <li><a class="delete-comment" href="javascript:void(0)">Delete</a></li>
                                                            </ul>
                                                         </div>
                                                         <div class="less-opt">
                                                            <div class="timestamp">8h</div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div class="edit-mode">
                                                      <div class="desc">
                                                         <textarea class="editcomment-tt materialize-textarea mb0 md_textarea descinput" id="ec-4">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                                         <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close"></i></a>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="pcomment comment-reply">
                                                <div class="img-holder">
                                                   <div class="profiletipholder">
                                                      <span class="profile-tooltip">
                                                      <img class="circle" src="images/demo-profile.jpg"/>
                                                      </span>
                                                      <span class="profiletooltip_content slidingpan-holder">
                                                         <div class="profile-tip dis-none">
                                                            <div class="profile-tip-avatar">
                                                               <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                                  
                                                               <div class="sliding-pan location-span">
                                                                  <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                               </div>
                                                            </div>
                                                            <div class="profile-tip-name">
                                                               <a href="javascript:void(0)">Adel Hasanat</a>
                                                            </div>
                                                            <div class="profile-tip-info">
                                                               <div class="profiletip-icon">
                                                                  <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                               </div>
                                                               <div class="profiletip-icon">
                                                                  <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                               </div>
                                                               <div class="profiletip-icon">
                                                                  <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class=”mdi mdi-eye”></i></span></a>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </span>
                                                   </div>
                                                </div>
                                                <div class="desc-holder">
                                                   <div class="normal-mode">
                                                      <div class="desc">
                                                         <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                                         <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit...</p>
                                                      </div>
                                                      <div class="comment-stuff">
                                                         <div class="more-opt">
                                                            <span class="likeholder">
                                                            <span class="like-tooltip">
                                                            <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>">
                                                            <i class="zmdi zmdi-thumb-up"></i>
                                                            </a>
                                                            </span>
                                                            </span> 
                                                            <a href="javascript:void(0)" class="pa-reply reply-comment"><span>icon</span></a>
                                                            <a class="dropdown-button more_btn" href="javascript:void(0);" data-activates="comment_ecit37">                                <i class="zmdi zmdi-hc-2x zmdi-more-vert"></i>
                                                            </a>
                                                            <ul id="comment_ecit37" class="dropdown-content custom_dropdown">
                                                               <li><a class="edit-comment" href="javascript:void(0)">Edit</a></li>
                                                               <li><a class="delete-comment" href="javascript:void(0)">Delete</a></li>
                                                            </ul>
                                                         </div>
                                                         <div class="less-opt">
                                                            <div class="timestamp">8h</div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div class="edit-mode">
                                                      <div class="desc">
                                                         <textarea class="editcomment-tt materialize-textarea mb0 md_textarea descinput" id="ec-5">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                                         <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close"></i></a>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="comment-reply-holder comment-addreply">
                                          <div class="addnew-comment valign-wrapper comment-reply">
                                             <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                                             <div class="desc-holder">                                  
                                                <textarea class="materialize-textarea mb0 md_textarea descinput">Write a reply...</textarea>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="pcomment-holder">
                                       <div class="pcomment main-comment">
                                          <div class="img-holder">
                                             <div class="profiletipholder">
                                                <span class="profile-tooltip">
                                                <img class="circle" src="images/demo-profile.jpg"/>
                                                </span>
                                                <span class="profiletooltip_content slidingpan-holder">
                                                   <div class="profile-tip dis-none">
                                                      <div class="profile-tip-avatar">
                                                         <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                                    
                                                         <div class="sliding-pan location-span">
                                                            <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                         </div>
                                                      </div>
                                                      <div class="profile-tip-name">
                                                         <a href="javascript:void(0)">Adel Hasanat</a>
                                                      </div>
                                                      <div class="profile-tip-info">
                                                         <div class="profiletip-icon">
                                                            <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                         </div>
                                                         <div class="profiletip-icon">
                                                            <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                         </div>
                                                         <div class="profiletip-icon">
                                                            <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class=”mdi mdi-eye”></i></span></a>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </span>
                                             </div>
                                          </div>
                                          <div class="desc-holder">
                                             <div class="normal-mode">
                                                <div class="desc">
                                                   <a href="javascript:void(0)" class="userlink">Adel Hasanat</a>
                                                   <p class="shorten">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.<a a="" href="javascript:void(0)" class="overlay" onclick="explandReadMore(this)"><span class="readlink">Read More</span></a></p>
                                                </div>
                                                <div class="comment-stuff">
                                                   <div class="more-opt">
                                                      <span class="likeholder">
                                                      <span class="like-tooltip">
                                                      <a href="javascript:void(0)" class="pa-like tooltipstered liveliketooltip" data-title="User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>User Name<br/>">
                                                      <i class="zmdi zmdi-thumb-up"></i>
                                                      </a>
                                                      </span>
                                                      </span>   
                                                      <a href="javascript:void(0)" class="pa-reply reply-comment"><span>icon</span></a>
                                                      <a class="dropdown-button more_btn" href="javascript:void(0);" data-activates="comment_ecit36" >                              <i class="zmdi zmdi-hc-2x zmdi-more-vert"></i>
                                                      </a>
                                                      <ul id="comment_ecit36" class="dropdown-content custom_dropdown">
                                                         <li><a class="edit-comment" href="javascript:void(0)">Edit</a></li>
                                                         <li><a class="delete-comment" href="javascript:void(0)">Delete</a></li>
                                                      </ul>
                                                   </div>
                                                   <div class="less-opt">
                                                      <div class="timestamp">8h</div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="edit-mode">
                                                <div class="desc">
                                                   <textarea class="editcomment-tt materialize-textarea mb0 md_textarea descinput" id="ec-6">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</textarea>
                                                   <a href="javascript:void(0)" class="editcomment-cancel"><i class="mdi mdi-close"></i></a>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="clear"></div>
                                       <div class="comment-reply-holder comment-addreply">
                                          <div class="addnew-comment valign-wrapper comment-reply">
                                             <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
                                             <div class="desc-holder">                                  
                                                <textarea class="materialize-textarea mb0 md_textarea descinput">Write a reply...</textarea>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
</div>
<div class="modal-footer">
<div class="new-post active">
<div class="post-bcontent">
   <div class="addnew-comment valign-wrapper">
      <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
      <div class="desc-holder">                                    
         <textarea class="materialize-textarea mb0 md_textarea descinput" id="comment_txt_1">Write a reply...</textarea>
      </div>
   </div>
</div>
</div>
</div>
<div class="additem_modal_footer modal-footer">
<div class="">
<div class="btn-holder">
   <div class="addnew-comment valign-wrapper">
      <div class="img-holder"><a href="javascript:void(0)"><img class="circle" src="images/demo-profile.jpg"/></a></div>
      <div class="desc-holder">                                    
         <textarea class="materialize-textarea mb0 md_textarea descinput" id="comment_txt_1">Write a reply...</textarea>
      </div>
   </div>
</div>
</div>
</div>
      <div class="new-post-mobile clear">
         <a class="popup-window composetoolboxAction" href="javascript:void(0)"><i class="mdi mdi-pencil"></i></a>
      </div>
   </div>
</div>
   
</div>   
<!--map modal-->
<div id="compose_mapmodal" class="modal map_modal compose_inner_modal modalxii_level1">
   <?php include('common/map_modal.php'); ?>
</div>

<link rel="stylesheet" type="text/css" href="css/owl.carousel.css">
<script type="text/javascript" src="js/owl.carousel.js"></script>
<script type="text/javascript" src="js/wall.js"></script>
<script type="text/javascript">
$(document).ready(function(){ 
$w = $(window).width();
if ( $w > 739) {      
$(".places-tabs .sub-tabs li a").click(function(){
   $("body").removeClass("remove_scroller");
}); 
$(".tabs.icon-menu.tabsnew li a").click(function(){
   $("body").removeClass("remove_scroller");
}); 
$(".mbl-tabnav").click(function(){
   $("body").removeClass("remove_scroller");
}); 
$(".clicable.viewall-link").click(function(){
   $("body").removeClass("remove_scroller");
}); 
} else {
$(".places-tabs .sub-tabs li a").click(function(){
   $("body").addClass("remove_scroller");
}); 
$(".clicable.viewall-link").click(function(){
   $("body").addClass("remove_scroller");
});         
$(".tabs.icon-menu.tabsnew li a").click(function(){
   $("body").addClass("remove_scroller");
}); 
$(".mbl-tabnav").click(function(){
   $("body").removeClass("remove_scroller");
});
}

$(".header-icon-tabs .tabsnew .tab a").click(function(){
$(".bottom_tabs").hide();
});

$(".places-tabs .tab a").click(function(){
$(".top_tabs").hide();
});

// footer work for places home page only
$('.footer-section').css('left', '0');
$w = $(window).width();
if($w <= 768) {
$('.main-footer').css({
   'width': '100%',
   'left': '0'
});
} else {
var $_I = $('.places-content.places-all').width();
var $__I = $('.places-content.places-all').find('.container').width();

var $half = parseInt($_I) - parseInt($__I);
$half = parseInt($half) / 2;

$('.main-footer').css({
   'width': $_I+'px',
   'left': '-'+$half+'px'
});
}
});

$(window).resize(function() {
// footer work for places home page only
if($('#places-all').hasClass('active')) {
$('.footer-section').css('left', '0');
$w = $(window).width();
if($w <= 768) {
   $('.main-footer').css({
      'width': '100%',
      'left': '0'
   });
} else {
   var $_I = $('.places-content.places-all').width();
   var $__I = $('.places-content.places-all').find('.container').width();

   var $half = parseInt($_I) - parseInt($__I);
   $half = parseInt($half) / 2;

   $('.main-footer').css({
      'width': $_I+'px',
      'left': '-'+$half+'px'
   });
}
}
});

$(document).on('click', '.tablist .tab a', function(e) {
$href = $(this).attr('href');
$href = $href.replace('#', '');

$('.places-content').removeClass().addClass('places-content '+$href);
   $this = $(this);
});
</script>

<?php include('common/discard_popup.php'); ?>
<?php include('common/upload_gallery_popup.php'); ?>
<?php include('common/privacymodal.php'); ?>
<?php include('common/share_popup.php'); ?>
<?php include('common/custom_modal.php'); ?>
<?php include('common/addcategories_popup.php'); ?>
<?php include('common/compose_post_popup.php'); ?>

<?php include("script.php"); ?>