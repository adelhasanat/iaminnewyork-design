<?php include("header.php"); ?>
<?php include("common/menu.php"); ?>
<div class="notice-holder fixed pulled-down">
   <div class="settings-notice">
      <span class="success-note"></span>
      <span class="error-note"></span>
      <span class="info-note"></span>
   </div>
</div>
<div class="floating-icon">
   <div class="scrollup-btnbox anim-side btnbox scrollup-float">
      <div class="scrollup-button float-icon"><span class="icon-holder ispan"><i class="mdi mdi-arrow-up-bold-circle"></i>
</span></div>
   </div>
</div>
<div class="clear"></div>
<div class="container page_container">
   <?php include("common/leftmenu.php"); ?>
   <div class="fixed-layout ipad-mfix">
      <div class="main-content main-page places-page localdriver-page pb-0 m-t-50 with-lmenu general-page generaldetails-page split-page m-hide">
         <div class="combined-column mx-auto float-none">
            <div class="content-box">
               <div class="cbox-desc">
                  <div class="tab-content view-holder">
                     <div class="local-heading">
                        <div class="row mx-0 valign-wrapper">
                           <div class="left">
                              <h3 class="heading-inner">LOCAL DRIVERS <span class="lt">(20)</span></h3>
                              <p class="para-inner">Hire a local driver and enjoy your tour</p>
                           </div>
                           <div class="right ml-auto">
                              <a href="javascript:void(0)" class="createDriverAction">
                                 <span class="hidden-sm">BECOME</span> DRIVER
                              </a>
                           </div>
                        </div>
                     </div>
                     <div class="page-details travelbuddy-details">
                        <div class="row">
                           <div class="travelbuddy-summery gdetails-summery">
                              <div class="main-info">
                                 <a href="javascript:void(0)" class="expand-link mbl-filter-icon main-icon gray-text-555" onclick="mbl_mng_drop_searcharea(this,'searcharea1')"><i class="mdi mdi-tune mdi-20px"></i></a>
                                 <div class="search-area side-area main-search" id="searcharea1">
                                    <a href="javascript:void(0)" class="expand-link" onclick="mng_drop_searcharea(this)">Advanced Search</a>
                                    <div class="expandable-area">
                                       <a href="javascript:void(0)" class="closearea" onclick="mng_drop_searcharea(this)">
                                       <img src="images/cross-icon.png"/><span>DONE</span>
                                       </a>
                                       <div class="tab-content">
                                          <div class="tab-pane fade active in" id="find-traveller">
                                             <div class="srow">
                                                <h6>Refine search</h6>
                                             </div>
                                             <div class="srow">
                                                <h6>Guide Info</h6>
                                                <ul>
                                                   <li>
                                                      <input type="checkbox" id="test1" />
                                                      <label for="test1">Check all</label>
                                                   </li>
                                                   <li>
                                                      <input type="checkbox" id="test2" />
                                                      <label for="test2">Licensed</label>
                                                   </li>
                                                   <li>
                                                      <input type="checkbox" id="test3" />
                                                      <label for="test3">Verified</label>
                                                   </li>
                                                   <li>
                                                      <input type="checkbox" id="test4" />
                                                      <label for="test4">Has reviews</label>
                                                   </li> 
                                                </ul> 
                                             </div>
                                             <div class="srow">
                                                <h6>Language spoken</h6>
                                                <div class="input-field dropdown782">
                                                   <select id="chooseLanguage" class="languagedrp" data-selectore="languagedrp" data-fill="n" data-action="language" multiple>
                                                      <option value="" disabled selected>Choose language</option>
                                                      <?php
                                                          $language = array("English" => "English", "Afar" => "Afar", "Abkhazian" => "Abkhazian", "Afrikaans" => "Afrikaans", "Amharic" => "Amharic", "Arabic" => "Arabic", "Assamese" => "Assamese", "Aymara" => "Aymara", "Azerbaijani" => "Azerbaijani", "Bashkir" => "Bashkir", "Belarusian" => "Belarusian", "Bulgarian" => "Bulgarian", "Bihari" => "Bihari", "Bislama" => "Bislama", "Bengali/Bangla" => "Bengali/Bangla", "Tibetan" => "Tibetan", "Breton" => "Breton", "Catalan" => "Catalan", "Corsican" => "Corsican", "Czech" => "Czech", "Welsh" => "Welsh", "Danish" => "Danish", "German" => "German", "Bhutani" => "Bhutani", "Greek" => "Greek", "Esperanto" => "Esperanto", "Spanish" => "Spanish", "Estonian" => "Estonian", "Basque" => "Basque", "Persian" => "Persian", "Finnish" => "Finnish", "Fiji" => "Fiji", "Faeroese" => "Faeroese", "French" => "French", "Frisian" => "Frisian", "Irish" => "Irish", "Scots/Gaelic" => "Scots/Gaelic", "Galician" => "Galician", "Guarani" => "Guarani", "Gujarati" => "Gujarati", "Hausa" => "Hausa", "Hindi" => "Hindi", "Croatian" => "Croatian", "Hungarian" => "Hungarian", "Armenian" => "Armenian", "Interlingua" => "Interlingua", "Interlingue" => "Interlingue", "Inupiak" => "Inupiak", "Indonesian" => "Indonesian", "Icelandic" => "Icelandic", "Italian" => "Italian", "Hebrew" => "Hebrew", "Japanese" => "Japanese", "Yiddish" => "Yiddish", "Javanese" => "Javanese", "Georgian" => "Georgian", "Kazakh" => "Kazakh", "Greenlandic" => "Greenlandic", "Cambodian" => "Cambodian", "Kannada" => "Kannada", "Korean" => "Korean", "Kashmiri" => "Kashmiri", "Kurdish" => "Kurdish", "Kirghiz" => "Kirghiz", "Latin" => "Latin", "Lingala" => "Lingala", "Laothian" => "Laothian", "Lithuanian" => "Lithuanian", "Latvian/Lettis" => "Latvian/Lettis", "Malagasy" => "Malagasy", "Maori" => "Maori", "Macedonian" => "Macedonian", "Malayalam" => "Malayalam", "Mongolian" => "Mongolian", "Moldavian" => "Moldavian", "Marathi" => "Marathi", "Malay" => "Malay", "Maltese" => "Maltese", "Burmese" => "Burmese", "Nauru" => "Nauru", "Nepali" => "Nepali", "Dutch" => "Dutch", "Norwegian" => "Norwegian", "Occitan" => "Occitan", "Afan" => "Afan", "Punjabi" => "Punjabi", "Polish" => "Polish", "Pashto/Pushto" => "Pashto/Pushto", "Portuguese" => "Portuguese", "Quechua" => "Quechua", "Rhaeto-Romance" => "Rhaeto-Romance", "Kirundi" => "Kirundi", "Romanian" => "Romanian", "Russian" => "Russian", "Kinyarwanda" => "Kinyarwanda", "Sanskrit" => "Sanskrit", "Sindhi" => "Sindhi", "Sangro" => "Sangro", "Serbo-Croatian" => "Serbo-Croatian", "Singhalese" => "Singhalese", "Slovak" => "Slovak", "Slovenian" => "Slovenian", "Samoan" => "Samoan", "Shona" => "Shona", "Somali" => "Somali", "Albanian" => "Albanian", "Serbian" => "Serbian", "Siswati" => "Siswati", "Sesotho" => "Sesotho", "Sundanese" => "Sundanese", "Swedish" => "Swedish", "Swahili" => "Swahili", "Tamil" => "Tamil", "Telugu" => "Telugu", "Tajik" => "Tajik", "Thai" => "Thai", "Tigrinya" => "Tigrinya", "Turkmen" => "Turkmen", "Tagalog" => "Tagalog", "Setswana" => "Setswana", "Tonga" => "Tonga", "Turkish" => "Turkish", "Tsonga" => "Tsonga", "Tatar" => "Tatar", "Twi" => "Twi", "Ukrainian" => "Ukrainian", "Urdu" => "Urdu", "Uzbek" => "Uzbek", "Vietnamese" => "Vietnamese", "Volapuk" => "Volapuk", "Wolof" => "Wolof", "Xhosa" => "Xhosa", "Yoruba" => "Yoruba", "Chinese" => "Chinese", "Zulu" => "Zulu");
                                                          foreach ($language as $s9032n) {
                                                             echo "<option value=".$s9032n.">$s9032n</option>";
                                                          }
                                                       ?>
                                                   </select>
                                                </div>
                                             </div>
                                             <div class="srow">
                                                <h6>Age</h6>
                                                <div class="range-slider">
                                                   <div id="age-slider"></div>
                                                </div>
                                             </div>
                                             <div class="srow">
                                                <h6>Gender</h6>
                                                <ul>
                                                   <li>
                                                      <input type="checkbox" id="male_check" />
                                                      <label for="male_check">Male</label>
                                                   </li>
                                                   <li>
                                                      <input type="checkbox" id="female_check" />
                                                      <label for="female_check">Female</label>
                                                   </li>
                                                   <li>
                                                      <input type="checkbox" id="several_check" />
                                                      <label for="several_check">Several people</label>
                                                   </li>
                                                </ul>
                                             </div>
                                             <div class="btn-holder">
                                                <a href="javascript:void(0)" class="btn-custom waves-effect">Cancel</a>
                                                <a href="javascript:void(0)" class="btn-custom waves-effect">Search</a>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <a href="javascript:void(0)" class="expand-link mbl-filter-icon offer-profile-icon" onclick="mbl_mng_drop_searcharea(this,'searcharea2')"><img src="images/profile-filter.png"/></a>
                                 <div class="search-area side-area offer-profile" id="searcharea2">
                                    <a href="javascript:void(0)" class="expand-link" onclick="mng_drop_searcharea(this)"><i class="mdi mdi-menu-right"></i>User's Profile</a>
                                    <div class="expandable-area">
                                       <a href="javascript:void(0)" class="closearea" onclick="mng_drop_searcharea(this)">
                                       <i class="mdi mdi-close"></i>
                                       </a>
                                       <div class="user-profile">
                                          <div class="desc-holder">
                                             <div class="img-holder"><img src="images/offer-propic.jpg"/></div>
                                             <div class="content-area">
                                                <h4>Linka_U</h4>
                                                <div class="row-sec">
                                                   <div class="inforow">
                                                      <div class="icon-holder"><i class="zmdi zmdi-pin"></i></div>
                                                      Lives in <span>Poland</span>
                                                   </div>
                                                </div>
                                                <div class="row-sec">
                                                   <div class="inforow">
                                                      <div class="icon-holder"><i class="mdi mdi mdi-gender-male-female"></i></div>
                                                      <span>34 / Female</span>
                                                   </div>
                                                   <div class="inforow">
                                                      <div class="icon-holder"><i class="mdi mdi-comment"></i></div>
                                                      Speaks <span>English, French</span>
                                                   </div>
                                                   <div class="inforow">
                                                      <div class="icon-holder"><i class="mdi mdi-briefcase"></i></div>
                                                      Works as <span>Graphics Designer</span>
                                                   </div>
                                                   <div class="inforow">
                                                      <div class="icon-holder"><i class="mdi mdi-format-quote-open"></i></div>
                                                      Lastly logged in <span>Yesterday</span>
                                                   </div>
                                                </div>
                                                <div class="inforow">
                                                   <div class="icon-holder"><i class=”mdi mdi-account-group”></i></i></div>
                                                   Connections <span>56</span>
                                                </div>
                                                <div class="inforow">
                                                   <div class="icon-holder"><i class="mdi mdi-bookmark"></i></div>
                                                   References <span>20</span>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="sub-info">
                                             <div class="sub-title">
                                                <h5>Trip Plan</h5>
                                             </div>
                                             <div class="content-area">
                                                <div class="inforow">
                                                   <div class="icon-holder"><i class="zmdi zmdi-pin"></i></div>
                                                   <label>City</label>
                                                   <p>Salt Lake City, Utah, United States</p>
                                                </div>
                                                <div class="inforow">
                                                   <div class="icon-holder"><i class="mdi mdi-airplane"></i></div>
                                                   <label>Arrival Date</label>
                                                   <p>4 Nov 2016</p>
                                                </div>
                                                <div class="inforow">
                                                   <div class="icon-holder"><i class="mdi mdi-reply"></i></div>
                                                   <label>Departure Date</label>
                                                   <p>10 Nov 2016</p>
                                                </div>
                                                <div class="inforow">
                                                   <div class="icon-holder"><i class="mdi mdi-account"></i></div>
                                                   <label>Total Travellers</label>
                                                   <p>2</p>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="sub-info">
                                             <div class="sub-title">
                                                <h5>Host Services</h5>
                                             </div>
                                             <div class="content-area hostservice">
                                                <div class="inforow">
                                                   <label>Host Availability</label>
                                                   <p>Accepting Guests</p>
                                                </div>
                                                <div class="inforow">
                                                   <label>Host Amenities</label>
                                                   <ul>
                                                      <li><i class="mdi mdi-coffee"></i>Hang Around</li>
                                                      <li><i class="mdi mdi-binoculars"></i>Dininig</li>
                                                      <li><i class="mdi mdi-bus"></i>Site Touring</li>
                                                   </ul>
                                                </div>
                                                <div class="inforow">
                                                   <label>Host Message</label>
                                                   <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="sideboxes">
                                 <?php include('common/recently_joined.php'); ?>
                                 <div class="content-box bshadow">
                                    <div class="cbox-desc">
                                       <div class="side-travad brand-travad">
                                          <div class="travad-maintitle">Best coffee in the world!</div>
                                          <div class="imgholder">
                                             <img src="images/brand-p.jpg">
                                          </div>
                                          <div class="descholder">
                                             <div class="ad-subtitle">We just get new starbucks coffee that is double in caffine that everybody is calling it a boost!</div>
                                             <a href="javascript:void(0)" class="btn btn-primary btn-sm adbtn waves-effect waves-light waves-effect waves-light waves-effect waves-light waves-effect waves-light waves-effect waves-light waves-effect waves-light">Explore</a>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="content-box bshadow">
                                    <div class="side-travad action-travad">
                                       <div class="travad-maintitle">
                                          <span class="iholder"><i class=”mdi mdi-account-group”></i></i></span>
                                          <h6>Heal Well</h6>
                                          <span class="adtext">Sponsored</span>
                                       </div>
                                       <div class="imgholder">
                                          <img src="images/groupad-actionvideo.jpg"/>
                                       </div>
                                       <div class="descholder">
                                          <div class="ad-title">Medical Research Methodolgy</div>
                                          <div class="ad-subtitle">Checkout the new video on our website exploring the latest techniques of medicine research</div>
                                          <a href="" class="btn btn-primary btn-sm adbtn waves-effect waves-light">Learn More</a>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="post-column">
                              <div class="tab-content ">
                                 <div class="tab-pane fade main-pane" id="travelbuddy-recent">
                                    <div class="post-list">
                                       <div class="post-holder localguide-post bshadow">
                                          <div class="post-topbar">
                                             <div class="post-userinfo">
                                                <div class="img-holder">
                                                   <div id="profiletip-1" class="profiletipholder">
                                                      <span class="profile-tooltip">
                                                      <img class="circle" src="images/demo-profile.jpg"/>
                                                      </span>
                                                      <span class="profiletooltip_content slidingpan-holder">
                                                         <div class="profile-tip dis-none">
                                                            <div class="profile-tip-avatar">
                                                               <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                                               <div class="sliding-pan location-span">
                                                                  <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                               </div>
                                                            </div>
                                                            <div class="profile-tip-name">
                                                               <a href="javascript:void(0)">Adel Hasanat</a>
                                                            </div>
                                                            <div class="profile-tip-info">
                                                               <div class="profiletip-icon">
                                                                  <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                               </div>
                                                               <div class="profiletip-icon">
                                                                  <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                               </div>
                                                               <div class="profiletip-icon">
                                                                  <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class=”mdi mdi-eye”></i></span></a>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </span>
                                                   </div>
                                                </div>
                                                <div class="desc-holder">
                                                   <a href="javascript:void(0)">Nimish Parekh</a> is <span class="etext">Local Driver</span> in <span class="etext">London</span>
                                                   <span class="timestamp">Yesterday</span>
                                                </div>
                                             </div>
                                             <div class="settings-icon">
                                                <div class="dropdown dropdown-custom dropdown-med">
                                                   <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='post_settings1'>
                                                   <i class="zmdi zmdi-more zmdi-hc-2x"></i>
                                                   </a>
                                                   <ul id='post_settings1' class='dropdown-content custom_dropdown'>
                                                      <li class="nicon">
                                                         <!--<a href="javascript:void(0)" class="Send request to guide
                                                            " onclick="generateDiscard('dis_savepost')">Save post</a>-->
                                                         <a href="javascript:void(0)">Save post</a>
                                                      </li>
                                                      <li class="nicon"><a href="javascript:void(0)" onclick="generateDiscard('dis_delpost')">Delete post</a></li>
                                                      <li class="nicon"><a href="javascript:void(0)" onclick="generateDiscard('dis_mutepost')">Mute this person post</a></li>
                                                      <li class="nicon"><a href="javascript:void(0)" onclick="reportAbuseModal()">Report post</a></li>
                                                      <li class="nicon"><a href="javascript:void(0)"  onclick="generateDiscard('dis_blockoffer')">Block</a></li>
                                                   </ul>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="post-content">
                                             <div class="post-details">
                                                <div class="post-localguide">
                                                   <span class="ispan"><i class="zmdi zmdi-pin"></i>Lives in London</span>
                                                   <div class="more-info">
                                                      <span class="ispan"><i class="mdi mdi-comment-outline"></i>Speaks English</span>
                                                      <span class="ispan"><i class="mdi mdi-bookmark"></i>References 12</span>
                                                      <span class="ispan connectionslabel"><i class=”mdi mdi-account-group”></i></i>Connections 35</span>                                                
                                                   </div>
                                                </div>
                                                <div class="content-holder mode-holder">
                                                   <div class="normal-mode">
                                                      <div class="post-desc">
                                                         <div class="para-section hire-para">
                                                            <div class="para">
                                                               <p class="dpara"><i class="mdi mdi-format-quote-open"></i>Hello Well, Firstly I&apos;m extremely  outgoing and confident. Always honest about everything else! Living in London I love to try new things and generally try to try everything at least once...</p>
                                                            </div>
                                                            <a href="javascript:void(0)" class="readlink">Read More</a>
                                                         </div>
                                                      </div>
                                                      <div class="drow">
                                                         <div class="caption-holder">Guide activities</div>
                                                         <div class="detail-holder">Group Events, Biking, Hangout</div>
                                                      </div>
                                                      <div class="drow">
                                                         <div class="caption-holder">Fees</div>
                                                         <div class="detail-holder">$15 per hour</div>
                                                      </div>
                                                      <div class="right">
                                                         <a href="driver-detail.php">LEARN MORE</a>
                                                         <a href="javascript:void(0)" class="pl-2">CONTACT</a>
                                                      </div>
                                                   </div>
                                                   <div class="detail-mode">
                                                      <div class="post-desc">
                                                         <div class="para-section hire-para">
                                                            <div class="para">
                                                               <p class="dpara"><i class="mdi mdi-format-quote-open"></i>Hello Well, Firstly I&apos;m extremely  outgoing and confident. Always honest about everything else! Living in London I love to try new things and generally try to try everything at least once...</p>
                                                            </div>
                                                            <a href="javascript:void(0)" class="readlink">Read More</a>
                                                         </div>
                                                      </div>
                                                      <div class="drow">
                                                         <div class="caption-holder">Guide activities</div>
                                                         <div class="detail-holder">Group Events, Biking, Hangout</div>
                                                      </div>
                                                      <div class="drow">
                                                         <div class="caption-holder">Fees</div>
                                                         <div class="detail-holder">$15 per hour</div>
                                                      </div>
                                                      <div class="invite-section">
                                                         <div class="inner-box">
                                                            <div class="notice-holder">
                                                               <div class="settings-notice">
                                                                  <span class="success-note"></span>
                                                                  <span class="error-note"></span>
                                                                  <span class="info-note"></span>
                                                               </div>
                                                            </div>
                                                            <h5>Send request to guide</h5>
                                                            <div class="form-holder">
                                                               <ul>
                                                                  <li>
                                                                     <div class="caption-holder"><label>Arrival Date</label></div>
                                                                     <div class="detail-holder">
                                                                        <div class="halfwidth dateinput">
                                                                           <input type="text" onkeydown="return false;" placeholder="Choose Date" class="form-control datetime-picker hasDatepicker datepicker" data-toggle="datepicker" id="dp1484202055789" readonly>
                                                                        </div>
                                                                     </div>
                                                                  </li>
                                                                  <li>
                                                                     <div class="caption-holder"><label>Departure Date</label></div>
                                                                     <div class="detail-holder">
                                                                        <div class="halfwidth dateinput">
                                                                           <input type="text" onkeydown="return false;" placeholder="Choose Date" class="form-control datetime-picker hasDatepicker datepicker" data-toggle="datepicker" id="dp1484202055790" readonly>
                                                                        </div>
                                                                     </div>
                                                                  </li>
                                                                  <li>
                                                                     <div class="fullwidth"><label>Traveller message to guide</label></div>
                                                                     <div class="fullwidth">
                                                                        <div class="fullwidth tt-holder">
                                                                           <textarea class="materialize-textarea mb0 md_textarea descinput" placeholder="Write your message to Nimish Parekh">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</textarea>
                                                                        </div>
                                                                     </div>
                                                                  </li>
                                                               </ul>
                                                               <div class="btn-holder">
                                                                  <a href="javascript:void(0)" class="btngen-center-align waves-effect" onclick="close_detail(this)">Cancel</a>
                                                                  <a href="javascript:void(0)" class="btngen-center-align waves-effect" onclick="close_detail(this)">Send</a>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="post-holder localguide-post bshadow">
                                          <div class="post-topbar">
                                             <div class="post-userinfo">
                                                <div class="img-holder">
                                                   <div id="profiletip-1" class="profiletipholder">
                                                      <span class="profile-tooltip">
                                                      <img class="circle" src="images/demo-profile.jpg"/>
                                                      </span>
                                                      <span class="profiletooltip_content slidingpan-holder">
                                                         <div class="profile-tip dis-none">
                                                            <div class="profile-tip-avatar">
                                                               <img alt="user-photo" class="img-responsive" src="images/demo-profile.jpg">                                       
                                                               <div class="sliding-pan location-span">
                                                                  <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                               </div>
                                                            </div>
                                                            <div class="profile-tip-name">
                                                               <a href="javascript:void(0)">Adel Hasanat</a>
                                                            </div>
                                                            <div class="profile-tip-info">
                                                               <div class="profiletip-icon">
                                                                  <a href="javascript:void(0)" class="sliding-link" onclick="manageSlidingPan(this,'location-span')" title="Ahmedabad, Gujarat, India"><span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span></a>
                                                               </div>
                                                               <div class="profiletip-icon">
                                                                  <a href="javascript:void(0)"><i class="mdi mdi-account-plus"></i></a>
                                                               </div>
                                                               <div class="profiletip-icon">
                                                                  <a href="javascript:void(0)" title="View Profile"><span class="ptip-icon"><i class=”mdi mdi-eye”></i></span></a>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </span>
                                                   </div>
                                                </div>
                                                <div class="desc-holder">
                                                   <a href="javascript:void(0)">Adel Hasanat</a> is <span class="etext">Local Driver</span> in <span class="etext"><?=$st_nm_S?></span>
                                                   <span class="timestamp">Yesterday</span>
                                                </div>
                                             </div>
                                             <div class="settings-icon">
                                                <div class="dropdown dropdown-custom dropdown-med">
                                                   <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='post_settings2'>
                                                   <i class="zmdi zmdi-more zmdi-hc-2x"></i>
                                                   </a>
                                                   <ul id='post_settings2' class='dropdown-content custom_dropdown'>
                                                      <li class="nicon"><a href="javascript:void(0)" class="Send request to guide
                                                         " onclick="generateDiscard('dis_savepost')">Save post</a></li>
                                                      <li class="nicon"><a href="javascript:void(0)" onclick="generateDiscard('dis_delpost')">Delete post</a></li>
                                                      <li class="nicon"><a href="javascript:void(0)" onclick="generateDiscard('dis_mutepost')">Mute this person post</a></li>
                                                      <li class="nicon"><a href="javascript:void(0)" onclick="reportAbuseModal()">Report post</a></li>
                                                      <li class="nicon"><a href="javascript:void(0)"  onclick="generateDiscard('dis_blockoffer')">Block</a></li>
                                                   </ul>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="post-content">
                                             <div class="post-details">
                                                <div class="post-localguide">
                                                   <span class="ispan"><i class="zmdi zmdi-pin"></i>Lives in <?=$st_nm_S?></span>
                                                   <div class="more-info">
                                                      <span class="ispan"><i class="mdi mdi-comment-outline"></i>Speaks English</span>
                                                      <span class="ispan"><i class="mdi mdi-bookmark"></i>References 12</span>
                                                      <span class="ispan connectionslabel"><i class=”mdi mdi-account-group”></i></i>Connections 35</span>                                                
                                                   </div>
                                                </div>
                                                <div class="content-holder mode-holder">
                                                   <div class="normal-mode">
                                                      <div class="post-desc">
                                                         <div class="para-section hire-para">
                                                            <div class="para">
                                                               <p class="dpara"><i class="mdi mdi-format-quote-open"></i>Hello Well, Firstly I&apos;m extremely  outgoing and confident. Always honest about everything else! Living in London I love to try new things and generally try to try everything at least once...</p>
                                                            </div>
                                                            <a href="javascript:void(0)" class="readlink">Read More</a>
                                                         </div>
                                                      </div>
                                                      <div class="drow">
                                                         <div class="caption-holder">Guide activities</div>
                                                         <div class="detail-holder">Group Events, Biking, Hangout</div>
                                                      </div>
                                                      <div class="drow">
                                                         <div class="caption-holder">Fees</div>
                                                         <div class="detail-holder">Negotiable</div>
                                                      </div>
                                                      <div class="right">
                                                         <a href="guide-detail.php">LEARN MORE</a>
                                                         <a href="javascript:void(0)" class="pl-2">CONTACT</a>
                                                      </div>
                                                   </div>
                                                   <div class="detail-mode">
                                                      <div class="post-desc">
                                                         <div class="para-section hire-para">
                                                            <div class="para">
                                                               <p class="dpara"><i class="mdi mdi-format-quote-open"></i>Hello Well, Firstly I&apos;m extremely  outgoing and confident. Always honest about everything else! Living in London I love to try new things and generally try to try everything at least once...</p>
                                                            </div>
                                                            <a href="javascript:void(0)" class="readlink">Read More</a>
                                                         </div>
                                                      </div>
                                                      <div class="drow">
                                                         <div class="caption-holder">Guide activities</div>
                                                         <div class="detail-holder">Group Events, Biking, Hangout</div>
                                                      </div>
                                                      <div class="drow">
                                                         <div class="caption-holder">Fees</div>
                                                         <div class="detail-holder">$15 per hour</div>
                                                      </div>
                                                      <div class="invite-section">
                                                         <div class="inner-box">
                                                            <div class="notice-holder">
                                                               <div class="settings-notice">
                                                                  <span class="success-note"></span>
                                                                  <span class="error-note"></span>
                                                                  <span class="info-note"></span>
                                                               </div>
                                                            </div>
                                                            <h5>Send request to guide</h5>
                                                            <div class="form-holder">
                                                               <ul>
                                                                  <li>
                                                                     <div class="caption-holder"><label>Arrival Date</label></div>
                                                                     <div class="detail-holder">
                                                                        <div class="halfwidth dateinput">
                                                                           <input type="text" onkeydown="return false;" placeholder="Choose Date" class="form-control datetime-picker hasDatepicker datepickerinput" data-query="all" data-toggle="datepicker" id="dp1484202055789" readonly>
                                                                        </div>
                                                                     </div>
                                                                  </li>
                                                                  <li>
                                                                     <div class="caption-holder"><label>Departure Date</label></div>
                                                                     <div class="detail-holder">
                                                                        <div class="halfwidth dateinput">
                                                                           <input type="text" onkeydown="return false;" placeholder="Choose Date" class="form-control datetime-picker hasDatepicker datepickerinput" data-query="all" data-toggle="datepicker" id="dp1484202055790" readonly>
                                                                        </div>
                                                                     </div>
                                                                  </li>
                                                                  <li>
                                                                     <div class="fullwidth"><label>Traveller message to guide</label></div>
                                                                     <div class="fullwidth">
                                                                        <div class=" fullwidth tt-holder">
                                                                           <textarea class="materialize-textarea mb0 md_textarea descinput" placeholder="Write your message to Nimish Parekh">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</textarea>
                                                                        </div>
                                                                     </div>
                                                                  </li>
                                                               </ul>
                                                               <div class="btn-holder">
                                                                  <a href="javascript:void(0)" class="btngen-center-align waves-effect" onclick="close_detail(this)">Cancel</a>
                                                                  <a href="javascript:void(0)" class="btngen-center-align waves-effect" onclick="close_detail(this)">Send</a>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="new-post-mobile clear createDriverAction">
                           <a href="javascript:void(0)" class="popup-window" ><i class="mdi mdi-account"></i></a>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<?php include("common/footer.php"); ?>
</div>	
<?php include('common/discard_popup.php'); ?>
</body>
</html>

<div id="compose_tb_travel" class="modal cust-pop tbpost_modal custom_modal split-page main_modal">
   <div class="modal_content_container">
      <div class="modal_content_child modal-content">
         <div class="popup-title ">
            <button class="hidden_close_span close_span waves-effect">
            <i class="mdi mdi-close mdi-20px compose_discard_popup"></i>
            </button>            
            <h3>Create public trip</h3>
            <a type="button" class="item_done crop_done hidden_close_span custom_close waves-effect" href="javascript:void(0)" >Done</a>
         </div>
         <div class="custom_modal_content modal_content" id="createpopup">
            <div class="travelbuddy-yours profile-tab">
               <div class="travelbuddy-box detail-box">
                  <div class="content-holder main-holder">
                     <div class="summery">
                        <div class="dsection bborder expandable-holder expanded">
                           <div class="form-area expandable-area">
                              <form class="travelbuddy-form">
                                 <div class="form-box">
                                    <div class="fulldiv">
                                       <div class="half">
                                          <div class="frow">
                                             <div class="caption-holder">
                                                <label>Where would you like to go</label>
                                             </div>
                                             <div class="detail-holder">
                                                <div class="input-field">
                                                   <input placeholder="Enter city name" data-query="all"  onfocus="filderMapLocationModal(this)" class="fullwidth locinput" type="text" value="" id="createlocation">
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="frow">
                                       <div class="caption-holder">
                                          <label>Arrival/Departure</label>
                                       </div>
                                       <div class="fulldiv">
                                          <div class="half">
                                             <div class="detail-holder">
                                                <div class="input-field">
                                                   <input type="text" placeholder="Arrival Date" data-toggle="datepicker" class="fullwidth datepickerinput" data-query="all" readonly/>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="half">
                                             <div class="detail-holder">
                                                <div class="input-field">
                                                   <input type="text" placeholder="Departure Date" data-toggle="datepicker" class="fullwidth locinput margin-top15 datepickerinput" data-query="all" readonly/>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="frow radio-div">
                                       <div class="caption-holder">
                                          <label>What are you looking for?</label>
                                       </div>
                                       <div class="cus-redio">
                                          <input name="group1" type="radio" id="buddy1"  />
                                          <label for="buddy1">Buddy</label>
                                       </div>
                                       <div class="cus-redio">
                                          <input name="group1" type="radio" id="Host1"  />
                                          <label for="Host1">Host</label>
                                       </div>
                                    </div>
                                    <div class="frow">
                                       <div class="caption-holder">
                                          <label>When i'm in my destination, I am planning to</label>
                                       </div>
                                       <div class="detail-holder">
                                          <div class="input-field">
                                             <textarea class="materialize-textarea mb0 md_textarea descinput" placeholder="Tell people about your plan" onkeypress="callmyfun('createpopup');"></textarea>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </form>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div> 
   </div>
   <div class="valign-wrapper additem_modal_footer modal-footer">
      <a href="javascript:void(0)" class="btngen-center-align close_modal open_discard_modal waves-effect">Cancel</a>
      <a href="javascript:void(0)" class="btngen-center-align waves-effect">Publish</a>
   </div>
</div>

<!--Create travel host-->
<div id="compose_tb_travelhost" class="modal cust-pop tbpost_modal custom_modal split-page main_modal small-height">
   <div class="modal_content_container">
      <div class="modal_content_child modal-content">
         <div class="popup-title ">
            <button class="hidden_close_span close_span waves-effect">
            <i class="mdi mdi-close mdi-20px compose_discard_popup"></i>
            </button>	
            <h3>Host profile</h3>
            <a type="button" class="item_done crop_done hidden_close_span custom_close waves-effect" href="javascript:void(0)" >Done</a>
         </div>
         <div class="custom_modal_content modal_content" id="travelhost">
            <div class="travelbuddy-yours profile-tab">
               <div class="travelbuddy-box detail-box">
                  <div class="content-holder main-holder">
                     <div class="summery">
                        <div class="dsection bborder expandable-holder expanded">
                           <div class="form-area expandable-area">
                              <form class="travelbuddy-form">
                                 <div class="form-box">
                                    <div class="fulldiv">
                                       <div class="half">
                                          <div class="frow">
                                             <div class="caption-holder">
                                                <label> Hosting Availability </label>
                                             </div>
                                             <div class="detail-holder">
                                                <div class="input-field">
                                                   <select class="radio-change">
                                                      <option value="">Accepting Guests</option>
                                                      <option value="noAccepting">Not Accepting Guests</option>
                                                   </select>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="frow radio-div  box11">
                                       <div class="caption-holder">
                                          <label> Host Services </label>
                                       </div>
                                       <div class="cus-redio disable-radi">
                                          <input name="group1" type="checkbox" id="Hangaround1"  />
                                          <label for="Hangaround1">Hang around</label>
                                       </div>
                                       <div class="cus-redio disable-radi">
                                          <input name="group1" type="checkbox" id="Dining1"  />
                                          <label for="Dining1">Dining</label>
                                       </div>
                                       <div class="cus-redio disable-radi">
                                          <input name="group1" type="checkbox" id="Site-Touring"  />
                                          <label for="Site-Touring">Site Touring</label>
                                       </div>
                                    </div>
                                    <div class="frow radio-div  box11 ">
                                       <div class="caption-holder">
                                          <label> Personal message to travellers</label>
                                       </div>
                                       <div class="detail-holder">
                                          <div class="input-field disable-radi">
                                             <textarea class="materialize-textarea mb0 md_textarea descinput" placeholder="Introduce yourself to travellers autof" onkeypress="callmyfun('travelhost');"></textarea>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </form>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="valign-wrapper additem_modal_footer modal-footer">
      <a href="javascript:void(0)" class="btngen-center-align close_modal open_discard_modal waves-effect">Cancel</a>
      <a href="javascript:void(0)" class="btngen-center-align waves-effect">Publish</a>
   </div>
</div>
<!--Create travel host-->
<!-- edit tb post modal -->
<div id="edit_tb_post" class="modal cust-pop tbpost_modal custom_modal split-page main_modal small-height">
   <div class="modal_content_container">
      <div class="modal_content_child modal-content">
         <div class="popup-title ">
            <button class="hidden_close_span close_span waves-effect">
            <i class="mdi mdi-close mdi-20px compose_discard_popup"></i>
            </button>			
            <h3>Edit public trip</h3>
            <a type="button" class="item_done crop_done hidden_close_span custom_close waves-effect" href="javascript:void(0)" >Done</a>
         </div>
         <div class="custom_modal_content modal_content" id="edit_travelhost">
            <div class="hangout-yours profile-tab">
               <div class="hangout-box detail-box">
                  <div class="content-holder main-holder">
                     <div class="summery">
                        <div class="dsection bborder expandable-holder expanded">
                           <div class="form-area expandable-area">
                              <form class="travelbuddy-form">
                                 <div class="form-box">
                                    <div class="fulldiv">
                                       <div class="half">
                                          <div class="frow">
                                             <div class="caption-holder">
                                                <label>Where would you like to go</label>
                                             </div>
                                             <div class="detail-holder">
                                                <div class="input-field">
                                                   <input placeholder="Enter location" data-query="M"  onfocus="filderMapLocationModal(this)" class="fullwidth locinput" type="text" value="" id="editlocation">
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="frow">
                                       <div class="caption-holder">
                                          <label>Arrival/Departure</label>
                                       </div>
                                       <div class="fulldiv ">
                                          <div class="half">
                                             <div class="detail-holder">
                                                <div class="input-field">
                                                   <input type="text" placeholder="Arrival Date" value="" class="fullwidth locinput datepickerinput" data-query="all" data-toggle="datepicker" readonly/>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="half">
                                             <div class="detail-holder">
                                                <div class="input-field">
                                                   <input type="text" placeholder="Departure Date" class="fullwidth locinput datepickerinput margin-top15" data-query="all" data-toggle="datepicker" readonly/>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="frow radio-div">
                                       <div class="caption-holder">
                                          <label>What are you looking for?</label>
                                       </div>
                                       <div class="cus-redio">
                                          <input name="group1" type="radio" id="buddy-new"  />
                                          <label for="buddy-new">Buddy</label>
                                       </div>
                                       <div class="cus-redio">
                                          <input name="group1" type="radio" id="Host11-new"  />
                                          <label for="Host11-new">Host</label>
                                       </div>
                                    </div>
                                    <div class="frow">
                                       <div class="caption-holder">
                                          <label>When i'm in my destination, I am planning to</label>
                                       </div>
                                       <div class="detail-holder">
                                          <div class="input-field">
                                             <textarea class="materialize-textarea mb0 md_textarea descinput" placeholder="Tell people about your plan" onkeypress="callmyfun('edit_travelhost');">Lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum</textarea>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </form>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="valign-wrapper additem_modal_footer modal-footer">
      <a href="javascript:void(0)" class="btngen-center-align close_modal open_discard_modal waves-effect">Cancel</a>
      <a href="javascript:void(0)" class="btngen-center-align waves-effect">Save</a>
   </div>
</div>

<!-- create driver profile modal -->
<div id="createLocalDriverModal" class="modal tbpost_modal custom_modal split-page main_modal cust-pop dicrease-popup-compose event-detail-modal">
   <div class="modal_content_container">
      <div class="modal_content_child modal-content">
         <div class="popup-title ">
            <button class="hidden_close_span close_span waves-effect">
            <i class="mdi mdi-close mdi-20px compose_discard_popup"></i>
            </button>         
            <h3>Create driver profile</h3>
            <a type="button" class="item_done crop_done hidden_close_span custom_close waves-effect" href="javascript:void(0)">Done</a>
         </div>
         <div class="custom_modal_content modal_content" id="createpopup">
            <div class="ablum-yours profile-tab">
               <div class="ablum-box detail-box">
                  <div class="content-holder main-holder">
                     <div class="summery">
                        <div class="dsection bborder expandable-holder expanded">
                           <div class="form-area expandable-area">
                              <form class="ablum-form">
                                 <div class="form-box">
                                    <div class="fulldiv">
                                       <div class="frow">
                                          <div class="caption-holder">
                                             <label>Vehicle type</label>
                                          </div>
                                          <div class="detail-holder">
                                             <div class="input-field">
                                                <input type="text" placeholder="i.e Toyata van with air condition" class="fullwidth locinput "/>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="fulldiv">
                                       <div class="frow">
                                          <div class="caption-holder">
                                             <label>On-board</label>
                                          </div>
                                          <div class="detail-holder">
                                             <div class="input-field">
                                                <input type="text" placeholder="i.e fresh water bottles, cooler, charger..." class="fullwidth locinput "/>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="fulldiv">
                                       <div class="frow">
                                          <div class="caption-holder">
                                             <label>Vehicle capacity</label>
                                          </div>
                                          <div class="detail-holder">
                                             <div class="input-field">
                                                <input type="text" placeholder="From 1 to 6 people i.e six travellers "/>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="fulldiv">
                                       <div class="frow">
                                          <div class="caption-holder">
                                             <label>Restriction</label>
                                          </div>
                                          <div class="detail-holder">
                                             <div class="input-field">
                                                <input type="text" placeholder="i.e things that you can not do"/>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="fulldiv">
                                       <div class="frow">
                                          <div class="caption-holder">
                                             <label>Meet your driver</label>
                                          </div>
                                          <div class="detail-holder">
                                             <div class="input-field">
                                                <input type="text" placeholder="Tell people about your talent"/>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="fulldiv">
                                       <div class="row">
                                          <div class="col s6">
                                             <div class="frow">
                                                <div class="caption-holder">
                                                   <label>Activities that I can be hired for*</label>
                                                </div>
                                                <div class="detail-holder custom-hireaguide dropdown782">
                                                   <p class="firs-show mt-5 mb0">
                                                      <input type="checkbox" id="everything6"  onChange="selectAll(this);"/>
                                                      <label for="everything6">I'm up for everything</label>
                                                   </p>
                                                   <div class="input-field input-field1 dropdown782">
                                                      <select data-fill="n" data-action="hireguideevent" data-selectore="hireguideeventname" id="creproactivitydropdown" class="eventname hireguideeventname" multiple>
                                                         <?php
                                                         $event = array("Touring", "Site Seeing", "Parks", "Museum", "Beaches", "Showing the city", "Outdoor event");
                                                         foreach ($event as $s9032n) {
                                                           echo "<option value=".$s9032n.">$s9032n</option>";
                                                         }
                                                         ?>
                                                      </select>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="fulldiv">
                                       <div class="frow">
                                          <div class="caption-holder">
                                             <div class="row">
                                                <div class="col l3 m4 s12">
                                                   <label>My Fees*</label>
                                                </div>
                                                <div class="col l6 m8 s12">
                                                   <div class="detail-holder">
                                                      <div class="input-field dropdown782">
                                                         <select id="chooseFee" class="feedrp" data-selectore="feedrp" data-fill="n" data-action="fee">
                                                            <option value="" disabled selected>Choose Fee</option>
                                                            <?php
                                                               $fee = array("$35 per day", "$40 per day", "$45 per day", "$50 per day", "$55 per day", "$60 per day", "$65 per day", "$70 per day", "$75 per day", "$80 per day", "$90 per day", "$100 per day");
                                                               foreach ($fee as $s8032n) {
                                                                  echo "<option value=".$s8032n.">$s8032n</option>";
                                                               }
                                                             ?>
                                                         </select>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="frow nomargin new-post">
                                       <div class="caption-holder">
                                          <label>Awesome photos help guests want to hire you</label>
                                       </div>
                                       <div class="detail-holder">
                                          <div class="input-field ">
                                             <div class="post-photos new_pic_add">
                                                <div class="img-row">
                                                   <div class="img-box">
                                                      <div class="custom-file addimg-box add-photo ablum-add">
                                                         <span class="icont">+</span><br><span class="">Upload photo</span>
                                                         <div class="addimg-icon">
                                                         </div>
                                                         <input class="upload custom-upload remove-custom-upload" title="Choose a file to upload" required="" data-class=".post-photos .img-row" multiple="true" type="file">
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <p class="photolabelinfo">Please add three cover photos for your profile</p>
                                    </div>
                                 </div>
                              </form>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="valign-wrapper additem_modal_footer modal-footer">
      <a href="javascript:void(0)" class="btngen-center-align close_modal open_discard_modal waves-effect">Cancel</a>
      <a href="javascript:void(0)" class="btngen-center-align waves-effect">Publish</a>
   </div>
</div>

<?php include('common/datepicker.php'); ?>

<!--map modal-->
<div id="compose_mapmodal" class="modal map_modal compose_inner_modal modalxii_level1">	 
   <?php include('common/map_modal.php'); ?>
</div>
<?php include("script.php"); ?>