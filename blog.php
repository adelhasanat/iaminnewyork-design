
<?php include("header.php"); ?>
<?php include("common/menu.php"); ?>
<div class="floating-icon">
   <div class="scrollup-btnbox anim-side btnbox scrollup-float">
      <div class="scrollup-button float-icon"><span class="icon-holder ispan"><i class="mdi mdi-arrow-up-bold-circle"></i>
</span></div>
   </div>
</div>
<div class="clear"></div> 
<div class="container page_container pages_container">
   <?php include("common/leftmenu.php"); ?>
   <div class="fixed-layout ipad-mfix">
      <div class="content-box nbg noboxshadow">
         <div class="hcontent-holder home-section gray-section tours-page tours dine-local dine-inner-pages">
            <div class="container mt-10">
               <div class="tours-section">
                  <div class="row mx-0 valign-wrapper label-head">
                     <div class="py-20 left">
                        <h3 class="heading-inner">BLOGS</h3>
                        <p class="para-inner">Create blog or add comments to blogs</p>
                     </div>
                     <div class="ml-auto">
                        <a href=""><i class="mdi mdi-plus-box"></i> BLOG</a>
                     </div>
                  </div>
                  <div class="row">
                     <ul class="collection">
                        <li class="collection-item avatar">
                           <img src="images/page-img.png" alt="">
                           <div class="icons">
                              <i class="zmdi zmdi-edit"></i>
                              <i class="zmdi zmdi-delete"></i>
                           </div>
                           <a href="blog-detail.php">
                              <span class="title">US 'underestimates' Huawei, founder says</span>
                              <p>
                              A dispute over Huawei has escalated with implications for the firm, the tech sector and consumers. A dispute over Huawei has escalated with implications for the firm, the tech sector and consumers. A dispute over Huawei has escalated with implications for the firm, the tech sector and consumers A dispute over Huawei has escalated with implications for the firm, the tech sector and consumers A dispute over Huawei has escalated with implications for the firm, the tech sector and consumers A dispute over Huawei has escalated with implications for the firm, the tech sector and consumersA dispute over Huawei has escalated with implications for the firm, the tech sector and consumersAA dispute over Huawei has escalated with implications for the firm, the tech sector and consumersA dispute over Huawei has escalated with implications for the firm, the tech sector and consumersA dispute over Huawei has escalated with implications for the firm, the tech sector and consumersA dispute over Huawei has escalated with implications for the firm, the tech sector and consumersAA A dispute over Huawei has escalated with implications for the firm, the tech sector and consumersA dispute over Huawei has escalated with implications for the firm, the tech sector and consumersA dispute over Huawei has escalated with implications for the firm, the tech sector and consumersA dispute over Huawei has escalated with implications for the firm, the tech sector and consumers.
                              </p>
                           </a>
                           <span class="secondary-content">6h</span>
                           <span class="post-by">by Adel Hasanat</span>
                           <div class="social-icons">
                              <a href="javascript: void(0)" class=""><i class="mdi mdi-thumb-up-outline mdi-15px" onclick="toggleIcons(this)"></i><span class="lcount">7</span></a>
                              <a href="javascript: void(0)" class="cmnt"><i class="mdi mdi-comment-outline mdi-15px"></i></a>
                           </div>
                        </li>
                        <li class="collection-item avatar">
                           <img src="images/page-img.png" alt="">
                           <div class="icons">
                              <i class="zmdi zmdi-edit"></i>
                              <i class="zmdi zmdi-delete"></i>
                           </div>
                           <a href="blog-detail.php">
                              <span class="title">US 'underestimates' Huawei, founder says</span>
                              <p>
                              A dispute over Huawei has escalated with implications for the firm, the tech sector and consumers. A dispute over Huawei has escalated with implications for the firm, the tech sector and consumers. A dispute over Huawei has escalated with implications for the firm, the tech sector and consumers A dispute over Huawei has escalated with implications for the firm, the tech sector and consumers A dispute over Huawei has escalated with implications for the firm, the tech sector and consumers A dispute over Huawei has escalated with implications for the firm, the tech sector and consumers A dispute over Huawei has escalated with implications for the firm, the tech sector and consumers.
                              </p>
                           </a>
                           <span class="secondary-content">6h</span>
                           <span class="post-by">by Adel Hasanat</span>
                           <div class="social-icons">
                              <a href="javascript: void(0)" class=""><i class="mdi mdi-thumb-up-outline mdi-15px" onclick="toggleIcons(this)"></i><span class="lcount">7</span></a>
                              <a href="javascript: void(0)" class="cmnt"><i class="mdi mdi-comment-outline mdi-15px"></i></a>
                           </div>
                        </li>
                        <li class="collection-item avatar">
                           <img src="images/page-img.png" alt="">
                           <div class="icons">
                              <i class="zmdi zmdi-edit"></i>
                              <i class="zmdi zmdi-delete"></i>
                           </div>
                           <a href="blog-detail.php">
                              <span class="title">US 'underestimates' Huawei, founder says</span>
                              <p>
                              A dispute over Huawei has escalated with implications for the firm, the tech sector and consumers. A dispute over Huawei has escalated with implications for the firm, the tech sector and consumers. A dispute over Huawei has escalated with implications for the firm, the tech sector and consumers.
                              </p>
                           </a>
                           <span class="secondary-content">6h</span>
                           <span class="post-by">by Adel Hasanat</span>
                           <div class="social-icons">
                              <a href="javascript: void(0)" class=""><i class="mdi mdi-thumb-up-outline mdi-15px" onclick="toggleIcons(this)"></i><span class="lcount">7</span></a>
                              <a href="javascript: void(0)" class="cmnt"><i class="mdi mdi-comment-outline mdi-15px"></i></a>
                           </div>
                        </li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

<div class="new-post-mobile clear upload-gallery">
   <a href="javascript:void(0)"><i class="mdi mdi-plus"></i></a>
</div>

<?php include("common/footer.php"); ?>
</div>
<!--add page modal-->
<div id="add-item-popup" class="modal add-item-popup custom_md_modal collec_main_cus add_collection_new dropdownheight145 blog-popup">
   <div class="modal_content_container">
      <div class="modal_content_child modal-content">
         <div class="popup-title ">
            <button class="hidden_close_span close_span waves-effect">
            <i class="mdi mdi-close mdi-20px compose_discard_popup"></i>
            </button>			 
            <h3>Create a page</h3>
            <a type="button" class="item_done crop_done hidden_close_span close_modal waves-effect" href="javascript:void(0)" >Done</a>
         </div>
         <div class="main-pcontent">
            <form class="add-item-form">
               <div class="frow frowfull">
                  <div class="crop-holder" id="image-cropper">
                     <div class="cropit-preview"></div>
                     <div class="main-img">
                        <img src="images/additem-collections.png" class="ui-corner-all"/>
                     </div>
                     <div class="main-img1">
                        <img id="imageid" draggable="false"/>
                     </div>
                     <div class="btnupload custom_up_load" id="upload_img_action">
                        <div class="fileUpload">
                           <i class="zmdi zmdi-camera"></i>
                           <input type="file" name="filupload" id="crop-file" class="upload cropit-image-input" />
                        </div>
                     </div>
                     <a  href="javascript:void(0)" class="btn btn-save image_save_btn image_save dis-none">
                     <span class="zmdi zmdi-check"></span>
                     </a>
                     <a id="removeimg" href="javascript:void(0)" class="collection_image_trash image_trash">
                     <i class="mdi mdi-close" ></i>
                     </a>
                  </div>
               </div>
               <div class="sidepad">
                  <div class="frow">
                     <input id="page_title" type="text" class="validate item_title" placeholder="Page title" />					
                  </div>
                  <div class="frow dropdown782">
                     <select id="pageCatDrop1" class="pageservices" data-fill="n" data-action="pageservices" data-selectore="pageservices">
                     <?php
                        $page = array("Bags/Luggage" => "Bags/Luggage", "Camera/Photo" => "Camera/Photo", "Cars" => "Cars", "Clothing" => "Clothing", "Entertainment" => "Entertainment", "Professional Services" => "Professional Services", "Sporting Goods" => "Sporting Goods", "Kitchen/Cooking" => "Kitchen/Cooking", "Concert Tour" => "Concert Tour", "Concert Venue" => "Concert Venue", "Food/Beverages" => "Food/Beverages", "Outdoor Gear" => "Outdoor Gear", "Tour Operator" => "Tour Operator", "Travel Agency" => "Travel Agency", "Travel Services" => "Travel Services", "Attractions/Things to Do" => "Attractions/Things to Do", "Event Planning/Event Services" => "Event Planning/Event Services", "Hotel" => "Hotel", "Landmark" => "Landmark", "Movie Theater" => "Movie Theater", "Museum/Art gallery" => "Museum/Art gallery", "Outdoor Gear/Sporting Goods" => "Outdoor Gear/Sporting Goods", "Public Places" => "Public Places", "Travel Site" => "Travel Site", "Travel Destination" => "Travel Destination", "Organization" => "Organization", "Website" => "Website");
                        foreach ($page as $s9032n) {
                          echo "<option value=".$s9032n.">$s9032n</option>";
                        }
                     ?>
                     </select>
                  </div>
                  <div class="frow">
                     <textarea id="Collection_tagline" class="materialize-textarea mb0 md_textarea item_tagline" placeholder="Short description of your page"></textarea>
                     <span class="char-limit">0/80</span>								
                  </div>
                  <div class="frow">
                     <textarea type="text" placeholder="Tell people more about the page" class="materialize-textarea md_textarea item_about"></textarea>
                  </div>
                  <div class="frow">
                     <input type="text" id="compose_mapmodalAction" class="validate item_title compose_mapmodalAction" placeholder="Bussines address 'City/Country'" data-query="all" onfocus="filderMapLocationModal(this)" autocomplete="off">
                  </div>
                  <div class="frow">
                     <input id="extweb" type="text" class="validate item_title" placeholder="List your external website, if you have one" />
                  </div>
                  <div class="frow">
                     <span class="icon-span"><input type="radio" id="agreeemailpage" name="verify-radio"></span>
                     <p>Veryfiy ownership by sending  a text message to following email</p>
                  </div>
                  <div class="frow">
                     <input type="text" placeholder="Your company email address">
                  </div>
                  <div class="frow">
                     <input type="checkbox" id="create_page" />
                     <label for="create_page">I verify that I am the official representative of this entity and have the right to act on behalf of my entity in the creation of this page.</label>						
                  </div>
               </div>
            </form>
         </div>
      </div>
   </div>
   <div class="valign-wrapper additem_modal_footer modal-footer">
      <a href="javascript:void(0)" class="btngen-center-align close_modal open_discard_modal waves-effect">Cancel</a>
      <a href="javascript:void(0)" class="btngen-center-align waves-effect">Create</a>
   </div>
</div>
<?php include('common/discard_popup.php'); ?>
<div id="compose_mapmodal" class="modal map_modal compose_inner_modal modalxii_level1">
   <?php include('common/map_modal.php'); ?>
</div>
<?php include("script.php"); ?>			
</body>
</html>