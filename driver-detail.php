
<?php include("header.php"); ?>
<?php include("common/menu.php"); ?>
<div class="floating-icon">
   <div class="scrollup-btnbox anim-side btnbox scrollup-float">
      <div class="scrollup-button float-icon">
         <span class="icon-holder ispan">
            <i class="mdi mdi-arrow-up-bold-circle"></i>
         </span>
      </div>
   </div>
</div>
<div class="clear"></div>
<div class="">
   <?php include("common/leftmenu.php"); ?>
   <div class="fixed-layout">
      <div class="collection-page event-detail-page pb-0 m-t-50">
         <div class="combined-column wide-open main-page full-page">
            <div class="width-100 m-top">
                  <div class="collection-gallery-wrapper">
                     <div class="collection-container">
                        <div class="row mx-0">
                           <div class="collection-gallery">
                              <div class="collection-card">
                                 <div class="collection-card-body">  
                                    <a href="">
                                       <div class="collection-card-inner">
                                          <div class="collection-card-left">
                                             <img role="presentation" class="" src="images/driver-detail1.jpg" alt="">
                                          </div> 
                                          <div class="collection-card-middle">
                                             <img role="presentation" class="" src="images/driver-detail2.jpg" alt="">
                                          </div>
                                          <div class="collection-card-right">
                                             <div class="img-right-top">
                                                <img role="presentation" class="" src="images/driver-detail3.jpg" alt="">
                                             </div>
                                          </div>  
                                       </div>
                                    </a>
                                 </div>
                              </div> 
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="event-info-wrapper">
                     <div class="container">
                        <div class="row mx-0">
                           <div class="col m8 s12">
                              <div class="event-title-container">
                                 <div class="row mx-0 valign-wrapper">
                                    <div class="left">
                                       <!-- <h1 class="event-title">Meet Your Guide</h1> -->
                                       <div class="people-box">
                                          <div class="img-holder">
                                             <img src="images/people-2.png">
                                          </div>
                                          <div class="desc-holder">
                                             <a href="javascript:void(0)" class="userlink">Adel Ahasanat</a>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="right ml-auto">
                                       <a href="javascript:void(0)" class="waves-effect waves-theme editDriverAction"><i class="mdi mdi-pencil mdi-20px"></i></a>
                                    </div>
                                 </div>
                              </div>
                              <div class="detail-title-container">
                                 <h3 class="event-detail-title">Meet Your Driver</h3>
                              </div>
                              <div class="info-container">
                                 <p>I invite you to a unique meal with a view on the beautiful canals of Amsterdam. Wine climate cabinet, sous vide teppanyaki and grill cooking options. The menu is 100% plant-based, kosher, lactose-free and can also be made gluten-free upon request! All drinks are included.</p>
                              </div>
                              <div class="info-list full-width-list">
                                 <ul>
                                    <li>
                                       <h5>Vehicle Type</h5>
                                       <p>Toyata Avanza with Air Conditioning</p>
                                    </li>
                                    <li>
                                       <h5>On -Board</h5>
                                       <p>
                                          Mobile phone charger, fresh water bottle, fresh towels, sarongs for the visits and tissues
                                       </p>
                                    </li>
                                    <li>
                                       <h5>Vehicle Capacity</h5>
                                       <p>From 1 to 6 people</p>
                                    </li>
                                    <li>
                                       <h5>Restrictions</h5>
                                       <p>My tours are mostly walking tours but if a vehicle and driver is required I can arrange this</p>
                                    </li>
                                    <li>
                                       <h5>Activities</h5>
                                       <p>Touring, Site Seeing, Business Event, Hangout, Driver with bus showing the city</p>
                                    </li>
                                 </ul>
                              </div>
                              <div class="photo-section mt-20">
                                 <div class="row mx-0 valign-wrapper">
                                    <div class="left">
                                       <h5>PHOTOS</h5>
                                    </div>
                                    <div class="right ml-auto">
                                       <a href="">+ Upload</a>
                                    </div>
                                 </div>
                                 <div class="row mt-10">
                                    <div class="col s3 photobox">
                                       <img role="presentation" class="" src="images/wgallery3.jpg" alt="">
                                       <i class="mdi mdi-delete photosdelete"></i>
                                    </div>
                                    <div class="col s3 photobox">
                                       <img role="presentation" class="" src="images/wgallery3.jpg" alt="">
                                       <i class="mdi mdi-delete photosdelete"></i>
                                    </div>
                                    <div class="col s3 photobox">
                                       <img role="presentation" class="" src="images/wgallery3.jpg" alt="">
                                       <i class="mdi mdi-delete photosdelete"></i>
                                    </div>
                                    <div class="col s3 photobox">
                                       <img role="presentation" class="" src="images/wgallery3.jpg" alt="">
                                       <i class="mdi mdi-delete photosdelete"></i>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="col m4 s12">
                              <div class="event-right-wrapper">
                                 <div class="booking-form-section">
                                    <div class="price-container">
                                       <span><span class="price"> $100 </span> per day</span>
                                    </div>
                                    <div class="ddl-select">
                                       <label>Date</label>
                                       <select class="select2" tabindex="-1" >
                                          <option>Saturday 06/01/2019</option>
                                          <option>Saturday 06/01/2019</option>
                                       </select>
                                    </div>
                                    <div class="ddl-select">
                                       <label>Number of guests</label>
                                       <select class="select2" tabindex="-1" >
                                          <option>1 guest</option>
                                          <option>2 guests</option>
                                       </select>
                                    </div>
                                    <div class="personal-message ddl-select">
                                       <label>Personal Message</label>
                                       <textarea class="materialize-textarea md_textarea item_tagline" placeholder="Hi ... Profile and eperience look wonderful! I  will be in town  for a few days and i m wondering if you could host me Thank you!"></textarea>
                                    </div>
                                    <div class="btn-sec">
                                       <a class="waves-effect waves-light btn" href="javascript:void(0)">Message to Driver</a>
                                    </div>
                                 </div>
                                 <div class="contact-host valign-wrapper">
                                    <span><i class="mdi mdi-comment-outline"></i> Questions? </span>
                                    <a href="">Contact the driver</a>
                                    <span class="right ml-auto"><i class="mdi mdi-chevron-right mdi-17px"></i></span>
                                 </div>
                                 <div class="save-wishlist">
                                    <p class="text-center m-0">
                                       <span class="icon-heart"><i class="mdi mdi-heart mdi-20px"></i></span>
                                       <a href="">Save to your wishlist</a>
                                    </p>
                                 </div>
                                 <div class="request-work">
                                    <h6>How requesting works...</h6>
                                    <p>
                                       <i class="mdi mdi-calendar"></i>
                                       <span>Suggest a date for your trip to the driver. Select how many guests you would like to bring.</span>
                                    </p>
                                    <p>
                                       <i class="mdi mdi-account-multiple-outline mdi-17px"></i></i>
                                       <span>After clicking "Message to driver", the host will then message you about availabilty. You will not be charged to send a request. </span>
                                    </p>
                                 </div> 
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="row reviews-row mx-0">
                        <div class="container">
                           <div class="col m8 s12">
                              <div class="reviews-section mt-20">
                                 <div class="row mx-0 valign-wrapper">
                                    <div class="left">
                                       <h5>REVIEWS</h5>
                                    </div>
                                    <div class="right ml-auto">
                                       <a href="">+ Review</a>
                                    </div>
                                 </div>
                                 <ul class="collection">
                                    <li class="collection-item avatar">
                                       <img src="images/demo-profile.jpg" alt="" class="circle">
                                       <span class="title">Adel Hasanat</span>
                                       <span class="ratings">
                                          <i class="mdi mdi-star"></i>
                                          <i class="mdi mdi-star"></i>
                                          <i class="mdi mdi-star"></i>
                                          <i class="mdi mdi-star"></i>
                                          <i class="mdi mdi-star"></i>
                                       </span>
                                       <p class="date">May 03, 2019</p>
                                       <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                    </li>
                                    <li class="collection-item avatar">
                                       <img src="images/demo-profile.jpg" alt="" class="circle">
                                       <span class="title">Adel Hasanat</span>
                                       <span class="ratings">
                                          <i class="mdi mdi-star"></i>
                                          <i class="mdi mdi-star"></i>
                                          <i class="mdi mdi-star"></i>
                                          <i class="mdi mdi-star"></i>
                                          <i class="mdi mdi-star"></i>
                                       </span>
                                       <p class="date">May 03, 2019</p>
                                       <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                    </li>
                                    <li class="collection-item avatar">
                                       <img src="images/demo-profile.jpg" alt="" class="circle">
                                       <span class="title">Adel Hasanat</span>
                                       <span class="ratings">
                                          <i class="mdi mdi-star"></i>
                                          <i class="mdi mdi-star"></i>
                                          <i class="mdi mdi-star"></i>
                                          <i class="mdi mdi-star"></i>
                                          <i class="mdi mdi-star"></i>
                                       </span>
                                       <p class="date">May 03, 2019</p>
                                       <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                    </li>
                                 </ul>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
            </div>
         </div>
      </div>
   </div>
</div>
   
<!--map modal-->
<div id="compose_mapmodal" class="modal map_modal compose_inner_modal modalxii_level1">
   <?php include('common/map_modal.php'); ?>
</div>

<!-- create driver profile modal -->
<div id="editLocalDriverModal" class="modal tbpost_modal custom_modal split-page main_modal cust-pop dicrease-popup-compose event-detail-modal">
   <div class="modal_content_container">
      <div class="modal_content_child modal-content">
         <div class="popup-title ">
            <button class="hidden_close_span close_span waves-effect">
            <i class="mdi mdi-close mdi-20px compose_discard_popup"></i>
            </button>         
            <h3>Edit driver profile</h3>
            <a type="button" class="item_done crop_done hidden_close_span custom_close waves-effect" href="javascript:void(0)">Done</a>
         </div>
         <div class="custom_modal_content modal_content" id="createpopup">
            <div class="ablum-yours profile-tab">
               <div class="ablum-box detail-box">
                  <div class="content-holder main-holder">
                     <div class="summery">
                        <div class="dsection bborder expandable-holder expanded">
                           <div class="form-area expandable-area">
                              <form class="ablum-form">
                                 <div class="form-box">
                                    <div class="fulldiv">
                                       <div class="frow">
                                          <div class="caption-holder">
                                             <label>Vehicle type</label>
                                          </div>
                                          <div class="detail-holder">
                                             <div class="input-field">
                                                <input type="text" placeholder="i.e Toyata van with air condition" class="fullwidth locinput "/>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="fulldiv">
                                       <div class="frow">
                                          <div class="caption-holder">
                                             <label>On-board</label>
                                          </div>
                                          <div class="detail-holder">
                                             <div class="input-field">
                                                <input type="text" placeholder="i.e fresh water bottles, cooler, charger..." class="fullwidth locinput "/>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="fulldiv">
                                       <div class="frow">
                                          <div class="caption-holder">
                                             <label>Vehicle capacity</label>
                                          </div>
                                          <div class="detail-holder">
                                             <div class="input-field">
                                                <input type="text" placeholder="From 1 to 6 people i.e six travellers "/>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="fulldiv">
                                       <div class="frow">
                                          <div class="caption-holder">
                                             <label>Restriction</label>
                                          </div>
                                          <div class="detail-holder">
                                             <div class="input-field">
                                                <input type="text" placeholder="i.e things that you can not do"/>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="fulldiv">
                                       <div class="frow">
                                          <div class="caption-holder">
                                             <label>Meet your driver</label>
                                          </div>
                                          <div class="detail-holder">
                                             <div class="input-field">
                                                <input type="text" placeholder="Tell people about your talent"/>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="fulldiv">
                                       <div class="row">
                                          <div class="col s6">
                                             <div class="frow">
                                                <div class="caption-holder">
                                                   <label>Activities that I can be hired for*</label>
                                                </div>
                                                <div class="detail-holder custom-hireaguide dropdown782">
                                                   <p class="firs-show mt-5 mb0">
                                                      <input type="checkbox" id="everything6"  onChange="selectAll(this);"/>
                                                      <label for="everything6">I'm up for everything</label>
                                                   </p>
                                                   <div class="input-field input-field1 dropdown782">
                                                      <select data-fill="n" data-action="hireguideevent" data-selectore="hireguideeventname" id="creproactivitydropdown" class="eventname hireguideeventname" multiple>
                                                         <?php
                                                         $event = array("Touring", "Site Seeing", "Parks", "Museum", "Beaches", "Showing the city", "Outdoor event");
                                                         foreach ($event as $s9032n) {
                                                           echo "<option value=".$s9032n.">$s9032n</option>";
                                                         }
                                                         ?>
                                                      </select>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="fulldiv">
                                       <div class="frow">
                                          <div class="caption-holder">
                                             <div class="row">
                                                <div class="col l3 m4 s12">
                                                   <label>My Fees*</label>
                                                </div>
                                                <div class="col l6 m8 s12">
                                                   <div class="detail-holder">
                                                      <div class="input-field dropdown782">
                                                         <select id="chooseFee" class="feedrp" data-selectore="feedrp" data-fill="n" data-action="fee">
                                                            <option value="" disabled selected>Choose Fee</option>
                                                            <?php
                                                               $fee = array("$35 per day", "$40 per day", "$45 per day", "$50 per day", "$55 per day", "$60 per day", "$65 per day", "$70 per day", "$75 per day", "$80 per day", "$90 per day", "$100 per day");
                                                               foreach ($fee as $s8032n) {
                                                                  echo "<option value=".$s8032n.">$s8032n</option>";
                                                               }
                                                             ?>
                                                         </select>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="frow nomargin new-post">
                                       <div class="caption-holder">
                                          <label>Awesome photos help guests want to hire you</label>
                                       </div>
                                       <div class="detail-holder">
                                          <div class="input-field ">
                                             <div class="post-photos new_pic_add">
                                                <div class="img-row">
                                                   <div class="img-box">
                                                      <div class="custom-file addimg-box add-photo ablum-add">
                                                         <span class="icont">+</span><br><span class="">Upload photo</span>
                                                         <div class="addimg-icon">
                                                         </div>
                                                         <input class="upload custom-upload remove-custom-upload" title="Choose a file to upload" required="" data-class=".post-photos .img-row" multiple="true" type="file">
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <p class="photolabelinfo">Please add three cover photos for your profile</p>
                                    </div>
                                 </div>
                              </form>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="valign-wrapper additem_modal_footer modal-footer">
      <a href="javascript:void(0)" class="btngen-center-align close_modal open_discard_modal waves-effect">Cancel</a>
      <a href="javascript:void(0)" class="btngen-center-align waves-effect">Publish</a>
   </div>
</div>


<link rel="stylesheet" type="text/css" href="css/owl.carousel.css">
<script type="text/javascript" src="js/owl.carousel.js"></script>
<script type="text/javascript" src="js/wall.js"></script>

<?php include("script.php"); ?>