<?php include("header.php"); ?>
<?php include("common/menu.php"); ?>
<div class="floating-icon">
   <div class="scrollup-btnbox anim-side btnbox scrollup-float">
      <div class="scrollup-button float-icon">
         <span class="icon-holder ispan">
            <i class="mdi mdi-arrow-up-bold-circle"></i>
         </span>
      </div>
   </div>
</div>
<div class="clear"></div>
<div class="container page_container pages_container">
   <?php include("common/leftmenu.php"); ?>
   <div class="fixed-layout">
      <div class="collection-page event-detail-page pb-0 m-t-50">
         <div class="combined-column wide-open main-page full-page">
            <div class="width-100 m-top">
              <div class="page-content">
                <div class="container">

                  <div class="row mx-0 valign-wrapper">
                    <div class="col m6">
                      <div class="header-content">
                        <h1><span class="text-primary ff-bold">Visit</span> from Home</h1>
                        <p>Stay inspired and experience <br> the very best of <?=$st_nm_S?>, from home.</p>
                      </div>
                    </div>
                    <div class="col m6 text-right">
                      <a href="javascript:void(0)" class="vtourCreateAction"> 
                        <span class="hidden-sm">BECOME</span> HOST
                      </a>
                    </div>
                  </div>

                   <div class="row mx-0">
                    <div class="col s12 m6 l4">
                        <div class="card">
                          <div class="card-image">
                            <img src="images/gallrey3.jpg">
                          </div>
                          <div class="card-content">
                              <h3><?=$st_nm_S?></h3>
                              <span> <i class="mdi mdi-account text-primary mdi-16px"></i> Adel Hasanat</span>
                              <p>I am a very simple card. I am good at containing small bits of information. I am convenient because I require little markup to use effectively.</p>
                              <div class="vtour-pp mt-10 pt-10 bt-line valign-wrapper">
                                <p class="left mt-0">
                                  <span class="ff-bold text-primary">$27</span>
                                  <span>/ per person</span>
                                </p>
                                <span class="card-rating right ml-auto">
                                  <i class="mdi mdi-star"></i>
                                  <i class="mdi mdi-star"></i>
                                  <i class="mdi mdi-star"></i>
                                  <i class="mdi mdi-star"></i>
                                  <i class="mdi mdi-star"></i>
                                </span>
                              </div>
                          </div>
                          <div class="card-action pt-0">
                              <a href="vtour-detail.php" class="btn ff-medium bg-primary">Learn More</a>
                          </div>
                        </div>
                     </div>
                     <div class="col s12 m6 l4">
                        <div class="card">
                          <div class="card-image">
                            <img src="images/gallrey3.jpg">
                          </div>
                          <div class="card-content">
                              <h3><?=$st_nm_L?>, Dead Sea</h3>
                              <span> <i class="mdi mdi-account text-primary mdi-16px"></i> Adel Hasanat</span>
                              <p>I am a very simple card. I am good at containing small bits of information. I am convenient because I require little markup to use effectively.</p>
                              <div class="vtour-pp mt-10 pt-10 bt-line valign-wrapper">
                                <p class="left mt-0">
                                  <span class="ff-bold text-primary">$12</span>
                                  <span>/ per person</span>
                                </p>
                                <span class="card-rating right ml-auto">
                                  <i class="mdi mdi-star"></i>
                                  <i class="mdi mdi-star"></i>
                                  <i class="mdi mdi-star"></i>
                                  <i class="mdi mdi-star"></i>
                                  <i class="mdi mdi-star"></i>
                                </span>
                              </div>
                          </div>
                          <div class="card-action pt-0">
                              <a href="vtour-detail.php" class="btn ff-medium bg-primary">Learn More</a>
                          </div>
                        </div>
                     </div>
                     <div class="col s12 m6 l4">
                        <div class="card">
                          <div class="card-image">
                            <img src="images/gallrey3.jpg">
                          </div>
                          <div class="card-content">
                              <h3><?=$st_nm_S?>, Wadi Rum</h3>
                              <span> <i class="mdi mdi-account text-primary mdi-16px"></i> Adel Hasanat</span>
                              <p>I am a very simple card. I am good at containing small bits of information. I am convenient because I require little markup to use effectively.</p>
                              <div class="vtour-pp mt-10 pt-10 bt-line valign-wrapper">
                                <p class="left mt-0">
                                  <span class="ff-bold text-primary">$34</span>
                                  <span>/ per person</span>
                                </p>
                                <span class="card-rating right ml-auto">
                                  <i class="mdi mdi-star"></i>
                                  <i class="mdi mdi-star"></i>
                                  <i class="mdi mdi-star"></i>
                                  <i class="mdi mdi-star"></i>
                                  <i class="mdi mdi-star"></i>
                                </span>
                              </div>
                          </div>
                          <div class="card-action pt-0">
                              <a href="vtour-detail.php" class="btn ff-medium bg-primary">Learn More</a>
                          </div>
                        </div>
                     </div>
                     <div class="col s12 m6 l4">
                        <div class="card">
                          <div class="card-image">
                            <img src="images/gallrey3.jpg">
                          </div>
                          <div class="card-content">
                              <h3><?=$st_nm_S?>, Wadi Musa</h3>
                              <span> <i class="mdi mdi-account text-primary mdi-16px"></i> Adel Hasanat</span>
                              <p>I am a very simple card. I am good at containing small bits of information. I am convenient because I require little markup to use effectively.</p>
                              <div class="vtour-pp mt-10 pt-10 bt-line valign-wrapper">
                                <p class="left mt-0">
                                  <span class="ff-bold text-primary">$23</span>
                                  <span>/ per person</span>
                                </p>
                                <span class="card-rating right ml-auto">
                                  <i class="mdi mdi-star"></i>
                                  <i class="mdi mdi-star"></i>
                                  <i class="mdi mdi-star"></i>
                                  <i class="mdi mdi-star"></i>
                                  <i class="mdi mdi-star"></i>
                                </span>
                              </div>
                          </div>
                          <div class="card-action pt-0">
                              <a href="vtour-detail.php" class="btn ff-medium bg-primary">Learn More</a>
                          </div>
                        </div>
                     </div>
                     <div class="col s12 m6 l4">
                        <div class="card">
                          <div class="card-image">
                            <img src="images/gallrey3.jpg">
                          </div>
                          <div class="card-content">
                              <h3><?=$st_nm_S?>, Amman </h3>
                              <span> <i class="mdi mdi-account text-primary mdi-16px"></i> Adel Hasanat</span>
                              <p>I am a very simple card. I am good at containing small bits of information. I am convenient because I require little markup to use effectively.</p>
                              <div class="vtour-pp mt-10 pt-10 bt-line valign-wrapper">
                                <p class="left mt-0">
                                  <span class="ff-bold text-primary">$14</span>
                                  <span>/ per person</span>
                                </p>
                                <span class="card-rating right ml-auto">
                                  <i class="mdi mdi-star"></i>
                                  <i class="mdi mdi-star"></i>
                                  <i class="mdi mdi-star"></i>
                                  <i class="mdi mdi-star"></i>
                                  <i class="mdi mdi-star"></i>
                                </span>
                              </div>
                          </div>
                          <div class="card-action pt-0">
                              <a href="vtour-detail.php" class="btn ff-medium bg-primary">Learn More</a>
                          </div>
                        </div>
                     </div>
                     <div class="col s12 m6 l4">
                        <div class="card">
                          <div class="card-image">
                            <img src="images/gallrey3.jpg">
                          </div>
                          <div class="card-content">
                              <h3><?=$st_nm_S?></h3>
                              <span> <i class="mdi mdi-account text-primary mdi-16px"></i> Adel Hasanat</span>
                              <p>I am a very simple card. I am good at containing small bits of information. I am convenient because I require little markup to use effectively.</p>
                              <div class="vtour-pp mt-10 pt-10 bt-line valign-wrapper">
                                <p class="left mt-0">
                                  <span class="ff-bold text-primary">$43</span>
                                  <span>/ per person</span>
                                </p>
                                <span class="card-rating right ml-auto">
                                  <i class="mdi mdi-star"></i>
                                  <i class="mdi mdi-star"></i>
                                  <i class="mdi mdi-star"></i>
                                  <i class="mdi mdi-star"></i>
                                  <i class="mdi mdi-star"></i>
                                </span>
                              </div>
                          </div>
                          <div class="card-action pt-0">
                              <a href="vtour-detail.php" class="btn ff-medium bg-primary">Learn More</a>
                          </div>
                        </div>
                     </div>
                     <div class="col s12 m6 l4">
                        <div class="card">
                          <div class="card-image">
                            <img src="images/gallrey3.jpg">
                          </div>
                          <div class="card-content">
                              <h3><?=$st_nm_L?>, Dead Sea</h3>
                              <span> <i class="mdi mdi-account text-primary mdi-16px"></i> Adel Hasanat</span>
                              <p>I am a very simple card. I am good at containing small bits of information. I am convenient because I require little markup to use effectively.</p>
                              <div class="vtour-pp mt-10 pt-10 bt-line valign-wrapper">
                                <p class="left mt-0">
                                  <span class="ff-bold text-primary">$10</span>
                                  <span>/ per person</span>
                                </p>
                                <span class="card-rating right ml-auto">
                                  <i class="mdi mdi-star"></i>
                                  <i class="mdi mdi-star"></i>
                                  <i class="mdi mdi-star"></i>
                                  <i class="mdi mdi-star"></i>
                                  <i class="mdi mdi-star"></i>
                                </span>
                              </div>
                          </div>
                          <div class="card-action pt-0">
                              <a href="vtour-detail.php" class="btn ff-medium bg-primary">Learn More</a>
                          </div>
                        </div>
                     </div>
                     <div class="col s12 m6 l4">
                        <div class="card">
                          <div class="card-image">
                            <img src="images/gallrey3.jpg">
                          </div>
                          <div class="card-content">
                              <h3><?=$st_nm_S?>, Wadi Rum</h3>
                              <span> <i class="mdi mdi-account text-primary mdi-16px"></i> Adel Hasanat</span>
                              <p>I am a very simple card. I am good at containing small bits of information. I am convenient because I require little markup to use effectively.</p>
                              <div class="vtour-pp mt-10 pt-10 bt-line valign-wrapper">
                                <p class="left mt-0">
                                  <span class="ff-bold text-primary">$34</span>
                                  <span>/ per person</span>
                                </p>
                                <span class="card-rating right ml-auto">
                                  <i class="mdi mdi-star"></i>
                                  <i class="mdi mdi-star"></i>
                                  <i class="mdi mdi-star"></i>
                                  <i class="mdi mdi-star"></i>
                                  <i class="mdi mdi-star"></i>
                                </span>
                              </div>
                          </div>
                          <div class="card-action pt-0">
                              <a href="vtour-detail.php" class="btn ff-medium bg-primary">Learn More</a>
                          </div>
                        </div>
                     </div>
                     <div class="col s12 m6 l4">
                        <div class="card">
                          <div class="card-image">
                            <img src="images/gallrey3.jpg">
                          </div>
                          <div class="card-content">
                              <h3><?=$st_nm_S?>, Wadi Musa</h3>
                              <span> <i class="mdi mdi-account text-primary mdi-16px"></i> Adel Hasanat</span>
                              <p>I am a very simple card. I am good at containing small bits of information. I am convenient because I require little markup to use effectively.</p>
                              <div class="vtour-pp mt-10 pt-10 bt-line valign-wrapper">
                                <p class="left mt-0">
                                  <span class="ff-bold text-primary">$24</span>
                                  <span>/ per person</span>
                                </p>
                                <span class="card-rating right ml-auto">
                                  <i class="mdi mdi-star"></i>
                                  <i class="mdi mdi-star"></i>
                                  <i class="mdi mdi-star"></i>
                                  <i class="mdi mdi-star"></i>
                                  <i class="mdi mdi-star"></i>
                                </span>
                              </div>
                          </div>
                          <div class="card-action pt-0">
                              <a href="vtour-detail.php" class="btn ff-medium bg-primary">Learn More</a>
                          </div>
                        </div>
                     </div>
                  </div>
                </div>
              </div>
            </div>
         </div>
      </div>
   </div>
</div>

<!-- become host modal -->
<div id="vtourCreateModal" class="modal tbpost_modal custom_modal split-page main_modal cust-pop dicrease-popup-compose event-detail-modal">
   <div class="modal_content_container">
      <div class="modal_content_child modal-content">
         <div class="popup-title">
            <button class="hidden_close_span close_span waves-effect">
            <i class="mdi mdi-close mdi-20px compose_discard_popup"></i>
            </button>         
            <h3>Create Virtual Tour</h3>
            <a type="button" class="item_done crop_done hidden_close_span custom_close waves-effect" href="javascript:void(0)">Done</a>
         </div>
         <div class="custom_modal_content modal_content" id="createpopup">
            <div class="ablum-yours profile-tab">
               <div class="ablum-box detail-box">
                  <div class="content-holder main-holder">
                     <div class="summery">
                        <div class="dsection bborder expandable-holder expanded">
                           <div class="form-area expandable-area">
                              <form class="ablum-form">
                                 <div class="form-box">
                                    <div class="fulldiv">
                                       <div class="">
                                          <div class="frow">
                                             <div class="caption-holder">
                                                <label>Event title</label>
                                             </div>
                                             <div class="detail-holder">
                                                <div class="input-field">
                                                   <input type="text" placeholder="Homestay title: i.e one room for homestay" class="fullwidth locinput "/>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="fulldiv">
                                       <div class="row">
                                          <div class="col s6">
                                             <div class="frow dropdown782">
                                                <div class="caption-holder">
                                                   <label>Event type</label>
                                                </div>
                                                <a class="dropdown_text dropdown-button-left" href="javascript:void(0)" data-activates="dineFish">
                                                   <span>Choose type</span>
                                                   <i class="zmdi zmdi-caret-down"></i>
                                                </a>
                                                <ul id="dineFish" class="dropdown-privacy dropdown-content custom_dropdown select-dropdown">
                                                   <li>
                                                      <a href="javascript:void(0)">Aperitif</a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">Breakfast</a>
                                                   </li>
                                                </ul>
                                             </div>
                                          </div>
                                       </div> 
                                    </div>
                                    <div class="fulldiv">
                                       <div class="frow">
                                          <div class="caption-holder mb0">
                                             <label>Tell Audience about you</label>
                                          </div>
                                          <div class="detail-holder">
                                             <div class="input-field">
                                                <textarea class="materialize-textarea md_textarea item_tagline" placeholder="What makes you uniquely qualified to host this experience..."></textarea>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="fulldiv">
                                       <div class="frow">
                                          <div class="caption-holder mb0">
                                             <label>Event Description</label>
                                          </div>
                                          <div class="detail-holder">
                                             <div class="input-field">
                                                <textarea class="materialize-textarea md_textarea item_tagline" placeholder="Your activity description is a chance to inspire guest..."></textarea>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="fulldiv">
                                       <div class="frow">
                                          <div class="caption-holder mb0">
                                             <label>Describe where the experience takes place</label>
                                          </div>
                                          <div class="detail-holder">
                                             <div class="input-field">
                                                <textarea class="materialize-textarea md_textarea item_tagline" placeholder="What makes the location special?"></textarea>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="fulldiv mb-5">
                                       <div class="frow">
                                          <div class="caption-holder">
                                             <label>Where will you broadcast from?</label>
                                          </div>
                                          <div class="detail-holder mb-5">
                                            <input name="bath" checked="" type="radio" id="prBt1" value="prBt1">
                                            <label for="prBt1">Indoors or my personal property</label>
                                          </div>
                                          <div class="detail-holder">
                                            <input name="bath" type="radio" id="shBt" value="shBt">
                                            <label for="shBt">Outdoors and on my personal property</label>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="fulldiv">
                                       <div class="frow">
                                          <div class="caption-holder">
                                             <label>What guests should bring</label>
                                          </div>
                                          <p>This list will be emailed to guests when they book your experience to help them prepare.</p>
                                       </div>
                                    </div>
                                    <div class="fulldiv">
                                       <div class="frow">
                                          <div class="caption-holder">
                                             <div class="row">
                                                <div class="col s4">
                                                   <label>Rate per adult guest</label>
                                                </div>
                                                <div class="col s6">
                                                   <div class="detail-holder">
                                                      <div class="input-field">
                                                         <input type="text" placeholder="20" class="fullwidth input-rate" id="createlocation"/>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="col s2">
                                                   <a class="dropdown_text dropdown-button currency_drp" href="javascript:void(0)" data-activates="currency_handler">
                                                      <span class="currency_label">USD</span>
                                                      <i class="zmdi zmdi-caret-down"></i>
                                                   </a>
                                                   <ul id="currency_handler" class="dropdown-privacy dropdown-content custom_dropdown">
                                                      <?php
                                                      $fee = array("USD", "EUR", "YEN", "CAD", "AUE");
                                                      foreach ($fee as $s8032n) {
                                                         ?>
                                                         <li> <a href="javascript:void(0)"><?=$s8032n?></a> </li>
                                                         <?php
                                                      }
                                                      ?>
                                                   </ul>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="fulldiv">
                                       <div class="frow">
                                          <div class="caption-holder">
                                             <label>Where you will host this event</label>
                                          </div>
                                          <div class="detail-holder">
                                             <div class="input-field">
                                                <input type="text" placeholder="Enter city name" class="fullwidth locinput" data-query="all" onfocus="filderMapLocationModal(this)"id="createlocation"/>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="fulldiv">
                                       <div class="row">
                                          <div class="col s6">
                                             <div class="frow dropdown782">
                                                <div class="caption-holder">
                                                   <label>Enter event duration</label>
                                                </div>
                                                <a class="dropdown_text dropdown-button-left" href="javascript:void(0)" data-activates="dineCuisine">
                                                   <span>Choose Duration</span>
                                                   <i class="zmdi zmdi-caret-down"></i>
                                                </a>
                                                <ul id="dineCuisine" class="dropdown-privacy dropdown-content custom_dropdown select-dropdown">
                                                   <li>
                                                      <a href="javascript:void(0)">90 min</a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">120 min</a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">150 min</a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">180 min</a>
                                                   </li>
                                                   <li>
                                                      <a href="javascript:void(0)">210 min</a>
                                                   </li>
                                                </ul>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <!-- <div class="add-dish">
                                       <div class="dish-wrapper">
                                          <div class="fulldiv mobile275">
                                             <div class="frow">
                                                <div class="caption-holder">
                                                   <label>Dish name</label>
                                                </div>
                                                <div class="detail-holder">
                                                   <div class="input-field">
                                                      <input type="text" placeholder="i.e Appetiser, Main Dish, Dessert" class="fullwidth locinput "/>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="fulldiv">
                                             <div class="frow">
                                                <div class="caption-holder mb0">
                                                   <label>Summary</label>
                                                </div>
                                                <div class="detail-holder">
                                                   <div class="input-field">
                                                      <textarea class="materialize-textarea md_textarea item_tagline" placeholder="Tell your guest what you are cooking. Detail description get the most guests joining up!"></textarea>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="fulldiv">
                                          <div class="frow">
                                             <div class="detail-holder">
                                                <a href="" id="addDish"><i class="mdi mdi-plus"></i> Add Dish</a>
                                             </div>
                                          </div>
                                       </div>
                                    </div> -->
                                    <div class="frow nomargin new-post">
                                       <div class="caption-holder">
                                          <label>Event photos</label>
                                       </div>
                                       <div class="detail-holder">
                                          <div class="input-field ">
                                             <div class="post-photos new_pic_add">
                                                <div class="img-row">
                                                   <div class="img-box">
                                                      <div class="custom-file addimg-box add-photo ablum-add">
                                                         <span class="icont">+</span><br><span class="">Upload photo</span>
                                                         <div class="addimg-icon">
                                                         </div>
                                                         <input class="upload custom-upload remove-custom-upload" title="Choose a file to upload" required="" data-class=".post-photos .img-row" multiple="true" type="file">
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <p class="photolabelinfo">Please add atleast 3 high-quality photos to show guests what it's look like to take your experience.</p>
                                    </div>
                                 </div>
                              </form>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="valign-wrapper additem_modal_footer modal-footer">
      <a href="javascript:void(0)" class="btngen-center-align close_modal open_discard_modal waves-effect">Cancel</a>
      <a href="javascript:void(0)" class="btngen-center-align waves-effect">Publish</a>
   </div>
</div>

<?php include('common/discard_popup.php'); ?>
<div id="compose_mapmodal" class="modal map_modal compose_inner_modal modalxii_level1">
   <?php include('common/map_modal.php'); ?>
</div>
<?php include("script.php"); ?>