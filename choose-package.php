<?php include("header.php"); ?>
<?php include("common/menu.php"); ?>
<div class="floating-icon">
   <div class="scrollup-btnbox anim-side btnbox scrollup-float">
      <div class="scrollup-button float-icon">
         <span class="icon-holder ispan">
            <i class="mdi mdi-arrow-up-bold-circle"></i>
         </span>
      </div>
   </div>
</div>
<div class="clear"></div>
<div class="">
   <?php include("common/leftmenu.php"); ?>
   <div class="fixed-layout">
      <div class="collection-page event-detail-page pb-0 m-t-50">
         <div class="combined-column wide-open main-page full-page">
            <div class="width-100 m-top">
               <div class="event-info-wrapper whitebg pt-30">
                  <div class="container">
                     <div class="row mx-0">
                        <div class="col m7 s12">
                           <div class="event-title-container bb-none">
                              <div class="row mx-0 localdine-title mt-5">
                                 <h1 class="event-title ff-bold">Review and pay</h1>
                                 <div class="online-exphosted bb-none">
                                    <p>You can add more friends to this experience and confirm your reservation.</p>
                                    <p>You’ll need an internet connection and the ability to stream audio and video to participate. A link and details on how to join will be included in your booking confirmation email. Guests of all ages can attend.</p>
                                 </div>
                              </div>
                           </div>
                           <div class="detail-title-container book-group">
                              <div class="row mx-0 py-20 online-exphosted bb-none">
                                 <h5 class="ff-medium mb-15">Who’s coming?</h5>
                                 <div class="wrapper">
                                    <h4 class="ff-bold">Book just for you and your group</h4>
                                    <p>Groups of 31 or more guests need to book a private group starting at $793.</p>
                                    <!-- Switch -->
                                    <div class="switch">
                                       <label>
                                          <input type="checkbox">
                                          <span class="lever"></span>
                                       </label>
                                    </div>
                                 </div>
                              </div>
                              <div class="row mx-0 ddl-guest">
                                 <div class="fulldiv">
                                    <div class="row">
                                       <div class="col m6 s12">
                                          <div class="frow dropdown782">
                                             <div class="caption-holder">
                                                <label>Number of guests</label>
                                             </div>
                                             <a class="dropdown_text dropdown-button-left" href="javascript:void(0)" data-activates="dineCuisine">
                                                <span>1</span>
                                                <i class="zmdi zmdi-caret-down right"></i>
                                             </a>
                                             <ul id="dineCuisine" class="dropdown-privacy dropdown-content custom_dropdown select-dropdown">
                                                <li>
                                                   <a href="javascript:void(0)">1</a>
                                                </li>
                                                <li>
                                                   <a href="javascript:void(0)">2</a>
                                                </li>
                                                <li>
                                                   <a href="javascript:void(0)">3</a>
                                                </li>
                                                <li>
                                                   <a href="javascript:void(0)">4</a>
                                                </li>
                                                <li>
                                                   <a href="javascript:void(0)">5</a>
                                                </li>
                                                <li>
                                                   <a href="javascript:void(0)">6</a>
                                                </li>
                                                <li>
                                                   <a href="javascript:void(0)">7</a>
                                                </li>
                                                <li>
                                                   <a href="javascript:void(0)">8</a>
                                                </li>
                                                <li>
                                                   <a href="javascript:void(0)">9</a>
                                                </li><li>
                                                   <a href="javascript:void(0)">10</a>
                                                </li>
                                             </ul>
                                          </div>
                                       </div>
                                       <div class="col m10 s12 online-exphosted bb-none">
                                          <p>There are <span class="ff-medium"> only 100 spots left </span> on this experience. Be sure to book soon.</p>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="row mx-0 online-exphosted guest-detail">
                                 <h5>Guest details</h5>
                                 <p>
                                    The info entered will be used to add people to this reservation.
                                 </p>
                              </div>
                              <div class="row mx-0 online-exphosted py-15 valign-wrapper">
                                 <div class="col s6">
                                    <p class="mb0 mt-0">Noor Ul haq</p>
                                 </div>
                                 <div class="col s6">
                                    <img class="right circle" src="https://a0.muscache.com/defaults/user_pic-225x225.png?v=3" height="48" width="48" alt="text">
                                 </div>
                              </div>
                              <div class="row mt-30 mb-20 package-form">
                                 <div class="col m6 s12">
                                    <div class="frow">
                                       <div class="caption-holder">
                                          <label>Street address</label>
                                       </div>
                                       <div class="detail-holder">
                                          <div class="input-field">
                                             <input type="text">
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="col m6 s12">
                                    <div class="frow">
                                       <div class="caption-holder">
                                          <label>Apt #</label>
                                       </div>
                                       <div class="detail-holder">
                                          <div class="input-field">
                                             <input type="text">
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="col m6 s12">
                                    <div class="frow">
                                       <div class="caption-holder">
                                          <label>City</label>
                                       </div>
                                       <div class="detail-holder">
                                          <div class="input-field">
                                             <input type="text">
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="col m6 s12">
                                    <div class="frow">
                                       <div class="caption-holder">
                                          <label>State</label>
                                       </div>
                                       <div class="detail-holder">
                                          <div class="input-field">
                                             <input type="text">
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="col m6 s12">
                                    <div class="frow">
                                       <div class="caption-holder">
                                          <label>ZIP code</label>
                                       </div>
                                       <div class="detail-holder">
                                          <div class="input-field">
                                             <input type="text">
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="col m6 s12">
                                    <div class="frow dropdown782">
                                       <div class="caption-holder">
                                          <label>Country/region</label>
                                       </div>
                                       <a class="dropdown_text dropdown-button-left" href="javascript:void(0)" data-activates="countryReg">
                                          <span>Jordan</span>
                                          <i class="zmdi zmdi-caret-down right"></i>
                                       </a>
                                       <ul id="countryReg" class="dropdown-privacy dropdown-content custom_dropdown select-dropdown">
                                          <li>
                                             <a href="javascript:void(0)">Jordan</a>
                                          </li>
                                          <li>
                                             <a href="javascript:void(0)">USA</a>
                                          </li>
                                          <li>
                                             <a href="javascript:void(0)">Pakistan</a>
                                          </li>
                                       </ul>
                                    </div>
                                 </div>
                                 <div class="col m12 s12">
                                    <p>
                                       By selecting the button below, you agree to the Guest Release and Waiver, the Cancellation Policy, and the Guest Refund Policy.
                                    </p>
                                 </div>
                                 <div class="col m12 s12">
                                    <a href="javascript:void(0)" class="confirm bg-primary white-text ff-bold">Confirm</a>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="col m5 s12">
                           <div class="packages-rightsec">
                              <div class="row mx-0 py-10 online-exphosted guest-detail">
                                 <div class="col s8">
                                    <h5>Sangria and Secrets with Drag Queens</h5>
                                    <p>
                                       90 min experience <br>
                                       Hosted by Pedro
                                    </p>
                                 </div>
                                 <div class="col s4 media-sec">
                                    <img src="https://a0.muscache.com/im/pictures/lombard/MtTemplate-1652939-media_library/original/a7f906fc-f4c8-4b27-a5e2-b783115350e4.jpeg?aki_policy=poster" class="img-fluid" alt="text">
                                 </div>
                              </div>
                              <div class="row mx-0 py-10 mt-15 online-exphosted guest-detail">
                                 <div class="col s12">
                                    <p>
                                       Sat, Nov 14 <br>
                                       1:00 PM − 2:30 PM (PKT)
                                    </p>
                                 </div>
                              </div>
                              <div class="row mx-0 py-10 mt-15 online-exphosted guest-detail">
                                 <div class="col s8">
                                    <p>$34.23 x 3 guests</p>
                                 </div>
                                 <div class="col s4">
                                    <p class="right">$102.69</p>
                                 </div>
                              </div>
                              <div class="row mx-0 py-10 mt-15 online-exphosted guest-detail">
                                 <div class="col s8">
                                    <p class="ff-bold">Total (USD)</p>
                                 </div>
                                 <div class="col s4">
                                    <p class="ff-bold right">$102.69</p>
                                 </div>
                              </div>
                              <div class="row mx-0 py-10 online-exphosted guest-detail cancel-pol">
                                 <div class="col s12">
                                    <a href="">Cancellation policy</a>
                                    <p>
                                       Cancel within 24 hours of booking for a full refund under the experiences cancellation policy. Cancellations related to coronavirus (COVID-19) are addressed under our extenuating circumstances policy. For reservations made after March 14, coronavirus (COVID-19) cancellations will not qualify as an extenuating circumstance. Please review official advisories and take the necessary precautions.
                                    </p>
                                 </div>
                              </div>
                              <div class="row mx-0 py-10 online-exphosted guest-detail bb-none">
                                 <div class="col s12">
                                    <h5>Review guest requirements</h5>
                                    <p>
                                       You’ll need an internet connection and the ability to stream audio and video to participate. A link and details on how to join will be included in your booking confirmation email.
                                    </p>
                                    <p>Guests of all ages can attend.</p>
                                    <h5>Language</h5>
                                    <p>
                                       This experience is hosted in English, Portuguese, and Spanish.
                                    </p>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

<?php include('common/discard_popup.php'); ?>


<link rel="stylesheet" type="text/css" href="css/owl.carousel.css">
<script type="text/javascript" src="js/owl.carousel.js"></script>
<script type="text/javascript" src="js/wall.js"></script>

<?php include('common/datepicker.php'); ?>

<?php include("script.php"); ?>
<script type="text/javascript">

   $('.dept-return').daterangepicker();

</script>