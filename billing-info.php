
<?php include("header.php"); ?>
<div class="floating-icon">
   <div class="scrollup-btnbox anim-side btnbox scrollup-float">
      <div class="scrollup-button float-icon"><span class="icon-holder ispan"><i class="mdi mdi-arrow-up-bold-circle"></i>
</span></div>
   </div>
</div>
<div class="clear"></div>
<?php include("common/leftmenu.php"); ?>
<div class="fixed-layout ipad-mfix">
   <div class="main-content with-lmenu sub-page billinginfo-page main-page">
      <div class="combined-column wide-open">
         <div class="content-box">
            <div class="container">
               <div class="cbox-title nborder">
                  <i class="zmdi zmdi-file-text"></i>
                  Billing Information
               </div>
               <div class="cbox-desc">
                  <div class="fake-title-area">
                     <ul class="nav nav-tabs">
                        <li class="active"><a href="#billinginfo-history" data-toggle="tab" aria-expanded="false">Purchase History</a></li>
                        <li><a href="#billinginfo-currency" data-toggle="tab" aria-expanded="false">Currency</a></li>
                        <li><a href="#billinginfo-auto" data-toggle="tab" aria-expanded="false">Auto-Recharge</a></li>
                     </ul>
                  </div>
                  <div class="tab-content">
                     <div class="tab-pane fade active in main-pane" id="billinginfo-history">
                        <div class="history-table">
                           <div class="table-navigation">	
                              <a href="javascript:void(0)" class="prev-month left"><i class="zmdi zmdi-chevron-left"></i>March 2016</a>
                              <span>April 2016</span>
                              <a href="javascript:void(0)" class="next-month right">May 2016<i class="zmdi zmdi-chevron-right"></i></a>
                           </div>
                           <div class="table-responsive">
                              <table class="striped">
                                 <thead>
                                    <tr>
                                       <th>Date</th>
                                       <th>Order#</th>
                                       <th>Item</th>
                                       <th>Paid Via</th>
                                       <th>Amount</th>
                                       <th>Status</th>
                                    </tr>
                                 </thead>
                                 
                                 <tbody class="bill-info-table">
                                    <tr>
                                       <td>April 29</td>
                                       <td>54645928232</td>
                                       <td>VIP Membership</td>
                                       <td>Credit Card(visa)</td>
                                       <td>$30</td>
                                       <td class="complet-bill">Completed</td>
                                    </tr>
                                    <tr>
                                       <td>April 29</td>
                                       <td>54645928232</td>
                                       <td>VIP Membership</td>
                                       <td>Credit Card(visa)</td>
                                       <td>$30</td>
                                       <td class="pending-bill">Pending</td>
                                    </tr>
                                    <tr>
                                       <td>April 29</td>
                                       <td>54645928232</td>
                                       <td>VIP Membership</td>
                                       <td>Credit Card(visa)</td>
                                       <td>$30</td>
                                       <td class="canceled-bill">Cancelled</td>
                                    </tr>
                                    <tr>
                                       <td>April 29</td>
                                       <td>54645928232</td>
                                       <td>VIP Membership</td>
                                       <td>Credit Card(visa)</td>
                                       <td>$30</td>
                                       <td class="process-bill">In Process</td>
                                    </tr>
                                 </tbody>
                              </table>
                           </div>
                        </div>
                     </div>
                     <div class="tab-pane fade main-pane" id="billinginfo-currency">
                     </div>
                     <div class="tab-pane fade main-pane" id="billinginfo-auto">
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <?php include('common/chat.php'); ?>
   </div>
</div>
<?php include("common/footer.php"); ?>
</div>	
<?php include("script.php"); ?>
</body>
</html>